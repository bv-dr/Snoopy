.. image:: https://gitlab.com/bv-dr/Snoopy/badges/master/pipeline.svg?branch=master
   :target: https://gitlab.com/bv-dr/Snoopy/badges/master/pipeline.svg
   :alt: Build Status


Snoopy library
==============

Documentation and tutorials : `https://bv-dr.gitlab.io/Snoopy <https://bv-dr.gitlab.io/Snoopy>`_

Snoopy is an open source library for marine applications. Among other things, it contains the following sub-packages

* 
  Spectral : Routines for spectral analysis of sea-keeping results

  * Wave spectra (Jonswap, Ochihubble, spreading function...)
  * Manipulation of transfer function (RAOs)
  * Response spectrum
  * 2nd order response spectrum


* 
  TimeDomain

  * Reconstruction of seakeeping results in time domain (1st and 2nd order)
  * Convolution for wave radiation in time domain (from added-mass).
  * Upcrossing analysis

* 
  Fatigue

  * Rainflow counting
  * SN-Curves
  * Spectral fatigue

* 
  Meshing

* 
   WaveKinematics : (Linear, wheeler stretching, 2nd order...)
  
* 
   Statistics

   * Peak Over Threshold
   * Long term analysis
   * IFORM contour
  


Example of use
--------------

.. code-block:: python

   >>> from Snoopy import Spectral as sp
   >>> spec = sp.Jonswap( hs = 1.0 , tp = 10.0 , gamma = 1.0)
   >>> spec.plot()

.. image:: https://gitlab.com/bv-dr/Snoopy/-/raw/master/Doc/pictures/Jonswap.png
   :width: 400
   :alt: Jonswap picture



How to install
--------------

Snoopy is available on pypi.org and can be using pip: 

.. code-block:: sh

   pip install snoopy-bv


In case conda is used, it is recommended to install dependencies using conda (hence the --no-deps in above command line):

.. code-block:: sh

   conda install xarray numpy scipy pandas matplotlib h5py tqdm Deprecated
   pip install snoopy-bv --no-deps



How to build from sources
-------------------------

To build the package from sources, instructions are given in `readme_dev.rst <https://gitlab.com/bv-dr/Snoopy/-/blob/master/readme_dev.rst>`_ 

