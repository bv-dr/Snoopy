@echo off
rem arguments:
rem %1: build directory, relative to current working directory
rem %2: Miniforge environment
set PYTHONPATH=%~dp0;%~dp0%1%
pushd %1%
GeometryRunUnitTests -f JUNIT 1>GeometryRunUnitTests.xml
FunctionsRunUnitTests -f JUNIT 1>FunctionsRunUnitTests.xml
FiniteDifferenceRunUnitTests -f JUNIT 1>FiniteDifferenceRunUnitTests.xml
InterpolatorsRunUnitTests -f JUNIT 1>InterpolatorsRunUnitTests.xml
SolversRunUnitTests -f JUNIT 1>SolversRunUnitTests.xml
MechanicsRunUnitTests -f JUNIT 1>MechanicsRunUnitTests.xml
popd
call C:\ProgramData\miniforge3\Scripts\activate %2
pushd Snoopy
pytest Spectral\Tests Meshing\Tests TimeDomain\Tests Statistics\Tests Fatigue\Tests Fatigue\Tests WaveKinematic\Tests Mechanics\Tests Tools\Tests Math\Tests Math\Tests Reader\Tests
popd