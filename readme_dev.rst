
How to build from sources
-------------------------

* 
  In Snoopy directiory, create a build folder, for instance "build"

* 
  Run cmake

go in "build" directory and run :

.. code-block:: sh

   cmake .. -G"Visual Studio 17 2022" -Ax64 

* Compile

.. code-block:: sh

   cmake --build . --config Release


Alternatively, if developments are made only in the python part, the c++ pre-compiled binaries code can be retrieved from the CI/CD artifact (or  `pypi wheel package <https://pypi.org/project/snoopy-bv/#files>`_  ) and copied to /bin directory. In such case, the "version.py" file needs to be copied as well.



How to install and use in develop mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To install in "developer mode" (uses current sources, does not copy in python environment) :

.. code-block:: sh

   pip install -e C:\path_to_snoopy_location --no-deps


How to install
^^^^^^^^^^^^^^

to install in python environment :

.. code-block:: sh

   pip install pathToRootfolder


How to package
^^^^^^^^^^^^^^


* The folder containing the .pyd should be in the SNOOPY_PYD environement variable

.. code-block:: sh

   python setup.py bdist_wheel


How to install on Linux (tested with Ubuntu 20.04 with bash terminal)
---------------------------------------------------------------------


* clone in the folder snoopy_dev (using snoopy as name may cause problems)

.. code-block:: sh

   git clone https://mar-gitlab.bureauveritas.com/applications/dr/snoopy.git snoopy_dev



* compile the code in a build directory

.. code-block:: sh

   cd snoopy_dev/Snoopy
   mkdir build
   cd build
   cmake ..
   make -j 10  


* install the package

.. code-block:: sh

   cd ../..
   pip install .   


To install in develop mode:   

.. code-block:: sh

   pip install -e .


Dev notes:
----------

Commit message
^^^^^^^^^^^^^^


* [FUN] : New feature
* [ENH] : Enhancement of existing code
* [BUG] : Bug correction, [BBUG] for really BIG BUG (with consequences in case of use...)
* [QLT] : Quality (Comments, removal of dead code...)
* [FIX] : Correction of compiling issue
* [DAT] : Data added or modified
* [DOC] : Document added or modified
* [TEST] : Test added or modified
* [INI] : Initial commit.

The subpackage should be mentioned after this Flag. For instance : 

[ENH] TimeDomain : Performance optimization of ReconstructionRaoLocal