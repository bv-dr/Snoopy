#!/usr/bin/env python
import os, shutil
from os.path import join
import sys

if __name__ == "__main__" :
    from setuptools import setup, Distribution
    from Snoopy.version import snoopy_tag_only
    from Snoopy import getDevPath

    package_dir= {
                  "Snoopy" : "./Snoopy" ,
                  "Snoopy.Spectral"  : r"./Snoopy/Spectral" ,
                  "Snoopy.Reader"  : r"./Snoopy/Reader" ,
                  "Snoopy.TimeDomain"  : r"./Snoopy/TimeDomain" ,
                  "Snoopy.Statistics"  : r"./Snoopy/Statistics" ,
                  "Snoopy.Math"  : r"./Snoopy/Math" ,
                  "Snoopy.Meshing"  : r"./Snoopy/Meshing" ,
                  "Snoopy.Geometry"  : r"./Snoopy/Geometry" ,
                  "Snoopy.Mechanics"  : r"./Snoopy/Mechanics" ,
                  "Snoopy.Fatigue"  : r"./Snoopy/Fatigue" ,
                  "Snoopy.Tools"  : r"./Snoopy/Tools" ,
                  "Snoopy.WaveKinematic" : r"./Snoopy/WaveKinematic" ,
                  "Snoopy.PyplotTools" : r"./Snoopy/PyplotTools",
                  "Snoopy.Dataset" : r"./Snoopy/Dataset",
                 }


    sharedCodeDir = os.path.abspath(join(os.path.dirname(__file__)))
    devPath = join("Snoopy", getDevPath(join(sharedCodeDir, "Snoopy")))
    #Add compiled dll and pyd (un peu crado...)
    lDll = [ join("Snoopy", getDevPath(join(sharedCodeDir, "Snoopy")), f)  for f in os.listdir( devPath )  if  os.path.splitext(f)[1] in [".dll", ".pyd", ".so"] ]
    folderDLL = join(sharedCodeDir, "Snoopy", "DLLs")
    # cpp compiled files moved to a folder Snoopy/DLLs
    os.makedirs(folderDLL, exist_ok=True)
    for filename in lDll: 
        shutil.copy(filename, folderDLL)

    long_description = open('readme.rst', 'r').read()

    with open('requirements.txt') as f:
         required = f.read().splitlines()

    if sys.platform in ("linux", "linux2"): 
        os_classifier = "Operating System :: POSIX :: Linux"
        options       = {'bdist_wheel': {'plat_name': 'manylinux_2_28_x86_64'}}
    elif sys.platform in ("win32", "win64"): 
        os_classifier = "Operating System :: Microsoft :: Windows"
        options       = {}
    else : 
        os_classifier = "Operating System :: Unknown"
        options       = {}

    class BinaryDistribution(Distribution):
        """Distribution which always forces a binary package with platform name"""
        def has_ext_modules(foo):
            return True
     
    setup(name='snoopy-bv',
          version=snoopy_tag_only.replace("v", ""),
          description='DR C++/Python library',
          long_description=long_description,
          long_description_content_type="text/x-rst",
          author='Research Department BV M&O',
          author_email='',
          url= r'https://gitlab.com/bv-dr/Snoopy',
          package_dir = package_dir,
          packages = list(package_dir.keys()) ,
          package_data = {"Snoopy": ["DLLs/*", "PyplotTools/snoopy.mplstyle", "Mechanics/hydro_data.csv", "Dataset/rec34_rev2.csv" ]}, 
          include_package_data=True,
          install_requires = required,
          distclass=BinaryDistribution,
          classifiers=[
            "Intended Audience :: Science/Research",
            "Topic :: Scientific/Engineering",
            "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
            "Programming Language :: Python :: 3.11",
            os_classifier,
        ],
          options=options
         )
         
    # Clean Dlls folder, so that SNOOPY_PYD is not overiden by the its presence
    shutil.rmtree(folderDLL)
