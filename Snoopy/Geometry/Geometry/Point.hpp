/*!
 * \file Point.hpp
 * \brief Implementing a small 3D point class
 *
 * \author Thomas Gazzola
 */

#pragma once
#ifndef __BV_Geometry_Point_hpp__
#define __BV_Geometry_Point_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Dense>
#include "Geometry/XYZ.hpp"

namespace BV {
/*!
 * \brief Namespace Geometry is here to include pure geometric objects, 
 *        such as points or vectors.
 */
namespace Geometry {

/*!
 * \brief A small 3D point class
 */
class GEOMETRY_API Point: public XYZ<Point>
{
public:
    /*!
     * \copydoc XYZ::XYZ(const double & x, const double & y, const double & z)
     */
    Point(const double & x, const double & y, const double & z) :
        XYZ<Point>(x, y, z)
    {
        // Left blank
    }

    /*!
     * \copydoc XYZ::XYZ(void)
     */
    Point(void) :
        XYZ<Point>()
    {
        // Left blank
    }

#ifdef BV_HAVE_LIBCONFIGPP
    /*!
     * \copydoc XYZ::XYZ(const BV::Utils::ConfigFile & cfg, const string & groupName)
     */
    Point(const BV::Utils::ConfigFile & cfg, const std::string & groupName):
    XYZ<Point>( cfg, groupName )
    {
        // Left blank
    }
#endif

    Point(const Point & point) :
        XYZ<Point>(point)
    {
        // Left blank
    }

    Point(const Eigen::Vector3d & xyz) :
        XYZ<Point>(xyz)
    {
        // Left blank
    }

    /*!
     * \brief Returns a vector representing the difference between two points.
     * \param[in] point The other point.
     */
    Vector operator-(const Point & point) const ;

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point & operator+=(const Vector & vect) ;

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point & operator-=(const Vector & vect) ;

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point operator+(const Vector & vect) const ;

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point operator-(const Vector & vect) const ;

    /*!
     * \brief Converter to Point
     */
    Point toPoint(void) const ;

    /*!
     * \brief Converter to Vector
     */
    Vector toVector(void) const ;

} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Vector_hpp__
