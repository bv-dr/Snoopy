#include <iostream>
#include <unsupported/Eigen/MatrixFunctions>
#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

#include "Geometry/sx3Manipulation.hpp"


namespace BV {
namespace Geometry {
namespace Rotation {



double RotationVector::theta1(void) const
{
    return state_(0) ;
}
double RotationVector::theta2(void) const
{
    return state_(1) ;
}
double RotationVector::theta3(void) const
{
    return state_(2) ;
}

double & RotationVector::theta1(void)
{
    return state_(0) ;
}
double & RotationVector::theta2(void)
{
    return state_(1) ;
}
double & RotationVector::theta3(void)
{
    return state_(2) ;
}

double RotationVector::norm(void) const
{
    return state_.norm() ;
}

const Eigen::Vector3d & RotationVector::getState() const 
{
    return state_ ;
}

RotationVector & RotationVector::operator=(const RotationVector & other)
{
    state_ = other.state_ ;
    return *this ;
}

Eigen::Matrix3d RotationVector::getMatrix(void) const
{
    using BV::Geometry::R3Toso3 ;
    Eigen::Matrix3d thetaSkew(R3Toso3(state_)) ;
    
    return thetaSkew.exp() ;
}

void RotationVector::addOtherRotationAtLeft(const ABC & abc)
{   
    Quaternion quat(toQuaternion()) ;
    quat.addOtherRotationAtLeft(abc.toQuaternion()) ;
    state_ = RotationVector(quat).getState() ;
}

void RotationVector::addOtherRotationAtRight(const ABC & abc)
{
    Quaternion quat(toQuaternion()) ;
    quat.addOtherRotationAtRight(abc.toQuaternion()) ;
    state_ = RotationVector(quat).getState() ;
}

void RotationVector::subtractOtherRotationAtLeft(const ABC & abc)
{
    Quaternion quat(toQuaternion()) ;
    quat.subtractOtherRotationAtLeft(abc.toQuaternion()) ;
    state_ = RotationVector(quat).getState() ;
}

void RotationVector::subtractOtherRotationAtRight(const ABC & abc)
{
    Quaternion quat(toQuaternion()) ;
    quat.subtractOtherRotationAtRight(abc.toQuaternion()) ;
    state_ = RotationVector(quat).getState() ;
}


Eigen::Vector3d RotationVector::rotate(const Eigen::Vector3d & vect) const
{
    return (toAxisAndAngle().rotate(vect)) ;
    //return getMatrix() * vect ;
}

void RotationVector::inverse(void)
{
    state_(0) = -state_(0) ;
    state_(1) = -state_(1) ;
    state_(2) = -state_(2) ;
}

Eigen::VectorXd RotationVector::unknowns(void) const
{
    unknowns_(0) = state_(0) ;
    unknowns_(1) = state_(1) ;
    unknowns_(2) = state_(2) ;
    return unknowns_ ;
}

void RotationVector::unknowns(const Eigen::VectorXd & unknowns)
{
    assert( unknowns.size() == nUnknowns() ) ;
    state_ = unknowns ;
    // state_(0) = unknowns_(0) ;
    // state_(1) = unknowns_(1) ;
    // state_(2) = unknowns_(2) ;
}

Eigen::VectorXd RotationVector::constraints(void)
{
    return constraints_ ;
}

void RotationVector::reset(void)
{
    state_.setZero() ;
}

void RotationVector::copy(const RotationVector & other)
{
    state_ = other.state_ ;
}

RotationMatrix RotationVector::toRotationMatrix(void) const
{
    return RotationMatrix(getMatrix()) ;
}
BasisVectors RotationVector::toBasisVectors(void) const
{
    return BasisVectors(getMatrix()) ;
}

Quaternion RotationVector::toQuaternion( void ) const
{
    if(norm()!=0)
    {
        Details::EigenQuaternion q(Eigen::AngleAxis<double>(norm(), state_ / norm())) ;
        return Quaternion(q.w(), q.x(), q.y(), q.z()) ;
    }
    return Quaternion() ;
    
}

AxisAndAngle RotationVector::toAxisAndAngle(void) const
{
    if(norm()!=0)
    {
        return AxisAndAngle(Vector(state_),norm()) ;   
    }    
    return AxisAndAngle() ;
    
}

RotationVector RotationVector::toRotationVector(void) const
{
    return *this ;
}

HorizontalPlane RotationVector::toHorizontalPlane(void) const
{
    return HorizontalPlane(getMatrix()) ;
}

MRP RotationVector::toMRP(void) const
{
    return MRP(getMatrix()) ;
}

RotationVector & RotationVector::operator=(const ABC & other)
{
    state_ = other.toRotationVector().state_ ;
    return *this ;
}

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
