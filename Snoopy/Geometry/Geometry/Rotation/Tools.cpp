#include "Geometry/Rotation/Tools.hpp"
#include "Geometry/Exceptions.hpp"
#include "Geometry/GeometryTypedefs.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {
namespace Details {

void CheckRotationMatrix(const Eigen::Matrix3d & R)
{
    using BV::Geometry::Exceptions::RotationInitialisationException ;
    auto isClose = [](double a, double b) -> bool
    {
        return std::abs(a-b) < BV::Details::epsilon ;
    } ;
    // This matrix should represent an ortho-normal basis
    Eigen::Matrix3d tRR(R.transpose() * R) ;
    if ((!isClose(tRR(0, 0), 1.)) || (!isClose(tRR(1, 1), 1.)) || (!isClose(tRR(2, 2), 1.)))
    {
        throw RotationInitialisationException("Incorrect rotation matrix: not normalized") ;
    }
    if ((!isClose(tRR(0, 1), 0.)) || (!isClose(tRR(0, 2), 0.)) || (!isClose(tRR(1, 2), 0.)))
    {
        throw RotationInitialisationException("Incorrect rotation matrix: not orthogonal") ;
    }
    if (!isClose(R.determinant(), 1.))
    {
        throw RotationInitialisationException("Incorrect rotation matrix: not right-handed") ;
    }
}

double AngleMinusPi_Pi(double angle)
{
    if(angle == 0.)
    {
        return angle ;
    }
    else if(angle>0.)
     {
         return std::fmod(angle+M_PI, 2.0*M_PI)-M_PI;
     }
    else
    {
        return std::fmod(angle-M_PI, 2.0*M_PI)+M_PI;
    }
}

} // end of namespace Details
} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
