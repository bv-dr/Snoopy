/*!
 * \file AxisAndAngle.hpp
 */

#pragma once
#ifndef __BV_Geometry_Rotation_AxisAndAngle_hpp__
#define __BV_Geometry_Rotation_AxisAndAngle_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Geometry>
#include <Eigen/Dense>

#include "Geometry/Vector.hpp"

#include "Geometry/Rotation/ABC.hpp"

#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

/*!
 * \brief Defines a rotation thanks to an axis and a point.
 */
class GEOMETRY_API AxisAndAngle: public Details::RotatorABC<AxisAndAngle>
{
private:
    Eigen::AngleAxis<double> innerData_ ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs from a rotation matrix
     */
    template<typename Derived>
    AxisAndAngle(const Eigen::MatrixBase<Derived> & mat,
                 typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC<AxisAndAngle>(4, 1),
        innerData_(mat)
    {
        Details::CheckRotationMatrix(mat) ;
    }

    /*!
     * \brief Dummy constructor
     *
     * Angle is initialized to 0.
     * Axis is initialized to \f$(1,0,0)\f$.
     */
    AxisAndAngle( void ): 
        Details::RotatorABC<AxisAndAngle>(4, 1),
        innerData_(0., Eigen::Vector3d(1., 0., 0.))
    {
        // Left blank
    }

    /*!
     * \brief Constructing from values
     *
     * Note that the vector part is normalized.
     */
    AxisAndAngle( const Eigen::Vector3d & axis, const double & angle ):
        Details::RotatorABC<AxisAndAngle>(4, 1),
        innerData_(angle, axis.normalized())
    {
        // Left blank
    }

    /*!
     * \copydoc BV::Geometry::Rotation::AxisAndAngle::AxisAndAngle(const Eigen::Vector3d &, const double &)
     */
    AxisAndAngle( const Geometry::Vector & axis, const double & angle ):
        Details::RotatorABC<AxisAndAngle>(4, 1),
        innerData_(angle, axis.normalised().toArray())
    {
        // Left blank
    }

    /*!
     * \brief Constructs from a Eigen::VectorXd
     *
     * The first three components are the axis part. The fourth is the angle.
     *
     * Note that the vector part is normalized.
     */
    // AxisAndAngle(const Eigen::Ref<const Eigen::VectorXd > & unknowns) :
    AxisAndAngle(const Eigen::VectorXd & unknowns) :
        Details::RotatorABC<AxisAndAngle>(4, 1)
    {
        assert(unknowns.size() == nUnknowns_) ;
        Eigen::Vector3d axis(unknowns(0), unknowns(1), unknowns(2)) ;
        double angle(unknowns(3)) ;
        innerData_ =  Eigen::AngleAxis<double>(angle, axis.normalized()) ;
    }

    /*!
     * \brief Constructs a copy of provided AxisAndAngle.
     * \param[in] other The other AxisAndAngle object.
     */
    AxisAndAngle(const AxisAndAngle & other) :
        Details::RotatorABC<AxisAndAngle>(4, 1),
        innerData_(other.angle(), other.axis())
    {
        // Left blank
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in an AxisAndAngle.
     * \param[in] other The other Rotator object.
     */
    AxisAndAngle(const ABC & other):
        AxisAndAngle(other.toAxisAndAngle())
    {
        // Left blank
    }

    /*!
     * \brief Angle getter (by reference).
     * \return the angle as a double
     */
    double & angle(void) { return innerData_.angle() ; }

    /*!
     * \brief Angle getter (by copy).
     * \return the angle as a double
     */
    double angle(void) const { return innerData_.angle() ; }

    /*!
     * \brief Axis getter (by reference).
     * \return the axis as an Eigen::Vector3d
     */
    Eigen::Vector3d & axis(void) { return innerData_.axis() ; }

    /*!
     * \brief Axis getter (by copy).
     * \return the axis as an Eigen::Vector3d
     */
    Eigen::Vector3d axis(void) const { return innerData_.axis() ; }

    Eigen::Matrix3d getMatrix(void) const { return innerData_.toRotationMatrix() ; }

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<AxisAndAngle>::rotate ;

    void reset() ;

    void copy(const AxisAndAngle & other) ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;
    Eigen::VectorXd unknowns(void) const ;
    void unknowns(const Eigen::VectorXd & unknowns) ;
    Eigen::VectorXd constraints(void) ;
    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;

    /*! \brief Converter from AxisAndAngle to Quaternion.
     *
     * Axis and Angle is parametrized by an normalised axis e and an angle theta
     * q.w = cos(theta/2.)
     * q.x = e(0) * sin(theta/2.)
     * q.y = e(1) * sin(theta/2.)
     * q.z = e(2) * sin(theta/2.)
     * \return Quaternion(q.w, q.x, q.y, q.z)
     *
     */
    Quaternion toQuaternion( void ) const ;
    AxisAndAngle toAxisAndAngle(void) const ;
    MRP toMRP(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    RotationVector toRotationVector(void) const ;

    //// Overload from RotatorABC<T>
    AxisAndAngle & operator=(const ABC & other) ;
    AxisAndAngle & operator=(const AxisAndAngle & other) ;

} ;

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::AxisAndAngle>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::AXIS_AND_ANGLE ;
} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_AxisAndAngle_hpp__
