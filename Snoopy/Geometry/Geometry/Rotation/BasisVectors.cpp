/*!
 * \file BasisVectors.cpp
 * \brief FILL THIS!
 */

#include <Eigen/Geometry>

#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"


namespace BV {
namespace Geometry {
namespace Rotation {

void BasisVectors::assignDirectorsFromMatrixComposition_(Eigen::Matrix3d RLeft,
                                                         Eigen::Matrix3d RRight)
{
    RLeft *= RRight ;
    d1_.fromArray(RLeft.col(0)) ;
    d2_.fromArray(RLeft.col(1)) ;
    d3_.fromArray(RLeft.col(2)) ;
}

void BasisVectors::addOtherRotationAtLeft(const ABC & abc)
{
    Eigen::Matrix3d abcMat(abc.toRotationMatrix().getMatrix()) ;
    Eigen::Matrix3d selfMat(getMatrix()) ;
    assignDirectorsFromMatrixComposition_(abcMat, selfMat) ;

}
void BasisVectors::addOtherRotationAtRight(const ABC & abc)
{
    Eigen::Matrix3d selfMat(getMatrix()) ;
    Eigen::Matrix3d abcMat(abc.toRotationMatrix().getMatrix()) ;
    assignDirectorsFromMatrixComposition_(selfMat, abcMat) ;
}
void BasisVectors::subtractOtherRotationAtLeft(const ABC & abc)
{
    Eigen::Matrix3d abcMatTranspose(abc.toRotationMatrix().getMatrix().transpose()) ;
    Eigen::Matrix3d selfMat(getMatrix()) ;
    assignDirectorsFromMatrixComposition_(abcMatTranspose, selfMat) ;
}

void BasisVectors::subtractOtherRotationAtRight(const ABC & abc)
{
    Eigen::Matrix3d selfMat(getMatrix()) ;
    Eigen::Matrix3d abcMatTranspose(abc.toRotationMatrix().getMatrix().transpose()) ;
    assignDirectorsFromMatrixComposition_(selfMat, abcMatTranspose) ;
}

Eigen::Vector3d BasisVectors::rotate(const Eigen::Vector3d & vect) const
{
    return getMatrix() * vect ;
}

void BasisVectors::inverse(void)
{
    Vector d1(d1_.x(), d2_.x(), d3_.x()) ;
    Vector d2(d1_.y(), d2_.y(), d3_.y()) ;
    Vector d3(d1_.z(), d2_.z(), d3_.z()) ;
    d1_ = d1 ;
    d2_ = d2 ;
    d3_ = d3 ;
}

Eigen::VectorXd BasisVectors::unknowns( void ) const
{
    unknowns_(0) = d1_.x() ;
    unknowns_(1) = d1_.y() ;
    unknowns_(2) = d1_.z() ;
    unknowns_(3) = d2_.x() ;
    unknowns_(4) = d2_.y() ;
    unknowns_(5) = d2_.z() ;
    unknowns_(6) = d3_.x() ;
    unknowns_(7) = d3_.y() ;
    unknowns_(8) = d3_.z() ;
    return unknowns_ ;
}

void BasisVectors::unknowns( const Eigen::VectorXd & unknowns )
{
    assert( unknowns.size() == nUnknowns() ) ;
    d1_.x() = unknowns(0) ;
    d1_.y() = unknowns(1) ;
    d1_.z() = unknowns(2) ;
    d2_.x() = unknowns(3) ;
    d2_.y() = unknowns(4) ;
    d2_.z() = unknowns(5) ;
    d3_.x() = unknowns(6) ;
    d3_.y() = unknowns(7) ;
    d3_.z() = unknowns(8) ;
}

Eigen::VectorXd BasisVectors::constraints( void )
{
    constraints_(0) = d1_ * d1_ - 1. ;
    constraints_(1) = d1_ * d2_ ;
    constraints_(2) = d1_ * d3_ ;
    constraints_(3) = d2_ * d2_ - 1. ;
    constraints_(4) = d2_ * d3_ ;
    constraints_(5) = d3_ * d3_ - 1. ;
    return constraints_ ;
}

void BasisVectors::reset()
{
    d1_.x() = 1. ;
    d1_.y() = 0. ;
    d1_.z() = 0. ;
    d2_.x() = 0. ;
    d2_.y() = 1. ;
    d2_.z() = 0. ;
    d3_.x() = 0. ;
    d3_.y() = 0. ;
    d3_.z() = 1. ;
}

void BasisVectors::copy(const BasisVectors & other)
{
    d1_.x() = other.d1_.x() ;
    d1_.y() = other.d1_.y() ;
    d1_.z() = other.d1_.z() ;
    d2_.x() = other.d2_.x() ;
    d2_.y() = other.d2_.y() ;
    d2_.z() = other.d2_.z() ;
    d3_.x() = other.d3_.x() ;
    d3_.y() = other.d3_.y() ;
    d3_.z() = other.d3_.z() ;
}

RotationMatrix BasisVectors::toRotationMatrix( void ) const
{
    // Extracting coefficients from the directors.
    const double r00( d1_.x() ) ;
    const double r10( d1_.y() ) ;
    const double r20( d1_.z() ) ;

    const double r01( d2_.x() ) ;
    const double r11( d2_.y() ) ;
    const double r21( d2_.z() ) ;

    const double r02( d3_.x() ) ;
    const double r12( d3_.y() ) ;
    const double r22( d3_.z() ) ;

    RotationMatrix m(r00, r10, r20,
                     r01, r11, r21,
                     r02, r12, r22) ;
    return m ;
}

BasisVectors BasisVectors::toBasisVectors(void) const
{
    return *this ;
}

Quaternion BasisVectors::toQuaternion( void ) const
{
    return Quaternion(toRotationMatrix());
}

AxisAndAngle BasisVectors::toAxisAndAngle(void) const
{
    return AxisAndAngle(toRotationMatrix()) ;
}

MRP BasisVectors::toMRP(void) const
{
    return toQuaternion().toMRP() ;
}

HorizontalPlane BasisVectors::toHorizontalPlane(void) const
{
    return HorizontalPlane(getMatrix()) ;
}

RotationVector BasisVectors::toRotationVector(void) const 
{
    return RotationVector(getMatrix()) ;
}

BasisVectors & BasisVectors::operator=(const ABC & other)
{
    BasisVectors tmp(other.toBasisVectors()) ;
    d1_ = tmp.d1_ ;
    d2_ = tmp.d2_ ;
    d3_ = tmp.d3_ ;
    return *this ;
}

BasisVectors & BasisVectors::operator=(const BasisVectors & other)
{
    d1_ = Geometry::Vector(other.d1_) ;
    d2_ = Geometry::Vector(other.d2_) ;
    d3_ = Geometry::Vector(other.d3_) ;
    return *this ;
}

Geometry::Vector BasisVectors::d1(void) const
{
    return d1_ ;
}
Geometry::Vector BasisVectors::d2(void) const
{
    return d2_ ;
}
Geometry::Vector BasisVectors::d3(void) const
{
    return d3_ ;
}

Eigen::Matrix3d BasisVectors::getMatrix(void) const
{
    Eigen::Matrix3d mat(Eigen::Matrix3d::Zero()) ;
    mat.col(0) = d1_.toArray() ;
    mat.col(1) = d2_.toArray() ;
    mat.col(2) = d3_.toArray() ;
    return mat ;
}

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
