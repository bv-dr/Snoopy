/*!
 * \file EulerAnglesAxisConvention.hpp
 */

#pragma once
#ifndef __BV_Geometry_Rotation_EulerAnglesAxisConvention_hpp__
#define __BV_Geometry_Rotation_EulerAnglesAxisConvention_hpp__

#include <Eigen/Dense>
#include <map>
#include "Geometry/Exceptions.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

/*!
 * \struct AxisConvention
 * \brief Container of axis convention representation used to parametrize Euler Angles.
 */
enum AxisConvention
{
    X_AXIS,
    Y_AXIS,
    Z_AXIS
} ;

/*!
 * \struct OrderConvention
 * \brief Container of rotation order convention parametrize Euler Angles.
 *
 */
enum OrderConvention
{
    EXTRINSIC,
    INTRINSIC
} ;

/*!
 * \class EulerAnglesConvention
 *
 * \brief Container of rotation order convention parametrize Euler Angles.
 *
 * \tparam Rotation::AxisConvention a first axis \a U
 * \tparam Rotation::AxisConvention a second axis \a V
 * \tparam Rotation::AxisConvention a third axis \a W
 * \tparam Rotation::OrderConvention the order euler angles are applied on each axis.
 *
 * Basically for \b UVW axis convention with \b extrinsic order convention the rotation are applied:
 *     - firstly along \a U axis
 *     - secondly along \a V axis
 *     - thirdly along \a W axis
 * This results in a composed rotation matrix such that: \f$ R = R_W.R_V.R_U\f$.
 *
 * For \b UVW axis convention with \b intrinsic order convention the rotation are applied:
 *     - firstly along \a W axis
 *     - secondly along \a V axis
 *     - thirdly along \a U axis
 * This results in a composed rotation matrix such that: \f$ R = R_U.R_V.R_W\f$.
 *
 * It is fundamental to know the difference between
 * \b intrinsic and \b extrinsic convention for given \b UVW.\n
 * For \b extrinsic convention:\n
 *     The rotations are performed in the order U -> V -> W:\n
 *     U in first, V in second, W in third i.e. the matrix product \f$ R = R_W.R_V.R_U\f$ (according to Eigen)\n
 *      --> Extrinsic convention corresponds to rotation about global (or parent) axis\n\n
 *
 * For \b intrinsic convention:\n
 *     The rotations are performed in the order U <- V <- W:\n
 *     W in first, V in second, U in third i.e. the matrix product \f$ R = R_U.R_V.R_W\f$ (according to Eigen)\n
 *      --> Intrinsic convention corresponds to rotation about local new axis
 *
 * A set of three axis is a valid Euler convention if \a U != \a V and \a V != \a W.
 */
template <AxisConvention U=AxisConvention::Z_AXIS, AxisConvention V=AxisConvention::X_AXIS, AxisConvention W=AxisConvention::Y_AXIS, OrderConvention O=OrderConvention::INTRINSIC>
class EulerAnglesConvention
{
private:
    AxisConvention U_ ;
    AxisConvention V_ ;
    AxisConvention W_ ;
    OrderConvention orderConvention_ ;
    //! A map that associates axis conventions to axis of rotation.
    std::map<AxisConvention, Eigen::Vector3d> unitVectors_ ;

    /*!
     * \brief Unit vectors initializer.
     *
     * Convert enum Rotation::AxisConvention into Eigen::Vector3d for is Rotation::EulerAngles class.
     */
    void initializeUnitVectors_(void)
    {
        unitVectors_[ AxisConvention::X_AXIS ] = Eigen::Vector3d::UnitX() ;
        unitVectors_[ AxisConvention::Y_AXIS ] = Eigen::Vector3d::UnitY() ;
        unitVectors_[ AxisConvention::Z_AXIS ] = Eigen::Vector3d::UnitZ() ;
    }

    /*!
     * \brief Euler angle convention checker.
     *
     * Check if \a U != \a V and \a V != \a W.
     */
    void checkConvention_(void) const
    {
        using BV::Geometry::Exceptions::RotationInitialisationException ;
        if( U == V )
        {
            throw RotationInitialisationException("Not valid Euler Angle convention for axis U and V.") ;
        }
        if( V == W )
        {
            throw RotationInitialisationException("Not valid Euler Angle convention for axis V and W.") ;
        }
    }
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Dummy constructor
     */
    EulerAnglesConvention(void) :
        U_(U), V_(V), W_(W), orderConvention_(O)
    {
        checkConvention_() ;
        initializeUnitVectors_() ;
    }

    /*!
     * \brief \a U enum axis getter.
     *
     * \return a Rotation::AxisConvention enum type corresponding to axis \a U
     */
    const AxisConvention & getU(void) const
    {
        return U_ ;
    }

    /*!
     * \brief \a V enum axis getter.
     *
     * \return a Rotation::AxisConvention enum type corresponding to axis \a V
     */
    const AxisConvention & getV(void) const
    {
        return V_ ;
    }

    /*!
     * \brief \a W enum axis getter.
     *
     * \return a Rotation::AxisConvention enum type corresponding to axis \a W
     */
    const AxisConvention & getW(void) const
    {
        return W_ ;
    }

    /*!
     * \brief order convention getter.
     *
     * \return a Rotation::OrderConvention enum type corresponding to convention of axis UVW
     */
    const OrderConvention & getOrderConvention(void) const
    {
        return orderConvention_ ;
    }

    /*!
     * \brief Equality member method.
     *
     * This method check if other convention is the same as \c this.
     * Check is performed on \a U, \a V, \a W axis and \a order \a convention.
     *
     * \tparam OtherConventionType an EulerAngles convention
     *
     * \return True if other convention is the same as \c this, False otherwise.
     */
    template <typename OtherConventionType>
    bool isEqualTo(const OtherConventionType & other) const
    {
        return ((U_ == other.getU())
                 && (V_ == other.getV())
                 && (W_ == other.getW())
                 && (orderConvention_ == other.getOrderConvention())) ;
    }

    /*!
     * \brief Non-equality member method.
     *
     * This method check if other convention is the same as \c this.
     * Check is performed on \a U, \a V, \a W axis and \a order \a convention.
     *
     * \tparam OtherConventionType an EulerAngles convention
     *
     * \return True if other convention is different from \c this, False otherwise.
     */
    template <typename OtherConventionType>
    bool isNotEqualTo(const OtherConventionType & other) const
    {
        return !(isEqualTo<OtherConventionType>(other)) ;
    }

    /*!
     * \brief \a U unit vector getter.
     *
     * \return an Eigen::Vector3d corresponding to the unit vector of axis \a U.
     */
    Eigen::Vector3d getUUnitVector(void) const
    {
        return unitVectors_.at(U_) ;
    }

    /*!
     * \brief \a V unit vector getter.
     *
     * \return an Eigen::Vector3d corresponding to the unit vector of axis \a V.
     */
    Eigen::Vector3d getVUnitVector(void) const
    {
        return unitVectors_.at(V_) ;
    }

    /*!
     * \brief \a W unit vector getter.
     *
     * \return an Eigen::Vector3d corresponding to the unit vector of axis \a W.
     */
    Eigen::Vector3d getWUnitVector(void) const
    {
        return unitVectors_.at(W_) ;
    }
} ;

typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Y_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XYX_i ;
typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Z_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XZX_i ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::X_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YXY_i ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YZY_i ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::X_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZXZ_i ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZYZ_i ;
typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XYZ_i ;
typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XZY_i ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::X_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YXZ_i ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YZX_i ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::X_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZXY_i ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZYX_i ;


typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Y_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XYX_e ;
typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Z_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XZX_e ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::X_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YXY_e ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YZY_e ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::X_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZXZ_e ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZYZ_e ;
typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XYZ_e ;
typedef EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XZY_e ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::X_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YXZ_e ;
typedef EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YZX_e ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::X_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZXY_e ;
typedef EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZYX_e ;


namespace Details {

/*!
 * \brief A dummy struct that contains nothing
 * \tparam U Can be any Rotation::AxisConvention enum
 * \tparam V Can be any Rotation::AxisConvention enum
 * \tparam W Can be any Rotation::AxisConvention enum
 * \tparam O Can be any Rotation::OrderConvention enum
 *
 * The aim of this class is to prevent SFINAE to be successful
 * by calling a non-existent member of this empty class.
 */
template < AxisConvention U, 
           AxisConvention V,
           AxisConvention W,
           OrderConvention O>
struct Disabler
{
    // Left blank
} ;

} // end of namespace Details

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_EulerAnglesAxisConvention_hpp__
