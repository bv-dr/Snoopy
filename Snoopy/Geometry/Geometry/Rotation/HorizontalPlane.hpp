#pragma once
#ifndef __BV_Geometry_Rotation_HorizontalPlane_hpp__
#define __BV_Geometry_Rotation_HorizontalPlane_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Geometry>
#include <vector>
#include <complex>
#include <cmath>

#include "Geometry/GeometryTypedefs.hpp"

#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

/*!
 * \brief Implementation of a 2D horizontal rotation class based on complex
 *        numbers.
 */
class GEOMETRY_API HorizontalPlane: public Details::RotatorABC<HorizontalPlane>
{
private:

    //! The angle of the rotation in radians
    double angle_ ;

    void checkMatrix_(const Eigen::Matrix3d & R) ;
    double getAngleFromR_(const Eigen::Matrix3d & R) ;
public:

    /*!
     * \brief Constructs a HorizontalPlane instance from matrix.
     * \param[in] mat the Eigen::Matrix3d
     */
    // HorizontalPlane( const Eigen::Matrix3d & mat ):
    // HorizontalPlane(const Eigen::Ref<const Eigen::Matrix3d > & mat) :
    template<typename Derived>
    HorizontalPlane(const Eigen::MatrixBase<Derived> & mat,
                    typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC<HorizontalPlane>(1, 0)
    {
        checkMatrix_(mat) ;
        Details::CheckRotationMatrix(mat) ;
        angle_ = Details::AngleMinusPi_Pi( getAngleFromR_(mat) ) ;
    }

    /*!
     * \brief Dummy constructor
     */
    HorizontalPlane( void ):
        Details::RotatorABC<HorizontalPlane>(1, 0), angle_(0.)
    {
    }

    /*!
     * \brief Constructs a HorizontalPlane instance from values.
     * \param[in] angle The angle of the rotation in radians
     */
    HorizontalPlane( const double & angle ):
        Details::RotatorABC<HorizontalPlane>(1, 0)
    {
        angle_ = Details::AngleMinusPi_Pi(angle) ;
    }

    // HorizontalPlane( const Eigen::Ref<const Eigen::VectorXd > & unknowns ):
    HorizontalPlane( const Eigen::VectorXd & unknowns ):
        Details::RotatorABC<HorizontalPlane>(1, 0)
    {
        assert(unknowns.size() == nUnknowns_) ;
        angle_ = Details::AngleMinusPi_Pi( unknowns(0) ) ;
    }

    HorizontalPlane(const HorizontalPlane & other) :
        HorizontalPlane(other.angle_)
    {
        // Left blank
    }

    HorizontalPlane(const ABC & other) :
        HorizontalPlane(other.toHorizontalPlane())
    {
    }

    /*!
     * \brief Constructs a HorizontalPlane instance from unknowns.
     * \param[in] unknowns A vector of length one that contains the angle.
     */
    // HorizontalPlane( const Eigen::VectorXd & unknowns ):
    //     Details::RotatorABC<HorizontalPlane>(1, 0), angle_( unknowns( 0 ) )
    // {
    //     assert( unknowns.size() == nUnknowns() ) ;
    // }

    double angle(void) const ;
    double & angle(void) ;

    Eigen::Matrix3d getMatrix(void) const ;
    Geometry::Vector d1(void) const ;
    Geometry::Vector d2(void) const ;
    Geometry::Vector d3(void) const ;

    HorizontalPlane & operator=(const HorizontalPlane & other) ;

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<HorizontalPlane>::rotate ;

    void reset() ;

    void copy(const HorizontalPlane & other) ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;
    Eigen::VectorXd unknowns(void) const ;
    void unknowns(const Eigen::VectorXd & unknowns) ;
    Eigen::VectorXd constraints(void) ;
    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;
    Quaternion toQuaternion( void ) const ;
    AxisAndAngle toAxisAndAngle(void) const ;
    MRP toMRP(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    RotationVector toRotationVector(void) const ;

    //// Overload from RotatorABC<T>
    HorizontalPlane & operator=(const ABC & other) ;

};

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::HorizontalPlane>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::HORIZONTAL_PLANE ;
} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_HorizontalPlane_hpp__
