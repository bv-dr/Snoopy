#pragma once
#ifndef __BV_Geometry_Rotation_ModifiedRodriguesParameters_hpp__
#define __BV_Geometry_Rotation_ModifiedRodriguesParameters_hpp__

#include "GeometryExport.hpp"

#include <algorithm>

#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

class GEOMETRY_API MRP : public Details::RotatorABC<MRP>
{
private:
    Eigen::Vector3d sigma_ ;

    /*!
     * \brief Switch to shadow representation.
     *
     * If \f$|\sigma|>1\f$, then modify \a sigma_ values to shadow representation
     * see MRP::shadowRepr(void) for more details.
     *
     */
    void setInUnitCircle_(void) ;

    /*!
     * \brief Implementation of rotation multiplication with MRP foramlism.
     *
     * Compose rotations \f$\sigma_L\f$ and \f$\sigma_R\f$ with
     * MRP composition formula to compute \f$sigma_C\f$ such that:
     * \f[R(\sigma_C) = R(\sigma_L) R(\sigma_R) \f]
     *
     * Hence:
     * \f[\sigma_C = \frac{(1-|\sigma_R|^2)\sigma_L+(1-|\sigma_L|^2)\sigma_R-2\sigma_L\times\sigma_R}{1+|\sigma_L|^2|\sigma_R|^2+2\sigma_L\cdot\sigma_R} \f]
     *
     * The singularity for \f$ 1+|\sigma_L|^2|\sigma_R|^2+2\sigma_L\cdot\sigma_R = 0 \f$ is handled such that:
     *
     * If: \f$|\sigma_L|>|\sigma_R|\f$
     *     \f[\sigma_L = {\sigma_L}^S\f]
     *
     * Else:
     *     \f[\sigma_R = {\sigma_R}^S\f]
     *
     * Where upper index \f${\cdot}^S\f$ stands for the shadow representation.
     *
     */
    Eigen::Vector3d sigmaMult_(Eigen::Vector3d sigmaLeft,
                               Eigen::Vector3d sigmaRight) ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs from a rotation matrix.
     *
     * \param[in] mat an Eigen::Matrix3d matrix
     */
    template<typename Derived>
    MRP(const Eigen::MatrixBase<Derived> & mat,
        typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC< MRP >(3, 0)
    {
        Quaternion q(mat) ;
        // already checked in quaternion conversion
        // Details::CheckRotationMatrix(mat) ;
        sigma_ = q.toMRP().unknowns() ;
        setInUnitCircle_() ;
    }

    /*!
     * \brief Dummy constructor
     *
     * The MRP is initialized to vector (0,0,0).
     */
    MRP(void) :
        Details::RotatorABC<MRP>(3, 0),
        sigma_(0., 0., 0.)
    {
    }

    /*!
     * \brief Constructors from MRP components.
     *
     * Note that the MRP is set in unit circle.
     *
     */
    MRP(const double & s1, const double & s2, const double & s3) :
        Details::RotatorABC<MRP>(3, 0),
        sigma_(s1, s2, s3)
    {
        setInUnitCircle_() ;
    }

    /*!
     * \brief Constructs from a Eigen::VectorXd
     *
     */
    MRP( const Eigen::VectorXd & unknowns ):
        Details::RotatorABC<MRP>(3, 0)
    {
        assert(unknowns.size() == nUnknowns_) ;
        sigma_(0) = unknowns(0) ;
        sigma_(1) = unknowns(1) ;
        sigma_(2) = unknowns(2) ;
        setInUnitCircle_() ;
    }

    /*!
     * \brief Constructs a copy of provided MRP.
     *
     * \param[in] other The other MRP object to copy.
     */
    MRP(const MRP & other) :
        Details::RotatorABC<MRP>(3, 0),
        sigma_(other.sigma_)
    {
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in an MRP.
     *
     * \param[in] other The other Rotator object.
     *
     */
    MRP(const ABC & other) :
        Details::RotatorABC<MRP>(3, 0),
        sigma_(other.toMRP().sigma_)
    {
    }

    /*!
     * \brief \a sigma1 getter (by copy).
     *
     * \return a double: copied \a sigma1
     *
     */
    double sigma1(void) const ;

    /*!
     * \brief \a sigma2 getter (by copy).
     *
     * \return a double: copied \a sigma2
     *
     */
    double sigma2(void) const ;

    /*!
     * \brief \a sigma3 getter (by copy).
     *
     * \return a double: copied \a sigma3
     *
     */
    double sigma3(void) const ;

    /*!
     * \brief \a sigma1 getter (by reference).
     *
     * \return a double: a reference to \a sigma1
     *
     */
    double & sigma1(void) ;

    /*!
     * \brief \a sigma2 getter (by reference).
     *
     * \return a double: a reference to \a sigma2
     *
     */
    double & sigma2(void) ;

    /*!
     * \brief \a sigma3 getter (by reference).
     *
     * \return a double: a reference to \a sigma3
     *
     */
    double & sigma3(void) ;

    /*!
     * \brief Compute the norm of MRP vector.
     */
    double norm(void) const ;

    /*!
     * \brief Convert the MRP representation into its shadow representation.
     *
     * The shadow representation yields:
     * \f[ \sigma^S = \frac{-\sigma}{|\sigma|^2} \f]
     */
    Eigen::Vector3d shadowRepr(void) const ;

    /*!
     * \brief Assignment operator.
     *
     * Set \a sigma value of other into \c this.
     */
    MRP & operator=(const MRP & other) ;

    /*!
    * \brief Give the equivalent rotation matrix.
    *
    * The cosine matrix is computed such that:
    * \f[ R(\sigma) = I_3 + \frac{1}{(1+\sigma^2)^2} \left( 8 \textbf{$\tilde{\sigma}$}^2 - 4(1-\sigma^2) \textbf{$\tilde{\sigma}$} \right) \f]
    *
    *
    * Where \f$ \textbf{$\tilde{\sigma}$} \f$ stands for the anti-symmetric SO3 matrix generated by vector \f$\sigma = (\sigma_1, \sigma_2, \sigma_3) \f$ such that:
    * \f[ \textbf{$\tilde{\sigma}$} = \left(\begin{array}{ccc}0&-\sigma_3&\sigma_2\\\sigma_3&0&-\sigma_1\\-\sigma_2&\sigma_1&0\end{array}\right) \f]
    * \return The Eigen::Matrix3d Rotation Matrix.
    */
    Eigen::Matrix3d getMatrix(void) const ;

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<MRP>::rotate ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;
    /*!
    * \brief Returns a vector containing all the MRP coordinates (sigma1, sigma2, sigma3).
    * \return The vector with the four MRP coordinates (sigma1, sigma2, sigma3)
    */
    Eigen::VectorXd unknowns(void) const ;
    /*!
    * \brief Defines the MRP coordinates from a vector.
    * \param[in] unknowns The vector containing the MRP coordinates (sigma1, sigma2, sigma3).
    */
    void unknowns(const Eigen::VectorXd & unknowns) ;
    /*!
    * \brief Returns a contraints vector of size 0.
    * \returns The constraints vector of size 0 (no constraint).
    */
    Eigen::VectorXd constraints(void) ;

    void reset() ;

    void copy(const MRP & other) ;

    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;
    Quaternion toQuaternion( void ) const ;
    AxisAndAngle toAxisAndAngle(void) const ;
    MRP toMRP(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    RotationVector toRotationVector(void) const ;


    //// Overload from RotatorABC<T>
    MRP & operator=(const ABC & other) ;

} ;

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::MRP>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS ;
} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_ModifiedRodriguesParameters_hpp__
