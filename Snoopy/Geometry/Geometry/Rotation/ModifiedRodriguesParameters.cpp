#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

#include "Geometry/sx3Manipulation.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

void MRP::setInUnitCircle_(void)
{
    if (norm() > 1.)
    {
        sigma_ = shadowRepr() ;
    }
}

Eigen::Vector3d MRP::sigmaMult_(Eigen::Vector3d sigmaLeft,
                                Eigen::Vector3d sigmaRight)
{
    Eigen::Vector3d sigmaLeftTmp(sigmaLeft) ;
    Eigen::Vector3d sigmaRightTmp(sigmaRight) ;

    double ss(sigmaLeftTmp.dot(sigmaRightTmp)) ;
    if (std::abs(ss - 1.) < 0.5)
    {
        // In case of final rotation angle is 360°: singularity
        // We chose to rotate according to the shadow MRP
        if(sigmaLeftTmp.norm()>sigmaRightTmp.norm())
        {
            sigmaLeftTmp *= -1. / sigmaLeftTmp.squaredNorm() ;
        }
        else
        {
            sigmaRightTmp *= -1. / sigmaRightTmp.squaredNorm() ;
        }
    }

    double left_n2(sigmaLeftTmp.squaredNorm()) ;
    double right_n2(sigmaRightTmp.squaredNorm()) ;
    double scalarProd(sigmaLeftTmp.dot(sigmaRightTmp)) ;

    Eigen::Matrix3d rightTilde(R3Toso3(sigmaRightTmp)) ;

    double denom(1.+left_n2*right_n2-2.*scalarProd) ;

    Eigen::Vector3d num((1.-right_n2)*sigmaLeftTmp + (1.-left_n2)*sigmaRightTmp -  2.*rightTilde * sigmaLeftTmp) ;
    Eigen::Vector3d sigma_Mult = num /denom ;

    return sigma_Mult ;
}

double MRP::sigma1(void) const
{
    return sigma_.coeff(0) ;
}
double MRP::sigma2(void) const
{
    return sigma_.coeff(1) ;
}
double MRP::sigma3(void) const
{
    return sigma_.coeff(2) ;
}

double & MRP::sigma1(void)
{
    return sigma_.coeffRef(0) ;
}
double & MRP::sigma2(void)
{
    return sigma_.coeffRef(1) ;
}
double & MRP::sigma3(void)
{
    return sigma_.coeffRef(2) ;
}

double MRP::norm(void) const
{
    return sigma_.norm() ;
}

Eigen::Vector3d MRP::shadowRepr(void) const
{
    return -sigma_ / std::pow(norm(), 2) ;
}

MRP & MRP::operator=(const MRP & other)
{
    sigma_ = other.sigma_ ;
    return *this ;
}

Eigen::Matrix3d MRP::getMatrix(void) const
{
    using BV::Geometry::R3Toso3 ;
    Eigen::Matrix3d sigmaTilde(R3Toso3(sigma_)) ;
    Eigen::Matrix3d sigmaTilde2(sigmaTilde*sigmaTilde) ;
    Eigen::Matrix3d I(Eigen::Matrix3d::Identity()) ;
    double n2(std::pow(sigma_.norm(), 2)) ;
    double den(std::pow(1. + n2, 2)) ;
    Eigen::Matrix3d num(8.*sigmaTilde2 - 4.*(1-n2)*sigmaTilde) ;
    Eigen::Matrix3d cosineMatrix(I + num / den) ;
    return cosineMatrix.transpose() ;
}

void MRP::addOtherRotationAtLeft(const ABC & abc)
{
    Eigen::Vector3d leftSigma(abc.toMRP().sigma_) ;
    Eigen::Vector3d rightSigma(sigma_) ;
    sigma_ = sigmaMult_(leftSigma, rightSigma) ;
    setInUnitCircle_() ;
}

void MRP::addOtherRotationAtRight(const ABC & abc)
{
    Eigen::Vector3d leftSigma(sigma_) ;
    Eigen::Vector3d rightSigma(abc.toMRP().sigma_) ;
    sigma_ = sigmaMult_(leftSigma, rightSigma) ;
    setInUnitCircle_() ;
}

void MRP::subtractOtherRotationAtLeft(const ABC & abc)
{
    Eigen::Vector3d leftSigma( - abc.toMRP().sigma_) ;
    Eigen::Vector3d rightSigma(sigma_) ;
    sigma_ = sigmaMult_(leftSigma, rightSigma) ;
    setInUnitCircle_() ;
}

void MRP::subtractOtherRotationAtRight(const ABC & abc)
{
    Eigen::Vector3d leftSigma(sigma_) ;
    Eigen::Vector3d rightSigma( - abc.toMRP().sigma_) ;
    sigma_ = sigmaMult_(leftSigma, rightSigma) ;
    setInUnitCircle_() ;
}

Eigen::Vector3d MRP::rotate(const Eigen::Vector3d & vect) const
{
    return getMatrix() * vect ;
}

void MRP::inverse(void)
{
    sigma_(0) = -sigma_(0) ;
    sigma_(1) = -sigma_(1) ;
    sigma_(2) = -sigma_(2) ;
}

Eigen::VectorXd MRP::unknowns(void) const
{
    unknowns_(0) = sigma_(0) ;
    unknowns_(1) = sigma_(1) ;
    unknowns_(2) = sigma_(2) ;
    return unknowns_ ;
}

void MRP::unknowns(const Eigen::VectorXd & unknowns)
{
    assert( unknowns.size() == nUnknowns() ) ;
    sigma_ = unknowns ;
    // sigma_(0) = unknowns_(0) ;
    // sigma_(1) = unknowns_(1) ;
    // sigma_(2) = unknowns_(2) ;
}

Eigen::VectorXd MRP::constraints(void)
{
    return constraints_ ;
}

void MRP::reset(void)
{
    sigma_.setZero() ;
}

void MRP::copy(const MRP & other)
{
    sigma_ = other.sigma_ ;
}

RotationMatrix MRP::toRotationMatrix(void) const
{
    return RotationMatrix(getMatrix()) ;
}
BasisVectors MRP::toBasisVectors(void) const
{
    return BasisVectors(getMatrix()) ;
}

Quaternion MRP::toQuaternion( void ) const
{
    double sqNorm(sigma_.squaredNorm()) ;
    double denom(1.) ;
    denom /= 1.+sqNorm ;
    double w( (1.-sqNorm)*denom ) ;
    denom *= 2. ;
    double x( sigma_(0)*denom ) ;
    double y( sigma_(1)*denom ) ;
    double z( sigma_(2)*denom ) ;
    return Quaternion(w, x, y, z) ;
}

AxisAndAngle MRP::toAxisAndAngle(void) const
{
    return AxisAndAngle(toQuaternion().toAxisAndAngle()) ;
}

MRP MRP::toMRP(void) const
{
    return *this ;
}

HorizontalPlane MRP::toHorizontalPlane(void) const
{
    return HorizontalPlane(getMatrix()) ;
}

RotationVector MRP::toRotationVector(void) const
{
    return RotationVector(getMatrix()) ;
}

MRP & MRP::operator=(const ABC & other)
{
    sigma_ = other.toMRP().sigma_ ;
    return *this ;
}

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
