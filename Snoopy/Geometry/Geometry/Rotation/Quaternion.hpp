#pragma once

#include "GeometryExport.hpp"

#include <Eigen/Geometry>
#include <complex>
#include <cmath>
#include <iostream>

#include "Geometry/GeometryTypedefs.hpp"

#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Vector.hpp"

#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

namespace Details {

// FIXME I can't use the Align stuff...see if it is worth it !
typedef Eigen::Quaternion<double, Eigen::DontAlign> EigenQuaternion ;
//typedef Eigen::Quaternion<double> EigenQuaternion ;

} // end of namespace Details

/*!
 * \brief Implementation of the Quaternion class
 *        to orientate a body in 3D space.
 */
class GEOMETRY_API Quaternion : public Details::RotatorABC<Quaternion>
{
private:
    Details::EigenQuaternion innerData_ ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs from a rotation matrix
     *
     * A non optimized algorithm is the following:
     *\code{.cpp}
     * Eigen::Vector4d res(Eigen::Vector4d::Zero()) ;
     * if( R.trace() > 0. )
     * {
     *     double t( sqrt( 1. + R.trace() ) );
     *     res(0) = t ;
     *     res(1) = (R(2,1) - R(1,2)) / t ;
     *     res(2) = (R(0,2) - R(2,0)) / t ;
     *     res(3) = (R(1,0) - R(0,1)) / t ;
     * }
     * else if( (R(0,0)>R(1,1)) && (R(0,0)>R(2,2)) )
     * {
     *     double t( sqrt( 1. + R(0,0) - R(1,1) - R(2,2) ) );
     *     res(0) = (R(2,1) - R(1,2)) / t ;
     *     res(1) = t ;
     *     res(2) = (R(0,1) + R(1,0)) / t ;
     *     res(3) = (R(0,2) + R(2,0)) / t ;
     * }
     * else if( R(1,1)>R(2,2) )
     * {
     *     double t( sqrt( 1. - R(0,0) + R(1,1) - R(2,2) ) );
     *     res(0) = (R(0,2) - R(2,0)) / t ;
     *     res(1) = (R(0,1) + R(1,0)) / t ;
     *     res(2) = t ;
     *     res(3) = (R(1,2) + R(2,1)) / t ;
     * }
     * else
     * {
     *     double t( sqrt( 1. - R(0,0) - R(1,1) + R(2,2) ) );
     *     res(0) = (R(1,0) - R(0,1)) / t ;
     *     res(1) = (R(0,2) + R(2,0)) / t ;
     *     res(2) = (R(1,2) + R(2,1)) / t ;
     *     res(3) = t ;
     * }
     * res *= 0.5 ;
     * return res ;
     * \endcode
     */
    // Quaternion(const Eigen::Matrix3d & mat) :
    // Quaternion(const Eigen::Ref<const Eigen::Matrix3d > & mat) :
    template<typename Derived>
    Quaternion(const Eigen::MatrixBase<Derived> & mat,
               typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC< Quaternion >(4, 1),
        innerData_(mat)
    {
        Details::CheckRotationMatrix(mat) ;
    }

    /*!
     * \brief Constructs from values
     * \param[in] w The w coordinate
     * \param[in] x The x coordinate
     * \param[in] y The y coordinate
     * \param[in] z The z coordinate
     *
     * Note that the quaternion is \b supposed normalized.
     */
    Quaternion( const double & w, const double & x,
                const double & y, const double & z ) :
        Details::RotatorABC< Quaternion >(4, 1),
        innerData_(w, x, y, z)
    {
        // Left blank
    }

    /*!
     * \brief Constructs a Quaternion instance from unknowns.
     * \param[in] unknowns A vector of length four
     *                     that contains the 4 Quaternion coordinates.
     *
     * Note that the quaternion is \b supposed normalized.
     */
    // Quaternion(const Eigen::Ref<const Eigen::VectorXd > & unknowns) :
    Quaternion(const Eigen::VectorXd & unknowns) :
        Details::RotatorABC< Quaternion >(4, 1),
        innerData_(unknowns(0), unknowns(1),
                   unknowns(2), unknowns(3))
    {
        assert( unknowns.size() == nUnknowns() ) ;
    }

    /*!
     * \brief Constructs a default Quaternion with coordinates \f$q = (1,0,0,0)\f$.
     */
    Quaternion(void) :
        Details::RotatorABC< Quaternion >(4, 1),
        innerData_(1., 0., 0., 0.)
    {
        // Left blank
    }

    /*!
     * \brief Constructs a copy of provided Quaternion.
     * \param[in] other The other Quaternion object.
     */
    Quaternion(const Quaternion & other) :
        Details::RotatorABC< Quaternion >(4, 1),
        innerData_(other.innerData_)
    {
        // Left blank
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in a quaternion.
     * \param[in] other The other Rotator object.
     */
    Quaternion(const ABC & other) :
        Details::RotatorABC<Quaternion>(4, 1),
        innerData_(other.toQuaternion().innerData_)
    {
        // Left blank
    }

    /*!
     * \brief Constructs a rotator from two arbitrary vectors.
     * \param[in] v1 an Eigen::Vector3d representing the first vector
     * \param[in] v2 an Eigen::Vector3d representing the second vector
     *
     * The rotation ( \f$R\f$ ) is constructed such that it represents
     * a rotation between \f$v_1\f$ and \f$v_2\f$. This means that \f$v_1\f$ is sent to \f$v_2\f$ such that:
     * \f[v_2 = R.v_1\f]
     *
     * Note that the two input vectors do \b not have to be normalized, and
     * do not need to have the same norm.
     *
     * Here is an example of constructor implementation (with \f$v_1\f$ and \f$v_2\f$ as Eigen::Vector3d):
     * \code{.cpp}
     *   Eigen::Vector3d a(v1.normalized()) ;
     *   Eigen::Vector3d b(v2.normalized()) ;
     *   double c(a.dot(b)) ;
     *   // the vectors are nearly along same direction... we must find a way to compute
     *   // efficiently the rotation
     *   if( c < -1.+1.e-12 )
     *   {
     *       throw "Unable to compute quaternion from two opposite vectors..."
     *   }
     *   else
     *   {
     *       double k(1. + c) ;
     *       double s(1. / sqrt(k+k) ) ;
     *       innerData_.w() = k*s ;
     *       innerData_.vec() = s*a.cross(b) ;
     *   }
     * \endcode
     */
    Quaternion(const Eigen::Vector3d & v1, const Eigen::Vector3d & v2):
        Details::RotatorABC<Quaternion>(4, 1)
    {
        // FIXME: use of Eigen or not?
        innerData_.setFromTwoVectors(v1, v2) ;
    }
    /*!
     * \copydoc BV::Geometry::Rotation::Quaternion::Quaternion(const Eigen::Vector3d &, const Eigen::Vector3d &)
     */
    Quaternion(const Vector & v1, const Vector & v2) :
        Quaternion(v1.toArray(), v2.toArray())
    {
    }

    /*!
     * \brief Get w quaternion component (by reference).
     */
    double & w(void) { return innerData_.w() ; }
    /*!
     * \brief Get w quaternion component (by copy).
     */
    double w(void) const { return innerData_.w() ; }
    /*!
     * \brief Get x quaternion component (by reference).
     */
    double & x(void) { return innerData_.x() ; }
    /*!
     * \brief Get x quaternion component (by copy).
     */
    double x(void) const { return innerData_.x() ; }
    /*!
     * \brief Get y quaternion component (by reference).
     */
    double & y(void) { return innerData_.y() ; }
    /*!
     * \brief Get y quaternion component (by copy).
     */
    double y(void) const { return innerData_.y() ; }
    /*!
     * \brief Get z quaternion component (by reference).
     */
    double & z(void) { return innerData_.z() ; }
    /*!
     * \brief Get z quaternion component (by copy).
     */
    double z(void) const { return innerData_.z() ; }

    /*!
     * \brief Normalising the quaternion to 1, with an english writing.
     */
    void normalise( void ) ;

    /*!
     * \brief Get the quaternion norm.
     *
     * \return \f$ |q|= \sqrt{w^2+x^2+y^2+z^2}\f$
     */
    double norm(void) const ;

    /*!
     * \brief Return a copy of the normalized quaternion.
     *
     * \return a copy of the normalized quaterion.
     */
    Quaternion normalised(void) const ;

    /*!
     * \brief Interpolates between two quaternions.
     *
     * Uses SLERP interpolation to compute an interpolated quaternion \f$q_2\f$
     * from current quaternion \f$q\f$ and given quaternion in signature \f$q_1\f$.
     *
     * This represents an interpolation for a constrant motion between \f$q\f$ and \f$q_2\f$.
     * see also http://en.wikipedia.org/wiki/Slerp.
     *
     * \param[in] an interpolator factor \a t in [0;1]
     * \param[in] \a q2 the other quaternion
     *
     * \return the interpolated quaterion.
     */
    Quaternion slerp(const double & t, const Quaternion & q2) const ;

    /*!
     * \copydoc BV::Geometry::Rotation::Details::RotatorABC::operator=(const BV::Geometry::Rotation::ABC &)
     */
    Quaternion & operator=(const Quaternion & other) ;

    /*!
    * \brief Give the equivalent rotation matrix.
    * \return The Eigen::Matrix3d Rotation Matrix.
    */
    Eigen::Matrix3d getMatrix(void) const ;

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<Quaternion>::rotate ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtLeft(const Quaternion & quat) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void addOtherRotationAtRight(const Quaternion & quat) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const Quaternion & quat) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtRight(const Quaternion & quat) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;
    /*!
    * \brief Returns a vector containing all the quaternion coordinates (w,x,y,z).
    * \return The vector with the four quaternion coordinates  (w,x,y,z)
    */
    Eigen::VectorXd unknowns(void) const ;
    /*!
    * \brief Defines the quaternion coordinates from a vector.
    * \param[in] unknowns The vector containing the quaternion coordinates  (w,x,y,z).
    */
    void unknowns(const Eigen::VectorXd & unknowns) ;
    /*!
    * \brief Returns a contraints vector of size 1.
    * \returns The constraints vector of size 1: q.norm()-1..
    */
    Eigen::VectorXd constraints(void) ;

    void reset() ;

    void copy(const Quaternion & other) ;

    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;
    Quaternion toQuaternion( void ) const ;
    /*! \brief Converter from Quaternion to AxisAndAngle.
     *
     * The Quaternion is parametrized by the normalized vector: \f$ q = (w, x, y, z) \f$
     *
     * An AxisAndAngle  is parametrized by an normalised axis \f$\boldsymbol{e}\f$ and an angle \f$\theta\f$.
     * The relation between the two is:
     * \f[w = cos(\theta/2) \f]
     * \f[x = \boldsymbol{e}(0) * sin(\theta/2) \f]
     * \f[y = \boldsymbol{e}(1) * sin(\theta/2) \f]
     * \f[z = \boldsymbol{e}(2) * sin(\theta/2) \f]
     *
     * Then:
     * \code{.cpp}
     * theta = 2.*acos(q.w)
     * if((q.x, q.y, q.z).norm())>epsilon:  // if the vector part is non zero
     *     e = (q.x, q.y, q.z).normalised()
     * else: // whatever axis is valid (because theta = 0. and the rotation is null), we choose (1., 0., 0.)
     *     e = (1., 0., 0.)
     * \return AxisAndAngle(e, theta)
     * \endcode
     *
     */
    AxisAndAngle toAxisAndAngle(void) const ;
    MRP toMRP(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    RotationVector toRotationVector(void) const ;

    //// Overload from RotatorABC<T>
    Quaternion & operator=(const ABC & other) ;

} ;

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::Quaternion>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::QUATERNION ;
} ;

} // end of namespace Geometry
} // end of namespace BV

