/*!
 * \file AxisAndAngle.cpp
 */

#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

//Geometry::Vector AxisAndAngle::d1( void ) const
//{
//    // Here, I use the Rodrigues' rotation formula
//    // for the specific case of the directors.
//
//    // In the global frame, the initial d1 is  ( 1., 0., 0.)
//    const double L( axis_ * axis_ ) ; 
//
//    // u, v, w = axisVec.asTuple()
//    const double u( axis_.x() ) ;
//    const double v( axis_.y() ) ;
//    const double w( axis_.z() ) ;
//
//    // x, y, z = xyz.asTuple()
//    const double x( 1. ) ;
//    // Some small optimisations. 
//    const double cosAngle = cos( angle_ ) ;
//    const double sinAngleSqrtL = sin( angle_ ) * sqrt( L ) ;
//    // The computation
//    const double resX = x * ( u * u * ( 1. - cosAngle ) + L * cosAngle ) / L ;
//    const double resY = x * ( v * u * ( 1. - cosAngle ) + w * sinAngleSqrtL ) / L ;
//    const double resZ = x * ( w * u * ( 1. - cosAngle ) - v * sinAngleSqrtL ) / L ;
//
//    Geometry::Vector result( resX, resY, resZ ) ;
//    // Result should have norm 1, but I want to be sure
//    result.normalise() ;
//    return result ;
//}
//
//Geometry::Vector AxisAndAngle::d2( void ) const
//{
//    // Here, I use the Rodrigues' rotation formula
//    // for the specific case of the directors.
//
//    // In the global frame, the initial d1 is  ( 1., 0., 0.)
//    const double L( axis_ * axis_ ) ; 
//
//    // u, v, w = axisVec.asTuple()
//    const double u( axis_.x() ) ;
//    const double v( axis_.y() ) ;
//    const double w( axis_.z() ) ;
//
//    // x, y, z = xyz.asTuple()
//    const double y( 1. ) ;
//    // Some small optimisations. 
//    const double cosAngle = cos( angle_ ) ;
//    const double sinAngleSqrtL = sin( angle_ ) * sqrt( L ) ;
//    // The computation
//    const double resX = y * ( u * v * ( 1. - cosAngle ) - sinAngleSqrtL * w ) / L ;
//    const double resY = y * ( v * v * ( 1. - cosAngle ) + L * cosAngle ) / L ;
//    const double resZ = y * ( w * v * ( 1. - cosAngle ) + sinAngleSqrtL * u ) / L ;
//
//    Geometry::Vector result( resX, resY, resZ ) ;
//    // Result should have norm 1, but I want to be sure
//    result.normalise() ;
//    return result ;
//}

// TODO Factorise code bewteen d1 & d2.

void AxisAndAngle::addOtherRotationAtLeft(const ABC & abc)
{
    innerData_ = abc.toAxisAndAngle().innerData_ * innerData_ ;
}

void AxisAndAngle::addOtherRotationAtRight(const ABC & abc)
{
    innerData_ = innerData_ * abc.toAxisAndAngle().innerData_ ;
}

void AxisAndAngle::subtractOtherRotationAtLeft(const ABC & abc)
{
    innerData_ = abc.toAxisAndAngle().innerData_.inverse() * innerData_ ;
}

void AxisAndAngle::subtractOtherRotationAtRight(const ABC & abc)
{
    innerData_ = innerData_ * abc.toAxisAndAngle().innerData_.inverse() ;
}

Eigen::Vector3d AxisAndAngle::rotate(const Eigen::Vector3d & vect) const
{
    return innerData_ * vect ;
}

void AxisAndAngle::inverse(void)
{
    innerData_ = innerData_.inverse() ;
}

Eigen::VectorXd AxisAndAngle::unknowns( void ) const
{
    const Eigen::Vector3d ax(innerData_.axis()) ;
    unknowns_(0) = ax(0) ;
    unknowns_(1) = ax(1) ;
    unknowns_(2) = ax(2) ;
    unknowns_(3) = angle() ;
    return unknowns_ ;
}

void AxisAndAngle::unknowns( const Eigen::VectorXd & unknowns )
{
    assert(unknowns.size() == nUnknowns()) ;
    Eigen::Vector3d & axis(innerData_.axis()) ;
    axis(0) = unknowns(0) ;
    axis(1) = unknowns(1) ;
    axis(2) = unknowns(2) ;
    angle() = unknowns(3) ;
}

Eigen::VectorXd AxisAndAngle::constraints( void )
{
    constraints_(0) = axis().norm() - 1. ;
    return constraints_ ;
}

void AxisAndAngle::reset()
{
    innerData_.angle() = 0. ;
    innerData_.axis() = Eigen::Vector3d(1., 0., 0.) ;
}

void AxisAndAngle::copy(const AxisAndAngle & other)
{
    innerData_.angle() = other.innerData_.angle() ;
    innerData_.axis() = other.innerData_.axis() ;
}

RotationMatrix AxisAndAngle::toRotationMatrix(void) const
{
    return RotationMatrix(innerData_.toRotationMatrix()) ;
}

BasisVectors AxisAndAngle::toBasisVectors(void) const
{
    return BasisVectors(d1(), d2(), d3()) ;
}

Quaternion AxisAndAngle::toQuaternion( void ) const
{
    Details::EigenQuaternion q(innerData_) ;
    return Quaternion(q.w(), q.x(), q.y(), q.z()) ;
}

AxisAndAngle AxisAndAngle::toAxisAndAngle(void) const
{
    return *this ;
}

AxisAndAngle & AxisAndAngle::operator=(const ABC & other)
{
    innerData_ = other.toAxisAndAngle().innerData_ ;
    return *this ;
}

AxisAndAngle & AxisAndAngle::operator=(const AxisAndAngle & other)
{
    innerData_ = other.toAxisAndAngle().innerData_ ;
    return *this ;
}

MRP AxisAndAngle::toMRP(void) const
{
    double principalAngle(Details::AngleMinusPi_Pi(angle())) ;
    Eigen::Vector3d sigma(axis() * std::tan(principalAngle / 4.)) ;
    return MRP(sigma(0), sigma(1), sigma(2)) ;
}

HorizontalPlane AxisAndAngle::toHorizontalPlane(void) const
{
    return HorizontalPlane(getMatrix()) ;
}

RotationVector AxisAndAngle::toRotationVector(void) const 
{
    return RotationVector(innerData_.angle()*innerData_.axis()) ;
}


} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
