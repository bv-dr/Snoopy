#pragma once
#ifndef __BV_Geometry_Rotation_BasisVectors_hpp__
#define __BV_Geometry_Rotation_BasisVectors_hpp__

#include "GeometryExport.hpp"

#include <algorithm>

#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

/*!
 * \brief A basis may be decribed by its unit vectors.
 */
class GEOMETRY_API BasisVectors: public Details::RotatorABC<BasisVectors>
{
private:

    //! The first director
    Geometry::Vector d1_ ;
    //! The second director
    Geometry::Vector d2_ ;
    //! The third director
    Geometry::Vector d3_ ;

    /*!
     * \brief Convenient private member method to compose rotation matrices then assign to private member variables.
     *
     * This method is used in implementation of:
     *     - BasisVectors::addOtherRotationAtLeft(const Rotation::ABC &)
     *     - BasisVectors::addOtherRotationAtRight(const Rotation::ABC &)
     *     - BasisVectors::subtractOtherRotationAtLeft(const Rotation::ABC &)
     *     - BasisVectors::subtractOtherRotationAtRight(const Rotation::ABC &)
     */
    void assignDirectorsFromMatrixComposition_(Eigen::Matrix3d RLeft,
                                               Eigen::Matrix3d RRight) ;
public:
    /*!
     * \brief Constructs from a rotation matrix
     */
    template<typename Derived>
    BasisVectors(const Eigen::MatrixBase<Derived> & mat,
                 typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_(mat.col(0)), d2_(mat.col(1)), d3_(mat.col(2))
    {
        // FIXME Check orthonormality ?
        Details::CheckRotationMatrix(mat) ;
    }

    /*!
     * \brief Building from basis vectors
     *
     * \param[in] d1 The first vector of the basis
     * \param[in] d2 The second vector of the basis
     * \param[in] d3 The third vector of the basis
     */
    BasisVectors( const Eigen::Vector3d & d1,
                  const Eigen::Vector3d & d2,
                  const Eigen::Vector3d & d3 ) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_( d1 ), d2_( d2 ), d3_( d3 )
    {
        // FIXME Check orthonormality ?
//        toRotationMatrix().check() ;
        Details::CheckRotationMatrix(getMatrix()) ;
    }

    /*!
     * \copydoc BasisVectors::BasisVectors(const Eigen::Vector3d &, const Eigen::Vector3d &, const Eigen::Vector3d &)
     */
    BasisVectors( const Vector & d1, const Vector & d2, const Vector & d3 ) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_( d1 ), d2_( d2 ), d3_( d3 )
    {
        // FIXME Check orthonormality ?
//        toRotationMatrix().check() ;
        Details::CheckRotationMatrix(getMatrix()) ;
    }
    /*!
     * \brief Dummy constructor
     *
     * \f$d_1\f$ is initialized to \f$(1,0,0)\f$
     *
     * \f$d_2\f$ is initialized to \f$(0,1,0)\f$
     *
     * \f$d_3\f$ is initialized to \f$(0,0,1)\f$
     */
    BasisVectors( void ) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_( 1., 0., 0. ),
        d2_( 0., 1., 0. ),
        d3_( 0., 0., 1. )
    {
        // Left blank
    }

    /*!
     * \brief Constructs a copy of provided BasisVectors.
     *
     * \param[in] other The other BasisVectors object.
     */
    BasisVectors(const BasisVectors & other) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_(other.d1_),
        d2_(other.d2_),
        d3_(other.d3_)
    {
    }

    /*!
     * \brief Constructors with each director's components.
     *
     * \param[in] d1x
     * \param[in] d1y
     * \param[in] d1z
     * \param[in] d2x
     * \param[in] d2y
     * \param[in] d2z
     * \param[in] d3x
     * \param[in] d3y
     * \param[in] d3z
     */
    BasisVectors( const double & d1x, const double & d1y, const double & d1z,
                  const double & d2x, const double & d2y, const double & d2z,
                  const double & d3x, const double & d3y, const double & d3z ) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_( d1x, d1y, d1z ),
        d2_( d2x, d2y, d2z ),
        d3_( d3x, d3y, d3z )
    {
        // FIXME Check orthonormality ?
//        toRotationMatrix().check() ;
        Details::CheckRotationMatrix(getMatrix()) ;
    }

    /*!
     * \brief Constructs from a Eigen::VectorXd
     *
     * see BasisVectors::BasisVectors(const double &, const double &, const double &, const double &, const double &, const double &, const double &, const double &, const double &)
     * for the order of the components
     */
    BasisVectors(const Eigen::VectorXd & unknowns) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_(1., 0., 0.),
        d2_(0., 1., 0.),
        d3_(0., 0., 1.)
    {
        assert(unknowns.size() == nUnknowns_) ;
        d1_.x() = unknowns(0) ;
        d1_.y() = unknowns(1) ;
        d1_.z() = unknowns(2) ;
        d2_.x() = unknowns(3) ;
        d2_.y() = unknowns(4) ;
        d2_.z() = unknowns(5) ;
        d3_.x() = unknowns(6) ;
        d3_.y() = unknowns(7) ;
        d3_.z() = unknowns(8) ;
        // FIXME Check orthonormality ?
//        toRotationMatrix().check() ;
        Details::CheckRotationMatrix(getMatrix()) ;
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in an BasisVectors.
     *
     * \param[in] other The other Rotator object.
     *
     */
    BasisVectors(const ABC & other) :
        Details::RotatorABC<BasisVectors>(9, 6),
        d1_(other.d1()), d2_(other.d2()), d3_(other.d3())
    {
    }

    Geometry::Vector d1(void) const ;
    Geometry::Vector d2(void) const ;
    Geometry::Vector d3(void) const ;

    /*!
     * \brief Gives the equivalent rotation matrix.
     *
     * Basis vectors \f$d_1\f$, \f$d_2\f$, \f$d_3\f$
     * are reassembled into the rotation matrix directly.
     *
     * \return The Eigen::Matrix3d Rotation Matrix.
     */
    Eigen::Matrix3d getMatrix(void) const ;

    BasisVectors & operator=(const BasisVectors & other) ;

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<BasisVectors>::rotate ;

    void reset() ;

    void copy(const BasisVectors & other) ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;

    /*!
    * \brief Returns a vector containing all the basis vectors coordinates.
    *
    * see BasisVectors::BasisVectors(const Eigen::VectorXd &)
    * for the order of the components
    * \return The vector with the nine vector coordinates
    */
    Eigen::VectorXd unknowns(void) const ;
    /*!
    * \brief Sets the directors coordinates from a vector.
    * \param[in] unknowns The vector containing the basis vectors coordinates.
    */
    void unknowns(const Eigen::VectorXd & unknowns) ;

    /*!
    * \brief Returns the constraints. \f$ \textbf{d}_i \cdot \textbf{d}_j = \delta_{ij} \f$
    * \returns The constraints vector.
    */
    Eigen::VectorXd constraints(void) ;
    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;
    Quaternion toQuaternion( void ) const ;
    AxisAndAngle toAxisAndAngle(void) const ;
    MRP toMRP(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    RotationVector toRotationVector(void) const ;

    //// Overload from RotatorABC<T>
    BasisVectors & operator=(const ABC & other) ;

} ;

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::BasisVectors>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::BASIS_VECTORS ;
} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_BasisVectors_hpp__
