#include <Eigen/Geometry>
#include <vector>
#include <complex>
#include <cmath>

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Exceptions.hpp"

#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

void HorizontalPlane::checkMatrix_(const Eigen::Matrix3d & R)
{
    using BV::Geometry::Exceptions::RotationInitialisationException ;
    using BV::Details::epsilon ;
    if( (std::abs(R(2,0)) > epsilon) || (std::abs(R(2,1)) > epsilon)
            || (std::abs(R(0,2)) > epsilon) || (std::abs(R(1,2)) > epsilon)
            || (std::abs( std::abs(R(2,2) - 1.) ) > epsilon) )
    {
        throw RotationInitialisationException("Incorrect Horizontal plane rotation matrix: not in XY plane.") ;
    }
}

double HorizontalPlane::getAngleFromR_(const Eigen::Matrix3d & R)
{
    return std::atan2(R(1,0), R(0,0)) ;
}

double HorizontalPlane::angle(void) const
{
    return angle_ ;
}

double & HorizontalPlane::angle(void)
{
    return angle_ ;
}

Eigen::Matrix3d HorizontalPlane::getMatrix(void) const
{
    Eigen::Matrix3d mat ;
    const double cosa(std::cos(angle_)) ;
    const double sina(std::sin(angle_)) ;
    mat(0, 0) = cosa ;
    mat(0, 1) = -sina ;
    mat(0, 2) = 0. ;
    mat(1, 0) = sina ;
    mat(1, 1) = cosa ;
    mat(1, 2) = 0. ;
    mat(2, 0) = 0. ;
    mat(2, 1) = 0. ;
    mat(2, 2) = 1. ;
    return mat ;
}

Geometry::Vector HorizontalPlane::d1( void ) const
{
    return Geometry::Vector( std::cos( angle_ ), std::sin( angle_ ), 0. ) ;
}
Geometry::Vector HorizontalPlane::d2( void ) const
{
    return Geometry::Vector( - std::sin( angle_ ), std::cos( angle_ ), 0. ) ;
}
Geometry::Vector HorizontalPlane::d3( void ) const
{
    return Geometry::Vector( 0., 0., 1. ) ;
}

HorizontalPlane & HorizontalPlane::operator=(const HorizontalPlane & other)
{
    angle_ = Details::AngleMinusPi_Pi(other.angle_) ;
    return *this ;
}


void HorizontalPlane::addOtherRotationAtLeft(const ABC & abc)
{
    Eigen::Matrix3d otherMatrix(abc.getMatrix()) ;
    checkMatrix_(otherMatrix) ;
    angle_ += getAngleFromR_(otherMatrix) ;
    angle_ = Details::AngleMinusPi_Pi(angle_) ;
}

void HorizontalPlane::addOtherRotationAtRight(const ABC & abc)
{
    addOtherRotationAtLeft(abc) ;
}

void HorizontalPlane::subtractOtherRotationAtLeft(const ABC & abc)
{
    Eigen::Matrix3d otherMatrix(abc.getMatrix()) ;
    checkMatrix_(otherMatrix) ;
    angle_ -= getAngleFromR_(otherMatrix) ;
    angle_ = Details::AngleMinusPi_Pi(angle_) ;
}

void HorizontalPlane::subtractOtherRotationAtRight(const ABC & abc)
{
    subtractOtherRotationAtLeft(abc) ;
}

Eigen::Vector3d HorizontalPlane::rotate(const Eigen::Vector3d & vect) const
{
    Eigen::Vector3d res ;
    const double cosa(std::cos(angle_)) ;
    const double sina(std::sin(angle_)) ;
    const double v0(vect(0)) ;
    const double v1(vect(1)) ;
    res(0) = cosa*v0 - sina*v1;
    res(1) = sina*v0 + cosa*v1;
    res(2) = vect(2) ;
    return res ;
}

void HorizontalPlane::inverse(void)
{
    angle_ = -angle_ ;
}

Eigen::VectorXd HorizontalPlane::unknowns(void) const
{
    unknowns_(0) = angle_ ;
    return unknowns_ ;
}

void HorizontalPlane::unknowns(const Eigen::VectorXd & unknowns)
{
    assert( unknowns.size() == nUnknowns() ) ;
    angle_ = unknowns(0) ;
}

Eigen::VectorXd HorizontalPlane::constraints(void)
{
    return constraints_ ;
}

void HorizontalPlane::reset()
{
    angle_ = 0. ;
}

void HorizontalPlane::copy(const HorizontalPlane & other)
{
    angle_ = other.angle_ ;
}

RotationMatrix HorizontalPlane::toRotationMatrix(void) const
{
    return RotationMatrix(getMatrix()) ;
}

BasisVectors HorizontalPlane::toBasisVectors(void) const
{
    return BasisVectors(getMatrix()) ;
}

Quaternion HorizontalPlane::toQuaternion( void ) const
{
    return Quaternion( std::cos( angle_ / 2. ), 0., 0., std::sin( angle_ / 2. ) ) ;
}

AxisAndAngle HorizontalPlane::toAxisAndAngle(void) const
{
    return AxisAndAngle(Eigen::Vector3d(0., 0., 1.), angle_) ;
}

MRP HorizontalPlane::toMRP(void) const
{
    return MRP(getMatrix()) ;
}

HorizontalPlane HorizontalPlane::toHorizontalPlane(void) const
{
    return *this ;
}

RotationVector HorizontalPlane::toRotationVector(void) const 
{
    return RotationVector(getMatrix()) ;
}

HorizontalPlane & HorizontalPlane::operator=(const ABC & other)
{
    angle_ = Details::AngleMinusPi_Pi(other.toHorizontalPlane().angle_) ;
    return *this ;
}

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
