/*!
 * \file EulerAngles.hpp
 * \brief FILL THIS!
 *
 * \author Damien Coache
 */

#pragma once
#ifndef __BV_Geometry_Rotation_EulerAngles_hpp__
#define __BV_Geometry_Rotation_EulerAngles_hpp__

#include <Eigen/Geometry>
#include <cmath>
#include <map>
#include <iostream>

#include "Geometry/GeometryTypedefs.hpp"

#include "Geometry/Rotation/Tools.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"
#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

/*!
 * \brief Implementation of the EulerAngles class to orientate a solid in 3D
 *        space.\n
 *        This is a template class to be able to deal with all conventions
 *        (12 possibilities). Tait-Bryan angles can also be defined using the
 *        appropriate conventions.
 *        It is fundamental to note that this implementation uses
 *        \e intrinsic or \e extrinsic convention.\n\n
 *        For intrinsic convention:\n
 *            The rotations are performed in the order U <- V <- W:\n
 *            W in first, V in second, U in third i.e. the matrix product \f$ R = R_U.R_V.R_W\f$ (according to Eigen)\n
 *             --> Intrinsic convention corresponds to rotation about local new axis\n\n
 *        For extrinsic convention:\n
 *            The rotations are performed in the order U -> V -> W:\n
 *            U in first, V in second, W in third i.e. the matrix product \f$ R = R_W.R_V.R_U\f$ (according to Eigen)\n
 *             --> Extrinsic convention corresponds to rotation about global (or parent) axis
 */
template <typename EulerConv>
class EulerAngles : public Details::RotatorABC<EulerAngles<EulerConv> >
{
protected:

    /*!
     * \brief The Euler angles convention used.
     */
    EulerConv convention_ ;
    /*!
     * \brief The internal representation of the Euler angles rotation.
     *
     * The domain for each angle is: [0,Pi]*[-Pi,Pi]*[-Pi,Pi]
     *
     * For extrinsic convention:
     *     - \a theta_U in [0,Pi]
     *     - \a theta_V in [-Pi,Pi]
     *     - \a theta_W in [-Pi,Pi]
     *
     * For intrinsic convention:
     *     - \a theta_U in [-Pi,Pi]
     *     - \a theta_V in [-Pi,Pi]
     *     - \a theta_W in [0,Pi]
     *
     * innerData_ vector contains angles order such that:\n
     * innerData_(0) corresponds to U axis\n
     * innerData_(1) corresponds to V axis\n
     * innerData_(2) corresponds to W axis
     */
    Eigen::Vector3d innerData_ ;

    /*!
     * \brief Constructs euler angle vector of 3 components.
     *
     * The euler angles are computed such to respect the \b UVW convention
     * and the \b order \convention (extrinsic or intrinsic).
     *
     * \param[in] R an Eigen::Matrix3d corresponding to a rotation matrix.
     * \return an Eigen::Vector3d of euler angles
     */
    const Eigen::Vector3d getEulerAnglesFromMatrix_(const Eigen::Matrix3d & R) const
    {
        using BV::Geometry::Rotation::OrderConvention ;
        if(convention_.getOrderConvention() == OrderConvention::INTRINSIC)
        {
            return R.eulerAngles(convention_.getU(),
                                 convention_.getV(),
                                 convention_.getW()
                                ) ;
        }
        else if(convention_.getOrderConvention() == OrderConvention::EXTRINSIC)
        {
            return R.eulerAngles(convention_.getW(),
                                 convention_.getV(),
                                 convention_.getU()
                                ).reverse() ;
        }
        else
        {
            throw "Error in euler angles order convention." ;
        }
    }


public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs from a rotation matrix.
     *
     * \param[in] mat an Eigen::Matrix3d matrix
     */
    template<typename Derived>
    EulerAngles(const Eigen::MatrixBase<Derived> & mat,
                typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0):
        Details::RotatorABC<EulerAngles<EulerConv> >(3, 0),
        convention_(EulerConv()),
        innerData_(getEulerAnglesFromMatrix_(mat))
    {
        Details::CheckRotationMatrix(mat) ;
    }

    /*!
     * \brief Dummy constructor
     */
    EulerAngles(void):
        Details::RotatorABC<EulerAngles<EulerConv> >(3, 0),
        convention_(EulerConv()),
        innerData_(0., 0., 0.)
    {
    }

    /*!
     * \brief Constructs EulerAngles according to initial alpha, beta and
     *        gamma angles.
     * \param[in] alpha The angle of rotation around U.
     * \param[in] beta The angle of rotation around new V.
     * \param[in] gamma The angle of rotation around new W.
     *
     * Note that if \a alpha, \a beta and \gamma are processed
     * such that they are in right domain of definition
     * given in EulerAngles::innerData_
     *
     */
    EulerAngles(const double & alpha, const double & beta, const double & gamma) :
        Details::RotatorABC<EulerAngles<EulerConv> >(3, 0),
        convention_(EulerConv()),
        innerData_(alpha, beta, gamma)
    {
        // Recompute the angles in order to have them defined in
        // [0,Pi]*[-Pi,Pi]*[-Pi,Pi]
        innerData_ = getEulerAnglesFromMatrix_(getMatrix()) ;
    }

    /*!
     * \brief Constructs a EulerAngles instance from unknowns.
     *
     * \param[in] unknowns A vector of length three
     *                     that contains the 3 Euler angles.
     *
     * Note that if \a alpha, \a beta and \gamma are processed
     * such that they are in right domain of definition
     * given in EulerAngles::innerData_
     *
     */
        // EulerAngles(const Eigen::Ref<const Eigen::VectorXd > & unknowns,
        EulerAngles(const Eigen::VectorXd & unknowns):
        Details::RotatorABC<EulerAngles<EulerConv> >(3, 0),
        convention_(EulerConv())

    {
        assert(unknowns.size() == this->nUnknowns_) ;
        innerData_(0) = unknowns(0) ;
        innerData_(1) = unknowns(1) ;
        innerData_(2) = unknowns(2) ;
        // Recompute the angles in order to have them defined in
        // [0,Pi]*[-Pi,Pi]*[-Pi,Pi]
        innerData_ = getEulerAnglesFromMatrix_(getMatrix()) ;
    }

    /*!
     * \brief Constructs a copy of provided EulerAngles (of same convention).
     *
     * \param[in] other The other EulerAngles object to copy.
     */
    EulerAngles(const EulerAngles<EulerConv> & other) :
        Details::RotatorABC<EulerAngles<EulerConv> >(3, 0),
        convention_(other.convention_),
        innerData_(other.innerData_)
    {
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in an EulerAngles.
     *
     * \param[in] other The other Rotator object.
     *
     */
    EulerAngles(const Rotation::ABC & other) :
        EulerAngles(other.template toEulerAngles<EulerConv>())
    {
    }

    /*!
     * \brief \a alpha getter (by copy).
     *
     * \a alpha is the angle corresponding to first angle in euler angles vector.
     * It corresponds to \a U axis. Take care, \a U axis can be the first or third multiplicated axis
     * depending on Rotation::OrderConvention choosen.
     *
     * \return a double: copied \a alpha
     *
     */
    double alpha(void) const
    {
        return innerData_(0) ;
    }

    /*!
     * \brief \a beta getter (by copy).
     *
     * \a beta is the angle corresponding to second angle in euler angles vector.
     * It corresponds to \a V axis.
     *
     * \return a double: copied \a beta
     *
     */
    double beta(void) const
    {
        return innerData_(1) ;
    }

    /*!
     * \brief \a gamma getter (by copy).
     *
     * \a gamma is the angle corresponding to third angle in euler angles vector.
     * It corresponds to \a W axis. Take care, \a W axis can be the first or third multiplicated axis
     * depending on Rotation::OrderConvention choosen.
     *
     * \return a double: copied \a gamma
     *
     */
    double gamma(void) const
    {
        return innerData_(2) ;
    }

    /*!
     * \brief \a alpha getter (by reference).
     *
     * \a alpha is the angle corresponding to first angle in euler angles vector.
     * It corresponds to \a U axis. Take care, \a U axis can be the first or third multiplicated axis
     * depending on Rotation::OrderConvention choosen.
     *
     * \return a double: a reference to \a alpha
     *
     */
    double & alpha(void)
    {
        return innerData_.coeffRef(0) ;
    }

    /*!
     * \brief \a beta getter (by reference).
     *
     * \a beta is the angle corresponding to second angle in euler angles vector.
     * It corresponds to \a V axis.
     *
     * \return a double: a reference to \a beta
     *
     */
    double & beta(void)
    {
        return innerData_.coeffRef(1) ;
    }

    /*!
     * \brief \a gamma getter (by reference).
     *
     * \a gamma is the angle corresponding to third angle in euler angles vector.
     * It corresponds to \a W axis. Take care, \a W axis can be the first or third multiplicated axis
     * depending on Rotation::OrderConvention choosen.
     *
     * \return a double: a reference to \a gamma
     *
     */
    double & gamma(void)
    {
        return innerData_.coeffRef(2) ;
    }

    /*!
     * \brief Euler angles convention getter.
     *
     * \return EulerConv a Rotation::EulerAnglesConvention
     *
     */
    const EulerConv & getConvention(void) const
    {
        return convention_ ;
    }

    /*!
     * \brief Member method to check singularity in euler angles.
     *
     * This function detects if a gimbal lock occurs for a specific Euler angle convention
     * Note that the checks are valid for both \a intrinsic and \a extrinsic convention.
     * For an Euler angle convention U-V-W, remembering that
     * the rotation matrix for \a intrinsic and \a extrinsic are the following:
     *
     * intrinsic: \f$R = R_U(\theta_U) . R_V(\theta_V) . R_W(\theta_W)\f$
     *
     * extrinsic: \f$R = R_W(\theta_W) . R_V(\theta_V) . R_U(\theta_U)\f$
     *
     * - If assymetric convention (i.e. in U-V-W, U and W are different)
     * then the singularity (gimbal lock) occurs when \f$\theta_V\f$ = +-90 deg
     * - If symetric convention (i.e. in U-V-W, U and W are the same)
     * then the singularity (gimbal lock) occurs when \f$\theta_V\f$ = 0 deg, +-180 deg
     *
     * \return a boolean if the current euler angles values are on a singularity
     *
     */
    bool isSingular(void) const
    {


        using BV::Details::epsilon ;
        double midAngle_MinusPi_Pi(Details::AngleMinusPi_Pi(innerData_.coeff(1))) ;
        if(convention_.getU() != convention_.getW())
        {
            return ( std::abs(std::abs(midAngle_MinusPi_Pi) - M_PI/2.) < epsilon ) ;
        }
        else if(convention_.getU() == convention_.getW())
        {
            return ( (std::abs(std::abs(midAngle_MinusPi_Pi) - M_PI) < epsilon)
                     || (std::abs(midAngle_MinusPi_Pi) < epsilon)
                   ) ;
        }
        else
        {
            throw "The convention passed to detect the singularity in Euler angles "
                    "does not represent an Euler Angle valid convention." ;
        }
        return false ;
    }

    /*!
     * \brief Assignment operator for EulerAngles class with same EulerAnglesConvention.
     */
    EulerAngles<EulerConv> & operator=(const EulerAngles<EulerConv> & other)
    {
        // We first have to convert the other to the correct convention
        //EulerAngles tmp(other.toEulerAngles(convention_)) ;
        //innerData_ = other.innerData_ ;
        //return *this ;
        innerData_ = other.innerData_ ;
        return *this ;
    }

    /*!
    * \brief Give the equivalent rotation matrix.
    *
    * see Rotation::EulerAngles for the way the rotation matrix is constructed
    * depending on \a axis and \a order conventions.
    *
    * \return The Eigen::Matrix3d Rotation Matrix.
    */
    Eigen::Matrix3d getMatrix(void) const
    {
        // FIXME: comment below is old, we now deal with intrinsic/extrinsic
        // FIXME: remove it?
        // Here is the fundamental place for intrinsic/extrinsic conventions.
        // For the moment, we implement _intrinsic_ convention.
        // Up to my current knowledge, it should be enough to invert
        // the order to change convention. But don't trust me, read & test!
        // The convention below follow the Eigen convetion to generate the angles.
        // If we want to change the convention (the multiplication order,
        // take care of the construction of the angles through Eigen
        // see Eigen/src/Geometry/EulerAngles.h for more details

        if(convention_.getOrderConvention() == OrderConvention::INTRINSIC)
        {
        return (
                Eigen::AngleAxisd(innerData_(0), convention_.getUUnitVector())
              * Eigen::AngleAxisd(innerData_(1),  convention_.getVUnitVector())
              * Eigen::AngleAxisd(innerData_(2), convention_.getWUnitVector())
                                 ).toRotationMatrix() ;
        }
        else if(convention_.getOrderConvention() == OrderConvention::EXTRINSIC)
        {
            return (Eigen::AngleAxisd(innerData_(2), convention_.getWUnitVector())
                  * Eigen::AngleAxisd(innerData_(1),  convention_.getVUnitVector())
                  * Eigen::AngleAxisd(innerData_(0), convention_.getUUnitVector())
                                     ).toRotationMatrix() ;
        }
        else
        {
            throw "Error in euler angles order convention." ;
        }
    }
    /*
     * \brief Check whether angles are close to PI and set them back close to zero to
     *        avoid any timeseries discontinuity.
     *        FIXME Works for EulerAngleXYZe convention.
     *        FIXME Need to check if this method works for other conventions too!!
     */
    void setSmallAnglesContinuity(void)
    {
        if ((this->getConvention().isEqualTo(EulerAnglesConvention_XYZ_e())) ||
            (this->getConvention().isEqualTo(EulerAnglesConvention_XYZ_i())))
        {
            Eigen::Vector3d unknownsTmp(0., 0., 0.) ;
            for (int i=0; i<3; ++i)
            {
               if (innerData_(i) < -2.)
               {
                   unknownsTmp(i) = innerData_(i) + M_PI ;
                   if (i == 1)
                   {
                       unknownsTmp(i) *= -1. ;
                   }
               }
               else if (innerData_(i) > 2.)
               {
                   unknownsTmp(i) = innerData_(i) - M_PI ;
                   if (i == 1)
                   {
                       unknownsTmp(i) *= -1. ;
                   }
               }
               else
               {
                   unknownsTmp(i) = innerData_(i) ;
               }
            }
            innerData_ = unknownsTmp ;
        }
        else
        {
            throw "Euler angles convention must be EulerAnglesConvention_XYZ" ;
        }
    }

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<EulerAngles<EulerConv> >::rotate ;

    void reset()
    {
        innerData_.setZero() ;
    }

    void copy(const EulerAngles<EulerConv> & other)
    {
        innerData_ = other.innerData_ ;
    }

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const Rotation::ABC & abc)
    {
        Eigen::Matrix3d compRot(abc.getMatrix() * getMatrix()) ;
        innerData_ = getEulerAnglesFromMatrix_(compRot) ;
    }
    void addOtherRotationAtRight(const Rotation::ABC & abc)
    {
        Eigen::Matrix3d compRot(getMatrix() * abc.getMatrix()) ;
        innerData_ = getEulerAnglesFromMatrix_(compRot) ;
    }
    void subtractOtherRotationAtLeft(const Rotation::ABC & abc)
    {
        Eigen::Matrix3d compRot(abc.getMatrix().transpose() * getMatrix()) ;
        innerData_ = getEulerAnglesFromMatrix_(compRot) ;
    }
    void subtractOtherRotationAtRight(const Rotation::ABC & abc)
    {
        Eigen::Matrix3d compRot(getMatrix() * abc.getMatrix().transpose()) ;
        innerData_ = getEulerAnglesFromMatrix_(compRot) ;
    }
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const
    {
        return getMatrix() * vect ;
    }

    void inverse(void)
    {
        innerData_ = getEulerAnglesFromMatrix_(getMatrix().transpose()) ;
    }

    /*!
     * \brief Returns a vector containing all the EulerAngles coordinates (\a alpha,\a beta, \a gamma).
     *
     * see EulerAngles::innerData_ for return order of angles depending on axis and order convention.
     *
     * \return The vector with the three EulerAngles coordinates (\a alpha,\a beta, \a gamma)
     */
    Eigen::VectorXd unknowns(void) const
    {
        this->unknowns_(0) = innerData_(0) ;
        this->unknowns_(1) = innerData_(1) ;
        this->unknowns_(2) = innerData_(2) ;
        return this->unknowns_ ;
    }
    /*!
    * \brief Defines the EulerAngles coordinates (\a alpha,\a beta, \a gamma) from a vector.
    *
    * see EulerAngles::innerData_ for return order of angles depending on axis and order convention.
    *
    * \param[in] unknowns The vector containing the EulerAngles coordinates (\a alpha,\a beta, \a gamma).
    */
    void unknowns(const Eigen::VectorXd & unknowns)
    {
        assert( unknowns.size() == this->nUnknowns() ) ;
        innerData_ = unknowns ;
        innerData_ = getEulerAnglesFromMatrix_(getMatrix()) ;
        // FIXME: seem that code below does not work while wrapping in python
        // innerData_(0) = unknowns_.coeff(0) ;
        // innerData_(1) = unknowns_.coeff(1) ;
        // innerData_(2) = unknowns_.coeff(2) ;
    }
    /*!
    * \brief Returns a contraints vector of size 0.
    *
    * EulerAngles does not have to fulfill constraints equations.
    *
    * \returns The constraints vector of size 0.
    */
    Eigen::VectorXd constraints(void)
    {
        return this->constraints_ ;
    }
    RotationMatrix toRotationMatrix(void) const
    {
        return RotationMatrix(getMatrix()) ;
    }
    BasisVectors toBasisVectors(void) const
    {
        return BasisVectors(getMatrix()) ;
    }
    Quaternion toQuaternion( void ) const
    {
        return Quaternion(getMatrix()) ;
    }
    AxisAndAngle toAxisAndAngle(void) const
    {
        return AxisAndAngle(getMatrix()) ;
    }
    MRP toMRP(void) const
    {
        return toQuaternion().toMRP() ;
    }
    HorizontalPlane toHorizontalPlane(void) const
    {
        return HorizontalPlane(getMatrix()) ;
    }
    RotationVector toRotationVector(void) const
    {
        return RotationVector(toRotationMatrix()) ;
    }

    //// Overload from RotatorABC<T>
    EulerAngles<EulerConv> & operator=(const Rotation::ABC & other)
    {
        innerData_ = other.template toEulerAngles<EulerConv>().innerData_ ;

        // innerData_ = other.getMatrix().eulerAngles(convention_.getU(), convention_.getV(), convention_.getW()) ;
        return *this ;
    }

} ;

/*
EULER_ANGLES_XYX
EULER_ANGLES_XYZ
EULER_ANGLES_XZX
EULER_ANGLES_XZY

EULER_ANGLES_YXY
EULER_ANGLES_YXZ
EULER_ANGLES_YZX
EULER_ANGLES_YZY

EULER_ANGLES_ZXY
EULER_ANGLES_ZXZ
EULER_ANGLES_ZYX
EULER_ANGLES_ZYZ
*/

typedef EulerAngles<EulerAnglesConvention_XYX_i> EulerAngles_XYX_i ;
typedef EulerAngles<EulerAnglesConvention_XYZ_i> EulerAngles_XYZ_i ;
typedef EulerAngles<EulerAnglesConvention_XZX_i> EulerAngles_XZX_i ;
typedef EulerAngles<EulerAnglesConvention_XZY_i> EulerAngles_XZY_i ;
typedef EulerAngles<EulerAnglesConvention_YXY_i> EulerAngles_YXY_i ;
typedef EulerAngles<EulerAnglesConvention_YXZ_i> EulerAngles_YXZ_i ;
typedef EulerAngles<EulerAnglesConvention_YZX_i> EulerAngles_YZX_i ;
typedef EulerAngles<EulerAnglesConvention_YZY_i> EulerAngles_YZY_i ;
typedef EulerAngles<EulerAnglesConvention_ZXY_i> EulerAngles_ZXY_i ;
typedef EulerAngles<EulerAnglesConvention_ZXZ_i> EulerAngles_ZXZ_i ;
typedef EulerAngles<EulerAnglesConvention_ZYX_i> EulerAngles_ZYX_i ;
typedef EulerAngles<EulerAnglesConvention_ZYZ_i> EulerAngles_ZYZ_i ;

typedef EulerAngles<EulerAnglesConvention_XYX_e> EulerAngles_XYX_e ;
typedef EulerAngles<EulerAnglesConvention_XYZ_e> EulerAngles_XYZ_e ;
typedef EulerAngles<EulerAnglesConvention_XZX_e> EulerAngles_XZX_e ;
typedef EulerAngles<EulerAnglesConvention_XZY_e> EulerAngles_XZY_e ;
typedef EulerAngles<EulerAnglesConvention_YXY_e> EulerAngles_YXY_e ;
typedef EulerAngles<EulerAnglesConvention_YXZ_e> EulerAngles_YXZ_e ;
typedef EulerAngles<EulerAnglesConvention_YZX_e> EulerAngles_YZX_e ;
typedef EulerAngles<EulerAnglesConvention_YZY_e> EulerAngles_YZY_e ;
typedef EulerAngles<EulerAnglesConvention_ZXY_e> EulerAngles_ZXY_e ;
typedef EulerAngles<EulerAnglesConvention_ZXZ_e> EulerAngles_ZXZ_e ;
typedef EulerAngles<EulerAnglesConvention_ZYX_e> EulerAngles_ZYX_e ;
typedef EulerAngles<EulerAnglesConvention_ZYZ_e> EulerAngles_ZYZ_e ;

} // end of namespace Rotation
/*
EULER_ANGLES_XYX
EULER_ANGLES_XYZ
EULER_ANGLES_XZX
EULER_ANGLES_XZY
 */
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XYX_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XYX_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XYZ_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XYZ_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XZX_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XZX_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XZY_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XZY_i ;
} ;


template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XYX_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XYX_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XYZ_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XYZ_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XZX_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XZX_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_XZY_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_XZY_e ;
} ;
/*
EULER_ANGLES_YXY
EULER_ANGLES_YXZ
EULER_ANGLES_YZX
EULER_ANGLES_YZY
 */
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YXY_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YXY_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YXZ_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YXZ_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YZX_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YZX_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YZY_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YZY_i ;
} ;


template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YXY_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YXY_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YXZ_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YXZ_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YZX_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YZX_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_YZY_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_YZY_e ;
} ;

/*
EULER_ANGLES_ZXY
EULER_ANGLES_ZXZ
EULER_ANGLES_ZYX
EULER_ANGLES_ZYZ
 */
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZXY_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZXY_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZXZ_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZXZ_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZYX_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZYX_i ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZYZ_i>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZYZ_i ;
} ;

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZXY_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZXY_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZXZ_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZXZ_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZYX_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZYX_e ;
} ;
template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::EulerAngles_ZYZ_e>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::EULER_ANGLES_ZYZ_e ;
} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_EulerAngles_hpp__
