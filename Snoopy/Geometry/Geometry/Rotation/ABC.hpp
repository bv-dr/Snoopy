#pragma once

#include <vector>
#include <exception>
#include <Eigen/Geometry>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"

#if EIGEN_VERSION_AT_LEAST(3, 3, 0)

namespace BV {
namespace Geometry {
namespace Rotation {

class ABC ;

} // End of namespace Rotation
} // End of namespace Geometry
} // End of namespace ABC

namespace Eigen {

template<> struct NumTraits<BV::Geometry::Rotation::ABC>
{
  enum {IsInteger = false};
} ;

//namespace internal {
//
//template<typename To>
//struct is_convertible<BV::Geometry::Rotation::ABC, To>
//{
//  enum {value = false} ;
//} ;
//
//} // End of namespace internal
} // End of namespace Eigen

#endif // Test Eigen version

namespace BV {
namespace Geometry {

// FIXME not class enum as they are used as template parameters at the moment...
enum RotatorTypeEnum {
                       QUATERNION,
                       BASIS_VECTORS,
                       AXIS_AND_ANGLE,
                       ROTATION_MATRIX,
                       MODIFIED_RODRIGUES_PARAMETERS,
                       ROTATION_VECTOR,
                       HORIZONTAL_PLANE,
                       EULER_ANGLES_XYX_i,
                       EULER_ANGLES_XYZ_i,
                       EULER_ANGLES_XZX_i,
                       EULER_ANGLES_XZY_i,
                       EULER_ANGLES_YXY_i,
                       EULER_ANGLES_YXZ_i,
                       EULER_ANGLES_YZX_i,
                       EULER_ANGLES_YZY_i,
                       EULER_ANGLES_ZXY_i,
                       EULER_ANGLES_ZXZ_i,
                       EULER_ANGLES_ZYX_i,
                       EULER_ANGLES_ZYZ_i,
                       EULER_ANGLES_XYX_e,
                       EULER_ANGLES_XYZ_e,
                       EULER_ANGLES_XZX_e,
                       EULER_ANGLES_XZY_e,
                       EULER_ANGLES_YXY_e,
                       EULER_ANGLES_YXZ_e,
                       EULER_ANGLES_YZX_e,
                       EULER_ANGLES_YZY_e,
                       EULER_ANGLES_ZXY_e,
                       EULER_ANGLES_ZXZ_e,
                       EULER_ANGLES_ZYX_e,
                       EULER_ANGLES_ZYZ_e,
                       UNDEFINED_ROTATION
                           } ;

template <typename RotatorType>
struct   RotatorTypeToRotatorTypeEnum ;

namespace Rotation {

// Forward declaration for conversion
class RotationMatrix ;
class BasisVectors ;
class Quaternion ;
class AxisAndAngle ;
class MRP ;
class HorizontalPlane ;
class RotationVector ;

template <typename EulerConv>
class EulerAngles ;

/*!
 * \class ABC
 * \brief The abstract base class pattern for rotations
 *
 * Pure virtual methods:
 *     - virtual void addOtherRotationAtLeft(const ABC & abc) = 0 ;
 *     - virtual void addOtherRotationAtRight(const ABC & abc) = 0 ;
 *     - virtual void subtractOtherRotationAtLeft(const ABC & abc) = 0 ;
 *     - virtual void subtractOtherRotationAtRight(const ABC & abc) = 0 ;
 *     - virtual Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const = 0 ;
 *     - virtual Eigen::Vector3d inverseRotate(const Eigen::Vector3d & vect) const = 0 ;
 *     - virtual void inverse(void) const = 0 ;
 *     - virtual Eigen::VectorXd unknowns(void) = 0 ;
 *     - virtual void unknowns(const Eigen::VectorXd & unknowns) = 0 ;
 *     - virtual Eigen::VectorXd constraints(void) = 0 ;
 *     - virtual RotationMatrix toRotationMatrix(void) const = 0 ;
 *     - virtual Quaternion toQuaternion( void ) const = 0 ;
 *     - virtual AxisAndAngle toAxisAndAngle(void) const = 0 ;
 *     - virtual BasisVectors toBasisVectors(void) const = 0 ;
 *     - virtual HorizontalPlane toHorizontalPlane(void) const = 0 ;
 *     - virtual MRP toMRP(void) const = 0 ;
 *
 * Defined operators:
 *     - Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const
 *     - Geometry::Vector operator*(const Geometry::Vector & vect) const
 *     - Geometry::Point operator*(const Geometry::Point & pt) const
 */
class ABC
{
protected:
    const unsigned int nUnknowns_ ;
    const unsigned int nConstraints_ ;
    mutable Eigen::VectorXd unknowns_ ;
    Eigen::VectorXd constraints_ ;

public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Abstract constructor which initialize the unknowns number and values
     *            and also the constraints number and values.
     *
     * \param[in] nUnknowns The unknowns number of the rotator.
     * \param[in] nConstraints The constraints number of the rotator.
     */
    ABC(const unsigned int & nUnknowns, const unsigned int & nConstraints) :
        nUnknowns_(nUnknowns), nConstraints_(nConstraints),
        unknowns_(nUnknowns),
        constraints_(nConstraints)
    {
        // Left blank
    }

    /*!
     * \brief Abstract destructor.
     */
    virtual ~ABC( void )
    {
        // Left blank
    }

    virtual RotatorTypeEnum getRotatorType() const = 0 ;

    /*!
     * \brief Add a rotation to the current rotator. The "added" rotation is performed \b after current rotation.
     *
     * This method composes the rotations such that
     * for a given vector \f$b\f$ to be rotated twice firstly with regards to current rotator ( \f$R\f$ ),
     * then with the rotation given in signature ( \f$R_2\f$ ):
     * \f[ b' = R.b\f]
     * \f[ b'' = R_2.b'\f]
     * The final rotation represent the matrix \f$R''\f$ such that:
     * \f[ b'' = R'' b = R_2.R.b\f]
     * Thus the final rotation is \f$R'' = R_2.R\f$
     */
    virtual void addOtherRotationAtLeft(const ABC & abc) = 0 ;

    /*!
     * \brief Add a rotation to the current rotator. The "added" rotation is performed \b before current rotation.
     *
     * This method composes the rotations such that
     * for a given vector \f$b\f$ to be rotated twice firstly with regards to the rotation given in signature ( \f$R_2\f$ ),
     * then with current rotator ( \f$R\f$ ):
     * \f[ b' = R_2.b\f]
     * \f[ b'' = R.b'\f]
     * The final rotation represent the matrix \f$R''\f$ such that:
     * \f[ b'' = R'' b = R.R_2.b\f]
     * Thus the final rotation is \f$R'' = R.R_2\f$
     */
    virtual void addOtherRotationAtRight(const ABC & abc) = 0 ;

    /*!
     * \brief Substract a rotation to the current rotator. The "subtracted" rotation is performed \b after current rotation.
     *
     * This method composes the rotations such that
     * for a given vector \f$b\f$ to be rotated twice firstly with regards to current rotator ( \f$R\f$ ),
     * then with the inverse of the rotation given in signature ( \f$R_2\f$ ):
     * \f[ b' = R.b\f]
     * \f[ b'' = {}^tR_2.b'\f]
     * The final rotation represent the matrix \f$R''\f$ such that:
     * \f[ b'' = R'' b = {}^tR_2.R.b\f]
     * Thus the final rotation is \f$R'' = {}^tR_2.R\f$
     */
    virtual void subtractOtherRotationAtLeft(const ABC & abc) = 0 ;

    /*!
     * \brief Substract a rotation to the current rotator. The "subtracted" rotation is performed \b before current rotation.
     *
     * This method composes the rotations such that
     * for a given vector \f$b\f$ to be rotated twice firstly with regards to the inverse of the rotation given in signature ( \f$R_2\f$ ),
     * then with current rotator ( \f$R\f$ ):
     * \f[ b' = {}^tR_2.b\f]
     * \f[ b'' = R.b'\f]
     * The final rotation represent the matrix \f$R''\f$ such that:
     * \f[ b'' = R'' b = R .{}^tR_2.b\f]
     * Thus the final rotation is \f$R'' = R .{}^tR_2\f$
     */
    virtual void subtractOtherRotationAtRight(const ABC & abc) = 0 ;

    /*!
     * \brief Rotate a vector
     *
     * This method rotates the vector \f$b\f$ given in signature such that: \f$ b' = R.b\f$
     * \return The rotated Vector \f$b'\f$
     */
    virtual Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const = 0 ;

    /*!
     * \brief Apply the inverse rotation to a vector
     *
     * This method rotates the vector \f$b\f$ given in signature such that: \f$b' = {}^tR.b \f$
     * \return The rotated Vector \f$b'\f$
     */
    virtual Eigen::Vector3d inverseRotate(const Eigen::Vector3d & vect) const = 0 ;

    /*!
     * \brief Compute and set the inverse in internal representation, representing the inverse rotation of itself.
     */
    virtual void inverse(void) = 0 ;

    /*!
     * \brief Unknowns paramaters getter.
     *
     * \return The unknowns of the derived parametrization.
     */
    virtual Eigen::VectorXd unknowns(void) const = 0 ;

    /*!
     * \brief Unknowns paramaters setter. Set the unknowns of the derived parametrization.
     */
    virtual void unknowns(const Eigen::VectorXd & unknowns) = 0 ;

    /*!
     * \brief Constraints paramaters getter.
     *
     * \return The constraints of the derived parametrization.
     */
    virtual Eigen::VectorXd constraints(void) = 0 ;

    virtual void reset() = 0 ;

    /*!
     * \brief Converter to RotationMatrix parametrization.
     *
     * \return A rotation of type RotationMatrix
     */
    virtual RotationMatrix toRotationMatrix(void) const = 0 ;

    /*!
     * \brief Converter to BasisVectors parametrization.
     *
     * \return A rotation of type BasisVectors
     */
    virtual BasisVectors toBasisVectors(void) const = 0 ;

    /*!
     * \brief Converter to Quaternion parametrization.
     *
     * \return A rotation of type Quaternion
     */
    virtual Quaternion toQuaternion( void ) const = 0 ;

    /*!
     * \brief Converter to AxisAndAngle parametrization.
     *
     * \return A rotation of type AxisAndAngle
     */
    virtual AxisAndAngle toAxisAndAngle(void) const = 0 ;

    /*!
     * \brief Converter to Modified Rodrigues Parameters (MRP) parametrization.
     *
     * \return A rotation of type Modified Rodrigues Parameters (MRP)
     */
    virtual MRP toMRP(void) const = 0 ;

    /*!
     * \brief Converter to RotationVector parametrization.
     *
     * \return A rotation of type RotationVector
     */
    virtual RotationVector toRotationVector(void) const = 0 ;

    /*!
     * \brief Converter to HorizontalPlane parametrization.
     *
     * \return A rotation of type HorizontalPlane
     */
    virtual HorizontalPlane toHorizontalPlane(void) const = 0 ;

    /*!
     * \brief Converter to EulerAngle parametrization.
     *
     * \tparam An EulerAnglesConvention
     * \return A rotation of type EulerAngles with given convention as template parameter
     */
    template <typename EulerConvention>
    EulerAngles<EulerConvention> toEulerAngles(void) const
    {
        return EulerAngles<EulerConvention>(getMatrix()) ;
    }

    /*!
     * \brief Returning the number of unknowns
     *
     * \return Unknowns number
     */
    const unsigned int & nUnknowns(void) const
    {
        return nUnknowns_ ;
    }

    /*!
     * \brief Returning the number of constraints
     *
     * \return Constraints number
     */
    const unsigned int & nConstraints(void) const
    {
        return nConstraints_ ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::rotate(const Eigen::Vector3d &) const
     */
    Vector rotate(const Vector & vect) const
    {
        return Vector(rotate(vect.toArray())) ;
    }

    /*!
     * \brief Rotate a point
     *
     * This method rotates the point \f$b\f$ given in signature such that \f$b' = R.b \f$
     * \return The rotated point \f$b'\f$
     */
    Point rotate(const Point & pt) const
    {
        return Point(rotate(pt.toArray())) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::inverseRotate(const Eigen::Vector3d &) const
     */
    Vector inverseRotate(const Vector & vect) const
    {
        return Vector(inverseRotate(vect.toArray())) ;
    }

    /*!
     * \brief Apply the inverse rotation to a point
     *
     * This method rotates the point \f$b\f$ given in signature such that: \f$b' = {}^tR.b \f$
     * \return The rotated point \f$b'\f$
     */
    Point inverseRotate(const Point & pt) const
    {
        return Point(inverseRotate(pt.toArray())) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::rotate(const Eigen::Vector3d &) const
     */
    Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const
    {
        return rotate(vect) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::rotate(const BV::Geometry::Vector &) const
     */
    Vector operator*(const Vector & vect) const
    {
        return rotate(vect) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::rotate(const BV::Geometry::Point &) const
     */
    Point operator*(const Point & pt) const
    {
        return rotate(pt) ;
    }

    /*!
     * \brief Return the director \f$d_1\f$
     *
     * \f$d_1\f$ correspond to the transformed vector \f$X = (1, 0, 0)\f$
     * by the rotation such that \f$d_1 = R.X \f$
     *
     * This functions delegates to BV::Geometry::Rotation::ABC::rotate(const BV::Geometry::Vector &) const.
     *
     * \return Director \f$d_1\f$
     */
    virtual Vector d1(void) const // virtual only because of BasisVectors private attributes: FIXME?
    {
        return rotate(Vector(1., 0., 0.));
    }

    /*!
     * \brief Return the director \f$d_2\f$
     *
     * \f$d_2\f$ correspond to the transformed vector \f$Y = (0, 1, 0)\f$
     * by the rotation such that \f$d_2 = R.Y\f$
     *
     * This functions delegates to BV::Geometry::Rotation::ABC::rotate(const BV::Geometry::Vector &) const.
     *
     * \return Director \f$d_2\f$
     */
    virtual Vector d2(void) const // virtual only because of BasisVectors private attributes: FIXME?
    {
        return rotate(Vector(0., 1., 0.));
    }

    /*!
     * \brief Return the director \f$d_3\f$
     *
     * \f$d_3\f$ correspond to the transformed vector \f$Z = (0, 0, 1)\f$
     * by the rotation such that \f$d_3 = R.Z\f$
     *
     * This functions delegates to BV::Geometry::Rotation::ABC::rotate(const BV::Geometry::Vector &) const.
     *
     * \return Director \f$d_3\f$
     */
    virtual Vector d3(void) const // virtual only because of BasisVectors private attributes: FIXME?
    {
        return rotate(Vector(0., 0., 1.));
    }

    /*!
     * \brief Give the equivalent rotation matrix.
     *
     * This function delegates to BV::Geometry::Rotation::ABC::rotate(const Eigen::Vector3d &) const.
     * \return The Eigen::Matrix3d Rotation Matrix.
     */
    virtual Eigen::Matrix3d getMatrix(void) const
    {
        Eigen::Matrix3d mat(Eigen::Matrix3d::Zero()) ;
        mat.col(0) = rotate(Eigen::Vector3d(1., 0., 0.)) ;
        mat.col(1) = rotate(Eigen::Vector3d(0., 1., 0.)) ;
        mat.col(2) = rotate(Eigen::Vector3d(0., 0., 1.)) ;
        return mat ;
    }

    /*!
     * \brief Equality operator
     *
     * \return True if the rotator represents the same rotation as an other
     * (by comparing equality between directors)
     * False otherwise
     */
    bool operator==(const ABC & other) const
    {
        return ((d1() == other.d1()) && (d2() == other.d2())
                        && (d3() == other.d3()));
    }

    /*!
     * \brief Non Equality operator
     *
     * \return True if the rotator represents different rotation as an other,
     * by comparing equality between directors
     * False otherwise
     */
    bool operator!=(const ABC & other) const
    {
        return !(*this == other) ;
    }
} ;

namespace Details {

/*!
 * \class RotatorABC
 * \tparam Derived the derived type
 * \brief Template class which allow to implement member methods with derived type.
 *
 * Pure virtual methods:
 *     - virtual Derived & operator=(const ABC & other) = 0 ;
 *
 * RotatorABC overloads ABC's member methods:
 *     - virtual Eigen::Vector3d inverseRotate(const Eigen::Vector3d & vect) const = 0 ;
 *
 * Defined operators:
 *     - Derived & operator*=(const ABC & other)
 *     - Derived & operator-=(const ABC & other)
 *     - Derived & operator+=(const ABC & other)
 *     - Derived operator*(const ABC & other)
 *     - Derived operator+(const ABC & other)
 *     - Derived operator-(const ABC & other)
 *     - Derived operator-(void)
 */
template <typename Derived>
class RotatorABC : public ABC
{
private:
    mutable Derived * p_inverse_ ;
public:
    // typedef Derived RotatorType ;

    /*!
     * \brief Abstract constructor which initialize the unknowns number and values
     *            and also the constraints number and values.
     *
     * \param[in] nUnknowns The unknowns number of the rotator.
     * \param[in] nConstraints The constraints number of the rotator.
     */
    RotatorABC(const unsigned int & nUnknowns,
               const unsigned int & nConstraints) :
        ABC(nUnknowns, nConstraints),
        p_inverse_(nullptr)
    {
    }

    /*!
     * \brief Abstract destructor.
     */
    virtual ~RotatorABC(void)
    {
        delete p_inverse_ ;
    }

    RotatorTypeEnum getRotatorType() const
    {
        return RotatorTypeToRotatorTypeEnum<Derived>::value ;
    }

    // === Using to avoid c++ hidden methods when overloading === //
    /*!
     * \copydoc BV::Geometry::Rotation::ABC::operator*(const Eigen::Vector3d &) const
     */
    using ABC::operator* ;

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::inverseRotate(const Eigen::Vector3d &) const
     */
    using ABC::inverseRotate ;

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::rotate(const Eigen::Vector3d &) const
     */
    using ABC::rotate ;

    // === End of Using === //

    /*!
     * \brief Return the Rotation objects of same type representing the inverse rotation of itself.
     */
    // virtual Derived getInversed(void) const = 0 ;
    const Derived & getInversed(void) const
    {
        //Derived tmp(*this) ;
        if (p_inverse_ == nullptr)
        {
            p_inverse_ = new Derived ;
        }
        p_inverse_->copy(static_cast<const Derived &>(*this)) ;
        p_inverse_->inverse() ;
        return *p_inverse_ ;
    }

    /*!
     * \brief Assigment operator that convert an other rotation represention
     *        into self parameter convention.
     */
    virtual Derived & operator=(const ABC & other) = 0 ;
    virtual Derived & operator=(const Derived & other) = 0 ;

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::addOtherRotationAtRight(const ABC & abc)
     */
    Derived & operator*=(const ABC & other)
    {
        addOtherRotationAtRight(other) ;
        return static_cast<Derived &>(*this) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::subtractOtherRotationAtRight(const ABC & abc)
     */
    Derived & operator-=(const ABC & other)
    {
        subtractOtherRotationAtRight(other) ;
        return static_cast<Derived &>(*this) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::addOtherRotationAtRight(const ABC & abc)
     */
    Derived & operator+=(const ABC & other)
    {
        addOtherRotationAtRight(other) ;
        return static_cast<Derived &>(*this) ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::addOtherRotationAtRight(const ABC & abc)
     */
    Derived operator*(const ABC & other) const
    {
        Derived tmp(*this) ;
        tmp *= other ;
        return tmp ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::addOtherRotationAtRight(const ABC & abc)
     */
    Derived operator+(const ABC & other) const
    {
        return *this * other ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::subtractOtherRotationAtRight(const ABC & abc)
     */
    Derived operator-(const ABC & other) const
    {
        Derived tmp(*this) ;
        tmp -= other ;
        return tmp ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::Details::RotatorABC::getInversed(void) const
     */
    Derived operator-(void)
    {
        return getInversed() ;
    }

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::inverseRotate(const Eigen::Vector3d &) const
     */
    Eigen::Vector3d inverseRotate(const Eigen::Vector3d & vect) const
    {
        return getInversed() * vect ;
    }
} ;

} // end of namespace Details

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
