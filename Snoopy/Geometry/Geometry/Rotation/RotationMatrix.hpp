#pragma once
#ifndef __BV_Geometry_Rotation_RotationMatrix_hpp__
#define __BV_Geometry_Rotation_RotationMatrix_hpp__

#include "GeometryExport.hpp"

#include <algorithm>
#include <Eigen/Dense>

#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

class GEOMETRY_API RotationMatrix : public Details::RotatorABC<RotationMatrix>
{
private:
    Eigen::Matrix3d innerData_ ;

public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Dummy constructor
     *
     * The rotation matrix is initialized to identity.
     */
    RotationMatrix(void) :
        Details::RotatorABC<RotationMatrix>(9, 6),
        innerData_(Eigen::Matrix3d::Identity())
    {
    }

    /*!
     * \brief Constructs from a rotation matrix.
     *
     * \param[in] mat an Eigen::Matrix3d matrix
     */
    template<typename Derived>
    RotationMatrix(const Eigen::MatrixBase<Derived> & mat,
                   typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC<RotationMatrix>(9, 6),
        innerData_(mat)
    {
        Details::CheckRotationMatrix(innerData_) ;
    }

    /*!
     * \brief Constructs a copy of provided RotationMatrix.
     *
     * \param[in] other The other RotationMatrix object to copy.
     */
    RotationMatrix(const RotationMatrix & other) :
        Details::RotatorABC<RotationMatrix>(9, 6),
        innerData_(other.innerData_)
    {
    }

    /*!
     * \brief Constructors with each rotation matrix components.
     *
     * \param[in] r00
     * \param[in] r10
     * \param[in] r20
     * \param[in] r01
     * \param[in] r11
     * \param[in] r21
     * \param[in] r02
     * \param[in] r12
     * \param[in] r22
     */
    RotationMatrix(const double & r00, const double & r10, const double & r20,
                   const double & r01, const double & r11, const double & r21,
                   const double & r02, const double & r12, const double & r22) :
        Details::RotatorABC<RotationMatrix>(9, 6),
        innerData_(Eigen::Matrix3d::Zero())
    {
        innerData_(0, 0) = r00 ;
        innerData_(1, 0) = r10 ;
        innerData_(2, 0) = r20 ;
        innerData_(0, 1) = r01 ;
        innerData_(1, 1) = r11 ;
        innerData_(2, 1) = r21 ;
        innerData_(0, 2) = r02 ;
        innerData_(1, 2) = r12 ;
        innerData_(2, 2) = r22 ;
        Details::CheckRotationMatrix(innerData_) ;
    }

    /*!
     * \brief Constructs from a Eigen::VectorXd
     *
     * see RotationMatrix::RotationMatrix(const double &, const double &, const double &, const double &, const double &, const double &, const double &, const double &, const double &)
     * for the order of the components
     */
    RotationMatrix(const Eigen::VectorXd & unknowns) :
        Details::RotatorABC<RotationMatrix>(9, 6),
        innerData_(Eigen::Matrix3d::Zero())
    {
        assert(unknowns.size() == nUnknowns_) ;
        innerData_(0, 0) = unknowns(0) ;
        innerData_(1, 0) = unknowns(1) ;
        innerData_(2, 0) = unknowns(2) ;
        innerData_(0, 1) = unknowns(3) ;
        innerData_(1, 1) = unknowns(4) ;
        innerData_(2, 1) = unknowns(5) ;
        innerData_(0, 2) = unknowns(6) ;
        innerData_(1, 2) = unknowns(7) ;
        innerData_(2, 2) = unknowns(8) ;
        Details::CheckRotationMatrix(innerData_) ;
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in an RotationMatrix.
     *
     * \param[in] other The other Rotator object.
     *
     */
    RotationMatrix(const ABC & other):
        RotationMatrix(other.toRotationMatrix())
    {
    }

    /*!
     * \brief Transpose the RotationMatrix.
     *
     * Copy and transpose the RotationMatrix components.
     *
     * \return a copy of RotationMatrix object with data transposed
     *
     */
    RotationMatrix transpose(void) const ;

    /*!
     * \brief Compute the determinant of the RotationMatrix.
     *
     * \return a double: the determinant of \c this
     *
     */
    double determinant(void) const ;

    /*!
     * \brief Component getter (\b non \b const reference).
     *
     * \return a double: the required component as \b non \b const reference
     *
     */
    double & operator()(const unsigned & i, const unsigned & j) ;

    /*!
     * \brief Component getter (\b const reference).
     *
     * \return a double: the required component as \b const reference
     *
     */
    const double & operator()(const unsigned & i, const unsigned & j) const ;

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<RotationMatrix>::rotate ;

    void reset() ;

    void copy(const RotationMatrix & other) ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC
    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;
    Eigen::VectorXd unknowns(void) const ;
    void unknowns(const Eigen::VectorXd & unknowns) ;
    Eigen::VectorXd constraints(void) ;
    Eigen::Matrix3d getMatrix(void) const ;
    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;
    Quaternion toQuaternion( void ) const ;
    AxisAndAngle toAxisAndAngle(void) const ;
    MRP toMRP(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    RotationVector toRotationVector(void) const ;

    /*!
     * \copydoc BV::Geometry::Rotation::ABC::toEulerAngles(void) const
     */
    template <typename EulerConvention>
    EulerAngles<EulerConvention> toEulerAngles(void) const
    {
        return EulerAngles<EulerConvention>(innerData_) ;
    }

    //// Overload from RotatorABC<T>
    RotationMatrix & operator=(const ABC & other) ;
    RotationMatrix & operator=(const RotationMatrix & other) ;

    /*!
     * \brief RotationMatrix printer.
     *
     */
    std::ostream & print(std::ostream & f) const
    {
        f << innerData_ ;
        return f ;
    }

};

inline std::ostream & operator<<(std::ostream & f, const RotationMatrix & mat)
{
    return mat.print(f) ;
}

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::RotationMatrix>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::ROTATION_MATRIX ;
} ;

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_RotationMatrix_hpp__
