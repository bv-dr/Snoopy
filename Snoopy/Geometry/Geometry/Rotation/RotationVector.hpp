#pragma once

#include "GeometryExport.hpp"

#include <algorithm>
#include <Eigen/Geometry>

#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

class GEOMETRY_API RotationVector : public Details::RotatorABC<RotationVector>
{
private:
    Eigen::Vector3d state_ ;

public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs from a rotation matrix.
     *
     * \param[in] mat an Eigen::Matrix3d matrix
     */
    template<typename Derived>
    RotationVector(const Eigen::MatrixBase<Derived> & mat,
        typename Eigen::internal::enable_if<(!Derived::IsVectorAtCompileTime) && Derived::SizeAtCompileTime==9, Derived*>::type = 0) :
        Details::RotatorABC< RotationVector >(3, 0)
    {
        state_ = RotationMatrix(mat).toRotationVector().getState() ;
    }

    /*!
     * \brief Dummy constructor
     *
     * The RotationVector is initialized to vector (0,0,0).
     */
    RotationVector(void) :
        Details::RotatorABC<RotationVector>(3, 0),
        state_(0., 0., 0.)
    {
    }

    /*!
     * \brief Constructors from RotationVector components.
     */
    RotationVector(const double & s1, const double & s2, const double & s3) :
        Details::RotatorABC<RotationVector>(3, 0),
        state_(s1, s2, s3)
    {
    }

    /*!
     * \brief Constructs from a Eigen::VectorXd
     *
     */
    RotationVector( const Eigen::VectorXd & unknowns ):
        Details::RotatorABC<RotationVector>(3, 0)
    {
        assert(unknowns.size() == nUnknowns_) ;
        state_(0) = unknowns(0) ;
        state_(1) = unknowns(1) ;
        state_(2) = unknowns(2) ;
    }

    /*!
     * \brief Constructs a copy of provided RotationVector.
     *
     * \param[in] other The other RotationVector object to copy.
     */
    RotationVector(const RotationVector & other) :
        Details::RotatorABC<RotationVector>(3, 0),
        state_(other.state_)
    {
    }

    /*!
     * \brief Constructs a copy of provided rotator converted in an RotationVector.
     *
     * \param[in] other The other Rotator object.
     *
     */
    RotationVector(const ABC & other) :
        Details::RotatorABC<RotationVector>(3, 0),
        state_(other.toRotationVector().state_)
    {
    }

    /*!
     * \brief \a theta1 getter (by copy).
     *
     * \return a double: copied \a theta1
     *
     */
    double theta1(void) const ;

    /*!
     * \brief \a theta2 getter (by copy).
     *
     * \return a double: copied \a theta2
     *
     */
    double theta2(void) const ;

    /*!
     * \brief \a theta3 getter (by copy).
     *
     * \return a double: copied \a theta3
     *
     */
    double theta3(void) const ;

    /*!
     * \brief \a theta1 getter (by reference).
     *
     * \return a double: a reference to \a theta1
     *
     */
    double & theta1(void) ;

    /*!
     * \brief \a theta2 getter (by reference).
     *
     * \return a double: a reference to \a theta2
     *
     */
    double & theta2(void) ;

    /*!
     * \brief \a theta3 getter (by reference).
     *
     * \return a double: a reference to \a theta3
     *
     */
    double & theta3(void) ;

    /*!
     * \brief Compute the norm of RotationVector vector.
     */
    double norm(void) const ;


    const Eigen::Vector3d & getState() const ;

    /*!
     * \brief Assignment operator.
     *
     * Set \a theta value of other into \c this.
     */
    RotationVector & operator=(const RotationVector & other) ;

    /*!
    * \brief Give the equivalent rotation matrix.
    */
    Eigen::Matrix3d getMatrix(void) const ;

    // === Using to avoid c++ hidden methods when overloading === //
    using Details::RotatorABC<RotationVector>::rotate ;

    // ========= Overloading of abstract methods ======== //
    //// Overload from ABC

    void addOtherRotationAtLeft(const ABC & abc) ;
    void addOtherRotationAtRight(const ABC & abc) ;
    void subtractOtherRotationAtLeft(const ABC & abc) ;
    void subtractOtherRotationAtRight(const ABC & abc) ;
    Eigen::Vector3d rotate(const Eigen::Vector3d & vect) const ;
    void inverse(void) ;
    /*!
    * \brief Returns a vector containing all the RotationVector coordinates (theta1, theta2, theta3).
    * \return The vector with the four RotationVector coordinates (theta1, theta2, theta3)
    */
    Eigen::VectorXd unknowns(void) const ;
    /*!
    * \brief Defines the RotationVector coordinates from a vector.
    * \param[in] unknowns The vector containing the RotationVector coordinates (theta1, theta2, theta3).
    */
    void unknowns(const Eigen::VectorXd & unknowns) ;
    /*!
    * \brief Returns a contraints vector of size 0.
    * \returns The constraints vector of size 0 (no constraint).
    */
    Eigen::VectorXd constraints(void) ;

    void reset() ;

    void copy(const RotationVector & other) ;

    RotationMatrix toRotationMatrix(void) const ;
    BasisVectors toBasisVectors(void) const ;
    Quaternion toQuaternion( void ) const ;
    AxisAndAngle toAxisAndAngle(void) const ;
    RotationVector toRotationVector(void) const ;
    HorizontalPlane toHorizontalPlane(void) const ;
    MRP toMRP(void) const ;


    //// Overload from RotatorABC<T>
    RotationVector & operator=(const ABC & other) ;

} ;

} // end of namespace Rotation

template <>
struct  RotatorTypeToRotatorTypeEnum<BV::Geometry::Rotation::RotationVector>
{
    static const RotatorTypeEnum value = RotatorTypeEnum::ROTATION_VECTOR ;
} ;

} // end of namespace Geometry
} // end of namespace BV
