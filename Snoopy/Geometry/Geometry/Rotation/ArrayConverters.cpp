#include "Geometry/Rotation/ArrayConverters.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {
namespace Details {

// Rotation matrix to others
Eigen::ArrayX4d RotationMatrixToQuaternion(const ArrayX9d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationMatrix rotmat(T(iTime, 0), T(iTime, 1), T(iTime, 2),
            T(iTime, 3), T(iTime, 4), T(iTime, 5),
            T(iTime, 6), T(iTime, 7), T(iTime, 8));
        res.row(iTime) = rotmat.toQuaternion().unknowns();
    }
    return res;
}

ArrayX9d RotationMatrixToBasisVectors(const ArrayX9d & T)
{
    return T;
}

Eigen::ArrayX4d RotationMatrixToAxisAndAngle(const ArrayX9d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationMatrix rotmat(T(iTime, 0), T(iTime, 1), T(iTime, 2),
            T(iTime, 3), T(iTime, 4), T(iTime, 5),
            T(iTime, 6), T(iTime, 7), T(iTime, 8));
        res.row(iTime) = rotmat.toAxisAndAngle().unknowns();
    }
    return res;
}

Eigen::ArrayX3d RotationMatrixToMRP(const ArrayX9d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationMatrix rotmat(T(iTime, 0), T(iTime, 1), T(iTime, 2),
            T(iTime, 3), T(iTime, 4), T(iTime, 5),
            T(iTime, 6), T(iTime, 7), T(iTime, 8));
        res.row(iTime) = rotmat.toMRP().unknowns();
    }
    return res;
}

Eigen::ArrayX3d RotationMatrixToRotationVector(const ArrayX9d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationMatrix rotmat(T(iTime, 0), T(iTime, 1), T(iTime, 2),
            T(iTime, 3), T(iTime, 4), T(iTime, 5),
            T(iTime, 6), T(iTime, 7), T(iTime, 8));
        res.row(iTime) = rotmat.toRotationVector().unknowns();
    }
    return res;
}


template <typename EulerConv>
Eigen::ArrayX3d RotationMatrixToEulerAngle(const ArrayX9d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationMatrix rotmat(T(iTime, 0), T(iTime, 1), T(iTime, 2),
            T(iTime, 3), T(iTime, 4), T(iTime, 5),
            T(iTime, 6), T(iTime, 7), T(iTime, 8));
        res.row(iTime) = rotmat.toEulerAngles<EulerConv>().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>(const ArrayX9d & T);

// BasisVectorss to others
Eigen::ArrayX4d BasisVectorsToQuaternion(const ArrayX9d & T)
{
    return RotationMatrixToQuaternion(T);
}

ArrayX9d BasisVectorsToRotationMatrix(const ArrayX9d & T)
{
    return T;
}

Eigen::ArrayX4d BasisVectorsToAxisAndAngle(const ArrayX9d & T)
{
    return RotationMatrixToAxisAndAngle(T);
}

Eigen::ArrayX3d BasisVectorsToMRP(const ArrayX9d & T)
{
    return RotationMatrixToMRP(T);
}

Eigen::ArrayX3d BasisVectorsToRotationVector(const ArrayX9d & T)
{
    return RotationMatrixToRotationVector(T);
}

template <typename EulerConv>
Eigen::ArrayX3d BasisVectorsToEulerAngle(const ArrayX9d & T)
{
    return RotationMatrixToEulerAngle<EulerConv>(T);
}

template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>(const ArrayX9d & T);
template GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>(const ArrayX9d & T);

// Axis and angle to others
ArrayX9d AxisAndAngleToRotationMatrix(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    ArrayX9d res(ArrayX9d::Zero(nTimes, 9));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        AxisAndAngle aa(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)), T(iTime, 3));
        res.row(iTime) = aa.toRotationMatrix().unknowns();
    }
    return res;
}

ArrayX9d AxisAndAngleToBasisVectors(const Eigen::ArrayX4d & T)
{
    return AxisAndAngleToRotationMatrix(T);
}

Eigen::ArrayX4d AxisAndAngleToQuaternion(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        AxisAndAngle aa(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)), T(iTime, 3));
        res.row(iTime) = aa.toQuaternion().unknowns();
    }
    return res;
}

Eigen::ArrayX3d AxisAndAngleToMRP(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        AxisAndAngle aa(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)), T(iTime, 3));
        res.row(iTime) = aa.toMRP().unknowns();
    }
    return res;
}

Eigen::ArrayX3d AxisAndAngleToRotationVector(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        AxisAndAngle aa(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)), T(iTime, 3));
        res.row(iTime) = aa.toRotationVector().unknowns();
    }
    return res;
}

template <typename EulerConv>
Eigen::ArrayX3d AxisAndAngleToEulerAngle(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        AxisAndAngle aa(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)), T(iTime, 3));
        res.row(iTime) = aa.toEulerAngles<EulerConv>().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX4d & T);

// Quaternion to others
ArrayX9d QuaternionToRotationMatrix(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    ArrayX9d res(ArrayX9d::Zero(nTimes, 9));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        Quaternion q(T(iTime, 0), T(iTime, 1), T(iTime, 2), T(iTime, 3));
        res.row(iTime) = q.toRotationMatrix().unknowns();
    }
    return res;
}

ArrayX9d QuaternionToBasisVectors(const Eigen::ArrayX4d & T)
{
    return QuaternionToRotationMatrix(T);
}

Eigen::ArrayX4d QuaternionToAxisAndAngle(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        Quaternion q(T(iTime, 0), T(iTime, 1), T(iTime, 2), T(iTime, 3));
        res.row(iTime) = q.toAxisAndAngle().unknowns();
    }
    return res;
}

Eigen::ArrayX3d QuaternionToMRP(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        Quaternion q(T(iTime, 0), T(iTime, 1), T(iTime, 2), T(iTime, 3));
        res.row(iTime) = q.toMRP().unknowns();
    }
    return res;
}

Eigen::ArrayX3d QuaternionToRotationVector(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));
    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        Quaternion q(T(iTime, 0), T(iTime, 1), T(iTime, 2), T(iTime, 3));
        res.row(iTime) = q.toRotationVector().unknowns();
    }
    return res;
}

template <typename EulerConv>
Eigen::ArrayX3d QuaternionToEulerAngle(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        Quaternion q(T(iTime, 0), T(iTime, 1), T(iTime, 2), T(iTime, 3));
        res.row(iTime) = q.toEulerAngles<EulerConv>().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX4d & T);
template GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX4d & T);

// MRP to others
ArrayX9d MRPToRotationMatrix(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    ArrayX9d res(ArrayX9d::Zero(nTimes, 9));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        MRP mrp(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = mrp.toRotationMatrix().unknowns();
    }
    return res;
}

ArrayX9d MRPToBasisVectors(const Eigen::ArrayX4d & T)
{
    return MRPToRotationMatrix(T);
}

Eigen::ArrayX4d MRPToAxisAndAngle(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        MRP mrp(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = mrp.toAxisAndAngle().unknowns();
    }
    return res;
}

Eigen::ArrayX4d MRPToQuaternion(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        MRP mrp(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = mrp.toQuaternion().unknowns();
    }
    return res;
}

Eigen::ArrayX3d MRPToRotationVector(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        MRP mrp(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = mrp.toRotationVector().unknowns();
    }
    return res;
}

template <typename EulerConv>
Eigen::ArrayX3d MRPToEulerAngle(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        MRP mrp(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = mrp.toEulerAngles<EulerConv>().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);

// RotationVector to others
ArrayX9d RotationVectorToRotationMatrix(const Eigen::ArrayX4d & T)
{
    Eigen::Index nTimes(T.rows());
    ArrayX9d res(ArrayX9d::Zero(nTimes, 9));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationVector rtv(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = rtv.toRotationMatrix().unknowns();
    }
    return res;
}
ArrayX9d RotationVectorToBasisVectors(const Eigen::ArrayX4d & T)
{
    return RotationVectorToRotationMatrix(T);
}

Eigen::ArrayX4d RotationVectorToAxisAndAngle(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationVector rtv(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = rtv.toAxisAndAngle().unknowns();
    }
    return res;
}

Eigen::ArrayX4d RotationVectorToQuaternion(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationVector rtv(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = rtv.toQuaternion().unknowns();
    }
    return res;
}

Eigen::ArrayX3d RotationVectorToMRP(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationVector rtv(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = rtv.toMRP().unknowns();
    }
    return res;
}

template <typename EulerConv>
Eigen::ArrayX3d RotationVectorToEulerAngle(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        RotationVector rtv(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = rtv.toEulerAngles<EulerConv>().unknowns();
    }
    return res;
}
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);



// EulerAngles to others
Eigen::ArrayX3d EulerAngleXYZeToEulerSmallAngleContinuity(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerAnglesConvention_XYZ_e> rot(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)));
        rot.setSmallAnglesContinuity() ;
        res.row(iTime) = rot.unknowns() ;
    }
    return res;
}
Eigen::ArrayX3d EulerAngleXYZiToEulerSmallAngleContinuity(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerAnglesConvention_XYZ_i> rot(Eigen::Vector3d(T(iTime, 0), T(iTime, 1), T(iTime, 2)));
        rot.setSmallAnglesContinuity() ;
        res.row(iTime) = rot.unknowns() ;
    }
    return res;
}


template <typename EulerConvIn>
ArrayX9d EulerAngleToRotationMatrix(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    ArrayX9d res(ArrayX9d::Zero(nTimes, 9));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerConvIn> ea(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = ea.toRotationMatrix().unknowns();
    }
    return res;
}

template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);

template <typename EulerConvIn>
Eigen::ArrayX4d EulerAngleToAxisAndAngle(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerConvIn> ea(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = ea.toAxisAndAngle().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);

template <typename EulerConvIn>
Eigen::ArrayX4d EulerAngleToQuaternion(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX4d res(Eigen::ArrayX4d::Zero(nTimes, 4));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerConvIn> ea(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = ea.toQuaternion().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);

template <typename EulerConvIn>
Eigen::ArrayX3d EulerAngleToMRP(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerConvIn> ea(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = ea.toMRP().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);


template <typename EulerConvIn>
Eigen::ArrayX3d EulerAngleToRotationVector(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

    //#pragma omp parallel for
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerConvIn> ea(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = ea.toRotationVector().unknowns();
    }
    return res;
}

template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XYZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XZY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YXZ_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YZX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZXY_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZYX_i>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XYX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XYZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_XZY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YXZ_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_YZX_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZXY_e>(const Eigen::ArrayX3d & T);
template GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector<Rotation::EulerAnglesConvention_ZYX_e>(const Eigen::ArrayX3d & T);

} // end of namespace Details
} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
