#pragma once
#ifndef __BV_Geometry_Rotation_Tools_hpp__
#define __BV_Geometry_Rotation_Tools_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Dense>

namespace BV {
namespace Geometry {
namespace Rotation {
namespace Details {

GEOMETRY_API void CheckRotationMatrix(const Eigen::Matrix3d & R) ;
GEOMETRY_API double AngleMinusPi_Pi(double angle) ;

} // end of namespace Details
} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Rotation_Tools_hpp__
