#include <Eigen/Geometry>
#include <iostream>
#include <unsupported/Eigen/MatrixFunctions>

#include "Geometry/Exceptions.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {

RotationMatrix RotationMatrix::transpose(void) const
{
    return RotationMatrix(innerData_.transpose()) ;
}

double RotationMatrix::determinant(void) const
{
    return innerData_.determinant() ;
}

 double & RotationMatrix::operator()(const unsigned & i, const unsigned & j)
 {
     return innerData_(i, j) ;
 }

const double & RotationMatrix::operator()(const unsigned & i, const unsigned & j) const
{
    return innerData_(i, j) ;
}

void RotationMatrix::addOtherRotationAtLeft(const ABC & abc)
{
    innerData_ = abc.getMatrix() * innerData_ ;
}

void RotationMatrix::addOtherRotationAtRight(const ABC & abc)
{
    innerData_ *= abc.getMatrix() ;
}

void RotationMatrix::subtractOtherRotationAtLeft(const ABC & abc)
{
    innerData_ = abc.getMatrix().transpose() * innerData_ ;
}

void RotationMatrix::subtractOtherRotationAtRight(const ABC & abc)
{
    innerData_ *= abc.getMatrix().transpose() ;
}

Eigen::Vector3d RotationMatrix::rotate(const Eigen::Vector3d & vect) const
{
    return this->innerData_ * vect ;
}

void RotationMatrix::inverse(void)
{
    Eigen::Matrix3d trans(innerData_.transpose()) ;
    innerData_ = trans ;
}

Eigen::VectorXd RotationMatrix::unknowns( void ) const
{
    unknowns_(0) = innerData_(0, 0) ;
    unknowns_(1) = innerData_(1, 0) ;
    unknowns_(2) = innerData_(2, 0) ;
    unknowns_(3) = innerData_(0, 1) ;
    unknowns_(4) = innerData_(1, 1) ;
    unknowns_(5) = innerData_(2, 1) ;
    unknowns_(6) = innerData_(0, 2) ;
    unknowns_(7) = innerData_(1, 2) ;
    unknowns_(8) = innerData_(2, 2) ;
    return unknowns_ ;
}

void RotationMatrix::unknowns( const Eigen::VectorXd & unknowns )
{
    // FIXME: is it right for python wrapping?
    // when doing this with EulerAngles, seems not to work...
    innerData_(0, 0) = unknowns(0) ;
    innerData_(1, 0) = unknowns(1) ;
    innerData_(2, 0) = unknowns(2) ;
    innerData_(0, 1) = unknowns(3) ;
    innerData_(1, 1) = unknowns(4) ;
    innerData_(2, 1) = unknowns(5) ;
    innerData_(0, 2) = unknowns(6) ;
    innerData_(1, 2) = unknowns(7) ;
    innerData_(2, 2) = unknowns(8) ;
}

Eigen::VectorXd RotationMatrix::constraints( void )
{
    Vector d1(ABC::d1()) ;
    Vector d2(ABC::d2()) ;
    Vector d3(ABC::d3()) ;
    constraints_(0) = d1 * d1 - 1. ;
    constraints_(1) = d1 * d2 ;
    constraints_(2) = d1 * d3 ;
    constraints_(3) = d2 * d2 - 1. ;
    constraints_(4) = d2 * d3 ;
    constraints_(5) = d3 * d3 - 1. ;
    return constraints_ ;
}

void RotationMatrix::reset()
{
    innerData_(0, 0) = 1. ;
    innerData_(1, 0) = 0. ;
    innerData_(2, 0) = 0. ;
    innerData_(0, 1) = 0. ;
    innerData_(1, 1) = 1. ;
    innerData_(2, 1) = 0. ;
    innerData_(0, 2) = 0. ;
    innerData_(1, 2) = 0. ;
    innerData_(2, 2) = 1. ;
}

void RotationMatrix::copy(const RotationMatrix & other)
{
    innerData_(0, 0) = other.innerData_(0, 0) ;
    innerData_(1, 0) = other.innerData_(1, 0) ;
    innerData_(2, 0) = other.innerData_(2, 0) ;
    innerData_(0, 1) = other.innerData_(0, 1) ;
    innerData_(1, 1) = other.innerData_(1, 1) ;
    innerData_(2, 1) = other.innerData_(2, 1) ;
    innerData_(0, 2) = other.innerData_(0, 2) ;
    innerData_(1, 2) = other.innerData_(1, 2) ;
    innerData_(2, 2) = other.innerData_(2, 2) ;
}

RotationMatrix & RotationMatrix::operator=(const ABC & other)
{
    innerData_ = other.getMatrix() ;
    return *this ;
}

RotationMatrix & RotationMatrix::operator=(const RotationMatrix & other)
{
    innerData_ = other.getMatrix() ;
    return *this ;
}

Eigen::Matrix3d RotationMatrix::getMatrix(void) const
{
    return innerData_ ;
}

RotationMatrix RotationMatrix::toRotationMatrix(void) const
{
    return *this ;
}

BasisVectors RotationMatrix::toBasisVectors(void) const
{
    return BasisVectors(d1(), d2(), d3()) ;
}

Quaternion RotationMatrix::toQuaternion( void ) const
{
    Eigen::Quaternion<double> q(this->innerData_) ;
    return Quaternion(q.w(), q.x(), q.y(), q.z()) ;
}

AxisAndAngle RotationMatrix::toAxisAndAngle(void) const
{
    Eigen::AngleAxis<double> aa(this->innerData_) ;
    return AxisAndAngle(Vector(aa.axis()), aa.angle()) ;
}

MRP RotationMatrix::toMRP(void) const
{
    return toQuaternion().toMRP() ;
}

RotationVector RotationMatrix::toRotationVector(void) const
{
    Eigen::AngleAxis<double> aa(this->innerData_) ;
    return AxisAndAngle(Vector(aa.axis()), aa.angle()).toRotationVector() ;
}

HorizontalPlane RotationMatrix::toHorizontalPlane(void) const
{
    return HorizontalPlane(getMatrix()) ;
}

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
