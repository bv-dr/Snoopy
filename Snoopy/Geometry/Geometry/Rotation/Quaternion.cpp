/*!
 * \file Quaternion.cpp
 */

#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

namespace BV {
namespace Geometry {
namespace Rotation {


void Quaternion::normalise( void )
{
    innerData_.normalize() ;
}

double Quaternion::norm(void) const
{
    return innerData_.norm() ;
}

Quaternion Quaternion::normalised(void) const
{
    Details::EigenQuaternion tmp(innerData_.normalized()) ;
    return Quaternion(tmp.w(), tmp.x(), tmp.y(), tmp.z()) ;
}

Quaternion Quaternion::slerp(const double & t, const Quaternion & q) const
{
    Details::EigenQuaternion qInterp(innerData_.slerp(t, q.innerData_)) ;
    return Quaternion(qInterp.w(), qInterp.x(), qInterp.y(), qInterp.z()) ;
}

void Quaternion::addOtherRotationAtLeft(const ABC & abc)
{
    Quaternion otherQuat(abc.toQuaternion()) ;
    innerData_ = otherQuat.innerData_ * innerData_ ;
}

void Quaternion::addOtherRotationAtLeft(const Quaternion & quat)
{
    innerData_ = quat.innerData_ * innerData_ ;
}

void Quaternion::addOtherRotationAtRight(const ABC & abc)
{
    Quaternion otherQuat(abc.toQuaternion()) ;
    innerData_ *= otherQuat.innerData_ ;
}

void Quaternion::addOtherRotationAtRight(const Quaternion & quat)
{
    innerData_ *= quat.innerData_ ;
}

void Quaternion::subtractOtherRotationAtLeft(const ABC & abc)
{
    Quaternion otherQuat(abc.toQuaternion()) ;
    innerData_ = otherQuat.innerData_.inverse() * innerData_ ;
}

void Quaternion::subtractOtherRotationAtLeft(const Quaternion & quat)
{
    innerData_ = quat.innerData_.inverse() * innerData_ ;
}

void Quaternion::subtractOtherRotationAtRight(const ABC & abc)
{
    Quaternion otherQuat(abc.toQuaternion()) ;
    innerData_ *= otherQuat.innerData_.inverse() ;
}

void Quaternion::subtractOtherRotationAtRight(const Quaternion & quat)
{
    innerData_ *= quat.innerData_.inverse() ;
}

Eigen::Vector3d Quaternion::rotate(const Eigen::Vector3d & vect) const
{
    return innerData_ * vect ;
}

void Quaternion::inverse(void)
{
    innerData_ = innerData_.inverse() ;
}

/*!
* \brief Returns a vector containing all the quaternion coordinates (w,x,y,z).
* \return The vector with the four quaternion coordinates  (w,x,y,z)
*/
Eigen::VectorXd Quaternion::unknowns( void ) const
{
    unknowns_(0) = w() ;
    unknowns_(1) = x() ;
    unknowns_(2) = y() ;
    unknowns_(3) = z() ;
    return unknowns_ ;
}

/*!
* \brief Defines the quaternion coordinates from a vector.
* \param[in] unknowns The vector containing the quaternion coordinates  (w,x,y,z).
*/
void Quaternion::unknowns( const Eigen::VectorXd & unknowns )
{
    assert( unknowns.size() == nUnknowns() ) ;
    w() = unknowns( 0 ) ;
    x() = unknowns( 1 ) ;
    y() = unknowns( 2 ) ;
    z() = unknowns( 3 ) ;
}

/*!
* \brief Returns a contraints vector of size 1.
* \returns The constraints vector of size 1: q.norm()-1..
*/
Eigen::VectorXd Quaternion::constraints( void )
{
    constraints_(0) = norm() - 1. ;
    return constraints_ ;
}

void Quaternion::reset()
{
    innerData_.w() = 1. ;
    innerData_.x() = 0. ;
    innerData_.y() = 0. ;
    innerData_.z() = 0. ;
}

void Quaternion::copy(const Quaternion & other)
{
    innerData_.w() = other.innerData_.w() ;
    innerData_.x() = other.innerData_.x() ;
    innerData_.y() = other.innerData_.y() ;
    innerData_.z() = other.innerData_.z() ;
}

RotationMatrix Quaternion::toRotationMatrix(void) const
{
    return RotationMatrix(innerData_.toRotationMatrix()) ;
}

BasisVectors Quaternion::toBasisVectors(void) const
{
    return BasisVectors(d1(), d2(), d3()) ;
}

Quaternion Quaternion::toQuaternion( void ) const
{
    return *this ;
}

AxisAndAngle Quaternion::toAxisAndAngle( void ) const
{
    Eigen::AngleAxis<double> aa(innerData_) ;
    return AxisAndAngle(Vector(aa.axis()), aa.angle()) ;
}

MRP Quaternion::toMRP(void) const
{
    double factor(1.) ;
    if (w() < 0.)
    {
        factor = -1. ;
    }
    double den(1 + factor * w()) ;
    double sigma1(factor * x()/den) ;
    double sigma2(factor * y()/den) ;
    double sigma3(factor * z()/den) ;
    return MRP(sigma1, sigma2, sigma3) ;
}

HorizontalPlane Quaternion::toHorizontalPlane(void) const
{
    return HorizontalPlane(getMatrix()) ;
}

RotationVector Quaternion::toRotationVector(void) const 
{
    Eigen::AngleAxis<double> aa(innerData_) ;
    return RotationVector(aa.axis()*aa.angle()) ;
}

Quaternion & Quaternion::operator=(const ABC & other)
{
    innerData_ = other.toQuaternion().innerData_ ;
    return *this ;
}

Quaternion & Quaternion::operator=(const Quaternion & other)
{
    innerData_ = other.innerData_ ;
    return *this ;
}

Eigen::Matrix3d Quaternion::getMatrix(void) const
{
    return innerData_.toRotationMatrix() ;
}

} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
