#pragma once

#include "GeometryExport.hpp"
#include "Geometry/Exceptions.hpp"
#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/RotationVector.hpp"

#include <unsupported/Eigen/MatrixFunctions>
#include <Eigen/Dense>

namespace BV {
namespace Geometry {
namespace Rotation {
namespace Details {

typedef Eigen::Array<double, Eigen::Dynamic, 9> ArrayX9d;

// Rotation matrix to others
GEOMETRY_API Eigen::ArrayX4d RotationMatrixToQuaternion(const ArrayX9d & T);
GEOMETRY_API ArrayX9d RotationMatrixToBasisVectors(const ArrayX9d & T);
GEOMETRY_API Eigen::ArrayX4d RotationMatrixToAxisAndAngle(const ArrayX9d & T);
GEOMETRY_API Eigen::ArrayX3d RotationMatrixToMRP(const ArrayX9d & T);
GEOMETRY_API Eigen::ArrayX3d RotationMatrixToRotationVector(const ArrayX9d & T);
template <typename EulerConv>
GEOMETRY_API Eigen::ArrayX3d RotationMatrixToEulerAngle(const ArrayX9d & T);

// BasisVectors to others
GEOMETRY_API Eigen::ArrayX4d BasisVectorsToQuaternion(const ArrayX9d & T);
GEOMETRY_API ArrayX9d BasisVectorsToRotationMatrix(const ArrayX9d & T);
GEOMETRY_API Eigen::ArrayX4d BasisVectorsToAxisAndAngle(const ArrayX9d & T);
GEOMETRY_API Eigen::ArrayX3d BasisVectorsToMRP(const ArrayX9d & T);
GEOMETRY_API Eigen::ArrayX3d BasisVectorsToRotationVector(const ArrayX9d & T);
template <typename EulerConv>
GEOMETRY_API Eigen::ArrayX3d BasisVectorsToEulerAngle(const ArrayX9d & T);

// Axis and angle to others
GEOMETRY_API ArrayX9d AxisAndAngleToRotationMatrix(const Eigen::ArrayX4d & T);
GEOMETRY_API ArrayX9d AxisAndAngleToBasisVectors(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX4d AxisAndAngleToQuaternion(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToMRP(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToRotationVector(const Eigen::ArrayX4d & T);
template <typename EulerConv>
GEOMETRY_API Eigen::ArrayX3d AxisAndAngleToEulerAngle(const Eigen::ArrayX4d & T);

// Quaternion to others
GEOMETRY_API ArrayX9d QuaternionToRotationMatrix(const Eigen::ArrayX4d & T);
GEOMETRY_API ArrayX9d QuaternionToBasisVectors(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX4d QuaternionToAxisAndAngle(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX3d QuaternionToMRP(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX3d QuaternionToRotationVector(const Eigen::ArrayX4d & T);
template <typename EulerConv>
GEOMETRY_API Eigen::ArrayX3d QuaternionToEulerAngle(const Eigen::ArrayX4d & T);

// MRP to others
GEOMETRY_API ArrayX9d MRPToRotationMatrix(const Eigen::ArrayX4d & T);
GEOMETRY_API ArrayX9d MRPToBasisVectors(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX4d MRPToAxisAndAngle(const Eigen::ArrayX3d & T);
GEOMETRY_API Eigen::ArrayX4d MRPToQuaternion(const Eigen::ArrayX3d & T);
GEOMETRY_API Eigen::ArrayX3d MRPToRotationVector(const Eigen::ArrayX3d & T);
template <typename EulerConv>
GEOMETRY_API Eigen::ArrayX3d MRPToEulerAngle(const Eigen::ArrayX3d & T);

// RotationVector to others
GEOMETRY_API ArrayX9d RotationVectorToRotationMatrix(const Eigen::ArrayX4d & T);
GEOMETRY_API ArrayX9d RotationVectorToBasisVectors(const Eigen::ArrayX4d & T);
GEOMETRY_API Eigen::ArrayX4d RotationVectorToAxisAndAngle(const Eigen::ArrayX3d & T);
GEOMETRY_API Eigen::ArrayX4d RotationVectorToQuaternion(const Eigen::ArrayX3d & T);
GEOMETRY_API Eigen::ArrayX3d RotationVectorToMRP(const Eigen::ArrayX3d & T) ;
template <typename EulerConv>
GEOMETRY_API Eigen::ArrayX3d RotationVectorToEulerAngle(const Eigen::ArrayX3d & T);

// EulerAngles to others
GEOMETRY_API Eigen::ArrayX3d EulerAngleXYZeToEulerSmallAngleContinuity(const Eigen::ArrayX3d & T);
GEOMETRY_API Eigen::ArrayX3d EulerAngleXYZiToEulerSmallAngleContinuity(const Eigen::ArrayX3d & T);
template <typename EulerConvIn>
GEOMETRY_API ArrayX9d EulerAngleToRotationMatrix(const Eigen::ArrayX3d & T);
template <typename EulerConvIn>
ArrayX9d EulerAngleToBasisVectors(const Eigen::ArrayX3d & T)
{
    return EulerAngleToRotationMatrix<EulerConvIn>(T);
}
template <typename EulerConvIn>
GEOMETRY_API Eigen::ArrayX4d EulerAngleToAxisAndAngle(const Eigen::ArrayX3d & T);
template <typename EulerConvIn>
GEOMETRY_API Eigen::ArrayX4d EulerAngleToQuaternion(const Eigen::ArrayX3d & T);
template <typename EulerConvIn>
GEOMETRY_API Eigen::ArrayX3d EulerAngleToMRP(const Eigen::ArrayX3d & T);
template <typename EulerConvIn>
GEOMETRY_API Eigen::ArrayX3d EulerAngleToRotationVector(const Eigen::ArrayX3d & T);
template <typename EulerConvIn, typename EulerConvOut>
Eigen::ArrayX3d EulerAngleToEulerAngle(const Eigen::ArrayX3d & T)
{
    Eigen::Index nTimes(T.rows());
    Eigen::ArrayX3d res(Eigen::ArrayX3d::Zero(nTimes, 3));

//TODO: move the function to ArrayConverter.cpp so MSVC is able to compile the code
// with OpenMP active
#if !defined(_MSC_VER) || defined(__INTEL_COMPILER)
    //#pragma omp parallel for
#endif
    for (Eigen::Index iTime = 0; iTime < nTimes; iTime++)
    {
        EulerAngles<EulerConvIn> ea(T(iTime, 0), T(iTime, 1), T(iTime, 2));
        res.row(iTime) = ea.template toEulerAngles<EulerConvOut>().unknowns();
    }
    return res;
}


} // end of namespace Details
} // end of namespace Rotation
} // end of namespace Geometry
} // end of namespace BV
