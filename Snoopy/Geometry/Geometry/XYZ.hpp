/*!
 * \file XYZ.hpp
 * \brief Implementing the XYZ class.
 *
 * \author Thomas Gazzola
 */

#pragma once
#ifndef __BV_Geometry_XYZ_hpp__
#define __BV_Geometry_XYZ_hpp__

#include <Eigen/Dense>
#include "Geometry/Tools.hpp"

#ifdef BV_HAVE_LIBCONFIGPP
#include "Utils/ConfigFile.hpp"
#endif

namespace BV {
namespace Geometry {

// Forward declaration for conversions
class Point ;
class Vector ;

class XYZABC
{
protected:
    //! The x coordinate
    double x_ ;
    //! The y coordinate
    double y_ ;
    //! The z coordinate
    double z_ ;

public:
    /*!
     * \brief Constructing from coordinates
     * \param[in] x The x coordinate
     * \param[in] y The y coordinate
     * \param[in] z The z coordinate
     */
    XYZABC(const double & x, const double & y, const double & z):
    x_(x), y_(y), z_(z)
    {
        // Left blank
    }

    /*!
     * \brief Constructing from void
     */
    XYZABC(void):
        XYZABC(0., 0., 0.)
    {
        // Left blank
    }

    /*!
      * \brief Constructing from eigen Vector3d
      * \param[in] array The eigen Vector3d containing the XYZ coordinates.
      */
     XYZABC( const Eigen::Vector3d & array ) :
         x_( array(0) ), y_( array(1) ), z_( array(2) )
     {
     }

    #ifdef BV_HAVE_LIBCONFIGPP
    /*!
     * \brief Contruct from config file
     * \param[in] cfg The config file in which
     *                we search for the parameters
     * \param[in] groupName Name of the group in which
     *                      we search for the parameters of this class
     *
     * <tt>
     * groupName:\n
     * {\n
     *     x = 0.0 ;\n
     *     y = 1.0 ;\n
     *     z = 2.0 ;\n
     * } ;\n
     * </tt>
     */
    XYZABC(const BV::Utils::ConfigFile & cfg, const std::string & groupName):
    x_( cfg.lookup<double>(groupName, "x") ),
    y_( cfg.lookup<double>(groupName, "y") ),
    z_( cfg.lookup<double>(groupName, "z") )
    {
        // Left blank
    }
    #endif

    /*!
     * \brief Abstract destructor.
     */
    virtual ~XYZABC( void )
    {
        // Left blank
    }

    /*!
     * \brief Accessing the x coordinate of the point
     * \return The x coordinate
     */
    const double & x(void) const
    {
        return x_ ;
    }

    /*!
     * \copydoc Geometry::Point::x(void)
     */
    double & x(void)
    {
        return x_ ;
    }

    /*!
     * \brief Accessing the y coordinate of the point
     * \return The y coordinate
     */
    const double & y(void) const
    {
        return y_ ;
    }

    /*!
     * \copydoc Geometry::Point::y(void)
     */
    double & y(void)
    {
        return y_ ;
    }

    /*!
     * \brief Accessing the z coordinate of the point
     * \return The z coordinate
     */
    const double & z(void) const
    {
        return z_ ;
    }

    /*!
     * \copydoc Geometry::Point::z(void)
     */
    double & z(void)
    {
        return z_ ;
    }

    /*!
     * \brief Returns the XYZ coordinates as an array
     * \return The XYZ coordinates in an array of type Eigen::Vector3d
     */
    Eigen::Vector3d toArray() const
    {
        Eigen::Vector3d xyzVector ;
        xyzVector(0) = x_ ;
        xyzVector(1) = y_ ;
        xyzVector(2) = z_ ;
        return xyzVector ;
    }

    void fromArray(const Eigen::Vector3d & array)
    {
        x_ = array(0) ;
        y_ = array(1) ;
        z_ = array(2) ;
    }

    /*!
     * \brief Converter to Point
     */
    virtual Point toPoint(void) const = 0 ;

    /*!
     * \brief Converter to Vector
     */
    virtual Vector toVector(void) const = 0 ;
};

/*!
 * \brief A template class, used through CRTP, 
 *        that will factorise some code for Point and Vector classes.
 * \tparam Derived The type of the class that will derive from XYZ, 
 *                 using the CRTP.
 */
template <class Derived>
class XYZ : public XYZABC
{

public:
//#ifndef EIGEN_DONT_VECTORIZE
//    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
//#endif

    /*!
     * \brief Constructing from coordinates
     * \param[in] x The x coordinate
     * \param[in] y The y coordinate
     * \param[in] z The z coordinate
     */
    XYZ(const double & x, const double & y, const double & z):
        XYZABC(x, y, z)
    {
        // Left blank
    }

    /*!
     * \brief Constructing from void
     */
    XYZ(void):
        XYZABC()
    {
        // Left blank
    }

    /*!
     * \brief Constructing from eigen Vector3d
     * \param[in] array The eigen Vector3d containing the XYZ coordinates.
     */
    XYZ( const Eigen::Vector3d & array ) :
        XYZABC(array)
    {
    }

    #ifdef BV_HAVE_LIBCONFIGPP
    /*!
     * \brief Contruct from config file
     * \param[in] cfg The config file in which 
     *                we search for the parameters
     * \param[in] groupName Name of the group in which 
     *                      we search for the parameters of this class
     *
     * <tt>
     * groupName:\n
     * {\n
     *     x = 0.0 ;\n
     *     y = 1.0 ;\n
     *     z = 2.0 ;\n
     * } ;\n
     * </tt>
     */
    XYZ(const BV::Utils::ConfigFile & cfg, const std::string & groupName):
        XYZABC(cfg, groupName)
    {
        // Left blank
    }
    #endif

    /*!
     * \copydoc Geometry::XYZABC::~XYZABC(void)
     */
    virtual ~XYZ(void)
    {
    }

    /*!
     * \brief Comparing 2 XYZ together (equality)
     * \param[in] other The XYZ to compare with
     */
    bool operator==(const Derived & other) const
    {
        // return ((x_ == other.x()) && (y_ == other.y())
        //         && (z_ == other.z()));

        // FIXME: FLS: modified in order to compare double at epsilon
        return Details::IsClose(x_, other.x())
                && Details::IsClose(y_, other.y())
                 && Details::IsClose(z_, other.z()) ;
    }

    /*!
     * \brief Comparing 2 XYZ together (difference)
     * \param[in] other The XYZ to compare with
     */
    bool operator!=(const Derived & other) const
    {
        return !(*this == other);
    }

    /*!
     * \brief Assignation operator
     * \param[in] other to assign to *this
     */
    Derived & operator=(const XYZABC & other)
    {
        x_ = other.x() ;
        y_ = other.y() ;
        z_ = other.z() ;
        return static_cast<Derived &>(*this) ;
    }

    friend Derived operator-(const Derived & d)
    {
        return Derived(-d.x(), -d.y(), -d.z()) ;
    }

} ;

/*!
 * \brief Computes the Euclididian distance between two XYZ.
 * \tparam Derived The derived type for the CRTP
 * \param[in] xyz0 The first data
 * \param[in] xyz1 The second data
 * \returns The distance betweent the two points
 */
template <class Derived>
inline double Distance(const XYZ<Derived> & xyz0, const XYZ<Derived> & xyz1)
{
    const double xDist( xyz0.x() - xyz1.x() ) ;
    const double yDist( xyz0.y() - xyz1.y() ) ;
    const double zDist( xyz0.z() - xyz1.z() ) ;
    return sqrt( xDist*xDist + yDist*yDist + zDist*zDist ) ;
}

/*!
 * \brief Prints a XYZ on a stream
 * \tparam Derived The derived type foir the CRTP
 * \param[in] f The stream to print in
 * \param[in] xyz The data to print
 * \returns The stream with the xyz output
 */
template <class Derived>
inline std::ostream & operator<<(std::ostream & f, const XYZ<Derived> & xyz) 
{ 
    f << "(" << xyz.x() << ", " << xyz.y() << ", " << xyz.z() << ")" ; 

    return f ; 
}

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_XYZ_hpp__
