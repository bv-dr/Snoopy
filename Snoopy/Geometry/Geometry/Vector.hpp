/*!
 * \file Geometry/Vector.hpp
 * \brief Implementing a small 3D vector class
 *
 * \author Thomas Gazzola
 */

#pragma once
#ifndef __BV_Geometry_Vector_hpp__
#define __BV_Geometry_Vector_hpp__

#include "GeometryExport.hpp"

#include "Geometry/XYZ.hpp"
#include "Geometry/Point.hpp"
#include <Eigen/Dense>

namespace BV {
namespace Geometry {

/*!
 * \brief A small 3D vector class
 */
class GEOMETRY_API Vector: public XYZ<Vector>
{
public:
    /*!
     * \copydoc Geometry::Point::Point(const double &, const double &, const double &)
     */
    Vector(const double & x, const double & y, const double & z):
    XYZ<Vector>(x, y, z)
    {
        // Left blank
    }

    /*!
     * \copydoc XYZ::XYZ(void)
     */
    Vector(void) :
        XYZ<Vector>()
    {
        // Left blank
    }

    #ifdef BV_HAVE_LIBCONFIGPP
    /*!
     * \copydoc Geometry::Point::Point(const BV::Utils::ConfigFile &, const std::string &)
     */
    Vector(const BV::Utils::ConfigFile & cfg, const std::string & groupName):
    XYZ<Vector>(cfg, groupName)
    {
        // Left blank
    }
    #endif

    Vector(const Vector & vect):
    XYZ<Vector>( vect.x(), vect.y(), vect.z() )
    {
        // Left blank
    }

    Vector(const Eigen::Vector3d & xyz):
    XYZ<Vector>( xyz )
    {
        // Left blank
    }

    /*!
     * \brief Returns the norm of the vector
     * \returns The norm of the vector
     */
    double norm(void) const ;

    /*!
     * \brief Returns the squared norm of the vector
     * \returns The squared norm of the vector
     */
    double squaredNorm(void) const ;

    /*!
     * \brief Normalise the vector to one by dividing by the current norm.
     */
    void normalise(void) ;

    /*!
     * \brief Returns a normalised copy of this
     */
    Vector normalised( void ) const ;

    /*!
     * \brief Substracting two vectors.
     * \param[in] vector The vector to subtract
     * \returns The vector, subtracted
     */
    Vector & operator-=(const Vector & vector) ;

    /*!
     * \brief Returns the difference between to vectors.
     * \param[in] vector The other vector.
     */
    Vector operator-(const Vector & vector) const ;

    /*!
     * \brief Adding two vectors.
     * \param[in] vector The vector to add
     * \returns The vector, added
     */
    Vector & operator+=(const Vector & vector) ;

    /*!
     * \brief Returns the addition between to vectors.
     * \param[in] vector The other vector.
     */
    Vector operator+(const Vector & vector) const ;

    /*!
     * \brief Computing the cross product between two vectors
     * \param[in] rhs The other vector
     * \returns The cross product
     */
    Vector operator^(const Vector & rhs) const ;

    /*!
     * \brief Computing the scalar product between two vectors
     * \param[in] rhs The other vector
     * \returns The value of the scalar product
     */
    double operator*(const Vector & rhs) const ;
    
    /*!
     * \brief Scaling the Vector quantities
     * \param[in] scale The scale factor
     */
    Vector & operator*=(const double & scale)
    {
        x_ *= scale ;
        y_ *= scale ;
        z_ *= scale ;

        return *this ;
    }

    /*!
     * \copydoc Geometry::Vector::operator*=(const double &)
     */
    friend Vector operator*(const Vector & d, const double & scale)
    {
        // Copy to temporary...
        Vector tmp( d ) ;
        // ... and scale
        tmp *= scale ;

        return tmp ;
    }

    /*!
     * \brief operator/ with double
     *
     * Copy self and divide resultant and moment by the given double.
     *
     * \param[in] double, a scalar
     * \param[out] the copied divided vector
     */
    Vector operator/(const double & k) const
    {
        Vector tmp(*this) ;
        tmp *= (1./k) ;
        return tmp ;
    }

    // Declaring the member function provided.
    // This is due to the CRTP.
    //Vector operator*(const double & scale) const ;

    /*!
     * \brief Converter to Point
     */
    Point toPoint(void) const ;

    /*!
     * \brief Converter to Vector
     */
    Vector toVector(void) const ;
} ;

inline Vector operator*(const double & scale, const Vector & d)
{
    return d * scale ;
}

} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Vector_hpp__
