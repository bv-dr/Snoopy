/*!
 * \file Geometry/Vector.hpp
 * \brief Implementing a small 3D vector class
 *
 * \author Thomas Gazzola
 */

#include <Eigen/Dense>
#include "Geometry/Vector.hpp"
#include "Geometry/Point.hpp"


namespace BV {
namespace Geometry {

    /*!
     * \brief Returns the norm of the vector
     * \returns The norm of the vector
     */
    double Vector::norm(void) const
    {
        return sqrt( x_*x_ + y_*y_ + z_*z_ ) ; 
    }

    /*!
     * \brief Returns the squared norm of the vector
     * \returns The squared norm of the vector
     */
    double Vector::squaredNorm(void) const
    {
        return this->x_*this->x_ + this->y_*this->y_ + this->z_*this->z_;
    }

    /*!
     * \brief Normalise the vector to one by dividing by the current norm.
     */
    void Vector::normalise(void)
    {
        const double tmp( norm() ) ;

        assert( tmp != 0. ) ;
        
        x_ /= tmp ;
        y_ /= tmp ;
        z_ /= tmp ;
    }

    /*!
     * \brief Returns a normalised copy of this
     */
    Vector Vector::normalised( void ) const
    {
        Vector tmp( *this ) ;
        tmp.normalise() ;
        return tmp ;
    }

    /*!
     * \brief Substracting two vectors.
     * \param[in] vector The vector to subtract
     * \returns The vector, subtracted
     */
    Vector & Vector::operator-=(const Vector & vector)
    {
        x_ -= vector.x() ;
        y_ -= vector.y() ;
        z_ -= vector.z() ;

        return *this ;
    }

    /*!
     * \brief Returns the difference between to vectors.
     * \param[in] vector The other vector.
     */
    Vector Vector::operator-(const Vector & vector) const
    {
        // Copy to temporary...
        Vector tmp( *this ) ;
        // ... and add
        tmp -= vector ;

        return tmp ;
    }

    /*!
     * \brief Adding two vectors.
     * \param[in] vector The vector to add
     * \returns The vector, added
     */
    Vector & Vector::operator+=(const Vector & vector)
    {
        x_ += vector.x() ;
        y_ += vector.y() ;
        z_ += vector.z() ;
        return *this ;
    }

    /*!
     * \brief Returns the addition between to vectors.
     * \param[in] vector The other vector.
     */
    Vector Vector::operator+(const Vector & vector) const
    {
        // Copy to temporary...
        Vector tmp( *this ) ;
        // ... and add
        tmp += vector ;
        return tmp ;
    }

    /*!
     * \brief Computing the cross product between two vectors
     * \param[in] rhs The other vector
     * \returns The cross product
     */
    Vector Vector::operator^(const Vector & rhs) const
    {
        return Vector(y_*rhs.z() - z_*rhs.y(),
                      z_*rhs.x() - x_*rhs.z(), 
                      x_*rhs.y() - y_*rhs.x()) ;
    }

    /*!
     * \brief Computing the scalar product between two vectors
     * \param[in] rhs The other vector
     * \returns The value of the scalar product
     */
    double Vector::operator*(const Vector & rhs) const
    {
        return x_*rhs.x() + y_*rhs.y() + z_*rhs.z() ;
    }
    
    // Declaring the member function provided.
    // This is due to the CRTP.
    //Vector operator*(const double & scale) const ;

    /*!
     * \brief Converter to Point
     */
    Point Vector::toPoint(void) const
    {
        return Point(x(), y(), z()) ;
    }

    /*!
     * \brief Converter to Point
     */
    Vector Vector::toVector(void) const
    {
        return *this ;
    }

} // end of namespace Geometry
} // end of namespace BV
