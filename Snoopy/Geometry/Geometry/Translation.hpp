/*!
 * \file Translation.hpp
 * \brief Implementing the Translation classes to position a body in space.
 */

#pragma once
#ifndef __BV_Geometry_Translation_hpp__
#define __BV_Geometry_Translation_hpp__

#include <memory>

#include "GeometryExport.hpp"

//#include "Tools/AbstractFactory/AbstractFactory.hpp"
#include "Tools/AbstractFactory/Utils.hpp"
#include "Geometry/Translation/ABC.hpp"
#include "Geometry/Translation/Horizontal.hpp"
#include "Geometry/Translation/Cartesian.hpp"
#include "Geometry/Translation/Spherical.hpp"


namespace BV {
namespace Geometry {

enum TranslatorTypeEnum {
           CARTESIAN,
           SPHERICAL_COLAT,
           SPHERICAL_LAT,
           HORIZONTAL,
           UNDEFINED_TRANSLATION
                     } ;

template <int TranslatorTypeEnum>
struct GEOMETRY_API TranslatorTypeEnumToTranslatorType ;

template <>
struct GEOMETRY_API TranslatorTypeEnumToTranslatorType<TranslatorTypeEnum::CARTESIAN>
{
    typedef BV::Geometry::Translation::Cartesian Type ;
} ;

template <>
struct GEOMETRY_API TranslatorTypeEnumToTranslatorType<TranslatorTypeEnum::SPHERICAL_COLAT>
{
    typedef BV::Geometry::Translation::Spherical_CoLat Type ;
} ;

template <>
struct GEOMETRY_API TranslatorTypeEnumToTranslatorType<TranslatorTypeEnum::SPHERICAL_LAT>
{
    typedef BV::Geometry::Translation::Spherical_Lat Type ;
} ;

template <>
struct GEOMETRY_API TranslatorTypeEnumToTranslatorType<TranslatorTypeEnum::HORIZONTAL>
{
    typedef BV::Geometry::Translation::Horizontal Type ;
} ;


template <typename TranslatorType>
struct GEOMETRY_API TranslatorTypeToTranslatorTypeEnum ;

template <>
struct GEOMETRY_API TranslatorTypeToTranslatorTypeEnum<BV::Geometry::Translation::Cartesian>
{
    static const TranslatorTypeEnum value = TranslatorTypeEnum::CARTESIAN ;
} ;

template <>
struct GEOMETRY_API TranslatorTypeToTranslatorTypeEnum<BV::Geometry::Translation::Spherical_CoLat>
{
    static const TranslatorTypeEnum value = TranslatorTypeEnum::SPHERICAL_COLAT ;
} ;

template <>
struct GEOMETRY_API TranslatorTypeToTranslatorTypeEnum<BV::Geometry::Translation::Spherical_Lat>
{
    static const TranslatorTypeEnum value = TranslatorTypeEnum::SPHERICAL_LAT ;
} ;

template <>
struct GEOMETRY_API TranslatorTypeToTranslatorTypeEnum<BV::Geometry::Translation::Horizontal>
{
    static const TranslatorTypeEnum value = TranslatorTypeEnum::HORIZONTAL ;
} ;


/////////////////////
namespace Factories {

//namespace Factory = BV::Tools::AbstractFactory ;
//
//class GEOMETRY_API KeyTranslations
//{
//private:
//    BV::Geometry::TranslatorTypeEnum translatorType_ ;
//public:
//    KeyTranslations(const BV::Geometry::TranslatorTypeEnum & translatorType) ;
//
//    bool operator==(const KeyTranslations & other) ;
//
//    bool operator!=(const KeyTranslations & other) ;
//
//    inline bool operator<(const KeyTranslations & ke) const
//    {
//        return translatorType_ < ke.translatorType_;
//    }
//
//    std::string getName(void) const ;
//
//} ;
//
//GEOMETRY_API std::ostream & operator<<(std::ostream & os, const KeyTranslations & key) ;

class TranslatorsFactory
{
public:
    template <class ... TranslatorArgs>
    static std::shared_ptr<BV::Geometry::Translation::ABC> create(
                                      const BV::Geometry::TranslatorTypeEnum & translatorTypeEnum,
                                      TranslatorArgs &&... translatorArgs
                                                              )
    {
    	using namespace BV::Tools::AbstractFactory ;

        switch (translatorTypeEnum)
        {
        case TranslatorTypeEnum::CARTESIAN:
            return Create<Translation::Cartesian>(std::forward<TranslatorArgs>(translatorArgs)...) ;
        case TranslatorTypeEnum::HORIZONTAL:
            return Create<Translation::Horizontal>(std::forward<TranslatorArgs>(translatorArgs)...) ;
        case TranslatorTypeEnum::SPHERICAL_COLAT:
            return Create<Translation::Spherical<Translation::LatitudeConvention::CO_LATITUDE> >(std::forward<TranslatorArgs>(translatorArgs)...) ;
        case TranslatorTypeEnum::SPHERICAL_LAT:
            return Create<Translation::Spherical<Translation::LatitudeConvention::LATITUDE> >(std::forward<TranslatorArgs>(translatorArgs)...) ;
        case TranslatorTypeEnum::UNDEFINED_TRANSLATION:
            throw "Invalid initialization of translator" ;
        default:
            throw "Invalid initialization of translator" ;
        }
        return Create<Translation::Cartesian>(std::forward<TranslatorArgs>(translatorArgs)...) ;
    }

} ;

} // End of namespace Factories
} // End of namespace Geometry
} // End of namespace BV

#endif //__BV_Geometry_Translation_hpp__
