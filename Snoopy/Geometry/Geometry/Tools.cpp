#include "Geometry/Tools.hpp"
#include "Geometry/Exceptions.hpp"

namespace BV {
namespace Geometry {
namespace Details {

bool IsClose(const double & value, const double & reference,
             const double & epsilon)
{
    return std::abs(value - reference) < epsilon ;
}

} // end of namespace Details
} // end of namespace Geometry
} // end of namespace BV
