/*!
 * \file Rotation.hpp
 * \brief Forwarding all different rotations classes.
 */

#pragma once
#ifndef __BV_Geometry_Rotation_hpp__
#define __BV_Geometry_Rotation_hpp__

#include <type_traits>
#include <memory>
#include <utility>

#include "GeometryExport.hpp"

//#include "Tools/AbstractFactory/AbstractFactory.hpp"
#include "Tools/AbstractFactory/Utils.hpp"
#include "Geometry/Rotation/ABC.hpp"
#include "Geometry/Rotation/RotationMatrix.hpp"
#include "Geometry/Rotation/BasisVectors.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"
#include "Geometry/Rotation/ModifiedRodriguesParameters.hpp"
#include "Geometry/Rotation/HorizontalPlane.hpp"
#include "Geometry/Rotation/RotationVector.hpp"


namespace BV {
namespace Geometry {


namespace Factories {

//namespace Factory = BV::Tools::AbstractFactory ;
//
//class GEOMETRY_API KeyRotations
//{
//private:
//    BV::Geometry::RotatorTypeEnum rotatorType_ ;
//public:
//    KeyRotations(const BV::Geometry::RotatorTypeEnum & rotatorType) ;
//
//    bool operator==(const KeyRotations & other) ;
//
//    bool operator!=(const KeyRotations & other) ;
//
//    inline bool operator<(const KeyRotations & ke) const
//    {
//        return rotatorType_ < ke.rotatorType_ ;
//    }
//
//    std::string getName(void) const ;
//
//} ;
//
//GEOMETRY_API std::ostream & operator<<(std::ostream & os, const KeyRotations & key) ;

class RotatorsFactory
{
public:
    template <class ... RotatorArgs>
    static std::shared_ptr<BV::Geometry::Rotation::ABC> create(
                                      const BV::Geometry::RotatorTypeEnum & rotatorTypeEnum,
                                      RotatorArgs &&... rotatorArgs
                                                              )
    {
        using namespace BV::Tools::AbstractFactory ;

        switch (rotatorTypeEnum)
        {
        case RotatorTypeEnum::AXIS_AND_ANGLE:
            return Create<Rotation::AxisAndAngle>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::BASIS_VECTORS:
            return Create<Rotation::BasisVectors>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XYX_i:
            return Create<Rotation::EulerAngles_XYX_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XYZ_i:
            return Create<Rotation::EulerAngles_XYZ_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XZX_i:
            return Create<Rotation::EulerAngles_XZX_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XZY_i:
            return Create<Rotation::EulerAngles_XZY_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YXY_i:
            return Create<Rotation::EulerAngles_YXY_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YXZ_i:
            return Create<Rotation::EulerAngles_YXZ_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YZX_i:
            return Create<Rotation::EulerAngles_YZX_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YZY_i:
            return Create<Rotation::EulerAngles_YZY_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZXY_i:
            return Create<Rotation::EulerAngles_ZXY_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZXZ_i:
            return Create<Rotation::EulerAngles_ZXZ_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZYX_i:
            return Create<Rotation::EulerAngles_ZYX_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZYZ_i:
            return Create<Rotation::EulerAngles_ZYZ_i>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XYX_e:
            return Create<Rotation::EulerAngles_XYX_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XYZ_e:
            return Create<Rotation::EulerAngles_XYZ_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XZX_e:
            return Create<Rotation::EulerAngles_XZX_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_XZY_e:
            return Create<Rotation::EulerAngles_XZY_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YXY_e:
            return Create<Rotation::EulerAngles_YXY_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YXZ_e:
            return Create<Rotation::EulerAngles_YXZ_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YZX_e:
            return Create<Rotation::EulerAngles_YZX_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_YZY_e:
            return Create<Rotation::EulerAngles_YZY_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZXY_e:
            return Create<Rotation::EulerAngles_ZXY_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZXZ_e:
            return Create<Rotation::EulerAngles_ZXZ_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZYX_e:
            return Create<Rotation::EulerAngles_ZYX_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::EULER_ANGLES_ZYZ_e:
            return Create<Rotation::EulerAngles_ZYZ_e>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::HORIZONTAL_PLANE:
            return Create<Rotation::HorizontalPlane>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS:
            return Create<Rotation::MRP>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::QUATERNION:
            return Create<Rotation::Quaternion>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::ROTATION_MATRIX:
            return Create<Rotation::RotationMatrix>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        case RotatorTypeEnum::UNDEFINED_ROTATION:
            throw "Bad rotation factory entry" ;
        default:
            throw "Bad rotation factory entry" ;
        }
        return Create<Rotation::BasisVectors>(std::forward<RotatorArgs>(rotatorArgs)...) ;
        //return Factory::AbstractFactory<
        //                  BV::Geometry::Rotation::ABC,
        //                  KeyRotations,
        //                  const RotatorArgs &...
        //                               >::create(
        //                 KeyRotations(rotatorTypeEnum), rotatorArgs...
        //                                        ) ;
    }

} ;

} // End of namespace Factories
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Rotation_hpp__
