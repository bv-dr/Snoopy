#include "Geometry/Transform3D.hpp"

namespace BV {
namespace Geometry {

Transform3D::Transform3D(void) :
    transform_(Eigen::Matrix4d::Identity())
{
}

Transform3D::Transform3D(const Translation::ABC & translation,
                         const Rotation::ABC & rotation) :
    Transform3D()
{
    *this *= translation ;
    *this *= rotation ;
}

Transform3D::Transform3D(const Transform3D & other) :
    transform_(other.transform_)
{
}

Transform3D & Transform3D::operator*=(const Transform3D & other)
{
    // First apply translation
    transform_.block<3, 1>(0, 3) += transform_.block<3, 3>(0, 0)
        * other.transform_.block<3, 1>(0, 3) ;
    // Then apply rotation
    transform_.block<3, 3>(0, 0) *= other.transform_.block<3, 3>(0, 0) ;
    return *this ;
}

Transform3D Transform3D::operator*(const Transform3D & other) const
{
    Transform3D tmp(*this) ; // copy
    tmp *= other ;
    return tmp ;
}

Transform3D & Transform3D::operator*=(const Translation::ABC & translator)
{
    Eigen::Vector3d tmp(
        transform_.block<3, 3>(0, 0) * translator.toPoint().toArray()) ;
    transform_.block<3, 1>(0, 3) += tmp ;
    return *this ;
}

Transform3D Transform3D::operator*(const Translation::ABC & translator) const
{
    Transform3D tmp(*this) ; // copy
    tmp *= translator ;
    return tmp ;
}

Transform3D & Transform3D::operator*=(const Rotation::ABC & rotator)
{
    Eigen::Matrix3d rot(transform_.block<3, 3>(0, 0)) ;
    Eigen::Matrix3d otherRot(rotator.toRotationMatrix().getMatrix()) ;
    transform_.block<3, 3>(0, 0) = rot * otherRot ;
    return *this ;
}

Transform3D Transform3D::operator*(const Rotation::ABC & rotator) const
{
    Transform3D tmp(*this) ; // copy
    tmp *= rotator ;
    return tmp ;
}

Geometry::Vector Transform3D::operator*(const Geometry::Vector & vect) const
{
    // local to global !
    Eigen::Vector4d tmp(vect.x(), vect.y(), vect.z(), 0.) ;
    Eigen::Vector4d tmp2(transform_ * tmp) ;
    return Geometry::Vector(tmp2(0), tmp2(1), tmp2(2)) ;
}

Geometry::Point Transform3D::operator*(const Geometry::Point & pt) const
{
    // local to global !
    Eigen::Vector4d tmp(pt.x(), pt.y(), pt.z(), 1.) ;
    Eigen::Vector4d tmp2(transform_ * tmp) ;
    return Geometry::Point(tmp2(0), tmp2(1), tmp2(2)) ;
}

Eigen::Vector4d Transform3D::operator*(const Eigen::Vector4d & vect) const
{
    return transform_ * vect ;
}

Eigen::Matrix4Xd Transform3D::operator*(const Eigen::Matrix4Xd & matrix) const
{
    return transform_ * matrix ;
}

Transform3D Transform3D::inverse(void) const
{
    Transform3D tmp ;
    Eigen::Matrix3d rotT(transform_.block<3, 3>(0, 0).transpose()) ;
    tmp.transform_.block<3, 3>(0, 0) = rotT ;
    Eigen::Vector3d trans(-rotT * transform_.block<3, 1>(0, 3)) ;
    tmp.transform_.block<3, 1>(0, 3) = trans ;
    return tmp ;
}

//Transform3D::EigenMatrix4d Transform3D::getMatrix(void) const
Eigen::Matrix4d Transform3D::getMatrix(void) const
{
    return transform_ ;
}

} // End of namespace Geometry
} // End of namespace BV

