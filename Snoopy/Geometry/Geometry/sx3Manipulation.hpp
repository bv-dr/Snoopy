#pragma once
#ifndef BV_Geometry_sx3Manipulation_hpp
#define BV_Geometry_sx3Manipulation_hpp

#include "GeometryExport.hpp"

#include <Eigen/Dense>

#include "Tools/EigenTypedefs.hpp"
#include "Geometry/Vector.hpp"

namespace BV {
namespace Geometry {

// class which define conversions of so(3) space (anti-symetric matrices)
// so(3) = { S \in \mathcal{R}^{n \times n} : S^T = -S }
GEOMETRY_API Eigen::Matrix3d R3Toso3(const double & u1, const double & u2, const double & u3) ;

GEOMETRY_API Eigen::Matrix3d R3Toso3(const Eigen::Vector3d & u) ;

GEOMETRY_API Eigen::Matrix3d R3Toso3(const Geometry::Vector & u) ;

GEOMETRY_API Eigen::Vector3d so3ToR3(const Eigen::Matrix3d & U) ;

// class which define conversions of se(3) space
// the space se(3) = { (p, R) : p \in \mathcal{R}^3, R \in SO(3) } = R^3 \times SO(3)
// where SO(3) = { R \in \mathcal{R}^{n \times n} : R \cdot R^T = I, det(R) = +1 }
GEOMETRY_API Eigen::Matrix4d R6Tose3(const double & v1,
                        const double & v2,
                        const double & v3,
                        const double & u1,
                        const double & u2,
                        const double & u3) ;

GEOMETRY_API Eigen::Matrix4d R6Tose3(const Eigen::Vector3d & v,
                        const Eigen::Vector3d & u) ;

GEOMETRY_API Eigen::Matrix4d R6Tose3(const Geometry::Vector & v,
                        const Geometry::Vector & u) ;

GEOMETRY_API Eigen::Matrix4d R6Tose3(const Tools::Vector6d & vect) ;

GEOMETRY_API Tools::Vector6d se3ToR6(const Eigen::Matrix4d & ksi) ;

} // End of namespace Geometry
} // End of namespace BV

#endif // BV_Geometry_sx3Manipulation_hpp
