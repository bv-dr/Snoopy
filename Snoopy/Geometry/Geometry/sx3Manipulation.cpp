#include "Geometry/sx3Manipulation.hpp"

namespace BV {
namespace Geometry {

// class which define conversions of so(3) space (anti-symetric matrices)
// so(3) = { S \in \mathcal{R}^{n \times n} : S^T = -S }
Eigen::Matrix3d R3Toso3(const double & u1,
                        const double & u2,
                        const double & u3)
{
    Eigen::Matrix3d U(Eigen::Matrix3d::Zero()) ;
    U(1, 0) =  u3 ;
    U(2, 0) = -u2 ;
    U(2, 1) =  u1 ;

    U(0, 1) = -u3 ;
    U(0, 2) =  u2 ;
    U(1, 2) = -u1 ;
    return U ;
}

Eigen::Matrix3d R3Toso3(const Eigen::Vector3d & u)
{
    return R3Toso3(u(0), u(1), u(2)) ;
}

Eigen::Matrix3d R3Toso3(const Geometry::Vector & u)
{
    return R3Toso3(u.x(), u.y(), u.z()) ;
}

Eigen::Vector3d so3ToR3(const Eigen::Matrix3d & U)
{
    Eigen::Vector3d u(Eigen::Vector3d::Zero()) ;
    u(0) = U(2, 1) ;
    u(1) = U(0, 2) ;
    u(2) = U(1, 0) ;
    return u ;
}

// class which define conversions of se(3) space
// the space se(3) = { (p, R) : p \in \mathcal{R}^3, R \in SO(3) } = R^3 \times SO(3)
// where SO(3) = { R \in \mathcal{R}^{n \times n} : R \cdot R^T = I, det(R) = +1 }
Eigen::Matrix4d R6Tose3(const double & v1,
                        const double & v2,
                        const double & v3,
                        const double & u1,
                        const double & u2,
                        const double & u3)
{
    Eigen::Matrix4d ksi(Eigen::Matrix4d::Zero()) ;
    ksi.topLeftCorner(3, 3) = R3Toso3(u1, u2, u3) ;
    ksi(3, 0) = v1 ;
    ksi(3, 1) = v2 ;
    ksi(3, 2) = v3 ;
    return ksi ;
}

Eigen::Matrix4d R6Tose3(const Eigen::Vector3d & v,
                        const Eigen::Vector3d & u)
{
    return R6Tose3(v(0), v(1), v(2), u(0), u(1), u(2)) ;
}

Eigen::Matrix4d R6Tose3(const Geometry::Vector & v,
                        const Geometry::Vector & u)
{
    return R6Tose3(v.x(), v.y(), v.z(), u.x(), u.y(), u.z()) ;
}

Eigen::Matrix4d R6Tose3(const Tools::Vector6d & vect)
{
    return R6Tose3(vect(0), vect(1), vect(2), vect(3), vect(4), vect(5)) ;
}

Tools::Vector6d se3ToR6(const Eigen::Matrix4d & ksi)
{
    Tools::Vector6d vect(Tools::Vector6d::Zero()) ;
    vect.tail(3) = so3ToR3(ksi.topLeftCorner(3, 3)) ;
    vect.head(3) = ksi.topRightCorner(3, 1) ;
    return vect ;
}

} // End of namespace Geometry
} // End of namespace BV
