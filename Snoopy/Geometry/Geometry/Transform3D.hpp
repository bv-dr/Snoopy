#ifndef __BV_Geometry_Transform3D_hpp__
#define __BV_Geometry_Transform3D_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Dense>

#include "Geometry/Translation.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {

class GEOMETRY_API Transform3D
{
private:
    // FIXME I don't know how to use the align stuff for python wrapping...
    //typedef Eigen::Matrix<double, 4, 4, Eigen::DontAlign> EigenMatrix4d ;
//    EigenMatrix4d transform_ ;
    Eigen::Matrix4d transform_ ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    Transform3D(void) ;
    Transform3D(const Translation::ABC & translation,
                const Rotation::ABC & rotation) ;
    Transform3D(const Transform3D & other) ;

    Transform3D & operator*=(const Transform3D & other) ; 
    Transform3D operator*(const Transform3D & other) const ; 

    Transform3D & operator*=(const Translation::ABC & translator) ;
    Transform3D operator*(const Translation::ABC & translator) const ;

    Transform3D & operator*=(const Rotation::ABC & rotator) ;
    Transform3D operator*(const Rotation::ABC & rotator) const ;

    Geometry::Vector operator*(const Geometry::Vector & vect) const ;
    Geometry::Point operator*(const Geometry::Point & pt) const ;
    Eigen::Vector4d operator*(const Eigen::Vector4d & vect) const ;
    Eigen::Matrix4Xd operator*(const Eigen::Matrix4Xd & matrix) const ;

    Transform3D operator-(void) const ;
    Transform3D inverse(void) const ;

    template <typename Rotator>
    Rotator getRotator(void) const
    {
        Rotation::RotationMatrix rot(transform_.block<3, 3>(0, 0)) ;
        return Rotator(rot) ;
    }

    template <typename Translator>
    Translator getTranslator(void) const
    {
        return Translator(transform_.block<3, 1>(0, 3)) ;
    }

//    EigenMatrix4d getMatrix(void) const ;
    Eigen::Matrix4d getMatrix(void) const ;

} ;

} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Transform3D_hpp__
