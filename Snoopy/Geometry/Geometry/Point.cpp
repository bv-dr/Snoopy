/*!
 * \file Point.hpp
 * \brief Implementing a small 3D point class
 *
 * \author Thomas Gazzola
 */

#include <Eigen/Dense>
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"

namespace BV {
/*!
 * \brief Namespace Geometry is here to include pure geometric objects, 
 *        such as points or vectors.
 */
namespace Geometry {

    /*!
     * \brief Returns a vector representing the difference between two points.
     * \param[in] point The other point.
     */
    Vector Point::operator-(const Point & point) const
    {
        return Vector(this->x_ - point.x(), this->y_ - point.y(),
            this->z_ - point.z()) ;
    }


    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point & Point::operator+=(const Vector & vect)
    {
        this->x_ += vect.x() ;
        this->y_ += vect.y() ;
        this->z_ += vect.z() ;
        return *this ;
    }

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point & Point::operator-=(const Vector & vect)
    {
        this->x_ -= vect.x() ;
        this->y_ -= vect.y() ;
        this->z_ -= vect.z() ;
        return *this ;
    }

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point Point::operator+(const Vector & vect) const
    {
        Point tmp(*this) ;
        return tmp += vect ;
    }

    /*!
     * \brief Returns a point representing the translation of a point with regard to the given vector.
     * \param[in] vector The vector to translate the point.
     */
    Point Point::operator-(const Vector & vect) const
    {
        Point tmp(*this) ;
        return tmp -= vect ;
    }

    /*!
     * \brief Converter to Point
     */
    Point Point::toPoint(void) const
    {
        return *this ;
    }

    /*!
     * \brief Converter to Point
     */
    Vector Point::toVector(void) const
    {
        return Vector(x(), y(), z()) ;
    }


} // end of namespace Geometry
} // end of namespace BV
