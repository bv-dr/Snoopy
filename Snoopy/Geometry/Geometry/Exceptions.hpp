#pragma once
#ifndef BV_Geometry_Exceptions_hpp
#define BV_Geometry_Exceptions_hpp

#include <exception>
#include <string>

namespace BV {
namespace Geometry {
namespace Exceptions {

class RotationInitialisationException: public std::exception
{
private:
    std::string message_ ;
public:
    RotationInitialisationException(std::string message) :
        message_(message)
    {
    }
    const char *what() const throw()
    {
        return message_.c_str() ;
    }
    ~RotationInitialisationException() throw()
    {
    }
    std::string getMessage() const
    {
        return message_ ;
    }
} ;

// class BVException : public std::exception
// {
// protected:
//     std::string message_ ;
// public:
//     BVException(std::string message) : message_(message)
//     {
//     }
//
//     ~BVException() throw()
//     {
//     }
//
//     const char *what() const throw()
//     {
//         return message_.c_str() ;
//     }
//
//     std::string getMessage() const
//     {
//         return message_ ;
//     }
// } ;

// struct RotationInitialisationException: public BVException
// {
//     RotationInitialisationException(std::string message) : BVException(message)
//     {
//     }
// } ;

} // End of namespace Exceptions
} // End of namespace Geometry
} // End of namespace BV

#endif // BV_Geometry_Exceptions_hpp
