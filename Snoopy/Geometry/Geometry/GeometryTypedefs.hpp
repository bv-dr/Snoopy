#pragma once
#ifndef __BV_GeometryTypedefs_hpp__
#define __BV_GeometryTypedefs_hpp__

namespace BV {

namespace Details
{
    const double epsilon(1.e-10) ;
}

} // End of namespace BV

#endif
