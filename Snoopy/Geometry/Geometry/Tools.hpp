#pragma once
#ifndef __BV_Geometry_Tools_hpp__
#define __BV_Geometry_Tools_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Dense>
#include "Geometry/GeometryTypedefs.hpp"

namespace BV {
namespace Geometry {
namespace Details {

GEOMETRY_API bool IsClose(const double & value, const double & reference,
             const double & epsilon = BV::Details::epsilon) ;

} // end of namespace Details
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tools_hpp__
