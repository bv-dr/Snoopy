#pragma once
#ifndef __BV_Geometry_Translation_Cartesian_hpp__
#define __BV_Geometry_Translation_Cartesian_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Geometry>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Translation/ABC.hpp"

namespace BV {
namespace Geometry {
namespace Translation {

/*!
 * \brief Implementation of a class enabling translation of points and vectors
 *        in 3D space.
 */
class GEOMETRY_API Cartesian : public Details::TranslatorABC<Cartesian>
{
private:
    Eigen::Translation3d innerData_ ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs a Cartesian given an initial 3D position.
     * \param[in] x The global x position.
     * \param[in] y The global z position.
     * \param[in] z The global z position.
     */
    Cartesian( const double & x, const double & y, const double & z ) :
        Details::TranslatorABC<Cartesian>(3, 0),
        innerData_(x, y, z)
    {
    }

    /*!
     * \brief Constructs a default Cartesian
     */
    Cartesian(void) : Cartesian(0., 0., 0.)
    {
    }

    explicit Cartesian(const Geometry::Vector & v) :
        Cartesian(v.x(), v.y(), v.z())
    {
    }

    explicit Cartesian(const Geometry::Point & p) :
        Cartesian(p.x(), p.y(), p.z())
    {
    }

    /*!
     * \brief Constructs a Cartesian instance from unknowns.
     * \param[in] unknowns A vector of length three that contains
     *                     the (x, y, z) data.
     */
    //Cartesian(const Eigen::Ref<const Eigen::Vector3d> & unknowns) :
    Cartesian(const Eigen::Vector3d & unknowns) :
        Cartesian(unknowns(0), unknowns(1), unknowns(2))
    {
        assert(unknowns.size() == nUnknowns()) ;
    }

    /*!
     * \brief Constructs a copy of given other Cartesian.
     * \param[in] other The other Cartesian object.
     */
    Cartesian(const Cartesian & other) :
        Details::TranslatorABC<Cartesian>(3, 0),
        innerData_(other.innerData_)
    {
        //setUnknowns_() ;
    }

    Cartesian(const ABC & other) :
        Cartesian(other.toCartesian())
    {
    }

    /*!
     * \brief Applies a 3D translation defined by other Cartesian
     * \param[in] other The other Cartesian object which defines
     *                  the new translation to apply.
     * \return A reference to this after applying the new translation.
     */
    Cartesian & operator*=( const ABC & other ) ;
    Cartesian & operator*=( const Cartesian & other ) ;
    Cartesian & operator+=(const ABC & other) ;
    Cartesian & operator+=(const Cartesian & other) ;
    Cartesian & operator-=(const ABC & other) ;
    Cartesian & operator-=(const Cartesian & other) ;
    Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const ;
    Cartesian & operator=(const Cartesian & other) ;
    Cartesian & operator=(const ABC & other) ;
    using TranslatorABC<Cartesian>::operator* ;

    void inverse(void) ;

    double & x(void) ;
    double x(void) const ;
    double & y(void) ;
    double y(void) const ;
    double & z(void) ;
    double z(void) const ;

    virtual void set( const Geometry::Point & c ) ;
    virtual void set( const Geometry::Vector & c ) ;

    /*!
     * \brief Returns the three unknowns (x, y, z) that defines this translation.
     * \returns The unknowns vector.
     */
    Eigen::VectorXd unknowns(void) const ;

    /*!
     * \brief Defines the Cartesian class from the (x, y, z) values
     *        contained in the unknowns vector.
     * \param[in] unknowns The unknowns vector.
     */
    void unknowns(const Eigen::VectorXd & unknowns) ;

    /*!
     * \brief Returns an empty contraints vector (no constraints).
     * \returns The constraints vector.
     */
    Eigen::VectorXd constraints(void) const ;
    
    Horizontal toHorizontal(void) const ;
    Cartesian toCartesian(void) const ;
    Geometry::Point toPoint(void) const ;
    Geometry::Vector toVector(void) const ;

};

} // end of namespace Translation
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Translation_Cartesian_hpp__
