#pragma once
#ifndef __BV_Geometry_Translation_Spherical_hpp__
#define __BV_Geometry_Translation_Spherical_hpp__

#include <cmath>
#include <Eigen/Geometry>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Translation/ABC.hpp"
#include "Geometry/Translation/Cartesian.hpp"
#include "Geometry/Translation/Horizontal.hpp"

namespace BV {
namespace Geometry {
namespace Translation {

/*!
 * \struct LatitudeConvention
 * \brief Container of latitude convention representation used to parametrize Spherical coordinates.
 */
enum LatitudeConvention
{
    CO_LATITUDE,
    LATITUDE
} ;


/*!
 * \brief Implementation of a class enabling translation of points and vectors
 *        in 3D space.
 */
template <int LatitudeConvEnum>
class Spherical : public Details::TranslatorABC<Spherical<LatitudeConvEnum> >
{
protected:
    double r_ ;
    double theta_ ;
    double phi_ ;

    void switchPhi_(double & phi) const
    {
        switch(LatitudeConvEnum)
        {
            case LatitudeConvention::CO_LATITUDE:
                return ;
            case LatitudeConvention::LATITUDE:
                phi = M_PI/2. - phi ;
                return ;
            default:
                throw "Error in spherical latitude convention." ;
        }
    }

    const double getRFromCartesian_(const Geometry::XYZABC & v) const
    {
        const double & x(v.x()) ;
        const double & y(v.y()) ;
        const double & z(v.z()) ;
        return std::sqrt(x*x+y*y+z*z) ;
    }

    const double getThetaFromCartesian_(const Geometry::XYZABC & v) const
    {
        return std::atan2(v.y(),v.x()) ;
    }

    const double getPhiFromCartesian_(const Geometry::XYZABC & v) const
    {
        const double & x(v.x()) ;
        const double & y(v.y()) ;
        double phi(std::atan2(std::sqrt(x*x + y*y), v.z())) ;
        switchPhi_(phi) ;
        return phi ;
    }

public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif

    /*!
     * \brief Constructs a Cartesian given an initial 3D position.
     * \param[in] r The radius.
     * \param[in] theta The longitude angle.
     * \param[in] phi The (co)latitude angle.
     */
    Spherical( const double & r, const double & theta, const double & phi ) :
        Details::TranslatorABC<Spherical<LatitudeConvEnum> >(3, 0),
        r_( r<0. ? -r : r), theta_(theta), phi_(r<0. ? phi + M_PI : phi)
    {
    }

    /*!
     * \brief Constructs a default Cartesian
     */
    Spherical(void) : Spherical(0., 0., 0.)
    {
    }

    Spherical(const Geometry::Vector & v) :
        Spherical(getRFromCartesian_(v), getThetaFromCartesian_(v), getPhiFromCartesian_(v))
    {
    }

    Spherical(const Geometry::Point & p):
        Spherical(getRFromCartesian_(p), getThetaFromCartesian_(p), getPhiFromCartesian_(p))
    {
    }

    /*!
     * \brief Constructs a Cartesian instance from unknowns.
     * \param[in] unknowns A vector of length three that contains
     *                     the (x, y, z) data.
     */
    Spherical(const Eigen::VectorXd & unknowns) :
        Spherical(unknowns(0), unknowns(1), unknowns(2))
    {
        assert(unknowns.size() == this->nUnknowns()) ;
    }

    /*!
     * \brief Constructs a copy of given other Cartesian.
     * \param[in] other The other Cartesian object.
     */
    Spherical(const Spherical<LatitudeConvEnum> & other) :
        Details::TranslatorABC<Spherical<LatitudeConvEnum> >(3, 0),
        r_(other.r_),
        theta_(other.theta_),
        phi_(other.phi_)
    {
        //setUnknowns_() ;
    }

    Spherical(const ABC & other) :
        Spherical(other.template toSpherical<LatitudeConvEnum>())
    {
    }

    /*!
     * \brief Applies a 3D translation defined by other Cartesian
     * \param[in] other The other Cartesian object which defines
     *                  the new translation to apply.
     * \return A reference to this after applying the new translation.
     */
    Spherical<LatitudeConvEnum> & operator*=( const ABC & other )
    {
        Cartesian c1(toCartesian()) ;
        Cartesian c2(other.toCartesian()) ;
        *this = c1 + c2 ;
        return *this ;
    } ;

    Spherical<LatitudeConvEnum> & operator+=(const ABC & other)
    {
        *this *= other ;
        return *this ;
    }

    Spherical<LatitudeConvEnum> & operator-=(const ABC & other)
    {
        *this *= other.template toSpherical<LatitudeConvEnum>().getInversed() ;
        return *this ;
    }

    Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const
    {
        return toCartesian() * vect ;
    }

    Spherical<LatitudeConvEnum> & operator=(const Spherical<LatitudeConvEnum> & other)
    {
        r() = other.r() ;
        theta() = other.theta() ;
        phi() = other.phi() ;
        return *this ;
    }

    Spherical<LatitudeConvEnum> & operator=(const ABC & other)
    {
        *this = other.template toSpherical<LatitudeConvEnum>() ;
        return *this ;
    }

    using Details::TranslatorABC<Spherical<LatitudeConvEnum> >::operator* ;

    void inverse(void)
    {
        phi_ += M_PI ;
    }

    double & r(void)
    {
        return r_ ;
    }

    double r(void) const
    {
        return r_ ;
    }

    double & theta(void)
    {
        return theta_ ;
    }

    double theta(void) const
    {
        return theta_ ;
    }

    double & phi(void)
    {
        return phi_ ;
    }

    double phi(void) const
    {
        return phi_ ;
    }

    Geometry::Vector uR(void) const
    {
        const double & theta(this->theta()) ;
        double phi(this->phi()) ;
        switchPhi_(phi) ;
        return Vector(std::cos(theta)*std::sin(phi),
                      std::sin(theta)*std::sin(phi),
                      std::cos(phi)) ;
    }

    Geometry::Vector uTheta(void) const
    {
        const double & theta(this->theta()) ;
        return Vector(-std::sin(theta),
                      std::cos(theta),
                      0) ;
    }

    Geometry::Vector uPhi(void) const
    {
        const double & theta(this->theta()) ;
        double phi(this->phi()) ;
        switchPhi_(phi) ;
        // sign for the vector to be directed along phi orientation
        // in the case of CO_LATITUDE, phi is positive (counter clockwise)
        // in the case of LATITUDE, phi is negative (clockwise)
        double sgn(1.) ;
        switch(LatitudeConvEnum)
        {
        case LatitudeConvention::CO_LATITUDE:
            break ;
        case LatitudeConvention:: LATITUDE:
            sgn *= -1;
            break ;
        default:
            throw "Error in spherical latitude convention." ;
        }

        return sgn*Vector(std::cos(theta)*std::cos(phi),
                          std::sin(theta)*std::cos(phi),
                          -std::sin(phi)) ;
    }

    void set( const Geometry::Point & c )
    {
        this->r() = getRFromCartesian_(c) ;
        this->theta() = getThetaFromCartesian_(c) ;
        this->phi() = getPhiFromCartesian_(c) ;
    }

    void set( const Geometry::Vector & c )
    {
        this->r() = getRFromCartesian_(c) ;
        this->theta() = getThetaFromCartesian_(c) ;
        this->phi() = getPhiFromCartesian_(c) ;
    }

    /*!
     * \brief Returns the three unknowns (x, y, z) that defines this translation.
     * \returns The unknowns vector.
     */
    Eigen::VectorXd unknowns(void) const
    {
        this->unknowns_(0) = this->r() ;
        this->unknowns_(1) = this->theta() ;
        this->unknowns_(2) = this->phi() ;
        return this->unknowns_ ;
    }

    /*!
     * \brief Defines the Cartesian class from the (x, y, z) values
     *        contained in the unknowns vector.
     * \param[in] unknowns The unknowns vector.
     */
    void unknowns(const Eigen::VectorXd & unknowns)
    {
        assert(unknowns.size() == this->nUnknowns()) ;
        this->r() = unknowns(0) ;
        this->theta() = unknowns(1) ;
        this->phi() = unknowns(2) ;
    }

    /*!
     * \brief Returns an empty contraints vector (no constraints).
     * \returns The constraints vector.
     */
    Eigen::VectorXd constraints(void) const
    {
        return this->constraints_ ;
    }

    Horizontal toHorizontal(void) const
    {
        Cartesian c(toCartesian()) ;
        return Horizontal(c.x(), c.y(), c.z()) ;
    }

    Cartesian toCartesian(void) const
    {
        return Cartesian(toVector()) ;
    }
    
//    template <>
//    Spherical toSpherical<LatitudeConvEnum>(void) const
//    {
//        return *this ;
//    }

    Geometry::Point toPoint(void) const
    {
        return (this->toVector()).toPoint() ;
    }

    Geometry::Vector toVector(void) const
    {
        return this->r()*(this->uR()) ;
    }

};

/*
SPHERICAL_COLAT
SPHERICAL_LAT
*/
typedef Spherical<LatitudeConvention::CO_LATITUDE> Spherical_CoLat ;
typedef Spherical<LatitudeConvention::LATITUDE> Spherical_Lat ;

} // end of namespace Translation
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Translation_Spherical_hpp__
