#pragma once
#ifndef __BV_Geometry_Translation_Horizontal_hpp__
#define __BV_Geometry_Translation_Horizontal_hpp__

#include "GeometryExport.hpp"

#include <Eigen/Geometry>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"

#include "Geometry/Translation/ABC.hpp"

namespace BV {
namespace Geometry {
namespace Translation {

/*!
 * \brief Implementation of a class enabling translation of 3D points and vectors
 *        in horizontal 2D space.
 */
class GEOMETRY_API Horizontal : public Details::TranslatorABC<Horizontal>
{
private:
    Eigen::Translation3d innerData_ ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif
    
    /*!
     * \brief Constructs a Horizontal instance from a Geometry::Vector.
     * \param[in] vect A geometry vector
     *                     the (x, y) data.
     */
    Horizontal(const Geometry::Vector & vect) :
        Horizontal(vect.x(), vect.y(), vect.z())
    {
    }

    /*!
     * \brief Constructs a Horizontal given an initial 3D position.
     * \param[in] x The global x position.
     * \param[in] y The global z position.
     * \param[in] z The global z position. This position will remain constant
     *              afterwards.
     */
    Horizontal(const double & x, const double & y, const double & z) :
        Details::TranslatorABC<Horizontal>(2, 0), innerData_(x, y, z)
    {
    }

    /*!
     * \brief Constructs a default Horizontal
     */
    Horizontal(void) : Horizontal(0., 0., 0.)
    {
    }

    /*!
     * \brief Constructs a Horizontal instance from unknowns.
     * \param[in] unknowns A vector of length two that contains
     *                     the (x, y) data.
     */
    Horizontal(const Eigen::VectorXd & unknowns) :
        Horizontal(unknowns(0), unknowns(1), 0.)
    {
        assert( unknowns.size() == nUnknowns() ) ;
    }

    /*!
     * \brief Constructs a copy of given other Horizontal.
     * \param[in] other The other Horizontal object.
     */
    Horizontal(const Horizontal & other) :
        Details::TranslatorABC<Horizontal>(2, 0),
        innerData_(other.innerData_)
    {
    }

    Horizontal(const ABC & other) :
        Horizontal(other.toHorizontal())
    {
    }

    /*!
     * \brief Applies a 2D translation defined by other Translation2D
     * \param[in] other The other Translation2D object which defines
     *                  the new translation to apply.
     * \return A reference to this after applying the new translation.
     */
    Horizontal & operator*=(const ABC & other) ;
    Horizontal & operator+=(const ABC & other) ;
    Horizontal & operator-=(const ABC & other) ;
    Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const ;
    Horizontal & operator=(const Horizontal & other) ;
    Horizontal & operator=(const ABC & other) ;
    using TranslatorABC<Horizontal>::operator* ;

    void inverse(void) ;

    double & x(void) ;
    double x(void) const ;
    double & y(void) ;
    double y(void) const ;
    double & z(void) ;
    double z(void) const ;

    virtual void set( const Geometry::Point & c ) ;
    virtual void set( const Geometry::Vector & c ) ;

    /*!
     * \brief Returns the 2 unknowns (x, y) that defines this translation.
     * \returns The unknowns vector.
     */
    Eigen::VectorXd unknowns(void) const ;

    /*!
     * \brief Defines the Horizontal class from the (x, y, z) values
     *        contained in the unknowns vector.
     * \param[in] unknowns The unknowns vector.
     */
    void unknowns(const Eigen::VectorXd & unknowns) ;

    /*!
     * \brief Returns an empty contraints vector (no constraints).
     * \returns The constraints vector.
     */
    Eigen::VectorXd constraints( void ) const ;

    Horizontal toHorizontal(void) const ;
    Cartesian toCartesian(void) const ;
    Geometry::Point toPoint( void ) const ;
    Geometry::Vector toVector( void ) const ;
} ;

} // end of namespace Translation
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Translation_Horizontal_hpp__
