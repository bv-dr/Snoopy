#include <cmath>

#include "Geometry/Translation/Cartesian.hpp"

#include "Geometry/Translation/Horizontal.hpp"

namespace BV {
namespace Geometry {
namespace Translation {

double & Cartesian::x(void)
{
    return innerData_.x() ;
}
double Cartesian::x(void) const
{
    return innerData_.x() ;
}
double & Cartesian::y(void)
{
    return innerData_.y() ;
}
double Cartesian::y(void) const
{
    return innerData_.y() ;
}
double & Cartesian::z(void)
{
    return innerData_.z() ;
}
double Cartesian::z(void) const
{
    return innerData_.z() ;
}

Cartesian & Cartesian::operator=(const Cartesian & other)
{
    x() = other.x() ;
    y() = other.y() ;
    z() = other.z() ;
    return *this ;
}

Cartesian & Cartesian::operator=(const ABC & other)
{
    *this = other.toCartesian() ;
    return *this ;
}

void Cartesian::inverse(void)
{
    innerData_ = innerData_.inverse() ;
}

Cartesian & Cartesian::operator*=( const ABC & other )
{
	Cartesian o(other.toCartesian()) ;
    x() += o.x() ;
    y() += o.y() ;
    z() += o.z() ;
    return *this ;
}

Cartesian & Cartesian::operator*=( const Cartesian & other )
{
    x() += other.x() ;
    y() += other.y() ;
    z() += other.z() ;
    return *this ;
}

Cartesian & Cartesian::operator+=(const ABC & other)
{
    *this *= other ;
    return *this ;
}

Cartesian & Cartesian::operator+=(const Cartesian & other)
{
    *this *= other ;
    return *this ;
}

Cartesian & Cartesian::operator-=(const ABC & other)
{
    *this *= other.toCartesian().getInversed() ;
    return *this ;
}

Cartesian & Cartesian::operator-=(const Cartesian & other)
{
    *this *= other.getInversed() ;
    return *this ;
}

Eigen::Vector3d Cartesian::operator*(const Eigen::Vector3d & vect) const
{
    return innerData_ * vect ;
}

void Cartesian::set( const Geometry::Point & c )
{
    this->x() = c.x() ;
    this->y() = c.y() ;
    this->z() = c.z() ;
}

void Cartesian::set( const Geometry::Vector & c )
{
    this->x() = c.x() ;
    this->y() = c.y() ;
    this->z() = c.z() ;
}

Eigen::VectorXd Cartesian::unknowns(void) const
{
    unknowns_(0) = this->x() ;
    unknowns_(1) = this->y() ;
    unknowns_(2) = this->z() ;
    return unknowns_ ;
}

void Cartesian::unknowns(const Eigen::VectorXd & unknowns)
{
    assert(unknowns.size() == nUnknowns()) ;
    this->x() = unknowns(0) ;
    this->y() = unknowns(1) ;
    this->z() = unknowns(2) ;
}

Eigen::VectorXd Cartesian::constraints(void) const
{
    return constraints_ ;
}

Horizontal Cartesian::toHorizontal(void) const
{
    return Horizontal(x(), y(), z()) ;
}

Cartesian Cartesian::toCartesian(void) const
{
    return *this ;
}

Geometry::Point Cartesian::toPoint( void ) const
{
    const Geometry::Point c( x(), y(), z() ) ;
    return c ;
}

Geometry::Vector Cartesian::toVector( void ) const
{
    const Geometry::Vector c( x(), y(), z() ) ;
    return c ;
}

} // end of namespace Translation
} // end of namespace Geometry
} // end of namespace BV
