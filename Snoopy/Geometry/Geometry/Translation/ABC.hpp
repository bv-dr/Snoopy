/*!
 * \file ABC.hpp
 */

#pragma once
#ifndef __BV_Geometry_Translation_ABC_hpp__
#define __BV_Geometry_Translation_ABC_hpp__

#include <Eigen/Dense>

#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"

#if EIGEN_VERSION_AT_LEAST(3, 3, 0)

namespace BV {
namespace Geometry {
namespace Translation {

class ABC ;

} // End of namespace Translation
} // End of namespace Geometry
} // End of namespace ABC

namespace Eigen {

template<> struct NumTraits<BV::Geometry::Translation::ABC>
{
  enum {IsInteger = false};

} ;

//namespace internal {
//
//template<typename To>
//struct is_convertible<BV::Geometry::Translation::ABC, To>
//{
//  enum {value = false} ;
//} ;
//
//} // End of namespace internal
} // End of namespace Eigen

#endif // Test eigen version

namespace BV {
namespace Geometry {
namespace Translation {

// Forward declaration for conversions
class Cartesian ;
template <int LatitudeConvEnum>
class Spherical ;
class Horizontal ;

/*!
 * \class ABC
 * \brief The abstract base class pattern for translations.
 *
 * Pure virtual methods:
 * virtual void addTranslation(const ABC & abc) = 0 ;
 * virtual void inverse(void) = 0 ;
 * virtual void addTranslation(const double & x, const double & y, const double & z = 0) = 0 ;
 * virtual void subtractTranslation(const ABC & abc) = 0 ;
 * virtual void subtractTranslation(const double & x, const double & y, const double & z = 0) = 0 ;
 * virtual void set(const Geometry::Point &) = 0 ;
 * virtual void set(const Geometry::Vector &) = 0 ;
 * virtual Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const = 0 ;
 * virtual Eigen::Vector3d inverseTranslation(const Eigen::Vector3d & vect) const = 0 ;
 * virtual Eigen::VectorXd unknowns(void) const = 0 ;
 * virtual void unknowns(const Eigen::VectorXd &) = 0 ;
 * virtual Eigen::VectorXd constraints(void) const = 0 ;
 * virtual Horizontal toHorizontal(void) const = 0 ;
 * virtual Cartesian toCartesian(void) const = 0 ;
 * virtual Spherical toSpherical(void) const = 0 ;
 * virtual Geometry::Point toPoint(void) const = 0 ;
 * virtual Geometry::Vector toVector(void) const = 0 ;
 *
 */
class ABC
{
protected:
    //! The number of unknowns
    const unsigned int nUnknowns_ ;
    //! The number of constraints
    const unsigned int nConstraints_ ;
    //! The unknowns vector
    mutable Eigen::VectorXd unknowns_ ;
    //! The constraints vector
    Eigen::VectorXd constraints_ ;
public:
#ifndef EIGEN_DONT_VECTORIZE
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
#endif
    /*!
    * \brief Abstract constructor which initialize the unknowns number and values
    *            and also the constraints number and values.
    * \param[in] nUnknowns The unknowns number of the translator.
    * \param[in] nConstraints The constraints number of the translator.
    */
    ABC(const unsigned int & nUnknowns, const unsigned int & nConstraints) :
        nUnknowns_(nUnknowns), nConstraints_(nConstraints),
        unknowns_(Eigen::VectorXd::Zero(nUnknowns)),
        constraints_(Eigen::VectorXd::Zero(nConstraints))
    {
    }

    /*!
    * \brief Abstract destructor.
    */
    virtual ~ABC( void )
    {
        // Left blank
    }

    /*!
    * \brief Returning the number of unknowns
    * \return Unknowns number
    */
    const unsigned int & nUnknowns(void) const
    {
        return nUnknowns_ ;
    }

    /*!
    * \brief Returning the number of constraints
    * \return Constraints number
    */
    const unsigned int & nConstraints(void) const
    {
        return nConstraints_ ;
    }

    /*!
    * \brief Add to self components the translation represented by an other ABC.
    *        Pure virtual member method.
    * \param[in] Abstract ABC pattern for translators
    */
    virtual void addTranslation(const ABC & abc) = 0 ;

    /*!
     * \brief Inverse the translation.
     */
    virtual void inverse(void) = 0 ;

    /*!
    * \brief Add to self components the translation represented by three double (x, y, z).
    *        Pure virtual member method.
    * \param[in] x value to be added to self x component
    * \param[in] y value to be added to self y component
    * \param[in] z value to be added to self z component
    */
    virtual void addTranslation(const double & x,
                                const double & y,
                                const double & z = 0) = 0 ;

    /*!
    * \brief Add to self components the translation represented by an Eigen::Vector3d.
    *        Member method which delegates to addTranslation(const double, const double, const double).
    * \param[in] v an Eigen::Vector3d the array to be added to self components (component by component)
    */
    virtual void addTranslation(const Eigen::Vector3d & v)
    {
        addTranslation(v(0), v(1), v(2)) ;
    }

    /*!
    * \brief Add to self components the translation represented by a Geometry::Vector.
    *        Member method which delegates to addTranslation(const double, const double, const double).
    * \param[in] v a Geometry::Vector to be added to self components (component by component)
    */
    virtual void addTranslation(const Geometry::Vector & v)
    {
        addTranslation(v.x(), v.y(), v.z()) ;
    }

    /*!
    * \brief Add to self components the translation represented by a Geometry::Point.
    *        Member method which delegates to addTranslation(const double, const double, const double).
    * \param[in] pt a Geometry::Point to be added to self components (component by component)
    */
    virtual void addTranslation(const Geometry::Point & pt)
    {
        addTranslation(pt.x(), pt.y(), pt.z()) ;
    }

    /*!
    * \brief Substract to self components the translation represented by an other ABC.
    *        Pure virtual member method.
    * \param[in] Abstract ABC pattern for translators
    */
    virtual void subtractTranslation(const ABC & abc) = 0 ;

    /*!
    * \brief Substract to self components the translation represented by three double (x, y, z).
    *        Pure virtual member method.
    * \param[in] x value to be subtracted to self x component
    * \param[in] y value to be subtracted to self y component
    * \param[in] z value to be subtracted to self z component
    */
    virtual void subtractTranslation(const double & x,
                                      const double & y,
                                      const double & z = 0) = 0 ;

    /*!
    * \brief Substract to self components the translation represented by an Eigen::Vector3d.
    *        Member method which delegates to subtractTranslation(const double, const double, const double).
    * \param[in] v an Eigen::Vector3d the array to be subtracted to self components (component by component)
    */
    virtual void subtractTranslation(const Eigen::Vector3d & v)
    {
        subtractTranslation(v(0), v(1), v(2)) ;
    }

    /*!
    * \brief Substract to self components the translation represented by a Geometry::Vector.
    *        Member method which delegates to subtractTranslation(const double, const double, const double).
    * \param[in] v a Geometry::Vector to be subtracted to self components (component by component)
    */
    virtual void subtractTranslation(const Geometry::Vector & v)
    {
        subtractTranslation(v.x(), v.y(), v.z()) ;
    }

    /*!
    * \brief Substract to self components the translation represented by a Geometry::Point.
    *        Member method which delegates to subtractTranslation(const double, const double, const double).
    * \param[in] pt a Geometry::Point to be subtracted to self components (component by component)
    */
    virtual void subtractTranslation(const Geometry::Point & pt)
    {
        subtractTranslation(pt.x(), pt.y(), pt.z()) ;
    }

    /*!
    * \brief Set translator components from Geometry::Point.
    *        Pure virtual member method.
    * \param[in] pt a Geometry::Point used to assign self components
    */
    virtual void set(const Geometry::Point &) = 0 ;

    /*!
    * \brief Set translator components from Geometry::Vector.
    *        Pure virtual member method.
    * \param[in] pt a Geometry::Vector used to assign self components
    */
    virtual void set(const Geometry::Vector &) = 0 ;

    /*!
    * \brief Translate an Eigen::Vector3d by adding self components to it (component by component)
    *        Pure virtual member method.
    * \param[in] vect an Eigen::Vector3d to be translated
    * \return An Eigen::Vector3d which is a copy of the input vect translated by self components
    */
    virtual Eigen::Vector3d operator*(const Eigen::Vector3d & vect) const = 0 ;

    /*!
    * \brief Translate a Geometry::Vector by adding self components to it (component by component)
    *        Member method which delegates to operator*(const Eigen::Vector3d).
    * \param[in] vect a Geometry::Vector to be translated
    * \return A Geometry::Vector which is a copy of the input vect translated by self components
    */
    virtual Geometry::Vector operator*(const Geometry::Vector & vect) const
    {
        return Geometry::Vector(*this * vect.toArray()) ;
    }

    /*!
    * \brief Translate a Geometry::Point by adding self components to it (component by component)
    *        Member method which delegates to operator*(const Eigen::Vector3d).
    * \param[in] pt a Geometry::Point to be translated
    * \return A Geometry::Point which is a copy of the input pt translated by self components
    */
    virtual Geometry::Point operator*(const Geometry::Point & pt) const
    {
        return Geometry::Point(*this * pt.toArray()) ;
    }

    /*!
    * \brief Translate an Eigen::Vector3d by adding self components to it (component by component)
    *        Member method which delegates to operator*(const Eigen::Vector3d).
    * \param[in] vect an Eigen::Vector3d to be translated
    * \return An Eigen::Vector3d which is a copy of the input vect translated by self components
    */
    virtual Eigen::Vector3d translation(const Eigen::Vector3d & vect) const
    {
        return *this * vect ;
    }

    /*!
    * \brief Translate a Geometry::Vector by adding self components to it (component by component)
    *        Member method which delegates to operator*(const Eigen::Vector3d).
    * \param[in] vect a Geometry::Vector to be translated
    * \return A Geometry::Vector which is a copy of the input vect translated by self components
    */
    virtual Vector translation(const Vector & vect) const
    {
        return Vector(*this * vect.toArray()) ;
    }

    /*!
    * \brief Translate a Geometry::Point by adding self components to it (component by component)
    *        Member method which delegates to operator*(const Eigen::Vector3d).
    * \param[in] pt a Geometry::Point to be translated
    * \return A Geometry::Point which is a copy of the input pt translated by self components
    */
    virtual Point translation(const Point & pt) const
    {
        return Point(*this * pt.toArray()) ;
    }


    /*!
    * \brief Translate an Eigen::Vector3d by subtracting self components to it (component by component)
    *        Pure virtual member method.
    * \param[in] vect an Eigen::Vector3d to be translated
    * \return An Eigen::Vector3d which is a copy of the input vect translated by the opposite of self components
    */
    virtual Eigen::Vector3d inverseTranslation(const Eigen::Vector3d & vect) const = 0 ;

    /*!
    * \brief Translate a Geometry:Vector by subtracting self components to it (component by component)
    *        Member method which delegates to inverseTranslation(const Eigen::Vector3d).
    * \param[in] vect a Geometry::Vector to be translated
    * \return A Geometry::Vector which is a copy of the input vect translated by the opposite of self components
    */
    virtual Vector inverseTranslation(const Vector & vect) const
    {
        return Vector(inverseTranslation(vect.toArray())) ;
    }

    /*!
    * \brief Translate a Geometry:Point by subtracting self components to it (component by component)
    *        Member method which delegates to inverseTranslation(const Eigen::Vector3d).
    * \param[in] vect a Geometry::Point to be translated
    * \return A Geometry::Point which is a copy of the input vect translated by the opposite of self components
    */
    virtual Point inverseTranslation(const Point & pt) const
    {
        return Point(inverseTranslation(pt.toArray())) ;
    }

    /*!
    * \brief Unknowns getter.
    *        Pure virtual member method.
    * \return An Eigen::VectorXd containing a representation of the translator component.
    *         The size may vary depending of the number of unknowns of the translator.
    */
    virtual Eigen::VectorXd unknowns(void) const = 0 ;

    /*!
    * \brief Unknowns setter.
    *        Pure virtual member method.
    * \param[in] An Eigen::VectorXd containing a representation of the translator component.
    *            The size may vary depending of the number of unknowns of the translator.
    */
    virtual void unknowns(const Eigen::VectorXd &) = 0 ;

    /*!
    * \brief Constraints getter.
    *        Pure virtual member method.
    * \return An Eigen::VectorXd containing a representation of the translator component.
    *         The size may vary depending of the number of unknowns of the translator.
    */
    virtual Eigen::VectorXd constraints(void) const = 0 ;

    /*!
    * \brief Convert self translator to a type Horizontal translator.
    *        Pure virtual member method.
    * \return An Horizontal translator
    */
    virtual Horizontal toHorizontal(void) const = 0 ;

    /*!
    * \brief Convert self translator to a type Cartesian translator.
    *        Pure virtual member method.
    * \return A Cartesian translator
    */
    virtual Cartesian toCartesian(void) const = 0 ;

    /*!
    * \brief Convert self translator to a type Spherical translator.
    *        Pure virtual member method.
    * \return A Cartesian translator
    */
    template <int LatitiudeConvEnum>
    Spherical<LatitiudeConvEnum> toSpherical(void) const
    {
    	return Spherical<LatitiudeConvEnum>(toVector()) ;
    }

    /*!
    * \brief Convert self translator to a type Point.
    *        Pure virtual member method.
    * \return A Point representing self translation components
    */
    virtual Geometry::Point toPoint(void) const = 0 ;

    /*!
    * \brief Convert self translator to a type Point.
    *        Pure virtual member method.
    * \return A Point representing self translation components
    */
    virtual Geometry::Vector toVector(void) const = 0 ;

    /*!
     * \brief Equality operator
     * \return True if the translator represents the same translation,
     * by comparing them in Cartesian coordinates with a Geometry::Vector
     * False otherwise
     */
    bool operator==(const ABC & other) const
    {
        return (toVector() == other.toVector()) ;
    }

    /*!
     * \brief Non Equality operator
     * \return True if the translator represents different translation,
     * by comparing them in Cartesian coordinates with a Geometry::Vector
     * False otherwise
     */
    bool operator!=(const ABC & other) const
    {
        return !(*this == other) ;
    }

} ;

namespace Details {

/*!
 * \class TranslatorABC
 * \tparam Derived The derived type for the CRTP
 * \brief The abstract class pattern for CRTP translations.
 * Overloaded methods:
 * virtual void addTranslation(const ABC & other) ;
 * virtual void addTranslation(const double & x, const double & y, const double & z = 0) ;
 * virtual void subtractTranslation(const ABC & other) ;
 * virtual void subtractTranslation(const double & x, const double & y, const double & z = 0) ;
 * virtual Eigen::Vector3d inverseTranslation(const Eigen::Vector3d & vect) const ;
 *
 * Pure virtual methods:
 * virtual Derived & operator*=(const ABC & other) = 0 ;
 * virtual Derived & operator-=(const ABC & other) = 0 ;
 * virtual Derived & operator+=(const ABC & other) = 0 ;
 */
template <typename Derived>
class TranslatorABC : public ABC
{
public:
    typedef Derived TranslatorType ;

    /*!
     * \copydoc Geometry::Translation::ABC::ABC(const unsigned int &, const unsigned int &)
     */
    TranslatorABC(const unsigned int & nUnknowns,
                  const unsigned int & nConstraints) :
        ABC(nUnknowns, nConstraints)
    {
    }

    /*!
    * \copydoc Geometry::Translation::ABC::~ABC(void)
    */
    virtual ~TranslatorABC(void)
    {
    }

    /*!
    * \brief Return the opposite of the translator (component by component).
    * \return A copy of the inversed translator of same type as self
    */
    // virtual Derived getInversed(void) const = 0 ;
    Derived getInversed(void) const
    {
        Derived tmp(*this) ;
        tmp.inverse() ;
        return tmp ;
    }

    /*!
    * \copydoc Geometry::Translation::ABC::addTranslation(const ABC &)
    */
    virtual Derived & operator*=(const ABC & other) = 0 ;

    /*!
    * \copydoc Geometry::Translation::ABC::subtractTranslation(const ABC &)
    */
    virtual Derived & operator-=(const ABC & other) = 0 ;

    /*!
    * \copydoc Geometry::Translation::ABC::addTranslation(const ABC &)
    */
    virtual Derived & operator+=(const ABC & other) = 0 ;

    /*!
    * \brief Translate an ABC by adding self components to it (component by component)
    * \param[in] other an ABC translator
    * \return A copy of same type as self containing the translated (added) values of self by other
    */
    using ABC::operator* ;
    virtual Derived operator*(const ABC & other) const
    {
        Derived tmp(*this) ;
        tmp *= other ;
        return tmp ;
    }

    /*!
    * \copydoc Geometry::Translation::TranslatorABC::operator*(const ABC &)
    */
    virtual Derived operator+(const ABC & other) const
    {
        return *this * other ;
    }

    /*!
    * \brief Substract to self components the translation represented by an other ABC.
    * \param[in] Abstract ABC pattern for translators
    * \return A copy of same type as self containing the translated (subtracted) values of self by other
    */
    virtual Derived operator-(const ABC & other) const
    {
        Derived tmp(*this) ;
        tmp -= other ;
        return tmp ;
    }

    using ABC::addTranslation ;
    /*!
    * \copydoc Geometry::Translation::ABC::addTranslation(const ABC &)
    */
    virtual void addTranslation(const ABC & other)
    {
        *this *= other ;
    }

    /*!
    * \copydoc Geometry::Translation::ABC::addTranslation(const double &, const double &, const double &)
    */
    virtual void addTranslation(const double & x,
                                const double & y,
                                const double & z = 0)
    {
        Derived tmp(x, y, z) ;
        *this *= tmp ;
    }

    using ABC::subtractTranslation ;
    /*!
    * \copydoc Geometry::Translation::ABC::subtractTranslation(const ABC &)
    */
    virtual void subtractTranslation(const ABC & other)
    {
        *this -= other ;
    }

    /*!
    * \copydoc Geometry::Translation::ABC::subtractTranslation(const double &, const double &, const double &)
    */
    virtual void subtractTranslation(const double & x,
                                      const double & y,
                                      const double & z = 0)
    {
        Derived tmp(x, y, z) ;
        *this -= tmp ;
    }

    /*!
    * \copydoc Geometry::Translation::TranslatorABC::getInversed(void)
    */
    Derived operator-(void)
    {
        return getInversed() ;
    }

    /*!
    * \brief A component setter from a Derived other.
    * \return reference to self once assigned components from other
    */
    virtual Derived & operator=(const Derived & other) = 0 ;

    /*!
    * \brief A component setter from a ABC other.
    * \return A copy of type Derived (self) once assigned components from other
    */
    virtual Derived & operator=(const ABC & other) = 0 ;

    using ABC::inverseTranslation ;
    /*!
     * \brief Returns the inverse translation to provided vector.
     * \param[in] vect The vector on which to apply the inverse translation.
     * \return The vector on which the inverse translation was applied.
     */
    virtual Eigen::Vector3d inverseTranslation(const Eigen::Vector3d & vect) const
    {
        return getInversed() * vect ;
    }

} ;

} // End of namespace Details

} // end of namespace Translation
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Translation_ABC_hpp__
