#include "Geometry/Translation/Horizontal.hpp"

#include "Geometry/Translation/Cartesian.hpp"

namespace BV {
namespace Geometry {
namespace Translation {

Horizontal & Horizontal::operator*=(const ABC & other)
{
    // Here z coordinate of other should not be taken into account
	Cartesian o(other.toCartesian()) ;
    x() += o.x() ;
    y() += o.y() ;
    return *this ;
}

Horizontal & Horizontal::operator+=(const ABC & other)
{
    *this *= other ;
    return *this ;
}
Horizontal & Horizontal::operator-=(const ABC & other)
{
    *this *= other.toHorizontal().getInversed() ;
    return *this ;
}
Eigen::Vector3d Horizontal::operator*(const Eigen::Vector3d & vect) const
{
    return innerData_ * vect ;
}

Horizontal & Horizontal::operator=(const Horizontal & other)
{
    x() = other.x() ;
    y() = other.y() ;
    z() = other.z() ; // FIXME !
    return *this ;
}

Horizontal & Horizontal::operator=(const ABC & other)
{
    *this = other.toHorizontal() ;
    return *this ;
}

void Horizontal::inverse(void)
{
    innerData_ = innerData_.inverse() ;
}

double & Horizontal::x(void)
{
    return innerData_.x() ;
}
double Horizontal::x(void) const
{
    return innerData_.x() ;
}
double & Horizontal::y(void)
{
    return innerData_.y() ;
}
double Horizontal::y(void) const
{
    return innerData_.y() ;
}
double & Horizontal::z(void)
{
    return innerData_.z() ;
}
double Horizontal::z(void) const
{
    return innerData_.z() ;
}

void Horizontal::set( const Geometry::Point & c )
{
    this->x() = c.x() ;
    this->y() = c.y() ;
    this->z() = c.z() ; // FIXME should we assign z ?
}

void Horizontal::set( const Geometry::Vector & c )
{
    this->x() = c.x() ;
    this->y() = c.y() ;
    this->z() = c.z() ; // FIXME should we assign z ?
}

Eigen::VectorXd Horizontal::unknowns(void) const
{
    unknowns_(0) = this->x() ;
    unknowns_(1) = this->y() ;
    return unknowns_ ;
}

void Horizontal::unknowns(const Eigen::VectorXd & unknowns)
{
    assert(unknowns.size() == nUnknowns()) ;
    this->x() = unknowns(0) ;
    this->y() = unknowns(1) ;
}

Eigen::VectorXd Horizontal::constraints(void) const
{
    return constraints_ ;
}

Horizontal Horizontal::toHorizontal(void) const
{
    return *this ;
}

Cartesian Horizontal::toCartesian(void) const
{
    return Cartesian(x(), y(), z()) ;
}

Geometry::Point Horizontal::toPoint( void ) const
{
    const Geometry::Point c( x(), y(), z() ) ;
    return c ;
}

Geometry::Vector Horizontal::toVector( void ) const
{
    const Geometry::Vector c( x(), y(), z() ) ;
    return c ;
}

} // end of namespace Translation
} // end of namespace Geometry
} // end of namespace BV
