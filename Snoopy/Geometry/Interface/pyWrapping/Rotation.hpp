#pragma once

#include "Geometry/Rotation.hpp"

namespace BV {
namespace PythonInterface {
namespace Geometry {

using namespace BV::Geometry ;
namespace py = pybind11 ;

template <Rotation::AxisConvention UEnum, Rotation::AxisConvention VEnum,
          Rotation::AxisConvention WEnum, Rotation::OrderConvention OEnum>
void AddEulerConvention_(py::module & m, const char * name)
{
    py::class_<Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum> >(m, name)
        .def("getU", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getU,
                    py::return_value_policy::copy,
             R"rst(
             Return the U axis, the first axis of the UVW convention.

             :return: a :py:class:`~Geometry.AxisConvention` axis.
             )rst")
        .def("getV", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getV,
                    py::return_value_policy::copy,
             R"rst(
             Return the V axis, the second axis of the UVW convention.

             :return: a :py:class:`~Geometry.AxisConvention` axis.
             )rst")
        .def("getW", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getW,
                    py::return_value_policy::copy,
             R"rst(
             Return the W axis, the third axis of the UVW convention.

             :return: a :py:class:`~Geometry.AxisConvention` axis.
             )rst")
        .def("getOrderConvention", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getOrderConvention,
                              py::return_value_policy::copy,
             R"rst(
             Return the order convention (extrinsic/intrinsic)

             :return: a :py:class:`~Geometry.OrderConvention` value.
             )rst")
        .def("getUUnitVector", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getUUnitVector,
             R"rst(
             Return the unit vector corresponding to the first axis (U) in the
             UVW convention. i.e. if U =
             :py:class:`Geometry.AxisConvention`.X_AXIS, then the unit vector
             will be numpy.array((1., 0., 0.), dtype=float)

             :return: a :py:class:`numpy.ndarray` containing the appropriate
                 unit vector.
             )rst")
        .def("getVUnitVector", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getVUnitVector,
             R"rst(
             Return the unit vector corresponding to the second axis (V) in the
             UVW convention. i.e. if V =
             :py:class:`Geometry.AxisConvention`.X_AXIS, then the unit vector
             will be numpy.array((1., 0., 0.), dtype=float)

             :return: a :py:class:`numpy.ndarray` containing the appropriate
                 unit vector.
             )rst")
        .def("getWUnitVector", &Rotation::EulerAnglesConvention<UEnum, VEnum, WEnum, OEnum>::getWUnitVector,
             R"rst(
             Return the unit vector corresponding to the third axis (w) in the
             UVW convention. i.e. if W =
             :py:class:`Geometry.AxisConvention`.X_AXIS, then the unit vector
             will be numpy.array((1., 0., 0.), dtype=float)

             :return: a :py:class:`numpy.ndarray` containing the appropriate
                 unit vector.
             )rst")
    ;
}

template <typename Scope>
void AddRotatorOperators_(Scope & scope)
{
    using RotatorType = typename Scope::type ;
    scope.def("__imul__",
              [](RotatorType & t, const Rotation::ABC & abc)
              {
                  return t *= abc ;
              },
              R"rst(Same as :py:meth:`addOtherRotationAtRight`)rst")
        .def("__mul__",
             [](RotatorType & t, const Rotation::ABC & abc)
             {
                 return t * abc ;
             },
             R"rst(Same as :py:meth:`addOtherRotationAtRight` but return a copy instead.)rst")
        .def("__mul__",
             [](RotatorType & t, const Eigen::Vector3d & v)
             {
                 return t * v ;
             },
             R"rst(Same as :py:meth:`rotate`.)rst")
        .def("__mul__",
             [](RotatorType & t, const Vector & v)
             {
                 return t * v ;
             },
             R"rst(Same as :py:meth:`rotate`.)rst")
        .def("__mul__",
             [](RotatorType & t, const Point & pt)
             {
                 return t * pt ;
             },
             R"rst(Same as :py:meth:`rotate`.)rst")
        .def("__isub__",
             [](RotatorType & t, const Rotation::ABC & abc)
             {
                 return t -= abc ;
             },
             R"rst(Same as :py:meth:`subtractOtherRotationAtRight`)rst")
        .def("__sub__",
             [](RotatorType & t, const Rotation::ABC & abc)
             {
                 return t - abc ;
             },
             R"rst(Same as :py:meth:`subtractOtherRotationAtRight` but return a copy instead.)rst")
        .def("__iadd__",
             [](RotatorType & t, const Rotation::ABC & abc)
             {
                 return t += abc ;
             },
             R"rst(Same as :py:meth:`addOtherRotationAtRight`)rst")
        .def("__add__",
             [](RotatorType & t, const Rotation::ABC & abc)
             {
                 return t + abc ;
             },
             R"rst(Same as :py:meth:`addOtherRotationAtRight` but return a copy instead.)rst")
        .def("__neg__",
             [](RotatorType & t)
             {
                 return -t ;
             },
             R"rst(Same as :py:meth:`inverse`)rst")
        .def("getInversed", &RotatorType::getInversed,
             R"rst(Same as :py:meth:`inverse` but return a copy instead)rst")
    ;
}


template <typename ConventionType>
void AddEulerClass_(py::module & m, const char * pythonName)
{
    py::class_<Rotation::EulerAngles<ConventionType>, Rotation::ABC> euler(m, pythonName) ;
    euler.def(py::init<const double &, const double &, const double &>())
        .def(py::init<const Eigen::Matrix3d &>())
        .def(py::init<>())
        .def(py::init<const Eigen::VectorXd &>())
        .def(py::init<const Rotation::EulerAngles<ConventionType> &>())
        .def(py::init<const Rotation::ABC &>())
#ifdef __INTEL_COMPILER
        .def_property_readonly("alpha", [](const Rotation::EulerAngles<ConventionType> & t){return t.alpha();})
        .def_property_readonly("beta", [](const Rotation::EulerAngles<ConventionType> & t){return t.beta();})
        .def_property_readonly("gamma", [](const Rotation::EulerAngles<ConventionType> & t){return t.gamma();})
#else
        .def_property_readonly("alpha", py::overload_cast<>(&Rotation::EulerAngles<ConventionType>::alpha, py::const_))
        .def_property_readonly("beta", py::overload_cast<>(&Rotation::EulerAngles<ConventionType>::beta, py::const_))
        .def_property_readonly("gamma", py::overload_cast<>(&Rotation::EulerAngles<ConventionType>::gamma, py::const_))
#endif
        .def("getConvention", &Rotation::EulerAngles<ConventionType>::getConvention,
                py::return_value_policy::copy)
        .def("isSingular", &Rotation::EulerAngles<ConventionType>::isSingular)
    ;
    AddRotatorOperators_(euler) ;
}

void exportRotationModule(py::module & m)
{
    py::enum_<Rotation::AxisConvention>(m, "AxisConvention")
        .value("X_AXIS", Rotation::AxisConvention::X_AXIS)//, R"rst(The x axis.)rst")
        .value("Y_AXIS", Rotation::AxisConvention::Y_AXIS)//, R"rst(The y axis.)rst")
        .value("Z_AXIS", Rotation::AxisConvention::Z_AXIS)//, R"rst(The z axis.)rst")
    ;

    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                        Rotation::AxisConvention::Y_AXIS,
                        Rotation::AxisConvention::X_AXIS,
                        Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_XYX_i") ;

    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_XYZ_i") ;
    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_XZX_i") ;
    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_XZY_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_YXY_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_YXZ_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_YZX_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_YZY_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_ZXY_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_ZXZ_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_ZYX_i") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::INTRINSIC>(m, "EulerAnglesConvention_ZYZ_i") ;

    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_XYX_e") ;
    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_XYZ_e") ;
    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_XZX_e") ;
    AddEulerConvention_<Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_XZY_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_YXY_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_YXZ_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_YZX_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_YZY_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_ZXY_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_ZXZ_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::X_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_ZYX_e") ;
    AddEulerConvention_<Rotation::AxisConvention::Z_AXIS,
                            Rotation::AxisConvention::Y_AXIS,
                            Rotation::AxisConvention::Z_AXIS,
                            Rotation::OrderConvention::EXTRINSIC>(m, "EulerAnglesConvention_ZYZ_e") ;

    py::enum_<RotatorTypeEnum>(m, "RotatorTypeEnum")
        .value("QUATERNION", RotatorTypeEnum::QUATERNION)//, R"rst(The :py:class:`~Geometry.Quaternion` rotation object enum value)rst")
        .value( "BASIS_VECTORS", RotatorTypeEnum::BASIS_VECTORS)//, R"rst(The :py:class:`~Geometry.BasisVectors` rotation object enum value)rst")
        .value("AXIS_AND_ANGLE", RotatorTypeEnum::AXIS_AND_ANGLE)//, R"rst(The :py:class:`~Geometry.AxisAndAngle` rotation object enum value)rst")
        .value("ROTATION_MATRIX", RotatorTypeEnum::ROTATION_MATRIX)//, R"rst(The :py:class:`~Geometry.RotationMatrix` rotation object enum value)rst")
        .value("MRP", RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS)//, R"rst(The :py:class:`~Geometry.MRP` rotation object enum value)rst")
        .value("EULER_ANGLES_XYX_i", RotatorTypeEnum::EULER_ANGLES_XYX_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_XYX_i` rotation object enum value)rst")
        .value("EULER_ANGLES_XYZ_i", RotatorTypeEnum::EULER_ANGLES_XYZ_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_XYZ_i` rotation object enum value)rst")
        .value("EULER_ANGLES_XZX_i", RotatorTypeEnum::EULER_ANGLES_XZX_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_XZX_i` rotation object enum value)rst")
        .value("EULER_ANGLES_XZY_i", RotatorTypeEnum::EULER_ANGLES_XZY_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_XZY_i` rotation object enum value)rst")
        .value("EULER_ANGLES_YXY_i", RotatorTypeEnum::EULER_ANGLES_YXY_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_YXY_i` rotation object enum value)rst")
        .value("EULER_ANGLES_YXZ_i", RotatorTypeEnum::EULER_ANGLES_YXZ_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_YXZ_i` rotation object enum value)rst")
        .value("EULER_ANGLES_YZX_i", RotatorTypeEnum::EULER_ANGLES_YZX_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_YZX_i` rotation object enum value)rst")
        .value("EULER_ANGLES_YZY_i", RotatorTypeEnum::EULER_ANGLES_YZY_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_YZY_i` rotation object enum value)rst")
        .value("EULER_ANGLES_ZXY_i", RotatorTypeEnum::EULER_ANGLES_ZXY_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZXY_i` rotation object enum value)rst")
        .value("EULER_ANGLES_ZXZ_i", RotatorTypeEnum::EULER_ANGLES_ZXZ_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZXZ_i` rotation object enum value)rst")
        .value("EULER_ANGLES_ZYX_i", RotatorTypeEnum::EULER_ANGLES_ZYX_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZYX_i` rotation object enum value)rst")
        .value("EULER_ANGLES_ZYZ_i", RotatorTypeEnum::EULER_ANGLES_ZYZ_i) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZYZ_i` rotation object enum value)rst")
        .value("EULER_ANGLES_XYX_e", RotatorTypeEnum::EULER_ANGLES_XYX_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_XYX_e` rotation object enum value)rst")
        .value("EULER_ANGLES_XYZ_e", RotatorTypeEnum::EULER_ANGLES_XYZ_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_XYZ_e` rotation object enum value)rst")
        .value("EULER_ANGLES_XZX_e", RotatorTypeEnum::EULER_ANGLES_XZX_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_XZX_e` rotation object enum value)rst")
        .value("EULER_ANGLES_XZY_e", RotatorTypeEnum::EULER_ANGLES_XZY_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_XZY_e` rotation object enum value)rst")
        .value("EULER_ANGLES_YXY_e", RotatorTypeEnum::EULER_ANGLES_YXY_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_YXY_e` rotation object enum value)rst")
        .value("EULER_ANGLES_YXZ_e", RotatorTypeEnum::EULER_ANGLES_YXZ_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_YXZ_e` rotation object enum value)rst")
        .value("EULER_ANGLES_YZX_e", RotatorTypeEnum::EULER_ANGLES_YZX_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_YZX_e` rotation object enum value)rst")
        .value("EULER_ANGLES_YZY_e", RotatorTypeEnum::EULER_ANGLES_YZY_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_YZY_e` rotation object enum value)rst")
        .value("EULER_ANGLES_ZXY_e", RotatorTypeEnum::EULER_ANGLES_ZXY_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZXY_e` rotation object enum value)rst")
        .value("EULER_ANGLES_ZXZ_e", RotatorTypeEnum::EULER_ANGLES_ZXZ_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZXZ_e` rotation object enum value)rst")
        .value("EULER_ANGLES_ZYX_e", RotatorTypeEnum::EULER_ANGLES_ZYX_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZYX_e` rotation object enum value)rst")
        .value("EULER_ANGLES_ZYZ_e", RotatorTypeEnum::EULER_ANGLES_ZYZ_e) //, R"rst(The :py:class:`~Geometry.EulerAngles_ZYZ_e` rotation object enum value)rst")
    ;

    py::class_<Rotation::ABC>(m, "_GeometryRotationABC")
        .def("addOtherRotationAtLeft", &Rotation::ABC::addOtherRotationAtLeft,
             R"rst(
             Add other :ref:`rotation object<GeometryRotation>` at left:
             the added other rotation is performed **after** current one.

             This method composes the rotations such that for a given vector
             :math:`b` to be rotated twice, first by current rotation :math:`R`,
             then by other rotation :math:`R_2`:

             .. math::

                b'  &= R.b \\
                b'' &= R_2.b'

             The final rotation represents the matrix :math:`R''` such that:
             :math:`b'' = R'' b = R_2.R.b`
             Thus the final rotation is :math:`R'' = R_2.R`

             :param other: the other rotation object to multiply left.
             :paramtype other: :ref:`rotation object<GeometryRotation>`
             :return: current rotation multiplied left (composition)
             )rst")
        .def("addOtherRotationAtRight", &Rotation::ABC::addOtherRotationAtRight,
             R"rst(
             Add other :ref:`rotation object<GeometryRotation>` at right:
             the added other rotation is performed **before** current one.

             This method composes the rotations such that for a given vector
             :math:`b` to be rotated twice, first by other rotation :math:`R_2`,
             then by current rotation :math:`R`:

             .. math::

                b'  &= R_2.b \\
                b'' &= R.b'

             The final rotation represents the matrix :math:`R''` such that:
             :math:`b'' = R'' b = R.R_2.b`
             Thus the final rotation is :math:`R'' = R.R_2`

             :param other: the other rotation object to multiply right.
             :paramtype other: :ref:`rotation object<GeometryRotation>`
             :return: current rotation multiplied right (composition)
             )rst")
        .def("subtractOtherRotationAtLeft", &Rotation::ABC::subtractOtherRotationAtLeft,
             R"rst(
             Subtract other :ref:`rotation object<GeometryRotation>` at left:
             the subtracted other rotation is performed **after** current one.

             This method composes the rotations such that for a given vector
             :math:`b` to be rotated twice, first by current rotation :math:`R`,
             then by other rotation :math:`R_2` inverse :math:`{}^tR_2`:

             .. math::

                b'  &= R.b \\
                b'' &= {}^tR_2.b'

             The final rotation represents the matrix :math:`R''` such that:
             :math:`b'' = R'' b = {}^tR_2.R.b`
             Thus the final rotation is :math:`R'' = {}^tR_2.R`

             :param other: the other rotation object to multiply its inverse left.
             :paramtype other: :ref:`rotation object<GeometryRotation>`
             :return: current rotation multiplied left (composition)
             )rst")
        .def("subtractOtherRotationAtRight", &Rotation::ABC::subtractOtherRotationAtRight,
             R"rst(
             Subtract other :ref:`rotation object<GeometryRotation>` at right:
             the subtracted other rotation is performed **before** current one.

             This method composes the rotations such that for a given vector
             :math:`b` to be rotated twice, first by other rotation :math:`R_2`
             inverse :math:`{}^tR_2`, then by current rotation :math:`R`:

             .. math::

                b'  &= {}^tR_2.b \\
                b'' &= R.b'

             The final rotation represents the matrix :math:`R''` such that:
             :math:`b'' = R'' b = R.{}^tR_2.b`
             Thus the final rotation is :math:`R'' = R.{}^tR_2`

             :param other: the other rotation object to multiply its inverse right.
             :paramtype other: :ref:`rotation object<GeometryRotation>`
             :return: current rotation multiplied right (composition)
             )rst")
        .def("rotate", py::overload_cast<const Eigen::Vector3d &>(&Rotation::ABC::rotate, py::const_),
             R"rst(
             Rotate the vector :math:`b` provided such that
             :math:`b' = R.b`.

             :param v: :py:class:`numpy.ndarray` containing the vector coordinates.
             :paramtype v: :py:class:`numpy.ndarray`
             :return: the rotated vector as a :py:class:`numpy.ndarray`
             :rtype: :py:class:`numpy.ndarray`
             )rst")
        .def("rotate", py::overload_cast<const Vector &>(&Rotation::ABC::rotate, py::const_),
             R"rst(
             Rotate the vector :math:`b` provided such that
             :math:`b' = R.b`.

             :param v: a :py:class:`~Geometry.Vector` to rotate
             :paramtype v: :py:class:`~Geometry.Vector`
             :return: the rotated vector as a :py:class:`~Geometry.Vector`
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("rotate", py::overload_cast<const Point &>(&Rotation::ABC::rotate, py::const_),
             R"rst(
             Rotate the vector :math:`b` provided such that
             :math:`b' = R.b`.

             :param v: a :py:class:`~Geometry.Point` to rotate
             :paramtype v: :py:class:`~Geometry.Point`
             :return: the rotated point as a :py:class:`~Geometry.Point`
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def("inverseRotate", py::overload_cast<const Vector &>(&Rotation::ABC::inverseRotate, py::const_),
             R"rst(
             Inverse rotate the vector :math:`b` provided such that
             :math:`b' = {}^tR.b`.

             :param v: a :py:class:`~Geometry.Vector` on which to apply the inverse rotation
             :paramtype v: :py:class:`~Geometry.Vector`
             :return: the rotated vector as a :py:class:`~Geometry.Vector`
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("inverseRotate", py::overload_cast<const Point &>(&Rotation::ABC::inverseRotate, py::const_),
             R"rst(
             Inverse rotate the vector :math:`b` provided such that
             :math:`b' = {}^tR.b`.

             :param v: a :py:class:`~Geometry.Point` on which to apply the inverse rotation
             :paramtype v: :py:class:`~Geometry.Point`
             :return: the rotated point as a :py:class:`~Geometry.Point`
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def("inverseRotate", py::overload_cast<const Eigen::Vector3d &>(&Rotation::ABC::inverseRotate, py::const_),
             R"rst(
             Inverse rotate the vector :math:`b` provided such that
             :math:`b' = {}^tR.b`.

             :param v: a :py:class:`numpy.ndarray` on which to apply the inverse rotation
             :paramtype v: :py:class:`numpy.ndarray`
             :return: the rotated point as a :py:class:`numpy.ndarray`
             :rtype: :py:class:`numpy.ndarray`
             )rst")
        .def("inverse", &Rotation::ABC::inverse,
             R"rst(
             Compute the inverse rotation of :math:`R` so that it then
             represents its inverse :math:`{}^tR`.
             )rst")
        .def_property("unknowns",
#ifdef __INTEL_COMPILER
                      [](Rotation::ABC & r){return r.unknowns();},
                      [](Rotation::ABC & r, const Eigen::VectorXd & u){r.unknowns(u);},
                      R"rst(The :py:class:`numpy.ndarray` containing the unknowns of the rotation)rst")
#else
                      py::overload_cast<>(&Rotation::ABC::unknowns, py::const_),
                      py::overload_cast<const Eigen::VectorXd &>(&Rotation::ABC::unknowns),
                      R"rst(The :py:class:`numpy.ndarray` containing the unknowns of the rotation)rst")
#endif
        .def_property_readonly("constraints", &Rotation::ABC::constraints,
                               R"rst(
                               The :py:class:`numpy.ndarray` containing the constraints of the rotation
                               )rst")
        .def("nUnknowns", &Rotation::ABC::nUnknowns, py::return_value_policy::copy,
             R"rst(The number of rotation unknowns)rst")
        .def("nConstraints", &Rotation::ABC::nConstraints, py::return_value_policy::copy,
             R"rst(The number of rotation constraints)rst")
        .def("toRotationMatrix", &Rotation::ABC::toRotationMatrix,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.RotationMatrix` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.RotationMatrix` object.
             )rst")
        .def("toBasisVectors", &Rotation::ABC::toBasisVectors,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.BasisVectors` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.BasisVectors` object.
             )rst")
        .def("toQuaternion", &Rotation::ABC::toQuaternion,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.Quaternion` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.Quaternion` object.
             )rst")
        .def("toAxisAndAngle", &Rotation::ABC::toAxisAndAngle,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.AxisAndAngle` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.AxisAndAngle` object.
             )rst")
        .def("toMRP", &Rotation::ABC::toMRP,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.MRP` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.MRP` object.
             )rst")
        .def("toRotationVector", &Rotation::ABC::toRotationVector,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.RotationVector` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.RotationVector` object.
             )rst")
        .def("toHorizontalPlane", &Rotation::ABC::toHorizontalPlane,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.HorizontalPlane` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.HorizontalPlane` object.
             )rst")
        .def("toEulerAngles_XYX_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XYX_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XYX_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XYX_i` object.
             )rst")
        .def("toEulerAngles_XYZ_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XYZ_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XYZ_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XYZ_i` object.
             )rst")
        .def("toEulerAngles_XZX_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XZX_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XZX_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XZX_i` object.
             )rst")
        .def("toEulerAngles_XZY_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XZY_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XZY_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XZY_i` object.
             )rst")
        .def("toEulerAngles_YXY_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YXY_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YXY_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YXY_i` object.
             )rst")
        .def("toEulerAngles_YXZ_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YXZ_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YXZ_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YXZ_i` object.
             )rst")
        .def("toEulerAngles_YZX_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YZX_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YZX_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YZX_i` object.
             )rst")
        .def("toEulerAngles_YZY_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YZY_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YZY_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YZY_i` object.
             )rst")
        .def("toEulerAngles_ZXY_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZXY_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZXY_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZXY_i` object.
             )rst")
        .def("toEulerAngles_ZXZ_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZXZ_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZXZ_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZXZ_i` object.
             )rst")
        .def("toEulerAngles_ZYX_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZYX_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZYX_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZYX_i` object.
             )rst")
        .def("toEulerAngles_ZYZ_i", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZYZ_i>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZYZ_i` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZYZ_i` object.
             )rst")
        .def("toEulerAngles_XYX_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XYX_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XYX_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XYX_e` object.
             )rst")
        .def("toEulerAngles_XYZ_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XYZ_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XYZ_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XYZ_e` object.
             )rst")
        .def("toEulerAngles_XZX_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XZX_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XZX_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XZX_e` object.
             )rst")
        .def("toEulerAngles_XZY_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_XZY_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_XZY_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_XZY_e` object.
             )rst")
        .def("toEulerAngles_YXY_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YXY_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YXY_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YXY_e` object.
             )rst")
        .def("toEulerAngles_YXZ_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YXZ_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YXZ_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YXZ_e` object.
             )rst")
        .def("toEulerAngles_YZX_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YZX_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YZX_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YZX_e` object.
             )rst")
        .def("toEulerAngles_YZY_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_YZY_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_YZY_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_YZY_e` object.
             )rst")
        .def("toEulerAngles_ZXY_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZXY_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZXY_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZXY_e` object.
             )rst")
        .def("toEulerAngles_ZXZ_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZXZ_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZXZ_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZXZ_e` object.
             )rst")
        .def("toEulerAngles_ZYX_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZYX_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZYX_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZYX_e` object.
             )rst")
        .def("toEulerAngles_ZYZ_e", &Rotation::ABC::toEulerAngles<Rotation::EulerAnglesConvention_ZYZ_e>,
             R"rst(
             Convert current rotation object into a
             :py:class:`~Geometry.EulerAngles_ZYZ_e` object representing the
             same rotation.

             :return: a :py:class:`~Geometry.EulerAngles_ZYZ_e` object.
             )rst")
        .def_property_readonly("d1", &Rotation::ABC::d1,
                               R"rst(
                               The :math:`d_1` director.
                               :math:`d_1` correspond to the transformed vector
                               :math:`X = (1, 0, 0)` by the rotation such
                               that :math:`d_1 = R.X`
                               )rst")
        .def_property_readonly("d2", &Rotation::ABC::d2,
                               R"rst(
                               The :math:`d_2` director.
                               :math:`d_2` correspond to the transformed vector
                               :math:`Y = (0, 1, 0)` by the rotation such
                               that :math:`d_2 = R.Y`
                               )rst")
        .def_property_readonly("d3", &Rotation::ABC::d3,
                               R"rst(
                               The :math:`d_3` director.
                               :math:`d_3` correspond to the transformed vector
                               :math:`Z = (0, 0, 1)` by the rotation such
                               that :math:`d_3 = R.Z`
                               )rst")
        .def("getMatrix", &Rotation::ABC::getMatrix,
             R"rst(
             Return the equivalent rotation matrix.

             :return: the rotation matrix as a 3x3 :py:class:`numpy.ndarray`
             )rst")
    ;

    py::class_<Rotation::RotationMatrix, Rotation::ABC> R(m, "RotationMatrix") ;
    R.def(py::init<const Eigen::Matrix3d &>(),
          R"rst(
          Initialisation from a rotation matrix.

          :param mat: the rotation matrix a 3x3 :py:class:`numpy.ndarray`
          )rst")
        .def(py::init<>(),
             R"rst(
             Default initialisation.
             This initialize an identity rotation matrix: zero rotation.
             )rst")
        .def(py::init<const Rotation::RotationMatrix &>(),
             R"rst(
             Initialisation from another :py:class:`~Geometry.RotationMatrix`

             :param other: the other :py:class:`~Geometry.RotationMatrix`
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
        .def(py::init<const double &, const double &, const double &,
                      const double &, const double &, const double &,
                      const double &, const double &, const double &>(),
             R"rst(
             Initialisation by 9 values representing the rotation matrix
             components.
             The values are provided column after column (the directors one
             after the other).

             :param float R00: :math:`d_1.x`
             :param float R10: :math:`d_1.y`
             :param float R20: :math:`d_1.z`
             :param float R01: :math:`d_2.x`
             :param float R11: :math:`d_2.y`
             :param float R21: :math:`d_2.z`
             :param float R02: :math:`d_3.x`
             :param float R12: :math:`d_3.y`
             :param float R22: :math:`d_3.z`
             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Initialisation by a :py:class:`numpy.array` representing the 9
             rotation matrix components.
             The values are provided column after column (the directors one
             after the other).

             :param v: a 9 values :py:class:`numpy.array` containing
                 :math:`d_1`, :math:`d_2`, :math:`d_3`
             )rst")
        .def("transpose", &Rotation::RotationMatrix::transpose,
             R"rst(
             Return the transposed (inverse) :py:class:`~Geometry.RotationMatrix`.

             :return: the transposed :py:class:`~Geometry.RotationMatrix`
             )rst")
        .def("determinant", &Rotation::RotationMatrix::determinant,
             R"rst(
             Computes the determinant of the rotation matrix.

             :return: the determinant of the rotation matrix
             )rst")
        .def("get", py::overload_cast<const unsigned &, const unsigned &>(&Rotation::RotationMatrix::operator()),
             py::return_value_policy::copy,
             R"rst(
             Return the inner rotation matrix as a :py:class:`numpy.ndarray`

             :return: the rotation matrix as a :py:class:`numpy.ndarray`
             )rst")
    ;
    AddRotatorOperators_(R) ;

    py::class_<Rotation::BasisVectors, Rotation::ABC> bv(m, "BasisVectors") ;
    bv.def(py::init<const Vector &, const Vector &, const Vector &>(),
           R"rst(
           Initialisation with the directors vectors :math:`d_1`, :math:`d_2`,
           :math:`d_3`. See :py:meth:`d1`, :py:meth:`d2`, :py:meth:`d3` for
           more information on the directors.

           :param d1: a :py:class:`~Geometry.Vector` representing :math:`d_1`
           :param d2: a :py:class:`~Geometry.Vector` representing :math:`d_2`
           :param d3: a :py:class:`~Geometry.Vector` representing :math:`d_3`
           )rst")
        .def(py::init<const Eigen::Matrix3d &>(),
             R"rst(
             Initialisation with the rotation matrix representing the rotation.

             :param mat: the rotation matrix as a :py:class:`numpy.ndarray`.
             )rst")
        .def(py::init<const Eigen::Vector3d &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector3d &>(),
             R"rst(
             Initialisation with the directors vectors :math:`d_1`, :math:`d_2`,
             :math:`d_3`. See :py:meth:`d1`, :py:meth:`d2`, :py:meth:`d3` for
             more information on the directors.

             :param d1: a :py:class:`numpy.ndarray` representing :math:`d_1`
             :param d2: a :py:class:`numpy.ndarray` representing :math:`d_2`
             :param d3: a :py:class:`numpy.ndarray` representing :math:`d_3`
             )rst")
        .def(py::init<>(),
             R"rst(
             Default constructor, corresponding to the identity rotation.
             :math:`d_1` is initialised to :math:`(1, 0, 0)`
             :math:`d_2` is initialised to :math:`(0, 1, 0)`
             :math:`d_3` is initialised to :math:`(0, 0, 1)`
             )rst")
        .def(py::init<const Rotation::BasisVectors &>(),
             R"rst(
             Initialisation with another :py:class:`~Geometry.BasisVectors`.

             :param other: the other :py:class:`~Geometry.BasisVectors`.
             )rst")
        .def(py::init<const double &, const double &, const double &,
                      const double &, const double &, const double &,
                      const double &, const double &, const double &>(),
             R"rst(
             Initialisation by 9 values representing the rotation matrix
             components.
             The values are provided column after column (the directors one
             after the other).

             :param float R00: :math:`d_1.x`
             :param float R10: :math:`d_1.y`
             :param float R20: :math:`d_1.z`
             :param float R01: :math:`d_2.x`
             :param float R11: :math:`d_2.y`
             :param float R21: :math:`d_2.z`
             :param float R02: :math:`d_3.x`
             :param float R12: :math:`d_3.y`
             :param float R22: :math:`d_3.z`
             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Initialisation by a :py:class:`numpy.array` representing the 9
             rotation matrix components.
             The values are provided column after column (the directors one
             after the other).

             :param v: a 9 values :py:class:`numpy.array` containing
                 :math:`d_1`, :math:`d_2`, :math:`d_3`
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
    ;
    AddRotatorOperators_(bv) ;

    py::class_<Rotation::Quaternion, Rotation::ABC> q(m, "Quaternion") ;
    q.def(py::init<const double &, const double &, const double &, const double &>(),
          R"rst(
          Initialisation of the quaternion from values.
          Note that the quaternion is supposed to be normalised.

          :param w: the w coordinate.
          :param x: the x coordinate.
          :param y: the y coordinate.
          :param z: the z coordinate.
          )rst")
        .def(py::init<const Eigen::Matrix3d &>(),
             R"rst(
             Initialisation from a rotation matrix :math:`R`.
             A non-optimised algorithm is the following::

                 res = numpy.zeros(4, dtype=flaot)
                 if R.trace() > 0.:
                     t = sqrt(1. + R.trace())
                     res[0] = t
                     res[1] = (R[2,1] - R[1,2]) / t
                     res[2] = (R[0,2] - R[2,0]) / t
                     res[3] = (R[1,0] - R[0,1]) / t
                 elif (R[0,0]>R[1,1]) and (R[0,0]>R[2,2]):
                     t = sqrt(1. + R[0,0] - R[1,1] - R[2,2])
                     res[0] = (R[2,1] - R[1,2]) / t
                     res[1] = t
                     res[2] = (R[0,1] + R[1,0]) / t
                     res[3] = (R[0,2] + R[2,0]) / t
                 elif R[1,1] > R[2,2]:
                     t = sqrt(1. - R[0,0] + R[1,1] - R[2,2])
                     res[0] = (R[0,2] - R[2,0]) / t
                     res[1] = (R[0,1] + R[1,0]) / t
                     res[2] = t
                     res[3] = (R[1,2] + R[2,1]) / t
                 else:
                     t = sqrt(1. - R[0,0] - R[1,1] + R[2,2])
                     res[0] = (R[1,0] - R[0,1]) / t
                     res[1] = (R[0,2] + R[2,0]) / t
                     res[2] = (R[1,2] + R[2,1]) / t
                     res[3] = t ;
                 res *= 0.5
                 return res

             :param R: the rotation matrix provided as a :py:class:`numpy.ndarray`
             )rst")
        .def(py::init<>(),
             R"rst(
             Default initialisation of the quaternion to :math:`(1, 0, 0, 0)`
             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Default initialisation of the quaternion with a
             :py:class:`numpy.ndarray` of 4 values: w, x, y, z

             :param v: the :py:class:`numpy.ndarray` containing the w, x, y, z values.
             )rst")
        .def(py::init<const Rotation::Quaternion &>(),
             R"rst(
             Initialisation with another :py:class:`~Geometry.Quaternion`.

             :param other: the other :py:class:`~Geometry.Quaternion`.
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
        .def(py::init<const Eigen::Vector3d &, const Eigen::Vector3d &>(),
             R"rst(
             Initialisation of the quaternion from 2 arbitrary vectors.

             The rotation (:math:`R`) is constructed such that it represents
             a rotation between :math:`v_1` and :math:`v_2`. This means that
             :math:`v_1` is sent to :math:`v_2` such that:
             :math:`v_2 = R.v_1`
     
             Note that the two input vectors do **not** have to be normalized,
             and do not need to have the same norm.
     
             Here is an example of constructor implementation (with :math:`v_1`
             and :math:`v_2` as :py:class:`numpy.ndarray`)::

                 a = v1 / numpy.linalg.norm(v1)
                 b = v2 / numpy.linalg.norm(v2)
                 c = a.dot(b)
                 # the vectors are nearly along same direction... we must find
                 # a way to compute efficiently the rotation
                 if  c < -1.+1.e-12:
                     raise Exception("Unable to compute quaternion from two opposite vectors...")
                 else:
                     k = 1. + c
                     s = 1. / sqrt(k+k)
                     w = k*s
                     xyz = s*a.cross(b)

             )rst")
        .def(py::init<const Vector &, const Vector &>(),
             R"rst(Same as initialisation with 2 :py:class:`numpy.ndarray`)rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("w", [](const Rotation::Quaternion & r) {return r.w();},
                               R"rst(The w component)rst")
        .def_property_readonly("x", [](const Rotation::Quaternion & r) {return r.x();},
                               R"rst(The x component)rst")
        .def_property_readonly("y", [](const Rotation::Quaternion & r) {return r.y();},
                               R"rst(The y component)rst")
        .def_property_readonly("z", [](const Rotation::Quaternion & r) {return r.z();},
                               R"rst(The z component)rst")
#else
        .def_property_readonly("w", py::overload_cast<>(&Rotation::Quaternion::w, py::const_),
                               R"rst(The w component)rst")
        .def_property_readonly("x", py::overload_cast<>(&Rotation::Quaternion::x, py::const_),
                               R"rst(The x component)rst")
        .def_property_readonly("y", py::overload_cast<>(&Rotation::Quaternion::y, py::const_),
                               R"rst(The y component)rst")
        .def_property_readonly("z", py::overload_cast<>(&Rotation::Quaternion::z, py::const_),
                               R"rst(The z component)rst")
#endif
        .def("normalise", &Rotation::Quaternion::normalise,
             R"rst(Normalise the quaternion internally.)rst")
        .def("norm", &Rotation::Quaternion::norm,
             R"rst(
             Return the norm of the quaternion.

             :return: the norm of the quaternion.
             )rst")
        .def("normalised", &Rotation::Quaternion::normalised,
             R"rst(
             Return a copy of the normalised quaternion.

             :return: a new normalised quaternion.
             )rst")
        .def("slerp", &Rotation::Quaternion::slerp,
             R"rst(
             Interpolates between two quaternions.
        
             Uses SLERP interpolation to compute an interpolated quaternion :math:`q_2`
             from current quaternion :math:`q` and given quaternion in signature :math:`q_1`
        
             This represents an interpolation for a constant motion between :math:`q` and :math:`q_2`
             see also `wikipedia article <http://en.wikipedia.org/wiki/Slerp.>`_
        
             :param t: an interpolator factor t in [0;1]
             :param q2: :math:`q_2` the other quaternion
        
             :return: the interpolated quaternion.
             )rst")
    ;
    AddRotatorOperators_(q) ;

    py::class_<Rotation::AxisAndAngle, Rotation::ABC> aa(m, "AxisAndAngle") ;
    aa.def(py::init<const Vector &, const double &>(),
           R"rst(
           Initialisation with a *normalised* axis and an angle.

           :param axis: a normalised :py:class:`~Geometry.Vector`
           :param float angle: the angle 
           )rst")
        .def(py::init<const Eigen::Matrix3d &>(),
             R"rst(
             Initialisation with the rotation matrix representing the rotation.

             :param mat: the rotation matrix as a :py:class:`numpy.ndarray`.
             )rst")
        .def(py::init<>(),
             R"rst(
             Default initialisation.
             The angle is set to 0.
             The axis is set to :math:`(1, 0, 0)`
             )rst")
        .def(py::init<const Eigen::Vector3d &, const double &>(),
           R"rst(
           Initialisation with a *normalised* axis and an angle.

           :param axis: a normalised :py:class:`numpy.ndarray`
           :param float angle: the angle 
           )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Initialisation from a :py:class:`numpy.ndarray`
             The first 3 values are the axis components, the last value
             is the angle.

             :param values: the vector containing the axis and angle components
             )rst")
        .def(py::init<const Rotation::AxisAndAngle &>(),
             R"rst(
             Initialisation with another :py:class:`~Geometry.AxisAndAngle`.

             :param other: the other :py:class:`~Geometry.AxisAndAngle`.
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("angle", [](const Rotation::AxisAndAngle & r){return r.angle();},
                               R"rst(The angle component of the rotation)rst")
        .def_property_readonly("axis", [](const Rotation::AxisAndAngle & r){return r.axis();},
                               R"rst(The axis  component of the rotation)rst")
#else
        .def_property_readonly("angle", py::overload_cast<>(&Rotation::AxisAndAngle::angle, py::const_),
                               R"rst(The angle component of the rotation)rst")
        .def_property_readonly("axis", py::overload_cast<>(&Rotation::AxisAndAngle::axis, py::const_),
                               R"rst(The axis  component of the rotation)rst")
#endif
    ;
    AddRotatorOperators_(aa) ;

    AddEulerClass_<Rotation::EulerAnglesConvention_XYX_i>(m, "EulerAngles_XYX_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_XYZ_i>(m, "EulerAngles_XYZ_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_XZX_i>(m, "EulerAngles_XZX_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_XZY_i>(m, "EulerAngles_XZY_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YXY_i>(m, "EulerAngles_YXY_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YXZ_i>(m, "EulerAngles_YXZ_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YZX_i>(m, "EulerAngles_YZX_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YZY_i>(m, "EulerAngles_YZY_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZXY_i>(m, "EulerAngles_ZXY_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZXZ_i>(m, "EulerAngles_ZXZ_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZYX_i>(m, "EulerAngles_ZYX_i") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZYZ_i>(m, "EulerAngles_ZYZ_i") ;

    AddEulerClass_<Rotation::EulerAnglesConvention_XYX_e>(m, "EulerAngles_XYX_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_XYZ_e>(m, "EulerAngles_XYZ_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_XZX_e>(m, "EulerAngles_XZX_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_XZY_e>(m, "EulerAngles_XZY_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YXY_e>(m, "EulerAngles_YXY_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YXZ_e>(m, "EulerAngles_YXZ_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YZX_e>(m, "EulerAngles_YZX_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_YZY_e>(m, "EulerAngles_YZY_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZXY_e>(m, "EulerAngles_ZXY_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZXZ_e>(m, "EulerAngles_ZXZ_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZYX_e>(m, "EulerAngles_ZYX_e") ;
    AddEulerClass_<Rotation::EulerAnglesConvention_ZYZ_e>(m, "EulerAngles_ZYZ_e") ;

    py::class_<Rotation::MRP, Rotation::ABC> mrp(m, "MRP") ;
    mrp.def(py::init<const double &, const double &, const double &>(),
            R"rst(
            Initialisation from MRP components.
            Note that the MRP is initialised in unit circle.

            :param sigma1: the :math:`\sigma_1` component
            :param sigma2: the :math:`\sigma_2` component
            :param sigma3: the :math:`\sigma_3` component
            )rst")
        .def(py::init<const Eigen::Matrix3d &>(),
             R"rst(
             Initialisation from a rotation matrix.

             :param mat: the 3x3 :py:class:`numpy.ndarray` representing the
                 rotation matrix.
             )rst")
        .def(py::init<>(),
             R"rst(
             Default initialisation of the MRP to :math:`(0, 0, 0)`
             )rst")
        .def(py::init<const Rotation::MRP &>(),
             R"rst(
             Initialisation with another :py:class:`~Geometry.MRP`.

             :param other: the other :py:class:`~Geometry.MRP`.
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Default initialisation of the MRP with a
             :py:class:`numpy.ndarray` of 3 values: :math:`\sigma_1`,
             :math:`\sigma_2`, :math:`\sigma_3`.

             :param v: the :py:class:`numpy.ndarray` containing the
                 :math:`\sigma_1`, :math:`\sigma_2`, :math:`\sigma_3` values
             )rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("sigma1", [](const Rotation::MRP & r){return r.sigma1();},
                               R"rst(The :math:`\sigma_1` coordinate)rst")
        .def_property_readonly("sigma2", [](const Rotation::MRP & r){return r.sigma2();},
                               R"rst(The :math:`\sigma_2` coordinate)rst")
        .def_property_readonly("sigma3", [](const Rotation::MRP & r){return r.sigma3();},
                               R"rst(The :math:`\sigma_3` coordinate)rst")
#else
        .def_property_readonly("sigma1", py::overload_cast<>(&Rotation::MRP::sigma1, py::const_),
                               R"rst(The :math:`\sigma_1` coordinate)rst")
        .def_property_readonly("sigma2", py::overload_cast<>(&Rotation::MRP::sigma2, py::const_),
                               R"rst(The :math:`\sigma_2` coordinate)rst")
        .def_property_readonly("sigma3", py::overload_cast<>(&Rotation::MRP::sigma3, py::const_),
                               R"rst(The :math:`\sigma_3` coordinate)rst")
#endif
        .def("norm", &Rotation::MRP::norm,
             R"rst(
             Computes the norm of the MRP vector.

             :return: the norm of the MRP.
             :rtype: float
             )rst")
        .def("shadowRepr", &Rotation::MRP::shadowRepr,
             R"rst(
             Convert the MRP representation into its shadow representation.

             The shadow representation yields:

             .. math::

                 \sigma^S = \frac{-\sigma}{|\sigma|^2}

             :return: the shadow MRP representation.
             :rtype: :py:class:`numpy.ndarray`
             )rst")
    ;
    AddRotatorOperators_(mrp) ;
     
    py::class_<Rotation::RotationVector, Rotation::ABC> rtv(m, "RotationVector") ;
    rtv.def(py::init<const double &, const double &, const double &>(),
            R"rst(
            Initialisation from MRP components.
            Note that the MRP is initialised in unit circle.

            :param sigma1: the :math:`\sigma_1` component
            :param sigma2: the :math:`\sigma_2` component
            :param sigma3: the :math:`\sigma_3` component
            )rst")
        .def(py::init<const Eigen::Matrix3d &>(),
             R"rst(
             Initialisation from a rotation matrix.

             :param mat: the 3x3 :py:class:`numpy.ndarray` representing the
                 rotation matrix.
             )rst")
        .def(py::init<>(),
             R"rst(
             Default initialisation of the MRP to :math:`(0, 0, 0)`
             )rst")
        .def(py::init<const Rotation::RotationVector &>(),
             R"rst(
             Initialisation with another :py:class:`~Geometry.MRP`.

             :param other: the other :py:class:`~Geometry.MRP`.
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Default initialisation of the MRP with a
             :py:class:`numpy.ndarray` of 3 values: :math:`\sigma_1`,
             :math:`\sigma_2`, :math:`\sigma_3`.

             :param v: the :py:class:`numpy.ndarray` containing the
                 :math:`\sigma_1`, :math:`\sigma_2`, :math:`\sigma_3` values
             )rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("theta1", [](const Rotation::RotationVector & r){return r.theta1();},
                               R"rst(The :math:`\theta_1` coordinate)rst")
        .def_property_readonly("theta2", [](const Rotation::RotationVector & r){return r.theta2();},
                               R"rst(The :math:`\theta_2` coordinate)rst")
        .def_property_readonly("theta3", [](const Rotation::RotationVector & r){return r.theta3();},
                               R"rst(The :math:`\theta_3` coordinate)rst")
#else
        .def_property_readonly("theta1", py::overload_cast<>(&Rotation::RotationVector::theta1, py::const_),
                               R"rst(The :math:`\sigma_1` coordinate)rst")
        .def_property_readonly("theta2", py::overload_cast<>(&Rotation::RotationVector::theta2, py::const_),
                               R"rst(The :math:`\sigma_2` coordinate)rst")
        .def_property_readonly("theta3", py::overload_cast<>(&Rotation::RotationVector::theta3, py::const_),
                               R"rst(The :math:`\sigma_3` coordinate)rst")
#endif
        .def("norm", &Rotation::RotationVector::norm,
             R"rst(
             Computes the norm of the MRP vector.

             :return: the norm of the MRP.
             :rtype: float
             )rst")
        .def_property_readonly("state", &Rotation::RotationVector::getState)
    ;
    AddRotatorOperators_(rtv) ;

    py::class_<Rotation::HorizontalPlane, Rotation::ABC> hp(m, "HorizontalPlane") ;
    hp.def(py::init<const double &>(),
           R"rst(
           Initialisation of the rotation given an angle.

           :param float angle: the angle of rotation in radians.
           )rst")
        .def(py::init<const Eigen::Matrix3d &>(),
           R"rst(
           Initialisation of the rotation with a rotation matrix.

           :param mat: the rotation matrix.
           :paramtype mat: :py:class:`numpy.ndarray`
           )rst")
        .def(py::init<>(),
           R"rst(
           Initialisation of the rotation with a null rotation angle.
           )rst")
        .def(py::init<const Eigen::VectorXd &>(),
           R"rst(
           Initialisation of the rotation with a one value vector.

           :param v: the one valued vector containing the angle of rotation.
           :paramtype v: :py:class:`numpy.ndarray`
           )rst")
        .def(py::init<const Rotation::HorizontalPlane &>(),
             R"rst(
             Initialisation with another :py:class:`~Geometry.HorizontalPlane`.

             :param other: the other :py:class:`~Geometry.HorizontalPlane`.
             )rst")
        .def(py::init<const Rotation::ABC &>(),
             R"rst(
             Initialisation from another :ref:`rotation object<GeometryRotation>`.

             :param other: the other :ref:`rotation object<GeometryRotation>`.
             )rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("angle", [](const Rotation::HorizontalPlane & r){return r.angle();},
                               R"rst(The angle of rotation)rst")
#else
        .def_property_readonly("angle", py::overload_cast<>(&Rotation::HorizontalPlane::angle, py::const_),
                               R"rst(The angle of rotation)rst")
#endif
    ;
    AddRotatorOperators_(hp) ;
}

} // End of namespace Geometry
} // End of namespace PythonInterface
} // End of namespace BV

