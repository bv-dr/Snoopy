#pragma once

#include <Eigen/Dense>
#include <pybind11/operators.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "Geometry/Translation.hpp"

namespace BV {
namespace PythonInterface {
namespace Geometry {

using namespace BV::Geometry ;
namespace py = pybind11 ;

template <typename Scope>
void AddTranslatorOperator_(Scope & scope)
{
    using TranslatorType = typename Scope::type ;
    scope.def("__imul__",
              [](TranslatorType & t, const Translation::ABC & abc)
              {
                  return t *= abc ;
              },
              R"rst(
              Applies a 3D translation defined by another
              :ref:`translation object<GeometryTranslation>`.

              :param other: the other translation object
              :paramtype other: :ref:`translation object<GeometryTranslation>`.
              :return: the modified object.
              )rst")
        .def("__mul__",
             [](TranslatorType & t, const Translation::ABC & abc)
             {
                 return t * abc ;
             },
             R"rst(
             Returns a copy to which a 3D translation defined by another
             :ref:`translation object<GeometryTranslation>` is applied.

             :param other: the other translation object
             :paramtype other: :ref:`translation object<GeometryTranslation>`.
             :return: a new translation object.
             )rst")
        .def("__mul__",
             [](TranslatorType & t, const Eigen::Vector3d & v)
             {
                 return t * v ;
             },
             R"rst(
             Returns a copy to which a 3D translation defined by a
             :py:class:`numpy.ndarray` is applied.

             :param v: the numpy array
             :paramtype v: :py:class:`numpy.ndarray`
             :return: a new object.
             )rst")
        .def("__isub__",
             [](TranslatorType & t, const Translation::ABC & abc)
             {
                 return t -= abc ;
             },
             R"rst(
             Substract a 3D translation defined by another
             :ref:`translation object<GeometryTranslation>`.

             :param other: the other translation object
             :paramtype other: :ref:`translation object<GeometryTranslation>`.
             :return: the modified object.
             )rst")
        .def("__sub__",
             [](TranslatorType & t, const Translation::ABC & abc)
             {
                 return t - abc ;
             },
             R"rst(
             Substract a 3D translation defined by another
             :ref:`translation object<GeometryTranslation>` to a copy.

             :param other: the other translation object
             :paramtype other: :ref:`translation object<GeometryTranslation>`.
             :return: a new object.
             )rst")
        .def("__iadd__",
             [](TranslatorType & t, const Translation::ABC & abc)
             {
                 return t += abc ;
             },
             R"rst(
             Add a 3D translation defined by another
             :ref:`translation object<GeometryTranslation>`.

             :param other: the other translation object
             :paramtype other: :ref:`translation object<GeometryTranslation>`.
             :return: the modified object.
             )rst")
        .def("__add__",
             [](TranslatorType & t, const Translation::ABC & abc)
             {
                 return t + abc ;
             },
             R"rst(
             Add a 3D translation defined by another
             :ref:`translation object<GeometryTranslation>` to a copy.

             :param other: the other translation object
             :paramtype other: :ref:`translation object<GeometryTranslation>`.
             :return: a new object.
             )rst")
        .def("__neg__",
             [](TranslatorType & t)
             {
                 return -t ;
             },
             R"rst(
             Returns a new object representing the inverse translation.

             :return: a new object.
             )rst")
        .def("getInversed", &TranslatorType::getInversed,
             R"rst(Same as :py:meth:`__neg__`)rst")
    ;
}

template <int LatitudeConvEnum>
void AddSphericalClass_(py::module & m, const char * pythonName)
{
    py::class_<Translation::Spherical<LatitudeConvEnum>, Translation::ABC> sph(m, pythonName) ;
    sph.def(py::init<const double &, const double &, const double &>(),
            R"rst(
            Initialisation of the spherical translation given an initial
            3D position with three values.

            :param float r: the radius
            :param float theta: the longitude angle :math:`\theta`.
            :param float phi: the (co)latitude angle :math:`\phi`.
            )rst")
        .def(py::init<const Vector &>(),
             R"rst(
             Initialisation of the spherical translation given a
             :py:class:`~Geometry.Vector` containing r, :math:`\theta`,
             :math:`\phi`.

             :param v: a vector providing r, :math:`\theta`, :math:`\phi`. 
             :paramtype v: :py:class:`~Geometry.Vector`
             )rst")
        .def(py::init<const Point &>(),
             R"rst(
             Initialisation of the spherical translation given a
             :py:class:`~Geometry.Point` containing r, :math:`\theta`,
             :math:`\phi`.

             :param pt: a point providing r, :math:`\theta`, :math:`\phi`. 
             :paramtype pt: :py:class:`~Geometry.Point`
             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Initialisation of the spherical translation given a
             :py:class:`numpy.ndarray` containing r, :math:`\theta`,
             :math:`\phi`.

             :param v: a :py:class:`numpy.ndarray` providing r, :math:`\theta`,
                 :math:`\phi`. 
             :paramtype pt: :py:class:`numpy.ndarray`
             )rst")
        .def(py::init<const Translation::ABC &>(),
             R"rst(
             Initialisation of the spherical translation given another
             :ref:`translation object<GeometryTranslation>`.

             :param other: a :ref:`translation object<GeometryTranslation>` 
             :paramtype other: :ref:`translation object<GeometryTranslation>`
             )rst")
        .def(py::init<const Translation::Spherical<LatitudeConvEnum> &>(),
             R"rst(
             Initialisation of the spherical translation given another
             spherical translation object.

             :param other: a spherical translation object
             )rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("r", [](const Translation::Spherical<LatitudeConvEnum> & t){return t.r();},
                               R"rst(The radius r of the translation)rst")
        .def_property_readonly("theta", [](const Translation::Spherical<LatitudeConvEnum> & t){return t.theta();},
                               R"rst(The longitude angle :math:`\theta`)rst")
        .def_property_readonly("phi", [](const Translation::Spherical<LatitudeConvEnum> & t){return t.phi();},
                               R"rst(The (co)latitude angle :math:`\phi`)rst")
#else
        .def_property_readonly("r", py::overload_cast<>(&Translation::Spherical<LatitudeConvEnum>::r, py::const_),
                               R"rst(The radius r of the translation)rst")
        .def_property_readonly("theta", py::overload_cast<>(&Translation::Spherical<LatitudeConvEnum>::theta, py::const_),
                               R"rst(The longitude angle :math:`\theta`)rst")
        .def_property_readonly("phi", py::overload_cast<>(&Translation::Spherical<LatitudeConvEnum>::phi, py::const_),
                               R"rst(The (co)latitude angle :math:`\phi`)rst")
#endif
        .def("uR", &Translation::Spherical<LatitudeConvEnum>::uR)
        .def("uTheta", &Translation::Spherical<LatitudeConvEnum>::uTheta)
        .def("uPhi", &Translation::Spherical<LatitudeConvEnum>::uPhi)
    ;
    AddTranslatorOperator_(sph) ;
}

void exportTranslationModule(py::module & m)
{
    py::enum_<TranslatorTypeEnum>(m, "TranslatorTypeEnum")
        .value("CARTESIAN", TranslatorTypeEnum::CARTESIAN)//, R"rst(The :py:class:`~Geometry.Cartesian` translation object enum value)rst")
        .value("SPHERICAL_COLAT", TranslatorTypeEnum::SPHERICAL_COLAT)//, R"rst(The :py:class:`~Geometry.Spherical_CoLat` translation object enum value)rst")
        .value("SPHERICAL_LAT", TranslatorTypeEnum::SPHERICAL_LAT)//, R"rst(The :py:class:`~Geometry.Spherical_Lat` translation object enum value)rst")
    ;

    py::class_<Translation::ABC>(m, "_GeometryTranslationABC")
        .def_property("unknowns",
#ifdef __INTEL_COMPILER
                      [](Translation::ABC & t){return t.unknowns();},
                      [](Translation::ABC & t, const Eigen::VectorXd & u){t.unknowns(u);},
                      R"rst(The :py:class:`numpy.ndarray` of translation unknowns)rst")
#else
                      py::overload_cast<>(&Translation::ABC::unknowns, py::const_),
                      py::overload_cast<const Eigen::VectorXd &>(&Translation::ABC::unknowns),
                      R"rst(The :py:class:`numpy.ndarray` of translation unknowns)rst")
#endif
        .def_property_readonly("constraints", &Translation::ABC::constraints,
                               R"rst(
                        The :py:class:`numpy.ndarray` containing the constraints
                               )rst")
        .def("nUnknowns", &Translation::ABC::nUnknowns, py::return_value_policy::copy,
             R"rst(The number of unknowns)rst")
        .def("nConstraints", &Translation::ABC::nConstraints, py::return_value_policy::copy,
             R"rst(The number of constraints)rst")
        .def("inverse", &Translation::ABC::inverse,
             R"rst(Inverse the translation.)rst")
        .def("addTranslation", py::overload_cast<const Translation::ABC &>(&Translation::ABC::addTranslation),
             R"rst(
             Add the components of the other
             :ref:`translation object<GeometryTranslation>`.

             :param other: the other :ref:`translation object<GeometryTranslation>`.
             :paramtype other: :ref:`translation object<GeometryTranslation>`
             )rst")
        .def("addTranslation", py::overload_cast<const double &, const double &, const double &>(&Translation::ABC::addTranslation),
             R"rst(
             Add the provided components.

             :param x: the x component to add.
             :paramtype x: float
             :param y: the y component to add.
             :paramtype y: float
             :param z: the z component to add.
             :paramtype z: float
             )rst")
        .def("addTranslation", py::overload_cast<const Eigen::Vector3d &>(&Translation::ABC::addTranslation),
             R"rst(
             Add the components of the :py:class:`numpy.ndarray`.

             :param v: the :py:class:`numpy.ndarray` containing the components to add.
             :paramtype v: :py:class:`numpy.ndarray`
             )rst")
        .def("addTranslation", py::overload_cast<const Point &>(&Translation::ABC::addTranslation),
             R"rst(
             Add the components of the :py:class:`~Geometry.Point`.

             :param pt: the :py:class:`~Geometry.Point` to add.
             :paramtype pt: :py:class:`~Geometry.Point`
             )rst")
        .def("addTranslation", py::overload_cast<const Vector &>(&Translation::ABC::addTranslation),
             R"rst(
             Add the components of the :py:class:`~Geometry.Vector`.

             :param pt: the :py:class:`~Geometry.Vector` to add.
             :paramtype pt: :py:class:`~Geometry.Vector`
             )rst")
        .def("subtractTranslation", py::overload_cast<const Translation::ABC &>(&Translation::ABC::subtractTranslation),
             R"rst(
             Substract the components of the other
             :ref:`translation object<GeometryTranslation>`.

             :param other: the other :ref:`translation object<GeometryTranslation>`.
             :paramtype other: :ref:`translation object<GeometryTranslation>`
             )rst")
        .def("subtractTranslation", py::overload_cast<const double &, const double &, const double &>(&Translation::ABC::subtractTranslation),
             R"rst(
             Substract the provided components.

             :param x: the x component to subtract.
             :paramtype x: float
             :param y: the y component to subtract.
             :paramtype y: float
             :param z: the z component to subtract.
             :paramtype z: float
             )rst")
        .def("subtractTranslation", py::overload_cast<const Eigen::Vector3d &>(&Translation::ABC::subtractTranslation),
             R"rst(
             Substract the components of the :py:class:`numpy.ndarray`.

             :param v: the :py:class:`numpy.ndarray` containing the components to subtract.
             :paramtype v: :py:class:`numpy.ndarray`
             )rst")
        .def("subtractTranslation", py::overload_cast<const Point &>(&Translation::ABC::subtractTranslation),
             R"rst(
             Substract the components of the :py:class:`~Geometry.Point`.

             :param pt: the :py:class:`~Geometry.Point` to subtract.
             :paramtype pt: :py:class:`~Geometry.Point`
             )rst")
        .def("subtractTranslation", py::overload_cast<const Vector &>(&Translation::ABC::subtractTranslation),
             R"rst(
             Substract the components of the :py:class:`~Geometry.Vector`.

             :param pt: the :py:class:`~Geometry.Vector` to subtract.
             :paramtype pt: :py:class:`~Geometry.Vector`
             )rst")
        .def_property("center", &Translation::ABC::toPoint, py::overload_cast<const Point &>(&Translation::ABC::set),
             R"rst(Get the translation object center as a :py:class:`~Geometry.Point`.)rst")
        .def("__mul__",
             [](const Translation::ABC & t, const Vector & v)
             {
                 return t * v ;
             },
             R"rst(
             Translate the provided :py:class:`~Geometry.Vector` by adding
             the translation object components.

             :param v: the :py:class:`~Geometry.Vector` to be translated.
             :paramtype v: :py:class:`~Geometry.Vector`
             :return: a new :py:class:`~Geometry.Vector` translated.
             )rst")
        .def("__mul__",
             [](const Translation::ABC & t, const Point & pt)
             {
                 return t * pt ;
             },
             R"rst(
             Translate the provided :py:class:`~Geometry.Point` by adding
             the translation object components.

             :param v: the :py:class:`~Geometry.Point` to be translated.
             :paramtype v: :py:class:`~Geometry.Point`
             :return: a new :py:class:`~Geometry.Point` translated.
             )rst")
        .def("translation", py::overload_cast<const Vector &>(&Translation::ABC::translation, py::const_),
             R"rst(
             Translate the provided :py:class:`~Geometry.Vector` by adding
             the translation object components.

             :param v: the :py:class:`~Geometry.Vector` to be translated.
             :paramtype v: :py:class:`~Geometry.Vector`
             :return: a new :py:class:`~Geometry.Vector` translated.
             )rst")
        .def("translation", py::overload_cast<const Point &>(&Translation::ABC::translation, py::const_),
             R"rst(
             Translate the provided :py:class:`~Geometry.Point` by adding
             the translation object components.

             :param v: the :py:class:`~Geometry.Point` to be translated.
             :paramtype v: :py:class:`~Geometry.Point`
             :return: a new :py:class:`~Geometry.Point` translated.
             )rst")
        .def("translation", py::overload_cast<const Eigen::Vector3d &>(&Translation::ABC::translation, py::const_),
             R"rst(
             Translate the provided :py:class:`numpy.ndarray` by adding
             the translation object components.

             :param v: the :py:class:`numpy.ndarray` to be translated.
             :paramtype v: :py:class:`numpy.ndarray`
             :return: a new :py:class:`numpy.ndarray` translated.
             )rst")
        .def("inverseTranslation", py::overload_cast<const Vector &>(&Translation::ABC::inverseTranslation, py::const_),
             R"rst(
             Translate the provided :py:class:`~Geometry.Vector` by subtracting 
             the translation object components.

             :param v: the :py:class:`~Geometry.Vector` to be translated.
             :paramtype v: :py:class:`~Geometry.Vector`
             :return: a new :py:class:`~Geometry.Vector` translated.
             )rst")
        .def("inverseTranslation", py::overload_cast<const Point &>(&Translation::ABC::inverseTranslation, py::const_),
             R"rst(
             Translate the provided :py:class:`~Geometry.Point` by subtracting 
             the translation object components.

             :param v: the :py:class:`~Geometry.Point` to be translated.
             :paramtype v: :py:class:`~Geometry.Point`
             :return: a new :py:class:`~Geometry.Point` translated.
             )rst")
        .def("inverseTranslation", py::overload_cast<const Eigen::Vector3d &>(&Translation::ABC::inverseTranslation, py::const_),
             R"rst(
             Translate the provided :py:class:`numpy.ndarray` by subtracting 
             the translation object components.

             :param v: the :py:class:`numpy.ndarray` to be translated.
             :paramtype v: :py:class:`numpy.ndarray`
             :return: a new :py:class:`numpy.ndarray` translated.
             )rst")
        .def("toCartesian", &Translation::ABC::toCartesian,
             R"rst(
             Convert the :ref:`translation object<GeometryTranslation>` into
             a :py:class:`~Geometry.Cartesian` translation object.

             :return: the new :py:class:`~Geometry.Cartesian` translation object
             )rst")
        .def("toSpherical_CoLat", &Translation::ABC::toSpherical<Translation::LatitudeConvention::CO_LATITUDE>,
             R"rst(
             Convert the :ref:`translation object<GeometryTranslation>` into
             a :py:class:`~Geometry.Spherical_CoLat` translation object.

             :return: the new :py:class:`~Geometry.Spherical_CoLat` translation object
             )rst")
        .def("toSpherical_Lat", &Translation::ABC::toSpherical<Translation::LatitudeConvention::LATITUDE>,
             R"rst(
             Convert the :ref:`translation object<GeometryTranslation>` into
             a :py:class:`~Geometry.Spherical_Lat` translation object.

             :return: the new :py:class:`~Geometry.Spherical_Lat` translation object
             )rst")
        .def("toHorizontal", &Translation::ABC::toHorizontal,
             R"rst(
             Convert the :ref:`translation object<GeometryTranslation>` into
             a :py:class:`~Geometry.Horizontal` translation object.

             :return: the new :py:class:`~Geometry.Horizontal` translation object
             )rst")
        .def("toPoint", &Translation::ABC::toPoint,
             R"rst(
             Convert the :ref:`translation object<GeometryTranslation>` into
             a :py:class:`~Geometry.Point`.

             :return: the new :py:class:`~Geometry.Point`
             )rst")
        .def("toVector", &Translation::ABC::toVector,
             R"rst(
             Convert the :ref:`translation object<GeometryTranslation>` into
             a :py:class:`~Geometry.Vector`.

             :return: the new :py:class:`~Geometry.Vector`
             )rst")
    ;

    py::class_<Translation::Cartesian, Translation::ABC> cart(m, "Cartesian") ;
    cart.def(py::init<const double &, const double &, const double &>(),
             R"rst(
             Initialisation of the cartesian translation given an initial
             3D position with three values.

             :param float x: the global axis system x position 
             :param float y: the global axis system y position 
             :param float z: the global axis system z position 

             )rst")
        .def(py::init<>(),
             R"rst(
             Initialisation of the cartesian translation at the center
             of the global axis system.
             )rst")
        .def(py::init<const Vector &>(),
             R"rst(
             Initialisation of the cartesian translation given a
             :py:class:`~Geometry.Vector` of global axis system positions.

             :param v: a vector providing the global axis system position.
             :paramtype v: :py:class:`~Geometry.Vector`

             )rst")
        .def(py::init<const Point &>(),
             R"rst(
             Initialisation of the cartesian translation given a
             :py:class:`~Geometry.Point` giving the global axis system position.

             :param p: a point providing the global axis system position.
             :paramtype p: :py:class:`~Geometry.Point`

             )rst")
        .def(py::init<const Eigen::VectorXd &>(),
             R"rst(
             Initialisation of the cartesian translation given a
             :py:class:`numpy.ndarray` containing the global axis system
             position.

             :param v: a vector providing the global axis system position.
             :paramtype v: :py:class:`numpy.ndarray`

             )rst")
        .def(py::init<const Translation::ABC &>(),
             R"rst(
             Initialisation of the cartesian translation with another
             :ref:`translation object<GeometryTranslation>`.

             :param other: the other translation.
             :paramtype other: :ref:`translation object<GeometryTranslation>` 

             )rst")
#ifdef __INTEL_COMPILER
        .def_property_readonly("x", [](const Translation::Cartesian & t){return t.x();},
                               R"rst(The x coordinate)rst")
        .def_property_readonly("y", [](const Translation::Cartesian & t){return t.y();},
                               R"rst(The y coordinate)rst")
        .def_property_readonly("z", [](const Translation::Cartesian & t){return t.z();},
                               R"rst(The z coordinate)rst")
#else
        .def_property_readonly("x", py::overload_cast<>(&Translation::Cartesian::x, py::const_),
                               R"rst(The x coordinate)rst")
        .def_property_readonly("y", py::overload_cast<>(&Translation::Cartesian::y, py::const_),
                               R"rst(The y coordinate)rst")
        .def_property_readonly("z", py::overload_cast<>(&Translation::Cartesian::z, py::const_),
                               R"rst(The z coordinate)rst")
#endif
    ;
    AddTranslatorOperator_(cart) ;

    AddSphericalClass_<Translation::LatitudeConvention::CO_LATITUDE>(m, "Spherical_CoLat") ;
    AddSphericalClass_<Translation::LatitudeConvention::LATITUDE>(m, "Spherical_Lat") ;

}

} // End of namespace Geometry
} // End of namespace PythonInterface
} // End of namespace BV

