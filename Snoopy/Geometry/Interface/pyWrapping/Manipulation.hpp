#pragma once

#include "Geometry/sx3Manipulation.hpp"

namespace BV {
namespace PythonInterface {
namespace Geometry {

using namespace BV::Geometry ;
namespace py = pybind11 ;

void exportManipulationModule(py::module & m)
{
    // sx3Manipulation
    m.def("R3Toso3", py::overload_cast<const double &, const double &, const double &>(&R3Toso3),
          R"rst(
          Conversion of a 3 value vector into so(3) space (anti-symmetric matrices)

          .. math::

              so(3) = { S in \mathcal{R}^{n \times n} : S^T = -S }

          )rst") ;
    m.def("R3Toso3", py::overload_cast<const Eigen::Vector3d &>(&R3Toso3)) ;
    m.def("R3Toso3", py::overload_cast<const Vector &>(&R3Toso3)) ;
    m.def("R6Tose3", py::overload_cast<const double &, const double &, const double &,
                                       const double &, const double &, const double &>(&R6Tose3)) ;
    m.def("R6Tose3", py::overload_cast<const Eigen::Vector3d &, const Eigen::Vector3d &>(&R6Tose3)) ;
    m.def("R6Tose3", py::overload_cast<const Vector &, const Vector &>(&R6Tose3)) ;
    m.def("R6Tose3", py::overload_cast<const Eigen::Matrix<double, 6, 1> &>(&R6Tose3)) ;
    m.def("so3ToR3", &so3ToR3) ;
    m.def("se3ToR6", &se3ToR6) ;
}

} // End of namespace Geometry
} // End of namespace PythonInterface
} // End of namespace BV

