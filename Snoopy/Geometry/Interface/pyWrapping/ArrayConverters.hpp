#pragma once

#include "Geometry/Rotation/ArrayConverters.hpp"

namespace BV {
namespace PythonInterface {
namespace Geometry {

using namespace BV::Geometry ;
namespace py = pybind11 ;

template <typename ConvIn>
void AddEulerArrayConverter_(py::module & m, const std::string nameIn)
{
    m.def(("EulerAngle"+nameIn+"ToAxisAndAngle").c_str(), &Rotation::Details::EulerAngleToAxisAndAngle<ConvIn>) ;
    m.def(("EulerAngle"+nameIn+"ToRotationMatrix").c_str(), &Rotation::Details::EulerAngleToRotationMatrix<ConvIn>) ;
    m.def(("EulerAngle"+nameIn+"ToBasisVectors").c_str(), &Rotation::Details::EulerAngleToBasisVectors<ConvIn>) ;
    m.def(("EulerAngle"+nameIn+"ToQuaternion").c_str(), &Rotation::Details::EulerAngleToQuaternion<ConvIn>) ;
    m.def(("EulerAngle"+nameIn+"ToMRP").c_str(), &Rotation::Details::EulerAngleToMRP<ConvIn>) ;
    m.def(("EulerAngle"+nameIn+"ToRotationVector").c_str(), &Rotation::Details::EulerAngleToRotationVector<ConvIn>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXYXi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXZXi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYXYi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYZYi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZXZi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZYZi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXYZi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXZYi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYXZi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYZXi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZXYi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZYXi").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXYXe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXZXe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYXYe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYZYe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZXZe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZYZe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXYZe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleXZYe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYXZe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleYZXe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZXYe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def(("EulerAngle"+nameIn+"ToEulerAngleZYXe").c_str(), &Rotation::Details::EulerAngleToEulerAngle<ConvIn, Rotation::EulerAnglesConvention_ZYX_e>) ;
}

void exportArrayConvertersModule(py::module & m)
{
    // Rotation matrix to other array converters
	m.def("EulerAngleXYZeToEulerSmallAngleContinuity", &Rotation::Details::EulerAngleXYZeToEulerSmallAngleContinuity);
	m.def("EulerAngleXYZiToEulerSmallAngleContinuity", &Rotation::Details::EulerAngleXYZiToEulerSmallAngleContinuity);
    m.def("RotationMatrixToQuaternion", &Rotation::Details::RotationMatrixToQuaternion) ;
    m.def("RotationMatrixToBasisVectors", &Rotation::Details::RotationMatrixToBasisVectors) ;
    m.def("RotationMatrixToAxisAndAngle", &Rotation::Details::RotationMatrixToAxisAndAngle) ;
    m.def("RotationMatrixToMRP", &Rotation::Details::RotationMatrixToMRP) ;
    m.def("RotationMatrixToRotationVector", &Rotation::Details::RotationMatrixToRotationVector) ;
    m.def("RotationMatrixToEulerAngleXYXi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def("RotationMatrixToEulerAngleXZXi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def("RotationMatrixToEulerAngleYXYi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def("RotationMatrixToEulerAngleYZYi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def("RotationMatrixToEulerAngleZXZi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def("RotationMatrixToEulerAngleZYZi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def("RotationMatrixToEulerAngleXYZi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def("RotationMatrixToEulerAngleXZYi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def("RotationMatrixToEulerAngleYXZi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def("RotationMatrixToEulerAngleYZXi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def("RotationMatrixToEulerAngleZXYi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def("RotationMatrixToEulerAngleZYXi", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def("RotationMatrixToEulerAngleXYXe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def("RotationMatrixToEulerAngleXZXe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def("RotationMatrixToEulerAngleYXYe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def("RotationMatrixToEulerAngleYZYe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def("RotationMatrixToEulerAngleZXZe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def("RotationMatrixToEulerAngleZYZe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def("RotationMatrixToEulerAngleXYZe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def("RotationMatrixToEulerAngleXZYe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def("RotationMatrixToEulerAngleYXZe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def("RotationMatrixToEulerAngleYZXe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def("RotationMatrixToEulerAngleZXYe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def("RotationMatrixToEulerAngleZYXe", &Rotation::Details::RotationMatrixToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>) ;

    // Basis vectors to other array converters
    m.def("BasisVectorsToQuaternion", &Rotation::Details::BasisVectorsToQuaternion) ;
    m.def("BasisVectorsToRotationMatrix", &Rotation::Details::BasisVectorsToRotationMatrix) ;
    m.def("BasisVectorsToAxisAndAngle", &Rotation::Details::BasisVectorsToAxisAndAngle) ;
    m.def("BasisVectorsToMRP", &Rotation::Details::BasisVectorsToMRP) ;
    m.def("BasisVectorsToRotationVector", &Rotation::Details::BasisVectorsToRotationVector) ;
    m.def("BasisVectorsToEulerAngleXYXi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def("BasisVectorsToEulerAngleXZXi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def("BasisVectorsToEulerAngleYXYi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def("BasisVectorsToEulerAngleYZYi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def("BasisVectorsToEulerAngleZXZi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def("BasisVectorsToEulerAngleZYZi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def("BasisVectorsToEulerAngleXYZi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def("BasisVectorsToEulerAngleXZYi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def("BasisVectorsToEulerAngleYXZi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def("BasisVectorsToEulerAngleYZXi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def("BasisVectorsToEulerAngleZXYi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def("BasisVectorsToEulerAngleZYXi", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def("BasisVectorsToEulerAngleXYXe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def("BasisVectorsToEulerAngleXZXe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def("BasisVectorsToEulerAngleYXYe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def("BasisVectorsToEulerAngleYZYe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def("BasisVectorsToEulerAngleZXZe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def("BasisVectorsToEulerAngleZYZe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def("BasisVectorsToEulerAngleXYZe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def("BasisVectorsToEulerAngleXZYe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def("BasisVectorsToEulerAngleYXZe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def("BasisVectorsToEulerAngleYZXe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def("BasisVectorsToEulerAngleZXYe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def("BasisVectorsToEulerAngleZYXe", &Rotation::Details::BasisVectorsToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>) ;

    // Axis and Angle to other array converters
    m.def("AxisAndAngleToQuaternion", &Rotation::Details::AxisAndAngleToQuaternion) ;
    m.def("AxisAndAngleToRotationMatrix", &Rotation::Details::AxisAndAngleToRotationMatrix) ;
    m.def("AxisAndAngleToBasisVectors", &Rotation::Details::AxisAndAngleToBasisVectors) ;
    m.def("AxisAndAngleToMRP", &Rotation::Details::AxisAndAngleToMRP) ;
    m.def("AxisAndAngleToRotationVector", &Rotation::Details::AxisAndAngleToRotationVector) ;
    m.def("AxisAndAngleToEulerAngleXYXi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def("AxisAndAngleToEulerAngleXZXi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def("AxisAndAngleToEulerAngleYXYi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def("AxisAndAngleToEulerAngleYZYi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def("AxisAndAngleToEulerAngleZXZi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def("AxisAndAngleToEulerAngleZYZi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def("AxisAndAngleToEulerAngleXYZi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def("AxisAndAngleToEulerAngleXZYi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def("AxisAndAngleToEulerAngleYXZi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def("AxisAndAngleToEulerAngleYZXi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def("AxisAndAngleToEulerAngleZXYi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def("AxisAndAngleToEulerAngleZYXi", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def("AxisAndAngleToEulerAngleXYXe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def("AxisAndAngleToEulerAngleXZXe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def("AxisAndAngleToEulerAngleYXYe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def("AxisAndAngleToEulerAngleYZYe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def("AxisAndAngleToEulerAngleZXZe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def("AxisAndAngleToEulerAngleZYZe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def("AxisAndAngleToEulerAngleXYZe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def("AxisAndAngleToEulerAngleXZYe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def("AxisAndAngleToEulerAngleYXZe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def("AxisAndAngleToEulerAngleYZXe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def("AxisAndAngleToEulerAngleZXYe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def("AxisAndAngleToEulerAngleZYXe", &Rotation::Details::AxisAndAngleToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>) ;

    // Quaternion to other array converters
    m.def("QuaternionToAxisAndAngle", &Rotation::Details::QuaternionToAxisAndAngle) ;
    m.def("QuaternionToRotationMatrix", &Rotation::Details::QuaternionToRotationMatrix) ;
    m.def("QuaternionToBasisVectors", &Rotation::Details::QuaternionToBasisVectors) ;
    m.def("QuaternionToMRP", &Rotation::Details::QuaternionToMRP) ;
    m.def("QuaternionToRotationVector", &Rotation::Details::QuaternionToRotationVector) ;
    m.def("QuaternionToEulerAngleXYXi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def("QuaternionToEulerAngleXZXi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def("QuaternionToEulerAngleYXYi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def("QuaternionToEulerAngleYZYi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def("QuaternionToEulerAngleZXZi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def("QuaternionToEulerAngleZYZi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def("QuaternionToEulerAngleXYZi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def("QuaternionToEulerAngleXZYi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def("QuaternionToEulerAngleYXZi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def("QuaternionToEulerAngleYZXi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def("QuaternionToEulerAngleZXYi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def("QuaternionToEulerAngleZYXi", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def("QuaternionToEulerAngleXYXe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def("QuaternionToEulerAngleXZXe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def("QuaternionToEulerAngleYXYe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def("QuaternionToEulerAngleYZYe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def("QuaternionToEulerAngleZXZe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def("QuaternionToEulerAngleZYZe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def("QuaternionToEulerAngleXYZe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def("QuaternionToEulerAngleXZYe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def("QuaternionToEulerAngleYXZe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def("QuaternionToEulerAngleYZXe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def("QuaternionToEulerAngleZXYe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def("QuaternionToEulerAngleZYXe", &Rotation::Details::QuaternionToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>) ;

    // MRP to other array converters
    m.def("MRPToAxisAndAngle", &Rotation::Details::MRPToAxisAndAngle) ;
    m.def("MRPToRotationMatrix", &Rotation::Details::MRPToRotationMatrix) ;
    m.def("MRPToBasisVectors", &Rotation::Details::MRPToBasisVectors) ;
    m.def("MRPToQuaternion", &Rotation::Details::MRPToQuaternion) ;
    m.def("MRPToRotationVector", &Rotation::Details::MRPToRotationVector) ;
    m.def("MRPToEulerAngleXYXi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def("MRPToEulerAngleXZXi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def("MRPToEulerAngleYXYi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def("MRPToEulerAngleYZYi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def("MRPToEulerAngleZXZi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def("MRPToEulerAngleZYZi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def("MRPToEulerAngleXYZi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def("MRPToEulerAngleXZYi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def("MRPToEulerAngleYXZi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def("MRPToEulerAngleYZXi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def("MRPToEulerAngleZXYi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def("MRPToEulerAngleZYXi", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def("MRPToEulerAngleXYXe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def("MRPToEulerAngleXZXe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def("MRPToEulerAngleYXYe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def("MRPToEulerAngleYZYe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def("MRPToEulerAngleZXZe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def("MRPToEulerAngleZYZe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def("MRPToEulerAngleXYZe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def("MRPToEulerAngleXZYe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def("MRPToEulerAngleYXZe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def("MRPToEulerAngleYZXe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def("MRPToEulerAngleZXYe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def("MRPToEulerAngleZYXe", &Rotation::Details::MRPToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>) ;

    // RotationVector to other array converters
    m.def("RotationVectorToAxisAndAngle", &Rotation::Details::RotationVectorToAxisAndAngle) ;
    m.def("RotationVectorToRotationMatrix", &Rotation::Details::RotationVectorToRotationMatrix) ;
    m.def("RotationVectorToBasisVectors", &Rotation::Details::RotationVectorToBasisVectors) ;
    m.def("RotationVectorToQuaternion", &Rotation::Details::RotationVectorToQuaternion) ;
    m.def("RotationVectorToMRP", &Rotation::Details::RotationVectorToMRP) ;
    m.def("RotationVectorToEulerAngleXYXi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYX_i>) ;
    m.def("RotationVectorToEulerAngleXZXi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZX_i>) ;
    m.def("RotationVectorToEulerAngleYXYi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXY_i>) ;
    m.def("RotationVectorToEulerAngleYZYi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZY_i>) ;
    m.def("RotationVectorToEulerAngleZXZi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_i>) ;
    m.def("RotationVectorToEulerAngleZYZi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_i>) ;
    m.def("RotationVectorToEulerAngleXYZi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYZ_i>) ;
    m.def("RotationVectorToEulerAngleXZYi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZY_i>) ;
    m.def("RotationVectorToEulerAngleYXZi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXZ_i>) ;
    m.def("RotationVectorToEulerAngleYZXi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZX_i>) ;
    m.def("RotationVectorToEulerAngleZXYi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXY_i>) ;
    m.def("RotationVectorToEulerAngleZYXi", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYX_i>) ;
    m.def("RotationVectorToEulerAngleXYXe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYX_e>) ;
    m.def("RotationVectorToEulerAngleXZXe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZX_e>) ;
    m.def("RotationVectorToEulerAngleYXYe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXY_e>) ;
    m.def("RotationVectorToEulerAngleYZYe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZY_e>) ;
    m.def("RotationVectorToEulerAngleZXZe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXZ_e>) ;
    m.def("RotationVectorToEulerAngleZYZe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYZ_e>) ;
    m.def("RotationVectorToEulerAngleXYZe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XYZ_e>) ;
    m.def("RotationVectorToEulerAngleXZYe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_XZY_e>) ;
    m.def("RotationVectorToEulerAngleYXZe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YXZ_e>) ;
    m.def("RotationVectorToEulerAngleYZXe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_YZX_e>) ;
    m.def("RotationVectorToEulerAngleZXYe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZXY_e>) ;
    m.def("RotationVectorToEulerAngleZYXe", &Rotation::Details::RotationVectorToEulerAngle<Rotation::EulerAnglesConvention_ZYX_e>) ;

//     EulerAngles to other array converters
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XYX_i>(m, "XYXi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XZX_i>(m, "XZXi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YXY_i>(m, "YXYi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YZY_i>(m, "YZYi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZXZ_i>(m, "ZXZi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZYZ_i>(m, "ZYZi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XYZ_i>(m, "XYZi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XZY_i>(m, "XZYi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YXZ_i>(m, "YXZi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YZX_i>(m, "YZXi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZXY_i>(m, "ZXYi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZYX_i>(m, "ZYXi") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XYX_e>(m, "XYXe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XZX_e>(m, "XZXe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YXY_e>(m, "YXYe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YZY_e>(m, "YZYe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZXZ_e>(m, "ZXZe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZYZ_e>(m, "ZYZe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XYZ_e>(m, "XYZe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_XZY_e>(m, "XZYe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YXZ_e>(m, "YXZe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_YZX_e>(m, "YZXe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZXY_e>(m, "ZXYe") ;
    AddEulerArrayConverter_<Rotation::EulerAnglesConvention_ZYX_e>(m, "ZYXe") ;
}

} // End of namespace Geometry
} // End of namespace PythonInterface
} // End of namespace BV

