#pragma once

#include <Eigen/Dense>
#include <pybind11/operators.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"

namespace BV {
namespace PythonInterface {
namespace Geometry {

using namespace BV::Geometry ;
namespace py = pybind11 ;


// Convenience wrappers to set the Point/Vector coordinates.

template <typename XYZ> double _GetX(const XYZ & xyz) { return xyz.x() ; }
template <typename XYZ> double _GetY(const XYZ & xyz) { return xyz.y() ; }
template <typename XYZ> double _GetZ(const XYZ & xyz) { return xyz.z() ; }
template <typename XYZ> void _SetX(XYZ & xyz, const double & value) { xyz.x() = value ; }
template <typename XYZ> void _SetY(XYZ & xyz, const double & value) { xyz.y() = value ; }
template <typename XYZ> void _SetZ(XYZ & xyz, const double & value) { xyz.z() = value ; }

void exportXYZModule(py::module & m)
{
    py::class_<Vector>(m, "Vector")
        .def(py::init<double, double, double>(),
             R"rst(
             Construction from coordinates.

             :param float x: x coordinate.
             :param float y: y coordinate.
             :param float z: z coordinate.

             )rst")
        .def(py::init<>(),
             R"rst(
             Default construction with null values (0., 0., 0.).
             )rst")
        .def(py::init<const Vector &>(),
             R"rst(
             Construction from another :py:class:`~Geometry.Vector`.
             
             :param v: the other :py:class:`~Geometry.Vector`.
             :paramtype v: :py:class:`~Geometry.Vector`
             )rst")
        .def(py::init<const Eigen::Vector3d &>(),
             R"rst(
             Construction from numpy array.
             
             :param array: the numpy array containing the 3 vector coordinates.
             :paramtype array: :py:class:`numpy.ndarray`
             )rst")
#ifdef __INTEL_COMPILER
        .def_property("x", [](const Vector & v){return v.x();}, [](Vector & v, const double & x){v.x() = x;},
                      R"rst(The x coordinate)rst")
        .def_property("y", [](const Vector & v){return v.y();}, [](Vector & v, const double & y){v.y() = y;},
                      R"rst(The y coordinate)rst")
        .def_property("z", [](const Vector & v){return v.z();}, [](Vector & v, const double & z){v.z() = z;},
                      R"rst(The z coordinate)rst")
#else
        .def_property("x", _GetX<Vector>, _SetX<Vector>,
                      R"rst(The x coordinate)rst")
        .def_property("y", _GetY<Vector>, _SetY<Vector>,
                      R"rst(The y coordinate)rst")
        .def_property("z", _GetZ<Vector>, _SetZ<Vector>,
                      R"rst(The z coordinate)rst")
#endif
        .def(py::self * py::self,
             R"rst(
             Compute the scalar product between two vectors.
             
             :param rhs: The other vector
             :paramtype rhs: :py:class:`~Geometry.Vector`
             :return: The value of the scalar product.
             :rtype: float
             )rst")
        .def(py::self * double(),
             R"rst(
             Scale the vector quantities.
             
             :param float rhs: the scale factor
             :return: the scaled vector
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def(double() * py::self,
             R"rst(
             Scale the vector quantities.
             
             :param float lhs: the scale factor
             :return: the scaled vector
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def(py::self + py::self,
             R"rst(
             Addition between two vectors.
             
             :param vector: the vector to add
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: the sum of the vectors
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def(py::self - py::self,
             R"rst(
             Difference between two vectors.
             
             :param vector: the vector to subtract
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: the subtraction of the vectors
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("__xor__", &Vector::operator^,
             R"rst(
             Cross product between two vectors.
             
             :param vector: the rhs vector
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: the cross product
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("norm", &Vector::norm,
             R"rst(
             Norm of the vector
             
             :return: the vector norm
             :rtype: float
             )rst")
        .def("normalise", &Vector::normalise,
             R"rst(
             Normalise the vector.
             )rst")
        .def("normalised", &Vector::normalised,
             R"rst(
             Get a normalised copy of the vector.
             This leaves the vector untouched.
             
             :return: a normalised copy of the vector
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("toPoint",
             [](const Vector & v)// -> Point
             {
                 return v.toPoint() ;
             },
             R"rst(
             Convert the vector into a Point.
             
             :return: a point 
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def("toVector",
             [](const Vector & v)// -> Vector
             {
                 return v.toVector() ;
             },
             R"rst(
             Get a copy of the vector.
             
             :return: a copy of the vector 
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def(-py::self,
             R"rst(
             Get the inverse of the vector.
             
             :return: the inverse of the vector 
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("get", &Vector::toArray,
             R"rst(
             Get the vector as a numpy array.
             
             :return: a numpy array containing the vector coordinates.
             :rtype: :py:class:`numpy.ndarray`
             )rst")
        .def("__repr__",
            [](const Vector & v)
            {
                return "("+std::to_string(v.x())
                     +", "+std::to_string(v.y())
                     +", "+std::to_string(v.z())+")" ;
            },
            R"rst(
            Prints the vector.
            
            :return: a string representing the vector coordinates.
            :rtype: str
            )rst")
        // FIXME pickle ??
    ;

    py::class_<Point>(m, "Point")
        .def(py::init<double, double, double>(),
             R"rst(
             Construction from coordinates.

             :param float x: x coordinate.
             :param float y: y coordinate.
             :param float z: z coordinate.

             )rst")
        .def(py::init<>(),
             R"rst(
             Default construction with null values (0., 0., 0.).
             )rst")
        .def(py::init<const Point &>(),
             R"rst(
             Construction from another :py:class:`~Geometry.Point`.
             
             :param v: the other :py:class:`~Geometry.Point`.
             :paramtype v: :py:class:`~Geometry.Point`
             )rst")
        .def(py::init<const Eigen::Vector3d &>(),
             R"rst(
             Construction from numpy array.
             
             :param array: the numpy array containing the 3 point coordinates.
             :paramtype array: :py:class:`numpy.ndarray`
             )rst")
        .def_property("x", [](const Point & v){return v.x();}, [](Point & v, const double & x){v.x() = x;},
                      R"rst(The x coordinate)rst")
        .def_property("y", [](const Point & v){return v.y();}, [](Point & v, const double & y){v.y() = y;},
                      R"rst(The y coordinate)rst")
        .def_property("z", [](const Point & v){return v.z();}, [](Point & v, const double & z){v.z() = z;},
                      R"rst(The z coordinate)rst")
//        .def_property("x", _GetX<Point>, _SetX<Point>)
//        .def_property("y", _GetY<Point>, _SetY<Point>)
//        .def_property("z", _GetZ<Point>, _SetZ<Point>)
        .def(py::self - Vector(),
             R"rst(
             Negative translation of the point according to the given
             :py:class:`~Geometry.Vector`.
             
             :param vector: the vector representing the translation.
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: a translated copy of the point.
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def(py::self + Vector(),
             R"rst(
             Positive translation of the point according to the given
             :py:class:`~Geometry.Vector`.
             
             :param vector: the vector representing the translation.
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: a translated copy of the point.
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def(py::self -= Vector(),
             R"rst(
             Negative translation of the point according to the given
             :py:class:`~Geometry.Vector`.
             
             :param vector: the vector representing the translation.
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: the translated point.
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def(py::self += Vector(),
             R"rst(
             Positive translation of the point according to the given
             :py:class:`~Geometry.Vector`.
             
             :param vector: the vector representing the translation.
             :paramtype vector: :py:class:`~Geometry.Vector`
             :return: the translated point.
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def(py::self - py::self,
             R"rst(
             Difference between two :py:class:`~Geometry.Point`
             
             :return: A :py:class:`~Geometry.Vector` representing the difference.
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def("toPoint",
             [](const Point & pt)// -> Point
             {
                 return pt.toPoint() ;
             },
             R"rst(
             Get a copy of the point.
             
             :return: a copy of the point 
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def("toVector",
             [](const Point & pt)// -> Vector
             {
                 return pt.toVector() ;
             },
             R"rst(
             Convert the point into a Vector.
             
             :return: a vector 
             :rtype: :py:class:`~Geometry.Vector`
             )rst")
        .def(-py::self,
             R"rst(
             Get the inverse of the point.
             
             :return: the inverse of the point 
             :rtype: :py:class:`~Geometry.Point`
             )rst")
        .def("get", &Point::toArray,
             R"rst(
             Get the point as a numpy array.
             
             :return: a numpy array containing the vector coordinates.
             :rtype: :py:class:`numpy.ndarray`
             )rst")
        .def("__repr__",
            [](const Point & pt)
            {
                return "("+std::to_string(pt.x())
                     +", "+std::to_string(pt.y())
                     +", "+std::to_string(pt.z())+")" ;
            })
        // FIXME pickle ??
    ;
}

} // End of namespace Geometry
} // End of namespace PythonInterface
} // End of namespace BV

