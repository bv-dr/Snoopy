#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/functional.h>

#include "Interface/pyWrapping/XYZ.hpp"
#include "Interface/pyWrapping/Translation.hpp"
#include "Interface/pyWrapping/Rotation.hpp"
#include "Interface/pyWrapping/Transform3D.hpp"
#include "Interface/pyWrapping/Manipulation.hpp"
#include "Interface/pyWrapping/ArrayConverters.hpp"

PYBIND11_MODULE(_Geometry, m)
{
	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    BV::PythonInterface::Geometry::exportXYZModule(m) ;
    BV::PythonInterface::Geometry::exportTranslationModule(m) ;
    BV::PythonInterface::Geometry::exportRotationModule(m) ;
    BV::PythonInterface::Geometry::exportTransform3DModule(m) ;
    BV::PythonInterface::Geometry::exportManipulationModule(m) ;
    BV::PythonInterface::Geometry::exportArrayConvertersModule(m) ;
}
