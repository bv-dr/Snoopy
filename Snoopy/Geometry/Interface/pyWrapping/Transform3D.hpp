#pragma once

#include "Geometry/Transform3D.hpp"

namespace BV {
namespace PythonInterface {
namespace Geometry {

using namespace BV::Geometry ;
namespace py = pybind11 ;

void exportTransform3DModule(py::module & m)
{

    py::class_<Transform3D>(m, "Transform3D")
        .def(py::init<const Translation::ABC &, const Rotation::ABC &>(),
             R"rst(
             Initialisation of the transformation with a translator and a
             rotator.

             :param translator: A :ref:`translation object<GeometryTranslation>`
             :param rotator: A :ref:`rotation object<GeometryRotation>`
             )rst")
        .def(py::init<>(),
             R"rst(
             Initialisation of the transformation with null translator and
             rotator. This is equivalent to no transformation.
             )rst")
        .def(py::init<const Transform3D &>(),
             R"rst(
             Initialisation of the transformation with copy of another
             :py:class:`Geometry.Transform3D` object.
             )rst")
        .def("__imul__",
             [](Transform3D & t, const Transform3D & other)
             {
                 return t *= other ;
             },
             R"rst(
             Addition of the other transformation.
             This first applies the translation by adding the new one to
             current one. Then it applies the additional rotation by
             adding other rotation at right. See for example
             :py:meth:`Geometry.Quaternion.addOtherRotationAtRight` for more
             information on the rotation order.

             :param other: the other :py:class:`Geometry.Transform3D` from
                 which the additional transformation is based.
             :return: the modified transformation.
             )rst")
        .def("__mul__",
             [](Transform3D & t, const Transform3D & other)
             {
                 return t * other ;
             },
             R"rst(
             Addition of the other transformation to a copy.
             This applies the translation by adding the new one to
             current one. Then it applies the additional rotation by
             adding other rotation at right. See for example
             :py:meth:`Geometry.Quaternion.addOtherRotationAtRight` for more
             information on the rotation order.

             :param other: the other :py:class:`Geometry.Transform3D` from
                 which the additional transformation is based.
             :return: a modified copy of the transformation.
             )rst")
        .def("__imul__",
             [](Transform3D & t, const Translation::ABC & other)
             {
                 return t *= other ;
             },
             R"rst(
             Addition of the other transformation.
             This applies the translation by adding the new one to current one.

             :param other: the other :ref:`translation object<GeometryTranslation>`
                 on which the additional translation is based.
             :return: the modified transformation.
             )rst")
        .def("__mul__",
             [](Transform3D & t, const Translation::ABC & other)
             {
                 return t * other ;
             },
             R"rst(
             Addition of the other transformation on a copy.
             This applies the translation by adding the new one to current one.

             :param other: the :ref:`translation object<GeometryTranslation>`
                 on which the additional translation is based.
             :return: a modified copy of the transformation.
             )rst")
        .def("__imul__",
             [](Transform3D & t, const Rotation::ABC & other)
             {
                 return t *= other ;
             },
             R"rst(
             Addition of the other transformation.
             This applies the additional rotation by
             adding other rotation at right. See for example
             :py:meth:`Geometry.Quaternion.addOtherRotationAtRight` for more
             information on the rotation order.

             :param other: the :ref:`rotation object<GeometryRotation>`
                 which the additional transformation is based.
             :return: the modified transformation.
             )rst")
        .def("__mul__",
             [](Transform3D & t, const Rotation::ABC & other)
             {
                 return t * other ;
             },
             R"rst(
             Addition of the other transformation on a copy.
             This applies the additional rotation by
             adding other rotation at right. See for example
             :py:meth:`Geometry.Quaternion.addOtherRotationAtRight` for more
             information on the rotation order.

             :param other: the :ref:`rotation object<GeometryRotation>`
                 which the additional transformation is based.
             :return: the modified copy of the transformation.
             )rst")
        .def("__mul__",
             [](Transform3D & t, const Point & other)
             {
                 return t * other ;
             },
             R"rst(
             Application of the transformation to the provided point.
             This multiplies the transformation matrix :math:`T` to the
             point augmented coordinates :math:`(pt.x, pt.y, pt.z, 1.)`.
             The :math:`1.` coordinate means that the transformation translation
             will be taken into account.

             :param pt: the point on which to apply the transformation.
             :paramtype pt: :py:class:`Geometry.Point`

             :return: the point on which the transformation was applied
             :rtype: :py:class:`Geometry.Point`
             )rst")
        .def("__mul__",
             [](Transform3D & t, const Vector & other)
             {
                 return t * other ;
             },
             R"rst(
             Application of the transformation to the provided vector.
             This multiplies the transformation matrix :math:`T` to the
             vector augmented coordinates :math:`(v.x, v.y, v.z, 0.)`.
             The :math:`0.` coordinate means that the transformation translation
             will not be taken into account.

             :param v: the vector on which to apply the transformation.
             :paramtype v: :py:class:`Geometry.Vector`

             :return: the vector on which the transformation was applied
             :rtype: :py:class:`Geometry.Vector`
             )rst")
        .def("inverse", &Transform3D::inverse,
             R"rst(See :py:meth:`Geometry.Quaternion.inverse` for more information)rst")
        .def("getMatrix", &Transform3D::getMatrix,
             R"rst(See :py:meth:`Geometry.Quaternion.getMatrix` for more information)rst")
        .def("getTranslator", &Transform3D::getTranslator<Translation::Cartesian>,
             R"rst(
             Return the cartesian translator corresponding to the translation
             transformation.

             :return: a :py:class:`Geometry.Cartesian` object.
             )rst")
        .def("getRotationMatrix", &Transform3D::getRotator<Rotation::RotationMatrix>,
             R"rst(See :py:meth:`Geometry.Quaternion.toRotationMatrix` for more information)rst")
        .def("getEulerAngles_XYX_i", &Transform3D::getRotator<Rotation::EulerAngles_XYX_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XYX_i` for more information)rst")
        .def("getEulerAngles_XYZ_i", &Transform3D::getRotator<Rotation::EulerAngles_XYZ_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XYZ_i` for more information)rst")
        .def("getEulerAngles_XZX_i", &Transform3D::getRotator<Rotation::EulerAngles_XZX_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XZX_i` for more information)rst")
        .def("getEulerAngles_XZY_i", &Transform3D::getRotator<Rotation::EulerAngles_XZY_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XZY_i` for more information)rst")
        .def("getEulerAngles_YXY_i", &Transform3D::getRotator<Rotation::EulerAngles_YXY_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YXY_i` for more information)rst")
        .def("getEulerAngles_YXZ_i", &Transform3D::getRotator<Rotation::EulerAngles_YXZ_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YXZ_i` for more information)rst")
        .def("getEulerAngles_YZX_i", &Transform3D::getRotator<Rotation::EulerAngles_YZX_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YZX_i` for more information)rst")
        .def("getEulerAngles_YZY_i", &Transform3D::getRotator<Rotation::EulerAngles_YZY_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YZY_i` for more information)rst")
        .def("getEulerAngles_ZXY_i", &Transform3D::getRotator<Rotation::EulerAngles_ZXY_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZXY_i` for more information)rst")
        .def("getEulerAngles_ZXZ_i", &Transform3D::getRotator<Rotation::EulerAngles_ZXZ_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZXZ_i` for more information)rst")
        .def("getEulerAngles_ZYX_i", &Transform3D::getRotator<Rotation::EulerAngles_ZYX_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZYX_i` for more information)rst")
        .def("getEulerAngles_ZYZ_i", &Transform3D::getRotator<Rotation::EulerAngles_ZYZ_i>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZYZ_i` for more information)rst")
        .def("getEulerAngles_XYX_e", &Transform3D::getRotator<Rotation::EulerAngles_XYX_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XYX_e` for more information)rst")
        .def("getEulerAngles_XYZ_e", &Transform3D::getRotator<Rotation::EulerAngles_XYZ_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XYZ_e` for more information)rst")
        .def("getEulerAngles_XZX_e", &Transform3D::getRotator<Rotation::EulerAngles_XZX_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XZX_e` for more information)rst")
        .def("getEulerAngles_XZY_e", &Transform3D::getRotator<Rotation::EulerAngles_XZY_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XZY_e` for more information)rst")
        .def("getEulerAngles_YXY_e", &Transform3D::getRotator<Rotation::EulerAngles_YXY_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_XZY_e` for more information)rst")
        .def("getEulerAngles_YXZ_e", &Transform3D::getRotator<Rotation::EulerAngles_YXZ_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YXZ_e` for more information)rst")
        .def("getEulerAngles_YZX_e", &Transform3D::getRotator<Rotation::EulerAngles_YZX_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YZX_e` for more information)rst")
        .def("getEulerAngles_YZY_e", &Transform3D::getRotator<Rotation::EulerAngles_YZY_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_YZY_e` for more information)rst")
        .def("getEulerAngles_ZXY_e", &Transform3D::getRotator<Rotation::EulerAngles_ZXY_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZXY_e` for more information)rst")
        .def("getEulerAngles_ZXZ_e", &Transform3D::getRotator<Rotation::EulerAngles_ZXZ_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZXZ_e` for more information)rst")
        .def("getEulerAngles_ZYX_e", &Transform3D::getRotator<Rotation::EulerAngles_ZYX_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZYX_e` for more information)rst")
        .def("getEulerAngles_ZYZ_e", &Transform3D::getRotator<Rotation::EulerAngles_ZYZ_e>,
             R"rst(See :py:meth:`Geometry.Quaternion.toEulerAngles_ZYZ_e` for more information)rst")
        .def("getQuaternion", &Transform3D::getRotator<Rotation::Quaternion>,
             R"rst(See :py:meth:`Geometry.Quaternion.toQuaternion` for more information)rst")
        .def("getBasisVectors", &Transform3D::getRotator<Rotation::BasisVectors>,
             R"rst(See :py:meth:`Geometry.Quaternion.toBasisVectors` for more information)rst")
        .def("getAxisAndAngle", &Transform3D::getRotator<Rotation::AxisAndAngle>,
             R"rst(See :py:meth:`Geometry.Quaternion.toAxisAndAngle` for more information)rst")
        .def("getMRP", &Transform3D::getRotator<Rotation::MRP>,
             R"rst(See :py:meth:`Geometry.Quaternion.toMRP` for more information)rst")
    ;

}

} // End of namespace Geometry
} // End of namespace PythonInterface
} // End of namespace BV

