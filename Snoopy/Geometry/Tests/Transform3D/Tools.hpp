#pragma once
#ifndef __BV_Geometry_Tests_Transform3D_Tools_hpp__
#define __BV_Geometry_Tests_Transform3D_Tools_hpp__

#include "Geometry/GeometryTypedefs.hpp"

namespace BV {
namespace Geometry {
namespace Tests {
namespace Details {

void Transform3DInitialisation(void) ;
void Transform3DComposition(void) ;
void Transform3DCompositionTranslatorRotator(void) ;
void Transform3DChangeOfBasis(void) ;
void Transform3DGetTranslatorRotator(void) ;

// Specific for Transform3d which returns a Eigen::Matrix<double, 4, 4, Eigen::DontAlign>
// Different than Eigen::Matrix4d for Python wrapping
template <typename T1, typename T2>
void CheckEigenMatrix(const T1 & m1, const T2 & m2)
{
    using BV::Details::epsilon ;
    unsigned nRows(m1.rows()) ;
    unsigned nCols(m1.cols()) ;
    for (unsigned i=0; i<nRows; ++i)
    {
        for (unsigned j=0; j<nCols; ++j)
        {
            BOOST_CHECK_SMALL(m1(i, j)-m2(i, j), epsilon) ;
        }
    }
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Transform3D_Tools_hpp__
