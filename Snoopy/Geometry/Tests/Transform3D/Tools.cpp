#include <boost/test/unit_test.hpp>
#include <iostream>
#include <cmath>

#include "Tests/Transform3D/Tools.hpp"

#include "Geometry/Transform3D.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/Translation.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Point.hpp"

#include "Tests/Objects/Tools.hpp"
#include "Tests/Rotation/RotationMatrix/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {
namespace Details {

Eigen::Matrix4d toMatrix(const Translation::ABC & trans,
                         const Rotation::ABC & rot)
{
    Eigen::Matrix4d tmp(Eigen::Matrix4d::Zero()) ;
    tmp.block<3, 3>(0, 0) = rot.toRotationMatrix().getMatrix() ;
    Point p(trans.toPoint()) ;
    tmp(0, 3) = p.x() ;
    tmp(1, 3) = p.y() ;
    tmp(2, 3) = p.z() ;
    tmp(3, 3) = 1. ;
    return tmp ;
}

void Transform3DInitialisation(void)
{
    {
        Transform3D transform ;
        Eigen::Matrix4d tmp(Eigen::Matrix4d::Identity()) ;
        BV::Tools::Tests::CheckEigenMatrix(transform.getMatrix(), tmp) ;
    }
    {
        Translation::Cartesian trans(1., 2., 3.) ;
        Rotation::AxisAndAngle rot(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
        Transform3D transform(trans, rot) ;
        BV::Tools::Tests::CheckEigenMatrix(transform.getMatrix(),
                                  Details::toMatrix(trans, rot)) ;
    }
}

void Transform3DComposition(void)
{
    Transform3D transform ;
    Translation::Cartesian trans(1., 2., 3.) ;
    Rotation::AxisAndAngle rot(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    Transform3D transform2(trans, rot) ;
    transform *= transform2 ;
    BV::Tools::Tests::CheckEigenMatrix(transform.getMatrix(),
                              Details::toMatrix(trans, rot)) ;
    Transform3D transform3(transform * transform2) ;
    Rotation::AxisAndAngle rot2(rot) ;
    rot2.addOtherRotationAtLeft(rot) ;
    Translation::Cartesian trans2(-1., 3., 6.) ;
    BV::Tools::Tests::CheckEigenMatrix(transform3.getMatrix(),
                              Details::toMatrix(trans2, rot2)) ;
    Transform3D transform4(transform3 * transform2.inverse()) ;
    BV::Tools::Tests::CheckEigenMatrix(transform4.getMatrix(),
                              Details::toMatrix(trans, rot)) ;
}

void Transform3DCompositionTranslatorRotator(void)
{
    Transform3D transform ;
    Translation::Cartesian trans(1., 2., 3.) ;
    Rotation::AxisAndAngle rot ;
    transform *= trans ;
    BV::Tools::Tests::CheckEigenMatrix(transform.getMatrix(),
                              Details::toMatrix(trans, rot)) ;
    Rotation::AxisAndAngle rot2(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    transform *= rot2 ;
    BV::Tools::Tests::CheckEigenMatrix(transform.getMatrix(),
                              Details::toMatrix(trans, rot2)) ;
    Transform3D transform2(transform * trans) ;
    Translation::Cartesian trans2(-1., 3., 6.) ;
    BV::Tools::Tests::CheckEigenMatrix(transform2.getMatrix(),
                              Details::toMatrix(trans2, rot2)) ;
}

void Transform3DChangeOfBasis(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    Translation::Cartesian trans(1., 2., 3.) ;
    Rotation::AxisAndAngle rot(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    Transform3D transform(trans, rot) ;
    // Check points
    Point pt(1., 1., 1.) ;
    Point ptGlob(transform * pt) ;
    Details::CheckXYZ<Point>(ptGlob, Geometry::Point(0., 3., 4.)) ;
    // Check vectors
    Vector vect(1., 1., 1.) ;
    Vector vectGlob(transform * vect) ;
    Details::CheckXYZ<Vector>(vectGlob, Geometry::Vector(-1., 1., 1.)) ;
}

void Transform3DGetTranslatorRotator(void)
{
    Translation::Cartesian trans(1., 2., 3.) ;
    Rotation::AxisAndAngle rot(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    Transform3D transform(trans, rot) ;
    // Cartesian
    Translation::Cartesian T(transform.getTranslator<Translation::Cartesian>()) ;
    BV::Tools::Tests::CheckEigenVectors(T.toPoint().toArray(), trans.toPoint().toArray()) ;
    // RotationMatrix
    Rotation::RotationMatrix R(transform.getRotator<Rotation::RotationMatrix>()) ;
    Details::CheckMatrix(R, rot.toRotationMatrix()) ;
    // AxisAndAngle
    Rotation::AxisAndAngle aa(transform.getRotator<Rotation::AxisAndAngle>()) ;
    Details::CheckMatrix(aa.toRotationMatrix(), rot.toRotationMatrix()) ;
    // Quaternion
    Rotation::Quaternion q(transform.getRotator<Rotation::Quaternion>()) ;
    Details::CheckMatrix(q.toRotationMatrix(), rot.toRotationMatrix()) ;
    // MRP
    Rotation::MRP mrp(transform.getRotator<Rotation::MRP>()) ;
    Details::CheckMatrix(mrp.toRotationMatrix(), rot.toRotationMatrix()) ;
    // FIXME check others ?
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
