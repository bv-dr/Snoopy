#include "Tests/Transform3D/Transform3D.hpp"
#include "Tests/Transform3D/Tools.hpp"


namespace BV {
namespace Geometry {
namespace Tests {

void Transform3D_Constructors(void)
{
    Details::Transform3DInitialisation() ;
}

void Transform3D_Methods(void)
{
    Details::Transform3DComposition() ;
    Details::Transform3DCompositionTranslatorRotator() ;
    Details::Transform3DChangeOfBasis() ;
    Details::Transform3DGetTranslatorRotator() ;
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
