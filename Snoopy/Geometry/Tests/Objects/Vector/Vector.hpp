#pragma once
#ifndef __BV_Geometry_Tests_Objects_Vector_Vector_hpp__
#define __BV_Geometry_Tests_Objects_Vector_Vector_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void Vector_Constructors( void ) ;
void Vector_Methods( void ) ;

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Objects_Vector_Vector_hpp__
