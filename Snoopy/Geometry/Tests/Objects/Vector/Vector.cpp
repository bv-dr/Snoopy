#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Vector.hpp"
#include "Tests/Objects/Vector/Vector.hpp"

#include "Tests/Objects/XYZ/XYZ.hpp"
#include "Tests/Objects/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

#ifdef BV_HAVE_LIBCONFIGPP
// FIXME, this is unclean, give the cfg to the function...
const BV::Utils::ConfigFile cfg("Tests/Objects/Vector/Vector.cfg") ;
#endif

void Vector_Constructors(void)
{
    using Geometry::Vector ;
    using BV::Details::epsilon ;

    #ifdef BV_HAVE_LIBCONFIGPP
    // Testing the constructor from file
    {
        const Vector vec(cfg, "OneVector") ;
        const Vector expected(1.0, 2.0, 3.0) ;

        const double dist = Distance(vec, expected) ;
        BOOST_CHECK_SMALL(dist, epsilon) ;
    }
    #endif

    // XYZ Constructors
    XYZ_Constructors<Vector>() ;

    // Construct from void
    Vector vec0 ;
    BOOST_CHECK_SMALL(vec0.x(), epsilon) ;
    BOOST_CHECK_SMALL(vec0.y(), epsilon) ;
    BOOST_CHECK_SMALL(vec0.z(), epsilon) ;
    Details::CheckXYZ(vec0, Vector(0., 0., 0.)) ;

    // Construct from coordinates
    Vector vec1(1.0, 2.0, 4.0) ;

    // Construct from vector
    Vector vec2(vec1) ;
    BOOST_CHECK_SMALL(Distance(vec1, vec2), epsilon) ;
    Details::CheckXYZ<Vector>(vec1, vec2) ;

    // Construct from Eigen Vector 3d
    Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
    array1 << 1.0, 2.0, 4.0 ;
    Vector vec3(array1) ;
    BOOST_CHECK_SMALL(Distance(vec1, vec3), epsilon) ;
    Details::CheckXYZ<Vector>(vec1, vec3) ;

    // Construct from Eigen Vector Xd
    Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
    array2(0) = 3.0 ;
    array2(1) = -5.0 ;
    array2(2) = 13.0 ;
    Vector vec4(array2) ;
    const double dX = 3.0 - 1.0 ;
    const double dY = -5.0 - 2.0 ;
    const double dZ = 13.0 - 4.0 ;
    const double dist( sqrt( dX*dX + dY*dY + dZ*dZ ) ) ;
    BOOST_CHECK_SMALL(Distance(vec4, vec1) - dist, epsilon) ;
}

void Vector_Methods(void)
{
    using Geometry::Vector ;
    using BV::Details::epsilon ;

    // XYZ Methods
    XYZ_Methods<Vector>() ;

    // Testing the norm member function
    {
        const Vector vec(1.0, 2.0, 3.0) ;
        const double norm = vec.norm() ;
        const double expected(sqrt(14.0)) ;
        const double squaredNorm = vec.squaredNorm() ;
        const double expectedSQNorm(14.0) ;
        BOOST_CHECK_SMALL(norm-expected, epsilon) ;
        BOOST_CHECK_SMALL(squaredNorm-expectedSQNorm, epsilon) ;
    }

    // Testing the normalise member function
    {
        Vector vec(1.0, 1.0, 1.0) ;
        Vector unit( vec.normalised() ) ;
        vec.normalise() ;
        BOOST_CHECK_SMALL( unit.norm() - vec.norm(), epsilon ) ;
        // Now, I may modify vec without modifying unit
        vec.x() = 2. ;
        vec.y() = 2. ;
        vec.z() = 2. ;
        BOOST_CHECK_SMALL( unit.norm() - 1., epsilon ) ;
        const double expected( 1.0 ) ;
        vec.normalise() ;
        BOOST_CHECK_SMALL( vec.norm() - expected, epsilon ) ;
    }

    // Testing the normalised member function
    {
        Vector vec(1.0, 1.0, 1.0) ;
        Vector unit( vec.normalised() ) ;
        BOOST_CHECK_SMALL( unit.norm() - 1., epsilon ) ;
        // Now, I may modify vec without modifying unit
        vec.x() = 2. ;
        vec.y() = 2. ;
        vec.z() = 2. ;
        BOOST_CHECK_SMALL( unit.norm() - 1., epsilon ) ;
        const double expected( sqrt( 12.0 ) ) ;
        BOOST_CHECK_SMALL( vec.norm() - expected, epsilon ) ;
    }

    // Testing operator -=
    {
        Vector vec1(1.0, 1.0, 1.0) ;
        Vector vec2(2.0, 2.0, 2.0) ;
        vec2 -= vec1 ;
        Details::CheckXYZ<Vector>(vec1,vec2) ;
    }

    // Testing operator -
    {
        Vector vec1(1.0, 1.0, 1.0) ;
        Vector vec2(2.0, 2.0, 2.0) ;
        Vector vec3 = vec2 - vec1 ;
        Details::CheckXYZ<Vector>(vec1,vec3) ;
    }

    // Testing operator +=
    {
        Vector vec1(1.0, 1.0, 1.0) ;
        Vector vec2(2.0, 2.0, 2.0) ;
        vec1 += vec1 ;
        Details::CheckXYZ<Vector>(vec1,vec2) ;
    }

    // Testing operator +
    {
        Vector vec1(1.0, 1.0, 1.0) ;
        Vector vec2(2.0, 2.0, 2.0) ;
        Vector vec3 = vec1 + vec1 ;
        Details::CheckXYZ<Vector>(vec2,vec3) ;
    }

    // Testing the cross product
    {
        const Vector vec1(1.0, 0.0, 0.0) ;
        const Vector vec2(0.0, 1.0, 0.0) ;
        const Vector vec3( vec1^vec2 ) ;
        const Vector expected(0.0, 0.0, 1.0) ;
        const double dist = Distance(expected, vec3) ;
        BOOST_CHECK_SMALL(dist, epsilon) ;
    }

    // Testing the scalar product
    {
        const Vector vec1(1.0, 2.0, 3.0) ;
        const Vector vec2(-1.0, -1.0, 5.0) ;
        const double res1 = vec1 * vec2 ;
        const double res2 = vec2 * vec1 ;
        const double expected(12.0) ;
        BOOST_CHECK_CLOSE(expected, res1, epsilon) ;
        BOOST_CHECK_CLOSE(expected, res2, epsilon) ;
    }

    // Check operator *=
    {
        Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
        array2(0) = 3.0 ;
        array2(1) = -5.0 ;
        array2(2) = 13.0 ;
        Vector xyz3(array2) ;
        xyz3 *= 2.0 ;
        Vector xyz4(6.0, -10.0, 26.0) ;
        Details::CheckXYZ(xyz3, xyz4) ;
        BOOST_CHECK_SMALL(Distance(xyz3, xyz4), epsilon) ;
    }

    // Check operator *
    {
        Vector xyz1(1.5, 2.0, 4.5) ;
        Vector xyz5 = xyz1 * 3.5 ;
        Vector xyz6(5.25, 7.0, 15.75) ;
        Details::CheckXYZ(xyz5, xyz6) ;
        BOOST_CHECK_SMALL(Distance(xyz5, xyz6), epsilon) ;
    }

    // Checking other stuff
    // Substracting several Vectors
    {
        Vector pt1(1.0, 2.0, 4.0) ;
        const Vector subtracter(0.1, 0.2, 0.3) ;
        const Vector res1(0.9, 1.8, 3.7) ;
        const Vector res2(0.8, 1.6, 3.4) ;
        const Vector res3(0.7, 1.4, 3.1) ;
        const Vector res4( 3.* subtracter - pt1 ) ;

        pt1 -= subtracter ;
        BOOST_CHECK_SMALL(Distance(pt1, res1), epsilon) ;
        pt1 -= subtracter ;
        BOOST_CHECK_SMALL(Distance(pt1, res2), epsilon) ;
        const Vector pt2( pt1 - subtracter ) ;
        BOOST_CHECK_SMALL(Distance(pt2, res3), epsilon) ;
        const Vector pt3( subtracter - pt1 ) ;
        BOOST_CHECK_SMALL(Distance(pt3, res4), epsilon) ;
    }

    {
        const Vector vec1(3.0, -3.0, 1.0) ;
        const Vector vec2(4.0, 9.0, 2.0) ;
        const Vector vec3( vec1^vec2 ) ;
        const Vector expected(-15.0, -2.0, 39.0) ;
        const double dist = Distance(expected, vec3) ;
        BOOST_CHECK_SMALL(dist, epsilon) ;
    }

    // Testing operator * with double
    {
        Vector vec1(1.0, 1.0, 1.0) ;
        Vector vec2 = 2. * vec1 ;
        Vector vec3(2.0, 2.0, 2.0) ;
        Details::CheckXYZ<Vector>(vec2,vec3) ;
        Vector vec4 = vec1 * 2. ;
        Details::CheckXYZ<Vector>(vec4,vec3) ;
    }
}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
