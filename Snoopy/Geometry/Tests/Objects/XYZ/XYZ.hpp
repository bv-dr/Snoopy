#pragma once
#ifndef __BV_Geometry_Tests_Objects_Abstract_XYZ_hpp__
#define __BV_Geometry_Tests_Objects_Abstract_XYZ_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"
#include "Tests/Objects/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

#ifdef BV_HAVE_LIBCONFIGPP
// FIXME, this is unclean, give the cfg to the function...
const BV::Utils::ConfigFile cfg("Tests/Objects/Point/Point.cfg") ;
#endif

template <typename T>
void XYZ_Constructors( void )
{
    using BV::Details::epsilon ;

    #ifdef BV_HAVE_LIBCONFIGPP
    // Testing the constructor from file
    {
        const T xyz(cfg, "OneXYZ") ;
        const T expected(1.0, 2.0, 3.0) ;

        const double dist = Distance(xyz, expected) ;
        BOOST_CHECK_SMALL(dist, epsilon) ;
    }
    #endif

    // Construct from void
    T xyz00 ;
    BOOST_CHECK_SMALL(xyz00.x(), epsilon) ;
    BOOST_CHECK_SMALL(xyz00.y(), epsilon) ;
    BOOST_CHECK_SMALL(xyz00.z(), epsilon) ;

    // Construct from coordinates
    T xyz1(1.0, 2.0, 4.0) ;

    // Construct from Eigen Vector 3d
    Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
    array1 << 1.0, 2.0, 4.0 ;
    T xyz2(array1) ;
    BV::Tools::Tests::CheckEigenVectors(xyz2.toArray(), array1) ;

    // Construct from Eigen Vector Xd
    Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
    array2(0) = 3.0 ;
    array2(1) = -5.0 ;
    array2(2) = 13.0 ;
    T xyz3(array2) ;
    BV::Tools::Tests::CheckEigenVectors(xyz3.toArray(), array2) ;
}

template <typename T>
void XYZ_Methods( void )
{
    using BV::Details::epsilon ;
    // Construct from coordinates
    {
        T xyz1(1.5, 2.0, 4.5) ;
    }

    // Construct from Eigen Vector 3d
    {
        T xyz1(1.5, 2.0, 4.5) ;
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        T xyz2(array1) ;
        BOOST_CHECK_SMALL(Distance(xyz1, xyz2), epsilon) ;
        Details::CheckXYZ<T>(xyz1, xyz2) ;

        // Check getters x(), y(), z()
        BOOST_CHECK_SMALL(xyz1.x() - 1.5, epsilon) ;
        BOOST_CHECK_SMALL(xyz1.y() - 2.0, epsilon) ;
        BOOST_CHECK_SMALL(xyz1.z() - 4.5, epsilon) ;

        // Check setters x(), y(), z()
        xyz1.x() = 3.5 ;
        xyz1.y() = 3.0 ;
        xyz1.z() = 5.5 ;

        BOOST_CHECK_SMALL(xyz1.x() - 3.5, epsilon) ;
        BOOST_CHECK_SMALL(xyz1.y() - 3.0, epsilon) ;
        BOOST_CHECK_SMALL(xyz1.z() - 5.5, epsilon) ;

        // Check method toArray()
        Eigen::Vector3d xyz1Array(xyz1.toArray()) ;
        BOOST_CHECK_SMALL(xyz1Array(0) - 3.5, epsilon) ;
        BOOST_CHECK_SMALL(xyz1Array(1) - 3.0, epsilon) ;
        BOOST_CHECK_SMALL(xyz1Array(2) - 5.5, epsilon) ;

        // Check method fromArray()
        Eigen::Vector3d xyz1FromArray(-1., -6.1, 2.1 ) ;
        xyz1.fromArray(xyz1FromArray) ;
        BOOST_CHECK_SMALL(xyz1FromArray(0) - xyz1.x(), epsilon) ;
        BOOST_CHECK_SMALL(xyz1FromArray(1) - xyz1.y(), epsilon) ;
        BOOST_CHECK_SMALL(xyz1FromArray(2) - xyz1.z(), epsilon) ;
    }

    // Construct from Eigen Vector Xd
    {
        T xyz1(1.5, 2.0, 4.5) ;
        Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
        array2(0) = 3.0 ;
        array2(1) = -5.0 ;
        array2(2) = 13.0 ;
        T xyz3(array2) ;
        const double dX = 3.0 - 1.5 ;
        const double dY = -5.0 - 2.0 ;
        const double dZ = 13.0 - 4.5 ;
        const double dist( sqrt( dX*dX + dY*dY + dZ*dZ ) ) ;
        BOOST_CHECK_SMALL(Distance(xyz3, xyz1) - dist, epsilon) ;
    }

    // Check operator ==
    {
        T xyz1(1.5, 2.0, 4.5) ;
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        T xyz2(array1) ;
        BOOST_CHECK(xyz1 == xyz2) ;
    }

    // Check operator !=
    {
        T xyz1(1.5, 2.0, 4.5) ;
        Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
        array2(0) = 3.0 ;
        array2(1) = -5.0 ;
        array2(2) = 13.0 ;
        T xyz3(array2) ;
        BOOST_CHECK(xyz1 != xyz3) ;
    }

    // Check operator =
    {
        T xyz ;
        Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
        array2(0) = 3.0 ;
        array2(1) = -5.0 ;
        array2(2) = 13.0 ;
        T xyz3(array2) ;
        xyz = xyz3 ;
        Details::CheckXYZ<T>(xyz3, xyz) ;
        BOOST_CHECK_SMALL(Distance(xyz3, xyz), epsilon) ;
        BOOST_CHECK(&xyz != &xyz3) ;
    }

    // Check operator -
    {
        T xyz1(1.5, 2.0, 4.5) ;
        T xyz5 ;
        xyz5 = - xyz1 ;
        BOOST_CHECK_SMALL(xyz5.x() + 1.5, epsilon) ;
        BOOST_CHECK_SMALL(xyz5.y() + 2.0, epsilon) ;
        BOOST_CHECK_SMALL(xyz5.z() + 4.5, epsilon) ;
    }


    // Check accessing members
    {
        T xyz1(1.5, 2.0, 4.5) ;
        const double cx = xyz1.x() ;
        const double cy = xyz1.y() ;
        const double cz = xyz1.z() ;

        double x = xyz1.x() ;
        double y = xyz1.y() ;
        double z = xyz1.z() ;

        BOOST_CHECK_SMALL(cx - x, epsilon) ;
        BOOST_CHECK_SMALL(cy - y, epsilon) ;
        BOOST_CHECK_SMALL(cz - z, epsilon) ;
    }

    // Check assigning members
    {
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        T xyz2(array1) ;
        xyz2.x() = 8.3 ;
        xyz2.y() = 9.4 ;
        xyz2.z() = 8.8 ;
        T xyz11(8.3, 9.4, 8.8) ;
        Details::CheckXYZ<T>(xyz2, xyz11) ;
        BOOST_CHECK_SMALL(Distance(xyz2, xyz11), epsilon) ;
    }

    // Check method toArray
    {
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        T xyz2(array1) ;
        Eigen::Vector3d array3(xyz2.toArray()) ;
        BV::Tools::Tests::CheckEigenVectors(array1, array3) ;
    }

    // Check method fromArray
    {
        T xyz1(1.5, 2.0, 4.5) ;
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        T xyz2(0., 0., 0.) ;
        xyz2.fromArray(array1) ;
        Details::CheckXYZ<T>(xyz1, xyz2) ;
        BOOST_CHECK_SMALL(Distance(xyz1, xyz2), epsilon) ;
    }
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Objects_Abstract_XYZ_hpp__
