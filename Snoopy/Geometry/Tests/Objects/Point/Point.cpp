#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"
#include "Tests/Objects/Point/Point.hpp"

#include "Tests/Objects/XYZ/XYZ.hpp"
#include "Tests/Objects/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

#ifdef BV_HAVE_LIBCONFIGPP
// FIXME, this is unclean, give the cfg to the function...
const BV::Utils::ConfigFile cfg("Tests/Objects/Point/Point.cfg") ;
#endif

void Point_Constructors(void)
{
    using Geometry::Point ;
    using BV::Details::epsilon ;

    #ifdef BV_HAVE_LIBCONFIGPP
    // Testing the constructor from file
    {
        const Point pt(cfg, "OnePoint") ;
        const Point expected(1.0, 2.0, 3.0) ;

        const double dist = Distance(pt, expected) ;
        BOOST_CHECK_SMALL(dist, epsilon) ;
    }
    #endif

    // XYZ Constructors
    XYZ_Constructors<Point>() ;

    // Construct from void
    Point pt0 ;

    // Construct from coordinates
    Point pt1(1.0, 2.0, 4.0) ;

    // Construct from point
    Point pt2(pt1) ;
    BOOST_CHECK_SMALL(Distance(pt1, pt2), epsilon) ;
    Details::CheckXYZ<Point>(pt1, pt2) ;

    // Construct from Eigen Vector 3d
    Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
    array1 << 1.0, 2.0, 4.0 ;
    Point pt3(array1) ;
    BOOST_CHECK_SMALL(Distance(pt1, pt3), epsilon) ;
    Details::CheckXYZ<Point>(pt1, pt3) ;

    // Construct from Eigen Vector Xd
    Eigen::VectorXd array2(Eigen::VectorXd::Zero(3)) ;
    array2(0) = 3.0 ;
    array2(1) = -5.0 ;
    array2(2) = 13.0 ;
    Point pt4(array2) ;
    const double dX = 3.0 - 1.0 ;
    const double dY = -5.0 - 2.0 ;
    const double dZ = 13.0 - 4.0 ;
    const double dist( sqrt( dX*dX + dY*dY + dZ*dZ ) ) ;
    BOOST_CHECK_SMALL(Distance(pt4, pt1) - dist, epsilon) ;

    // Construct from Vector (lol, just kidding...)
    Vector vec(1., 2., 3.) ;
    Point pt5(vec.toPoint()) ;
    BOOST_CHECK_SMALL(pt5.x() - vec.x(), epsilon) ;
    BOOST_CHECK_SMALL(pt5.y() - vec.y(), epsilon) ;
    BOOST_CHECK_SMALL(pt5.z() - vec.z(), epsilon) ;
}

void Point_Methods(void)
{
    using Geometry::Point ;
    using BV::Details::epsilon ;

    // XYZ Methods
    XYZ_Methods<Point>() ;

    // Check operator - with Point
    {
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        Point xyz(array1) ;
        Point xyz1(6.0, -10.0, 26.0) ;
        Vector xyz2 ;
        xyz2 = xyz1 - xyz ;
        Vector expected(6.0-1.5, -10.0-2.0, 26.0-4.5) ;
        Details::CheckXYZ(xyz2, expected) ;
        BOOST_CHECK_SMALL(xyz2.norm(), expected.norm()) ;
    }

    // Check operator += with Vector
    {
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        Point xyz2(array1) ;
        Point xyz4(6.0, -10.0, 26.0) ;
        xyz4 += xyz2.toVector() ;
        Point xyz7(7.5, -8.0, 30.5) ;
        Details::CheckXYZ(xyz4, xyz7) ;
        BOOST_CHECK_SMALL(Distance(xyz4, xyz7), epsilon) ;
    }

    // Check operator -= with Vector
    {
        Point xyz1(4.5, -12.0, 21.5) ;
        Point xyz4(6.0, -10.0, 26.0) ;
        Eigen::Vector3d array1(Eigen::Vector3d::Zero()) ;
        array1 << 1.5, 2.0, 4.5 ;
        Point xyz2(array1) ;
        xyz4 -= xyz2.toVector() ;
        Details::CheckXYZ(xyz4, xyz1) ;
        BOOST_CHECK_SMALL(Distance(xyz4, xyz1), epsilon) ;
    }

    // Check operator + with Vector
    {
        Point xyz1(1.5, 2.0, 4.5) ;
        Point xyz8 = xyz1 + xyz1.toVector() ;
        Point xyz9(3.0, 4.0, 9.0) ;
        Details::CheckXYZ(xyz8, xyz9) ;
        BOOST_CHECK_SMALL(Distance(xyz8, xyz9), epsilon) ;
    }

    // Check operator - with Vector
    {
        Point xyz1(1.5, 2.0, 4.5) ;
        Point xyz8 = xyz1 + xyz1.toVector() ;
        Point xyz10((xyz8 - xyz1).toPoint()) ;
        Details::CheckXYZ(xyz10, xyz1) ;
        BOOST_CHECK_SMALL(Distance(xyz10, xyz1), epsilon) ;
    }

    // Adding points and vectors
    {
        Point pt1(1.0, 2.0, 4.0) ;
        const Point res1(1.1, 2.2, 4.3) ;
        const Point res2(1.2, 2.4, 4.6) ;
        const Point res3(1.3, 2.6, 4.9) ;
        const Vector adder(0.1, 0.2, 0.3) ;

        pt1 += adder ;
        BOOST_CHECK_SMALL(Distance(pt1, res1), epsilon) ;
        pt1 += adder ;
        BOOST_CHECK_SMALL(Distance(pt1, res2), epsilon) ;
        const Point pt2( pt1 + adder ) ;
        BOOST_CHECK_SMALL(Distance(pt2, res3), epsilon) ;
        const Point pt3( pt1 + adder ) ;
        BOOST_CHECK_SMALL(Distance(pt3, res3), epsilon) ;
    }
    // Adding and scaling
    {
        const Point pt1(1.0, 2.0, 3.0) ;
        const Point pt2(-1.0, 0.0, 1.0) ;

        const Point res( pt1 + 0.5*(pt2.toVector()) ) ;
        const Point expected(0.5, 2., 3.5);

        const double dist = Distance(res, expected) ;
        BOOST_CHECK_SMALL(dist, epsilon) ;
    }

    // Converters
    {
        const Point pt1(1.0, 2.0, 3.0) ;
        const Vector vec1(1.0, 2.0, 3.0) ;
        Details::CheckXYZ(pt1.toVector(), vec1) ;
        Details::CheckXYZ(pt1, vec1.toPoint()) ;
        Details::CheckXYZ(pt1, pt1.toPoint()) ;
    }
}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
