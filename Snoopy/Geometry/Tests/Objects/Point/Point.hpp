#pragma once
#ifndef __BV_Geometry_Tests_Objects_Point_Point_hpp__
#define __BV_Geometry_Tests_Objects_Point_Point_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void Point_Constructors( void ) ;
void Point_Methods( void ) ;

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Objects_Point_Point_hpp__
