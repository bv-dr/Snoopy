#pragma once
#ifndef __BV_Geometry_Tests_Objects_Tools_hpp__
#define __BV_Geometry_Tests_Objects_Tools_hpp__

#include <boost/test/unit_test.hpp>

#include "Geometry/GeometryTypedefs.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <typename T>
void CheckXYZ(const T & xyz1, const T & xyz2)
{
    using BV::Details::epsilon ;
    BOOST_CHECK_SMALL(xyz1.x() - xyz2.x(), epsilon) ;
    BOOST_CHECK_SMALL(xyz1.y() - xyz2.y(), epsilon) ;
    BOOST_CHECK_SMALL(xyz1.z() - xyz2.z(), epsilon) ;
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Objects_Tools_hpp__
