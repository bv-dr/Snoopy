#pragma once
#ifndef __BV_Geometry_Tests_Rotation_AxisAndAngleRotation_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_AxisAndAngleRotation_Tools_hpp__

#include <Eigen/Dense>

#include "Tests/Rotation/ABC/Tools.hpp"
#include "Geometry/Rotation.hpp"

using BV::Geometry::Rotation::AxisAndAngle ;

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckAxisAndAngleCoordinates( const double & angle1, const double & ax10,
                                   const double & ax11, const double & ax12,
                                   const double & angle2, const double & ax20,
                                   const double & ax21, const double & ax22) ;

void CheckAxisAndAngleCoordinates(AxisAndAngle & aa1, AxisAndAngle & aa2) ;

void CheckAxisAndAngleCoordinates(const Eigen::VectorXd & aa1, const Eigen::VectorXd & aa2) ;

template <>
void CheckUnknowns_<AxisAndAngle>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_AxisAndAngleRotation_Tools_hpp__
