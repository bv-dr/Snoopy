#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/AxisAndAngle/AxisAndAngle.hpp"
#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/AxisAndAngle/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

using BV::Geometry::Rotation::AxisAndAngle ;

namespace BV {
namespace Geometry {
namespace Tests {


void AxisAndAngle_Constructors(void)
{
    Eigen::Matrix3d I(Eigen::Matrix3d::Identity()) ;
    AxisAndAngle Iaa(I) ;

    AxisAndAngle aa ;
    AxisAndAngle aa2(aa) ;
    Details::CheckAxisAndAngleCoordinates(aa, aa2) ;
    const double angle(20.*M_PI/180.) ;
    aa.unknowns(Eigen::Vector4d(0., 0., 1., angle)) ;
    AxisAndAngle aa3(Vector(0., 0., 1.), angle) ;
    Details::CheckAxisAndAngleCoordinates(aa, aa3) ;
    BV::Tools::Tests::CheckEigenVectors(aa2.unknowns(), Eigen::Vector4d(1., 0., 0., 0.)) ;
}

void AxisAndAngle_Methods(void)
{
    using Geometry::Rotation::AxisAndAngle ;
    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<AxisAndAngle>() ;
    RotationABC_Methods<AxisAndAngle>() ;

    Details::CheckTypedCallsToOverLoads<AxisAndAngle>() ;

    // Check some matrix constructions with a set of angles values
    std::vector<Eigen::Matrix3d> allTestMatrices(Details::GetTestEigenRotationMatrices()) ;
    for (unsigned iR = 0 ; iR < allTestMatrices.size()-1; ++iR )
    {
        Eigen::Matrix3d eR(allTestMatrices[iR]) ;
        Rotation::AxisAndAngle aaR(eR) ;
        Details::ABCCheck<AxisAndAngle>(aaR, eR, 4, 1) ;
        Eigen::Matrix3d eR2(allTestMatrices[iR+1]) ;
        Rotation::AxisAndAngle aaR2(eR2) ;
        Details::ABCCheck_CompRot<AxisAndAngle>(aaR, eR, aaR2, eR2) ;
    }


    using BV::Details::epsilon ;
    // Test member method angle
    {
        AxisAndAngle aa ;
        double & rAngle(aa.angle()) ;
        rAngle += M_PI ;
        double angle(aa.angle()) ;
        BOOST_CHECK_SMALL(rAngle-angle, epsilon) ;
    }

    // Test member method axis
    {
        AxisAndAngle aa ;
        Eigen::Vector3d & rAxis(aa.axis()) ;
        rAxis = Eigen::Vector3d(0., 0., 1.) ;
        Eigen::Vector3d axis(aa.axis()) ;
        BV::Tools::Tests::CheckEigenVectors(rAxis, axis) ;
    }

    // Test member method getMatrix
    {
        Eigen::Matrix3d I(Eigen::Matrix3d::Identity()) ;
        AxisAndAngle aa(I) ;
        BV::Tools::Tests::CheckEigenMatrix(aa.getMatrix(), I) ;
    }
}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
