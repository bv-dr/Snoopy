#pragma once
#ifndef __BV_Geometry_Tests_Rotation_AxisAndAngle_AxisAndAngle_hpp__
#define __BV_Geometry_Tests_Rotation_AxisAndAngle_AxisAndAngle_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void AxisAndAngle_Constructors( void ) ;
void AxisAndAngle_Methods( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_AxisAndAngle_AxisAndAngle_hpp__
