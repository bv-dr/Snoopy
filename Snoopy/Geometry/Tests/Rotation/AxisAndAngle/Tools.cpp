#include <complex>

#include "Tests/Rotation/AxisAndAngle/Tools.hpp"

#include <boost/test/unit_test.hpp>

#include "Tools/Tests/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

#include "Tests/Rotation/Quaternion/Tools.hpp"

using BV::Geometry::Rotation::AxisAndAngle ;

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckAxisAndAngleCoordinates( const double & angle1, const double & ax10,
                                   const double & ax11, const double & ax12,
                                   const double & angle2, const double & ax20,
                                   const double & ax21, const double & ax22)
{
    using BV::Details::epsilon ;
    double angle1Modulo(AngleMinusPi_Pi(angle1)) ;
    double angle2Modulo(AngleMinusPi_Pi(angle2)) ;

    bool nullAngles( (std::abs(angle1) < epsilon) && (std::abs(angle2) < epsilon) ) ;
    // if null angles, do not check the axis... it is undefined...
    if(nullAngles)
    {
        BOOST_CHECK(nullAngles ) ;
        return ;
    }

    // We have to check for dual too..
    bool dualityCheck( (
                           (std::abs(angle1Modulo-angle2Modulo) < epsilon)
                        && (std::abs(ax10-ax20) < epsilon)
                        && (std::abs(ax11-ax21) < epsilon )
                        && (std::abs(ax12-ax22) < epsilon )
                        ) || (
                           (std::abs(angle1Modulo+angle2Modulo) < epsilon)
                            && (std::abs(ax10+ax20) < epsilon)
                            && (std::abs(ax11+ax21) < epsilon )
                            && (std::abs(ax12+ax22) < epsilon )
                             )
                      ) ;

    if( std::abs(std::abs(angle1) - M_PI) < epsilon )
    {
        bool M_PICheck = (
                            (std::abs(angle1Modulo+angle2Modulo) < epsilon)
                         && (std::abs(ax10-ax20) < epsilon)
                         && (std::abs(ax11-ax21) < epsilon )
                         && (std::abs(ax12-ax22) < epsilon )
                         ) || (
                            (std::abs(angle1Modulo-angle2Modulo) < epsilon)
                             && (std::abs(ax10+ax20) < epsilon)
                             && (std::abs(ax11+ax21) < epsilon )
                             && (std::abs(ax12+ax22) < epsilon )
                              ) ;
        BOOST_CHECK( (dualityCheck || M_PICheck) ) ;
        if(!(dualityCheck || M_PICheck))
            {
                std::cout << "angle1 = " << angle1 << "    |    " << "angle2 = " << angle2 << std::endl ;
                std::cout << "angle1Modulo = " << angle1Modulo << "    |    " << "angle2Modulo = " << angle2Modulo << std::endl ;
                std::cout << "ax10 = " << ax10 << "    |    " << "ax20 = " << ax20 << std::endl ;
                std::cout << "ax11 = " << ax11 << "    |    " << "ax21 = " << ax21 << std::endl ;
                std::cout << "ax12 = " << ax12 << "    |    " << "ax22 = " << ax22 << std::endl << std::endl ;
            }
    }
    else
    {
        BOOST_CHECK(dualityCheck) ;
        if(!(dualityCheck))
        {
            std::cout << "angle1 = " << angle1 << "    |    " << "angle2 = " << angle2 << std::endl ;
            std::cout << "angle1Modulo = " << angle1Modulo << "    |    " << "angle2Modulo = " << angle2Modulo << std::endl ;
            std::cout << "ax10 = " << ax10 << "    |    " << "ax20 = " << ax20 << std::endl ;
            std::cout << "ax11 = " << ax11 << "    |    " << "ax21 = " << ax21 << std::endl ;
            std::cout << "ax12 = " << ax12 << "    |    " << "ax22 = " << ax22 << std::endl << std::endl ;
        }
    }
}

void CheckAxisAndAngleCoordinates(AxisAndAngle & aa1, AxisAndAngle & aa2)
{
    using BV::Details::epsilon ;
    const Eigen::Vector3d axis1(aa1.axis()) ;
    const Eigen::Vector3d axis2(aa2.axis()) ;
    const double angle1(aa1.angle()) ;
    const double angle2(aa2.angle()) ;
    CheckAxisAndAngleCoordinates(angle1, axis1(0), axis1(1), axis1(2),
                                 angle2, axis2(0), axis2(1), axis2(2)) ;
}

void CheckAxisAndAngleCoordinates(const Eigen::VectorXd & aa1, const Eigen::VectorXd & aa2)
{
    CheckAxisAndAngleCoordinates(aa1(3), aa1(0), aa1(1), aa1(2),
                                 aa2(3), aa2(0), aa2(1), aa2(2)) ;
}

Eigen::Vector4d GetAxisAndAngleFromR_(Eigen::Matrix3d R)
{
    // const double epsilon( std::numeric_limits< double >::epsilon() ) ;
    using BV::Details::epsilon ;
    Eigen::Vector4d res(Eigen::Vector4d::Zero()) ;

    // FIXME depends on quaternion...
    // Tried with eigenvalues and eigenvectors without success...
    Eigen::Vector4d q(Details::GetQuaternionFromR(R)) ;
    Eigen::Vector3d qVec(q.tail(3)) ;
    double norm = qVec.norm() ;
    if (norm < epsilon)
    {
      res(3) = 0. ;
      res.head(3) << 1., 0., 0. ;
    }
    else
    {
        // res(3) = 2.*std::acos(q(0)) ;
        res(3) = 2.*std::acos( std::min( std::max(-1.,q(0)) , 1. ) ) ;
        res.head(3) = qVec / norm ;
    }
    return res ;
}

template <>
void CheckUnknowns_<Rotation::AxisAndAngle>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    Eigen::VectorXd unknowns(abc.unknowns()) ;
    Eigen::VectorXd expectedUnknowns(GetAxisAndAngleFromR_(R)) ;
    Details::CheckAxisAndAngleCoordinates(unknowns, expectedUnknowns) ;
}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
