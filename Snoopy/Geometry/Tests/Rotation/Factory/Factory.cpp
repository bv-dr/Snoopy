#include <boost/test/unit_test.hpp>
#include <memory>

#include "Tests/Rotation/Factory/Factory.hpp"
#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"
#include "Tools/Tests/Tools.hpp"

#include "Tests/Rotation/Tools.hpp"
#include "Tests/Rotation/ABC/Tools.hpp"
#include "Tests/Rotation/AxisAndAngle/Tools.hpp"
#include "Tests/Rotation/BasisVectors/Tools.hpp"
#include "Tests/Rotation/EulerAngles/Tools.hpp"
#include "Tests/Rotation/HorizontalPlane/Tools.hpp"
#include "Tests/Rotation/MRP/Tools.hpp"
#include "Tests/Rotation/Quaternion/Tools.hpp"
#include "Tests/Rotation/RotationMatrix/Tools.hpp"

#include "Tests/Rotation/Factory/Tools.hpp"


namespace BV {
namespace Geometry {
namespace Tests {


void Factory_Rotation_Constructors(void)
{
    using BV::Geometry::RotatorTypeEnum ;
    using BV::Geometry::Factories::RotatorsFactory ;
    using BV::Details::epsilon ;

    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Geometry::Rotation::BasisVectors ;
    using BV::Geometry::Rotation::EulerAngles ;
    using BV::Geometry::Rotation::HorizontalPlane ;
    using BV::Geometry::Rotation::MRP ;
    using BV::Geometry::Rotation::Quaternion ;
    using BV::Geometry::Rotation::RotationMatrix ;

    using BV::Geometry::Rotation::ABC ;

    Eigen::Matrix3d I3(Eigen::Matrix3d::Identity()) ;

    for (Eigen::Matrix3d eR : Details::GetTestEigenRotationMatrices())
    {

        std::shared_ptr<BV::Geometry::Rotation::ABC> p_aa(
                RotatorsFactory::create(RotatorTypeEnum::AXIS_AND_ANGLE, eR )
                                                                    ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_aa->getMatrix(), eR) ;
        Details::CheckUnknowns_<AxisAndAngle>(*p_aa, eR) ;
        AxisAndAngle aa(static_cast<AxisAndAngle &> (*p_aa)) ;

        // Check all others constructors of AxisAndAngle
        {
            // The dummy constructor
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_aa2(
                    RotatorsFactory::create(RotatorTypeEnum::AXIS_AND_ANGLE ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_aa2->getMatrix(), I3) ;
            }

            // The constructor from Eigen::Vector3d axis and angle
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_aa2(
                    RotatorsFactory::create(RotatorTypeEnum::AXIS_AND_ANGLE,
                                      aa.axis(),
                                      aa.angle() )
                                      ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_aa2->getMatrix(), eR) ;
            }

            // The constructor from Geometry::Vector axis and angle
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_aa2(
                    RotatorsFactory::create(RotatorTypeEnum::AXIS_AND_ANGLE,
                                      Vector(aa.axis()),
                                      aa.angle() )
                                      ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_aa2->getMatrix(), eR) ;
            }

            // The constructor from Eigen::VectorXd unknowns
            {
                Eigen::VectorXd unknowns(aa.unknowns()) ;
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_aa2(
                    RotatorsFactory::create(RotatorTypeEnum::AXIS_AND_ANGLE,
                                      unknowns )
                                      ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_aa2->getMatrix(), eR) ;
            }

            // The constructor from AxisAndAngle
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_aa2(
                    RotatorsFactory::create(RotatorTypeEnum::AXIS_AND_ANGLE,
                                      aa )
                                      ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_aa2->getMatrix(), eR) ;
            }
        }



        std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv(
                RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS, eR )
                                                            ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_bv->getMatrix(), eR) ;
        Details::CheckUnknowns_<BasisVectors>(*p_bv, eR) ;
        BasisVectors bv(static_cast<BasisVectors &> (*p_bv)) ;

        // Check all others constructors of BasisVectors
        {
            // The constructor from 3 Eigen::Vector3d
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv2(
                    RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS,
                                      bv.d1().toArray(),
                                      bv.d2().toArray(),
                                      bv.d3().toArray() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_bv2->getMatrix(), eR) ;
            }

            // The constructor from 3 Geometry::Vector
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv2(
                    RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS,
                                      bv.d1(),
                                      bv.d2(),
                                      bv.d3() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_bv2->getMatrix(), eR) ;
            }

            // The dummy constructor
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv2(
                    RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_bv2->getMatrix(), I3) ;
            }

            // The constructor from BasisVector
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv2(
                    RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS,
                                      bv ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_bv2->getMatrix(), eR) ;
            }

            // The constructor from 9 double
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv2(
                    RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS,
                                      eR(0,0), eR(1,0), eR(2,0),
                                      eR(0,1), eR(1,1), eR(2,1),
                                      eR(0,2), eR(1,2), eR(2,2) ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_bv2->getMatrix(), eR) ;
            }

            // The constructor from Eigen::VectorXd of unknowns
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_bv2(
                    RotatorsFactory::create(RotatorTypeEnum::BASIS_VECTORS,
                                      bv.unknowns() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_bv2->getMatrix(), eR) ;
            }
        }

        std::shared_ptr<BV::Geometry::Rotation::ABC> p_mrp(
                RotatorsFactory::create(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS, eR )
                                                            ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_mrp->getMatrix(), eR) ;
        Details::CheckUnknowns_<MRP>(*p_mrp, eR) ;
        MRP mrp(static_cast<MRP &> (*p_mrp)) ;

        // Check all others constructors of MRP
        {
            // The dummy constructor
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_mrp2(
                    RotatorsFactory::create(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_mrp2->getMatrix(), I3) ;
            }

            // The constructor from 3 double
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_mrp2(
                    RotatorsFactory::create(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS,
                                      mrp.sigma1(),
                                      mrp.sigma2(),
                                      mrp.sigma3() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_mrp2->getMatrix(), eR) ;
            }

            // The constructor from Eigen::MatrixXd of unknowns
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_mrp2(
                    RotatorsFactory::create(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS,
                                      mrp.unknowns() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_mrp2->getMatrix(), eR) ;
            }

            // The constructor from other MRP
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_mrp2(
                    RotatorsFactory::create(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS,
                                      mrp ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_mrp2->getMatrix(), eR) ;
            }
        }


        std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat(
                RotatorsFactory::create(RotatorTypeEnum::QUATERNION, eR )
                                                            ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_quat->getMatrix(), eR) ;
        Details::CheckUnknowns_<Quaternion>(*p_quat, eR) ;
        Quaternion quat(static_cast<Quaternion &> (*p_quat)) ;

        // Check all others constructors of Quaternion
        {
            // The constructor from 4 double
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat2(
                    RotatorsFactory::create(RotatorTypeEnum::QUATERNION,
                                      quat.w(), quat.x(), quat.y(), quat.z() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_quat2->getMatrix(), eR) ;
            }

            // The constructor from Eigen::VectorXd of unknowns
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat2(
                    RotatorsFactory::create(RotatorTypeEnum::QUATERNION,
                                      quat.unknowns() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_quat2->getMatrix(), eR) ;
            }

            // The dummy constructor
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat2(
                    RotatorsFactory::create(RotatorTypeEnum::QUATERNION ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_quat2->getMatrix(), I3) ;
            }

            // The constructor from other Quaternion
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat2(
                    RotatorsFactory::create(RotatorTypeEnum::QUATERNION,
                                      quat ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_quat2->getMatrix(), eR) ;
            }
        }

        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rm(
                RotatorsFactory::create(RotatorTypeEnum::ROTATION_MATRIX, eR )
                                                            ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rm->getMatrix(), eR) ;
        Details::CheckUnknowns_<RotationMatrix>(*p_rm, eR) ;
        RotationMatrix rm(static_cast<RotationMatrix &> (*p_rm)) ;

        // Check all others constructors of RotationMatrix
        {
            // The constructor from other RotationMatrix
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_rm2(
                    RotatorsFactory::create(RotatorTypeEnum::ROTATION_MATRIX,
                                      rm ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_rm2->getMatrix(), eR) ;
            }

            // The constructor from 9 double
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_rm2(
                    RotatorsFactory::create(RotatorTypeEnum::ROTATION_MATRIX,
                                      eR(0,0), eR(1,0), eR(2,0),
                                      eR(0,1), eR(1,1), eR(2,1),
                                      eR(0,2), eR(1,2), eR(2,2) ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_rm2->getMatrix(), eR) ;
            }

            // The constructor from Eigen::VectorXd of unknowns
            {
                std::shared_ptr<BV::Geometry::Rotation::ABC> p_rm2(
                    RotatorsFactory::create(RotatorTypeEnum::ROTATION_MATRIX,
                                      rm.unknowns() ) ) ;
                BV::Tools::Tests::CheckEigenMatrix(p_rm2->getMatrix(), eR) ;
            }
        }


        // The constructor from self and others
        {
            // AxisAndAngle
            // (the constructor signature used is ...(const BV::Geometry::Rotation::ABC &) )
            Details::CheckFactoryConverters(RotatorTypeEnum::AXIS_AND_ANGLE, eR) ;
            // BasisVectors
            // (the constructor signature used is ...(const BV::Geometry::Rotation::ABC &) )
            Details::CheckFactoryConverters(RotatorTypeEnum::BASIS_VECTORS, eR) ;
            // MRP
            // (the constructor signature used is ...(const BV::Geometry::Rotation::ABC &) )
            Details::CheckFactoryConverters(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS, eR) ;
            // Quaternion
            // (the constructor signature used is ...(const BV::Geometry::Rotation::ABC &) )
            Details::CheckFactoryConverters(RotatorTypeEnum::QUATERNION, eR) ;
            // RotationMatrix
            // (the constructor signature used is ...(const BV::Geometry::Rotation::ABC &) )
            Details::CheckFactoryConverters(RotatorTypeEnum::ROTATION_MATRIX, eR) ;
        }

        for (RotatorTypeEnum eulerEnum : Details::GetEulerEnum())
        {
            std::shared_ptr<BV::Geometry::Rotation::ABC> p_ea(
                    RotatorsFactory::create(eulerEnum, eR )
                                                                ) ;

            BV::Tools::Tests::CheckEigenMatrix(p_ea->getMatrix(), eR) ;
            Details::CheckFactoryConverters(eulerEnum, eR) ;
        }
    }

    for (double angle : Details::GetTestAngles() )
    {
        Eigen::Matrix3d RZ(Details::GetRZMatrix(angle)) ;
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_hp(
                RotatorsFactory::create(RotatorTypeEnum::HORIZONTAL_PLANE, RZ )
                                                            ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_hp->getMatrix(), RZ) ;
        Details::CheckUnknowns_<HorizontalPlane>(*p_hp, RZ) ;

        // The constructor from self and others
        // HorizontalPlane
        // (the constructor signature used is ...(const BV::Geometry::Rotation::ABC &) )
        Details::CheckFactoryConverters(RotatorTypeEnum::HORIZONTAL_PLANE, RZ) ;

        {
            Details::CheckHorizontalPlaneToOtherFactoryConverters(RotatorTypeEnum::AXIS_AND_ANGLE, RZ) ;
            Details::CheckHorizontalPlaneToOtherFactoryConverters(RotatorTypeEnum::BASIS_VECTORS, RZ) ;
            Details::CheckHorizontalPlaneToOtherFactoryConverters(RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS, RZ) ;
            Details::CheckHorizontalPlaneToOtherFactoryConverters(RotatorTypeEnum::QUATERNION, RZ) ;
            Details::CheckHorizontalPlaneToOtherFactoryConverters(RotatorTypeEnum::ROTATION_MATRIX, RZ) ;
            for (RotatorTypeEnum eulerEnum : Details::GetEulerEnum())
            {
                Details::CheckHorizontalPlaneToOtherFactoryConverters(eulerEnum, RZ) ;
            }
        }
    }

    // The Quaternion constructor from interpolation with 2 Eigen::Vector3d
    {
        Eigen::Vector3d v1(1., 2., 3.) ;
        Eigen::Vector3d v2(1., 2., 3.) ;
        Quaternion q_v1_v2(v1, v2) ;
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat2(
            RotatorsFactory::create(RotatorTypeEnum::QUATERNION,
                              v1, v2 ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_quat2->getMatrix(), q_v1_v2.getMatrix()) ;
    }

    // The Quaternion constructor from interpolation with 2 Geometry::Vector
    {
        Geometry::Vector v1(1., 2., 3.) ;
        Geometry::Vector v2(1., 2., 3.) ;
        Quaternion q_v1_v2(v1, v2) ;
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_quat2(
            RotatorsFactory::create(RotatorTypeEnum::QUATERNION,
                              v1, v2 ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_quat2->getMatrix(), q_v1_v2.getMatrix()) ;
    }
}


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
