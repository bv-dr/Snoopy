#include <boost/test/unit_test.hpp>

#include "Tools/Tests/Tools.hpp"
#include "Tests/Rotation/Factory/Tools.hpp"


namespace BV {
namespace Geometry {
namespace Tests {
namespace Details {

void CheckFactoryConverters( const BV::Geometry::RotatorTypeEnum & rotatorTypeEnum,
                             const Eigen::Matrix3d & eR )
{
    using BV::Geometry::Factories::RotatorsFactory ;

    using BV::Geometry::Rotation::ABC ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Geometry::Rotation::BasisVectors ;
    using BV::Geometry::Rotation::MRP ;
    using BV::Geometry::Rotation::Quaternion ;
    using BV::Geometry::Rotation::RotationMatrix ;
    using Geometry::Rotation::EulerAngles_XYX_i ;
    using Geometry::Rotation::EulerAngles_XYZ_i ;
    using Geometry::Rotation::EulerAngles_XZX_i ;
    using Geometry::Rotation::EulerAngles_XZY_i ;
    using Geometry::Rotation::EulerAngles_YXY_i ;
    using Geometry::Rotation::EulerAngles_YXZ_i ;
    using Geometry::Rotation::EulerAngles_YZX_i ;
    using Geometry::Rotation::EulerAngles_YZY_i ;
    using Geometry::Rotation::EulerAngles_ZXY_i ;
    using Geometry::Rotation::EulerAngles_ZXZ_i ;
    using Geometry::Rotation::EulerAngles_ZYX_i ;
    using Geometry::Rotation::EulerAngles_ZYZ_i ;
    using Geometry::Rotation::EulerAngles_XYX_e ;
    using Geometry::Rotation::EulerAngles_XYZ_e ;
    using Geometry::Rotation::EulerAngles_XZX_e ;
    using Geometry::Rotation::EulerAngles_XZY_e ;
    using Geometry::Rotation::EulerAngles_YXY_e ;
    using Geometry::Rotation::EulerAngles_YXZ_e ;
    using Geometry::Rotation::EulerAngles_YZX_e ;
    using Geometry::Rotation::EulerAngles_YZY_e ;
    using Geometry::Rotation::EulerAngles_ZXY_e ;
    using Geometry::Rotation::EulerAngles_ZXZ_e ;
    using Geometry::Rotation::EulerAngles_ZYX_e ;
    using Geometry::Rotation::EulerAngles_ZYZ_e ;

    AxisAndAngle aa( eR ) ;
    BasisVectors bv( eR ) ;
    MRP mrp( eR ) ;
    Quaternion quat( eR ) ;
    RotationMatrix rm( eR ) ;

    EulerAngles_XYX_i ea_XYX_i( eR ) ;
    EulerAngles_XYZ_i ea_XYZ_i( eR ) ;
    EulerAngles_XZX_i ea_XZX_i( eR ) ;
    EulerAngles_XZY_i ea_XZY_i( eR ) ;
    EulerAngles_YXY_i ea_YXY_i( eR ) ;
    EulerAngles_YXZ_i ea_YXZ_i( eR ) ;
    EulerAngles_YZX_i ea_YZX_i( eR ) ;
    EulerAngles_YZY_i ea_YZY_i( eR ) ;
    EulerAngles_ZXY_i ea_ZXY_i( eR ) ;
    EulerAngles_ZXZ_i ea_ZXZ_i( eR ) ;
    EulerAngles_ZYX_i ea_ZYX_i( eR ) ;
    EulerAngles_ZYZ_i ea_ZYZ_i( eR ) ;

    EulerAngles_XYX_e ea_XYX_e( eR ) ;
    EulerAngles_XYZ_e ea_XYZ_e( eR ) ;
    EulerAngles_XZX_e ea_XZX_e( eR ) ;
    EulerAngles_XZY_e ea_XZY_e( eR ) ;
    EulerAngles_YXY_e ea_YXY_e( eR ) ;
    EulerAngles_YXZ_e ea_YXZ_e( eR ) ;
    EulerAngles_YZX_e ea_YZX_e( eR ) ;
    EulerAngles_YZY_e ea_YZY_e( eR ) ;
    EulerAngles_ZXY_e ea_ZXY_e( eR ) ;
    EulerAngles_ZXZ_e ea_ZXZ_e( eR ) ;
    EulerAngles_ZYX_e ea_ZYX_e( eR ) ;
    EulerAngles_ZYZ_e ea_ZYZ_e( eR ) ;

    // AxisAndAngle
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator(
            RotatorsFactory::create(rotatorTypeEnum,
                              aa )
                              ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    // BasisVectors
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator(
            RotatorsFactory::create(rotatorTypeEnum,
                              bv )
                              ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    // MRP
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator(
            RotatorsFactory::create(rotatorTypeEnum,
                              mrp )
                              ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    // Quaternion
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator(
            RotatorsFactory::create(rotatorTypeEnum,
                              quat )
                              ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    // RotationMatrix
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator(
            RotatorsFactory::create(rotatorTypeEnum,
                              rm )
                              ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }


    // EulerAngles...
    /*
    EulerAngles_XYX ea_XYX( eR ) ;
    EulerAngles_XYZ ea_XYZ( eR ) ;
    EulerAngles_XZX ea_XZX( eR ) ;
    EulerAngles_XZY ea_XZY( eR ) ;
    EulerAngles_YXY ea_YXY( eR ) ;
    EulerAngles_YXZ ea_YXZ( eR ) ;
    EulerAngles_YZX ea_YZX( eR ) ;
    EulerAngles_YZY ea_YZY( eR ) ;
    EulerAngles_ZXY ea_ZXY( eR ) ;
    EulerAngles_ZXZ ea_ZXZ( eR ) ;
    EulerAngles_ZYX ea_ZYX( eR ) ;
    EulerAngles_ZYZ ea_ZYZ( eR ) ;
    */
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XYX_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XYZ_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XZX_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XZY_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YXY_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YXZ_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YZX_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YZY_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZXY_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZXZ_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZYX_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZYZ_i ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }

    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XYX_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XYZ_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XZX_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_XZY_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YXY_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YXZ_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YZX_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_YZY_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZXY_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZXZ_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZYX_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator( RotatorsFactory::create(rotatorTypeEnum, ea_ZYZ_e ) ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }

}


void CheckHorizontalPlaneToOtherFactoryConverters( const BV::Geometry::RotatorTypeEnum & rotatorTypeEnum,
                                                   const Eigen::Matrix3d & eR )
{
    using BV::Geometry::Factories::RotatorsFactory ;

    using BV::Geometry::Rotation::ABC ;
    using BV::Geometry::Rotation::HorizontalPlane ;

    HorizontalPlane hp( eR ) ;

    // HorizontalPlane
    {
        std::shared_ptr<BV::Geometry::Rotation::ABC> p_rotator(
            RotatorsFactory::create(rotatorTypeEnum,
                              hp )
                              ) ;
        BV::Tools::Tests::CheckEigenMatrix(p_rotator->getMatrix(), eR) ;
    }
}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
