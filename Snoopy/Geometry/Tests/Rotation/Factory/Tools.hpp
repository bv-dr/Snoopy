#pragma once
#ifndef __BV_Geometry_Tests_Rotation_Factory_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_Factory_Tools_hpp__

#include <Eigen/Dense>

#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckFactoryConverters( const BV::Geometry::RotatorTypeEnum & rotatorTypeEnum,
                             const Eigen::Matrix3d & eR) ;

void CheckHorizontalPlaneToOtherFactoryConverters( const BV::Geometry::RotatorTypeEnum & rotatorTypeEnum,
                                                   const Eigen::Matrix3d & eR ) ;


} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Factory_Tools_hpp__
