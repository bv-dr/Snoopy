#pragma once
#ifndef __BV_Geometry_Tests_Rotation_ABC_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_ABC_Tools_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tools/Tests/Tools.hpp"
#include "Geometry/Rotation/ABC.hpp"


namespace BV {
namespace Geometry {

namespace Tests {
namespace Details {

void CheckRVects_(const BV::Geometry::Rotation::ABC & abc, const Eigen::Matrix3d & R);

void CheckConstraints_(Rotation::ABC & abc, unsigned nConstraints) ;

template <typename T>
void CheckUnknowns_(BV::Geometry::Rotation::ABC & abc,
                    const Eigen::Matrix3d & R)
{
    T tmp(R) ;
    Eigen::VectorXd unknowns(abc.unknowns()) ;
    CheckEigenVectors(unknowns, tmp.unknowns()) ;
    Eigen::VectorXd constraints(abc.constraints()) ;
    CheckEigenVectors(constraints, tmp.constraints()) ;
}

template <typename T>
void ABCCheck(BV::Geometry::Rotation::ABC & abc, const Eigen::Matrix3d & R,
              unsigned nUnknowns, unsigned nConstraints)
{
    // call on ABC methods
    // unknowns and constraints
    BOOST_CHECK_EQUAL(nUnknowns, abc.nUnknowns()) ;
    BOOST_CHECK_EQUAL(nConstraints, abc.nConstraints()) ;

    BV::Tools::Tests::CheckEigenMatrix(abc.getMatrix(), R) ;

    // operator* eigen vector/Geometry::Vector and Geometry::Point
    CheckUnknowns_<T>(abc, R) ;
    CheckRVects_(abc, R) ;

    // Rotate abc by itself
    T abc2(abc) ;
    abc.addOtherRotationAtLeft(abc) ;
    Eigen::Matrix3d R_2(R*R) ;

    // Check unknowns, constraints and directors
    CheckUnknowns_<T>(abc, R_2) ;
    CheckConstraints_(abc, nConstraints) ;
    CheckRVects_(abc, R_2) ;
    BV::Tools::Tests::CheckEigenMatrix(abc.getMatrix(), R_2) ;

    // Get back to original rotation
    abc.subtractOtherRotationAtLeft(abc2) ;
    CheckUnknowns_<T>(abc, R) ;
    CheckConstraints_(abc, nConstraints) ;
    BV::Tools::Tests::CheckEigenMatrix(abc.getMatrix(), R) ;

    // inverse Rotation
    abc.inverse() ;
    Eigen::Matrix3d Rt(R.transpose()) ;
    CheckUnknowns_<T>(abc, Rt) ;
    BV::Tools::Tests::CheckEigenMatrix(abc.getMatrix(), Rt) ;
    abc.inverse() ;
    CheckUnknowns_<T>(abc, R) ;
    BV::Tools::Tests::CheckEigenMatrix(abc.getMatrix(), R) ;

}


template <typename T>
void ABCCheck_CompRot(BV::Geometry::Rotation::ABC & abc1, const Eigen::Matrix3d & R1,
                      BV::Geometry::Rotation::ABC & abc2, const Eigen::Matrix3d & R2)
{
    BV::Tools::Tests::CheckEigenMatrix(abc1.getMatrix(), R1) ;
    BV::Tools::Tests::CheckEigenMatrix(abc2.getMatrix(), R2) ;

    // operator* eigen vector/Geometry::Vector and Geometry::Point
    CheckUnknowns_<T>(abc1, R1) ;
    CheckUnknowns_<T>(abc2, R2) ;
    CheckRVects_(abc1, R1) ;
    CheckRVects_(abc2, R2) ;

    // Rotate abc1 by abc2 at left
    T abc1_tmp(abc1) ;
    abc1_tmp.addOtherRotationAtLeft(abc2) ;
    Eigen::Matrix3d R_product(R2*R1) ;

    // Check unknowns, constraints and directors
    CheckUnknowns_<T>(abc1_tmp, R_product) ;
    CheckRVects_(abc1_tmp, R_product) ;
    BV::Tools::Tests::CheckEigenMatrix(abc1_tmp.getMatrix(), R_product) ;

    // Get back to original rotation
    abc1_tmp.subtractOtherRotationAtLeft(abc2) ;
    CheckUnknowns_<T>(abc1_tmp, R1) ;
    BV::Tools::Tests::CheckEigenMatrix(abc1_tmp.getMatrix(), R1) ;

    // Rotate abc1 by abc2 at left
    T abc11_tmp(abc1) ;
    abc11_tmp.addOtherRotationAtRight(abc2) ;
    R_product = R1*R2 ;

    // Check unknowns, constraints and directors
    CheckUnknowns_<T>(abc11_tmp, R_product) ;
    CheckRVects_(abc11_tmp, R_product) ;
    BV::Tools::Tests::CheckEigenMatrix(abc11_tmp.getMatrix(), R_product) ;

    // Get back to original rotation
    abc11_tmp.subtractOtherRotationAtRight(abc2) ;
    CheckUnknowns_<T>(abc11_tmp, R1) ;
    BV::Tools::Tests::CheckEigenMatrix(abc11_tmp.getMatrix(), R1) ;
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_ABC_ABC_hpp__
