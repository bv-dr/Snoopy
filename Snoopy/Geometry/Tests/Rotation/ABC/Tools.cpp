#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Rotation/ABC/Tools.hpp"
#include "Tests/Objects/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {
namespace Details {

void CheckRVects_(const Rotation::ABC & abc,
                  const Eigen::Matrix3d & R )
{
    // Check the method of abstract rotation class ABC on a set of vectors/point
    // Methods tested:
    //    - rotate
    //    - operator*
    //    - inverseRotate
    //    - d1
    //    - d2
    //    - d3
    // We check the the rotation represented by abc perform the same operations
    // as the ones expected by the rotation with a rotation matrix R
    using Geometry::Point ;
    using Geometry::Vector ;

    std::vector<Eigen::Vector3d> vectors ;
    for (Eigen::Vector3d eVect : Details::GetTestEigenVectors())
    {
        Vector gVect(eVect) ;
        Point pt(eVect) ;

        // The expected results
        Eigen::Vector3d compEU1(R*eVect) ;
        Vector compGU1(compEU1) ;
        Point compPt(compEU1) ;

        // Check rotation of vectors/point with member method rotate
        Eigen::Vector3d eU2(abc.rotate(eVect)) ;
        Vector gU2(abc.rotate(gVect)) ;
        Point ptU2(abc.rotate(pt)) ;

        BV::Tools::Tests::CheckEigenVectors(eU2, compEU1) ;
        CheckXYZ<Vector>(gU2, compGU1) ;
        CheckXYZ<Point>(ptU2, compPt) ;

        // Check rotation of vectors/point with operator *
        Eigen::Vector3d eU1(abc * eVect) ;
        Vector gU1(abc * gVect) ;
        Point ptU1(abc * pt) ;

        BV::Tools::Tests::CheckEigenVectors(eU1, compEU1) ;
        CheckXYZ<Vector>(gU1, compGU1) ;
        CheckXYZ<Point>(ptU1, compPt) ;


        // The inverse checked with initial vectors/points
        // using member method inverseRotate
        Eigen::Vector3d eUInv(abc.inverseRotate(eU1)) ;
        Vector gUInv(abc.inverseRotate(gU1)) ;
        Point ptUInv(abc.inverseRotate(ptU1)) ;

        BV::Tools::Tests::CheckEigenVectors(eUInv, eVect) ;
        CheckXYZ<Vector>(gUInv, gVect) ;
        CheckXYZ<Point>(ptUInv, pt) ;

        // Check directors
        Vector d1(abc.d1()) ;
        Vector d2(abc.d2()) ;
        Vector d3(abc.d3()) ;

        Eigen::Vector3d X(1., 0., 0.) ;
        Eigen::Vector3d Y(0., 1., 0.) ;
        Eigen::Vector3d Z(0., 0., 1.) ;
        Vector expectedD1(Vector(R*X)) ;
        Vector expectedD2(Vector(R*Y)) ;
        Vector expectedD3(Vector(R*Z)) ;
        CheckXYZ<Vector>(d1, expectedD1) ;
        CheckXYZ<Vector>(d2, expectedD2) ;
        CheckXYZ<Vector>(d3, expectedD3) ;

    }
}

void CheckConstraints_(Rotation::ABC & abc,
                       unsigned nConstraints)
{
    using BV::Details::epsilon ;

    Eigen::VectorXd constraints(abc.constraints()) ;
    for (unsigned i=0; i<nConstraints; ++i)
    {
        BOOST_CHECK_SMALL(constraints(i), epsilon) ;
    }
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

