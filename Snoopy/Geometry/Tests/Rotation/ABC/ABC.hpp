#pragma once
#ifndef __BV_Geometry_Tests_Rotation_ABC_ABC_hpp__
#define __BV_Geometry_Tests_Rotation_ABC_ABC_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tools/Tests/Tools.hpp"
#include "Tests/Objects/Tools.hpp"

#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"


namespace BV {
namespace Geometry {
namespace Tests {

template <typename T>
void ABC_Methods( void )
{
    using BV::Details::epsilon ;

    // Test calls to:
    //    - nUnkowns
    //    - nConstraints
    {
        T r ;
        r.nUnknowns() ;
        r.nConstraints() ;
    }

    // Test call to rotate an Geometry::Vector
    {
        using Geometry::Vector ;
        T r ;
        Vector gVect(1., 1., 2.) ;
        Vector gVect2(1., 1., 2.) ;
        Vector res(r.rotate(gVect)) ;
        Details::CheckXYZ<Vector>(gVect, gVect2) ;
        Details::CheckXYZ<Vector>(res, gVect2) ;
    }

    // Test call to rotate an Geometry::Point
    {
        using Geometry::Point ;
        T r ;
        Point pt(1., 1., 2.) ;
        Point pt2(1., 1., 2.) ;
        Point res(r.rotate(pt)) ;
        Details::CheckXYZ<Point>(pt, pt2) ;
        Details::CheckXYZ<Point>(res, pt2) ;
    }


    // Test call to inverseRotate an Geometry::Vector
    {
        using Geometry::Vector ;
        T r ;
        T r2 ;
        r.inverse() ;
        BOOST_CHECK(r == r2) ;
    }

    // Test call to inverseRotate an Geometry::Vector
    {
        using Geometry::Vector ;
        T r ;
        Vector gVect(1., 1., 2.) ;
        Vector gVect2(1., 1., 2.) ;
        Vector res(r.inverseRotate(gVect)) ;
        Details::CheckXYZ<Vector>(gVect, gVect2) ;
        Details::CheckXYZ<Vector>(res, gVect2) ;
    }

    // Test call to inverseRotate an Geometry::Point
    {
        using Geometry::Point ;
        T r ;
        Point pt(1., 1., 2.) ;
        Point pt2(1., 1., 2.) ;
        Point res(r.inverseRotate(pt)) ;
        Details::CheckXYZ<Point>(pt, pt2) ;
        Details::CheckXYZ<Point>(res, pt2) ;
    }

    // Test call to operator* to rotate an Eigen::Vector3d
    {
        T r ;
        Eigen::Vector3d eVect(1., 1., 2.) ;
        Eigen::Vector3d eVect2(1., 1., 2.) ;
        Eigen::Vector3d res(r * eVect) ;
        BV::Tools::Tests::CheckEigenVectors(eVect, eVect2) ;
        BV::Tools::Tests::CheckEigenVectors(res, eVect2) ;
    }

    // Test call to operator* to rotate an Geometry::Vector
    {
        using Geometry::Vector ;
        T r ;
        Vector gVect(1., 1., 2.) ;
        Vector gVect2(1., 1., 2.) ;
        Vector res(r * gVect) ;
        Details::CheckXYZ<Vector>(gVect, gVect2) ;
        Details::CheckXYZ<Vector>(res, gVect2) ;
    }

    // Test call to operator* to rotate an Geometry::Point
    {
        using Geometry::Point ;
        T r ;
        Point pt(1., 1., 2.) ;
        Point pt2(1., 1., 2.) ;
        Point res(r * pt) ;
        Details::CheckXYZ<Point>(pt, pt2) ;
        Details::CheckXYZ<Point>(res, pt2) ;
    }

    // Test call to member method d1
    {
        using Geometry::Vector ;
        T r ;
        Vector d1(r.d1()) ;
        Details::CheckXYZ<Vector>(d1, Vector(1., 0., 0.)) ;
    }

    // Test call to member method d2
    {
        using Geometry::Vector ;
        T r ;
        Vector d2(r.d2()) ;
        Details::CheckXYZ<Vector>(d2, Vector(0., 1., 0.)) ;
    }

    // Test call to member method d2
    {
        using Geometry::Vector ;
        T r ;
        Vector d3(r.d3()) ;
        Details::CheckXYZ<Vector>(d3, Vector(0., 0., 1.)) ;
    }

    // Test call to member method getMatrix
    {
        Eigen::Matrix3d I(Eigen::Matrix3d::Identity()) ;
        T r ;
        Eigen::Matrix3d rI(r.getMatrix()) ;
        BV::Tools::Tests::CheckEigenMatrix(I, rI) ;
    }

}

template <typename T>
void RotationABC_Methods( void )
{
    // Test call to operator *= with other ABC
    {
        T t1 ;
        T t2 ;
        t1 *= t2 ;
    }

    // Test call to operator -=  with other ABC
    {
        T t1 ;
        T t2 ;
        t1 -= t2 ;
    }

    // Test call to operator +=  with other ABC
    {
        T t1 ;
        T t2 ;
        t1 += t2 ;
    }

    // Test call to operator *  with other ABC
    {
        T t1 ;
        T t2 ;
        T t3(t1 * t2) ;
    }

    // Test call to operator + with other ABC
    {
        T t1 ;
        T t2 ;
        T t3(t1 + t2) ;
    }

    // Test call to operator - with other ABC
    {
        T t1 ;
        T t2 ;
        T t3(t1 - t2) ;
    }

    // Test call to operator - void
    {
        T t1 ;
        T t3( -t1) ;
    }

    // Test call to inverseRotate to apply inverse rotation to an Eigen::Vector3d
    {
        Eigen::Vector3d eVect1(1., 1., 2.) ;
        Eigen::Vector3d eVect2(1., 1., 2.) ;
        T t1 ;
        Eigen::Vector3d res(t1.inverseRotate(eVect1)) ;
        BV::Tools::Tests::CheckEigenVectors(eVect1, eVect2) ;
        BV::Tools::Tests::CheckEigenVectors(res, eVect2) ;
    }
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Rotation_ABC_ABC_hpp__
