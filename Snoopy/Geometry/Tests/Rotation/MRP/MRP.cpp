#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/MRP/MRP.hpp"
#include "Tests/Rotation/MRP/Tools.hpp"
#include "Tests/Rotation/Quaternion/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

using BV::Geometry::Rotation::Quaternion ;

namespace BV {
namespace Geometry {
namespace Tests {


void MRP_Constructors(void)
{
    using Geometry::Rotation::MRP ;
    MRP mrp ;
    BV::Tools::Tests::CheckEigenVectors(mrp.unknowns(), Eigen::Vector3d::Zero()) ;
    MRP mrp2(0.1, 0.2, 0.3) ;
    BV::Tools::Tests::CheckEigenVectors(mrp2.unknowns(), Eigen::Vector3d(0.1, 0.2, 0.3)) ;
    MRP mrp3(mrp2) ;
    BV::Tools::Tests::CheckEigenVectors(mrp2.unknowns(), mrp3.unknowns()) ;
    Eigen::Vector3d u(1., 2., 3.) ;
    MRP mrp4(u(0), u(1), u(2)) ;
    BV::Tools::Tests::CheckEigenVectors(mrp4.unknowns(), -u/std::pow(u.norm(), 2)) ;
    Eigen::Matrix3d R(Details::GetRXMatrix(20.*M_PI/180.) * Details::GetRXMatrix(40.*M_PI/180.)) ;
    MRP mrp5(R) ;
    BV::Tools::Tests::CheckEigenMatrix(mrp5.getMatrix(), R) ;
}

void MRP_Methods(void)
{
    using Geometry::Rotation::MRP ;
    using BV::Details::epsilon ;

    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<MRP>() ;
    RotationABC_Methods<MRP>() ;

    Details::CheckTypedCallsToOverLoads<MRP>() ;

    // Check some matrix constructions with a set of angles values
    std::vector<Eigen::Matrix3d> allTestMatrices(Details::GetTestEigenRotationMatrices()) ;
    for (unsigned iR = 0 ; iR < allTestMatrices.size()-1; ++iR )
    {
        Eigen::Matrix3d eR(allTestMatrices[iR]) ;
        MRP MRPR(eR) ;
        BV::Tools::Tests::CheckEigenMatrix(MRPR.getMatrix(), eR) ;
        MRP MRPR1(MRPR) ;
        BV::Tools::Tests::CheckEigenMatrix(MRPR1.getMatrix(), eR) ;
        MRPR1.addOtherRotationAtLeft(MRPR) ;
        Eigen::Matrix3d R2(eR*eR) ;
        BV::Tools::Tests::CheckEigenMatrix(MRPR1.getMatrix(), R2) ;
        Details::ABCCheck<MRP>(MRPR, eR, 3, 0) ;

        Eigen::Matrix3d eR2(allTestMatrices[iR+1]) ;
        MRP MRPR2(eR2) ;
        Details::ABCCheck_CompRot<MRP>(MRPR, eR, MRPR2, eR2) ;
    }

}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
