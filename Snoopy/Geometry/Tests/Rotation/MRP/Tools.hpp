#pragma once
#ifndef __BV_Geometry_Tests_Rotation_MRP_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_MRP_Tools_hpp__

#include "Tests/Rotation/ABC/Tools.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckMRPCoordinates( const double & sigma0_1, const double & sigma0_2, const double & sigma0_3,
                          const double & sigma1_1, const double & sigma1_2, const double & sigma1_3) ;

void CheckMRPCoordinates( Geometry::Rotation::MRP & sigma0,
                          Geometry::Rotation::MRP & sigma1 ) ;

void CheckMRPCoordinates( const Eigen::Vector3d & sigma0, const Eigen::Vector3d & sigma1 ) ;

Eigen::Vector4d GetQuaternionFromR(const Eigen::Matrix3d & R) ;

template <>
void CheckUnknowns_<Rotation::MRP>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_MRP_Tools_hpp__
