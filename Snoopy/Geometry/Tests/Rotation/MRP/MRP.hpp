#pragma once
#ifndef __BV_Geometry_Tests_Rotation_MRP_MRP_hpp__
#define __BV_Geometry_Tests_Rotation_MRP_MRP_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void MRP_Constructors( void ) ;
void MRP_Methods( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_MRP_MRP_hpp__
