#include "Tests/Rotation/Quaternion/Tools.hpp"

#include <boost/test/unit_test.hpp>

#include "Tools/Tests/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckMRPCoordinates( const double & sigma0_1, const double & sigma0_2, const double & sigma0_3,
                          const double & sigma1_1, const double & sigma1_2, const double & sigma1_3)
{
    using BV::Details::epsilon ;
    bool equalityCheck( (std::abs(sigma0_1-sigma1_1) < epsilon)
                        && (std::abs(sigma0_2-sigma1_2) < epsilon)
                        && (std::abs(sigma0_3-sigma1_3) < epsilon)
                      ) ;

    bool s0_MRP1_EqualityCheck(false) ;
    bool MRP0_s1_EqualityCheck(false) ;
    bool s0_s1_EqualityCheck(false) ;

    double sigma0_norm2(std::pow(sigma0_1,2)+std::pow(sigma0_2,2)+std::pow(sigma0_3,2)) ;
    double sigma1_norm2(std::pow(sigma1_1,2)+std::pow(sigma1_2,2)+std::pow(sigma1_3,2)) ;

    // Compute the shadow representations if they exist
    if(sigma0_norm2>epsilon)
    {
        double shadow0_1 = -sigma0_1/sigma0_norm2 ;
        double shadow0_2 = -sigma0_2/sigma0_norm2 ;
        double shadow0_3 = -sigma0_3/sigma0_norm2 ;

        s0_MRP1_EqualityCheck = (std::abs(shadow0_1-sigma1_1) < epsilon)
                                && (std::abs(shadow0_2-sigma1_2) < epsilon)
                                && (std::abs(shadow0_3-sigma1_3) < epsilon) ;

        if(sigma1_norm2>epsilon)
        {
            double shadow1_1 = -sigma1_1/sigma1_norm2 ;
            double shadow1_2 = -sigma1_2/sigma1_norm2 ;
            double shadow1_3 = -sigma1_3/sigma1_norm2 ;

            MRP0_s1_EqualityCheck = (std::abs(sigma0_1-shadow1_1) < epsilon)
                                    && (std::abs(sigma0_2-shadow1_2) < epsilon)
                                    && (std::abs(sigma0_3-shadow1_3) < epsilon) ;

            s0_s1_EqualityCheck = (std::abs(shadow0_1-shadow1_1) < epsilon)
                                           && (std::abs(shadow0_2-shadow1_2) < epsilon)
                                           && (std::abs(shadow0_3-shadow1_3) < epsilon) ;
        }
    }
    if(sigma1_norm2>epsilon)
    {
        double shadow1_1 = -sigma1_1/sigma1_norm2 ;
        double shadow1_2 = -sigma1_2/sigma1_norm2 ;
        double shadow1_3 = -sigma1_3/sigma1_norm2 ;

        MRP0_s1_EqualityCheck = (std::abs(sigma0_1-shadow1_1) < epsilon)
                                && (std::abs(sigma0_2-shadow1_2) < epsilon)
                                && (std::abs(sigma0_3-shadow1_3) < epsilon) ;
    }

    bool MRPCheck(equalityCheck || s0_MRP1_EqualityCheck || MRP0_s1_EqualityCheck || s0_s1_EqualityCheck) ;
    BOOST_CHECK( MRPCheck ) ;

    if(!MRPCheck)
    {
        std::cout << "sigma0_1 = " << sigma0_1 << "    |    " << "sigma1_1 = " << sigma1_1 << std::endl ;
        std::cout << "sigma0_2 = " << sigma0_2 << "    |    " << "sigma1_2 = " << sigma1_2 << std::endl ;
        std::cout << "sigma0_3 = " << sigma0_3 << "    |    " << "sigma1_3 = " << sigma1_3 << std::endl << std::endl ;
    }
}

void CheckMRPCoordinates( Geometry::Rotation::MRP & sigma0,
                          Geometry::Rotation::MRP & sigma1 )
{
    CheckMRPCoordinates(sigma0.sigma1(), sigma0.sigma2(), sigma0.sigma3(),
                               sigma1.sigma1(), sigma1.sigma2(), sigma1.sigma3()) ;
    Eigen::VectorXd u1(sigma0.unknowns()) ;
    Eigen::VectorXd u2(sigma1.unknowns()) ;
    BV::Tools::Tests::CheckEigenVectors(u1, u2) ;
    Eigen::Vector3d xyz1(sigma0.sigma1(), sigma0.sigma2(), sigma0.sigma3()) ;
    BV::Tools::Tests::CheckEigenVectors(xyz1, u1) ;
    BV::Tools::Tests::CheckEigenVectors(xyz1, u2) ;
}

void CheckMRPCoordinates( const Eigen::Vector3d & sigma0, const Eigen::Vector3d & sigma1 )
{
    CheckMRPCoordinates(sigma0(0), sigma0(1), sigma0(2),
                        sigma1(0), sigma1(1), sigma1(2)) ;
}

Eigen::Vector3d GetMRPFromQ(const Eigen::Vector4d & q)
{
    Eigen::Vector3d sigma(Eigen::Vector3d::Zero()) ;
    double w(q(0)) ;
    double x(q(1)) ;
    double y(q(2)) ;
    double z(q(3)) ;
    double factor(1.) ;
    if (w < 0.)
    {
        factor = -1. ;
    }
    double den(1 + factor * w) ;
    sigma(0) = (factor * x/den) ;
    sigma(1) = (factor * y/den) ;
    sigma(2) = (factor * z/den) ;
    return sigma ;
}

template <>
void CheckUnknowns_<Rotation::MRP>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    Eigen::VectorXd unknowns(abc.unknowns()) ;
    Eigen::Vector4d q(GetQuaternionFromR(R)) ;
    Eigen::Vector3d expectedUnknowns(GetMRPFromQ(q)) ;
    Details::CheckMRPCoordinates(unknowns, expectedUnknowns) ;
    BV::Tools::Tests::CheckEigenMatrix(abc.getMatrix(), R) ;
}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
