#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/ABC/Tools.hpp"
#include "Tests/Rotation/EulerAngles/EulerAngles.hpp"
#include "Tests/Rotation/EulerAngles/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/Exceptions.hpp"

using BV::Geometry::Rotation::EulerAngles ;

namespace BV {
namespace Geometry {
namespace Tests {


void EulerAngles_Constructors(void)
{
    using Geometry::Rotation::AxisConvention ;
    using Geometry::Rotation::OrderConvention ;
    using Geometry::Rotation::EulerAngles_XYX_i ;
    using Geometry::Rotation::EulerAngles_XYZ_i ;
    using Geometry::Rotation::EulerAngles_XZX_i ;
    using Geometry::Rotation::EulerAngles_XZY_i ;
    using Geometry::Rotation::EulerAngles_YXY_i ;
    using Geometry::Rotation::EulerAngles_YXZ_i ;
    using Geometry::Rotation::EulerAngles_YZX_i ;
    using Geometry::Rotation::EulerAngles_YZY_i ;
    using Geometry::Rotation::EulerAngles_ZXY_i ;
    using Geometry::Rotation::EulerAngles_ZXZ_i ;
    using Geometry::Rotation::EulerAngles_ZYX_i ;
    using Geometry::Rotation::EulerAngles_ZYZ_i ;

    using Geometry::Rotation::EulerAngles_XYX_e ;
    using Geometry::Rotation::EulerAngles_XYZ_e ;
    using Geometry::Rotation::EulerAngles_XZX_e ;
    using Geometry::Rotation::EulerAngles_XZY_e ;
    using Geometry::Rotation::EulerAngles_YXY_e ;
    using Geometry::Rotation::EulerAngles_YXZ_e ;
    using Geometry::Rotation::EulerAngles_YZX_e ;
    using Geometry::Rotation::EulerAngles_YZY_e ;
    using Geometry::Rotation::EulerAngles_ZXY_e ;
    using Geometry::Rotation::EulerAngles_ZXZ_e ;
    using Geometry::Rotation::EulerAngles_ZYX_e ;
    using Geometry::Rotation::EulerAngles_ZYZ_e ;

    {
        Details::EulerAngles_Constructors_<EulerAngles_XYX_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_XYZ_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_XZX_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_XZY_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YXY_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YXZ_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YZX_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YZY_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZXY_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZXZ_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZYX_i>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZYZ_i>() ;

        Details::EulerAngles_Constructors_<EulerAngles_XYX_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_XYZ_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_XZX_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_XZY_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YXY_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YXZ_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YZX_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_YZY_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZXY_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZXZ_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZYX_e>() ;
        Details::EulerAngles_Constructors_<EulerAngles_ZYZ_e>() ;
    }
    {
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::X_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XXX_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::X_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XXY_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::X_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XXZ_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YYX_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YYY_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YYZ_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZZX_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZZY_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZZZ_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::X_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YXX_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::X_AXIS, AxisConvention::X_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZXX_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XYY_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_ZYY_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_XZZ_i ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, OrderConvention::INTRINSIC> EulerAnglesConvention_YZZ_i ;

        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::X_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XXX_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::X_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XXY_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::X_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XXZ_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YYX_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YYY_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YYZ_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZZX_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZZY_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZZZ_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::X_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YXX_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::X_AXIS, AxisConvention::X_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZXX_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XYY_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Z_AXIS, AxisConvention::Y_AXIS, AxisConvention::Y_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_ZYY_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::X_AXIS, AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_XZZ_e ;
        typedef BV::Geometry::Rotation::EulerAnglesConvention<AxisConvention::Y_AXIS, AxisConvention::Z_AXIS, AxisConvention::Z_AXIS, OrderConvention::EXTRINSIC> EulerAnglesConvention_YZZ_e ;

        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XXX_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XXY_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XXZ_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YYX_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YYY_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YYZ_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZZX_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZZY_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZZZ_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YXX_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZXX_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XYY_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZYY_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XZZ_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YZZ_i ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;

        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XXX_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XXY_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XXZ_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YYX_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YYY_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YYZ_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZZX_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZZY_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZZZ_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleUVConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YXX_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZXX_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XYY_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_ZYY_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_XZZ_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;
        BOOST_CHECK_EXCEPTION(EulerAnglesConvention_YZZ_e ea_conv, BV::Geometry::Exceptions::RotationInitialisationException, Details::WrongEulerAngleVWConventionChecker_ ) ;

        using Geometry::Rotation::EulerAnglesConvention_XYX_i ;
        using Geometry::Rotation::EulerAnglesConvention_XYZ_i ;
        using Geometry::Rotation::EulerAnglesConvention_XZX_i ;
        using Geometry::Rotation::EulerAnglesConvention_XZY_i ;
        using Geometry::Rotation::EulerAnglesConvention_YXY_i ;
        using Geometry::Rotation::EulerAnglesConvention_YXZ_i ;
        using Geometry::Rotation::EulerAnglesConvention_YZX_i ;
        using Geometry::Rotation::EulerAnglesConvention_YZY_i ;
        using Geometry::Rotation::EulerAnglesConvention_ZXY_i ;
        using Geometry::Rotation::EulerAnglesConvention_ZXZ_i ;
        using Geometry::Rotation::EulerAnglesConvention_ZYX_i ;
        using Geometry::Rotation::EulerAnglesConvention_ZYZ_i ;

        using Geometry::Rotation::EulerAnglesConvention_XYX_e ;
        using Geometry::Rotation::EulerAnglesConvention_XYZ_e ;
        using Geometry::Rotation::EulerAnglesConvention_XZX_e ;
        using Geometry::Rotation::EulerAnglesConvention_XZY_e ;
        using Geometry::Rotation::EulerAnglesConvention_YXY_e ;
        using Geometry::Rotation::EulerAnglesConvention_YXZ_e ;
        using Geometry::Rotation::EulerAnglesConvention_YZX_e ;
        using Geometry::Rotation::EulerAnglesConvention_YZY_e ;
        using Geometry::Rotation::EulerAnglesConvention_ZXY_e ;
        using Geometry::Rotation::EulerAnglesConvention_ZXZ_e ;
        using Geometry::Rotation::EulerAnglesConvention_ZYX_e ;
        using Geometry::Rotation::EulerAnglesConvention_ZYZ_e ;

        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XYX_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XYZ_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XZX_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XZY_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YXY_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YXZ_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YZX_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YZY_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZXY_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZXZ_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZYX_i ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZYZ_i ea_conv) ;

        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XYX_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XYZ_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XZX_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_XZY_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YXY_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YXZ_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YZX_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_YZY_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZXY_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZXZ_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZYX_e ea_conv) ;
        BOOST_CHECK_NO_THROW(EulerAnglesConvention_ZYZ_e ea_conv) ;



    }

}

void EulerAngles_Methods(void)
{

    using Geometry::Rotation::AxisConvention ;
    using BV::Details::epsilon ;
    using Geometry::Rotation::EulerAngles_XYX_i ;
    using Geometry::Rotation::EulerAngles_XYZ_i ;
    using Geometry::Rotation::EulerAngles_XZX_i ;
    using Geometry::Rotation::EulerAngles_XZY_i ;
    using Geometry::Rotation::EulerAngles_YXY_i ;
    using Geometry::Rotation::EulerAngles_YXZ_i ;
    using Geometry::Rotation::EulerAngles_YZX_i ;
    using Geometry::Rotation::EulerAngles_YZY_i ;
    using Geometry::Rotation::EulerAngles_ZXY_i ;
    using Geometry::Rotation::EulerAngles_ZXZ_i ;
    using Geometry::Rotation::EulerAngles_ZYX_i ;
    using Geometry::Rotation::EulerAngles_ZYZ_i ;

    using Geometry::Rotation::EulerAngles_XYX_e ;
    using Geometry::Rotation::EulerAngles_XYZ_e ;
    using Geometry::Rotation::EulerAngles_XZX_e ;
    using Geometry::Rotation::EulerAngles_XZY_e ;
    using Geometry::Rotation::EulerAngles_YXY_e ;
    using Geometry::Rotation::EulerAngles_YXZ_e ;
    using Geometry::Rotation::EulerAngles_YZX_e ;
    using Geometry::Rotation::EulerAngles_YZY_e ;
    using Geometry::Rotation::EulerAngles_ZXY_e ;
    using Geometry::Rotation::EulerAngles_ZXZ_e ;
    using Geometry::Rotation::EulerAngles_ZYX_e ;
    using Geometry::Rotation::EulerAngles_ZYZ_e ;

    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<EulerAngles_XYX_i>() ;
    ABC_Methods<EulerAngles_XYZ_i>() ;
    ABC_Methods<EulerAngles_XZX_i>() ;
    ABC_Methods<EulerAngles_XZY_i>() ;
    ABC_Methods<EulerAngles_YXY_i>() ;
    ABC_Methods<EulerAngles_YXZ_i>() ;
    ABC_Methods<EulerAngles_YZX_i>() ;
    ABC_Methods<EulerAngles_YZY_i>() ;
    ABC_Methods<EulerAngles_ZXY_i>() ;
    ABC_Methods<EulerAngles_ZXZ_i>() ;
    ABC_Methods<EulerAngles_ZYX_i>() ;
    ABC_Methods<EulerAngles_ZYZ_i>() ;

    ABC_Methods<EulerAngles_XYX_e>() ;
    ABC_Methods<EulerAngles_XYZ_e>() ;
    ABC_Methods<EulerAngles_XZX_e>() ;
    ABC_Methods<EulerAngles_XZY_e>() ;
    ABC_Methods<EulerAngles_YXY_e>() ;
    ABC_Methods<EulerAngles_YXZ_e>() ;
    ABC_Methods<EulerAngles_YZX_e>() ;
    ABC_Methods<EulerAngles_YZY_e>() ;
    ABC_Methods<EulerAngles_ZXY_e>() ;
    ABC_Methods<EulerAngles_ZXZ_e>() ;
    ABC_Methods<EulerAngles_ZYX_e>() ;
    ABC_Methods<EulerAngles_ZYZ_e>() ;

    RotationABC_Methods<EulerAngles_XYX_i>() ;
    RotationABC_Methods<EulerAngles_XYZ_i>() ;
    RotationABC_Methods<EulerAngles_XZX_i>() ;
    RotationABC_Methods<EulerAngles_XZY_i>() ;
    RotationABC_Methods<EulerAngles_YXY_i>() ;
    RotationABC_Methods<EulerAngles_YXZ_i>() ;
    RotationABC_Methods<EulerAngles_YZX_i>() ;
    RotationABC_Methods<EulerAngles_YZY_i>() ;
    RotationABC_Methods<EulerAngles_ZXY_i>() ;
    RotationABC_Methods<EulerAngles_ZXZ_i>() ;
    RotationABC_Methods<EulerAngles_ZYX_i>() ;
    RotationABC_Methods<EulerAngles_ZYZ_i>() ;

    RotationABC_Methods<EulerAngles_XYX_e>() ;
    RotationABC_Methods<EulerAngles_XYZ_e>() ;
    RotationABC_Methods<EulerAngles_XZX_e>() ;
    RotationABC_Methods<EulerAngles_XZY_e>() ;
    RotationABC_Methods<EulerAngles_YXY_e>() ;
    RotationABC_Methods<EulerAngles_YXZ_e>() ;
    RotationABC_Methods<EulerAngles_YZX_e>() ;
    RotationABC_Methods<EulerAngles_YZY_e>() ;
    RotationABC_Methods<EulerAngles_ZXY_e>() ;
    RotationABC_Methods<EulerAngles_ZXZ_e>() ;
    RotationABC_Methods<EulerAngles_ZYX_e>() ;
    RotationABC_Methods<EulerAngles_ZYZ_e>() ;

    Details::CheckTypedCallsToOverLoads<EulerAngles_XYX_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_XYZ_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_XZX_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_XZY_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YXY_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YXZ_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YZX_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YZY_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZXY_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZXZ_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZYX_i>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZYZ_i>() ;

    Details::CheckTypedCallsToOverLoads<EulerAngles_XYX_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_XYZ_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_XZX_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_XZY_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YXY_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YXZ_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YZX_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_YZY_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZXY_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZXZ_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZYX_e>() ;
    Details::CheckTypedCallsToOverLoads<EulerAngles_ZYZ_e>() ;

    // Check some matrix constructions with a set of angles values
    std::vector<Eigen::Matrix3d> allTestMatrices(Details::GetTestEigenRotationMatrices()) ;
    for (unsigned iR = 0 ; iR < allTestMatrices.size()-1; ++iR )
    {

        Details::ABCCheck_EulerType<EulerAngles_XYX_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_XYZ_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_XZX_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_XZY_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YXY_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YXZ_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YZX_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YZY_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZXY_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZXZ_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZYX_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZYZ_i>( allTestMatrices[iR], allTestMatrices[iR+1]) ;

        Details::ABCCheck_EulerType<EulerAngles_XYX_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_XYZ_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_XZX_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_XZY_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YXY_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YXZ_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YZX_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_YZY_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZXY_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZXZ_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZYX_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
        Details::ABCCheck_EulerType<EulerAngles_ZYZ_e>( allTestMatrices[iR], allTestMatrices[iR+1]) ;
    }

    Details::CheckEulerSpecificImplemantation<EulerAngles_XYX_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_XYZ_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_XZX_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_XZY_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YXY_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YXZ_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YZX_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YZY_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZXY_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZXZ_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZYX_i>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZYZ_i>() ;

    Details::CheckEulerSpecificImplemantation<EulerAngles_XYX_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_XYZ_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_XZX_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_XZY_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YXY_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YXZ_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YZX_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_YZY_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZXY_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZXZ_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZYX_e>() ;
    Details::CheckEulerSpecificImplemantation<EulerAngles_ZYZ_e>() ;


}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
