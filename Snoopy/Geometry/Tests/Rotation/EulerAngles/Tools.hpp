#pragma once
#ifndef __BV_Geometry_Tests_Rotation_EulerAngles_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_EulerAngles_Tools_hpp__

#include "Tests/Rotation/ABC/Tools.hpp"
#include "Geometry/Rotation/EulerAngles.hpp"
#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"
#include "Geometry/Exceptions.hpp"

#include "Tools/Tests/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;
template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

std::vector<BV::Geometry::RotatorTypeEnum> GetEulerEnum(void) ;

template <typename EulerType>
void EulerAngles_Constructors_(void)
{
    using BV::Details::epsilon ;
    using Geometry::Rotation::AxisConvention ;
    using Geometry::Rotation::OrderConvention ;

    {
        EulerType ea ;
        Eigen::Vector3d comp(0., 0., 0.) ;
        BV::Tools::Tests::CheckEigenVectors(comp, ea.unknowns()) ;
        BOOST_CHECK_SMALL(ea.alpha(), epsilon) ;
        BOOST_CHECK_SMALL(ea.beta(), epsilon) ;
        BOOST_CHECK_SMALL(ea.gamma(), epsilon) ;
    }

    {
        EulerType ea ;
        EulerType ea2(0., 0., 0.) ;
        BV::Tools::Tests::CheckEigenVectors(ea2.unknowns(), ea.unknowns()) ;
        BOOST_CHECK_SMALL(ea2.alpha(), epsilon) ;
        BOOST_CHECK_SMALL(ea2.beta(), epsilon) ;
        BOOST_CHECK_SMALL(ea2.gamma(), epsilon) ;
    }

    {
        EulerType ea(10.*M_PI/180., 20.*M_PI/180., -15.*M_PI/180.) ;
        EulerType ea3(ea) ;
        BV::Tools::Tests::CheckEigenVectors(ea3.unknowns(), ea.unknowns()) ;
        BOOST_CHECK(ea3.getConvention().isEqualTo(ea.getConvention())) ;
    }

    {
        Eigen::Vector3d comp(0., 0., 0.) ;
        comp(0) = 0.5*M_PI;
        comp(1) = 0.4*M_PI;
        comp(2) = 0.33*M_PI;
        EulerType ea4(comp(0), comp(1), comp(2)) ;
        BV::Tools::Tests::CheckEigenVectors(ea4.unknowns(), comp) ;
        BOOST_CHECK_SMALL(ea4.alpha()-comp(0), epsilon) ;
        BOOST_CHECK_SMALL(ea4.beta()-comp(1), epsilon) ;
        BOOST_CHECK_SMALL(ea4.gamma()-comp(2), epsilon) ;

        EulerType ea5(ea4.unknowns()(0), ea4.unknowns()(1), ea4.unknowns()(2)) ;
        BV::Tools::Tests::CheckEigenVectors(ea5.unknowns(), comp) ;
        BOOST_CHECK_SMALL(ea5.alpha()-comp(0), epsilon) ;
        BOOST_CHECK_SMALL(ea5.beta()-comp(1), epsilon) ;
        BOOST_CHECK_SMALL(ea5.gamma()-comp(2), epsilon) ;
        EulerType ea6(ea5) ;
        BV::Tools::Tests::CheckEigenVectors(ea6.unknowns(), ea5.unknowns()) ;
        BOOST_CHECK_SMALL(ea6.alpha()-comp(0), epsilon) ;
        BOOST_CHECK_SMALL(ea6.beta()-comp(1), epsilon) ;
        BOOST_CHECK_SMALL(ea6.gamma()-comp(2), epsilon) ;
    }

}


template <typename EulerType>
void CheckEulerSpecificImplemantation( void )
{
    using BV::Details::epsilon ;

    {
        double inputAngle(20.*M_PI/180.) ;
        Eigen::Matrix3d R(Details::GetRZMatrix(inputAngle)) ;
        EulerType euler(R) ;
        double alpha(euler.alpha()) ;
        double beta(euler.beta()) ;
        double gamma(euler.gamma()) ;

        alpha += 0.1 ;
        beta += 0.2 ;
        gamma += 0.3 ;

        BOOST_CHECK_SMALL(euler.alpha() - (alpha - 0.1), epsilon) ;
        BOOST_CHECK_SMALL(euler.beta() - (beta - 0.2), epsilon) ;
        BOOST_CHECK_SMALL(euler.gamma() - (gamma - 0.3), epsilon) ;
    }

    // Check arguments getters by ref or copy
    {
        double inputAngle(20.*M_PI/180.) ;
        Eigen::Matrix3d R(Details::GetRZMatrix(inputAngle)) ;
        EulerType euler(R) ;
        double & alpha(euler.alpha()) ;
        double & beta(euler.beta()) ;
        double & gamma(euler.gamma()) ;

        alpha += 0.1 ;
        beta += 0.2 ;
        gamma += 0.3 ;

        BOOST_CHECK_SMALL(euler.alpha() - alpha, epsilon) ;
        BOOST_CHECK_SMALL(euler.beta() - beta, epsilon) ;
        BOOST_CHECK_SMALL(euler.gamma() - gamma, epsilon) ;
    }

    // Check singularities
    /// No singularity for assymetric/symetric convention
    {
        double alpha(20.*M_PI/180.) ;
        double beta(20.*M_PI/180.) ;
        double gamma(20.*M_PI/180.) ;
        EulerType euler(alpha, beta, gamma) ;
        BOOST_CHECK(!euler.isSingular()) ;
    }

    // Singularity for assymetric convention
    {
        double alpha(20.*M_PI/180.) ;
        double beta(90.*M_PI/180.) ;
        double gamma(20.*M_PI/180.) ;
        EulerType euler(alpha, beta, gamma) ;
        if(euler.getConvention().getU() != euler.getConvention().getW())
        {
            BOOST_CHECK(euler.isSingular()) ;
        }
        euler.beta() = -90.*M_PI/180. ;
        if(euler.getConvention().getU() != euler.getConvention().getW())
        {
            BOOST_CHECK(euler.isSingular()) ;
        }
    }

    // Singularity for symetric convention
    {
        double alpha(20.*M_PI/180.) ;
        double beta(180.*M_PI/180.) ;
        double gamma(20.*M_PI/180.) ;
        EulerType euler(alpha, beta, gamma) ;
        if(euler.getConvention().getU() == euler.getConvention().getW())
        {
            BOOST_CHECK(euler.isSingular()) ;
        }
        euler.beta() = 180.*M_PI/180. ;
        if(euler.getConvention().getU() == euler.getConvention().getW())
        {
            BOOST_CHECK(euler.isSingular()) ;
        }

        euler.beta() = 0. ;
        if(euler.getConvention().getU() == euler.getConvention().getW())
        {
            BOOST_CHECK(euler.isSingular()) ;
        }
    }
}

template <typename EulerType>
void ABCCheck_EulerType(const Eigen::Matrix3d & iR, const Eigen::Matrix3d & iRp1)
{
    {
        EulerType eulerR(iR) ;
        Details::ABCCheck<EulerType>(eulerR, iR, 3, 0) ;
        EulerType eulerR2(iRp1) ;
        Details::ABCCheck_CompRot<EulerType>(eulerR, iR, eulerR2, iRp1) ;
    }
}

template <typename EulerConv>
bool SingularityDetector(const double & angle0, const double & angle1, const double & angle2)
{
    // This function detects if a gimbal lock occurs for a specific Euler angle convention
    // Note that the checks are valid for both intrinsic and extrinsic convention.
    // For an Euler angle convention U-V-W, the rotation matrix for intrinsic and extrinsic are the following
    // intrinsic: R = RU(theta_i) * RV(theta_j) * RW(theta_k)
    // extrinsic: R = RW(theta_k) * RV(theta_j) * RU(theta_i)
    using BV::Details::epsilon ;
    EulerConv convention ;
    double angle1_MinusPi_Pi(AngleMinusPi_Pi(angle1)) ;

    // If assymetric convention (i.e. in U-V-W, U and W are different)
    // then the singularity (gimbal lock) occurs when theta_j = +-90�
    if(convention.getU() != convention.getW())
    {
        return std::abs(std::abs(angle1_MinusPi_Pi) - M_PI/2.) < epsilon ;
    }
    // If assymetric convention (i.e. in U-V-W, U and W are different)
    // then the singularity (gimbal lock) occurs when theta_j = 0�,+-180�
    else if(convention.getU() == convention.getW())
    {
        return ( (std::abs(std::abs(angle1_MinusPi_Pi) - M_PI) < epsilon) || (std::abs(angle1_MinusPi_Pi) < epsilon) ) ;
    }
    else
    {
        throw "The convention passed to detect the singularity in Euler angles "
                "does not represent an Euler Angle valid convention." ;
    }
}

Eigen::Vector3d unknownsMinusPi_Pi(const Eigen::Vector3d & unknowns) ;

Eigen::Vector3d unknowns0_2Pi(const Eigen::Vector3d & unknowns) ;



template <typename EulerConv>
void CheckCoordinates(double angle00, double angle01, double angle02,
                      double angle10, double angle11, double angle12)
{
    using BV::Details::epsilon ;
    bool is1Singular(SingularityDetector<EulerConv>(angle00, angle01, angle02)) ;
    bool is2Singular(SingularityDetector<EulerConv>(angle10, angle11, angle12)) ;


    bool theta_i1_NearPi( ( std::abs( std::abs(angle00) - M_PI ) < epsilon ) ||  ( ( std::abs(angle00) < epsilon ) ) ) ;
    bool theta_i1_NearHalfPi( ( std::abs( std::abs(angle00) - M_PI/2. ) < epsilon ) ) ;

    bool theta_i2_NearPi( ( std::abs( std::abs(angle10) - M_PI ) < epsilon ) ||  ( ( std::abs(angle10) < epsilon ) ) ) ;
    bool theta_i2_NearHalfPi( ( std::abs( std::abs(angle10) - M_PI/2. ) < epsilon ) ) ;

    bool theta_k1_NearPi( ( std::abs( std::abs(angle02) - M_PI ) < epsilon ) ||  ( ( std::abs(angle02) < epsilon ) ) ) ;
    bool theta_k1_NearHalfPi( ( std::abs( std::abs(angle02) - M_PI/2. ) < epsilon ) ) ;

    bool theta_k2_NearPi( ( std::abs( std::abs(angle12) - M_PI ) < epsilon ) ||  ( ( std::abs(angle12) < epsilon ) ) ) ;
    bool theta_k2_NearHalfPi( ( std::abs( std::abs(angle12) - M_PI/2. ) < epsilon ) ) ;

    bool specialCase1( (theta_i1_NearPi && theta_k1_NearPi) || (theta_i2_NearPi && theta_k2_NearPi) ) ;
    bool specialCase2( (theta_i1_NearHalfPi && theta_k1_NearHalfPi) || (theta_i2_NearHalfPi && theta_k2_NearHalfPi) ) ;
    bool specialCase3( (theta_i1_NearHalfPi && theta_k1_NearPi) || (theta_i2_NearHalfPi && theta_k2_NearPi) ) ;
    bool specialCase4( (theta_i1_NearPi && theta_k1_NearHalfPi) || (theta_i2_NearPi && theta_k2_NearHalfPi) ) ;

    // if singularity (gimbal lock)
    if(is1Singular || is2Singular)
    {
        bool checkGimbalLockSecondComponent( std::abs(std::abs(angle01)-std::abs(angle11))<epsilon) ;
        BOOST_CHECK(checkGimbalLockSecondComponent) ;

        // Check the sum/diff angles
        double sum1(AngleMinusPi_Pi(angle00 + angle02)) ;
        double sum2(AngleMinusPi_Pi(angle10 + angle12)) ;
        double diff1(AngleMinusPi_Pi(angle00 - angle02)) ;
        double diff2(AngleMinusPi_Pi(angle10 - angle12)) ;
        bool checkGimbalLockSum( ( std::abs( AngleMinusPi_Pi(sum1 - sum2) ) < epsilon) ||
                                 ( std::abs( AngleMinusPi_Pi(sum1 - diff2) ) < epsilon) ||
                                 ( std::abs( AngleMinusPi_Pi(diff1 - sum2) ) < epsilon) ||
                                 ( std::abs( AngleMinusPi_Pi(diff1 - diff2) ) < epsilon) ) ;
        BOOST_CHECK(checkGimbalLockSum) ;
    }
    // Special angles for ( theta_i = 0�,+-180� and theta_k = 0�,+-180�) or ( theta_i = +-90� and theta_k = +-90�)
    // Special angles for ( theta_i = +-90� and theta_k = 0�,+-180�) or ( theta_i = 0�,+-180� and theta_k = +-90�)
    else if(specialCase1 || specialCase2 || specialCase3 || specialCase4 )
    {
        double sum1(AngleMinusPi_Pi(angle00 + angle01 + angle02)) ;
        double sum2(AngleMinusPi_Pi(angle10 + angle11 + angle12)) ;
        bool specialCasesCheck( ( std::abs(AngleMinusPi_Pi(sum1 - sum2) ) < epsilon )
                                  || ( std::abs(AngleMinusPi_Pi(sum1 - sum2) ) - M_PI < epsilon )
                              ) ;
        BOOST_CHECK(specialCasesCheck) ;

    }
    else
    {
        bool equalityCheck(
                            (std::abs(angle00-angle10) < epsilon )
                            && (std::abs(angle01-angle11) < epsilon )
                            && (std::abs(angle02-angle12) < epsilon )
                          ) ;
        if(equalityCheck)
        {
            BOOST_CHECK(equalityCheck) ;
        }
        // it seems that there is a special case when the first angle theta_i (with R = RU(theta_i) * RV(theta_j) * RW(theta_k) )
        // is equal to M_PI, the unknowns can be different, but represents the same matrix
        // We check it !
        // Maybe there is a specific relation between the angles sum, but I don't know it...
        else if( ( (std::abs(angle00)-M_PI <epsilon) && (std::abs(angle10) <epsilon) )
                 || ( (std::abs(angle00) <epsilon) && (std::abs(angle10) - M_PI <epsilon) )
               )
        {
            Rotation::EulerAngles<EulerConv> e1(angle00, angle01, angle02) ;
            Rotation::EulerAngles<EulerConv> e2(angle10, angle11, angle12) ;
            BV::Tools::Tests::CheckEigenMatrix(e1.getMatrix(), e2.getMatrix()) ;
        }
        // All other cases we check by matrix
        else
        {
            Rotation::EulerAngles<EulerConv> e1(angle00, angle01, angle02) ;
            Rotation::EulerAngles<EulerConv> e2(angle10, angle11, angle12) ;
            BV::Tools::Tests::CheckEigenMatrix(e1.getMatrix(), e2.getMatrix()) ;
        }
    }
}

template <typename EulerConv>
void CheckCoordinates(Eigen::Vector3d angles0, Eigen::Vector3d angles1)
{
    CheckCoordinates<EulerConv>(angles0(0), angles0(1), angles0(2),
                     angles1(0), angles1(1), angles1(2)) ;
}

template <typename EulerConv>
void CheckEulerUnknowns_(Rotation::ABC & abc,
                         const Eigen::Matrix3d & R)
{
    BV::Geometry::Rotation::EulerAngles<EulerConv> tmp(R) ;
    Eigen::Vector3d unknowns(abc.unknowns()) ;
    Eigen::Vector3d tmpUnknowns(tmp.unknowns()) ;
    CheckCoordinates<EulerConv>(unknowns, tmpUnknowns) ;
    Eigen::VectorXd constraints(abc.constraints()) ;
    BV::Tools::Tests::CheckEigenVectors(constraints, tmp.constraints()) ;
}

bool WrongEulerAngleUVConventionChecker_(const BV::Geometry::Exceptions::RotationInitialisationException & e) ;

bool WrongEulerAngleVWConventionChecker_(const BV::Geometry::Exceptions::RotationInitialisationException & e) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Quaternion_Tools_hpp__
