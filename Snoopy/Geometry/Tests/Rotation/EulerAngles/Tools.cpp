#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/EulerAngles/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"
#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {
namespace Details
{

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XYX_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XYZ_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XZX_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XZY_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YXY_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YXZ_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YZX_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YZY_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXY_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZXY_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZXZ_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYX_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZYX_i>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYZ_i>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZYZ_i>(abc, R) ;
}






template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XYX_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XYZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XYZ_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XZX_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_XZY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_XZY_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YXY_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YXZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YXZ_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YZX_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_YZY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_YZY_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXY_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZXY_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZXZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZXZ_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYX_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZYX_e>(abc, R) ;
}

template <>
void CheckUnknowns_<BV::Geometry::Rotation::EulerAngles_ZYZ_e>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    CheckEulerUnknowns_<BV::Geometry::Rotation::EulerAnglesConvention_ZYZ_e>(abc, R) ;
}





Eigen::Vector3d unknownsMinusPi_Pi(const Eigen::Vector3d & unknowns)
{
    Eigen::Vector3d res ;
    for (unsigned i=0;i<3;++i)
    {
        res(i) = AngleMinusPi_Pi(unknowns(i)) ;
    }
    return res ;
}

Eigen::Vector3d unknowns0_2Pi(const Eigen::Vector3d & unknowns)
{
    Eigen::Vector3d res ;
    for (unsigned i=0;i<3;++i)
    {
        res(i) = Angle02PI(unknowns(i)) ;
    }
    return res ;
}

std::vector<BV::Geometry::RotatorTypeEnum> GetEulerEnum(void)
{
    using BV::Geometry::RotatorTypeEnum ;
    std::vector<RotatorTypeEnum> conventionsEnum ;

    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XYX_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XYZ_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XZX_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XZY_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YXY_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YXZ_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YZX_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YZY_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZXY_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZXZ_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZYX_i ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZYZ_i ) ;

    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XYX_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XYZ_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XZX_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_XZY_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YXY_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YXZ_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YZX_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_YZY_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZXY_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZXZ_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZYX_e ) ;
    conventionsEnum.push_back( RotatorTypeEnum::EULER_ANGLES_ZYZ_e ) ;

    return conventionsEnum ;
}

bool WrongEulerAngleUVConventionChecker_(const BV::Geometry::Exceptions::RotationInitialisationException & e)
{
    return e.getMessage() == "Not valid Euler Angle convention for axis U and V." ;
}
bool WrongEulerAngleVWConventionChecker_(const BV::Geometry::Exceptions::RotationInitialisationException & e)
{
    return e.getMessage() == "Not valid Euler Angle convention for axis V and W." ;
}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
