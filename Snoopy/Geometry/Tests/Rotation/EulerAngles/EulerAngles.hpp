#pragma once
#ifndef __BV_Geometry_Tests_Rotation_EulerAngles_EulerAngles_hpp__
#define __BV_Geometry_Tests_Rotation_EulerAngles_EulerAngles_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void EulerAngles_Constructors( void ) ;
void EulerAngles_Methods( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_EulerAngles_EulerAngles_hpp__
