#include "Tests/Rotation/Quaternion/Tools.hpp"

#include <boost/test/unit_test.hpp>

#include "Tools/Tests/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckQuaternionCoordinates( const double & q1w, const double & q1x,
                                 const double & q1y, const double & q1z,
                                 const double & q2w, const double & q2x,
                                 const double & q2y, const double & q2z )
{
    using BV::Details::epsilon ;

    bool dualityCheck( (
                          (std::abs(q1w-q2w) < epsilon)
                       && (std::abs(q1x-q2x) < epsilon)
                       && (std::abs(q1y-q2y) < epsilon)
                       && (std::abs(q1z-q2z) < epsilon)
                       )
                     || (
                          (std::abs(q1w+q2w) < epsilon)
                       && (std::abs(q1x+q2x) < epsilon)
                       && (std::abs(q1y+q2y) < epsilon)
                       && (std::abs(q1z+q2z) < epsilon)
                        )
                     ) ;
    // We have to check for dual too..
    BOOST_CHECK(dualityCheck) ;

    if(!dualityCheck)
    {
        std::cout << "q1w = " << q1w << "    |    " << "q2w = " << q2w << std::endl ;
        std::cout << "q1x = " << q1x << "    |    " << "q2x = " << q2x << std::endl ;
        std::cout << "q1y = " << q1y << "    |    " << "q2y = " << q2y << std::endl ;
        std::cout << "q1z = " << q1z << "    |    " << "q2z = " << q2z << std::endl << std::endl ;
    }
}

void CheckQuaternionCoordinates( Geometry::Rotation::Quaternion & q1,
                                 Geometry::Rotation::Quaternion & q2 )
{
    CheckQuaternionCoordinates(q1.w(), q1.x(), q1.y(), q1.z(),
                               q2.w(), q2.x(), q2.y(), q2.z()) ;
    Eigen::VectorXd u1(q1.unknowns()) ;
    Eigen::VectorXd u2(q2.unknowns()) ;
    BV::Tools::Tests::CheckEigenVectors(u1, u2) ;
    Eigen::Vector4d xyz1(q1.w(), q1.x(), q1.y(), q1.z()) ;
    BV::Tools::Tests::CheckEigenVectors(xyz1, u1) ;
    BV::Tools::Tests::CheckEigenVectors(xyz1, u2) ;
}

void CheckQuaternionCoordinates( const Geometry::Rotation::Quaternion & q,
                                 const double & w,
                                 const double & x, const double & y, const double & z )
{
    CheckQuaternionCoordinates(q.w(), q.x(), q.y(), q.z(),
                               w, x, y, z) ;
}

void CheckQuaternionCoordinates( const Eigen::VectorXd & q1, const Eigen::VectorXd & q2 )
{
    CheckQuaternionCoordinates(q1(0), q1(1), q1(2), q1(3),
                               q2(0), q2(1), q2(2), q2(3)) ;
}

Eigen::Vector4d GetQuaternionFromR(const Eigen::Matrix3d & R)
{
    Eigen::Vector4d res(Eigen::Vector4d::Zero()) ;
    if( R.trace() > 0. )
    {
        double t( sqrt( 1. + R.trace() ) );
        res(0) = t ;
        res(1) = (R(2,1) - R(1,2)) / t ;
        res(2) = (R(0,2) - R(2,0)) / t ;
        res(3) = (R(1,0) - R(0,1)) / t ;
    }
    else if( (R(0,0)>R(1,1)) && (R(0,0)>R(2,2)) )
    {
        double t( sqrt( 1. + R(0,0) - R(1,1) - R(2,2) ) );
        res(0) = (R(2,1) - R(1,2)) / t ;
        res(1) = t ;
        res(2) = (R(0,1) + R(1,0)) / t ;
        res(3) = (R(0,2) + R(2,0)) / t ;
    }
    else if( R(1,1)>R(2,2) )
    {
        double t( sqrt( 1. - R(0,0) + R(1,1) - R(2,2) ) );
        res(0) = (R(0,2) - R(2,0)) / t ;
        res(1) = (R(0,1) + R(1,0)) / t ;
        res(2) = t ;
        res(3) = (R(1,2) + R(2,1)) / t ;
    }
    else
    {
        double t( sqrt( 1. - R(0,0) - R(1,1) + R(2,2) ) );
        res(0) = (R(1,0) - R(0,1)) / t ;
        res(1) = (R(0,2) + R(2,0)) / t ;
        res(2) = (R(1,2) + R(2,1)) / t ;
        res(3) = t ;
    }
    res *= 0.5 ;
    return res ;
}

template <>
void CheckUnknowns_<Rotation::Quaternion>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    Eigen::VectorXd unknowns(abc.unknowns()) ;
    Eigen::VectorXd expectedUnknowns(GetQuaternionFromR(R)) ;
    Details::CheckQuaternionCoordinates(unknowns, expectedUnknowns) ;
}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
