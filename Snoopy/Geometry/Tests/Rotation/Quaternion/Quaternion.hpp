#pragma once
#ifndef __BV_Geometry_Tests_Rotation_Quaternion_Quaternion_hpp__
#define __BV_Geometry_Tests_Rotation_Quaternion_Quaternion_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void Quaternion_Constructors( void ) ;
void Quaternion_Methods( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Quaternion_Quaternion_hpp__
