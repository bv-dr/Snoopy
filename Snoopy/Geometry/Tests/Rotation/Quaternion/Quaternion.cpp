#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/Quaternion/Quaternion.hpp"
#include "Tests/Rotation/Quaternion/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

using BV::Geometry::Rotation::Quaternion ;

namespace BV {
namespace Geometry {
namespace Tests {


void Quaternion_Constructors(void)
{
    using Geometry::Vector ;
    using Geometry::Rotation::Quaternion ;
    using Geometry::Rotation::BasisVectors ;
    using BV::Details::epsilon ;

    // Test the constructors
    Quaternion quaternion1 ;
    Details::CheckQuaternionCoordinates( quaternion1, 1., 0., 0., 0. ) ;
    Quaternion quaternion2( M_PI, 1., 0., 0. ) ;
    Quaternion quaternion3( quaternion2 ) ;
    Eigen::VectorXd tmp(4) ;
    tmp(0) = M_PI ;
    tmp(1) = 1. ;
    tmp(2) = 0. ;
    tmp(3) = 0. ;
    Quaternion quaternion4(tmp) ;

    // Check the coordinates
    Details::CheckQuaternionCoordinates( quaternion2, quaternion3) ;
    // Make sure it is a copy
    quaternion3.unknowns(Eigen::Vector4d(2.*M_PI, 1., 0., 0.)) ;
    BOOST_CHECK_CLOSE(quaternion3.w()-quaternion2.w(), M_PI, epsilon) ;
    Details::CheckQuaternionCoordinates( quaternion2, quaternion4) ;
}

void Quaternion_Methods(void)
{
    using Geometry::Rotation::Quaternion ;
    using BV::Details::epsilon ;

    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<Quaternion>() ;
    RotationABC_Methods<Quaternion>() ;

    Details::CheckTypedCallsToOverLoads<Quaternion>() ;

    // Check some matrix constructions with a set of angles values
    std::vector<Eigen::Matrix3d> allTestMatrices(Details::GetTestEigenRotationMatrices()) ;
    for (unsigned iR = 0 ; iR < allTestMatrices.size()-1; ++iR )
    {
        Eigen::Matrix3d eR(allTestMatrices[iR]) ;
        Rotation::Quaternion qR(eR) ;
        Details::ABCCheck<Quaternion>(qR, eR, 4, 1) ;
        Eigen::Matrix3d eR2(allTestMatrices[iR+1]) ;
        Rotation::Quaternion qR2(eR2) ;
        Details::ABCCheck_CompRot<Rotation::Quaternion>(qR, eR, qR2, eR2) ;
    }

    // Test member method normalise
    {
        Quaternion q(1.2, 156., 123857., 156.) ;
        Quaternion q2(q.normalised()) ;
        BOOST_CHECK_CLOSE(q2.norm(), 1., epsilon) ;
        BOOST_CHECK(q.norm() > 1.) ;
        q.normalise() ;
        BOOST_CHECK_CLOSE(q.norm(), 1., epsilon) ;
    }

}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
