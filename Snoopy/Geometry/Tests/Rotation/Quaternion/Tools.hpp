#pragma once
#ifndef __BV_Geometry_Tests_Rotation_Quaternion_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_Quaternion_Tools_hpp__

#include "Tests/Rotation/ABC/Tools.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckQuaternionCoordinates( const double & q1w, const double & q1x,
                                 const double & q1y, const double & q1z,
                                 const double & q2w, const double & q2x,
                                 const double & q2y, const double & q2z ) ;

void CheckQuaternionCoordinates( Geometry::Rotation::Quaternion & q1,
                                 Geometry::Rotation::Quaternion & q2 ) ;

void CheckQuaternionCoordinates( const Geometry::Rotation::Quaternion & q,
                                 const double & w,
                                 const double & x, const double & y, const double & z ) ;

void CheckCoordinates( const Eigen::VectorXd & q1, const Eigen::VectorXd & q2 ) ;

Eigen::Vector4d GetQuaternionFromR(const Eigen::Matrix3d & R) ;

template <>
void CheckUnknowns_<Rotation::Quaternion>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Quaternion_Tools_hpp__
