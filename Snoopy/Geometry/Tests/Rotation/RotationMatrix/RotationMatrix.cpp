#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>
#include <iostream>

#include "Tools/Tests/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/ABC/Tools.hpp"

#include "Tests/Rotation/RotationMatrix/RotationMatrix.hpp"
#include "Tests/Rotation/RotationMatrix/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

#include "Geometry/Exceptions.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

using Geometry::Rotation::RotationMatrix ;
using BV::Geometry::Exceptions::RotationInitialisationException ;

void RotationMatrix_Constructors( void )
{
    // Check constructors
    {
        RotationMatrix R ;
        Details::CheckMatrixWithEigen_(R, Eigen::Matrix3d::Identity()) ;
        const double angle(20.*M_PI/180.) ;
        Eigen::Matrix3d rot(Details::GetRXMatrix(angle)) ;
        RotationMatrix R2(rot) ;
        Details::CheckMatrixWithEigen_(R2, rot) ;
        RotationMatrix R3(R2) ;
        Details::CheckMatrix(R2, R3) ;
        RotationMatrix R4(rot(0, 0), rot(1, 0), rot(2, 0),
                          rot(0, 1), rot(1, 1), rot(2, 1),
                          rot(0, 2), rot(1, 2), rot(2, 2)) ;
        Details::CheckMatrix(R3, R4) ;
    }


    // Check construction exceptions
    {
        // Check orthogonality
        BOOST_CHECK_EXCEPTION(RotationMatrix(1., 0., 0.,
                                             0., 1., 0.,
                                             0., 1., 0.),
                              RotationInitialisationException,
                              Details::NotOrthoExceptionChecker_) ;
        // Check normality
        BOOST_CHECK_EXCEPTION(RotationMatrix(2., 0., 0.,
                                             0., 1., 0.,
                                             0., 0., 1.),
                              RotationInitialisationException,
                              Details::NotNormalExceptionChecker_) ;
        // Check right-handed
        BOOST_CHECK_EXCEPTION(RotationMatrix(1., 0., 0.,
                                             0., 0., 1.,
                                             0., 1., 0.),
                              RotationInitialisationException,
                              Details::NotRightHandExceptionChecker_) ;
    }
}

void RotationMatrix_Methods( void )
{
    using BV::Details::epsilon ;

    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<RotationMatrix>() ;
    RotationABC_Methods<RotationMatrix>() ;

    Details::CheckTypedCallsToOverLoads<RotationMatrix>() ;

    /// Tests for member methods of RotationMatrix implementation
    // Test call member method transpose
    {
        RotationMatrix r ;
        r.transpose() ;
    }

    // Test call member method determinant
    {
        RotationMatrix r ;
        r.determinant() ;
    }

    // Test call getter operator ()
    {
        RotationMatrix r ;
        const double a00 = r(0, 0) ;
        const double a10 = r(1, 0) ;
        const double a20 = r(2, 0) ;
        const double a01 = r(0, 1) ;
        const double a11 = r(1, 1) ;
        const double a21 = r(2, 1) ;
        const double a02 = r(0, 2) ;
        const double a12 = r(1, 2) ;
        const double a22 = r(2, 2) ;
        BOOST_CHECK(a00 == 1.) ;
        BOOST_CHECK(a10 == 0.) ;
        BOOST_CHECK(a20 == 0.) ;
        BOOST_CHECK(a01 == 0.) ;
        BOOST_CHECK(a11 == 1.) ;
        BOOST_CHECK(a21 == 0.) ;
        BOOST_CHECK(a02 == 0.) ;
        BOOST_CHECK(a12 == 0.) ;
        BOOST_CHECK(a22 == 1.) ;
    }

    // Test call member method getMatrix
    {
        RotationMatrix r ;
        Eigen::Matrix3d mat(r.getMatrix()) ;
        BOOST_CHECK(mat(0,0) == 1.) ;
        BOOST_CHECK(mat(1,0) == 0.) ;
        BOOST_CHECK(mat(2,0) == 0.) ;
        BOOST_CHECK(mat(0,1) == 0.) ;
        BOOST_CHECK(mat(1,1) == 1.) ;
        BOOST_CHECK(mat(2,1) == 0.) ;
        BOOST_CHECK(mat(0,2) == 0.) ;
        BOOST_CHECK(mat(1,2) == 0.) ;
        BOOST_CHECK(mat(2,2) == 1.) ;
    }


    // Check some matrix constructions with a set of angles values
    std::vector<Eigen::Matrix3d> allTestMatrices(Details::GetTestEigenRotationMatrices()) ;
    for (unsigned iR = 0 ; iR < allTestMatrices.size()-1; ++iR )
    {
        Eigen::Matrix3d eR(allTestMatrices[iR]) ;
        RotationMatrix gR(eR) ;
        Details::CheckMatrixWithEigen_(gR, eR) ;
        Details::CheckMatrixWithEigen_(gR.transpose(), eR.transpose()) ;
        BV::Tools::Tests::CheckEigenMatrix(gR.getMatrix(), eR) ;
        BV::Tools::Tests::CheckEigenMatrix(gR.getMatrix().transpose(), eR.transpose()) ;
        BOOST_CHECK_SMALL(gR.determinant()-1., epsilon) ;

        Details::ABCCheck<RotationMatrix>(gR, eR, 9, 6) ;
        Eigen::Matrix3d eR2(allTestMatrices[iR+1]) ;
        Rotation::RotationMatrix gR2(eR2) ;
        Details::ABCCheck_CompRot<RotationMatrix>(gR, eR, gR2, eR2) ;
    }

}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
