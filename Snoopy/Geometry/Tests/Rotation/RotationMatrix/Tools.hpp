#pragma once
#ifndef __BV_Geometry_Tests_Rotation_RotationMatrix_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_RotationMatrix_Tools_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/Exceptions.hpp"
#include "Tests/Rotation/ABC/Tools.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

using Geometry::Rotation::RotationMatrix ;
using BV::Geometry::Exceptions::RotationInitialisationException ;

namespace Details {

void CheckMatrix(const RotationMatrix & m1, const RotationMatrix & m2) ;

void CheckMatrixWithEigen_(const RotationMatrix & m1, const Eigen::Matrix3d & m2) ;

bool NotOrthoExceptionChecker_(const RotationInitialisationException & e) ;

bool NotNormalExceptionChecker_(const RotationInitialisationException & e) ;

bool NotRightHandExceptionChecker_(const RotationInitialisationException & e) ;

template <>
void CheckUnknowns_<Rotation::RotationMatrix>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

} // End of namespace Details


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_RotationMatrix_Tools_hpp__
