#include <boost/test/unit_test.hpp>

#include "Geometry/GeometryTypedefs.hpp"
#include "Tests/Rotation/RotationMatrix/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

using Geometry::Rotation::RotationMatrix ;
using BV::Geometry::Exceptions::RotationInitialisationException ;

namespace Details {

void CheckMatrix(const RotationMatrix & m1, const RotationMatrix & m2)
{
    using BV::Details::epsilon ;
    for (unsigned i=0; i<3; ++i)
    {
        for (unsigned j=0; j<3; ++j)
        {
            BOOST_CHECK_SMALL(m1(i, j)-m2(i, j), epsilon) ;
        }
    }
}

void CheckMatrixWithEigen_(const RotationMatrix & m1, const Eigen::Matrix3d & m2)
{
    using BV::Details::epsilon ;
    for (unsigned i=0; i<3; ++i)
    {
        for (unsigned j=0; j<3; ++j)
        {
            BOOST_CHECK_SMALL(m1(i, j)-m2(i, j), epsilon) ;
        }
    }
}

bool NotOrthoExceptionChecker_(const RotationInitialisationException & e)
{
    return e.getMessage() == "Incorrect rotation matrix: not orthogonal" ;
}

bool NotNormalExceptionChecker_(const RotationInitialisationException & e)
{
    return e.getMessage() == "Incorrect rotation matrix: not normalized" ;
}

bool NotRightHandExceptionChecker_(const RotationInitialisationException & e)
{
    return e.getMessage() == "Incorrect rotation matrix: not right-handed" ;
}

template <>
void CheckUnknowns_<Rotation::RotationMatrix>(Rotation::ABC & abc,
                                              const Eigen::Matrix3d & R)
{
    using BV::Details::epsilon ;

    // Warning unknowns are stored by columns
    Eigen::VectorXd unknowns(abc.unknowns()) ;
    for (unsigned i=0; i<3; ++i)
    {
        for (unsigned j=0; j<3; ++j)
        {
            BOOST_CHECK_SMALL(R(i, j)-unknowns(3*j+i), epsilon) ;
        }
    }
    // Constraints are given as:
    // 0: d1.d1 - 1.
    // 1: d1.d2
    // 2: d1.d3
    // 3: d2.d2 - 1.
    // 4: d2.d3
    // 5: d3.d3 - 1.

    Eigen::Vector3d X(1., 0., 0.) ;
    Eigen::Vector3d Y(0., 1., 0.) ;
    Eigen::Vector3d Z(0., 0., 1.) ;
    Eigen::Vector3d expectedD1(R*X) ;
    Eigen::Vector3d expectedD2(R*Y) ;
    Eigen::Vector3d expectedD3(R*Z) ;

    double c0(expectedD1.dot(expectedD1)-1.) ;
    double c1(expectedD1.dot(expectedD2)) ;
    double c2(expectedD1.dot(expectedD3)) ;
    double c3(expectedD2.dot(expectedD2)-1.) ;
    double c4(expectedD2.dot(expectedD3)) ;
    double c5(expectedD3.dot(expectedD3)-1.) ;

    Eigen::VectorXd constraints(abc.constraints()) ;
    BOOST_CHECK_SMALL(constraints(0) - c0, epsilon) ;
    BOOST_CHECK_SMALL(constraints(1) - c1, epsilon) ;
    BOOST_CHECK_SMALL(constraints(2) - c2, epsilon) ;
    BOOST_CHECK_SMALL(constraints(3) - c3, epsilon) ;
    BOOST_CHECK_SMALL(constraints(4) - c4, epsilon) ;
    BOOST_CHECK_SMALL(constraints(5) - c5, epsilon) ;


    // Modify unknowns (multiply d2 by 2)
    Eigen::VectorXd unknowns2(unknowns) ;
    unknowns2(3) *= 2. ;
    unknowns2(4) *= 2. ;
    unknowns2(5) *= 2. ;
    abc.unknowns(unknowns2) ;

    Eigen::VectorXd constraints2(abc.constraints()) ;
    BOOST_CHECK_SMALL(constraints2(0) - c0, epsilon) ;
    BOOST_CHECK_SMALL(constraints2(1) - 2.*c1, epsilon) ;
    BOOST_CHECK_SMALL(constraints2(2) - c2, epsilon) ;
    BOOST_CHECK_SMALL(constraints2(3) - (4.*c3 + 3.), epsilon) ;
    BOOST_CHECK_SMALL(constraints2(4) - 2.*c4, epsilon) ;
    BOOST_CHECK_SMALL(constraints2(5) - c5, epsilon) ;

    // Reset unknowns to what they were
    abc.unknowns(unknowns) ;

    // Check again
    Eigen::VectorXd constraints3(abc.constraints()) ;
    BOOST_CHECK_SMALL(constraints3(0) - c0, epsilon) ;
    BOOST_CHECK_SMALL(constraints3(1) - c1, epsilon) ;
    BOOST_CHECK_SMALL(constraints3(2) - c2, epsilon) ;
    BOOST_CHECK_SMALL(constraints3(3) - c3, epsilon) ;
    BOOST_CHECK_SMALL(constraints3(4) - c4, epsilon) ;
    BOOST_CHECK_SMALL(constraints3(5) - c5, epsilon) ;

    Eigen::VectorXd unknowns3(abc.unknowns()) ;
    for (unsigned i=0; i<3; ++i)
    {
        for (unsigned j=0; j<3; ++j)
        {
            BOOST_CHECK_SMALL(R(i, j)-unknowns3(3*j+i), epsilon) ;
        }
    }
}

} // End of namespace Details


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
