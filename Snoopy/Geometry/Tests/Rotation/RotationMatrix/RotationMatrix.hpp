#pragma once
#ifndef __BV_Geometry_Tests_Rotation_RotationMatrix_RotationMatrix_hpp__
#define __BV_Geometry_Tests_Rotation_RotationMatrix_RotationMatrix_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void RotationMatrix_Constructors( void ) ;
void RotationMatrix_Methods( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_RotationMatrix_RotationMatrix_hpp__
