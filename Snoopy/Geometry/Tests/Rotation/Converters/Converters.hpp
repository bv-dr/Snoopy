#pragma once
#ifndef __BV_Geometry_Tests_Rotation_Converters_Converters_hpp__
#define __BV_Geometry_Tests_Rotation_Converters_Converters_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void Rotation_Converters( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Converters_Converters_hpp__
