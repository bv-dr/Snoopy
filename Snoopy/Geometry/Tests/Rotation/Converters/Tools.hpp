#pragma once
#ifndef __BV_Geometry_Tests_Rotation_Converters_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_Converters_Tools_hpp__


namespace BV {
namespace Geometry {
namespace Tests {
namespace Details
{

void ConversionChecks( void ) ;

void TestRotationOperators(void) ;

void TestDirectorsWithOtherRotation(void) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Converters_Tools_hpp__
