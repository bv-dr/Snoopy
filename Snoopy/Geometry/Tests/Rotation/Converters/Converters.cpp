#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/Converters/Converters.hpp"
#include "Tests/Rotation/Converters/Tools.hpp"


namespace BV {
namespace Geometry {
namespace Tests {


void Rotation_Converters(void)
{
    Details::ConversionChecks() ;
    Details::TestRotationOperators() ;
    Details::TestDirectorsWithOtherRotation() ;
}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
