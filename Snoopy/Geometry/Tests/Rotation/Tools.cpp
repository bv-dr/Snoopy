
#include "Tests/Rotation/Tools.hpp"

#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

Eigen::Matrix3d GetRXMatrix(const double & angle)
{
    Eigen::Matrix3d R ;
    R(0, 0) = 1. ;
    R(0, 1) = 0. ;
    R(0, 2) = 0. ;
    R(1, 0) = 0. ;
    R(1, 1) = std::cos(angle) ;
    R(1, 2) = -std::sin(angle) ;
    R(2, 0) = 0. ;
    R(2, 1) = std::sin(angle) ;
    R(2, 2) = std::cos(angle) ;
    return R ;
}

Eigen::Matrix3d GetRYMatrix(const double & angle)
{
    Eigen::Matrix3d R ;
    R(0, 0) = std::cos(angle) ;
    R(0, 1) = 0. ;
    R(0, 2) = std::sin(angle) ;
    R(1, 0) = 0. ;
    R(1, 1) = 1. ;
    R(1, 2) = 0. ;
    R(2, 0) = -std::sin(angle) ;
    R(2, 1) = 0. ;
    R(2, 2) = std::cos(angle) ;
    return R ;
}

Eigen::Matrix3d GetRZMatrix(const double & angle)
{
    Eigen::Matrix3d R ;
    R(0, 0) = std::cos(angle) ;
    R(0, 1) = -std::sin(angle) ;
    R(0, 2) = 0. ;
    R(1, 0) = std::sin(angle) ;
    R(1, 1) = std::cos(angle) ;
    R(1, 2) = 0. ;
    R(2, 0) = 0. ;
    R(2, 1) = 0. ;
    R(2, 2) = 1. ;
    return R ;
}

Eigen::Matrix3d GetRFromConvention(const BV::Geometry::Rotation::AxisConvention & conv, const double & angle)
{
    using BV::Geometry::Rotation::AxisConvention ;
    if(conv == AxisConvention::X_AXIS)
    {
        return GetRXMatrix(angle) ;
    }
    else if(conv == AxisConvention::Y_AXIS)
    {
        return GetRYMatrix(angle) ;
    }
    else if(conv == AxisConvention::Z_AXIS)
    {
        return GetRZMatrix(angle) ;
    }
    else
    {
        throw "Not recognized axis convention for matrix construction in tests." ;
    }
}

Eigen::Vector3d Rotate(const Eigen::Matrix3d & R,
                       const Eigen::Vector3d & vect)
{
    return R * vect ;
}

double Angle02PI(const double & val)
{
    double valTmp(std::fmod(val, 2*M_PI)) ;
    if (val < 0.)
    {
        valTmp += 2. * M_PI ;
    }
    return valTmp ;
}

double AngleMinusPi_Pi(const double & angle)
{
    using BV::Details::epsilon ;
    if( (std::abs(fmod(angle, 2.0*M_PI))<epsilon) || (std::abs(angle)<epsilon))
    {
        return 0. ;
    }
    double ang(fmod(angle, 2.0*M_PI)) ;
    if(ang>M_PI)
    {
        ang -= 2.*M_PI ;
    }
    else if(ang<-M_PI)
    {
        ang += 2.*M_PI ;
    }
    return ang ;
}

double degToRad(const double & angle)
{
    return angle * M_PI / 180. ;
}

double radToDeg(double angle)
{
    return angle * 180. / M_PI ;
}

std::vector<Eigen::Vector3d> GetTestEigenVectors(void)
{
    std::vector<Eigen::Vector3d> vectors ;
    vectors.push_back(Eigen::Vector3d(0., 0., 0.)) ;
    vectors.push_back(Eigen::Vector3d(1., 0., 0.)) ;
    vectors.push_back(Eigen::Vector3d(0., 1., 0.)) ;
    vectors.push_back(Eigen::Vector3d(0., 0., 1.)) ;
    vectors.push_back(Eigen::Vector3d(1., 1., 1.)) ;
    vectors.push_back(Eigen::Vector3d(-1., -1., 1.)) ;
    vectors.push_back(Eigen::Vector3d(10., -8., 3.586)) ;
    return vectors ;
}

std::vector<double> GetTestAngles(void)
{
    std::vector<double> angles ;

    // [ -360. : 360. : 30. ]
    double deltaAngle(degToRad(30.)) ;

    double minAngle(degToRad(-360.)) ;
    double maxAngle(degToRad(360.)) ;

    angles.push_back(minAngle) ;

    while( angles.back() < maxAngle )
    {
        angles.push_back(angles.back() + deltaAngle) ;
    }

    // Others....
    angles.push_back(degToRad(2000.)) ;
    angles.push_back(degToRad(-1000.5)) ;

    return angles ;
}

std::vector<Eigen::Vector3d> GetTripleTestAngles(void)
{
    std::vector<Eigen::Vector3d> tripleAngles ;

    tripleAngles.push_back(Eigen::Vector3d(0., 0., 0.)) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(0.)  , degToRad(0.)  , degToRad(20.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(0.)  , degToRad(0.)  , degToRad(90.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(0.)  , degToRad(90.) , degToRad(90.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(90.) , degToRad(90.) , degToRad(90.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(90.) , degToRad(90.) , degToRad(180.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(90.) , degToRad(180.), degToRad(180.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(180.), degToRad(180.), degToRad(180.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(180.), degToRad(180.), degToRad(270.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(180.), degToRad(270.), degToRad(270.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(270.), degToRad(270.), degToRad(270.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-0.)  , degToRad(-0.)  , degToRad(-90.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-0.)  , degToRad(-90.) , degToRad(-90.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-90.) , degToRad(-90.) , degToRad(-90.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-90.) , degToRad(-90.) , degToRad(-180.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-90.) , degToRad(-180.), degToRad(-180.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-180.), degToRad(-180.), degToRad(-180.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-180.), degToRad(-180.), degToRad(-270.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-180.), degToRad(-270.), degToRad(-270.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(-270.), degToRad(-270.), degToRad(-270.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(80.), degToRad(-50.), degToRad(-10.))) ;
    tripleAngles.push_back(Eigen::Vector3d(degToRad(20.), degToRad(30.), degToRad(10.))) ;

    // FIXME: Takes a lot of time to run the tests... keep this?
    // std::vector<double> angles(GetTestAngles()) ;
    // Eigen::Vector3d tmpVec(Eigen::Vector3d::Zero()) ;
    // for (double angle1 : angles)
    // {
    //     tmpVec(0) = angle1 ;
    //     for (double angle2 : angles)
    //     {
    //         tmpVec(1) = angle2 ;
    //         for (double angle3 : angles)
    //         {
    //             tmpVec(2) = angle3 ;
    //             tripleAngles.push_back(tmpVec) ;
    //         }
    //     }
    // }
    return tripleAngles ;
}

std::vector< std::vector<BV::Geometry::Rotation::AxisConvention> > GetTestRotationConventions(void)
{
    using BV::Geometry::Rotation::AxisConvention ;
    std::vector< std::vector<AxisConvention> > conventions ;
    std::vector<BV::Geometry::Rotation::AxisConvention> conv ;
    for (AxisConvention axUConv = AxisConvention::X_AXIS ; axUConv < AxisConvention::Z_AXIS + 1 ; axUConv = AxisConvention(axUConv+1))
    {
        for (AxisConvention axVConv = AxisConvention::X_AXIS ; axVConv < AxisConvention::Z_AXIS + 1 ; axVConv = AxisConvention(axVConv+1))
        {
            for (AxisConvention axWConv = AxisConvention::X_AXIS ; axWConv < AxisConvention::Z_AXIS + 1 ; axWConv = AxisConvention(axWConv+1))
            {
                conv.push_back(axUConv) ;
                conv.push_back(axVConv) ;
                conv.push_back(axWConv) ;
                conventions.push_back(conv) ;
                conv.clear() ;
            }
        }
    }
    return conventions ;
}

Eigen::Matrix3d GetRProductFromConvention_(const Eigen::Vector3d & tripleAngles,
                                           const std::vector<BV::Geometry::Rotation::AxisConvention> & convention)
{
    using BV::Geometry::Rotation::AxisConvention ;
    Eigen::Matrix3d R0(GetRFromConvention(convention[0], tripleAngles(0))) ;
    Eigen::Matrix3d R1(GetRFromConvention(convention[1], tripleAngles(1))) ;
    Eigen::Matrix3d R2(GetRFromConvention(convention[2], tripleAngles(2))) ;
    return R0*R1*R2 ;
}

std::vector<Eigen::Matrix3d> GetTestEigenRotationMatrices(void)
{
    std::vector<Eigen::Matrix3d> matrices ;
    for (const std::vector<BV::Geometry::Rotation::AxisConvention> & convention : GetTestRotationConventions())
    {
        for (Eigen::Vector3d angles : GetTripleTestAngles())
        {
            matrices.push_back(GetRProductFromConvention_(angles, convention)) ;
        }
    }
    return matrices ;
}



} // End of namespace Details


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
