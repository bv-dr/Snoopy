#pragma once
#ifndef __BV_Geometry_Tests_Rotation_BasisVectors_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_BasisVectors_Tools_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Objects/Tools.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/GeometryTypedefs.hpp"
#include "Tests/Rotation/ABC/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <>
void CheckUnknowns_<Rotation::BasisVectors>( Rotation::ABC & abc, const Eigen::Matrix3d & R ) ;

void BasisVectorPointer_(void) ;

template < class Euler  >
void ConversionToEulerAnglesDirectorsTester_(
                            const Geometry::Vector & d1,
                            const Geometry::Vector & d2,
                            const Geometry::Vector & d3
                                            )
{
    const Rotation::BasisVectors bv( d1, d2, d3 ) ;

    // FIXME not available anymore...
    //const std::vector< Euler > es ( bv.convertTo< Euler >() ) ;

    //for ( const Euler & e: es )
    //{
    //    BOOST_CHECK_SMALL( Distance( e.d1(), d1 ), epsilon ) ;
    //    BOOST_CHECK_SMALL( Distance( e.d2(), d2 ), epsilon ) ;
    //    BOOST_CHECK_SMALL( Distance( e.d3(), d3 ), epsilon ) ;
    //}
}

template < class Euler  >
void ConversionToEulerAnglesGimbalLockTester_( void )
{
    using Geometry::Vector ;
    using BV::Details::epsilon ;
    std::vector< Vector > directors ;
    directors.reserve( 6 ) ;

    directors.push_back( Vector(  1.,  0.,  0. ) ) ;
    directors.push_back( Vector(  0.,  1.,  0. ) ) ;
    directors.push_back( Vector(  0.,  0.,  1. ) ) ;
    directors.push_back( Vector( -1.,  0.,  0. ) ) ;
    directors.push_back( Vector(  0., -1.,  0. ) ) ;
    directors.push_back( Vector(  0.,  0., -1. ) ) ;

    for ( const Vector & d1: directors )
    {
        for ( const Vector & d2: directors )
        {
            if ( std::abs( d1 * d2 ) < epsilon )
            {
                const Vector d3( d1 ^ d2 ) ;
                ConversionToEulerAnglesDirectorsTester_< Euler >( d1, d2, d3 ) ;
            }
        }
    }
}

template < class Euler  >
void ConversionToEulerAnglesTester_( void )
{
    {
        std::vector< double > angles ;
        angles.push_back( 0. ) ;
        angles.push_back( M_PI / 5. ) ;
        angles.push_back( M_PI / 4. ) ;
        angles.push_back( M_PI / 3. ) ;
        angles.push_back( M_PI / 2. ) ;
        angles.push_back( (1.0 - 1.e-4) * M_PI ) ;
        angles.push_back( M_PI ) ;
        angles.push_back( (1.0 + 1.e-4) * M_PI ) ;
        angles.push_back( 3. * M_PI / 2. ) ;
        angles.push_back( 2. * M_PI ) ;

        for ( const double & t: angles )
        {
            const double s( std::sin( t ) ) ;
            const double c( std::cos( t ) ) ;
            {
                const Vector d1(   c,   s,  0. ) ;
                const Vector d2(  -s,   c,  0. ) ;
                const Vector d3(  0.,  0.,  1. ) ;
                ConversionToEulerAnglesDirectorsTester_< Euler >( d1, d2, d3 ) ;
            }
            {
                const Vector d1(   c,  0.,   s ) ;
                const Vector d2(  0.,  1.,  0. ) ;
                const Vector d3(  -s,  0.,   c ) ;
                ConversionToEulerAnglesDirectorsTester_< Euler >( d1, d2, d3 ) ;
            }
            {
                const Vector d1(  1.,  0.,  0. ) ;
                const Vector d2(  0.,   c,   s ) ;
                const Vector d3(  0.,  -s,   c ) ;
                ConversionToEulerAnglesDirectorsTester_< Euler >( d1, d2, d3 ) ;
            }
        }
    }

    ConversionToEulerAnglesGimbalLockTester_< Euler >() ;
}


} // End of namespace Details


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_RotationMatrix_Tools_hpp__
