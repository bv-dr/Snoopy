#include <memory>
#include <boost/test/unit_test.hpp>

#include "Geometry/GeometryTypedefs.hpp"
#include "Tests/Rotation/RotationMatrix/Tools.hpp"
#include "Tests/Rotation/BasisVectors/Tools.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <>
void CheckUnknowns_<Rotation::BasisVectors>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    // Same thing as rotation matrix !
    CheckUnknowns_<Rotation::RotationMatrix>(abc, R) ;
}

void BasisVectorPointer_(void)
{
    Vector d1(1., 0., 0.) ;
    Vector d2(0., 1., 0.) ;
    Vector d3(0., 0., 1.) ;
    const Vector vect(1., 2., 3.) ;
    // First we try the operation without the pointer.
    {
        Rotation::BasisVectors b ;
        const Eigen::Vector3d v(b.rotate(vect.toArray())) ;
    }
    // Then we perform the same thing with a pointer
    {
        Rotation::ABC * ptr(new Rotation::BasisVectors(d1, d2, d3)) ;
        const Eigen::Vector3d v(ptr->rotate(vect.toArray())) ;
        delete ptr ;
    }
    // Then we perform the same thing with a smart pointer
    // We try each of the constructors
    {
        std::unique_ptr<Rotation::ABC> ptr(new Rotation::BasisVectors()) ;
        const Eigen::Vector3d v(ptr->rotate(vect.toArray())) ;
        std::unique_ptr<Rotation::ABC> ptr2(new Rotation::BasisVectors()) ;
        ptr = std::move(ptr2) ;
        const Eigen::Vector3d v2(ptr->rotate(vect.toArray())) ;
    }
    {
        std::unique_ptr<Rotation::ABC> ptr(
                        new Rotation::BasisVectors(d1, d2, d3)
                                          ) ;
        const Eigen::Vector3d v(ptr->rotate(vect.toArray())) ;
        std::unique_ptr<Rotation::ABC> ptr2(new Rotation::BasisVectors()) ;
        ptr = std::move(ptr2) ;
        const Eigen::Vector3d v2(ptr->rotate(vect.toArray())) ;
    }
    {
        std::unique_ptr<Rotation::ABC> ptr(
                new Rotation::BasisVectors(1., 0., 0., 0., 1., 0., 0., 0., 1.)
                                          ) ;
        const Eigen::Vector3d v(ptr->rotate(vect.toArray())) ;
        std::unique_ptr<Rotation::ABC> ptr2(new Rotation::BasisVectors()) ;
        ptr = std::move(ptr2) ;
        const Eigen::Vector3d v2(ptr->rotate(vect.toArray())) ;
    }
}

} // End of namespace Details


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
