/*!
 * \file BasisVector.cpp
 */

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tools/Tests/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/ABC/Tools.hpp"

#include "Tests/Rotation/BasisVectors/BasisVectors.hpp"
#include "Tests/Rotation/BasisVectors/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"

#include "Tests/Objects/Tools.hpp"
#include "Geometry/Vector.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

void BasisVectors_Constructors(void)
{
    using Geometry::Vector ;
    Eigen::Matrix3d I(Eigen::Matrix3d::Identity()) ;
    Rotation::BasisVectors Ibv(I) ;

    Rotation::BasisVectors bv ;
    Details::CheckXYZ<Vector>(bv.d1(), Vector(1., 0., 0.)) ;
    Details::CheckXYZ<Vector>(bv.d2(), Vector(0., 1., 0.)) ;
    Details::CheckXYZ<Vector>(bv.d3(), Vector(0., 0., 1.)) ;
    Rotation::BasisVectors bv2(Vector(0., 1., 0.),
                               Vector(0., 0., 1.),
                               Vector(1., 0., 0.)) ;
    Details::CheckXYZ<Vector>(bv2.d1(), Vector(0., 1., 0.)) ;
    Details::CheckXYZ<Vector>(bv2.d2(), Vector(0., 0., 1.)) ;
    Details::CheckXYZ<Vector>(bv2.d3(), Vector(1., 0., 0.)) ;
    Eigen::Matrix3d R(Details::GetRZMatrix(5.*M_PI/180.)) ;
    Rotation::BasisVectors bv3(R.col(0), R.col(1), R.col(2)) ;
    Details::CheckXYZ<Vector>(bv3.d1(), Vector(R(0, 0), R(1, 0), R(2, 0))) ;
    Details::CheckXYZ<Vector>(bv3.d2(), Vector(R(0, 1), R(1, 1), R(2, 1))) ;
    Details::CheckXYZ<Vector>(bv3.d3(), Vector(R(0, 2), R(1, 2), R(2, 2))) ;
    Rotation::BasisVectors bv4(R(0, 0), R(1, 0), R(2, 0),
                               R(0, 1), R(1, 1), R(2, 1),
                               R(0, 2), R(1, 2), R(2, 2)) ;
    Details::CheckXYZ<Vector>(bv4.d1(), Vector(R(0, 0), R(1, 0), R(2, 0))) ;
    Details::CheckXYZ<Vector>(bv4.d2(), Vector(R(0, 1), R(1, 1), R(2, 1))) ;
    Details::CheckXYZ<Vector>(bv4.d3(), Vector(R(0, 2), R(1, 2), R(2, 2))) ;
}

void BasisVectors_Methods( void )
{
    using BV::Details::epsilon ;

    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<Rotation::BasisVectors>() ;
    RotationABC_Methods<Rotation::BasisVectors>() ;

    Details::CheckTypedCallsToOverLoads<Rotation::BasisVectors>() ;

    // Check some matrix constructions with a set of angles values
    std::vector<Eigen::Matrix3d> allTestMatrices(Details::GetTestEigenRotationMatrices()) ;
    for (unsigned iR = 0 ; iR < allTestMatrices.size()-1; ++iR )
    {
        Eigen::Matrix3d eR(allTestMatrices[iR]) ;
        Rotation::BasisVectors gR(eR.col(0), eR.col(1), eR.col(2)) ;
        Details::ABCCheck<Rotation::BasisVectors>(gR, eR, 9, 6) ;
        Eigen::Matrix3d eR2(allTestMatrices[iR+1]) ;
        Rotation::BasisVectors gR2(eR2) ;
        Details::ABCCheck_CompRot<Rotation::BasisVectors>(gR, eR, gR2, eR2) ;
    }

    Details::BasisVectorPointer_() ;

}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
