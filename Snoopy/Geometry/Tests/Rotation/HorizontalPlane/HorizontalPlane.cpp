#include <boost/test/unit_test.hpp>

#include "Tests/Rotation/HorizontalPlane/HorizontalPlane.hpp"
#include "Tests/Rotation/ABC/ABC.hpp"
#include "Tests/Rotation/HorizontalPlane/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/Exceptions.hpp"

using BV::Geometry::Rotation::HorizontalPlane ;

namespace BV {
namespace Geometry {
namespace Tests {


void HorizontalPlane_Constructors(void)
{
    using BV::Details::epsilon ;
    Eigen::Matrix3d I(Eigen::Matrix3d::Identity()) ;
    HorizontalPlane Ihp(I) ;

    HorizontalPlane hp ;
    HorizontalPlane hp2(hp) ;
    BOOST_CHECK_SMALL(hp.angle() - hp2.angle(), epsilon) ;
    const double angle(20.*M_PI/180.) ;
    Eigen::VectorXd unk(Eigen::VectorXd::Zero(1)) ;
    unk << angle ;
    hp.unknowns(unk) ;
    HorizontalPlane hp3(angle) ;
    hp.angle() = angle ;
    BOOST_CHECK_SMALL(hp.angle() - hp3.angle(), epsilon) ;
    BV::Tools::Tests::CheckEigenVectors(hp3.unknowns(), unk) ;

    using BV::Geometry::Exceptions::RotationInitialisationException ;

    BOOST_CHECK_EXCEPTION(HorizontalPlane(Details::GetRXMatrix(0.1)),
                                          RotationInitialisationException,
                                          Details::InitExceptionChecker) ;

    BOOST_CHECK_EXCEPTION(HorizontalPlane(Details::GetRYMatrix(-0.2)),
                                          RotationInitialisationException,
                                          Details::InitExceptionChecker) ;
}

void HorizontalPlane_Methods(void)
{
    // Tests for call to member methods implementation of abstract classes
    ABC_Methods<HorizontalPlane>() ;
    RotationABC_Methods<HorizontalPlane>() ;

    Details::CheckTypedCallsToOverLoads<HorizontalPlane>() ;

    // Check some matrix constructions with a set of angles values
    std::vector<double> allTestAngles(Details::GetTestAngles()) ;
    for (unsigned iAngle = 0 ; iAngle < allTestAngles.size()-1; ++iAngle )
    {
        double angle(allTestAngles[iAngle]) ;
        Eigen::Matrix3d RZ(Details::GetRZMatrix(angle)) ;
        Rotation::HorizontalPlane hpR(RZ) ;
        Details::ABCCheck<HorizontalPlane>(hpR, RZ, 1, 0) ;

        double angle2(allTestAngles[iAngle+1]) ;
        Eigen::Matrix3d RZ2(Details::GetRZMatrix(angle2)) ;
        Rotation::HorizontalPlane hpR2(RZ2) ;
        Details::ABCCheck_CompRot<Rotation::HorizontalPlane>(hpR, RZ, hpR2, RZ2) ;
    }

}

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
