#pragma once
#ifndef __BV_Geometry_Tests_Rotation_HorizontalPlane_HorizontalPlane_hpp__
#define __BV_Geometry_Tests_Rotation_HorizontalPlane_HorizontalPlane_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void HorizontalPlane_Constructors( void ) ;
void HorizontalPlane_Methods( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_HorizontalPlane_HorizontalPlane_hpp__
