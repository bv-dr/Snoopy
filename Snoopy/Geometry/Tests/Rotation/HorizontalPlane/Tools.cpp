#include <complex>

#include "Tests/Rotation/HorizontalPlane/Tools.hpp"

#include <boost/test/unit_test.hpp>

#include "Tools/Tests/Tools.hpp"
#include "Tests/Rotation/Tools.hpp"

#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/Exceptions.hpp"

using BV::Geometry::Rotation::HorizontalPlane ;

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

bool HorizontalPlaneMatrixCheck(const Eigen::Matrix3d & R)
{
    using BV::Geometry::Exceptions::RotationInitialisationException ;
    using BV::Details::epsilon ;
    if( (std::abs(R(2,0)) > epsilon) || (std::abs(R(2,1)) > epsilon)
            || (std::abs(R(0,2)) > epsilon) || (std::abs(R(1,2)) > epsilon)
            || (std::abs( std::abs(R(2,2) - 1.) ) > epsilon) )
    {
        return false ;
    }
    return true ;
}

double GetAngleFromR_(const Eigen::Matrix3d & R)
{
    return Details::AngleMinusPi_Pi(std::atan2(R(1,0), R(0,0))) ;
}

void CheckHorizontalPlaneCoordinates(const double & angle1, const double & angle2)
{
    using BV::Details::epsilon ;
    if( (std::abs(std::abs(angle1) - M_PI) < epsilon)
        || (std::abs(std::abs(angle2) - M_PI) < epsilon) )
    {
        BOOST_CHECK_SMALL(std::abs(angle1)-std::abs(angle2), epsilon) ;
    }
    else
    {
        BOOST_CHECK_SMALL(std::abs(Details::AngleMinusPi_Pi(angle1)-Details::AngleMinusPi_Pi(angle2)), epsilon) ;
    }
}

void CheckHorizontalPlaneCoordinates(const Eigen::VectorXd & unk1, const Eigen::VectorXd & unk2)
{
    CheckHorizontalPlaneCoordinates(unk1(0), unk2(0)) ;
}

template <>
void CheckUnknowns_<Rotation::HorizontalPlane>(Rotation::ABC & abc, const Eigen::Matrix3d & R)
{
    Eigen::VectorXd unknowns(abc.unknowns()) ;
    Eigen::VectorXd expectedUnknowns(Eigen::VectorXd::Zero(1)) ;
    expectedUnknowns << GetAngleFromR_(R) ;
    CheckHorizontalPlaneCoordinates(unknowns, expectedUnknowns) ;
}

bool InitExceptionChecker(const BV::Geometry::Exceptions::RotationInitialisationException & e)
{
    return e.getMessage() == "Incorrect Horizontal plane rotation matrix: not in XY plane." ;
}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
