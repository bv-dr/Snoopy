#pragma once
#ifndef __BV_Geometry_Tests_Rotation_HorizontalPlane_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_HorizontalPlane_Tools_hpp__

#include <Eigen/Dense>

#include "Tests/Rotation/ABC/Tools.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/Exceptions.hpp"

using BV::Geometry::Rotation::AxisAndAngle ;

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

bool HorizontalPlaneMatrixCheck(const Eigen::Matrix3d & R) ;

void CheckHorizontalPlaneCoordinates(const double & angle1, const double & angle2) ;

void CheckHorizontalPlaneCoordinates(const Eigen::VectorXd & unk1, const Eigen::VectorXd & unk2) ;

template <>
void CheckUnknowns_<Rotation::HorizontalPlane>(Rotation::ABC & abc, const Eigen::Matrix3d & R) ;

bool InitExceptionChecker(const BV::Geometry::Exceptions::RotationInitialisationException & e) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_HorizontalPlane_Tools_hpp__
