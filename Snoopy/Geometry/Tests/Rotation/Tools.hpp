#pragma once
#ifndef __BV_Geometry_Tests_Rotation_Tools_hpp__
#define __BV_Geometry_Tests_Rotation_Tools_hpp__

#include <Eigen/Dense>
#include <vector>

#include "Geometry/Rotation/EulerAnglesAxisConvention.hpp"
#include "Tools/Tests/Tools.hpp"
#include "Tests/Objects/Tools.hpp"
#include "Geometry/Vector.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

Eigen::Matrix3d GetRXMatrix(const double & angle) ;
Eigen::Matrix3d GetRYMatrix(const double & angle) ;
Eigen::Matrix3d GetRZMatrix(const double & angle) ;

Eigen::Matrix3d GetRFromConvention(const BV::Geometry::Rotation::AxisConvention & conv, const double & angle) ;

Eigen::Vector3d Rotate(const Eigen::Matrix3d & R,
                       const Eigen::Vector3d & vect) ;

double Angle02PI(const double & angle) ;

double AngleMinusPi_Pi(const double & angle) ;

double degToRad(const double & angle) ;

double radToDeg(const double & angle) ;

std::vector<Eigen::Vector3d> GetTestEigenVectors(void) ;

std::vector<double> GetTestAngles(void) ;

std::vector<Eigen::Vector3d> GetTripleTestAngles(void) ;

std::vector< std::vector<BV::Geometry::Rotation::AxisConvention> > GetTestRotationConventions(void) ;

std::vector<Eigen::Matrix3d> GetTestEigenRotationMatrices(void) ;

template <typename T>
void CheckTypedCallsToOverLoads( void )
{
    // Test call member method unknowns getter
    {
        T r ;
        Eigen::VectorXd unk(r.unknowns()) ;
    }

    // Test call member method unknowns setter
    {
        T r ;
        Eigen::VectorXd unk(Eigen::VectorXd::Ones(r.nUnknowns())) ;
        r.unknowns(unk) ;
    }


    // Test call member method addOtherRotationAtLeft
    {
        T r1 ;
        Eigen::VectorXd expected(r1.unknowns()) ;
        T r2 ;
        r1.addOtherRotationAtLeft(r2) ;
        BV::Tools::Tests::CheckEigenVectors(r1.unknowns(), expected) ;
    }

    // Test call member method addOtherRotationAtRight
    {
        T r1 ;
        Eigen::VectorXd expected(r1.unknowns()) ;
        T r2 ;
        r1.addOtherRotationAtRight(r2) ;
        BV::Tools::Tests::CheckEigenVectors(r1.unknowns(), expected) ;
    }

    // Test call member method subtractOtherRotationAtLeft
    {
        T r1 ;
        Eigen::VectorXd expected(r1.unknowns()) ;
        T r2 ;
        r1.subtractOtherRotationAtLeft(r2) ;
        BV::Tools::Tests::CheckEigenVectors(r1.unknowns(), expected) ;
    }

    // Test call member method subtractOtherRotationAtRight
    {
        T r1 ;
        Eigen::VectorXd expected(r1.unknowns()) ;
        T r2 ;
        r1.subtractOtherRotationAtRight(r2) ;
        BV::Tools::Tests::CheckEigenVectors(r1.unknowns(), expected) ;
    }

    // Test call member method rotate
    {
        T r ;
        Eigen::Vector3d vect(1., 1., 2.) ;
        Eigen::Vector3d expected(1., 1., 2.) ;
        r.rotate(vect) ;
        BV::Tools::Tests::CheckEigenVectors(vect, expected) ;
    }

    // Test call member method constraints getter
    {
        T r ;
        Eigen::VectorXd constraints(r.constraints()) ;
    }

    // Test call member method constraints getter
    {
        T r1 ;
        Eigen::VectorXd expected(r1.unknowns()) ;
        T r2(r1.getInversed()) ;
        BV::Tools::Tests::CheckEigenVectors(r2.unknowns(), expected) ;
    }

    // Test call member method constraints getter
    {
        T r1 ;
        Eigen::VectorXd expected(r1.unknowns()) ;
        T r2 = r1 ;
        BV::Tools::Tests::CheckEigenVectors(r2.unknowns(), expected) ;
    }
}

} // End of namespace Details


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Tools_hpp__
