#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

// The geometry objects:
//     - Point
//     - Vector
#include "Tests/Objects/Point/Point.hpp"
#include "Tests/Objects/Vector/Vector.hpp"

// The geometry translations:
//    - Horizontal
//    - Cartesian
//    - the converters
#include "Tests/Translation/Horizontal/Horizontal.hpp"
#include "Tests/Translation/Cartesian/Cartesian.hpp"
#include "Tests/Translation/Converters/Converters.hpp"

// The geometry rotations:
//    - RotationMatrix
//    - BasisVectors
//    - Quaternion
//    - AxisAndAngle
//    - EulerAngles
//    - Modified Rodrigues Parameters (MRP)
//    - HorizontalPlane
//    - the converters
#include "Tests/Rotation/RotationMatrix/RotationMatrix.hpp"
#include "Tests/Rotation/BasisVectors/BasisVectors.hpp"
#include "Tests/Rotation/Quaternion/Quaternion.hpp"
#include "Tests/Rotation/AxisAndAngle/AxisAndAngle.hpp"
#include "Tests/Rotation/EulerAngles/EulerAngles.hpp"
#include "Tests/Rotation/MRP/MRP.hpp"
#include "Tests/Rotation/HorizontalPlane/HorizontalPlane.hpp"
#include "Tests/Rotation/Converters/Converters.hpp"

// The geometry Transform3D class
#include "Tests/Transform3D/Transform3D.hpp"

// The rotation Factory
#include "Tests/Rotation/Factory/Factory.hpp"
#include "Tests/Translation/Factory/Factory.hpp"

using namespace boost::unit_test ;

#define BV_ADD_GEOMETRY_TEST( testName )                  \
    GeometryTestSuite->add(BOOST_TEST_CASE( &(            \
                            BV::Geometry::Tests::testName \
                                             ) ) ) ;

test_suite * init_unit_test_suite( int argc, char* argv[] )
{
    test_suite * GeometryTestSuite = BOOST_TEST_SUITE( "GeometryTestSuite" ) ;

    // The objects tests
    BV_ADD_GEOMETRY_TEST(Point_Constructors) ;
    BV_ADD_GEOMETRY_TEST(Point_Methods) ;
    BV_ADD_GEOMETRY_TEST(Vector_Constructors) ;
    BV_ADD_GEOMETRY_TEST(Vector_Methods) ;

    // The translation tests
    BV_ADD_GEOMETRY_TEST(HorizontalTranslation_Constructors) ;
    BV_ADD_GEOMETRY_TEST(HorizontalTranslation_Methods) ;
    BV_ADD_GEOMETRY_TEST(Cartesian_Constructors) ;
    BV_ADD_GEOMETRY_TEST(Cartesian_Methods) ;

    BV_ADD_GEOMETRY_TEST(Translation_Converters) ;

    // The rotation tests
    BV_ADD_GEOMETRY_TEST(RotationMatrix_Constructors) ;
    BV_ADD_GEOMETRY_TEST(RotationMatrix_Methods) ;

    BV_ADD_GEOMETRY_TEST(BasisVectors_Constructors) ;
    BV_ADD_GEOMETRY_TEST(BasisVectors_Methods) ;

    BV_ADD_GEOMETRY_TEST(Quaternion_Constructors) ;
    BV_ADD_GEOMETRY_TEST(Quaternion_Methods) ;

    BV_ADD_GEOMETRY_TEST(AxisAndAngle_Constructors) ;
    BV_ADD_GEOMETRY_TEST(AxisAndAngle_Methods) ;

    BV_ADD_GEOMETRY_TEST(EulerAngles_Constructors) ;
    BV_ADD_GEOMETRY_TEST(EulerAngles_Methods) ;

    BV_ADD_GEOMETRY_TEST(MRP_Constructors) ;
    BV_ADD_GEOMETRY_TEST(MRP_Methods) ;

    BV_ADD_GEOMETRY_TEST(HorizontalPlane_Constructors) ;
    BV_ADD_GEOMETRY_TEST(HorizontalPlane_Methods) ;

    BV_ADD_GEOMETRY_TEST(Rotation_Converters) ;

    BV_ADD_GEOMETRY_TEST(Transform3D_Constructors) ;
    BV_ADD_GEOMETRY_TEST(Transform3D_Methods) ;

    BV_ADD_GEOMETRY_TEST(Factory_Rotation_Constructors) ;
    BV_ADD_GEOMETRY_TEST(Factory_Translation_Constructors) ;

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV Geometry test suite" ;
    framework::master_test_suite().add( GeometryTestSuite ) ;

  return 0 ;
}

#undef BV_ADD_GEOMETRY_TEST
