#pragma once
#ifndef __BV_Geometry_Tests_Translation_Factory_Tools_hpp__
#define __BV_Geometry_Tests_Translation_Factory_Tools_hpp__

#include <Eigen/Dense>

#include "Geometry/Translation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details
{

void CheckFactoryConverters( const BV::Geometry::TranslatorTypeEnum & translatorTypeEnum,
                             const BV::Geometry::Vector & vect) ;

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Rotation_Factory_Tools_hpp__
