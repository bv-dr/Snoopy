#pragma once
#ifndef __BV_Geometry_Tests_Translation_Factory_Factory_hpp__
#define __BV_Geometry_Tests_Translation_Factory_Factory_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void Factory_Translation_Constructors( void ) ;

} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV

#endif // __BV_Geometry_Tests_Translation_Factory_Factory_hpp__
