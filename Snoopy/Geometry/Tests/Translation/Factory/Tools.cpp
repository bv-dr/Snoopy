#include "Tests/Objects/Tools.hpp"
#include "Tests/Translation/Tools.hpp"
#include "Tests/Translation/Factory/Tools.hpp"
#include "Tests/Translation/Cartesian/Tools.hpp"
#include "Tests/Translation/Horizontal/Tools.hpp"


namespace BV {
namespace Geometry {
namespace Tests {
namespace Details {

void CheckFactoryConverters( const BV::Geometry::TranslatorTypeEnum & translatorTypeEnum,
                             const BV::Geometry::Vector & vect )
{
    using BV::Geometry::Factories::TranslatorsFactory ;

    using BV::Geometry::Translation::ABC ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Translation::Horizontal ;

    Cartesian c( vect ) ;
    Horizontal h( vect ) ;

    // Cartesian
    {
        std::shared_ptr<BV::Geometry::Translation::ABC> p_translator(
            TranslatorsFactory::create(translatorTypeEnum,
                              c )
                              ) ;
        Details::CheckXYZ(p_translator->toVector(), vect) ;
    }
    // Horizontal
    {
        std::shared_ptr<BV::Geometry::Translation::ABC> p_translator(
            TranslatorsFactory::create(translatorTypeEnum,
                              h )
                              ) ;
        Details::CheckXYZ(p_translator->toVector(), vect) ;
    }

}

} // End of namespace Details
} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
