#include <boost/test/unit_test.hpp>
#include <memory>

#include "Tests/Translation/Factory/Factory.hpp"
#include "Geometry/GeometryTypedefs.hpp"
#include "Geometry/Translation.hpp"

#include "Tests/Objects/Tools.hpp"
#include "Tests/Translation/Tools.hpp"
#include "Tests/Translation/ABC/Tools.hpp"
#include "Tests/Translation/Cartesian/Tools.hpp"
#include "Tests/Translation/Horizontal/Tools.hpp"

#include "Tests/Translation/Factory/Tools.hpp"


namespace BV {
namespace Geometry {
namespace Tests {


void Factory_Translation_Constructors(void)
{
    using BV::Geometry::TranslatorTypeEnum ;
    using BV::Geometry::Factories::TranslatorsFactory ;
    using BV::Details::epsilon ;

    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Translation::Horizontal;
    using BV::Geometry::Translation::ABC ;

    BV::Geometry::Vector v0 ;

    Geometry::Vector gV(1., 2., 3.) ;


    std::shared_ptr<BV::Geometry::Translation::ABC> p_c(
            TranslatorsFactory::create(TranslatorTypeEnum::CARTESIAN, gV )
                                                                ) ;
    Details::CheckCoordinates<Cartesian>(*p_c, gV) ;
    Cartesian c(static_cast<Cartesian &> (*p_c)) ;

    // Check all others constructors of Cartesian
    {
        // The constructor from 3 double
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_c2(
                TranslatorsFactory::create(TranslatorTypeEnum::CARTESIAN,
                                    gV.x(), gV.y(), gV.z()
                                    ) ) ;
            Details::CheckCoordinates<Cartesian>(*p_c2, gV) ;
        }

        // The dummy constructor
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_c2(
                TranslatorsFactory::create(TranslatorTypeEnum::CARTESIAN ) ) ;
            Details::CheckCoordinates<Cartesian>(*p_c2, v0) ;
        }

        // The constructor from a Point
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_c2(
                TranslatorsFactory::create(TranslatorTypeEnum::CARTESIAN,
                                    gV.toPoint()
                                    ) ) ;
            Details::CheckCoordinates<Cartesian>(*p_c2, gV) ;
        }

        // The constructor from a Eigen::VectorXd of unknowns
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_c2(
                TranslatorsFactory::create(TranslatorTypeEnum::CARTESIAN,
                                    c.unknowns()
                                    ) ) ;
            Details::CheckCoordinates<Cartesian>(*p_c2, gV) ;
        }

        // The constructor from other Cartesian
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_c2(
                TranslatorsFactory::create(TranslatorTypeEnum::CARTESIAN,
                                    c
                                    ) ) ;
            Details::CheckCoordinates<Cartesian>(*p_c2, gV) ;
        }
    }

    std::shared_ptr<BV::Geometry::Translation::ABC> p_h(
            TranslatorsFactory::create(TranslatorTypeEnum::HORIZONTAL, gV )
                                                                ) ;
    Details::CheckCoordinates<Horizontal>(*p_h, gV) ;
    Horizontal h(static_cast<Horizontal &> (*p_h)) ;

    // Check all others constructors of Horizontal
    {
        // The constructor from Geometry::Vector
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_h2(
                TranslatorsFactory::create(TranslatorTypeEnum::HORIZONTAL,
                                    gV
                                    ) ) ;
            Details::CheckCoordinates<Horizontal>(*p_h2, gV) ;
        }

        // The constructor from 3 double
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_h2(
                TranslatorsFactory::create(TranslatorTypeEnum::HORIZONTAL,
                                    gV.x(), gV.y(), gV.z()
                                    ) ) ;
            Details::CheckCoordinates<Horizontal>(*p_h2, gV) ;
        }

        // The dummy constructor
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_h2(
                TranslatorsFactory::create(TranslatorTypeEnum::HORIZONTAL ) ) ;
            Details::CheckCoordinates<Horizontal>(*p_h2, v0) ;
        }

        // The constructor from Eigen::VectorXd of unknowns
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_h2(
                TranslatorsFactory::create(TranslatorTypeEnum::HORIZONTAL,
                                    h.unknowns()
                                    ) ) ;
            Details::CheckCoordinates<Horizontal>(*p_h2, gV) ;
        }

        // The constructor from other Horizontal
        {
            std::shared_ptr<BV::Geometry::Translation::ABC> p_h2(
                TranslatorsFactory::create(TranslatorTypeEnum::HORIZONTAL,
                                    h
                                    ) ) ;
            Details::CheckCoordinates<Horizontal>(*p_h2, gV) ;
        }
    }
    Details::CheckFactoryConverters(TranslatorTypeEnum::CARTESIAN, gV) ;
    Details::CheckFactoryConverters(TranslatorTypeEnum::HORIZONTAL, gV) ;

}


} // end of namespace Tests
} // end of namespace Geometry
} // end of namespace BV
