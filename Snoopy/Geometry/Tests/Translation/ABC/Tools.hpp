#pragma once
#ifndef __BV_Geometry_Tests_Translation_ABC_Tools_hpp__
#define __BV_Geometry_Tests_Translation_ABC_Tools_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/Translation/ABC.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <typename T>
void CheckCoordinates( const T & t,
                       const BV::Geometry::Vector & vect )
{
    using BV::Details::epsilon ;
    BV::Geometry::Vector calcVec(t.toVector()) ;
    BOOST_CHECK_CLOSE( calcVec.x(), vect.x(), epsilon ) ;
    BOOST_CHECK_CLOSE( calcVec.y(), vect.y(), epsilon ) ;
    BOOST_CHECK_CLOSE( calcVec.z(), vect.z(), epsilon ) ;
}

template <typename T>
void CheckNUnknowns( const T & t )
{

}

template <typename T>
void CheckNConstraints( const T & t )
{

}

template <typename T>
void CheckCoordinates( const T & t1,
                       const T & t2 )
{

}

template <typename T>
void Translate( T & t,
                const double & x, const double & y, const double & z )
{

}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_ABC_Tools_hpp__
