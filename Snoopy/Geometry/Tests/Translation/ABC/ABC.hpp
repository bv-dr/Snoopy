#pragma once
#ifndef __BV_Geometry_Tests_Translation_ABC_ABC_hpp__
#define __BV_Geometry_Tests_Translation_ABC_ABC_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Translation/Cartesian/Tools.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"

#include "Tools/Tests/Tools.hpp"
#include "Tests/Objects/Tools.hpp"
#include "Tests/Translation/Tools.hpp"
#include "Tests/Translation/Horizontal/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

template <typename T>
void ABC_Methods( void )
{
    using BV::Details::epsilon ;

    // Test method nUnkowns
    {
        T t(0., 0., 0.) ;
        t.nUnknowns() ;
        t.nConstraints() ;
        Details::CheckNUnknowns(t) ;
        Details::CheckNConstraints(t) ;
    }

    // Test add Translation with Eigen::Vector3d
    {
        T t(0., 0., 0.) ;
        T expected(0., 0., 0.) ;
        Eigen::Vector3d array(1., 2., 3.) ;
        t.addTranslation(array) ;
        Details::Translate(expected, array(0), array(1), array(2)) ;
        Details::CheckCoordinates(t, expected) ;
    }

    // Test add Translation with Geometry::Vector
    {
        T t(0., 0., 0.) ;
        T expected(0., 0., 0.) ;
        Geometry::Vector vec(1., 2., 3.) ;
        t.addTranslation(vec) ;
        Details::Translate(expected, vec.x(), vec.y(), vec.z()) ;
        Details::CheckCoordinates(t, expected) ;
    }

    // Test add Translation with Geometry::Point
    {
        T t(0., 0., 0.) ;
        T expected(0., 0., 0.) ;
        Geometry::Point pt(1., 2., 3.) ;
        t.addTranslation(pt) ;
        Details::Translate(expected, pt.x(), pt.y(), pt.z()) ;
        Details::CheckCoordinates(t, expected) ;
    }

    // Test subtract Translation with Eigen::Vector3d
    {
        T t(0., 0., 0.) ;
        T expected(0., 0., 0.) ;
        Eigen::Vector3d array(1., 2., 3.) ;
        t.subtractTranslation(array) ;
        Details::Translate(expected, -array(0), -array(1), -array(2)) ;
        Details::CheckCoordinates(t, expected) ;
    }

    // Test subtract Translation with Geometry::Vector
    {
        T t(0., 0., 0.) ;
        T expected(0., 0., 0.) ;
        Geometry::Vector vec(1., 2., 3.) ;
        t.subtractTranslation(vec) ;
        Details::Translate(expected, -vec.x(), -vec.y(), -vec.z()) ;
        Details::CheckCoordinates(t, expected) ;
    }


    // Test subtract Translation with Geometry::Point
    {
        T t(0., 0., 0.) ;
        T expected(0., 0., 0.) ;
        Geometry::Point pt(1., 2., 3.) ;
        t.subtractTranslation(pt) ;
        Details::Translate(expected, -pt.x(), -pt.y(), -pt.z()) ;
        Details::CheckCoordinates(t, expected) ;
    }

    // Test operator * with Geometry::Vector
    {
        T t(1., 2., 3.) ;
        Geometry::Vector mult(3., 2., 1.5) ;
        Geometry::Vector res(t*mult) ;
        Geometry::Vector expected(4., 4., 4.5) ;
        Details::CheckXYZ<Vector>(res, expected) ;
    }

    // Test operator * with Geometry::Point
    {
        T t(1., 2., 3.) ;
        Geometry::Point mult(3., 2., 1.5) ;
        Geometry::Point res(t*mult) ;
        Geometry::Point expected(4., 4., 4.5) ;
        Details::CheckXYZ<Point>(res, expected) ;
    }


    // Test method translation with Eigen::Vector3d
    {
        T t(1., 2., 3.) ;
        Eigen::Vector3d trans(3., 2., 1.5) ;
        Eigen::Vector3d res(t.translation(trans)) ;
        Eigen::Vector3d expected(4., 4., 4.5) ;
        BV::Tools::Tests::CheckEigenVectors(res, expected) ;
    }

    // Test method translation with Geometry::Vector
    {
        T t(1., 2., 3.) ;
        Geometry::Vector trans(3., 2., 1.5) ;
        Geometry::Vector res(t.translation(trans)) ;
        Geometry::Vector expected(4., 4., 4.5) ;
        Details::CheckXYZ<Vector>(res, expected) ;
    }

    // Test method translation with Geometry::Point
    {
        T t(1., 2., 3.) ;
        Geometry::Point trans(3., 2., 1.5) ;
        Geometry::Point res(t.translation(trans)) ;
        Geometry::Point expected(4., 4., 4.5) ;
        Details::CheckXYZ<Point>(res, expected) ;
    }

    // Test method inverse translation with Geometry::Vector
    {
        T t(1., 2., 3.) ;
        Geometry::Vector trans(3., 2., 1.5) ;
        Geometry::Vector res(t.inverseTranslation(trans)) ;
        Geometry::Vector expected(2., 0., -1.5) ;
        Details::CheckXYZ<Vector>(res, expected) ;
    }

    // Test method inverse translation with Geometry::Point
    {
        T t(1., 2., 3.) ;
        Geometry::Point trans(3., 2., 1.5) ;
        Geometry::Point res(t.inverseTranslation(trans)) ;
        Geometry::Point expected(2., 0., -1.5) ;
        Details::CheckXYZ<Point>(res, expected) ;
    }


}

template <typename T>
void TranslatorABC_Methods( void )
{
    // Test operator * with ABC other
    {
        T t1(1., 2., 3.) ;
        T t11(1., 2., 3.) ;
        T t2(4.5, -3., 1.8) ;
        T t22(4.5, -3., 1.8) ;
        T t3(t1*t2) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, 1., 2., 3.) ;
        Details::CheckCoordinates(t3, expected) ;
        Details::CheckCoordinates(t1, t11) ;
        Details::CheckCoordinates(t2, t22) ;
    }

    // Test operator + with ABC other
    {
        T t1(1., 2., 3.) ;
        T t11(1., 2., 3.) ;
        T t2(4.5, -3., 1.8) ;
        T t22(4.5, -3., 1.8) ;
        T t3(t1+t2) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, 1., 2., 3.) ;
        Details::CheckCoordinates(t3, expected) ;
        Details::CheckCoordinates(t1, t11) ;
        Details::CheckCoordinates(t2, t22) ;
    }

    // Test operator - with ABC other
    {
        T t1(1., 2., 3.) ;
        T t11(1., 2., 3.) ;
        T t2(4.5, -3., 1.8) ;
        T t22(4.5, -3., 1.8) ;
        T t3(t1-t2) ;
        T expected(1., 2., 3.) ;
        Details::Translate(expected, -4.5, 3., -1.8) ;
        Details::CheckCoordinates(t3, expected) ;
        Details::CheckCoordinates(t1, t11) ;
        Details::CheckCoordinates(t2, t22) ;
    }

    // Test member method addTranslation with ABC other
    {
        T t1(1., 2., 3.) ;
        T t11(1., 2., 3.) ;
        T t2(4.5, -3., 1.8) ;
        t2.addTranslation(t1) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, 1., 2., 3.) ;
        Details::CheckCoordinates(t2, expected) ;
        Details::CheckCoordinates(t1, t11) ;
    }

    // Test member method addTranslation with double, double, double
    {
        T t1(4.5, -3., 1.8) ;
        t1.addTranslation(1., 2., 3.) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, 1., 2., 3.) ;
        Details::CheckCoordinates(t1, expected) ;
    }

    // Test member method subtractTranslation with ABC other
    {
        T t1(1., 2., 3.) ;
        T t11(1., 2., 3.) ;
        T t2(4.5, -3., 1.8) ;
        t2.subtractTranslation(t1) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, -1., -2., -3.) ;
        Details::CheckCoordinates(t2, expected) ;
        Details::CheckCoordinates(t1, t11) ;
    }

    // Test member method subtractTranslation with double, double, double
    {
        T t1(4.5, -3., 1.8) ;
        t1.subtractTranslation(1., 2., 3.) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, -1., -2., -3.) ;
        Details::CheckCoordinates(t1, expected) ;
    }

    // Test operator -
    {
        T t1(4.5, -3., 1.8) ;
        T t11(4.5, -3., 1.8) ;
        T t2(-t1) ;
        T expected(4.5, -3., 1.8) ;
        Details::Translate(expected, -2.*4.5, -2.*-3., -2.*1.8) ;
        Details::CheckCoordinates(t2, expected) ;
        Details::CheckCoordinates(t1, t11) ;
    }

    // Test member method inverseTranslation from Eigen::Vector3d
    {
        Eigen::Vector3d array1(4.5, -3., 1.8) ;
        T t1(1., 2., 3.) ;
        T t11(1., 2., 3.) ;
        Eigen::Vector3d array2(t1.inverseTranslation(array1)) ;
        Eigen::Vector3d expected(3.5, -5., -1.2) ;
        BV::Tools::Tests::CheckEigenVectors(array2, expected) ;
        Details::CheckCoordinates(t1, t11) ;
    }
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_ABC_ABC_hpp__
