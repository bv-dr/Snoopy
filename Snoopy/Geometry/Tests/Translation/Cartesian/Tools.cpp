#include "Tests/Translation/Cartesian/Tools.hpp"

#include <vector>
#include <boost/test/unit_test.hpp>


namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <>
void CheckCoordinates<Translation::Cartesian>( const Translation::Cartesian & t,
                                               const BV::Geometry::Vector & vect )
{
    using BV::Details::epsilon ;
    BOOST_CHECK_CLOSE( t.x(), vect.x(), epsilon ) ;
    BOOST_CHECK_CLOSE( t.y(), vect.y(), epsilon ) ;
    BOOST_CHECK_CLOSE( t.z(), vect.z(), epsilon ) ;
}

template <>
void CheckNUnknowns<Translation::Cartesian>( const Translation::Cartesian & t )
{
    BOOST_CHECK(t.nUnknowns() == 3 ) ;
}

template <>
void CheckNConstraints<Translation::Cartesian>( const Translation::Cartesian & t )
{
    BOOST_CHECK(t.nConstraints() == 0 ) ;
}

template <>
void CheckCoordinates<Translation::Cartesian>( const Translation::Cartesian & t1,
                       const Translation::Cartesian & t2 )
{
    using BV::Details::epsilon ;
    BOOST_CHECK_CLOSE( t1.x(), t2.x(), epsilon ) ;
    BOOST_CHECK_CLOSE( t1.y(), t2.y(), epsilon ) ;
    BOOST_CHECK_CLOSE( t1.z(), t2.z(), epsilon ) ;
}

template <>
void Translate<Translation::Cartesian>( Translation::Cartesian & t,
                const double & x, const double & y, const double & z )
{
    //std::vector<double> u( t.unknowns() ) ;
    Eigen::VectorXd u(t.unknowns()) ;
    u(0) += x ;
    u(1) += y ;
    u(2) += z ;
    t.unknowns( u ) ;
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
