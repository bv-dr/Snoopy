#pragma once
#ifndef __BV_Geometry_Tests_Translation_Cartesian_Cartesian_hpp__
#define __BV_Geometry_Tests_Translation_Cartesian_Cartesian_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void Cartesian_Constructors( void ) ;
void Cartesian_Methods( void ) ;

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_Cartesian_Cartesian_hpp__
