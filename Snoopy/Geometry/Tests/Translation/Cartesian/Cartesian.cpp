#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Translation/Cartesian/Tools.hpp"
#include "Tests/Translation/Cartesian/Cartesian.hpp"
#include "Tests/Translation/Tools.hpp"
#include "Tests/Translation/ABC/ABC.hpp"
#include "Tests/Translation/ABC/Tools.hpp"
#include "Tests/Objects/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

#include "Geometry/Translation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

void Cartesian_Constructors( void )
{
    // Testing the constructors
    Translation::Cartesian translation1 ;
    Translation::Cartesian translation2( 0., 0., 0. ) ;
    Translation::Cartesian translation3( translation2 ) ;
}

void Cartesian_Methods( void )
{
    using Translation::Cartesian ;
    ABC_Methods<Cartesian>() ;

    TranslatorABC_Methods<Cartesian>() ;

    Details::CheckTypedTranslation<Cartesian>() ;

    {
        Cartesian translation1 ;
        Cartesian translation2( 0., 0., 0. ) ;
        Cartesian translation3( translation2 ) ;

        // Checks the coordinates are the same between the Cartesian objects
        Details::CheckCoordinates( translation1, translation2 ) ;
        Details::CheckCoordinates( translation1, translation3 ) ;

        // Checks that there are no constraints
        BOOST_CHECK( translation1.constraints().size() == 0 ) ;
        BOOST_CHECK( translation2.constraints().size() == 0 ) ;
        BOOST_CHECK( translation3.constraints().size() == 0 ) ;

        // Change the unknowns and check the coordinates have changed
        Details::Translate( translation1, 1., 2., 3. ) ;
        Details::CheckCoordinates( translation1, Vector(1., 2., 3.) ) ;
        Details::Translate( translation2, 10., 20., 30. ) ;
        Details::CheckCoordinates( translation2, Vector(10., 20., 30.) ) ;

        // Combine translations
        translation3 *= translation2 ;
        // They should be equal now because translation3 was 0 before.
        Details:: CheckCoordinates( translation2, translation3 ) ;
        translation3.subtractTranslation(translation2) ;
        translation3.addTranslation(translation2) ;
        Details:: CheckCoordinates( translation2, translation3 ) ;
        translation3.subtractTranslation(10., 20., 30.) ;
        translation3.addTranslation(10., 20., 30.) ;
        Details:: CheckCoordinates( translation2, translation3 ) ;
        translation3.subtractTranslation(Geometry::Vector(10., 20., 30.)) ;
        translation3.addTranslation(Geometry::Vector(10., 20., 30.)) ;
        Details:: CheckCoordinates( translation2, translation3 ) ;
        translation3.subtractTranslation(Geometry::Point(10., 20., 30.)) ;
        translation3.addTranslation(Geometry::Point(10., 20., 30.)) ;
        Details:: CheckCoordinates( translation2, translation3 ) ;

        translation3 *= translation1 ;
        Details::CheckCoordinates( translation3, Vector(11., 22., 33.) ) ;
        translation3 -= translation1 ;
        translation3 += translation1 ;
        Details::CheckCoordinates( translation3, Vector(11., 22., 33.) ) ;
        Details::CheckCoordinates( translation3.toPoint(), Vector(11., 22., 33.) ) ;
        translation3.set(translation3.toPoint() + Geometry::Vector(1., 1., 1.)) ;
        Details::CheckCoordinates( translation3.toPoint(), Vector(12., 23., 34.) ) ;

        // Check affectation
        translation1 = translation3 ;
        Details::CheckCoordinates( translation1, translation3 ) ;

        // Check eigen vectors translation
        Eigen::Vector3d eVect(1., 2., 3.) ;
        Eigen::Vector3d eVect2(translation1 * eVect) ;
        BV::Tools::Tests::CheckEigenVectors(eVect2, Eigen::Vector3d(13., 25., 37.)) ;
        Eigen::Vector3d eVect3(translation1.inverseTranslation(eVect)) ;
        BV::Tools::Tests::CheckEigenVectors(eVect3, Eigen::Vector3d(-11., -21., -31.)) ;
        Eigen::Vector3d eVect4(translation1.translation(eVect)) ;
        BV::Tools::Tests::CheckEigenVectors(eVect4, Eigen::Vector3d(13., 25., 37.)) ;
        // Check geometry vectors translation
        Geometry::Vector vect(1., 2., 3.) ;
        Geometry::Vector vect2(translation1 * vect) ;
        Details::CheckXYZ<Vector>(vect2, Geometry::Vector(13., 25., 37.)) ;
        Geometry::Vector vect3(translation1.inverseTranslation(vect)) ;
        Details::CheckXYZ<Vector>(vect3, Geometry::Vector(-11., -21., -31.)) ;
        Geometry::Vector vect4(translation1.translation(vect)) ;
        Details::CheckXYZ<Vector>(vect4, Geometry::Vector(13., 25., 37.)) ;
        // Check geometry points translation
        Geometry::Point pt(1., 2., 3.) ;
        Geometry::Point pt2(translation1 * pt) ;
        Details::CheckXYZ<Point>(pt2, Geometry::Point(13., 25., 37.)) ;
        Geometry::Point pt3(translation1.inverseTranslation(pt)) ;
        Details::CheckXYZ<Point>(pt3, Geometry::Point(-11., -21., -31.)) ;
        Geometry::Point pt4(translation1.translation(pt)) ;
        Details::CheckXYZ<Point>(pt4, Geometry::Point(13., 25., 37.)) ;
    }
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
