#pragma once
#ifndef __BV_Geometry_Tests_Translation_Converters_Converters_hpp__
#define __BV_Geometry_Tests_Translation_Converters_Converters_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Translation/Tools.hpp"
#include "Tests/Translation/Horizontal/Tools.hpp"
#include "Tests/Objects/Tools.hpp"

#include "Geometry/Point.hpp"
#include "Geometry/Translation.hpp"


namespace BV {
namespace Geometry {
namespace Tests {

void Translation_Converters( void ) ;

template <typename T>
void TemplateToOthers_( void )
{
    // Template to Horizontal
    {
        T t( 1., 2., 3. ) ;
        Translation::Horizontal horTranslation( t.toHorizontal() ) ;
        Translation::Horizontal expected(1., 2., 3.) ;
        Details::CheckCoordinates(horTranslation, expected) ;

        // In this case, the conversion returns returns a copy
        horTranslation += expected ;
        Translation::Horizontal expected2(2., 4., 3.) ;
        Details::CheckCoordinates(horTranslation, expected2) ;
        T expected3( 1., 2., 3. ) ;
        Details::CheckCoordinates(t, expected3) ;

        // Check operator ==
        {
            BOOST_CHECK( expected2 == horTranslation) ;
        }
    }

    // Template to Cartesian
    {
        T t( 1., 2., 3. ) ;
        Translation::Cartesian cartesianTranslation1( t.toCartesian() ) ;
        Translation::Cartesian expected(1., 2., 3.) ;
        Details::CheckCoordinates(cartesianTranslation1, expected) ;

        // In this case, the conversion returns a copy
        cartesianTranslation1 += expected ;
        Translation::Cartesian expected2(2., 4., 6.) ;
        Details::CheckCoordinates(cartesianTranslation1, expected2) ;
        T expected3( 1., 2., 3. ) ;
        Details::CheckCoordinates(t, expected3) ;

        // Check operator ==
        {
            BOOST_CHECK( expected2 == cartesianTranslation1) ;
        }
    }

    // Template to Point
    {
        T t( 1., 2., 3. ) ;
        Geometry::Point point1( t.toPoint() ) ;
        Geometry::Point expected(1., 2., 3.) ;
        Details::CheckXYZ<Geometry::Point>(point1, expected) ;
    }

    // Template to Vector
    {
        T t( 1., 2., 3. ) ;
        Geometry::Vector vect1( t.toVector() ) ;
        Geometry::Vector expected(1., 2., 3.) ;
        Details::CheckXYZ<Geometry::Vector>(vect1, expected) ;
    }
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_Converters_Converters_hpp__
