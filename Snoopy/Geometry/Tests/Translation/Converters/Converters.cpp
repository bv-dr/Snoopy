
#include "Tests/Translation/Converters/Converters.hpp"

#include "Geometry/Translation.hpp"

namespace BV {
namespace Geometry {
namespace Tests {


void Translation_Converters( void )
{
    // Testing the converters
    TemplateToOthers_<Geometry::Translation::Horizontal>() ;
    TemplateToOthers_<Geometry::Translation::Cartesian>() ;
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
