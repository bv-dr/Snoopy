#pragma once
#ifndef __BV_Geometry_Tests_Translation_Tools_hpp__
#define __BV_Geometry_Tests_Translation_Tools_hpp__

#include "Cartesian/Tools.hpp"

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Translation/Horizontal/Tools.hpp"
#include "Geometry/Translation.hpp"
#include "Geometry/GeometryTypedefs.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <typename T>
void CheckTypedTranslation( void )
{
    using BV::Details::epsilon ;


    // Test operator *=
    {
        T t1( 1., 2., 30. ) ;
        T t2( 0., 0., 30. ) ;
        t1 *= t2 ;
        T expected( 1., 2., 30. ) ;
        Details::Translate(expected, 0., 0., 30.) ;
        Details::CheckCoordinates(t1, expected) ;
    }

    // Test operator +=
    {
        T t1( 1., 2., 30. ) ;
        T t2( 0., 0., 30. ) ;
        t1 += t2 ;
        T expected( 1., 2., 30. ) ;
        Details::Translate(expected, 0., 0., 30.) ;
        Details::CheckCoordinates(t1, expected) ;
    }

    // Test operator -=
    {
        T t1( 1., 2., 30. ) ;
        T t2( 0., 0., 30. ) ;
        t1 -= t2 ;
        T expected( 1., 2., 30. ) ;
        Details::Translate(expected, -0., -0., -30.) ;
        Details::CheckCoordinates(t1, expected) ;
    }

    // Test operator *
    {
        T t1( 1., 2., 30. ) ;
        Eigen::Vector3d a( 0., 0., 30. ) ;
        Eigen::Vector3d array(t1 * a) ;
        Eigen::Vector3d expectedArray(1., 2., 60.) ;
    }

    // Test operator =
    {
        T t0( 1., 2., 30. ) ;
        T t1( 1., 2., 30. ) ;
        T t2 ;
        t2 = t1 ;
        Details::CheckCoordinates(t1, t2) ;

        T expected(1., 2., 30.) ;
        Details::Translate(expected, 1., 2., 30.) ;
        t2 *= t1 ;
        Details::CheckCoordinates(t0, t1) ;
        Details::CheckCoordinates(t2, expected) ;
    }

    // Test member method getInversed
    {
        T t0( 1., 2., 30. ) ;
        T t1( 1., 2., 30. ) ;
        T t3(t1.getInversed()) ;
        T expected( 1., 2., 30. ) ;
        Details::Translate(expected, -2.*1., -2.*2., -2.*30.) ;
        Details::CheckCoordinates(t0, t1) ;
        Details::CheckCoordinates(t3, expected) ;
    }

    // Test member method x(), y(), z()
    {

        T t1( 1., 2., 30. ) ;
        double x(t1.x()) ;
        double y(t1.y()) ;
        double z(t1.z()) ;
        BOOST_CHECK_CLOSE(x, 1., epsilon) ;
        BOOST_CHECK_CLOSE(y, 2., epsilon) ;
        BOOST_CHECK_CLOSE(z, 30., epsilon) ;

        double & rx(t1.x()) ;
        double & ry(t1.y()) ;
        double & rz(t1.z()) ;
        BOOST_CHECK_CLOSE(rx, 1., epsilon) ;
        BOOST_CHECK_CLOSE(ry, 2., epsilon) ;
        BOOST_CHECK_CLOSE(rz, 30., epsilon) ;

        rx = 2. ;
        ry = -1. ;
        rz = 5. ;
        BOOST_CHECK_CLOSE(t1.x(), 2., epsilon) ;
        BOOST_CHECK_CLOSE(t1.y(), -1., epsilon) ;
        BOOST_CHECK_CLOSE(t1.z(), 5., epsilon) ;

        t1.x() = 8. ;
        t1.y() = -9. ;
        t1.z() = 0.5 ;
        BOOST_CHECK_CLOSE(t1.x(), 8., epsilon) ;
        BOOST_CHECK_CLOSE(t1.y(), -9., epsilon) ;
        BOOST_CHECK_CLOSE(t1.z(), 0.5, epsilon) ;
    }

    // Test member method set (from a point)
    {
        T t1( 1., 2., 30. ) ;
        T expected( 8., -3., 4.5 ) ;
        Geometry::Point pt(8., -3., 4.5) ;
        t1.set(pt) ;
        Details::CheckCoordinates(t1, expected) ;
    }

    // Test member method unknows
    {
        T t1( 1., 2., 30. ) ;
        Eigen::Vector3d expected(1., 2., 30.) ;
        Eigen::VectorXd array(t1.unknowns()) ;
        for (unsigned i=0; i<array.size(); ++i)
        {
            BOOST_CHECK_CLOSE(array(i),  expected(i), epsilon) ;
        }
        Eigen::Vector3d array2 ;
        array2 << 5., 7., 3.2 ;
        t1.unknowns(array2.head(t1.nUnknowns())) ;

        T expected2( 0., 0., 0. ) ;
        Details::Translate(expected2, 5., 7., 3.2) ;
        Details::CheckCoordinates(t1, expected2) ;
    }

    // Test member method constraints
    {
        T t1 ;
        Eigen::VectorXd ct1(t1.constraints()) ;
        BOOST_CHECK(ct1.size() == t1.nConstraints()) ;
    }

}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_Tools_hpp__
