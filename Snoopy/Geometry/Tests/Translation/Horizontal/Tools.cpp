#include <boost/test/unit_test.hpp>

#include "Tests/Translation/Horizontal/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <>
void CheckCoordinates<Translation::Horizontal>( const Translation::Horizontal & t,
                                                const BV::Geometry::Vector & vect )
{
    using BV::Details::epsilon ;
    BOOST_CHECK_CLOSE( t.x(), vect.x(), epsilon ) ;
    BOOST_CHECK_CLOSE( t.y(), vect.y(), epsilon ) ;
}

template <>
void CheckNUnknowns<Translation::Horizontal>( const Translation::Horizontal & t )
{
    BOOST_CHECK(t.nUnknowns() == 2 ) ;
}

template <>
void CheckNConstraints<Translation::Horizontal>( const Translation::Horizontal & t )
{
    BOOST_CHECK(t.nConstraints() == 0 ) ;
}

template <>
void CheckCoordinates<Translation::Horizontal>( const Translation::Horizontal & t1,
                                                const Translation::Horizontal & t2 )
{
    using BV::Details::epsilon ;
    BOOST_CHECK_CLOSE( t1.x(), t2.x(), epsilon ) ;
    BOOST_CHECK_CLOSE( t1.y(), t2.y(), epsilon ) ;
}

template <>
void Translate<Translation::Horizontal>( Translation::Horizontal & t,
                                         const double & x, const double & y,
                                         const double & z )
{
    //std::vector<double> u( t.unknowns() ) ;
    Eigen::VectorXd u(t.unknowns()) ;
    u(0) += x ;
    u(1) += y ;
    t.unknowns( u ) ;
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
