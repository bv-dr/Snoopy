#pragma once
#ifndef __BV_Geometry_Tests_Translation_Horizontal_Horizontal_hpp__
#define __BV_Geometry_Tests_Translation_Horizontal_Horizontal_hpp__

namespace BV {
namespace Geometry {
namespace Tests {

void HorizontalTranslation_Constructors (void ) ;
void HorizontalTranslation_Methods( void ) ;

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_Horizontal_Horizontal_hpp__
