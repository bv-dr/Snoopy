#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Tests/Translation/Tools.hpp"
#include "Tests/Translation/Horizontal/Horizontal.hpp"
#include "Tests/Translation/Horizontal/Tools.hpp"
#include "Tests/Translation/ABC/ABC.hpp"
#include "Tests/Translation/ABC/Tools.hpp"

#include "Geometry/Translation.hpp"


namespace BV {
namespace Geometry {
namespace Tests {

void HorizontalTranslation_Constructors( void )
{
    // Testing the constructors

    // Construct from values
    {
        Translation::Horizontal translation1( 0., 0., 30. ) ;
    }

    // Default constructor
    {
        Translation::Horizontal translation2 ;
    }

    // Construct from Eigen::VectorXd
    {
        Eigen::VectorXd array1(Eigen::VectorXd::Zero(2)) ;
        array1(0) = 1.5 ;
        array1(1) = 2.5 ;
        Translation::Horizontal translation3( array1 ) ;
    }

    // Construct from other
    {
        Translation::Horizontal translation1( 0., 0., 30. ) ;
        Translation::Horizontal translation4( translation1 ) ;
    }
}

void HorizontalTranslation_Methods( void )
{
    using Translation::Horizontal ;
    ABC_Methods<Horizontal>() ;

    TranslatorABC_Methods<Horizontal>() ;

    Details::CheckTypedTranslation<Horizontal>() ;


    {
        Horizontal translation1 ;
        Horizontal translation2( 0., 0., 30. ) ;
        Horizontal translation3( translation2 ) ;

        // Checks the coordinates are the same between the Cartesian objects
        Details::CheckCoordinates( translation1, translation2 ) ;
        Details::CheckCoordinates( translation1, translation3 ) ;

        // Checks that there are no constraints
        BOOST_CHECK( translation1.constraints().size() == 0 ) ;
        BOOST_CHECK( translation2.constraints().size() == 0 ) ;
        BOOST_CHECK( translation3.constraints().size() == 0 ) ;

        // Change the unknowns and check the coordinates have changed
        Details::Translate( translation1, 1., 2. , 5.) ;
        Details::CheckCoordinates( translation1, Vector(1., 2., 0.) ) ;
        Details::Translate( translation2, 10., 20., 12.) ;
        Details::CheckCoordinates( translation2, Vector(10., 20., 30.) ) ;
    
        // Combine translations
        translation3 *= translation2 ;
        // They should be equal now because translation3 was 0 before.
        Details:: CheckCoordinates( translation3, Vector(10., 20., 30.) ) ;

        translation3 *= translation1 ;
        Details::CheckCoordinates( translation3, Vector(11., 22., 30.) ) ;
    
        // Check affectation
        translation1 = translation3 ;
        Details::CheckCoordinates( translation1, translation3 ) ;
    }
}

} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV
