#pragma once
#ifndef __BV_Geometry_Tests_Translation_Horizontal_Tools_hpp__
#define __BV_Geometry_Tests_Translation_Horizontal_Tools_hpp__

#include <boost/test/unit_test.hpp>

#include "Geometry/Translation.hpp"
#include "Geometry/GeometryTypedefs.hpp"
#include "Tests/Translation/ABC/Tools.hpp"

namespace BV {
namespace Geometry {
namespace Tests {

namespace Details {

template <>
void CheckCoordinates<Translation::Horizontal>( const Translation::Horizontal & t,
                                                const BV::Geometry::Vector & vect ) ;

template <>
void CheckNUnknowns<Translation::Horizontal>( const Translation::Horizontal & t ) ;

template <>
void CheckNConstraints<Translation::Horizontal>( const Translation::Horizontal & t ) ;

template <>
void CheckCoordinates<Translation::Horizontal>( const Translation::Horizontal & t1,
                                                const Translation::Horizontal & t2 ) ;


template <>
void Translate<Translation::Horizontal>( Translation::Horizontal & t,
                                         const double & x, const double & y,
                                         const double & z ) ;

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Geometry
} // End of namespace BV

#endif // __BV_Geometry_Tests_Translation_Horizontal_Tools_hpp__
