#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy

from Geometry import *

def isClose(val1, val2, eps=1.e-8):
    if type(val1) in (Point, Vector):
        v1 = val1.get()
    else:
        v1 = val1
    if type(val2) in (Point, Vector):
        v2 = val2.get()
    else:
        v2 = val2
    return numpy.all(numpy.abs(v1 - v2) < eps)

def _toMatrix(trans, rot):
    tmp = numpy.zeros((4, 4), dtype=float)
    tmp[:3, :3] = rot.toRotationMatrix().getMatrix()
    tmp[:3, 3] = trans.center.get()
    tmp[3, 3] = 1.
    return tmp

def testTransform3DInitialisation():
    transform = Transform3D()
    # problem of conversion with CPP typedef
    # typedef Eigen::Matrix<double, 4, 4, Eigen::DontAlign> EigenMatrix4d ;
    assert isClose(transform.getMatrix(), numpy.identity(4))
    trans = Cartesian(1., 2., 3.)
    rot = AxisAndAngle(Vector(0., 0., 1.), numpy.pi/2.)
    transform2 = Transform3D(trans, rot)
    assert isClose(transform2.getMatrix(), _toMatrix(trans, rot))

def testTransfor3DComposition():
    transform = Transform3D()
    trans = Cartesian(1., 2., 3.)
    rot = AxisAndAngle(Vector(0., 0., 1.), numpy.pi/2.)
    transform2 = Transform3D(trans, rot)
    transform *= transform2
    assert isClose(transform.getMatrix(), _toMatrix(trans, rot))
    transform3 = transform * transform2
    rot2 = AxisAndAngle(rot)
    rot2.addOtherRotationAtLeft(rot)
    trans2 = Cartesian(-1., 3., 6.)
    assert isClose(transform3.getMatrix(), _toMatrix(trans2, rot2))
    transform4 = transform3 * transform2.inverse()
    assert isClose(transform4.getMatrix(), _toMatrix(trans, rot))

def testTransform3DCompositionTranslatorRotator():
    transform = Transform3D()
    trans = Cartesian(1., 2., 3.)
    rot = AxisAndAngle()
    transform *= trans
    assert isClose(transform.getMatrix(), _toMatrix(trans, rot))
    rot2 = AxisAndAngle(Vector(0., 0., 1.), numpy.pi/2.)
    transform *= rot2
    assert isClose(transform.getMatrix(), _toMatrix(trans, rot2))
    transform2 = transform * trans
    trans2 = Cartesian(-1., 3., 6.)
    assert isClose(transform2.getMatrix(), _toMatrix(trans2, rot2))

def testTransform3DChangeOfBasis():
    trans = Cartesian(1., 2., 3.)
    rot = AxisAndAngle(Vector(0., 0., 1.), numpy.pi/2.)
    transform = Transform3D(trans, rot)
    # Check points
    pt = Point(1., 1., 1.)
    ptGlob = transform * pt
    assert isClose(ptGlob, Point(0., 3., 4.))
    ptLoc = transform.inverse() * pt
    assert isClose(ptLoc, Point(-1., 0., -2.))
    # Check vectors
    vect = Vector(1., 1., 1.)
    vectGlob = transform * vect
    assert isClose(vectGlob, Vector(-1., 1., 1.))
    vectLoc = transform.inverse() * vect
    assert isClose(vectLoc, Vector(1., -1., 1.))
    
def testTransform3DGetTranslatorRotator():
    trans = Cartesian(1., 2., 3.)
    rot = AxisAndAngle(Vector(0., 0., 1.), numpy.pi/2.)
    transform = Transform3D(trans, rot)
    # Cartesian
    T = transform.getTranslator()
    assert isClose(T.center, trans.center)
    # RotationMatrix
    R = transform.getRotationMatrix()
    assert isClose(R.getMatrix(), rot.toRotationMatrix().getMatrix())
    # AxisAndAngle
    aa = transform.getAxisAndAngle()
    assert isClose(aa.toRotationMatrix().getMatrix(), rot.toRotationMatrix().getMatrix())
    # Quaternion
    q = transform.getQuaternion()
    assert isClose(q.toRotationMatrix().getMatrix(), rot.toRotationMatrix().getMatrix())
    # MRP
    mrp = transform.getMRP()
    assert isClose(mrp.toRotationMatrix().getMatrix(), rot.toRotationMatrix().getMatrix())
    # FIXME check others ?

