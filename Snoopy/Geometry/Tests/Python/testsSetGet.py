import numpy

import Geometry

def _isClose(val1, val2, eps=1.e-8):
    return numpy.all(numpy.abs(val2 - val1) < eps)

def testEulerAngles():
    u0 = numpy.array(( 1., 2., 3. ), dtype=float)
    e = Geometry.EulerAngles_ZYX_i(u0[0],u0[1],u0[2])
    r0 = e.unknowns
    assert( len( r0 ) == e.nUnknowns() )
    assert( _isClose(r0, u0) )
    u1 = numpy.array(( 0.4, 0.5, 0.6 ), dtype=float)
    e.unknowns = u1
    r1 = e.unknowns
    assert( len( r1 ) == e.nUnknowns() )
    assert( _isClose(r1, u1) )

