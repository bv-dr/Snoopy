#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

sharedCodeDir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..'))
if sharedCodeDir not in sys.path:
    sys.path.insert(0, sharedCodeDir)

from SharedCodeLibPathManager import setPathTo
setPathTo('Geometry')