#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nose.tools import raises
import numpy

from Geometry import Vector, Point
from Geometry import RotationMatrix
from Geometry import Quaternion
from Geometry import BasisVectors
from Geometry import AxisAndAngle
from Geometry import MRP
from Geometry import EulerAngles_XYX_i as EulerAngles_XYX
from Geometry import EulerAngles_XYZ_i as EulerAngles_XYZ
from Geometry import EulerAngles_XZX_i as EulerAngles_XZX
from Geometry import EulerAngles_XZY_i as EulerAngles_XZY
from Geometry import EulerAngles_YXY_i as EulerAngles_YXY
from Geometry import EulerAngles_YXZ_i as EulerAngles_YXZ
from Geometry import EulerAngles_YZX_i as EulerAngles_YZX
from Geometry import EulerAngles_YZY_i as EulerAngles_YZY
from Geometry import EulerAngles_ZXY_i as EulerAngles_ZXY
from Geometry import EulerAngles_ZXZ_i as EulerAngles_ZXZ
from Geometry import EulerAngles_ZYX_i as EulerAngles_ZYX
from Geometry import EulerAngles_ZYZ_i as EulerAngles_ZYZ


def _get2DRotationMatrix(angle):
    R = numpy.zeros((3, 3), dtype=float)
    R[0, 0] = numpy.cos(angle)
    R[0, 1] = -numpy.sin(angle)
    R[0, 2] = 0.
    R[1, 0] = numpy.sin(angle)
    R[1, 1] = numpy.cos(angle)
    R[1, 2] = 0.
    R[2, 0] = 0.
    R[2, 1] = 0.
    R[2, 2] = 1.
    return R

def _isClose(val1, val2, eps=1.e-8):
    if type(val1) in (Point, Vector):
        v1 = val1.get()
    elif isinstance(val1, RotationMatrix):
        v1 = val1.getMatrix()
    else:
        v1 = val1
    if type(val2) in (Point, Vector):
        v2 = val2.get()
    elif isinstance(val2, RotationMatrix):
        v2 = val2.getMatrix()
    else:
        v2 = val2
    return numpy.all(numpy.abs(v2 - v1) < eps)

def _angle02PI(angle):
    angle = numpy.mod(angle, 2.*numpy.pi)
    if angle < 0.:
        angle += 2. * numpy.pi
    return angle

def _angleMinusPi_Pi(angle):
    if( _isClose(numpy.abs(numpy.mod(angle, 2.0*numpy.pi)) , 0.) or _isClose(numpy.abs(angle), 0.) ):
        return 0.
    ang = numpy.mod(angle, 2.0*numpy.pi)
    if(ang>numpy.pi):
        ang -= 2.*numpy.pi
    elif(ang<-numpy.pi):
        ang+= 2.*numpy.pi
    return ang
    # elif(angle>0.):
    #     return numpy.mod(angle+numpy.pi, 2.0*numpy.pi)-numpy.pi
    # else:
    #     return numpy.mod(angle-numpy.pi, 2.0*numpy.pi)+numpy.pi

def _singularityDetector(angle0, angle1, angle2, convention, epsilon=1.e-8):
    # This function detects if a gimbal lock occurs for a specific Euler angle convention
    # Note that the checks are valid for both intrinsic and extrinsic convention.
    # For an Euler angle convention U-V-W, the rotation matrix for intrinsic and extrinsic are the following
    # intrinsic: R = RU(theta_i) * RV(theta_j) * RW(theta_k)
    # extrinsic: R = RW(theta_k) * RV(theta_j) * RU(theta_i)

    angle1_MinusPi_Pi = _angleMinusPi_Pi(angle1)

    # If assymetric convention (i.e. in U-V-W, U and W are different)
    # then the singularity (gimbal lock) occurs when theta_j = +-90Â°
    if(convention.getU() != convention.getW()):
        return numpy.abs(numpy.abs(angle1_MinusPi_Pi) - numpy.pi/2.) < epsilon
    # If assymetric convention (i.e. in U-V-W, U and W are different)
    # then the singularity (gimbal lock) occurs when theta_j = 0Â°,+-180Â°
    elif(convention.getU() == convention.getW()):
        return ( (numpy.abs(numpy.abs(angle1_MinusPi_Pi) - numpy.pi) < epsilon) or (numpy.abs(angle1_MinusPi_Pi) < epsilon) )
    else:
        raise "The convention passed to detect the singularity in Euler angles does not represent an Euler Angle valid convention."
##############
def _eulerCheckCoordinates(angle00, angle01, angle02, angle10, angle11, angle12, convention, epsilon=1.e-8):
    def CheckRotationMatrices(R1, R2):
        for i in xrange(3):
            for j in xrange(3):
                assert _isClose(R1[i, j], R2[i, j], 1.e-5)
    is1Singular = _singularityDetector(angle00, angle01, angle02, convention)
    is2Singular = _singularityDetector(angle10, angle11, angle12, convention)


    theta_i1_NearPi = ( numpy.abs( numpy.abs(angle00) - numpy.pi ) < epsilon ) or  ( numpy.abs(angle00) < epsilon )
    theta_i1_NearHalfPi = ( numpy.abs( numpy.abs(angle00) - numpy.pi/2. ) < epsilon )

    theta_i2_NearPi = ( numpy.abs( numpy.abs(angle10) - numpy.pi ) < epsilon ) or  ( numpy.abs(angle10) < epsilon )
    theta_i2_NearHalfPi = ( numpy.abs( numpy.abs(angle10) - numpy.pi/2. ) < epsilon )

    theta_k1_NearPi = ( numpy.abs( numpy.abs(angle02) - numpy.pi ) < epsilon ) or  ( numpy.abs(angle02) < epsilon )
    theta_k1_NearHalfPi = ( numpy.abs( numpy.abs(angle02) - numpy.pi/2. ) < epsilon )

    theta_k2_NearPi = ( numpy.abs( numpy.abs(angle12) - numpy.pi ) < epsilon ) or  ( numpy.abs(angle12) < epsilon )
    theta_k2_NearHalfPi = ( numpy.abs( numpy.abs(angle12) - numpy.pi/2. ) < epsilon )

    specialCase1 =  (theta_i1_NearPi and theta_k1_NearPi) or (theta_i2_NearPi and theta_k2_NearPi)
    specialCase2 =  (theta_i1_NearHalfPi and theta_k1_NearHalfPi) or (theta_i2_NearHalfPi and theta_k2_NearHalfPi)
    specialCase3 =  (theta_i1_NearHalfPi and theta_k1_NearPi) or (theta_i2_NearHalfPi and theta_k2_NearPi)
    specialCase4 =  (theta_i1_NearPi and theta_k1_NearHalfPi) or (theta_i2_NearPi and theta_k2_NearHalfPi)

    # if singularity (gimbal lock)
    if(is1Singular or is2Singular):
        checkGimbalLockSecondComponent = numpy.abs(numpy.abs(angle01)-numpy.abs(angle11))<epsilon
        assert checkGimbalLockSecondComponent
        
        # Check the sum/diff angles
        sum1 = _angleMinusPi_Pi(angle00 + angle02)
        sum2 = _angleMinusPi_Pi(angle10 + angle12)
        diff1 = _angleMinusPi_Pi(angle00 - angle02)
        diff2 = _angleMinusPi_Pi(angle10 - angle12)
        checkGimbalLockSum = ( numpy.abs( _angleMinusPi_Pi(sum1 - sum2) ) < epsilon) or \
                                 ( numpy.abs( _angleMinusPi_Pi(sum1 - diff2) ) < epsilon) or \
                                 ( numpy.abs( _angleMinusPi_Pi(diff1 - sum2) ) < epsilon) or \
                                 ( numpy.abs( _angleMinusPi_Pi(diff1 - diff2) ) < epsilon)
        assert checkGimbalLockSum
    # Special angles for ( theta_i = 0Â°,+-180Â° and theta_k = 0Â°,+-180Â°) or ( theta_i = +-90Â° and theta_k = +-90Â°)
    # Special angles for ( theta_i = +-90Â° and theta_k = 0Â°,+-180Â°) or ( theta_i = 0Â°,+-180Â° and theta_k = +-90Â°)
    elif(specialCase1 or specialCase2 or specialCase3 or specialCase4 ):
        sum1 = _angleMinusPi_Pi(angle00 + angle01 + angle02)
        sum2 = _angleMinusPi_Pi(angle10 + angle11 + angle12)
        specialCasesCheck = ( numpy.abs(_angleMinusPi_Pi(sum1 - sum2) ) < epsilon ) \
                                  or ( numpy.abs(_angleMinusPi_Pi(sum1 - sum2) ) - numpy.pi < epsilon )
        assert specialCasesCheck
    else:
        equalityCheck = \
                            (numpy.abs(angle00-angle10) < epsilon ) \
                            and (numpy.abs(angle01-angle11) < epsilon ) \
                            and (numpy.abs(angle02-angle12) < epsilon ) 
        if(equalityCheck):
            assert equalityCheck
        # it seems that there is a special case when the first angle theta_i (with R = RU(theta_i) * RV(theta_j) * RW(theta_k) )
        # is equal to numpy.pi, the unknowns can be different, but represents the same matrix
        # We check it !
        # Maybe there is a specific relation between the angles sum, but I don't know it...
        elif( ( (numpy.abs(angle00)-numpy.pi <epsilon) and (numpy.abs(angle10) <epsilon) )
                 or ( (numpy.abs(angle00) <epsilon) and (numpy.abs(angle10) - numpy.pi <epsilon) )
               ):
            e1 = EulerAngles_XYX(angle00, angle01, angle02)
            e2 = EulerAngles_XYX(angle10, angle11, angle12)
            CheckRotationMatrices(e1.getMatrix(), e2.getMatrix())
        # All other cases we check by matrix
        else:
            e1 = EulerAngles_XYX(angle00, angle01, angle02)
            e2 = EulerAngles_XYX(angle10, angle11, angle12)
            CheckRotationMatrices(e1.getMatrix(), e2.getMatrix())
###############

def testRotationMatrix():
    def CheckRotationMatrices(R1, R2):
        for i in xrange(3):
            for j in xrange(3):
                assert _isClose(R1.get(i, j), R2.get(i, j))
    def CheckRotationMatrixAccordingToNumpy(R1, R2):
        for i in xrange(3):
            for j in xrange(3):
                assert _isClose(R1.get(i, j), R2[i, j])
    rot = _get2DRotationMatrix(numpy.radians(20.))
    # Initialisation
    R = RotationMatrix(rot)
    R1 = RotationMatrix(R)
    CheckRotationMatrices(R, R1)
    R2 = RotationMatrix(rot[0, 0], rot[1, 0], rot[2, 0],
                        rot[0, 1], rot[1, 1], rot[2, 1],
                        rot[0, 2], rot[1, 2], rot[2, 2])
    CheckRotationMatrices(R, R2)
    R3 = RotationMatrix()
    # Special methods
    CheckRotationMatrixAccordingToNumpy(R3, numpy.identity(3))
    CheckRotationMatrixAccordingToNumpy(R.transpose(), rot.transpose())
    assert _isClose(R.determinant(), numpy.linalg.det(rot))
    # operators

@raises(Exception)
def testIllFormedRotationMatrix():
    R = RotationMatrix(1., 0., 0.,
                       0., 1., 0.,
                       0., 1., 0.)

@raises(Exception)
def testIllFormedRotationMatrix2():
    R = RotationMatrix(2., 0., 0.,
                       0., 1., 0.,
                       0., 0., 1.)

@raises(Exception)
def testIllFormedRotationMatrix3():
    R = RotationMatrix(1., 0., 0.,
                       0., 0., 1.,
                       0., 1., 0.)

def testQuaternion():
    def CheckCoordinates(q, w, x, y, z):
        assert ((_isClose(q.w, w)
                 and _isClose(q.x, x)
                 and _isClose(q.y, y)
                 and _isClose(q.z, z))
                or (_isClose(q.w, -w)
                    and _isClose(q.x, -x)
                    and _isClose(q.y, -y)
                    and _isClose(q.z, -z)))
    def CheckCoordinatesQuaternions(q1, q2):
        assert ((_isClose(q1.w, q2.w)
                 and _isClose(q1.x, q2.x)
                 and _isClose(q1.y, q2.y)
                 and _isClose(q1.z, q2.z))
                or (_isClose(q1.w, -q2.w)
                    and _isClose(q1.x, -q2.x)
                    and _isClose(q1.y, -q2.y)
                    and _isClose(q1.z, -q2.z)))
    q = Quaternion()
    CheckCoordinates(q, 1., 0., 0., 0.)
    q1 = Quaternion(numpy.pi, 1., 0., 0.)
    CheckCoordinates(q1, numpy.pi, 1., 0., 0.)
    q2 = Quaternion(q1)
    CheckCoordinatesQuaternions(q1, q2)
    vals = numpy.array([numpy.pi, 1., 0., 0.], dtype=float)
    q3 = Quaternion(vals)
    CheckCoordinatesQuaternions(q1, q3)
    q3.unknowns = numpy.array([2.*numpy.pi, 1., 0., 0.], dtype=float)
    assert _isClose(q3.w - q2.w, numpy.pi)
    R = RotationMatrix(_get2DRotationMatrix(numpy.radians(20.)))
    q4 = Quaternion(R)
    q5 = Quaternion(q4)
    CheckCoordinatesQuaternions(q4, q5)
    # Special methods
    q6 = Quaternion(1.2, 156., 123857., 156.)
    q7 = q6.normalised()
    assert _isClose(q7.norm(), 1.)
    assert q6.norm() > 1.
    q6.normalise()
    assert _isClose(q6.norm(), 1.)
    
def testAxisAndAngle():
    def CheckAxisAndAngle(a1, a2):
        assert _isClose(a1.axis, a2.axis)
        assert _isClose(a1.angle, a2.angle)
    def CheckAxisAndAngleFromAngle(a, angle):
        factor = 1.
        if _isClose(a.angle, -angle):
            factor = -1.
        assert _isClose(factor*a.angle, angle)
        if abs(angle) < 1.e-8:
            assert _isClose(a.axis, numpy.array((1., 0., 0.), dtype=float))
            assert _isClose(a.unknowns, numpy.array((1., 0., 0., angle),
                                                    dtype=float))
        else:
            assert _isClose(factor*a.axis, numpy.array((0., 0., 1.),
                                                       dtype=float))
            assert _isClose(factor*a.unknowns, numpy.array((0., 0., 1., angle),
                                                           dtype=float))
    aa = AxisAndAngle()
    aa2 = AxisAndAngle(aa)
    CheckAxisAndAngle(aa, aa2)
    angle = numpy.radians(20.)
    aa.unknowns = numpy.array((0., 0., 1., angle), dtype=float)
    aa3 = AxisAndAngle(Vector(0., 0., 1.), angle)
    CheckAxisAndAngle(aa, aa3)
    assert _isClose(aa2.unknowns, numpy.array((1., 0., 0., 0.), dtype=float))
    R = RotationMatrix(_get2DRotationMatrix(angle))
    aa4 = AxisAndAngle(R)
    CheckAxisAndAngleFromAngle(aa4, angle)

def testBasisVectors():
    bv = BasisVectors()
    assert _isClose(bv.d1, numpy.array((1., 0., 0.), dtype=float))
    assert _isClose(bv.d2, numpy.array((0., 1., 0.), dtype=float))
    assert _isClose(bv.d3, numpy.array((0., 0., 1.), dtype=float))
    bv2 = BasisVectors(Vector(0., 1., 0.),
                       Vector(0., 0., 1.),
                       Vector(1., 0., 0.))
    assert _isClose(bv2.d1, numpy.array((0., 1., 0.), dtype=float))
    assert _isClose(bv2.d2, numpy.array((0., 0., 1.), dtype=float))
    assert _isClose(bv2.d3, numpy.array((1., 0., 0.), dtype=float))
    angle = numpy.radians(5.)
    R = RotationMatrix(_get2DRotationMatrix(angle))
    bv3 = BasisVectors(R)
    mat = R.getMatrix()
    assert _isClose(bv3.d1,
                    numpy.array((mat[0, 0], mat[1, 0], mat[2, 0]), dtype=float))
    assert _isClose(bv3.d2,
                    numpy.array((mat[0, 1], mat[1, 1], mat[2, 1]), dtype=float))
    assert _isClose(bv3.d3,
                    numpy.array((mat[0, 2], mat[1, 2], mat[2, 2]), dtype=float))
    bv4 = BasisVectors(mat[0, 0], mat[1, 0], mat[2, 0],
                       mat[0, 1], mat[1, 1], mat[2, 1],
                       mat[0, 2], mat[1, 2], mat[2, 2])
    assert _isClose(bv4.d1,
                    numpy.array((mat[0, 0], mat[1, 0], mat[2, 0]), dtype=float))
    assert _isClose(bv4.d2,
                    numpy.array((mat[0, 1], mat[1, 1], mat[2, 1]), dtype=float))
    assert _isClose(bv4.d3,
                    numpy.array((mat[0, 2], mat[1, 2], mat[2, 2]), dtype=float))

def testEulerAngles():
    ea = EulerAngles_XYZ()
    assert _isClose(numpy.array((0., 0., 0.),dtype=float),
                    ea.unknowns)
    assert _isClose(ea.alpha, 0.)
    assert _isClose(ea.beta, 0.)
    assert _isClose(ea.gamma, 0.)
    ea2 = EulerAngles_XYZ(0., 0., 0.)
    assert _isClose(ea2.unknowns, ea.unknowns)
    assert _isClose(ea2.alpha, 0.)
    assert _isClose(ea2.beta, 0.)
    assert _isClose(ea2.gamma, 0.)
    # FIXME if we set all angles, we don't get them back...
    comp = numpy.array((0.5*numpy.pi, 0.4*numpy.pi, 0.33*numpy.pi),
                       dtype=float)
    ea3 = EulerAngles_XYZ(comp[0], comp[1], comp[2])
    assert _isClose(comp, ea3.unknowns)
    assert _isClose(ea3.alpha, comp[0])
    assert _isClose(ea3.beta, comp[1])
    assert _isClose(ea3.gamma, comp[2])
    ## No more constructor with vector in CPP
    # ea4 = EulerAngles_XYZ(ea3.unknowns)
    # assert _isClose(comp, ea4.unknowns)
    # assert _isClose(ea4.alpha, comp[0])
    # assert _isClose(ea4.beta, comp[1])
    # assert _isClose(ea4.gamma, comp[2])
    ea5 = EulerAngles_XYZ(ea3)
    assert _isClose(comp, ea5.unknowns)
    assert _isClose(ea5.alpha, comp[0])
    assert _isClose(ea5.beta, comp[1])
    assert _isClose(ea5.gamma, comp[2])
    angle = numpy.radians(10.)
    R = RotationMatrix(_get2DRotationMatrix(angle))
    ea6 = EulerAngles_ZYX(R)
    assert _isClose(ea6.alpha, angle)

def testMRP():
    mrp = MRP()
    assert _isClose(mrp.unknowns, numpy.zeros(3, dtype=float))
    mrp2 = MRP(0.1, 0.2, 0.3)
    assert _isClose(mrp2.unknowns, numpy.array((0.1, 0.2, 0.3), dtype=float))
    mrp3 = MRP(mrp2)
    assert _isClose(mrp3.unknowns, mrp2.unknowns)
    #u = numpy.array((1., 2., 3.), dtype=float)
    #mrp4 = MRP(u)
    #assert _isClose(mrp4.unknowns, -u/numpy.linalg.norm(u)**2)
    angle = numpy.radians(10.)
    R = RotationMatrix(_get2DRotationMatrix(angle))
    mrp5 = MRP(R)
    assert _isClose(mrp5.toRotationMatrix(), R)

def _getTestAngles():
    angles = []
    for i in xrange(-8, 9):
        angles.append(i*numpy.pi/4.)
    angles.append(12.*numpy.pi)
    angles.append(-12.*numpy.pi)
    angles.append(0.999999*numpy.pi)
    angles.append(-0.999999*numpy.pi)
    return angles

def _CheckRVects(abc, vect, compVect):
    u1 = abc * vect
    u2 = abc.rotate(vect)
    uInv = abc.inverseRotate(u2)
    assert _isClose(u1, compVect)
    assert _isClose(u2, compVect)
    assert _isClose(uInv, vect)

def _CheckRotMatUnknowns(angle, abc):
    R = _get2DRotationMatrix(angle)
    # Warning unknowns are stored by columns
    unknowns = abc.unknowns + 0.
    tmp = unknowns + 0.
    tmp.shape = 3, 3
    tmp = tmp.T
    assert _isClose(R, tmp)
    # Modify unknowns
    unknowns2 = unknowns + 0.
    unknowns2[4] *= 2.
    abc.unknowns = unknowns2
    # Constraints are given as:
    # 0: d1.d1 - 1.
    # 1: d1.d2
    # 2: d1.d3
    # 3: d2.d2 - 1.
    # 4: d2.d3
    # 5: d3.d3 - 1.
    comp = numpy.array((0., numpy.sin(angle)*numpy.cos(angle),
                        0., 3.*numpy.cos(angle)**2, 0., 0.), dtype=float)
    assert _isClose(abc.constraints, comp)
    # Reset unknowns to what they were
    abc.unknowns = unknowns

def _CheckQuatUnknowns(angle, abc):
    unknowns = abc.unknowns
    comp = numpy.array((numpy.cos(angle/2.), 0., 0., numpy.sin(angle/2.)),
                       dtype=float)
    assert _isClose(unknowns, comp)

def _CheckBVUnknowns(angle, abc):
    # Same thing as RotationMatrix !
    _CheckRotMatUnknowns(angle, abc)

def _CheckMRPUnknowns(angle, abc):
    unknowns = abc.unknowns
    assert _isClose(unknowns[0], 0.)
    assert _isClose(unknowns[1], 0.)
    ang = _angleMinusPi_Pi(angle)
    # Check directly values if not close to Pi
    if( not _isClose(numpy.abs(ang), numpy.pi)):
        assert _isClose(numpy.tan(ang/4.)-unknowns[2], 0.)
    # Check both direct and shadow values if close to Pi
    else:
        assert (_isClose(abs(numpy.tan(ang/4.)-unknowns[2]), 0.) or _isClose(abs(numpy.tan(ang/4.)+unknowns[2]), 0.))

def _CheckEulerUnknowns(angle, abc):
    unknowns = abc.unknowns
    # This test does not take into account the case of singularity (gimbal lock) or special cases
    # when the rotation matrix is constructed with special angles +-90Â°, 0,+-180Â°
    ## the assertions should be recoded for each subcase
    _eulerCheckCoordinates(unknowns[0], unknowns[1], unknowns[2], 0., 0., angle, abc.getConvention()) ;

def _CheckAxisAngleUnknowns(angle, abc):
    unknowns = abc.unknowns
    ang = _angle02PI(angle)
    factor = 1.
    if (abs(unknowns[3]+ang) < 1.e-8) or (unknowns[2] < 0.):
        factor = -1. # correct in 2D case
    if abs(ang) < 1.e-8:
        assert _isClose(abs(unknowns[3]), 0.)
    else:
        assert _isClose(factor*unknowns[0], 0.)
        assert _isClose(factor*unknowns[1], 0.)
        assert _isClose(factor*unknowns[1], 0.)
        assert _isClose(_angle02PI(factor*unknowns[3]), ang)

_dTypeToCheckUnknowns = {RotationMatrix:_CheckRotMatUnknowns,
                         Quaternion:_CheckQuatUnknowns,
                         MRP:_CheckMRPUnknowns,
                         EulerAngles_XYX:_CheckEulerUnknowns,
                         EulerAngles_XYZ:_CheckEulerUnknowns,
                         EulerAngles_XZX:_CheckEulerUnknowns,
                         EulerAngles_XZY:_CheckEulerUnknowns,
                         EulerAngles_YXY:_CheckEulerUnknowns,
                         EulerAngles_YXZ:_CheckEulerUnknowns,
                         EulerAngles_YZX:_CheckEulerUnknowns,
                         EulerAngles_YZY:_CheckEulerUnknowns,
                         EulerAngles_ZXY:_CheckEulerUnknowns,
                         EulerAngles_ZXZ:_CheckEulerUnknowns,
                         EulerAngles_ZYX:_CheckEulerUnknowns,
                         EulerAngles_ZYZ:_CheckEulerUnknowns,
                         AxisAndAngle:_CheckAxisAngleUnknowns,
                         BasisVectors:_CheckBVUnknowns}

def _ABCCheck(abc, angle, nUnknowns, nConstraints):
    # Build the 2D rotation matrix
    R = numpy.zeros((3, 3), dtype=float)
    R[0, 0] = numpy.cos(angle)
    R[0, 1] = -numpy.sin(angle)
    R[1, 0] = numpy.sin(angle)
    R[1, 1] = numpy.cos(angle)
    R[2, 2] = 1.
    # Call on ABC methods
    # unknowns and constraints
    assert nUnknowns == abc.nUnknowns()
    assert nConstraints == abc.nConstraints()
    # Directors
    assert _isClose(abc.d1, numpy.array((R[0, 0], R[1, 0], R[2, 0]), dtype=float))
    assert _isClose(abc.d2, numpy.array((R[0, 1], R[1, 1], R[2, 1]), dtype=float))
    assert _isClose(abc.d3, numpy.array((R[0, 2], R[1, 2], R[2, 2]), dtype=float))
    # operator* array
    u1 = numpy.array((1., 0., 0.),dtype=float)
    u2 = numpy.array((numpy.cos(angle), -numpy.sin(angle), 0.),dtype=float)
    u3 = numpy.array((numpy.cos(angle), numpy.sin(angle), 0.),dtype=float)
    u4 = numpy.array((0., 0., 1.),dtype=float)
    _CheckRVects(abc, u1, u3)
    _CheckRVects(abc, u2, u1)
    _CheckRVects(abc, u4, u4)
    # operator* Vector
    v1 = Vector(u1)
    v2 = Vector(u2)
    v3 = Vector(u3)
    v4 = Vector(u4)
    _CheckRVects(abc, v1, v3)
    _CheckRVects(abc, v2, v1)
    _CheckRVects(abc, v4, v4)
    # operator* Point
    p1 = Vector(u1)
    p2 = Vector(u2)
    p3 = Vector(u3)
    p4 = Vector(u4)
    _CheckRVects(abc, p1, p3)
    _CheckRVects(abc, p2, p1)
    _CheckRVects(abc, p4, p4)
    # Rotate ABC by itself after copying it
    abc2 = type(abc)(abc)
    abc.addOtherRotationAtLeft(abc)
    assert _isClose(abc.d1, Vector(numpy.cos(2.*angle),
                                     numpy.sin(2.*angle), 0.))
    assert _isClose(abc.d2, Vector(-numpy.sin(2.*angle),
                                     numpy.cos(2.*angle), 0.))
    assert _isClose(abc.d3, Vector(0., 0., 1.))
    # Check unknowns and constraints
    _dTypeToCheckUnknowns[type(abc)](2.*angle, abc)
    assert _isClose(abc.constraints, numpy.zeros(nConstraints, dtype=float))
    # Get back to original rotation
    abc.subtractOtherRotationAtLeft(abc2)

def _ABCCheckRotators(angle):
    R = RotationMatrix(_get2DRotationMatrix(angle))
    _ABCCheck(R, angle, 9, 6)
    q = R.toQuaternion()
    _ABCCheck(q, angle, 4, 1)
    bv = R.toBasisVectors()
    _ABCCheck(bv, angle, 9, 6)
    aa = R.toAxisAndAngle()
    _ABCCheck(aa, angle, 4, 1)
    # ea_XYX = R.toEulerAngles_XYX()
    ea_XYZ = R.toEulerAngles_XYZ_i()
    # ea_XZX = R.toEulerAngles_XZX_i()
    # ea_XZY = R.toEulerAngles_XZY_i()
    # ea_YXY = R.toEulerAngles_YXY_i()
    # ea_YXZ = R.toEulerAngles_YXZ_i()
    # ea_YZX = R.toEulerAngles_YZX_i()
    # ea_YZY = R.toEulerAngles_YZY_i()
    # ea_ZXY = R.toEulerAngles_ZXY_i()
    # ea_ZXZ = R.toEulerAngles_ZXZ_i()
    # ea_ZYX = R.toEulerAngles_ZYX_i()
    # ea_ZYZ = R.toEulerAngles_ZYZ_i()
    # _ABCCheck(ea_XYX, angle, 3, 0)
    _ABCCheck(ea_XYZ, angle, 3, 0)
    # _ABCCheck(ea_XZX, angle, 3, 0)
    # _ABCCheck(ea_XZY, angle, 3, 0)
    # _ABCCheck(ea_YXY, angle, 3, 0)
    # _ABCCheck(ea_YXZ, angle, 3, 0)
    # _ABCCheck(ea_YZX, angle, 3, 0)
    # _ABCCheck(ea_YZY, angle, 3, 0)
    # _ABCCheck(ea_ZXY, angle, 3, 0)
    # _ABCCheck(ea_ZXZ, angle, 3, 0)
    # _ABCCheck(ea_ZYX, angle, 3, 0)
    # _ABCCheck(ea_ZYZ, angle, 3, 0)
    mrp = R.toMRP()
    _ABCCheck(mrp, angle, 3, 0)

def testABC():
    for angle in _getTestAngles():
        _ABCCheckRotators(angle)

def _CheckConversion(type_, R):
    t1 = type_(R)
    q = t1.toQuaternion()
    aa = t1.toAxisAndAngle()
    bv = t1.toBasisVectors()
    R2 = t1.toRotationMatrix()
    ea = t1.toEulerAngles_ZXY_i()
    mrp = t1.toMRP()
    t2 = type_(q)
    t3 = type_(aa)
    t4 = type_(bv)
    t5 = type_(R2)
    t6 = type_(ea)
    t7 = type_(mrp)
    assert _isClose(R, R2)
    assert _isClose(R, q.toRotationMatrix())
    assert _isClose(R, aa.toRotationMatrix())
    assert _isClose(R, bv.toRotationMatrix())
    assert _isClose(R, R2.toRotationMatrix())
    assert _isClose(R, ea.toRotationMatrix())
    assert _isClose(R, mrp.toRotationMatrix())
    assert _isClose(R, t2.toRotationMatrix())
    assert _isClose(R, t3.toRotationMatrix())
    assert _isClose(R, t4.toRotationMatrix())
    assert _isClose(R, t5.toRotationMatrix())
    assert _isClose(R, t6.toRotationMatrix())
    assert _isClose(R, t7.toRotationMatrix())

def _ABCCheckConversion(angle):
    R = RotationMatrix(_get2DRotationMatrix(angle))
    _CheckConversion(RotationMatrix, R)
    _CheckConversion(Quaternion, R)
    _CheckConversion(BasisVectors, R)
    _CheckConversion(AxisAndAngle, R)
    _CheckConversion(EulerAngles_XYX, R)
    _CheckConversion(EulerAngles_XYZ, R)
    _CheckConversion(EulerAngles_XZX, R)
    _CheckConversion(EulerAngles_XZY, R)
    _CheckConversion(EulerAngles_YXY, R)
    _CheckConversion(EulerAngles_YXZ, R)
    _CheckConversion(EulerAngles_YZX, R)
    _CheckConversion(EulerAngles_YZY, R)
    _CheckConversion(EulerAngles_ZXY, R)
    _CheckConversion(EulerAngles_ZXZ, R)
    _CheckConversion(EulerAngles_ZYX, R)
    _CheckConversion(EulerAngles_ZYZ, R)
    _CheckConversion(MRP, R)

def testConversion():
    for angle in _getTestAngles():
        _ABCCheckConversion(angle)
 
def _CheckOperators(T1, T2, angle):
    R = RotationMatrix(_get2DRotationMatrix(angle))
    R2 = RotationMatrix(_get2DRotationMatrix(angle*2.))
    rotator = T1(R)
    rotator2 = T2(R)
    assert _isClose(rotator.d1, rotator2.d1)
    assert _isClose(rotator.d2, rotator2.d2)
    assert _isClose(rotator.d3, rotator2.d3)
    rotator3 = rotator * rotator2
    assert _isClose(rotator3.d1, R2.d1)
    assert _isClose(rotator3.d2, R2.d2)
    assert _isClose(rotator3.d3, R2.d3)
    rotator4 = rotator + rotator2
    assert _isClose(rotator4.d1, rotator3.d1)
    assert _isClose(rotator4.d2, rotator3.d2)
    assert _isClose(rotator4.d3, rotator3.d3)
    rotator5 = rotator4 - rotator2
    assert _isClose(rotator5.d1, rotator.d1)
    assert _isClose(rotator5.d2, rotator.d2)
    assert _isClose(rotator5.d3, rotator.d3)
    rotator5 *= rotator2
    assert _isClose(rotator5.d1, rotator3.d1)
    assert _isClose(rotator5.d2, rotator3.d2)
    assert _isClose(rotator5.d3, rotator3.d3)
    rotator5 -= rotator2
    assert _isClose(rotator5.d1, rotator.d1)
    assert _isClose(rotator5.d2, rotator.d2)
    assert _isClose(rotator5.d3, rotator.d3)
    rotator5 += rotator2
    assert _isClose(rotator5.d1, rotator3.d1)
    assert _isClose(rotator5.d2, rotator3.d2)
    assert _isClose(rotator5.d3, rotator3.d3)
    rotator5 += -rotator2
    assert _isClose(rotator5.d1, rotator.d1)
    assert _isClose(rotator5.d2, rotator.d2)
    assert _isClose(rotator5.d3, rotator.d3)



def testOperators():
    for angle in _getTestAngles():
        _CheckOperators(RotationMatrix, RotationMatrix, angle)
        _CheckOperators(RotationMatrix, Quaternion, angle)
        _CheckOperators(RotationMatrix, BasisVectors, angle)
        _CheckOperators(RotationMatrix, AxisAndAngle, angle)
        _CheckOperators(RotationMatrix, EulerAngles_XYX, angle)
        _CheckOperators(RotationMatrix, EulerAngles_XYZ, angle)
        _CheckOperators(RotationMatrix, EulerAngles_XZX, angle)
        _CheckOperators(RotationMatrix, EulerAngles_XZY, angle)
        _CheckOperators(RotationMatrix, EulerAngles_YXY, angle)
        _CheckOperators(RotationMatrix, EulerAngles_YXZ, angle)
        _CheckOperators(RotationMatrix, EulerAngles_YZX, angle)
        _CheckOperators(RotationMatrix, EulerAngles_YZY, angle)
        _CheckOperators(RotationMatrix, EulerAngles_ZXY, angle)
        _CheckOperators(RotationMatrix, EulerAngles_ZXZ, angle)
        _CheckOperators(RotationMatrix, EulerAngles_ZYX, angle)
        _CheckOperators(RotationMatrix, EulerAngles_ZYZ, angle)
        _CheckOperators(RotationMatrix, MRP, angle)
        _CheckOperators(Quaternion, RotationMatrix, angle)
        _CheckOperators(Quaternion, Quaternion, angle)
        _CheckOperators(Quaternion, BasisVectors, angle)
        _CheckOperators(Quaternion, AxisAndAngle, angle)
        _CheckOperators(Quaternion, EulerAngles_XYX, angle)
        _CheckOperators(Quaternion, EulerAngles_XYZ, angle)
        _CheckOperators(Quaternion, EulerAngles_XZX, angle)
        _CheckOperators(Quaternion, EulerAngles_XZY, angle)
        _CheckOperators(Quaternion, EulerAngles_YXY, angle)
        _CheckOperators(Quaternion, EulerAngles_YXZ, angle)
        _CheckOperators(Quaternion, EulerAngles_YZX, angle)
        _CheckOperators(Quaternion, EulerAngles_YZY, angle)
        _CheckOperators(Quaternion, EulerAngles_ZXY, angle)
        _CheckOperators(Quaternion, EulerAngles_ZXZ, angle)
        _CheckOperators(Quaternion, EulerAngles_ZYX, angle)
        _CheckOperators(Quaternion, EulerAngles_ZYZ, angle)
        _CheckOperators(Quaternion, MRP, angle)
        _CheckOperators(BasisVectors, RotationMatrix, angle)
        _CheckOperators(BasisVectors, Quaternion, angle)
        _CheckOperators(BasisVectors, BasisVectors, angle)
        _CheckOperators(BasisVectors, AxisAndAngle, angle)
        _CheckOperators(BasisVectors, EulerAngles_XYX, angle)
        _CheckOperators(BasisVectors, EulerAngles_XYZ, angle)
        _CheckOperators(BasisVectors, EulerAngles_XZX, angle)
        _CheckOperators(BasisVectors, EulerAngles_XZY, angle)
        _CheckOperators(BasisVectors, EulerAngles_YXY, angle)
        _CheckOperators(BasisVectors, EulerAngles_YXZ, angle)
        _CheckOperators(BasisVectors, EulerAngles_YZX, angle)
        _CheckOperators(BasisVectors, EulerAngles_YZY, angle)
        _CheckOperators(BasisVectors, EulerAngles_ZXY, angle)
        _CheckOperators(BasisVectors, EulerAngles_ZXZ, angle)
        _CheckOperators(BasisVectors, EulerAngles_ZYX, angle)
        _CheckOperators(BasisVectors, EulerAngles_ZYZ, angle)
        _CheckOperators(BasisVectors, MRP, angle)
        _CheckOperators(AxisAndAngle, RotationMatrix, angle)
        _CheckOperators(AxisAndAngle, Quaternion, angle)
        _CheckOperators(AxisAndAngle, BasisVectors, angle)
        _CheckOperators(AxisAndAngle, AxisAndAngle, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_XYX, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_XYZ, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_XZX, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_XZY, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_YXY, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_YXZ, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_YZX, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_YZY, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_ZXY, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_ZXZ, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_ZYX, angle)
        _CheckOperators(AxisAndAngle, EulerAngles_ZYZ, angle)
        _CheckOperators(AxisAndAngle, MRP, angle)
        _CheckOperators(MRP, RotationMatrix, angle)
        _CheckOperators(MRP, Quaternion, angle)
        _CheckOperators(MRP, BasisVectors, angle)
        _CheckOperators(MRP, AxisAndAngle, angle)
        _CheckOperators(MRP, EulerAngles_XYX, angle)
        _CheckOperators(MRP, EulerAngles_XYZ, angle)
        _CheckOperators(MRP, EulerAngles_XZX, angle)
        _CheckOperators(MRP, EulerAngles_XZY, angle)
        _CheckOperators(MRP, EulerAngles_YXY, angle)
        _CheckOperators(MRP, EulerAngles_YXZ, angle)
        _CheckOperators(MRP, EulerAngles_YZX, angle)
        _CheckOperators(MRP, EulerAngles_YZY, angle)
        _CheckOperators(MRP, EulerAngles_ZXY, angle)
        _CheckOperators(MRP, EulerAngles_ZXZ, angle)
        _CheckOperators(MRP, EulerAngles_ZYX, angle)
        _CheckOperators(MRP, EulerAngles_ZYZ, angle)
        _CheckOperators(MRP, MRP, angle)
        
        _CheckOperators(EulerAngles_XYX, RotationMatrix, angle)
        _CheckOperators(EulerAngles_XYX, Quaternion, angle)
        _CheckOperators(EulerAngles_XYX, BasisVectors, angle)
        _CheckOperators(EulerAngles_XYX, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_XYX, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_XYX, MRP, angle)
        
        _CheckOperators(EulerAngles_XYZ, RotationMatrix, angle)
        _CheckOperators(EulerAngles_XYZ, Quaternion, angle)
        _CheckOperators(EulerAngles_XYZ, BasisVectors, angle)
        _CheckOperators(EulerAngles_XYZ, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_XYZ, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_XYZ, MRP, angle)
        
        _CheckOperators(EulerAngles_XZX, RotationMatrix, angle)
        _CheckOperators(EulerAngles_XZX, Quaternion, angle)
        _CheckOperators(EulerAngles_XZX, BasisVectors, angle)
        _CheckOperators(EulerAngles_XZX, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_XZX, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_XZX, MRP, angle)
        
        _CheckOperators(EulerAngles_XZY, RotationMatrix, angle)
        _CheckOperators(EulerAngles_XZY, Quaternion, angle)
        _CheckOperators(EulerAngles_XZY, BasisVectors, angle)
        _CheckOperators(EulerAngles_XZY, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_XZY, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_XZY, MRP, angle)
        
        _CheckOperators(EulerAngles_YXY, RotationMatrix, angle)
        _CheckOperators(EulerAngles_YXY, Quaternion, angle)
        _CheckOperators(EulerAngles_YXY, BasisVectors, angle)
        _CheckOperators(EulerAngles_YXY, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_YXY, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_YXY, MRP, angle)
        
        _CheckOperators(EulerAngles_YXZ, RotationMatrix, angle)
        _CheckOperators(EulerAngles_YXZ, Quaternion, angle)
        _CheckOperators(EulerAngles_YXZ, BasisVectors, angle)
        _CheckOperators(EulerAngles_YXZ, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_YXZ, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_YXZ, MRP, angle)
        
        _CheckOperators(EulerAngles_YZX, RotationMatrix, angle)
        _CheckOperators(EulerAngles_YZX, Quaternion, angle)
        _CheckOperators(EulerAngles_YZX, BasisVectors, angle)
        _CheckOperators(EulerAngles_YZX, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_YZX, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_YZX, MRP, angle)
        
        _CheckOperators(EulerAngles_YZY, RotationMatrix, angle)
        _CheckOperators(EulerAngles_YZY, Quaternion, angle)
        _CheckOperators(EulerAngles_YZY, BasisVectors, angle)
        _CheckOperators(EulerAngles_YZY, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_YZY, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_YZY, MRP, angle)
        
        _CheckOperators(EulerAngles_ZXY, RotationMatrix, angle)
        _CheckOperators(EulerAngles_ZXY, Quaternion, angle)
        _CheckOperators(EulerAngles_ZXY, BasisVectors, angle)
        _CheckOperators(EulerAngles_ZXY, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_ZXY, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_ZXY, MRP, angle)
        
        _CheckOperators(EulerAngles_ZXZ, RotationMatrix, angle)
        _CheckOperators(EulerAngles_ZXZ, Quaternion, angle)
        _CheckOperators(EulerAngles_ZXZ, BasisVectors, angle)
        _CheckOperators(EulerAngles_ZXZ, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_ZXZ, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_ZXZ, MRP, angle)
        
        _CheckOperators(EulerAngles_ZYX, RotationMatrix, angle)
        _CheckOperators(EulerAngles_ZYX, Quaternion, angle)
        _CheckOperators(EulerAngles_ZYX, BasisVectors, angle)
        _CheckOperators(EulerAngles_ZYX, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_ZYX, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_ZYX, MRP, angle)
        
        _CheckOperators(EulerAngles_ZYZ, RotationMatrix, angle)
        _CheckOperators(EulerAngles_ZYZ, Quaternion, angle)
        _CheckOperators(EulerAngles_ZYZ, BasisVectors, angle)
        _CheckOperators(EulerAngles_ZYZ, AxisAndAngle, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_XYX, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_XYZ, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_XZX, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_XZY, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_YXY, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_YXZ, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_YZX, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_YZY, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_ZXY, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_ZXZ, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_ZYX, angle)
        _CheckOperators(EulerAngles_ZYZ, EulerAngles_ZYZ, angle)
        _CheckOperators(EulerAngles_ZYZ, MRP, angle)
 
