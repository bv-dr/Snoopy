import Geometry

def testRotationsInstantiations():
    # BasisVectors
    d1 = Geometry.Vector( 1., 0., 0. )
    d2 = Geometry.Vector( 0., 1., 0. )
    d3 = Geometry.Vector( 0., 0., 1. )
    bv = Geometry.BasisVectors()
    bv = Geometry.BasisVectors( d1, d2, d3 )
    # EulerAngles31p3pp
    phi, theta, psi = 0., 0., 0.
    e = Geometry.EulerAngles_ZYX_e()
    e = Geometry.EulerAngles_ZYX_e(phi, theta, psi)
    # Quaternion
    a, b, c, d = 1., 0., 0., 0.
    q = Geometry.Quaternion()
    q = Geometry.Quaternion( a, b, c, d )
    # HorizontalPlane
    angle = 0.
    #hp = Geometry.HorizontalPlane()
    #hp = Geometry.HorizontalPlane( angle )
    # AxisAndAngle
    aaa = Geometry.AxisAndAngle()
    aaa = Geometry.AxisAndAngle( d1, angle )

