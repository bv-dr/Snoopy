import os, sys, math

epsilon = sys.float_info.epsilon

import Geometry

def testPointGetters():
    pt = Geometry.Point( 1., 2., 3.)
    assert( pt.x == 1. )
    assert( pt.y == 2. )
    assert( pt.z == 3. )

def testVectorGetters():
    vec = Geometry.Vector( 4., 5., 6.)
    assert( vec.x == 4. )
    assert( vec.y == 5. )
    assert( vec.z == 6. )

def testVectorNormalised():
    vec = Geometry.Vector( 4., 5., 6.)
    assert( abs( vec.norm() - math.sqrt( 4.*4. + 5.*5. + 6.*6. ) ) <= epsilon )
    unit = vec.normalised()
    assert( abs( unit.norm() - 1. ) <= epsilon )

def testPointBuildingFromConfigFile():
    try:
        from SlenderBodies import Utils
        here = os.path.dirname( os.path.abspath( __file__ ) )
        cfg = Utils.ConfigFile()
        cfg.read( os.path.join( here, "someData.cfg" ) )
        
        pt = Geometry.Point( cfg, "aPoint" )
        assert( pt.x == 1. )
        assert( pt.y == 2. )
        assert( pt.z == 3. )
        
        nestedPt = Geometry.Point( cfg, "nested.point" )
        assert( nestedPt.x == 4. )
        assert( nestedPt.y == 5. )
        assert( nestedPt.z == 6. )
    except ImportError:
        pass
    except:
        assert( False )

def testVectorScale():
    v1 = Geometry.Vector( 2., 4., -6 )
    # Testing classical __mul__ scale operator
    v2 = v1 * 0.5 
    assert( v2.x ==  1. )
    assert( v2.y ==  2. )
    assert( v2.z == -3. )
    # Testing reverse __rmul__ scale operator
    v3 = 0.5 * v1
    assert( v3.x ==  1. )
    assert( v3.y ==  2. )
    assert( v3.z == -3. )

def PointAndVectorCopiesTester( class_ ):
    import copy
    xyz0 = class_( 1., 2., 3. )
    xyz1 = copy.copy( xyz0 )
    xyz1.x, xyz1.y, xyz1.z = 4., 5., 6.
    assert( xyz0.x == 1. )
    assert( xyz0.y == 2. )
    assert( xyz0.z == 3. )
    xyz2 = copy.deepcopy( xyz0 )
    xyz2.x, xyz2.y, xyz2.z = 4., 5., 6.
    assert( xyz0.x == 1. )
    assert( xyz0.y == 2. )
    assert( xyz0.z == 3. )

def testPointVectorCopies():
    for class_ in ( Geometry.Point, Geometry.Vector ):
        yield PointAndVectorCopiesTester, class_

