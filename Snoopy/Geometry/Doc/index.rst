.. Geometry documentation master file, created by
   sphinx-quickstart on Wed Feb 21 17:47:17 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _GeometryDocumentation:

Welcome to Geometry's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   XYZ
   Translation/Translation
   Rotation/Rotation
   Transform3D


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
