3D transformation
=================

.. module:: Geometry

.. autoclass:: Transform3D
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.Transform3D
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.Transform3D
      :attributes:

   .. rubric:: Details
