.. _GeometryRotation:

Rotation
========

.. module:: Geometry

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Euler.rst
   RotationMatrix.rst
   BasisVectors.rst
   Quaternion.rst
   AxisAndAngle.rst
   MRP.rst
   HorizontalPlane.rst

.. autoclass:: RotatorTypeEnum
   :members:

