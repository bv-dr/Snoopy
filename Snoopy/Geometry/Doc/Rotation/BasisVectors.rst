Basis vectors
=============

.. module:: Geometry

.. autoclass:: BasisVectors
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.BasisVectors
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.BasisVectors
      :attributes:

   .. rubric:: Details
