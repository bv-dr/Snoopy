Modified Rodrigues Parameters
=============================

.. module:: Geometry

.. autoclass:: MRP
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.MRP
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.MRP
      :attributes:

   .. rubric:: Details
