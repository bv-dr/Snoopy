Rotation matrix
===============

.. module:: Geometry

.. autoclass:: RotationMatrix
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.RotationMatrix
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.RotationMatrix
      :attributes:

   .. rubric:: Details

