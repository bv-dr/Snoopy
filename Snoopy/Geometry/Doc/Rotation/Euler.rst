Euler
=====

.. module:: Geometry

All Euler angles classes have the same interface, only the first one is
listed here for the sake of brievity.


.. autoclass:: EulerAngles_XYX_i
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.EulerAngles_XYX_i
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.EulerAngles_XYX_i
      :attributes:

   .. rubric:: Details

Here is the complete list of Euler angles classes, depending on the desired
convention:

.. class:: EulerAngles_XYX_i
.. class:: EulerAngles_XYZ_i
.. class:: EulerAngles_XZX_i
.. class:: EulerAngles_XZY_i
.. class:: EulerAngles_YXY_i
.. class:: EulerAngles_YXZ_i
.. class:: EulerAngles_YZX_i
.. class:: EulerAngles_YZY_i
.. class:: EulerAngles_ZXY_i
.. class:: EulerAngles_ZXZ_i
.. class:: EulerAngles_ZYX_i
.. class:: EulerAngles_ZYZ_i
.. class:: EulerAngles_XYX_e
.. class:: EulerAngles_XYZ_e
.. class:: EulerAngles_XZX_e
.. class:: EulerAngles_XZY_e
.. class:: EulerAngles_YXY_e
.. class:: EulerAngles_YXZ_e
.. class:: EulerAngles_YZX_e
.. class:: EulerAngles_YZY_e
.. class:: EulerAngles_ZXY_e
.. class:: EulerAngles_ZXZ_e
.. class:: EulerAngles_ZYX_e
.. class:: EulerAngles_ZYZ_e

.. autoclass:: AxisConvention
   :members:

All Euler angles conventions have the same interface, only the first one is
listed here for the sake of brievity.

.. autoclass:: EulerAnglesConvention_XYX_i
   :members:
   :inherited-members:

   .. rubric:: Methods

   .. autoautosummary:: Geometry.EulerAnglesConvention_XYX_i
      :methods:

And here is the complete list of possible conventions.

.. class:: EulerAnglesConvention_XYX_i
.. class:: EulerAnglesConvention_XYZ_i
.. class:: EulerAnglesConvention_XZX_i
.. class:: EulerAnglesConvention_XZY_i
.. class:: EulerAnglesConvention_YXY_i
.. class:: EulerAnglesConvention_YXZ_i
.. class:: EulerAnglesConvention_YZX_i
.. class:: EulerAnglesConvention_YZY_i
.. class:: EulerAnglesConvention_ZXY_i
.. class:: EulerAnglesConvention_ZXZ_i
.. class:: EulerAnglesConvention_ZYX_i
.. class:: EulerAnglesConvention_ZYZ_i
.. class:: EulerAnglesConvention_XYX_e
.. class:: EulerAnglesConvention_XYZ_e
.. class:: EulerAnglesConvention_XZX_e
.. class:: EulerAnglesConvention_XZY_e
.. class:: EulerAnglesConvention_YXY_e
.. class:: EulerAnglesConvention_YXZ_e
.. class:: EulerAnglesConvention_YZX_e
.. class:: EulerAnglesConvention_YZY_e
.. class:: EulerAnglesConvention_ZXY_e
.. class:: EulerAnglesConvention_ZXZ_e
.. class:: EulerAnglesConvention_ZYX_e
.. class:: EulerAnglesConvention_ZYZ_e

