Axis and angle
==============

.. module:: Geometry

.. autoclass:: AxisAndAngle
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.AxisAndAngle
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.AxisAndAngle
      :attributes:

   .. rubric:: Details

