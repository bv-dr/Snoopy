XYZ
===

.. module:: Geometry

.. autosummary::
   :nosignatures:

   ~Geometry.Point
   ~Geometry.Vector

.. autoclass:: Point
   :members:
   :inherited-members:
   :special-members: __init__, __mul__, __add__, __sub__, __inv__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.Point
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.Point
      :attributes:

   .. rubric:: Details
   
.. autoclass:: Vector
   :members:
   :inherited-members:
   :special-members: __init__, __mul__, __add__, __sub__, __xor__, __inv__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.Vector
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.Vector
      :attributes:

   .. rubric:: Details
