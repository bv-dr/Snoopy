Spherical
=========

.. module:: Geometry

.. autoclass:: Spherical_Lat
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.Spherical_Lat
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.Spherical_Lat
      :attributes:

   .. rubric:: Details

.. autoclass:: Spherical_CoLat
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.Spherical_CoLat
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.Spherical_CoLat
      :attributes:

   .. rubric:: Details

