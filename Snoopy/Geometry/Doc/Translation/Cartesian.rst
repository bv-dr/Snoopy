Cartesian
=========

.. module:: Geometry


.. autoclass:: Cartesian
   :members:
   :inherited-members:
   :special-members: __init__, __imul__, __mul__, __isub__, __sub__, __iadd__, __add__, __neg__

   .. rubric:: Methods

   .. autoautosummary:: Geometry.Cartesian
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Geometry.Cartesian
      :attributes:

   .. rubric:: Details

