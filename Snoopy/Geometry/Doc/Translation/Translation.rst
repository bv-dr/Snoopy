.. _GeometryTranslation:

Translation
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Cartesian.rst
   Spherical.rst

.. module:: Geometry

.. autoclass:: TranslatorTypeEnum
   :members:

