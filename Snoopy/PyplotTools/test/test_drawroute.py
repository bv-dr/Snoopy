import pandas as pd
import pytest
from Snoopy.PyplotTools import drawRoute, TEST_DIR


def test_drawroute():
    """Just test that drawRoute is running, no check of the result
    """

    data = pd.read_csv(f"{TEST_DIR:}/sat_hs.csv").iloc[:500:10,:]
    drawRoute(data, var = "HS_SAT", vmin = 2.5 , vmax = 14.97, zoom = "atlantic")
    assert(True)

if __name__ == "__main__" :

    test_drawroute()






