import os
from Snoopy import PyplotTools as dplt


def test_read_csv_block():
    # Test read_csv_block routine

    d = dplt.read_csv_block(f"{dplt.TEST_DIR:}/data_block.dat", 0, sep='\s+', index_col = 0, header = None)
    assert( d.loc[0.1, 1] == 1.0)

    d1,d2 = dplt.read_csv_block(f"{dplt.TEST_DIR:}/data_block.dat", [0,1], sep='\s+', index_col = 0, header = None)

    assert( d1.loc[0.1, 1] == 1.0)
    assert( d2.loc[0.21, 2] == 2.5)

    d1,d2 = dplt.read_csv_block(f"{dplt.TEST_DIR:}/data_block.dat", "all", sep='\s+', index_col = 0, header = None)

    assert( d1.loc[0.1, 1] == 1.0)
    assert( d2.loc[0.21, 2] == 2.5)



if __name__ == "__main__" :
    test_read_csv_block()

