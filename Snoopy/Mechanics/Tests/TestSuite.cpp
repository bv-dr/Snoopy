#include <boost/test/unit_test.hpp>

#include "Tests/ReferenceFrame/ReferenceFrame.hpp"
#include "Tests/Torsor/Torsor.hpp"

using namespace boost::unit_test ;

#define BV_ADD_MECHANICS_TEST( testName )                  \
    MechanicsTestSuite->add(BOOST_TEST_CASE( &(            \
                            BV::Mechanics::Tests::testName \
                                             ) ) ) ;

test_suite * init_unit_test_suite( int argc, char* argv[] )
{
    test_suite * MechanicsTestSuite = BOOST_TEST_SUITE( "MechanicsTestSuite" ) ;

    BV_ADD_MECHANICS_TEST(ReferenceFrameConstruction) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameSetTranslator) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrame) ;
    BV_ADD_MECHANICS_TEST(DefaultDirectors) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameParent) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameToReferenceFrame) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameUnknownsConstraints) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameTranslatorAndRotatorGetSet) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameGetGlobalRotatorAndTranslator) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameGetRotatorAndTranslatorInFrame) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameCheckChangeOfBasis) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameTranslateIn) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameInverse) ;
    BV_ADD_MECHANICS_TEST(ReferenceFrameOperatorMinus) ;

    BV_ADD_MECHANICS_TEST(TorsorConstruction) ;
    BV_ADD_MECHANICS_TEST(TorsorMethods) ;

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV Mechanics test suite" ;
    framework::master_test_suite().add( MechanicsTestSuite ) ;

  return 0 ;
}

#undef BV_ADD_MECHANICS_TEST
