#include "Tests/Torsor/Tools.hpp"

namespace BV {
namespace Mechanics {
namespace Tests {

namespace Details {

void CheckVectorXd(const Eigen::VectorXd & v1, const Eigen::VectorXd & v2)
{
    assert(v1.size() == v2.size()) ;
    for (unsigned i=0; i<v1.size(); ++i)
    {
        BOOST_CHECK_SMALL(v1(i) - v2(i), eps) ;
    }
}

Eigen::Matrix3d GetRXMatrix(const double & angle)
{
    Eigen::Matrix3d R ;
    R(0, 0) = 1. ;
    R(0, 1) = 0. ;
    R(0, 2) = 0. ;
    R(1, 0) = 0. ;
    R(1, 1) = std::cos(angle) ;
    R(1, 2) = -std::sin(angle) ;
    R(2, 0) = 0. ;
    R(2, 1) = std::sin(angle) ;
    R(2, 2) = std::cos(angle) ;
    return R ;
}

Eigen::Matrix3d GetRYMatrix(const double & angle)
{
    Eigen::Matrix3d R ;
    R(0, 0) = std::cos(angle) ;
    R(0, 1) = 0. ;
    R(0, 2) = std::sin(angle) ;
    R(1, 0) = 0. ;
    R(1, 1) = 1. ;
    R(1, 2) = 0. ;
    R(2, 0) = -std::sin(angle) ;
    R(2, 1) = 0. ;
    R(2, 2) = std::cos(angle) ;
    return R ;
}

Eigen::Matrix3d GetRZMatrix(const double & angle)
{
    Eigen::Matrix3d R ;
    R(0, 0) = std::cos(angle) ;
    R(0, 1) = -std::sin(angle) ;
    R(0, 2) = 0. ;
    R(1, 0) = std::sin(angle) ;
    R(1, 1) = std::cos(angle) ;
    R(1, 2) = 0. ;
    R(2, 0) = 0. ;
    R(2, 1) = 0. ;
    R(2, 2) = 1. ;
    return R ;
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Mechanics
} // End of namespace BV
