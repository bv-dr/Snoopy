#include "Mechanics/ReferenceFrame.hpp"

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"

#include "Geometry/Translation.hpp"
#include "Geometry/Rotation.hpp"

#include "Tests/ReferenceFrame/ReferenceFrame.hpp"

#include "Tests/Tools.hpp"

namespace BV {
namespace Mechanics {
namespace Tests {

void ReferenceFrameConstruction(void)
{
    using Mechanics::ReferenceFrame ;
    ReferenceFrame rf ;
    ReferenceFrame rf2 ;
    rf2.setParent(rf) ;
    rf.originInParent(Geometry::Point(1., 2., 3.)) ;
    Details::CheckXYZ(rf.originInGlobal(), Geometry::Point(1., 2., 3.)) ;
    rf2.originInParent(Geometry::Point(1., 2., 3.)) ;
    Details::CheckXYZ(rf2.originInGlobal(), Geometry::Point(2., 4., 6.)) ;
    Details::CheckXYZ(rf2.originInParent(), Geometry::Point(1., 2., 3.)) ;

    // Check copy constructor
    ReferenceFrame rf3(rf2) ;
    BOOST_CHECK( &(rf2.getParent()) == &(rf3.getParent()) ) ;
    BOOST_CHECK( &rf2 != &rf3 ) ;
    Details::CheckXYZ(rf3.originInGlobal(), Geometry::Point(2., 4., 6.)) ;
    Details::CheckXYZ(rf3.originInParent(), Geometry::Point(1., 2., 3.)) ;

    // Check Operator =
    ReferenceFrame rf4 ;
    Details::CheckXYZ(rf4.originInGlobal(), Geometry::Point(0., 0., 0.)) ;
    Details::CheckXYZ(rf4.originInParent(), Geometry::Point(0., 0., 0.)) ;
    rf4 = rf3 ;
    BOOST_CHECK( &(rf2.getParent()) == &(rf4.getParent()) ) ;
    BOOST_CHECK( &rf4 != &rf3 ) ;
    Details::CheckXYZ(rf4.originInGlobal(), Geometry::Point(2., 4., 6.)) ;
    Details::CheckXYZ(rf4.originInParent(), Geometry::Point(1., 2., 3.)) ;

    // Check interpolation constructor
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Geometry::Vector ;
    Eigen::Vector3d eAxis(1., 2., 3.) ;
    eAxis.normalize() ;
    Vector gAxis(eAxis) ;
    ReferenceFrame rfInit ;
    double initAngle(5.*M_PI/180.) ;
    rfInit.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, AxisAndAngle(gAxis, initAngle)) ;

    ReferenceFrame rfFinal ;
    double finalAngle(70.*M_PI/180.) ;
    rfFinal.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, AxisAndAngle(gAxis, finalAngle)) ;

    double interpParam(0.3) ;
    ReferenceFrame rfInter(rfInit, rfFinal, interpParam) ;
    AxisAndAngle rfInterRotator(rfInter.getRotatorInParent()) ;
    Details::CheckVectorXd(rfInterRotator.axis(), eAxis) ;
    double interAngle(initAngle + (finalAngle-initAngle)*interpParam) ;
    BOOST_CHECK_SMALL(rfInterRotator.angle() - interAngle, Details::eps) ;

}

void ReferenceFrameSetTranslator(void)
{
    using Mechanics::ReferenceFrame ;
    using Geometry::Translation::Cartesian ;
    using Geometry::Translation::Horizontal ;
    using Geometry::TranslatorTypeEnum ;
    ReferenceFrame rf ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 2., 3.) ;
    Details::CheckXYZ(rf.originInGlobal(), Geometry::Point(1., 2., 3.)) ;
    Cartesian td(4., 5., 6.) ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, td) ;
    Details::CheckXYZ(rf.originInGlobal(), Geometry::Point(4., 5., 6.)) ;
    rf.setTranslator(TranslatorTypeEnum::HORIZONTAL, 7., 8., 9.) ;
    Details::CheckXYZ(rf.originInGlobal(), Geometry::Point(7., 8., 9.)) ;
    Horizontal h(10., 11., 12.) ;
    rf.setTranslator(TranslatorTypeEnum::HORIZONTAL, h) ;
    Details::CheckXYZ(rf.originInGlobal(), Geometry::Point(10., 11., 12.)) ;

    // Translation setter from Point object with originInGlobal method
    {
        using Geometry::Rotation::AxisAndAngle ;
        using Geometry::Point ;
        using Geometry::Vector ;
        AxisAndAngle aa(Vector(0., 0., 1.), 90.) ;
        const Point o2( 1., 8., -2. ) ;
        ReferenceFrame frame ;
        frame.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa) ;
        frame.originInGlobal( o2 ) ;
        Details::CheckXYZ(frame.originInGlobal(), o2) ;
    }
    // Translation setter from Point object with originInParent method
    {
        using Geometry::Rotation::AxisAndAngle ;
        using Geometry::Point ;
        using Geometry::Vector ;
        AxisAndAngle aa(Vector(0., 0., 1.), 90.*M_PI/180.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), -90.*M_PI/180.) ;
        const Point o1( -1., 5., 3. ) ;
        const Point o2( 1., 8., -2. ) ;
        const Vector v2(o2.x(), o2.y(), o2.z()) ;
        ReferenceFrame frame ;
        frame.setTranslator(TranslatorTypeEnum::CARTESIAN, o1) ;
        frame.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa) ;
        ReferenceFrame frame2 ;
        frame2.setParent(frame) ;
        frame2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa2) ;
        frame2.originInGlobal( o2 ) ;
        Details::CheckXYZ(frame2.originInGlobal(), o2) ;
        Details::CheckVectorXd(frame2.originInParent().toArray(), (aa.getInversed() * (o2 - o1)).toArray() ) ;

        frame2.originInParent( o1 ) ;
        Details::CheckXYZ(frame2.originInParent(), o1 ) ;
        Details::CheckVectorXd(frame2.originInGlobal().toArray(), (aa * o1 + o1.toVector()).toArray() ) ;

        const Point o3( 2., 9., -5. ) ;
        frame2.originInParent( o3 ) ;
        Details::CheckXYZ(frame2.originInParent(), o3 ) ;
        Details::CheckVectorXd(frame2.originInGlobal().toArray(), (aa * o3 + o1.toVector()).toArray() ) ;
    }
}

void ReferenceFrame( void )
{
    using Geometry::Point ;
    using Geometry::Vector ;
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;

    // I shall be able to set translators
    {
        const Point p( 1., 2., 3. ) ;
        ReferenceFrame frame ;
        frame.setTranslator( TranslatorTypeEnum::CARTESIAN,  p.x(), p.y(), p.z() ) ;
        Details::CheckXYZ(frame.originInGlobal(), p) ;
    }

    // I shall be able to set rotators
    {
        ReferenceFrame frame ;
        frame.setRotator(Geometry::RotatorTypeEnum::BASIS_VECTORS) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_XYZ_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_XZX_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_XZY_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_YXY_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_YXZ_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_YZX_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_YZY_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_ZXY_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_ZXZ_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYX_i) ;
        frame.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYZ_i) ;
    }

    // Testing some translations
    {
        const Point o( 0., 0., 0. ) ;
        const Vector t( 1., 2., 3. ) ;
        const Point result( 1., 2., 3. ) ;
        // Translating from coordinates
        {
            ReferenceFrame frame ;
            frame.setTranslator(TranslatorTypeEnum::CARTESIAN,  o.x(), o.y(), o.z() ) ;
            frame.translateWithTranslationInParent( t ) ;
            Details::CheckXYZ(frame.originInGlobal(), result) ;
        }
        // Translation from Translation object
        {
            ReferenceFrame frame ;
            frame.setTranslator(TranslatorTypeEnum::CARTESIAN,  o.x(), o.y(), o.z() ) ;
            const Geometry::Translation::Cartesian obj( t ) ;
            frame.translateWithTranslationInParent( obj ) ;
            Details::CheckXYZ(frame.originInGlobal(), result) ;
        }
    }
}

template <class... RotatorArgs >
void DefaultDirectorsTester_(const Geometry::RotatorTypeEnum & rotatorTypeEnum, RotatorArgs...rotatorArgs)
{
    using BV::Geometry::TranslatorTypeEnum ;
    // Accessing the directors
    // Testing the default directors. They should _all_ be the same
    const Geometry::Point c( 1., 2., 3. ) ;
    const Geometry::Vector d1( 1., 0., 0. ) ;
    const Geometry::Vector d2( 0., 1., 0. ) ;
    const Geometry::Vector d3( 0., 0., 1. ) ;

    Mechanics::ReferenceFrame frame ;                    
    frame.setTranslator(TranslatorTypeEnum::CARTESIAN, c ) ;

    frame.setRotator(rotatorTypeEnum, rotatorArgs...) ;
    Details::CheckXYZ(frame.d1InGlobal(), d1) ;
    Details::CheckXYZ(frame.d2InGlobal(), d2) ;
    Details::CheckXYZ(frame.d3InGlobal(), d3) ;
    Details::CheckXYZ(frame.originInGlobal(), c) ;
}

void DefaultDirectors( void )
{
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::BASIS_VECTORS) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::HORIZONTAL_PLANE) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::QUATERNION) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XYZ_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XZX_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XZY_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YXY_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YXZ_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YZX_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YZY_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZXY_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZXZ_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYX_i) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYZ_i) ;

    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XYZ_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XZX_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_XZY_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YXY_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YXZ_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YZX_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_YZY_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZXY_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZXZ_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYX_e) ;
    DefaultDirectorsTester_(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYZ_e) ;
}

void ReferenceFrameParent(void)
{
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;
    ReferenceFrame parent ;
    ReferenceFrame child ;
    child.setParent(parent) ;
    // Small checks on utility functions
    BOOST_ASSERT(child.hasParent() == true) ;
    BOOST_ASSERT(parent.hasParent() == false) ;
    BOOST_ASSERT(&parent == &(child.getParent())) ;
    // Translate parent frame
    parent.translateWithTranslationInParent(Geometry::Vector(5., 6., 7.)) ;
    // Check all origins
    Details::CheckXYZ(parent.originInGlobal(), Geometry::Point(5., 6., 7.)) ;
    Details::CheckXYZ(parent.originInParent(), Geometry::Point(5., 6., 7.)) ;
    Details::CheckXYZ(child.originInGlobal(), Geometry::Point(5., 6., 7.)) ;
    Details::CheckXYZ(child.originInParent(), Geometry::Point(0., 0., 0.)) ;
    Details::CheckXYZ(parent.d1InGlobal(), child.d1InGlobal()) ;
    Details::CheckXYZ(parent.d2InGlobal(), child.d2InGlobal()) ;
    Details::CheckXYZ(parent.d3InGlobal(), child.d3InGlobal()) ;
    Details::CheckXYZ(parent.d1InGlobal(), child.d1InParent()) ;
    Details::CheckXYZ(parent.d2InGlobal(), child.d2InParent()) ;
    Details::CheckXYZ(parent.d3InGlobal(), child.d3InParent()) ;
    // Check conversions from a frame to the other
    Geometry::Point pt(1., 2., 3.) ;
    Details::CheckXYZ(child.localToGlobal(pt), Geometry::Point(6., 8., 10.)) ;
    Details::CheckXYZ(child.localToParent(pt), pt) ;
    Details::CheckXYZ(child.parentToLocal(pt), pt) ;
    Details::CheckXYZ(child.globalToLocal(pt), Geometry::Point(-4., -4., -4.)) ;
    Details::CheckXYZ(parent.localToGlobal(pt), Geometry::Point(6., 8., 10.)) ;
    Details::CheckXYZ(parent.localToParent(pt), Geometry::Point(6., 8., 10.)) ;
    Details::CheckXYZ(parent.parentToLocal(pt), Geometry::Point(-4., -4., -4.)) ;
    Details::CheckXYZ(parent.globalToLocal(pt), Geometry::Point(-4., -4., -4.)) ;
    // Check again with another rotator
    child.setRotator(Geometry::RotatorTypeEnum::QUATERNION) ;
    Details::CheckXYZ(child.localToGlobal(pt), Geometry::Point(6., 8., 10.)) ;
    Details::CheckXYZ(child.localToParent(pt), pt) ;
    Details::CheckXYZ(child.parentToLocal(pt), pt) ;
    Details::CheckXYZ(child.globalToLocal(pt), Geometry::Point(-4., -4., -4.)) ;
    Details::CheckXYZ(parent.localToGlobal(pt), Geometry::Point(6., 8., 10.)) ;
    Details::CheckXYZ(parent.localToParent(pt), Geometry::Point(6., 8., 10.)) ;
    Details::CheckXYZ(parent.parentToLocal(pt), Geometry::Point(-4., -4., -4.)) ;
    Details::CheckXYZ(parent.globalToLocal(pt), Geometry::Point(-4., -4., -4.)) ;
    // Translate child
    child.translateWithTranslationInParent(Geometry::Vector(1., 1., 1.)) ;
    Details::CheckXYZ(child.localToGlobal(pt), Geometry::Point(7., 9., 11.)) ;
    Details::CheckXYZ(child.localToParent(pt), Geometry::Point(2., 3., 4.)) ;
    Details::CheckXYZ(child.parentToLocal(pt), Geometry::Point(0., 1., 2.)) ;
    Details::CheckXYZ(child.globalToLocal(pt), Geometry::Point(-5., -5., -5.)) ;
    // Rotate parent of 90° around z axis
    parent.addOtherRotationAtRight(Geometry::Rotation::BasisVectors(Geometry::Vector(0., 1., 0.),
                                                   Geometry::Vector(-1., 0., 0.),
                                                   Geometry::Vector(0., 0., 1.))) ;
    // Check directors are still the same between parent and child globally
    Details::CheckXYZ(parent.d1InGlobal(), child.d1InGlobal()) ;
    Details::CheckXYZ(parent.d2InGlobal(), child.d2InGlobal()) ;
    Details::CheckXYZ(parent.d3InGlobal(), child.d3InGlobal()) ;
    // However they have changed locally
    Details::CheckXYZ(child.d1InParent(), Geometry::Vector(1., 0., 0.)) ;
    Details::CheckXYZ(child.d2InParent(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(child.d3InParent(), Geometry::Vector(0., 0., 1.)) ;
    // // Now we check change of basis
    Details::CheckXYZ(parent.localToGlobal(pt), Geometry::Point(3., 7., 10.)) ;
    Details::CheckXYZ(parent.localToParent(pt), Geometry::Point(3., 7., 10.)) ;
    Details::CheckXYZ(parent.parentToLocal(pt), Geometry::Point(-4., 4., -4.)) ;
    Details::CheckXYZ(parent.globalToLocal(pt), Geometry::Point(-4., 4., -4.)) ;
    Details::CheckXYZ(child.localToGlobal(pt), Geometry::Point(2., 8., 11.)) ;
    Details::CheckXYZ(child.localToParent(pt), Geometry::Point(2., 3., 4.)) ;
    Details::CheckXYZ(child.parentToLocal(pt), Geometry::Point(0., 1., 2.)) ;
    Details::CheckXYZ(child.globalToLocal(pt), Geometry::Point(-5., 3., -5.)) ;
    // Try to set another parent to child (get rid of the other)
    ReferenceFrame otherParent ;
    otherParent.setTranslator(TranslatorTypeEnum::CARTESIAN, -1., -1., -1.) ;
    child.setParent(otherParent) ;
    // Check that parent now is otherParent
    Details::CheckXYZ(child.localToGlobal(pt), Geometry::Point(1., 2., 3.)) ;
    Details::CheckXYZ(child.localToParent(pt), Geometry::Point(2., 3., 4.)) ;
    Details::CheckXYZ(child.parentToLocal(pt), Geometry::Point(0., 1., 2.)) ;
    Details::CheckXYZ(child.globalToLocal(pt), Geometry::Point(1., 2., 3.)) ;
    // Check parent setting keeping relative position
    child.setParentKeepingPosition(parent) ;
    Details::CheckXYZ(child.localToGlobal(pt), Geometry::Point(1., 2., 3.)) ;
    Details::CheckXYZ(child.localToParent(pt), Geometry::Point(-4., 4., -4.)) ;
    Details::CheckXYZ(child.parentToLocal(pt), Geometry::Point(3., 7., 10.)) ;
    Details::CheckXYZ(child.globalToLocal(pt), Geometry::Point(1., 2., 3.)) ;
}

void ReferenceFrameToReferenceFrame(void)
{
    using Mechanics::ReferenceFrame ;
    using Geometry::Translation::Cartesian ;
    using BV::Geometry::TranslatorTypeEnum ;
    ReferenceFrame rf1 ;
    ReferenceFrame rf2 ;
    ReferenceFrame rf3 ;
    rf3.setParent(rf1) ;
    rf1.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 2., 3.) ;
    rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, 4., 5., 6.) ;
    Geometry::Point pt(1., 1., 1.) ;
    Details::CheckXYZ(rf1.localToFrame(pt, rf2), Geometry::Point(-2., -2., -2.)) ;
    Details::CheckXYZ(rf1.frameToLocal(pt, rf2), Geometry::Point(4., 4., 4.)) ;
    Details::CheckXYZ(rf2.localToFrame(pt, rf1), Geometry::Point(4., 4., 4.)) ;
    Details::CheckXYZ(rf2.frameToLocal(pt, rf1), Geometry::Point(-2., -2., -2.)) ;
    Details::CheckXYZ(rf3.localToFrame(pt, rf2), Geometry::Point(-2., -2., -2.)) ;
    Details::CheckXYZ(rf3.frameToLocal(pt, rf2), Geometry::Point(4., 4., 4.)) ;
}

void ReferenceFrameUnknownsConstraints(void)
{
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;
    // We are mainly going to change the rotators and use the
    // default Cartesian translator.
    ReferenceFrame rf ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, 0., 0., 0.) ;
    rf.setRotator(Geometry::RotatorTypeEnum::BASIS_VECTORS) ;
    // FIXME reference frame has evolved, it now has fixed
    // Cartesian translator and Quaternion rotator
    BOOST_ASSERT(rf.nUnknowns() == (3+4)) ;//(3+9)) ;
    BOOST_ASSERT(rf.nConstraints() == 1) ;//6) ;
    Eigen::VectorXd compUnknowns(Eigen::VectorXd::Zero(rf.nUnknowns())) ;
    // First unknowns concern the translator (3)
    compUnknowns(3) = 1. ;
    //compUnknowns(7) = 1. ;
    //compUnknowns(11) = 1. ;
    Eigen::VectorXd compConstraints(Eigen::VectorXd::Zero(rf.nConstraints())) ;
    Details::CheckVectorXd(rf.unknowns(), compUnknowns) ;
    Details::CheckVectorXd(rf.constraints(), compConstraints) ;
    // Try to change the unknowns
    Eigen::VectorXd tmp(compUnknowns) ; // Copy
    // First translate the frame
    tmp(0) = 1. ;
    tmp(1) = 2. ;
    tmp(2) = 3. ;
    rf.unknowns(tmp) ;
    // We check the translation by checking the change of basis of a point
    Details::CheckXYZ(rf.localToGlobal(Geometry::Point(1., 1., 1.)),
                      Geometry::Point(2., 3., 4.)) ;
    // Try to rotate the frame too of 90° around z axis
    Eigen::VectorXd tmp2(Eigen::VectorXd::Zero(rf.nUnknowns())) ; // Zeros
    tmp2(3) = std::sqrt(2.)/2. ;
    tmp2(6) = std::sqrt(2.)/2. ;
    rf.unknowns(tmp2) ;
    // We check the translation by checking the change of basis of a point
    Details::CheckXYZ(rf.localToGlobal(Geometry::Point(1., 1., 1.)),
                      Geometry::Point(-1., 1., 1.)) ;
    //// reset the unknowns
    rf.unknowns(compUnknowns) ;
    // check other rotators number of unknowns (refresh)
    // This is no more possible
    //rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE) ;
    //BOOST_ASSERT(rf.nUnknowns() == (3+4)) ;
    //BOOST_ASSERT(rf.nConstraints() == 1) ;
    //rf.setRotator(Geometry::RotatorTypeEnum::ROTATION_MATRIX) ;
    //BOOST_ASSERT(rf.nUnknowns() == (3+9)) ;
    //BOOST_ASSERT(rf.nConstraints() == 6) ;
    //rf.setRotator(Geometry::RotatorTypeEnum::QUATERNION) ;
    //BOOST_ASSERT(rf.nUnknowns() == (3+4)) ;
    //BOOST_ASSERT(rf.nConstraints() == 1) ;
    //rf.setRotator(Geometry::RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS) ;
    //BOOST_ASSERT(rf.nUnknowns() == (3+3)) ;
    //BOOST_ASSERT(rf.nConstraints() == 0) ;
    //rf.setRotator(Geometry::RotatorTypeEnum::EULER_ANGLES_ZYX_i) ;
    //BOOST_ASSERT(rf.nUnknowns() == (3+3)) ;
    //BOOST_ASSERT(rf.nConstraints() == 0) ;
}

void ReferenceFrameTranslatorAndRotatorGetSet(void)
{
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;
    using BV::Geometry::Rotation::RotationMatrix ;
    ReferenceFrame rf ;
    // Check rotator get/set
    Geometry::Rotation::BasisVectors bv(Geometry::Vector(0., 1., 0.),
                                        Geometry::Vector(-1., 0., 0.),
                                        Geometry::Vector(0., 0., 1.)) ;
    rf.setRotator(Geometry::RotatorTypeEnum::BASIS_VECTORS, bv) ;
    //BOOST_ASSERT(rf.nUnknowns() == (3+9)) ;
    //BOOST_ASSERT(rf.nConstraints() == 6) ;
    RotationMatrix rot(rf.getRotatorInParent()) ;
    Details::CheckXYZ(rot.d1(), bv.d1()) ;
    Details::CheckXYZ(rot.d2(), bv.d2()) ;
    Details::CheckXYZ(rot.d3(), bv.d3()) ;
    Details::CheckXYZ(rot.d1(), rf.d1InGlobal()) ;
    Details::CheckXYZ(rot.d2(), rf.d2InGlobal()) ;
    Details::CheckXYZ(rot.d3(), rf.d3InGlobal()) ;
    Geometry::Rotation::Quaternion q(bv) ; // Initialise quaternion from bv
    //rf.setRotatorType(Geometry::RotatorTypeEnum::QUATERNION) ;
    BOOST_ASSERT(rf.nUnknowns() == (3+4)) ;
    BOOST_ASSERT(rf.nConstraints() == 1) ;
    RotationMatrix rot2(rf.getRotatorInParent()) ;
    Details::CheckXYZ(rot2.d1(), bv.d1()) ;
    Details::CheckXYZ(rot2.d2(), bv.d2()) ;
    Details::CheckXYZ(rot2.d3(), bv.d3()) ;
    Details::CheckXYZ(rot2.d1(), rf.d1InGlobal()) ;
    Details::CheckXYZ(rot2.d2(), rf.d2InGlobal()) ;
    Details::CheckXYZ(rot2.d3(), rf.d3InGlobal()) ;
    Details::CheckXYZ(rot2.d1(), q.d1()) ;
    Details::CheckXYZ(rot2.d2(), q.d2()) ;
    Details::CheckXYZ(rot2.d3(), q.d3()) ;
    Geometry::Rotation::BasisVectors bv2(rot2.toBasisVectors()) ;
    Details::CheckXYZ(bv2.d1(), bv.d1()) ;
    Details::CheckXYZ(bv2.d2(), bv.d2()) ;
    Details::CheckXYZ(bv2.d3(), bv.d3()) ;
    // Mini check for translator
    Geometry::Translation::Cartesian td(1., 2., 3.) ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 2., 3.) ;
    Details::CheckXYZ(rf.originInGlobal(), td.toPoint()) ;
    //rf.setTranslatorType(TranslatorTypeEnum::CARTESIAN) ;
    Details::CheckXYZ(rf.originInGlobal(), td.toPoint()) ;

    //{
    //    ReferenceFrame rf ;
    //    rf.setRotatorType(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i) ;
    //    BOOST_CHECK(rf.getRotatorTypeEnum() == BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i) ;
    //    rf.setRotator(BV::Geometry::RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS) ;
    //    BOOST_CHECK(rf.getRotatorTypeEnum() == BV::Geometry::RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS) ;
    //}

    //{
    //    ReferenceFrame rf ;
    //    rf.setTranslatorType(BV::Geometry::TranslatorTypeEnum::HORIZONTAL) ;
    //    BOOST_CHECK(rf.getTranslatorTypeEnum() == BV::Geometry::TranslatorTypeEnum::HORIZONTAL) ;
    //    rf.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN) ;
    //    BOOST_CHECK(rf.getTranslatorTypeEnum() == BV::Geometry::TranslatorTypeEnum::CARTESIAN) ;
    //}
}

void ReferenceFrameGetGlobalRotatorAndTranslator(void)
{
    using Mechanics::ReferenceFrame ;
    using Geometry::Translation::Cartesian ;
    using Geometry::Rotation::Quaternion ;
    using Geometry::Rotation::BasisVectors ;
    using Geometry::Rotation::AxisAndAngle ;
    using BV::Geometry::TranslatorTypeEnum ;
    ReferenceFrame rf ;
    ReferenceFrame rf2 ;
    rf2.setParent(rf) ;
    rf.setRotator(Geometry::RotatorTypeEnum::QUATERNION) ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 2., 3.) ;
    rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE) ;
    rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 1., 1.) ;
    // Check translations
    Cartesian rfTrans(rf.getTranslatorInGlobal()) ;
    const Cartesian & rfTransInParent(rf.getTranslatorInParent()) ;
    Details::CheckXYZ(rfTrans.toPoint(), rf.originInGlobal()) ;
    Details::CheckXYZ(rfTransInParent.toPoint(), rf.originInGlobal()) ;
    Cartesian rf2Trans(rf2.getTranslatorInGlobal()) ;
    const Cartesian & rf2TransInParent(rf2.getTranslatorInParent()) ;
    Details::CheckXYZ(rf2Trans.toPoint(), Geometry::Point(2., 3., 4.)) ;
    Details::CheckXYZ(rf2TransInParent.toPoint(), rf2.originInParent()) ;
    // Check Rotations
    BasisVectors rfRot(rf.getRotatorInGlobal()) ;
    Details::CheckXYZ(rfRot.d1(), rf.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf.d3InGlobal()) ;
    BasisVectors rf2Rot(rf2.getRotatorInGlobal()) ;
    Details::CheckXYZ(rf2Rot.d1(), rf2.d1InGlobal()) ;
    Details::CheckXYZ(rf2Rot.d2(), rf2.d2InGlobal()) ;
    Details::CheckXYZ(rf2Rot.d3(), rf2.d3InGlobal()) ;

    // Rotate rf2 of 90° around z axis
    AxisAndAngle aa(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    rf2.addOtherRotationAtRight(aa) ;
    BasisVectors rf2Rot2(rf2.getRotatorInGlobal()) ;
    Details::CheckXYZ(rf2Rot2.d1(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(rf2Rot2.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf2Rot2.d3(), Geometry::Vector(0., 0., 1.)) ;
    Quaternion rf2RotInParent(rf2.getRotatorInParent()) ;
    Details::CheckXYZ(rf2RotInParent.d1(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(rf2RotInParent.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf2RotInParent.d3(), Geometry::Vector(0., 0., 1.)) ;

    // Then rotate rf2 of 90� around y axis
    AxisAndAngle aa2(Geometry::Vector(0., 1., 0.), M_PI/2.) ;
    AxisAndAngle aaFinal(aa*aa2) ;
    rf2.addOtherRotationAtRight(aa2) ;
    BasisVectors rf2Rot2_2(rf2.getRotatorInGlobal()) ;

    Details::CheckXYZ(rf2Rot2_2.d1(), Geometry::Vector(0., 0., -1.)) ;
    Details::CheckXYZ(rf2Rot2_2.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf2Rot2_2.d3(), Geometry::Vector(0., 1., 0.)) ;
    Quaternion rf2Rot2InParent(rf2.getRotatorInParent()) ;
    Details::CheckXYZ(rf2Rot2InParent.d1(), Geometry::Vector(0., 0., -1.)) ;
    Details::CheckXYZ(rf2Rot2InParent.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf2Rot2InParent.d3(), Geometry::Vector(0., 1., 0.)) ;

    Details::CheckXYZ(rf2Rot2InParent.d1(), aaFinal.d1()) ;
    Details::CheckXYZ(rf2Rot2InParent.d2(), aaFinal.d2()) ;
    Details::CheckXYZ(rf2Rot2InParent.d3(), aaFinal.d3()) ;

    // Then Rotate rf of 90° around z axis
    AxisAndAngle aaFinal2(aa*aaFinal) ;
    rf.addOtherRotationAtRight(aa) ;

    // Check rf rotators
    BasisVectors rfRot2(rf.getRotatorInGlobal()) ;
    Details::CheckXYZ(rfRot2.d1(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(rfRot2.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rfRot2.d3(), Geometry::Vector(0., 0., 1.)) ;

    Details::CheckXYZ(rfRot2.d1(), aa.d1()) ;
    Details::CheckXYZ(rfRot2.d2(), aa.d2()) ;
    Details::CheckXYZ(rfRot2.d3(), aa.d3()) ;

    Quaternion rfRotInParent(rf.getRotatorInParent()) ;
    Details::CheckXYZ(rfRotInParent.d1(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(rfRotInParent.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rfRotInParent.d3(), Geometry::Vector(0., 0., 1.)) ;

    Details::CheckXYZ(rfRotInParent.d1(), aa.d1()) ;
    Details::CheckXYZ(rfRotInParent.d2(), aa.d2()) ;
    Details::CheckXYZ(rfRotInParent.d3(), aa.d3()) ;

    // Check rf2 rotators
    BasisVectors rf2Rot3(rf2.getRotatorInGlobal()) ;
    Details::CheckXYZ(rf2Rot3.d1(), Geometry::Vector(0., 0., -1.)) ;
    Details::CheckXYZ(rf2Rot3.d2(), Geometry::Vector(0., -1., 0.)) ;
    Details::CheckXYZ(rf2Rot3.d3(), Geometry::Vector(-1., 0., 0.)) ;

    Details::CheckXYZ(rf2Rot3.d1(), aaFinal2.d1()) ;
    Details::CheckXYZ(rf2Rot3.d2(), aaFinal2.d2()) ;
    Details::CheckXYZ(rf2Rot3.d3(), aaFinal2.d3()) ;

    Details::CheckXYZ(rf2.d1InGlobal(), aaFinal2.d1()) ;
    Details::CheckXYZ(rf2.d2InGlobal(), aaFinal2.d2()) ;
    Details::CheckXYZ(rf2.d3InGlobal(), aaFinal2.d3()) ;

    Quaternion rf2RotInParent2(rf2.getRotatorInParent()) ;
    Details::CheckXYZ(rf2RotInParent2.d1(), Geometry::Vector(0., 0., -1.)) ;
    Details::CheckXYZ(rf2RotInParent2.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf2RotInParent2.d3(), Geometry::Vector(0., 1., 0.)) ;

    Details::CheckXYZ(rf2RotInParent2.d1(), aaFinal.d1()) ;
    Details::CheckXYZ(rf2RotInParent2.d2(), aaFinal.d2()) ;
    Details::CheckXYZ(rf2RotInParent2.d3(), aaFinal.d3()) ;

    Details::CheckXYZ(rf2.d1InParent(), aaFinal.d1()) ;
    Details::CheckXYZ(rf2.d2InParent(), aaFinal.d2()) ;
    Details::CheckXYZ(rf2.d3InParent(), aaFinal.d3()) ;
}

void ReferenceFrameGetRotatorAndTranslatorInFrame(void)
{
    using Mechanics::ReferenceFrame ;
    using Geometry::Translation::Cartesian ;
    using Geometry::Rotation::Quaternion ;
    using Geometry::Rotation::BasisVectors ;
    using Geometry::Rotation::AxisAndAngle ;
    using Geometry::Rotation::RotationMatrix ;
    using BV::Geometry::TranslatorTypeEnum ;
    ReferenceFrame rf ;
    ReferenceFrame rf2 ;
    ReferenceFrame rf3 ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 2., 3.) ;
    rf.setRotator(Geometry::RotatorTypeEnum::QUATERNION) ;
    rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, 1., 1., 1.) ;
    rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE) ;
    rf3.setParent(rf2) ;
    rf3.setTranslator(TranslatorTypeEnum::CARTESIAN, 2., 2., 2.) ;
    rf3.setRotator(Geometry::RotatorTypeEnum::ROTATION_MATRIX) ;
    // Check translations
    Cartesian rfTrans(rf.getTranslatorInFrame(rf2)) ;
    Details::CheckXYZ(rfTrans.toPoint(), Geometry::Point(0., 1., 2.)) ;
    Cartesian rf2Trans(rf2.getTranslatorInFrame(rf)) ;
    Details::CheckXYZ(rf2Trans.toPoint(), Geometry::Point(0., -1., -2.)) ;
    Cartesian rf3Trans(rf3.getTranslatorInFrame(rf)) ;
    Details::CheckXYZ(rf3Trans.toPoint(), Geometry::Point(2., 1., 0.)) ;
    Cartesian rf3Trans2(rf3.getTranslatorInFrame(rf2)) ;
    Details::CheckXYZ(rf3Trans2.toPoint(), Geometry::Point(2., 2., 2.)) ;
    Details::CheckXYZ(rf3Trans2.toPoint(), rf3.getTranslatorInParent().toPoint()) ;


    // Check translations
    using BV::Geometry::Point ;
    Point rfPt(rf.originInFrame(rf2)) ;
    Details::CheckXYZ(rfPt, Geometry::Point(0., 1., 2.)) ;
    Point rf2Pt(rf2.originInFrame(rf)) ;
    Details::CheckXYZ(rf2Pt, Geometry::Point(0., -1., -2.)) ;
    Point rf3Pt(rf3.originInFrame(rf)) ;
    Details::CheckXYZ(rf3Pt, Geometry::Point(2., 1., 0.)) ;
    Point rf3Pt2(rf3.originInFrame(rf2)) ;
    Details::CheckXYZ(rf3Pt2, Geometry::Point(2., 2., 2.)) ;
    Details::CheckXYZ(rf3Pt2, rf3.getTranslatorInParent().toPoint()) ;


    // Check Rotations
    BasisVectors rfRot(rf.getRotatorInFrame(rf2)) ;
    Details::CheckXYZ(rfRot.d1(), rf.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf.d3InGlobal()) ;
    Details::CheckXYZ(rfRot.d1(), rf2.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf2.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf2.d3InGlobal()) ;
    BasisVectors rfRot2(rf.getRotatorInFrame(rf3)) ;
    Details::CheckXYZ(rfRot.d1(), rf3.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf3.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf3.d3InGlobal()) ;
    BasisVectors rf2Rot(rf.getRotatorInFrame(rf2)) ;
    Details::CheckXYZ(rfRot.d1(), rf.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf.d3InGlobal()) ;
    Details::CheckXYZ(rfRot.d1(), rf2.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf2.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf2.d3InGlobal()) ;
    BasisVectors rf2Rot2(rf.getRotatorInFrame(rf3)) ;
    Details::CheckXYZ(rfRot.d1(), rf3.d1InGlobal()) ;
    Details::CheckXYZ(rfRot.d2(), rf3.d2InGlobal()) ;
    Details::CheckXYZ(rfRot.d3(), rf3.d3InGlobal()) ;
    // Rotate rf2 of 90° around z axis at right
    AxisAndAngle rf2Rotator(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    rf2.addOtherRotationAtRight(rf2Rotator) ;
    BasisVectors rf2Rot3(rf2.getRotatorInFrame(rf)) ;
    Details::CheckXYZ(rf2Rot3.d1(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(rf2Rot3.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf2Rot3.d3(), Geometry::Vector(0., 0., 1.)) ;
    BasisVectors rfRot3(rf.getRotatorInFrame(rf2)) ;
    Details::CheckXYZ(rfRot3.d1(), Geometry::Vector(0., -1., 0.)) ;
    Details::CheckXYZ(rfRot3.d2(), Geometry::Vector(1., 0., 0.)) ;
    Details::CheckXYZ(rfRot3.d3(), Geometry::Vector(0., 0., 1.)) ;
    BasisVectors rf3Rot(rf3.getRotatorInFrame(rf)) ;
    Details::CheckXYZ(rf3Rot.d1(), Geometry::Vector(0., 1., 0.)) ;
    Details::CheckXYZ(rf3Rot.d2(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf3Rot.d3(), Geometry::Vector(0., 0., 1.)) ;
    BasisVectors rfRot4(rf.getRotatorInFrame(rf3)) ;
    Details::CheckXYZ(rfRot4.d1(), Geometry::Vector(0., -1., 0.)) ;
    Details::CheckXYZ(rfRot4.d2(), Geometry::Vector(1., 0., 0.)) ;
    Details::CheckXYZ(rfRot4.d3(), Geometry::Vector(0., 0., 1.)) ;
    // Rotate rf3 of 90° around z axis at right
    AxisAndAngle rf3Rotator(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    rf3.addOtherRotationAtRight(rf3Rotator) ;
    BasisVectors rf3Rot2(rf3.getRotatorInFrame(rf)) ;
    Details::CheckXYZ(rf3Rot2.d1(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rf3Rot2.d2(), Geometry::Vector(0., -1., 0.)) ;
    Details::CheckXYZ(rf3Rot2.d3(), Geometry::Vector(0., 0., 1.)) ;
    BasisVectors rfRot5(rf.getRotatorInFrame(rf3)) ;
    Details::CheckXYZ(rfRot5.d1(), Geometry::Vector(-1., 0., 0.)) ;
    Details::CheckXYZ(rfRot5.d2(), Geometry::Vector(0., -1., 0.)) ;
    Details::CheckXYZ(rfRot5.d3(), Geometry::Vector(0., 0., 1.)) ;


    // Rotate rf of -260° around x axis at left
    AxisAndAngle rfRotator(Geometry::Vector(1., 0., 0.), -260. * M_PI/180.) ;
    rf.addOtherRotationAtLeft(rfRotator) ;

    // Rotate rf2 of 30° around y axis at left
    AxisAndAngle rf2Rotator2(Geometry::Vector(0., 1., 0.), 30. * M_PI/180.) ;
    rf2.addOtherRotationAtLeft(rf2Rotator2.toMRP()) ;

    // Rotate rf3 of 42.3° around y axis at left
    AxisAndAngle rf3Rotator2(Geometry::Vector(0., 1., 0.), 42.3 * M_PI/180.) ;
    rf3.addOtherRotationAtLeft(rf3Rotator2.toQuaternion()) ;

    AxisAndAngle compRot3To1(rfRotator.getInversed() * rf2Rotator2 * rf2Rotator * rf3Rotator2 * rf3Rotator) ;

    Quaternion rf3Rot3(rf3.getRotatorInFrame(rf)) ;
    Details::CheckXYZ(rf3Rot3.d1(), compRot3To1.d1()) ;
    Details::CheckXYZ(rf3Rot3.d2(), compRot3To1.d2()) ;
    Details::CheckXYZ(rf3Rot3.d3(), compRot3To1.d3()) ;

    Geometry::Point rfOriginInPar(rf.originInParent()) ;
    Geometry::Point rf2OriginInPar(rf2.originInParent()) ;
    Geometry::Point rf3OriginInPar(rf3.originInParent()) ;

    Cartesian rf3Trans3(rf3.getTranslatorInFrame(rf)) ;
    Geometry::Point expectedrf3OriginIn1( rfRotator.getInversed() * (- rfOriginInPar + rf2OriginInPar.toVector() + ( rf2Rotator2 * rf2Rotator * rf3OriginInPar).toVector() ) ) ;
    Geometry::Point localToFramerf3OriginIn1( rf2.localToFrame(rf3OriginInPar, rf) ) ;
    Details::CheckXYZ(rf3Trans3.toPoint(), expectedrf3OriginIn1) ;
    Details::CheckXYZ(rf3Trans3.toPoint(), localToFramerf3OriginIn1) ;


}

void ReferenceFrameCheckChangeOfBasis(void)
{
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;
    Geometry::Rotation::AxisAndAngle parentRot(Geometry::Vector(0., 0., 1.), M_PI/2.) ;
    Geometry::Rotation::AxisAndAngle childRot1(Geometry::Vector(0., 0., 1.), -M_PI/2.) ;
    Geometry::Rotation::AxisAndAngle childRot2(Geometry::Vector(0., 1., 1.), -120.*M_PI/180.) ;
    ReferenceFrame rf ;
    Geometry::Translation::Cartesian rfTranslator(1., 2., 3.) ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTranslator) ;
    rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, parentRot) ;
    // First check points change of basis
    Geometry::Point pt(4., 5., 6.) ;
    Geometry::Point pt1(rf.localToGlobal(pt)) ;
    Details::CheckXYZ(pt1, Geometry::Point(-4., 6., 9.)) ;
    Geometry::Point pt2(rf.globalToLocal(pt)) ;
    Details::CheckXYZ(pt2, Geometry::Point(3., -3., 3.)) ;
    // Then check vectors change of basis
    Geometry::Vector v(1., 1., 1.) ;
    Geometry::Vector v1(rf.localToGlobal(v)) ;
    Details::CheckXYZ(v1, Geometry::Vector(-1., 1., 1.)) ;
    Geometry::Vector v2(rf.globalToLocal(v)) ;
    Details::CheckXYZ(v2, Geometry::Vector(1., -1., 1.)) ;
    // add a child frame that matches global frame
    ReferenceFrame rf2 ;
    rf2.setParent(rf) ;
    Geometry::Translation::Cartesian rf2Translator(-2., 1., -3.) ;
    rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Translator) ;
    rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, childRot1) ;
    Details::CheckXYZ(rf2.localToParent(pt), pt2) ;
    Details::CheckXYZ(rf2.parentToLocal(pt), pt1) ;
    Details::CheckXYZ(rf2.localToParent(v), v2) ;
    Details::CheckXYZ(rf2.parentToLocal(v), v1) ;
    Details::CheckXYZ(rf2.localToFrame(pt, rf), pt2) ;
    Details::CheckXYZ(rf2.frameToLocal(pt, rf), pt1) ;
    Details::CheckXYZ(rf2.localToFrame(v, rf), v2) ;
    Details::CheckXYZ(rf2.frameToLocal(v, rf), v1) ;

    // Check with composed 3d rot
    ReferenceFrame rf3(rf2) ;
    rf3.addOtherRotationAtLeft(childRot2) ;
    Geometry::Rotation::AxisAndAngle compRotToGlobal(parentRot * childRot2 * childRot1) ;
    Geometry::Rotation::AxisAndAngle compRotToParent(childRot2 * childRot1) ;
    Geometry::Rotation::AxisAndAngle compRotToRF2(childRot1.getInversed() * childRot2 * childRot1) ;

    Details::CheckXYZ(rf3.localToParent(pt), compRotToParent * pt - parentRot.getInversed() * rfTranslator.toVector()) ;
    Details::CheckXYZ(rf3.parentToLocal(pt), compRotToParent.getInversed() * pt - compRotToParent.getInversed() * rf2Translator.toVector()) ;

    Details::CheckXYZ(rf3.localToGlobal(pt), compRotToGlobal * pt) ;
    Details::CheckXYZ(rf3.globalToLocal(pt), compRotToGlobal.getInversed() * pt) ;

    Details::CheckXYZ(rf3.localToFrame(pt, rf2), compRotToRF2 * pt) ;
    Details::CheckXYZ(rf3.frameToLocal(pt, rf2), compRotToRF2.getInversed() * pt) ;

}

void ReferenceFrameTranslateIn(void)
{
    using Geometry::Vector ;
    using Geometry::Translation::Cartesian ;
    using Geometry::Rotation::AxisAndAngle ;
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;

    // Check translateInGlobal with Vector
    {
        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Vector vToTranslate(-5., 3., 8.6) ;
        rf2.translateWithTranslationInGlobal(vToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + vToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * vToTranslate) ;

        Vector v2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInGlobal(v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + vToTranslate + v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * vToTranslate) ;

        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + v2ToTranslate) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + v2ToTranslate) ;
    }


    // Check translateInGlobal with Translation::ABC
    {
        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Cartesian cToTranslate(-6.1, 1., -0.6) ;
        rf2.translateWithTranslationInGlobal(cToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + cToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * cToTranslate.toVector()) ;

        Cartesian c2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInGlobal(c2ToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + cToTranslate.toVector() + c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * cToTranslate.toVector()) ;

        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + c2ToTranslate.toVector()) ;
    }


    // Check translateInLocal with Vector
    {
        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Vector vToTranslate(-5., 3., 8.6) ;
        rf2.translateWithTranslationInLocal(vToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * rf2Rot * vToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rf2Rot * vToTranslate) ;

        Vector v2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInLocal(v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * rf2Rot * vToTranslate + rfRot * v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rf2Rot * vToTranslate) ;


        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + rfRot * v2ToTranslate) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + rfRot * v2ToTranslate) ;
    }


    // Check translateInLocal with Translation::ABC
    {
        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Cartesian cToTranslate(-6.1, 1., -0.6) ;
        rf2.translateWithTranslationInLocal(cToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * rf2Rot * cToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rf2Rot * cToTranslate.toVector()) ;


        Cartesian c2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInLocal(c2ToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * rf2Rot * cToTranslate.toVector() + rfRot * c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rf2Rot * cToTranslate.toVector()) ;


        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + rfRot * c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + rfRot * c2ToTranslate.toVector()) ;
    }



    // Check translateInParent with Vector
    {
        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Vector vToTranslate(-5., 3., 8.6) ;
        rf2.translateWithTranslationInParent(vToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * vToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + vToTranslate) ;

        Vector v2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInParent(v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * vToTranslate + v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + vToTranslate) ;

        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + v2ToTranslate) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + v2ToTranslate) ;
    }
    // Check translateInParent with Translation::ABC
    {

        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Cartesian cToTranslate(-6.1, 1., -0.6) ;
        rf2.translateWithTranslationInParent(cToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * cToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + cToTranslate.toVector()) ;

        Cartesian c2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInParent(c2ToTranslate) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rfRot * cToTranslate.toVector() + c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + cToTranslate.toVector()) ;

        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + c2ToTranslate.toVector()) ;
    }


    // Check translateInFrame with Vector
    {
        // define a frame
        ReferenceFrame rf0 ;
        Cartesian rf0Trans(1.5, 2.5, 3.5) ;
        rf0.setTranslator(TranslatorTypeEnum::CARTESIAN, rf0Trans) ;
        AxisAndAngle rf0Rot(
                AxisAndAngle(Vector(0., 1., 0.), -250.*M_PI/180.) * AxisAndAngle(Vector(0., 0., 1.), 312.2*M_PI/180.)
                          ) ;
        rf0.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf0Rot) ;

        ReferenceFrame rf00 ;
        rf00.setParent(rf0) ;
        Cartesian rf00Trans(-1.5, -0.2, 4.5) ;
        rf00.setTranslator(TranslatorTypeEnum::CARTESIAN, rf00Trans) ;
        AxisAndAngle rf00Rot(
                AxisAndAngle(Vector(1., 0., 0.), -24.*M_PI/180.) * AxisAndAngle(Vector(0., 0., 1.), 45.9*M_PI/180.)
                          ) ;
        rf00.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf00Rot) ;
        //

        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Vector vToTranslate(-5., 3., 8.6) ;
        rf2.translateWithTranslationInFrame(vToTranslate, rf00) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rf0Rot * rf00Rot * vToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * rf0Rot * rf00Rot * vToTranslate) ;

        Vector v2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInFrame(v2ToTranslate, rf00) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rf0Rot * rf00Rot * vToTranslate + rf0Rot * rf00Rot * v2ToTranslate) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * rf0Rot * rf00Rot * vToTranslate) ;

        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + rf0Rot * rf00Rot * v2ToTranslate) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + rf0Rot * rf00Rot * v2ToTranslate) ;
    }

    // Check translateInFrame with Translation::ABC
    {
        // define a frame
        ReferenceFrame rf0 ;
        Cartesian rf0Trans(1.5, 2.5, 3.5) ;
        rf0.setTranslator(TranslatorTypeEnum::CARTESIAN, rf0Trans) ;
        AxisAndAngle rf0Rot(
                AxisAndAngle(Vector(0., 1., 0.), -250.*M_PI/180.) * AxisAndAngle(Vector(0., 0., 1.), 312.2*M_PI/180.)
                          ) ;
        rf0.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf0Rot) ;

        ReferenceFrame rf00 ;
        rf00.setParent(rf0) ;
        Cartesian rf00Trans(-1.5, -0.2, 4.5) ;
        rf00.setTranslator(TranslatorTypeEnum::CARTESIAN, rf00Trans) ;
        AxisAndAngle rf00Rot(
                AxisAndAngle(Vector(1., 0., 0.), -24.*M_PI/180.) * AxisAndAngle(Vector(0., 0., 1.), 45.9*M_PI/180.)
                          ) ;
        rf00.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf00Rot) ;
        //

        ReferenceFrame rf ;
        Cartesian rfTrans(1., 2., 3.) ;
        rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
        AxisAndAngle rfRot(
                AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                          ) ;
        rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

        ReferenceFrame rf2 ;
        rf2.setParent(rf) ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint()) ;

        Cartesian cToTranslate(-6.1, 1., -0.6) ;
        rf2.translateWithTranslationInFrame(cToTranslate, rf00) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rf0Rot * rf00Rot * cToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * rf0Rot * rf00Rot * cToTranslate.toVector()) ;

        Cartesian c2ToTranslate(-1., -2., -3.) ;
        rf.translateWithTranslationInFrame(c2ToTranslate, rf00) ;
        Details::CheckXYZ(rf2.originInGlobal(), rfTrans.toPoint() + rfRot * rf2Trans.toVector() + rf0Rot * rf00Rot * cToTranslate.toVector() + rf0Rot * rf00Rot * c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf2.originInParent(), rf2Trans.toPoint() + rfRot.getInversed() * rf0Rot * rf00Rot * cToTranslate.toVector()) ;

        Details::CheckXYZ(rf.originInGlobal(), rfTrans.toPoint() + rf0Rot * rf00Rot * c2ToTranslate.toVector()) ;
        Details::CheckXYZ(rf.originInParent(), rfTrans.toPoint() + rf0Rot * rf00Rot * c2ToTranslate.toVector()) ;
    }
}

void ReferenceFrameInverse(void)
{
    using Geometry::Vector ;
    using Geometry::Translation::Cartesian ;
    using Geometry::Rotation::AxisAndAngle ;
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;

    ReferenceFrame rf ;
    Cartesian rfTrans(1., 2., 3.) ;
    rf.setTranslator(TranslatorTypeEnum::CARTESIAN, rfTrans) ;
    AxisAndAngle rfRot(
            AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                      ) ;
    rf.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rfRot) ;

    // inverseInParent without parent
    {
        ReferenceFrame rfInv(rf.inverseInParent()) ;
        Details::CheckXYZ(rfInv.originInParent().toVector(), -rfTrans.toVector()) ;
        Details::CheckXYZ(rfInv.originInGlobal().toVector(), -rfTrans.toVector()) ;
        BOOST_CHECK(rfRot.getInversed() == rfInv.getRotatorInParent()) ;
        BOOST_CHECK( !(rfInv.hasParent())) ;
    }


    // inverseInParent with parent
    {
        ReferenceFrame rf2 ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;
        rf2.setParent(rf) ;

        ReferenceFrame rf2Inv(rf2.inverseInParent()) ;
        Details::CheckXYZ(rf2Inv.originInParent().toVector(), -rf2Trans.toVector()) ;
        BOOST_CHECK(rf2Rot.getInversed() == rf2Inv.getRotatorInParent()) ;
        BOOST_CHECK( rf2Inv.hasParent()) ;

        Details::CheckXYZ(rf2Inv.originInGlobal().toVector(), rfTrans.toVector() - rfRot * rf2Trans.toVector()) ;
        BOOST_CHECK(rf2Rot.getInversed() == rf2Inv.getRotatorInParent()) ;
        BOOST_CHECK(rfRot * rf2Rot.getInversed() == rf2Inv.getRotatorInGlobal()) ;
        BOOST_CHECK( rf2Inv.hasParent()) ;
    }

    // inverseInGlobal without parent
    {
        ReferenceFrame rfInv(rf.inverseInGlobal()) ;
        Details::CheckXYZ(rfInv.originInParent().toVector(), -rfTrans.toVector()) ;
        Details::CheckXYZ(rfInv.originInGlobal().toVector(), -rfTrans.toVector()) ;
        BOOST_CHECK(rfRot.getInversed() == rfInv.getRotatorInParent()) ;
        BOOST_CHECK(rfRot.getInversed() == rfInv.getRotatorInGlobal()) ;
        BOOST_CHECK( !(rfInv.hasParent())) ;
    }


    // inverseInGlobal with parent
    {
        ReferenceFrame rf2 ;
        Cartesian rf2Trans(-2., 8., 1.5) ;
        rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
        AxisAndAngle rf2Rot(
                AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                          ) ;
        rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;
        rf2.setParent(rf) ;

        BV::Geometry::Vector orInG(rf2.originInGlobal().toVector()) ;

        ReferenceFrame rf2Inv(rf2.inverseInGlobal()) ;
        Details::CheckXYZ(rf2Inv.originInGlobal().toVector(), -orInG) ;
        Details::CheckXYZ(rf2Inv.originInGlobal().toVector(), -rfTrans.toVector() - rfRot * rf2Trans.toVector()) ;
        BOOST_CHECK(rf2Rot.getInversed() * rfRot.getInversed() == rf2Inv.getRotatorInGlobal()) ;
        BOOST_CHECK( rf2Inv.hasParent()) ;
    }
}

void ReferenceFrameOperatorMinus(void)
{
    // test operator- between two reference frames
    // this operator is used to compute the values of the translator and rotator
    // of one reference frame with respect to the other
    // it is theoretically equivalent to the translatorInParent & rotatorInParent
    // computed with the method setParentKeepingPosition

    using Geometry::Vector ;
    using Geometry::Translation::Cartesian ;
    using Geometry::Rotation::AxisAndAngle ;
    using Mechanics::ReferenceFrame ;
    using BV::Geometry::TranslatorTypeEnum ;

    // Build a first reference frame
    ReferenceFrame rf1 ;
    Cartesian rf1Trans(1., 2., 3.) ;
    rf1.setTranslator(TranslatorTypeEnum::CARTESIAN, rf1Trans) ;
    AxisAndAngle rf1Rot(
            AxisAndAngle(Vector(1., 0., 0.), 10.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), -50.*M_PI/180.)
                      ) ;
    rf1.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf1Rot) ;

    // Build a second reference frame
    ReferenceFrame rf2 ;
    Cartesian rf2Trans(-2., 8., 1.5) ;
    rf2.setTranslator(TranslatorTypeEnum::CARTESIAN, rf2Trans) ;
    AxisAndAngle rf2Rot(
            AxisAndAngle(Vector(0., 0., 1.), 8.*M_PI/180.) * AxisAndAngle(Vector(0., 1., 0.), 120.*M_PI/180.)
                      ) ;
    rf2.setRotator(Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, rf2Rot) ;

    // test to express rf2-rf1
    // this operation express the translator of rf2 with respect to rf1
    // this operation express the rotator of rf2 with respect to rf1
    {
        // the delta translation expressed in rf1 basis
        Vector expectedTransInGlobalBasis(rf2.originInGlobal() - rf1.originInGlobal()) ;
        Vector expectedTransInRF1Basis(rf1.globalToLocal(expectedTransInGlobalBasis)) ;
        // the rotation which put a vector in 2's basis to 1's basis
        // by the composition R_Global->1 * R_2->Global = R_1->Global.transpose() * R_2->Global
        AxisAndAngle expectedRotation(rf1Rot.getInversed() * rf2Rot) ;

        ReferenceFrame rf2On1(rf2-rf1) ;

        // Check the translator
        Details::CheckXYZ(expectedTransInRF1Basis, rf2On1.originInParent().toVector()) ;
        // Check rotator
        BOOST_CHECK(expectedRotation == rf2On1.getRotatorInParent()) ;

        // Check that we have the same results by putting rf1 as parent of rf2 but keeping position
        ReferenceFrame rf3(rf2) ;
        rf3.setParentKeepingPosition(rf1) ;
        Details::CheckXYZ(expectedTransInRF1Basis, rf3.originInParent().toVector()) ;
        BOOST_CHECK(expectedRotation == rf3.getRotatorInParent()) ;
    }


    {
        ReferenceFrame rf3(rf2) ;
        AxisAndAngle rf3Rot(rf2Rot) ;
        rf3.setParent(rf1) ;

        ReferenceFrame rf4(rf1) ;
        AxisAndAngle rf4Rot(rf1Rot) ;
        rf4.setParent(rf2) ;

        // the delta translation expressed in rf3 basis
        Vector expectedTransInGlobalBasis(rf4.originInGlobal() - rf3.originInGlobal()) ;
        Vector expectedTransInRF3Basis(rf3.globalToLocal(expectedTransInGlobalBasis)) ;
        // the rotation which put a vector in 4's basis to 3's basis
        // by the composition R_1->3 * R_Global->1 * R_2->Global * R_4->2
        AxisAndAngle expectedRotation(rf3Rot.getInversed() * rf1Rot.getInversed() * rf2Rot * rf4Rot) ;

        ReferenceFrame rf4On3(rf4-rf3) ;

        // Check the translator (delta translation expressed in rf1 basis)
        Details::CheckXYZ(expectedTransInRF3Basis, rf4On3.originInParent().toVector()) ;
        // Check rotator
        BOOST_CHECK(expectedRotation == rf4On3.getRotatorInParent()) ;

        // Check that we have the same results by putting rf1 as parent of rf2 but keeping position
        ReferenceFrame rf5(rf4) ;
        rf5.setParentKeepingPosition(rf3) ;
        Details::CheckXYZ(expectedTransInRF3Basis, rf5.originInParent().toVector()) ;
        BOOST_CHECK(expectedRotation == rf5.getRotatorInParent()) ;
    }
}

} // end of namespace Tests
} // end of namespace Mechanics
} // end of namespace BV
