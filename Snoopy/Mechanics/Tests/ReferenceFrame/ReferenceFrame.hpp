#pragma once
#ifndef __BV_Mechanics_Tests_ReferenceFrame_ReferenceFrame_hpp__
#define __BV_Mechanics_Tests_ReferenceFrame_ReferenceFrame_hpp__

namespace BV {
namespace Mechanics {
namespace Tests {

void ReferenceFrameConstruction(void) ;
void ReferenceFrameSetTranslator(void) ;
void ReferenceFrame(void) ;
void DefaultDirectors(void) ;
void ReferenceFrameParent(void) ;
void ReferenceFrameToReferenceFrame(void) ;
void ReferenceFrameUnknownsConstraints(void) ;
void ReferenceFrameTranslatorAndRotatorGetSet(void) ;
void ReferenceFrameGetGlobalRotatorAndTranslator(void) ;
void ReferenceFrameCheckChangeOfBasis(void) ;
void ReferenceFrameGetRotatorAndTranslatorInFrame(void) ;
void ReferenceFrameGetTranslatorAndRotator(void) ;
void ReferenceFrameTranslateIn(void) ;
void ReferenceFrameInverse(void) ;
void ReferenceFrameOperatorMinus(void) ;

} // End of namespace Tests
} // End of namespace Mechanics
} // End of namespace BV

#endif // __BV_Mechanics_Tests_ReferenceFrame_ReferenceFrame_hpp__
