#!/usr/bin/env python3
import os
from glob import glob
from Snoopy.Mechanics import McnInput, TEST_DATA

def test_read_mcn():
    # Simply try to read all the input file in ./test_data/inputs
    files_to_parse = glob(os.path.join(TEST_DATA,'Hydrostar','*.mcn'))
    for file in files_to_parse:
        test = McnInput.read_mcn(file)

if __name__ == '__main__':
    test_read_mcn()
