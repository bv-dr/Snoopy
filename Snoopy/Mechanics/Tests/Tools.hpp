#pragma once
#ifndef __BV_Mechanics_Tests_Tools_hpp__
#define __BV_Mechanics_Tests_Tools_hpp__

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

namespace BV {
namespace Mechanics {
namespace Tests {
namespace Details {

const double eps( 1.e-10 ) ;

template <typename XYZ>
void CheckXYZ(const XYZ & xyz1, const XYZ & xyz2)
{
    BOOST_CHECK_SMALL(Distance(xyz1, xyz2), eps) ;
}

void CheckVectorXd(const Eigen::VectorXd & v1, const Eigen::VectorXd & v2) ;

Eigen::Matrix3d GetRXMatrix(const double & angle) ;
Eigen::Matrix3d GetRYMatrix(const double & angle) ;
Eigen::Matrix3d GetRZMatrix(const double & angle) ;

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Mechanics
} // End of namespace BV

#endif // __BV_Mechanics_Tests_Torsor_Tools_hpp__
