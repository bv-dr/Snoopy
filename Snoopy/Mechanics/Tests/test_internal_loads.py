"""Compare HydroStar and Snoopy mechanical feature

Compute motion, internal loads using Snoopy and compare with HydroStar results


Snoopy input
    - Decomposed pressure on mesh
    - File with longitudinal mass distribution

Comparison of 
    - Hydrodynamic coefficients
    - Motions
    - Internal loads

"""

import os
import numpy as np
from matplotlib import pyplot as plt

from Snoopy import Spectral as sp
from Snoopy import Meshing as msh
from Snoopy import Mechanics as mcn
from Snoopy import logger
from Snoopy.Mechanics import McnCoef, MechanicalSolver, McnInput, TEST_DATA
from Snoopy.Mechanics.internal_load import get_rdfcoef_from_mesh, InternalLoad

from Snoopy.Mechanics.mass_section import AllSections
logger.setLevel(10)


def compare_6_dof( snoopy, hydrostar, title, display = True , fun = np.abs, rtol = 0.001 , atol = 0.001) :

    if display : 
        fig, ax = plt.subplots( nrows = 2, ncols = 3)
        for idof in range(0,6):
            fun(snoopy).sel(mode = idof+1).plot(ax=ax[ idof//3 , idof%3 ], label = "Snoopy IL", marker = "+")
            fun(hydrostar).sel(mode = idof+1) .plot(ax=ax[ idof//3 , idof%3 ], label = "HydroStar")
        ax[0,0].legend()
        fig.suptitle(title)
        plt.show()

    for idof in range(0,6):
        snoop = fun( snoopy ).sel( mode = idof+1)
        hstar = fun( hydrostar ).sel( mode = idof+1)
        assert( np.isclose( snoop, hstar , atol = atol , rtol = rtol ).all() )




def test_internal_loads(display = False):
    head = 0.

    pressure_mesh = msh.Mesh.ReadHdf( f"{mcn.TEST_DATA:}/Hydrostar/hdf/Wigley4_prs.hdf" )

    mcn_coef_hydrostar = McnCoef.read( f"{mcn.TEST_DATA:}/Hydrostar/hdf/hsmcn_Wigley4.h5" , format_version = "auto")

    speed = 1.62715088421
    rho = 1000.

    # ------------- Integrate hydro coef with Snoopy and compare to HydroStar
    #print('mcn_coef_hydrostar.cog[0,:]:',mcn_coef_hydrostar.cog[0,:])
    rdf_coef_snoopy = get_rdfcoef_from_mesh( pressure_mesh, speed = speed, rho = rho).hydro.get_at_ref_point(mcn_coef_hydrostar.cog [0,:] )


    rdf_coef_snoopy = rdf_coef_snoopy.hydro.get_at_ref_point( mcn_coef_hydrostar.cog[0,:]  )

    compare_6_dof( snoopy = rdf_coef_snoopy.excitation_load.sel(body = 1 , heading = head ) ,
                   hydrostar = mcn_coef_hydrostar.excitation_load.sel(body = 1 , heading = head ),
                   fun = np.real,
                   title = "Excitation", rtol = 0.01, display = display)
       
    
    #---- Read .wld and calculate inertia per sections
    wld_don = AllSections.from_wld(  f"{mcn.TEST_DATA:}/Hydrostar/Wigley4.wld" , dispatch_load=(-1.5 , 1.5))
    wld_don.write_don(  f"{mcn.TEST_DATA:}/Hydrostar/test_out_wld.don" )
    
    
    
    mcn_input = McnInput.read_mcn( os.path.join(TEST_DATA,"Hydrostar","Wigley4.mcn") )
    mcn_coef_snoopy = MechanicalSolver( mcn_input, rdf_coef_snoopy ).mcn_coef
    
    # We can use either mcn_coef_hydrostar or mcn_coef_snoopy in internal load here
    internal_loads_db = InternalLoad(pressure_mesh, wld_don, mcn_coef = mcn_coef_snoopy, speed = speed , rho = rho)
    
    motion_snoopy = internal_loads_db.get_motion_da()
    
    loads_snoopy = internal_loads_db.get_internal_loads_ds()
    #internal_loads_db.write("internal_load_snoopy.h5")
    
    #---- Compare motion with HydroStar    
    compare_6_dof( snoopy = motion_snoopy.sel(body = 1 , heading = head ) ,
                   hydrostar = mcn_coef_hydrostar.motion.sel(body = 1 , heading = head ),
                   title = "Motion", rtol = 0.01, atol= 0.01, display = display)

    loads_snoopy["abs"] =  np.abs(loads_snoopy.internal_loads)
    
    #---- Compare internal loads with HydroStar, to do add an assert, not only a plot!

    # Internal loads
    hstar_midship = [ sp.Rao( f"{mcn.TEST_DATA:}/Hydrostar/rao/{str_dof:}_midship.rao" ) for str_dof in ["fx" ,"fy", "fz", "mx" , "my" , "mz"]  ]
    
    if display : 
        fig, ax = plt.subplots( nrows = 2, ncols = 3)
        
    for idof in range(0,6):
        loads_snoopy_df = loads_snoopy.sel(xyz = "x", heading = head,mode = idof +1,drop = True).to_dataframe().reset_index()
        snoopy_midship = loads_snoopy_df.loc[loads_snoopy_df.section_points == 0]

        if display : 
            snoopy_midship.plot(x = "frequency", y = "abs",ax = ax[ idof//3 , idof%3 ], label = "Snoopy")
            hstar_midship[idof].plot(headingsDeg=[head], ax=ax[ idof//3 , idof%3 ], label_prefix = "HydroStar")

        # assert:
        ihead = np.where(hstar_midship[idof].headDeg == head)[0]
        hstar_values = hstar_midship[idof].module[ihead,:,0]
        snoopy_values = snoopy_midship["abs"]
        snoopy_values = snoopy_values.values
        rel_diff = np.max(np.abs(hstar_values - snoopy_values)) / np.max(np.abs(hstar_values))
        #print('Relative diff:',idof, rel_diff)
        #assert np.allclose(snoopy_midship["abs"],hstar_value, rtol = 1e-2), 
        assert rel_diff<1e-1, f'Significant relative differences: {rel_diff}, at iDoF ={idof}'
        
        
    if display : 
        ax[0,0].legend()
        fig.suptitle("Internal loads at midship")
        plt.show()

    hstar_f4 = [ sp.Rao( f"{mcn.TEST_DATA:}/Hydrostar/rao/{str_dof:}_f6.rao" ).toDataFrame() for str_dof in ["fx" ,"fy", "fz", "mx" , "my" , "mz"]  ]
    
    if display : 
        fig, ax = plt.subplots( nrows = 2, ncols = 3)
        
    for idof in range(0,6):
        select_frequency = 6
        
        loads_snoopy_df = loads_snoopy.sel(xyz = "x", heading = head,mode = idof +1, frequency = select_frequency).to_dataframe()
        hstar_sel = np.abs(hstar_f4[idof]).loc[:,np.deg2rad(head)]

        if display : 
            current_ax = ax[ idof//3 , idof%3 ]
            loads_snoopy_df.plot(x ="section_points" , y = "abs",
                                 ax = current_ax, label = "Snoopy", 
                                 marker = "+")
            hstar_sel.plot( ax = current_ax, label = "HydroStar")
            current_ax.set_xlabel("SectionX")
            
        hstar_values = hstar_sel.values
        snoopy_values = loads_snoopy_df["abs"].values
        rel_diff = np.max(np.abs(hstar_values - snoopy_values)) / np.max(np.abs(hstar_values))
        #print('Relative diff:',idof, rel_diff)
        assert rel_diff<5e-2, f'Significant relative differences: {rel_diff}, at iDoF ={idof}'

    if display : 
        ax[0,0].legend()
        fig.suptitle(f"Internal loads along ship hull at frequency = {select_frequency}")
        plt.show()


if __name__ == "__main__":
    test_internal_loads(display = True)
    

