import numpy

from Snoopy.Mechanics.ReferenceFrameTools import MovingFrameAtLocalPoint

def testPointAt0():
    data = numpy.loadtxt("frameTS.dat")
    newTS = MovingFrameAtLocalPoint(data, (0., 0., 0.))
    assert(numpy.all(numpy.abs(data - newTS) < 1.e-8))


def testConversionLoop():
    data = numpy.loadtxt("frameTS.dat")
    newTS = MovingFrameAtLocalPoint(data, (10., 20., 30.))
    oldTS = MovingFrameAtLocalPoint(newTS, (-10., -20., -30.))
    assert(numpy.all(numpy.abs(oldTS - data) < 1.e-8))


def test2DFrame():
    data = numpy.zeros((100, 6), dtype=float)
    instants = numpy.arange(100)
    data[:, 0] = 4. * numpy.sin(0.5*instants)
    data[:, 1] = 6. * numpy.cos(0.5*instants)
    data[:, 5] = 0.1 * numpy.sin(0.05*instants)
    newTS = MovingFrameAtLocalPoint(data, (1., 2., 3.))
    ref = numpy.zeros((100, 6), dtype=float)
    ref[:, 0] = data[:, 0] + 1. * numpy.cos(data[:, 5]) - 2. * numpy.sin(data[:, 5])
    ref[:, 1] = data[:, 1] + 1. * numpy.sin(data[:, 5]) + 2. * numpy.cos(data[:, 5])
    ref[:, 2] = 3.
    ref[:, 5] = data[:, 5]
    assert(numpy.all(numpy.abs(newTS - ref) < 1.e-8))

def test2DFrameXZ():
    data = numpy.zeros((100, 6), dtype=float)
    instants = numpy.arange(100)
    data[:, 0] = 4. * numpy.sin(0.5*instants)
    data[:, 2] = 2. * numpy.sin(0.2*instants)
    data[:, 4] = 0.1 * numpy.sin(0.05*instants)
    newTS = MovingFrameAtLocalPoint(data, (0., 0., 3.))
    ref = numpy.zeros((100, 6), dtype=float)
    ref[:, 0] = data[:, 0] + 3. * numpy.sin(data[:, 4])
    ref[:, 2] = data[:, 2] + 3. * numpy.cos(data[:, 4])
    ref[:, 4] = data[:, 4]
    assert(numpy.all(numpy.abs(newTS - ref) < 1.e-8))