#!/usr/bin/env python3
from os.path import join,isfile
import subprocess
import sys
import numpy as np
from Snoopy.Mechanics.mass_distribution import LinearForm,MassElementTriLinCont,MassElementTriLin,MassElementPlan
from Snoopy.Mechanics.mass_section import AllSections,write_matrix
from Snoopy.Mechanics import TEST_DATA

test_data_dir = TEST_DATA + "/mass_distribution"

def check_change(filename):
    import shutil
    savefile = filename + "_save"
    if isfile(savefile):
        import filecmp
        if filecmp.cmp(filename, savefile):
            return False

    shutil.copyfile(filename,savefile)
    return True

def _test_linear_form1(x1     = 0,
                       x2     = 1,
                       coef   = -2,
                       intercept   = 2):

    a = LinearForm.solve_for_barycenter(x1,x2,coef,intercept)

    b = LinearForm.solve_for_coef(x1,x2,a.barycenter)
    assert np.allclose(b.coef/b.intercept*intercept,coef)
    c = LinearForm.solve_for_barycenter(x1,x2,b.coef,b.intercept)
    assert np.allclose(a.barycenter, b.barycenter)
    assert np.allclose(c.barycenter, b.barycenter)

def _test_linear_form2(x1     = 22.4,
                       x2     = 37.6,
                       xg     = 31.48  ):

    a = LinearForm.solve_for_coef(x1,x2,xg)
    b = LinearForm.solve_for_barycenter(x1,x2,a.coef,a.intercept)

    assert np.allclose(a.barycenter, b.barycenter)



def _test_MassElement(cls,
                    x1 = -10,
                    x2 = 10,
                    xg = 0,
                    y1 = -1,
                    y2 = 1,
                    yg = 0,
                    z1 = -1,
                    z2 = 1,
                    zg = 0,
                    mass = 10):
    print("-----------------------------")
    name = cls.__name__
    print('--> Test class :',name)

    item = cls(mass,x1,x2,xg,y1,y2,yg,z1,z2,zg)
    # Test translation:
    dx = 10
    dy = 100
    dz = 1000

    item2 = cls(mass,x1+dx,x2+dx,xg+dx,y1+dy,y2+dy,yg+dy,z1+dz,z2+dz,zg+dz)
    
    assert np.allclose(item.inertia_matrix_at_CoG,item2.inertia_matrix_at_CoG), "Test translation failed"
    print(f'Test {name} for translation: ok')
    

   
    # Test cut in half
    leftHalf,rightHalf = item.cutX(0)
    #debug_linear_form(leftHalf.linear_x)


    sum = leftHalf + rightHalf
    show_diff_matrix("Org","Cut and recombine",item.inertia_matrix,sum.inertia_matrix)    

    
    assert sum == item , f"Test {name} for cut and recombine failed"
    print(f'Test {name} for cut and recombine: ok')
    return item
def _compare_MassElementList(elementList):
    nb_elem = len(elementList)
    for ii in range(nb_elem):
        for jj in range(ii+1,nb_elem):
            _compare_MassElement(elementList[ii],elementList[jj])

def show_diff_matrix(name1,name2,val1,val2):
    if not np.allclose(val1,val2):
        print('--------->',name1)
        print(write_matrix(val1))
        print('--------->',name2)
        print(write_matrix(val2))
        print('-------------------> Different')
        print(write_matrix(val1-val2))
def _compare_MassElement(elem1,elem2):
    print("-----------------------------")
    name1 = elem1.__class__.__name__
    name2 = elem2.__class__.__name__
    random_point = np.random.rand(3)
    random_coef = np.random.random()
    x1 = elem1.x1
    x2 = elem2.x2
    X = x1 + (x2-x1)*random_coef/2
    X = 0.1
    assert elem1 == elem2, "Elements yeild different inertia property"
    val1 = elem1.get_inertia_matrix_at(random_point)
    val2 = elem2.get_inertia_matrix_at(random_point)
    if not np.allclose(val1,val2):
        print("Error in computing inertia matrix at random point")
        print('->',elem1,elem2)
        show_diff_matrix(elem1.__class__.__name__,
                         elem2.__class__.__name__,
                         val1,val2)
        
        assert False
    amont1,aval1 = elem1.cutX(X)
    amont2,aval2 = elem2.cutX(X)
    #show_diff_matrix(name1,name2,amont1.inertia_matrix,amont2.inertia_matrix)    
    assert amont1 == amont2, "Error in comparing amont cut"
    #show_diff_matrix(name1,name2,aval1.inertia_matrix,aval2.inertia_matrix)    
    assert aval1 == aval2, "Error in comparing aval cut"
    print(f'--> Test compare {name1} vs {name2}: ok')




#------------------------------------------------------------------
def test_linear_form():
    kwargs = {}
    _test_linear_form1(**kwargs)
    kwargs["x1"] = 25.66
    kwargs["x2"] = 37.6
    kwargs["xg"] = 33.62
    _test_linear_form2(**kwargs)
    kwargs["x1"] = -1.826
    kwargs["x2"] = -0.826
    kwargs["xg"] = -1.326
    _test_linear_form2(**kwargs)
    kwargs["x1"] = 7
    kwargs["x2"] = 12
    kwargs["xg"] = 9.674
    _test_linear_form2(**kwargs)
    print(f'--> Test linear form : ok')

def test_mass_element():
    test_kwargs = {}
    tobeTested = [MassElementTriLin,MassElementTriLinCont,MassElementPlan]
    #tobeTested = [MassElementTriLin,MassElementTriLinCont]
    collect_element = []
    for cls in tobeTested:
        collect_element.append(_test_MassElement(cls,**test_kwargs))
    _compare_MassElementList(collect_element)

def test_mass_section():
    print(f'--> Test IO: read XXX.wld --> write XXX_wld.don')
    hswld_path = join(test_data_dir,"Full.wld")
    print('\tRead wld ok!')
    obj = AllSections.from_wld(hswld_path)
    don_name = join(test_data_dir,"Full_wld.don")
    obj.write_don(don_name)
    print(f'\t Write don to: {don_name} ok')


    print(f'--> Test IO 2: read REF_wld.don produced by hstat and compare with Full_wld.don produced by this routine ')
    print("-----> Check ",hswld_path)
    isChanged = check_change(hswld_path)
    if isChanged:
        print('Something changed in file {hswld_path}! hstat need to be rerun!')
        try:
            with open(join(test_data_dir,"log_hstat.txt"),"w") as f:
                subprocess.call(join(test_data_dir,"run_hstat.bat"),cwd = test_data_dir,stdout=f)
            print('Rerun hstat with success!')
        except:
            pass

    print("-----> Compare file ")
    don_ref_name = join(test_data_dir,"Ref_wld.don")
    obj1 = AllSections.read_don(don_name)
    obj2 = AllSections.read_don(don_ref_name)
    
    isEqual =  obj1._compare(obj2,check_level = 2)
    if isEqual:
        print('--> Test IO 2: 2 file yield the same mass section')



def test_hstar_reader( ):
    """Read Hstar format. 
    
    For now, just check that it does not crash
    """
    from Snoopy.Mechanics.mass_distribution import read_hstar_mass_item, CollectMassElement
    test = read_hstar_mass_item( "5	 N.2H.F.OTK(P)	 779750.0	 22.4	 37.6	 30.21	 15.4682042914	 16.626" )
    
    test = read_hstar_mass_item( "76	 	 203000.0	 -3.0	 173.0	 75.75	 9.85021375403	 9.82 " )
    
    test_wld = CollectMassElement.from_wld( join(test_data_dir,"Full.wld") )
    
    assert(True)

 
if __name__ == "__main__":
    test_linear_form()
    test_mass_element()
    test_mass_section()
    test_hstar_reader()
    

    
    
    
    

