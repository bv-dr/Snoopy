#!/usr/bin/env python3
from os.path import join
import numpy as np
from Snoopy.Mechanics import TEST_DATA, RdfCoef, McnCoef, McnInput



def test_mcn_coef():
    hsrdf_path = join(TEST_DATA,"Hydrostar","hdf","hsrdf_Wigley4.rd1.h5")
    hsmcn_path = join(TEST_DATA,"Hydrostar","hdf","hsmcn_Wigley4.h5")
    mcninput_path = join(TEST_DATA,"Hydrostar","Wigley4.mcn")
    
    hsrdf_obj    = RdfCoef.read(hsrdf_path, format_version = "auto")
    hsmcn_obj    = McnCoef.read(hsmcn_path, format_version = "auto")
    
    mcninput_obj = McnInput.read_mcn(mcninput_path)

    _test_move_ref_point(hsrdf_obj,hsmcn_obj)
    _test_input_output(hsmcn_obj,mcninput_obj)


def _test_input_output(hsmcn_obj,mcninput_obj):
    """Hsmcn directly output some of its input to hdf
    We can check if it consistant with the parser
    Parameters
    ----------
    hsmcn_obj : McnCoef
        Hold the results from hsmcn
    mcninput_obj : McnInput
        Hold the input of hsmcn
    """
    hsmcn_obj = hsmcn_obj.hydro.get_at_ref_point(mcninput_obj.cog[0,:])
    assert np.allclose(hsmcn_obj.mass_matrix,mcninput_obj.mass_matrix),\
        "Different in mass matrix"
    


def _test_move_ref_point(hsrdf_obj,hsmcn_obj):
    """ Test if moving ref_point of hsrdf_obj to ref_point
    of hsmcn_obj, the results would yeild the same
    """
    # Change ref_point of hsrdf_obj to match ref_point of hsmcn_obj
    hsrdf_obj = hsrdf_obj.hydro.get_at_ref_point ( hsmcn_obj.ref_point[0,:] )
    
    # Compare the value
    assert np.allclose(hsrdf_obj.added_mass,hsmcn_obj.added_mass),\
        "Different in added mass"
    assert np.allclose(hsrdf_obj.wave_damping,hsmcn_obj.wave_damping),\
        "Different in wave damping"
    assert np.allclose(hsrdf_obj.excitation_load,hsmcn_obj.excitation_load),\
        "Different in excitation"


if __name__ == "__main__":
    test_mcn_coef()
