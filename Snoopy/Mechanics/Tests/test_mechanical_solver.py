#!/usr/bin/env python3
from os.path import join
import numpy as np
import xarray as xr
from Snoopy import logger
from Snoopy.Mechanics import TEST_DATA, MechanicalSolver, RdfCoef, McnCoef, McnInput
from Snoopy import Meshing as msh

logger.setLevel(10)


rdf_hdf   = join(TEST_DATA,"Hydrostar","hdf","hsrdf_Wigley4.rd1.h5")
mcn_hdf   = join(TEST_DATA,"Hydrostar","hdf","hsmcn_Wigley4.h5")
mcn_input_file = join(TEST_DATA,"Hydrostar","Wigley4.mcn")

mesh_file = join(TEST_DATA,"Hydrostar","Wigley4.hst")

def test_mechanical_solver(display = False):

          
    rdf_coef = RdfCoef.read(rdf_hdf, format_version = "auto")
    mcn_coef = McnCoef.read(mcn_hdf, format_version = "auto")
    mcn_input = McnInput.read_mcn( mcn_input_file )


    mcnSolver = MechanicalSolver( mcn_input, rdf_coef )


    # Default option, equation in solved at CoG
    motion_snoopy = mcnSolver.solve(  ) # Solve at CoG
    motion_snoopy = motion_snoopy.hydro.get_at_ref_point( mcn_coef.ref_point[0,:] )

    # Solve at random point
    motion_snoopy2 = mcnSolver.solve( ref_points = np.array([[10. , 20., 30.]])) 
    motion_snoopy2 = motion_snoopy2.hydro.get_at_ref_point( mcn_coef.ref_point[0,:] )

    # Solve in body-fixed system
    mesh = msh.Mesh(msh.HydroStarMesh(mesh_file).getUnderWaterHullMesh(0))
    mesh.refreshGaussPoints()
    hstat_hull_bf = 9.81*1000*mesh.integrate_stiffness_matrix( ref_frame = "body-fixed", output_ref_point = rdf_coef.ref_point[0,:].values )[np.newaxis , :, :]
    rdf_coef["hydrostatic_hull_bf"] = xr.DataArray( hstat_hull_bf , coords = rdf_coef.hydrostatic_hull.coords )

    mcnSolver = MechanicalSolver( mcn_input, rdf_coef )
    motion_snoopy3 = mcnSolver.solve(  ref_frame = "body-fixed"  )
    motion_snoopy3 = motion_snoopy3.hydro.get_at_ref_point( mcn_coef.ref_point[0,:] )
    
    _test_compare_motion(mcn_coef.motion,motion_snoopy.motion, display = display)

    _test_compare_motion(mcn_coef.motion,motion_snoopy2.motion, display = display)
    
    _test_compare_motion(mcn_coef.motion,motion_snoopy3.motion, display = display, label_1 = "HydroStar" , label_2 = "Snoopy BF")
   
    
    
   


def _test_compare_motion(motion1,motion2, label_1 = "1", label_2 = "2", display = False):
    """Test if python mechanical solver yeild the same results 
    as hsmcn
    Parameters
    ----------
    hsrdf_obj : HydroDatabase
        HydroDatabase object initialized with output of hsrdf and input of hsmcn
        In this case, a python mechanical solver will be called, it will solve 
        the mechanical equation, yielding motion 
    hsmcn_obj : HydroDatabase
        HydroDatabase object initialized with output of hsmcn
    """    
    import matplotlib.pyplot as plt
    
    #---- Compare motion with HydroStar
    if display : 
        fig, ax = plt.subplots( nrows = 2, ncols = 3)
        for headDeg in [ 0. ]:
            for idof in range(0,6):
                np.abs(motion1).sel(body=1 , heading=headDeg , mode=idof+1).plot(ax=ax[ idof//3 , idof%3 ], label = label_1, marker = "+")
                np.abs(motion2).sel(body=1 , heading=headDeg , mode=idof+1).plot(ax=ax[ idof//3 , idof%3 ], label = label_2)
            ax[0,0].legend()
        plt.show()
    # We need to accept a different here because strangely, it seem that 
    # hydrostar (hsmcn) compute additional stiffness du to the gravity
    # differently for K44 (and only for K44): it use rho*V*g instead of mass*g
    # So if there are no perfect balance of Archimède there will be different 
    # A fix is to input the mass = numerical volume x rho
    # But the numerical volume output is rounded, so there are still a tiny different
    diff = np.max(np.abs(motion1.values - motion2.values))
    logger.info(f"Absolute difference: {diff}")
    assert(diff < 1e-1)


if __name__ == "__main__":
    logger.info("START")
    test_mechanical_solver(display = True)
    logger.info("STOP")
    
    
    
