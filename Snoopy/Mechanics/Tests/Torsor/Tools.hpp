#pragma once
#ifndef __BV_Mechanics_Tests_Torsor_Tools_hpp__
#define __BV_Mechanics_Tests_Torsor_Tools_hpp__

#include <boost/test/unit_test.hpp>

#include "Mechanics/ReferenceFrame.hpp"
#include "Mechanics/Tools.hpp"
#include "Tests/Tools.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Translation.hpp"
#include "Geometry/Rotation.hpp"
#include "Tools/EigenTypedefs.hpp"

namespace BV {
namespace Mechanics {
namespace Tests {

namespace Details {

BV::Tools::Vector6d getInFrame_(const BV::Mechanics::ReferenceFrame & formerFrame,
                                const BV::Mechanics::ReferenceFrame & newFrame,
                                const BV::Geometry::Vector & resultantInFormerFrame,
                                const BV::Geometry::Vector & momentInFormerFrame) ;


template <typename T>
void checkStrictlyEqualsTorsors(const T & torsor1,
                                const T & torsor2)
{
    const BV::Mechanics::ReferenceFrame & ref1(torsor1.getReferenceFrame()) ;
    const BV::Mechanics::ReferenceFrame & ref2(torsor2.getReferenceFrame()) ;
    BOOST_CHECK( BV::Mechanics::Details::hasSameRepresentationInParent(ref1, ref2) ) ;
    BOOST_CHECK( BV::Mechanics::Details::hasSameParent(ref1, ref2) ) ;
    CheckXYZ(torsor1.getResultantInLocalBasis(), torsor2.getResultantInLocalBasis()) ;
    CheckXYZ(torsor1.getMomentInLocalBasis(), torsor2.getMomentInLocalBasis()) ;
}

template <typename T>
void checkTorsors(const T & torsor1,
                  const T & torsor2)
{
    T torsor2In1(torsor2.getTransportedInOtherRefFrame(torsor1.getReferenceFrame())) ;
    CheckXYZ(torsor1.getResultantInLocalBasis(), torsor2In1.getResultantInLocalBasis()) ;
    CheckXYZ(torsor1.getMomentInLocalBasis(), torsor2In1.getMomentInLocalBasis()) ;
}

template <typename T>
void checkTorsor(const T & torsor,
                 const BV::Mechanics::ReferenceFrame & refFrame,
                 const BV::Geometry::Vector & resultant,
                 const BV::Geometry::Vector & moment)
{
    CheckXYZ(resultant, torsor.getResultantInLocalBasis()) ;
    CheckXYZ(moment, torsor.getMomentInLocalBasis()) ;
    BOOST_CHECK(BV::Mechanics::Details::hasSameGlobalRepresentation(refFrame, torsor.getReferenceFrame())) ;
}

template <typename T>
bool isStrictlyEqualsTorsors(const T & torsor1,
                             const T & torsor2)
{
    const BV::Mechanics::ReferenceFrame & ref1(torsor1.getReferenceFrame()) ;
    const BV::Mechanics::ReferenceFrame & ref2(torsor2.getReferenceFrame()) ;
    return BV::Mechanics::Details::hasSameRepresentationInParent(ref1, ref2)
            && BV::Mechanics::Details::hasSameParent(ref1, ref2)
            && (torsor1.getResultantInLocalBasis() == torsor2.getResultantInLocalBasis())
            && (torsor1.getMomentInLocalBasis() == torsor2.getMomentInLocalBasis()) ;
}

template <typename T>
bool isEquivalentTorsors(const T & torsor1,
                         const T & torsor2)
{
    T torsor2In1(torsor2.getTransportedInOtherRefFrame(torsor1.getReferenceFrame())) ;
    return (torsor1.getResultantInLocalBasis() == torsor2In1.getResultantInLocalBasis())
            && (torsor1.getMomentInLocalBasis() == torsor2In1.getMomentInLocalBasis()) ;
}

template <typename T>
bool isEquivalentTorsor(const T & torsor,
                        const BV::Mechanics::ReferenceFrame & refFrame,
                        const BV::Geometry::Vector & resultant,
                        const BV::Geometry::Vector & moment)
{
    return (resultant == torsor.getResultantInLocalBasis())
            && (moment == torsor.getMomentInLocalBasis())
            && hasSameGlobalRepresentation(refFrame, torsor.getReferenceFrame()) ;
}

template <typename T>
void CheckDerivedConstructors_(void)
{
    using BV::Geometry::Vector ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;
    ReferenceFrame ref ;
    Cartesian cart(1., -2., -18.35) ;
    ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
    AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
    AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
    AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
    ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

    Vector resultant(10., 11., 3.2) ;
    Vector moment(8.6, -2.1, -1200.1569) ;

    BV::Tools::Vector6d vect6d ;
    vect6d.head(3) = resultant.toArray() ;
    vect6d.tail(3) = moment.toArray() ;

    T c1(ref, resultant, moment) ;
    checkTorsor(c1, ref, resultant, moment) ;

    T c2(ref, vect6d) ;
    checkTorsor(c2, ref, resultant, moment) ;

    T c3(ref) ;
    checkTorsor(c3, ref, Vector(), Vector()) ;

    T c4 ;
    checkTorsor(c4, ReferenceFrame(), Vector(), Vector()) ;

    T c5(c1) ;
    checkTorsor(c5, ref, resultant, moment) ;
}

template <typename T>
void CheckChangeRefPointInParent_(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Tools::Vector6d ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;

    // changeReferencePointInParent with pure resultant displaced along resultant (no new moment)
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(1., 0., 0.) ;
        torsor.changeReferencePointInParent(ptInFrame) ;

        ReferenceFrame ref2 ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, ptInFrame) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 0.) ;
        checkTorsor(torsor, ref2, expectedResultant, expectedMoment) ;
    }

    // changeReferencePointInParent with pure resultant displaced out of resultant
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(0., 1., 0.) ;
        torsor.changeReferencePointInParent(ptInFrame) ;

        ReferenceFrame ref2 ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, ptInFrame) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 1.) ;
        checkTorsor(torsor, ref2, expectedResultant, expectedMoment) ;
    }

    // changeReferencePointInParent with pure moment
    {
        ReferenceFrame ref ;
        Vector resultant(0., 0., 0.) ;
        Vector moment(1., 1., 1.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 1.) ;
        torsor.changeReferencePointInParent(ptInFrame) ;

        ReferenceFrame ref2 ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, ptInFrame) ;

        Vector expectedResultant(0., 0., 0.) ;
        Vector expectedMoment(1., 1., 1.) ;
        checkTorsor(torsor, ref2, expectedResultant, expectedMoment) ;
    }

    // changeReferencePointInParent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;
        T torsor(ref, resultant, moment) ;
        Point ptInParent(-1., 1., 5.897) ;
        ReferenceFrame ref2(ref) ;
        ref2.originInParent(ptInParent);
        torsor.changeReferencePointInParent(ptInParent) ;
        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        checkTorsor(torsor, ref2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }
}

template <typename T>
void CheckChangeRefPointInFrame_(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Tools::Vector6d ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;

    // changeReferencePointInFrame with pure resultant displaced along resultant (no new moment)
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(1., 0., 0.) ;

        ReferenceFrame frame(ref) ;
        torsor.changeReferencePointInFrame(ptInFrame, frame) ;

        ReferenceFrame ref2 ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, ptInFrame) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 0.) ;
        checkTorsor(torsor, ref2, expectedResultant, expectedMoment) ;
    }

    // changeReferencePointInFrame with pure resultant displaced out of resultant
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(0., 1., 0.) ;

        ReferenceFrame frame(ref) ;
        torsor.changeReferencePointInFrame(ptInFrame, frame) ;

        ReferenceFrame ref2 ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, ptInFrame) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 1.) ;
        checkTorsor(torsor, ref2, expectedResultant, expectedMoment) ;
    }

    // changeReferencePointInFrame with pure moment
    {
        ReferenceFrame ref ;
        Vector resultant(0., 0., 0.) ;
        Vector moment(1., 1., 1.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 1.) ;

        ReferenceFrame frame(ref) ;
        torsor.changeReferencePointInFrame(ptInFrame, frame) ;

        ReferenceFrame ref2 ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, ptInFrame) ;

        Vector expectedResultant(0., 0., 0.) ;
        Vector expectedMoment(1., 1., 1.) ;
        checkTorsor(torsor, ref2, expectedResultant, expectedMoment) ;
    }

    // changeReferencePointInFrame with frame's parent without torsor's ref frame parent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 5.897) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.changeReferencePointInFrame(ptInFrame, frame) ;

        ReferenceFrame ref2(ref) ;
        ref2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        checkTorsor(torsor, ref2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }


    // changeReferencePointInFrame with frame's parent with torsor's ref frame parent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i, aa3*aa1*aa2) ;
        ref.setParent(refParent) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 5.897) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.changeReferencePointInFrame(ptInFrame, frame) ;

        ReferenceFrame ref2(ref) ;
        ref2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        checkTorsor(torsor, ref2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }
}

template <typename T>
void CheckTransportInOtherRefFrame_WithPt_(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Tools::Vector6d ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;

    // transportInOtherRefFrame with pure resultant displaced along resultant (no new moment)
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(1., 0., 0.) ;

        ReferenceFrame frame(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 0.) ;
        checkTorsor(torsor, frame2, expectedResultant, expectedMoment) ;
    }

    // transportInOtherRefFrame with pure resultant displaced out of resultant
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(0., 1., 0.) ;

        ReferenceFrame frame(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 1.) ;
        checkTorsor(torsor, frame2, expectedResultant, expectedMoment) ;
    }

    // transportInOtherRefFrame with pure moment
    {
        ReferenceFrame ref ;
        Vector resultant(0., 0., 0.) ;
        Vector moment(1., 1., 1.) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 1.) ;

        ReferenceFrame frame(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector expectedResultant(0., 0., 0.) ;
        Vector expectedMoment(1., 1., 1.) ;
        checkTorsor(torsor, frame2, expectedResultant, expectedMoment) ;
    }

    // transportInOtherRefFrame with frame's parent without torsor's ref frame parent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 5.897) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame, false) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInOtherRefFrame with frame's parent with torsor's ref frame parent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i, aa3*aa1*aa2) ;
        ref.setParent(refParent) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 5.897) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame, false) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInOtherRefFrame with frame's parent without torsor's ref frame parent with copy
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 5.897) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame, true) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInOtherRefFrame with frame's parent with torsor's ref frame parent with copy
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_e, aa3*aa1*aa2) ;
        ref.setParent(refParent) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;
        Point ptInFrame(-1., 1., 5.897) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(ptInFrame, frame, true) ;

        ReferenceFrame frame2(frame) ;
        frame2.originInGlobal(frame.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }
}

template <typename T>
void CheckTransportInOtherRefFrame_WithOutPt_(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Tools::Vector6d ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;

    // transportInOtherRefFrame with pure resultant displaced along resultant (no new moment)
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        torsor.transportInOtherRefFrame(frame) ;

        ReferenceFrame frame2(frame) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 0.) ;
        checkTorsor(torsor, frame2, expectedResultant, expectedMoment) ;
    }

    // transportInOtherRefFrame with pure resultant displaced out of resultant
    {
        ReferenceFrame ref ;
        Vector resultant(1., 0., 0.) ;
        Vector moment(0., 0., 0.) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        frame.originInGlobal(Point(0., 1., 0.)) ;
        torsor.transportInOtherRefFrame(frame) ;

        ReferenceFrame frame2(frame) ;

        Vector expectedResultant(1., 0., 0.) ;
        Vector expectedMoment(0., 0., 1.) ;
        checkTorsor(torsor, frame2, expectedResultant, expectedMoment) ;
    }

    // transportInOtherRefFrame with pure moment
    {
        ReferenceFrame ref ;
        Vector resultant(0., 0., 0.) ;
        Vector moment(1., 1., 1.) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        torsor.transportInOtherRefFrame(frame) ;

        ReferenceFrame frame2(frame) ;

        Vector expectedResultant(0., 0., 0.) ;
        Vector expectedMoment(1., 1., 1.) ;
        checkTorsor(torsor, frame2, expectedResultant, expectedMoment) ;
    }

    // transportInOtherRefFrame with frame's parent without torsor's ref frame parent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(frame, false) ;

        ReferenceFrame frame2(frame) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInOtherRefFrame with frame's parent with torsor's ref frame parent
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i, aa3*aa1*aa2) ;
        ref.setParent(refParent) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(frame, false) ;

        ReferenceFrame frame2(frame) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInOtherRefFrame with frame's parent without torsor's ref frame parent with copy
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(frame, true) ;

        ReferenceFrame frame2(frame) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInOtherRefFrame with frame's parent with torsor's ref frame parent with copy
    {
        ReferenceFrame ref ;
        Cartesian cart(1., -2., -18.35) ;
        ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
        AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
        AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
        AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
        ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_e, aa3*aa1*aa2) ;
        ref.setParent(refParent) ;

        Vector resultant(10., 11., 3.2) ;
        Vector moment(8.6, -2.1, -1200.1569) ;

        T torsor(ref, resultant, moment) ;

        ReferenceFrame frame(ref) ;
        frame.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN,
                            -4., 7., 3.2358) ;
        frame.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE,
                          AxisAndAngle(Vector(0., 1., 0.), 0.12)
                           * AxisAndAngle(Vector(0., 0., 1.), 0.22)) ;
        frame.setParent(ref) ;
        torsor.transportInOtherRefFrame(frame, true) ;

        ReferenceFrame frame2(frame) ;

        Vector6d resMom(getInFrame_(ref, frame2, resultant, moment)) ;
        checkTorsor(torsor, frame2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }
}

template <typename T>
void CheckTorsorBaseMethods_(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Tools::Vector6d ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;
    ReferenceFrame ref ;
    Cartesian cart(1., -2., -18.35) ;
    ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
    AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
    AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
    AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
    ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

    Vector resultant(10., 11., 3.2) ;
    Vector moment(8.6, -2.1, -1200.1569) ;

    // resetComponents
    {
        T torsor(ref, resultant, moment) ;
        torsor.resetComponents() ;
        checkTorsor(torsor, ref, Vector(), Vector()) ;
    }

    // setReferenceFrame
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        Cartesian cart2(1.125, -20., -1.35) ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart2) ;
        AxisAndAngle aa4(Vector(0., 0., 1.), M_PI / 3.) ;
        AxisAndAngle aa5(Vector(1., 0., 0.), M_PI / 5.) ;
        AxisAndAngle aa6(Vector(0., 0., 1.), M_PI / 125.) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa4*aa5*aa6) ;

        torsor.setReferenceFrame(ref2) ;
        checkTorsor(torsor, ref2, resultant, moment) ;
    }

    // setReferenceFrame
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        Cartesian cart2(1.125, -20., -1.35) ;
        ref2.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart2) ;
        AxisAndAngle aa4(Vector(0., 0., 1.), M_PI / 3.) ;
        AxisAndAngle aa5(Vector(1., 0., 0.), M_PI / 5.) ;
        AxisAndAngle aa6(Vector(0., 0., 1.), M_PI / 125.) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa4*aa5*aa6) ;

        // torsor.setInternalReferenceFrameParent(ref2) ;
        // const ReferenceFrame & refTorsor(torsor.getReferenceFrame()) ;
        // Details::hasSameRepresentationInParent(refTorsor, ref) ;
        // BOOST_CHECK(refTorsor.hasParent()) ;
        // BOOST_CHECK( &(refTorsor.getParent()) == &ref2) ;
    }

    // setResultantInLocalBasis
    {
        T torsor(ref, resultant, moment) ;
        Vector resultant2(8., -3., 9.) ;
        torsor.setResultantInLocalBasis(resultant2) ;
        checkTorsor(torsor, ref, resultant2, moment) ;
    }

    // setMomentInLocalBasis
    {
        T torsor(ref, resultant, moment) ;
        Vector moment2(8., -3., 9.) ;
        torsor.setMomentInLocalBasis(moment2) ;
        checkTorsor(torsor, ref, resultant, moment2) ;
    }

    // getReferenceFrame by const reference
    {
        T torsor(ref, resultant, moment) ;
        const ReferenceFrame & refTorsor(torsor.getReferenceFrame()) ;
        BOOST_CHECK(BV::Mechanics::Details::hasSameGlobalRepresentation(refTorsor, ref)) ;
    }

    // getResultantInLocalBasis by const reference
    {
        T torsor(ref, resultant, moment) ;
        const Vector & resTorsor(torsor.getResultantInLocalBasis()) ;
        CheckXYZ(resTorsor, resultant) ;
    }

    // getMomentInLocalBasis by const reference
    {
        T torsor(ref, resultant, moment) ;
        const Vector & momTorsor(torsor.getMomentInLocalBasis()) ;
        CheckXYZ(momTorsor, moment) ;
    }


    // getReferenceFrame by copy
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame refTorsor(torsor.getReferenceFrame()) ;
        BOOST_CHECK(BV::Mechanics::Details::hasSameGlobalRepresentation(refTorsor, ref)) ;

        refTorsor.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN) ;
        refTorsor.setRotator(BV::Geometry::RotatorTypeEnum::ROTATION_MATRIX) ;

        BOOST_CHECK(BV::Mechanics::Details::hasSameGlobalRepresentation(torsor.getReferenceFrame(), ref)) ;
    }

    // getResultantInLocalBasis by copy
    {
        T torsor(ref, resultant, moment) ;
        Vector resTorsor(torsor.getResultantInLocalBasis()) ;
        CheckXYZ(resTorsor, resultant) ;
        resTorsor = Vector(1., 0., 2.) ;

        CheckXYZ(torsor.getResultantInLocalBasis(), resultant) ;
    }

    // getMomentInLocalBasis by copy
    {
        T torsor(ref, resultant, moment) ;
        Vector momTorsor(torsor.getMomentInLocalBasis()) ;
        CheckXYZ(momTorsor, moment) ;
        momTorsor = Vector(8., 9., 10.) ;

        CheckXYZ(torsor.getMomentInLocalBasis(), moment) ;
    }

    // setReferencePoint
    {
        T torsor(ref, resultant, moment) ;
        Point ptInParent(-1., 1., 5.897) ;
        ReferenceFrame ref2(ref) ;
        ref2.originInParent(ptInParent);

        torsor.setReferencePoint(ptInParent) ;
        checkTorsor(torsor, ref2, resultant, moment) ;
    }

    // setBasis
    {
        T torsor(ref, resultant, moment) ;
        AxisAndAngle rotInParent(Vector(1., 0., 0.), M_PI) ;
        ReferenceFrame ref2(ref) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, rotInParent);

        torsor.setBasis(rotInParent) ;
        checkTorsor(torsor, ref2, resultant, moment) ;
    }

    CheckChangeRefPointInParent_<T>() ;
    CheckChangeRefPointInFrame_<T>() ;

    // changeBasis
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(-12., 13., 14.)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS,
                        AxisAndAngle(Vector(1., 0., 0.), M_PI / 13.)
                        * AxisAndAngle(Vector(0., 1., 0.), 80. * M_PI / 1.23)
                        * AxisAndAngle(Vector(1., 0., 0.), 4560. * M_PI / 8.23)
                        * AxisAndAngle(Vector(0., 0., 1.), 30. * M_PI / 2.43)
                        * AxisAndAngle(Vector(1., 0., 0.), 1250. * M_PI / 12.23) ) ;

        ReferenceFrame ref3(ref2) ;
        ref3.originInGlobal(ref.originInGlobal()) ;

        torsor.changeBasis(ref2, false) ;
        Vector6d resMom(getInFrame_(ref, ref3, resultant, moment)) ;
        checkTorsor(torsor, ref3, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        torsor.changeBasis(ref, false) ;
        Vector6d resMom2(getInFrame_(ref3, ref, Vector(resMom.head(3)), Vector(resMom.tail(3)))) ;
        checkTorsor(torsor, ref, Vector(resMom2.head(3)), Vector(resMom2.tail(3))) ;
        checkTorsor(torsor, ref, resultant, moment) ;

        torsor.changeBasis(ref2, true) ;
        Vector6d resMom3(getInFrame_(ref, ref3, resultant, moment)) ;
        checkTorsor(torsor, ref3, Vector(resMom3.head(3)), Vector(resMom3.tail(3))) ;

        torsor.changeBasis(ref, true) ;
        Vector6d resMom4(getInFrame_(ref3, ref, Vector(resMom.head(3)), Vector(resMom.tail(3)))) ;
        checkTorsor(torsor, ref, Vector(resMom4.head(3)), Vector(resMom4.tail(3))) ;
        checkTorsor(torsor, ref, resultant, moment) ;

    }

    CheckTransportInOtherRefFrame_WithPt_<T>() ;
    CheckTransportInOtherRefFrame_WithOutPt_<T>() ;

    // transportInParentRefFrame without parent
    {
        T torsor(ref, resultant, moment) ;
        torsor.transportInParentRefFrame() ;
        Vector6d resMom(getInFrame_(ref, ReferenceFrame(), resultant, moment)) ;
        checkTorsor(torsor, ReferenceFrame(), Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // transportInParentRefFrame with parent
    {
        ReferenceFrame ref2(ref) ;
        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_e, aa3*aa1*aa2) ;
        ref2.setParent(refParent) ;

        T torsor(ref2, resultant, moment) ;
        torsor.transportInParentRefFrame() ;
        Vector6d resMom(getInFrame_(ref2, ref2.getParent(), resultant, moment)) ;
        checkTorsor(torsor, ref2.getParent(), Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }

    // operator* with other torsor (computation of the comoment)
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        Vector resultant2(3.*resultant) ;
        Vector moment2(-2.5*moment) ;

        T torsor2(ref2, resultant2, moment2) ;

        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        Vector6d resMom2(getInFrame_(ref2, ref2, resultant2, moment2)) ;

        double expected( resMom(0) * resMom2(3)
                        + resMom(1) * resMom2(4)
                        + resMom(2) * resMom2(5)
                        + resMom(3) * resMom2(0)
                        + resMom(4) * resMom2(1)
                        + resMom(5) * resMom2(2) ) ;
        BOOST_CHECK_CLOSE(expected, torsor * torsor2, 1.e-12) ;
    }

    // getResultantInGlobalBasis
    {
        T torsor(ref, resultant, moment) ;
        AxisAndAngle rot(ref.getRotatorInGlobal()) ;
        CheckXYZ(torsor.getResultantInGlobalBasis(), rot *  resultant) ;
    }

    // getResultantInGlobalBasis
    {
        T torsor(ref, resultant, moment) ;
        AxisAndAngle rot(ref.getRotatorInGlobal()) ;
        CheckXYZ(torsor.getMomentInGlobalBasis(), rot *  moment) ;
    }

    // getResultantInOtherRefFrameBasis
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        AxisAndAngle rot(ref.getRotatorInGlobal()) ;
        AxisAndAngle rot2(ref2.getRotatorInGlobal()) ;
        CheckXYZ(torsor.getResultantInOtherRefFrameBasis(ref2), rot2.getInversed() * rot *  resultant) ;
    }

    // getMomentInOtherRefFrameBasis
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        AxisAndAngle rot(ref.getRotatorInGlobal()) ;
        AxisAndAngle rot2(ref2.getRotatorInGlobal()) ;
        CheckXYZ(torsor.getMomentInOtherRefFrameBasis(ref2), rot2.getInversed() * rot *  moment) ;
    }

    // getMomentInOtherRefFrame with point
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        Point ptInFrame(0., 1., 12.5) ;

        ReferenceFrame frame(ref2) ;
        frame.originInGlobal(ref2.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;

        AxisAndAngle rot(ref.getRotatorInGlobal()) ;
        AxisAndAngle rot2(ref2.getRotatorInGlobal()) ;
        CheckXYZ(torsor.getMomentInOtherRefFrame(ptInFrame, ref2), Vector(resMom.tail(3))) ;
    }

    // getMomentInOtherRefFrame without point
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;

        AxisAndAngle rot(ref.getRotatorInGlobal()) ;
        AxisAndAngle rot2(ref2.getRotatorInGlobal()) ;
        CheckXYZ(torsor.getMomentInOtherRefFrame(ref2), Vector(resMom.tail(3))) ;
    }

    // getResultantMomentInLocalBasis
    {
        T torsor(ref, resultant, moment) ;
        Vector6d result(torsor.getResultantMomentInLocalBasis()) ;
        Vector6d resMom(getInFrame_(ref, ref, resultant, moment)) ;
        CheckVectorXd(result, resMom) ;
    }

    // getResultantMomentInLocalBasis
    {
        T torsor(ref, resultant, moment) ;
        Vector6d result(torsor.getResultantMomentInGlobalBasis()) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(ref.originInGlobal()) ;
        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        CheckVectorXd(result, resMom) ;
    }

    // getResultantMomentInOtherRefFrameBasis
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;
        Vector6d result(torsor.getResultantMomentInOtherRefFrameBasis(ref2)) ;

        ReferenceFrame frame(ref2) ;
        frame.originInGlobal(ref.originInGlobal()) ;

        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        CheckVectorXd(result, resMom) ;
    }

    // getResultantMomentInOtherRefFrame with point
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;
        Point ptInFrame(-0.1, 0.2, 189.0125) ;
        ReferenceFrame frame(ref2) ;
        frame.originInGlobal(ref2.localToGlobal(ptInFrame)) ;
        Vector6d result(torsor.getResultantMomentInOtherRefFrame(ptInFrame, ref2)) ;
        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        CheckVectorXd(result, resMom) ;
    }

    // getResultantMomentInOtherRefFrame without point
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;
        Vector6d result(torsor.getResultantMomentInOtherRefFrame(ref2)) ;
        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        CheckVectorXd(result, resMom) ;
    }
}
template <typename T>
void CheckTorsorDerivedMethods_(void)
{
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Tools::Vector6d ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;
    ReferenceFrame ref ;
    Cartesian cart(1., -2., -18.35) ;
    ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
    AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
    AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
    AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
    ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

    Vector resultant(10., 11., 3.2) ;
    Vector moment(8.6, -2.1, -1200.1569) ;

    // getInGlobalBasis
    {
        T torsor(ref, resultant, moment) ;
        T torsor2(torsor.getInGlobalBasis()) ;
        ReferenceFrame frame ;
        frame.originInGlobal(ref.originInGlobal()) ;
        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        checkTorsor(torsor2, frame, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;
    }

    // getInOtherRefFrameBasis
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        T torsor2(torsor.getInOtherRefFrameBasis(ref2)) ;
        ReferenceFrame frame(ref2) ;
        frame.originInGlobal(ref.originInGlobal()) ;
        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        checkTorsor(torsor2, frame, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;
    }

    // getTransportedInParentRefFrame without parent
    {
        T torsor(ref, resultant, moment) ;

        T torsor2(torsor.getTransportedInParentRefFrame()) ;
        Vector6d resMom(getInFrame_(ref, ReferenceFrame(), resultant, moment)) ;
        checkTorsor(torsor2, ReferenceFrame(), Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;
    }

    // getTransportedInParentRefFrame with parent
    {
        ReferenceFrame ref2(ref) ;
        ReferenceFrame refParent ;
        refParent.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, Vector(1., 0., -125.)) ;
        refParent.setRotator(BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_e, aa3*aa1*aa2) ;
        ref2.setParent(refParent) ;
        T torsor(ref2, resultant, moment) ;

        T torsor2(torsor.getTransportedInParentRefFrame()) ;
        Vector6d resMom(getInFrame_(ref2, refParent, resultant, moment)) ;
        checkTorsor(torsor2, refParent, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref2, resultant, moment) ;
    }

    // getTransportedInOtherRefFrame with point without copy
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;

        Point ptInFrame(1., -2.3, 3.59) ;

        T torsor2(torsor.getTransportedInOtherRefFrame(ptInFrame, ref2, false)) ;

        ReferenceFrame frame(ref2) ;
        frame.originInGlobal(ref2.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        checkTorsor(torsor2, frame, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;

        ReferenceFrame refTorsor2(torsor2.getReferenceFrame()) ;
        BOOST_CHECK(refTorsor2.getRotatorInParent()
                  != ref2.getRotatorInParent() ) ;
    }

    // getTransportedInOtherRefFrame with point with copy
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;

        Point ptInFrame(1., -2.3, 3.59) ;

        T torsor2(torsor.getTransportedInOtherRefFrame(ptInFrame, ref2, true)) ;

        ReferenceFrame frame(ref2) ;
        frame.originInGlobal(ref2.localToGlobal(ptInFrame)) ;

        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        checkTorsor(torsor2, frame, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;

        ReferenceFrame refTorsor2(torsor2.getReferenceFrame()) ;
        BOOST_CHECK(refTorsor2.getRotatorInParent()
                  == ref2.getRotatorInParent() ) ;
    }

    // getTransportedInOtherRefFrame with two transport
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;
        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;
        T torsorIn2(torsor.getTransportedInOtherRefFrame(ref2)) ;
        T torsorIn3(torsorIn2.getTransportedInOtherRefFrame(ref3)) ;
        T torsorIn3Bis(torsor.getTransportedInOtherRefFrame(ref3)) ;

        ReferenceFrame frame(ref3) ;
        Vector6d resMom(getInFrame_(ref, frame, resultant, moment)) ;
        checkTorsor(torsorIn3, frame, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
        checkTorsor(torsorIn3Bis, frame, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;
    }


    // getTransportedInOtherRefFrame without point without copy
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;

        T torsor2(torsor.getTransportedInOtherRefFrame(ref2, false)) ;

        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        checkTorsor(torsor2, ref2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;

        ReferenceFrame refTorsor2(torsor2.getReferenceFrame()) ;
        BOOST_CHECK(refTorsor2.getRotatorInParent()
                  != ref2.getRotatorInParent() ) ;
    }

    // getTransportedInOtherRefFrame without point with copy
    {
        T torsor(ref, resultant, moment) ;

        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;

        T torsor2(torsor.getTransportedInOtherRefFrame(ref2, true)) ;

        Vector6d resMom(getInFrame_(ref, ref2, resultant, moment)) ;
        checkTorsor(torsor2, ref2, Vector(resMom.head(3)), Vector(resMom.tail(3))) ;

        checkTorsor(torsor, ref, resultant, moment) ;

        ReferenceFrame refTorsor2(torsor2.getReferenceFrame()) ;
        BOOST_CHECK(refTorsor2.getRotatorInParent()
                  == ref2.getRotatorInParent() ) ;
    }

    // operator=
    {
        T torsor(ref, resultant, moment) ;
        T torsor2 ;

        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ReferenceFrame(), Vector(), Vector()) ;

        torsor2 = torsor ;
        checkTorsor(torsor2, ref, resultant, moment) ;
    }

    // operator+=
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;
        Vector resultant2(2.1*resultant) ;
        Vector moment2(-126.12354*moment) ;
        T torsor2(ref2, resultant2, moment2) ;

        Vector resultant3(resultant+resultant2) ;
        Vector moment3(moment+moment2) ;
        T torsor3(ref3, resultant3, moment3) ;

        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ref2, resultant2, moment2) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;

        Vector6d resMom1In1(torsor.getResultantMomentInLocalBasis()) ;
        Vector6d resMom2In2(torsor2.getResultantMomentInLocalBasis()) ;

        Vector6d resMom1In2(getInFrame_(ref, ref2, resultant, moment)) ;
        Vector6d resMom3In1(getInFrame_(ref3, ref, resultant3, moment3)) ;
        torsor2 += torsor ;
        torsor += torsor3 ;

        checkTorsor(torsor2, ref2, Vector((resMom2In2+resMom1In2).head(3)), Vector((resMom2In2+resMom1In2).tail(3))) ;
        checkTorsor(torsor, ref, Vector((resMom1In1+resMom3In1).head(3)), Vector((resMom1In1+resMom3In1).tail(3))) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;
    }

    // operator+
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;
        Vector resultant2(2.1*resultant) ;
        Vector moment2(-126.12354*moment) ;
        T torsor2(ref2, resultant2, moment2) ;

        Vector resultant3(resultant+resultant2) ;
        Vector moment3(moment+moment2) ;
        T torsor3(ref3, resultant3, moment3) ;

        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ref2, resultant2, moment2) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;

        Vector6d resMom1In1(torsor.getResultantMomentInLocalBasis()) ;
        Vector6d resMom2In2(torsor2.getResultantMomentInLocalBasis()) ;

        Vector6d resMom1In2(getInFrame_(ref, ref2, resultant, moment)) ;
        Vector6d resMom3In1(getInFrame_(ref3, ref, resultant3, moment3)) ;
        T torsor4(torsor2 + torsor) ;
        T torsor5(torsor + torsor3) ;

        checkTorsor(torsor4, ref2, Vector((resMom2In2+resMom1In2).head(3)), Vector((resMom2In2+resMom1In2).tail(3))) ;
        checkTorsor(torsor5, ref, Vector((resMom1In1+resMom3In1).head(3)), Vector((resMom1In1+resMom3In1).tail(3))) ;
        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ref2, resultant2, moment2) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;
    }


    // operator-=
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;
        Vector resultant2(2.1*resultant) ;
        Vector moment2(-126.12354*moment) ;
        T torsor2(ref2, resultant2, moment2) ;

        Vector resultant3(resultant+resultant2) ;
        Vector moment3(moment+moment2) ;
        T torsor3(ref3, resultant3, moment3) ;

        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ref2, resultant2, moment2) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;

        Vector6d resMom1In1(torsor.getResultantMomentInLocalBasis()) ;
        Vector6d resMom2In2(torsor2.getResultantMomentInLocalBasis()) ;

        Vector6d resMom1In2(getInFrame_(ref, ref2, resultant, moment)) ;
        Vector6d resMom3In1(getInFrame_(ref3, ref, resultant3, moment3)) ;
        torsor2 -= torsor ;
        torsor -= torsor3 ;

        checkTorsor(torsor2, ref2, Vector((resMom2In2-resMom1In2).head(3)), Vector((resMom2In2-resMom1In2).tail(3))) ;
        checkTorsor(torsor, ref, Vector((resMom1In1-resMom3In1).head(3)), Vector((resMom1In1-resMom3In1).tail(3))) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;
    }

    // operator-
    {
        T torsor(ref, resultant, moment) ;
        ReferenceFrame ref2 ;
        ref2.originInGlobal(Point(1., -5., -63.258)) ;
        ref2.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(1., 0., 0.), 0.1258) ) ;

        ReferenceFrame ref3 ;
        ref3.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, AxisAndAngle(Vector(0., 1., 0.), 0.1258) ) ;
        ref2.setParent(ref3) ;
        Vector resultant2(2.1*resultant) ;
        Vector moment2(-126.12354*moment) ;
        T torsor2(ref2, resultant2, moment2) ;

        Vector resultant3(resultant+resultant2) ;
        Vector moment3(moment+moment2) ;
        T torsor3(ref3, resultant3, moment3) ;

        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ref2, resultant2, moment2) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;

        Vector6d resMom1In1(torsor.getResultantMomentInLocalBasis()) ;
        Vector6d resMom2In2(torsor2.getResultantMomentInLocalBasis()) ;

        Vector6d resMom1In2(getInFrame_(ref, ref2, resultant, moment)) ;
        Vector6d resMom3In1(getInFrame_(ref3, ref, resultant3, moment3)) ;
        T torsor4(torsor2 - torsor) ;
        T torsor5(torsor - torsor3) ;

        checkTorsor(torsor4, ref2, Vector((resMom2In2-resMom1In2).head(3)), Vector((resMom2In2-resMom1In2).tail(3))) ;
        checkTorsor(torsor5, ref, Vector((resMom1In1-resMom3In1).head(3)), Vector((resMom1In1-resMom3In1).tail(3))) ;
        checkTorsor(torsor, ref, resultant, moment) ;
        checkTorsor(torsor2, ref2, resultant2, moment2) ;
        checkTorsor(torsor3, ref3, resultant3, moment3) ;
    }

    // operator*=
    {
        T torsor(ref, resultant, moment) ;
        torsor *= 2. ;
        CheckXYZ(torsor.getResultantInLocalBasis(), 2.*resultant) ;
        CheckXYZ(torsor.getMomentInLocalBasis(), 2.*moment) ;
    }

    // operator* : Torsor * scalar
    {
        T torsor(ref, resultant, moment) ;
        T torsor2(torsor * 1.5 ) ;
        CheckXYZ(torsor2.getResultantInLocalBasis(), 1.5*resultant) ;
        CheckXYZ(torsor2.getMomentInLocalBasis(), 1.5*moment) ;
    }

    // operator/ : Torsor / scalar
    {
        T torsor(ref, resultant, moment) ;
        T torsor2(torsor / 1.5 ) ;
        CheckXYZ(torsor2.getResultantInLocalBasis(), resultant / 1.5) ;
        CheckXYZ(torsor2.getMomentInLocalBasis(), moment / 1.5) ;
    }

    // operator- : -Torsor
    {
        T torsor(ref, resultant, moment) ;
        T torsor2( - torsor ) ;
        CheckXYZ(torsor2.getResultantInLocalBasis(), -resultant) ;
        CheckXYZ(torsor2.getMomentInLocalBasis(), -moment) ;
    }

    // operator* : Scalar * Torsor
    {
        T torsor(ref, resultant, moment) ;
        T torsor2( 1.5*torsor) ;
        CheckXYZ(torsor2.getResultantInLocalBasis(), 1.5*resultant) ;
        CheckXYZ(torsor2.getMomentInLocalBasis(), 1.5*moment) ;
    }
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Mechanics
} // End of namespace BV

#endif // __BV_Mechanics_Tests_Torsor_Tools_hpp__
