#include "Tests/Torsor/Tools.hpp"

namespace BV {
namespace Mechanics {
namespace Tests {

namespace Details {

BV::Tools::Vector6d getInFrame_(const BV::Mechanics::ReferenceFrame & formerFrame,
                                const BV::Mechanics::ReferenceFrame & newFrame,
                                const BV::Geometry::Vector & resultantInFormerFrame,
                                const BV::Geometry::Vector & momentInFormerFrame)
{
    using BV::Tools::Vector6d ;
    using BV::Geometry::Point ;
    using BV::Geometry::Vector ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::RotationMatrix ;

    Cartesian formerTranslatorInGlobal(formerFrame.getTranslatorInGlobal()) ;
    Cartesian newTranslatorInGlobal(newFrame.getTranslatorInGlobal()) ;

    Point formerOriginInGlobal(formerTranslatorInGlobal.toPoint()) ;
    Point newOriginInGlobal(newTranslatorInGlobal.toPoint()) ;
    Vector distance(formerOriginInGlobal - newOriginInGlobal) ;

    RotationMatrix formerRotatorInGlobal(formerFrame.getRotatorInGlobal()) ;
    RotationMatrix newRotatorInGlobal(newFrame.getRotatorInGlobal()) ;

    Vector formerResultantInGlobalBasis(formerRotatorInGlobal * resultantInFormerFrame) ;
    Vector formerMomentInGlobalBasis(formerRotatorInGlobal * momentInFormerFrame) ;

    Vector displacedMomentInGlobalBasis(formerMomentInGlobalBasis + (distance ^ formerResultantInGlobalBasis) ) ;

    Vector newResultantInNewBasis( newRotatorInGlobal.getInversed() * formerResultantInGlobalBasis) ;
    Vector newMomentInNewBasis( newRotatorInGlobal.getInversed() * displacedMomentInGlobalBasis) ;

    Vector6d toReturn ;
    toReturn.head(3) = newResultantInNewBasis.toArray() ;
    toReturn.tail(3) = newMomentInNewBasis.toArray() ;
    return toReturn ;
}

} // End of namespace Details
} // End of namespace Tests
} // End of namespace Mechanics
} // End of namespace BV
