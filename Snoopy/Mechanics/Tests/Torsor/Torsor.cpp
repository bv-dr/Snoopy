#include "Mechanics/Torsor.hpp"

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Geometry/Vector.hpp"
#include "Tests/Torsor/Torsor.hpp"

#include "Tools/EigenTypedefs.hpp"
#include "Mechanics/ReferenceFrame.hpp"
#include "Tests/Torsor/Tools.hpp"

namespace BV {
namespace Mechanics {
namespace Tests {

namespace Details {

} // End of namespace Details

void TorsorConstruction(void)
{
    using BV::Geometry::Vector ;
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::AxisAndAngle ;
    using BV::Mechanics::ReferenceFrame ;
    ReferenceFrame ref ;
    Cartesian cart(1., -2., -18.35) ;
    ref.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, cart) ;
    AxisAndAngle aa1(Vector(0., 0., 1.), 3. * M_PI / 2.) ;
    AxisAndAngle aa2(Vector(1., 0., 0.), M_PI / 2.) ;
    AxisAndAngle aa3(Vector(0., 1., 0.), M_PI / 4.) ;
    ref.setRotator(BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, aa1*aa2*aa3) ;

    Vector resultant(10., 11., 3.2) ;
    Vector moment(8.6, -2.1, -1200.1569) ;

    // TorsorBase
    {
        TorsorBase tB(ref, resultant, moment) ;
        Details::checkTorsor(tB, ref, resultant, moment) ;
    }

    // The derived classes
    Details::CheckDerivedConstructors_<Torsor>() ;
    Details::CheckDerivedConstructors_<KinematicTorsor>() ;
    Details::CheckDerivedConstructors_<StaticTorsor>() ;
}

void TorsorMethods(void)
{
    Details::CheckTorsorBaseMethods_<TorsorBase>() ;
    Details::CheckTorsorBaseMethods_<Torsor>() ;
    Details::CheckTorsorBaseMethods_<KinematicTorsor>() ;
    Details::CheckTorsorBaseMethods_<StaticTorsor>() ;
    Details::CheckTorsorDerivedMethods_<Torsor>() ;
    Details::CheckTorsorDerivedMethods_<KinematicTorsor>() ;
    Details::CheckTorsorDerivedMethods_<StaticTorsor>() ;
}

} // end of namespace Tests
} // end of namespace Mechanics
} // end of namespace BV
