/*
 * Torsor.hpp
 *
 *  Created on: 18 d�c. 2015
 *      Author: smartin
 */

#include "Mechanics/Torsor.hpp"

#include <Eigen/Dense>


namespace BV {
namespace Mechanics {

Geometry::Vector DisplacedMoment(const Geometry::Vector & distance,
                                 const Geometry::Vector & resultant)
{
    return distance ^ resultant ;
}

void TorsorBase::resetComponents(void)
{
    resultant_.x() = 0. ;
    resultant_.y() = 0. ;
    resultant_.z() = 0. ;
    moment_.x() = 0. ;
    moment_.y() = 0. ;
    moment_.z() = 0. ;
}

void TorsorBase::setReferenceFrame(const ReferenceFrame & ref)
{
    referenceFrame_ = ref ;
}
void TorsorBase::setResultantInLocalBasis(const Geometry::Vector & resultant)
{
    resultant_ = resultant ;
}
void TorsorBase::setMomentInLocalBasis(const Geometry::Vector & moment)
{
    moment_ = moment ;
}

const ReferenceFrame & TorsorBase::getReferenceFrame(void) const
{
    return referenceFrame_ ;
}
const Geometry::Vector & TorsorBase::getResultantInLocalBasis(void) const
{
    return resultant_ ;
}
const Geometry::Vector & TorsorBase::getMomentInLocalBasis(void) const
{
    return moment_ ;
}

void TorsorBase::setReferencePoint(const Geometry::Point & pt)
{
    if (pt != referenceFrame_.originInParent())
    {
        referenceFrame_.originInParent(pt) ;
    }
}

void TorsorBase::setBasis(const Geometry::Rotation::ABC & newBasisInParent)
{
    referenceFrame_.setRotator(newBasisInParent) ;
}

void TorsorBase::changeReferencePointInParent(const Geometry::Point & pt)
{
    // Handled manually by supposing that point is in parent
    if (pt != referenceFrame_.originInParent())
    {
        Geometry::Vector distance(referenceFrame_.parentToLocal(referenceFrame_.originInParent() - pt)) ;
        moment_ += DisplacedMoment(distance, resultant_) ;
        setReferencePoint(pt) ;
    }
}

void TorsorBase::changeBasisInParent(const Geometry::Rotation::ABC & newBasisInParent)
{
    // Handled manually by supposing that the Rotation::ABC represents a rotation in the
    // same parent as referenceFrame_
    Geometry::Vector resultantInFormerParent(referenceFrame_.localToParent(resultant_)) ;
    Geometry::Vector momentInFormerParent(referenceFrame_.localToParent(moment_)) ;
    setBasis(newBasisInParent) ;
    resultant_ = referenceFrame_.parentToLocal(resultantInFormerParent) ;
    moment_ = referenceFrame_.parentToLocal(momentInFormerParent) ;
}

void TorsorBase::changeReferencePointInFrame(const BV::Geometry::Point & ptInFrame,
                                             const ReferenceFrame & frame)
{
    BV::Geometry::Point ptInParent ;
    if(referenceFrame_.hasParent())
    {
        ptInParent = frame.localToFrame(ptInFrame, referenceFrame_.getParent()) ;
    }
    else
    {
        ptInParent = frame.localToGlobal(ptInFrame) ;
    }
    changeReferencePointInParent(ptInParent) ;
}

void TorsorBase::changeBasis(const ReferenceFrame & frame,
                             const bool & copyRefFrame)
{
    using BV::Geometry::Rotation::Quaternion ;
    if(referenceFrame_.hasParent())
    {
        const ReferenceFrame & parentRefFrame(referenceFrame_.getParent()) ;
        frame.setArgToRotatorInFrame(quatTmp_, parentRefFrame) ;
        changeBasisInParent(quatTmp_) ;
    }
    else
    {
        frame.setArgToRotatorInGlobal(quatTmp_) ;
        changeBasisInParent(quatTmp_) ;
    }

    if(copyRefFrame)
    {
        // if we copy the referenceFrame we must store and reset the application point
        // because we only want to change the basis and not the application point
        BV::Geometry::Point origInGlob(referenceFrame_.originInGlobal()) ;
        referenceFrame_ = frame ;
        referenceFrame_.originInGlobal(origInGlob) ;
    }
}

void TorsorBase::transportInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                          const BV::Mechanics::ReferenceFrame & frame,
                                          const bool & copyRefFrame)
{

    // the point given in argument is assumed to be in the local basis
    // of the referenceFrame also passed in argument
    changeReferencePointInFrame(ptInFrame, frame) ;
    changeBasis(frame, copyRefFrame) ;
}

void TorsorBase::transportInOtherRefFrame(const BV::Mechanics::ReferenceFrame & frame,
                                          const bool & copyRefFrame)
{
    transportInOtherRefFrame(BV::Geometry::Point(0.,0.,0.),
                             frame,
                             copyRefFrame) ;
}

void TorsorBase::transportInParentRefFrame(void)
{
    if(referenceFrame_.hasParent())
    {
        transportInOtherRefFrame(referenceFrame_.getParent()) ;
    }
    else
    {
        transportInOtherRefFrame(ReferenceFrame()) ;
    }
}

double TorsorBase::operator*(const TorsorBase & other) const
{
    TorsorBase otherInMe(other) ;
    otherInMe.transportInOtherRefFrame(referenceFrame_) ;
    return resultant_ * otherInMe.getMomentInLocalBasis() + moment_ * otherInMe.getResultantInLocalBasis() ;
}

Geometry::Vector TorsorBase::getResultantInGlobalBasis(void) const
{
    return referenceFrame_.localToGlobal(resultant_) ;
}
Geometry::Vector TorsorBase::getMomentInGlobalBasis(void) const
{
    return referenceFrame_.localToGlobal(moment_) ;
}

Geometry::Vector TorsorBase::getResultantInOtherRefFrameBasis(const ReferenceFrame & frame) const
{
    return referenceFrame_.localToFrame(resultant_, frame) ;
}
Geometry::Vector TorsorBase::getMomentInOtherRefFrameBasis(const ReferenceFrame & frame) const
{
    return referenceFrame_.localToFrame(moment_, frame) ;
}

Geometry::Vector TorsorBase::getMomentInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                                      const ReferenceFrame & frame) const
{
    Geometry::Vector distanceInFrame(referenceFrame_.originInFrame(frame) - ptInFrame) ;

    return getMomentInOtherRefFrameBasis(frame)
                + DisplacedMoment(distanceInFrame, getResultantInOtherRefFrameBasis(frame)) ;
}

Geometry::Vector TorsorBase::getMomentInOtherRefFrame(const ReferenceFrame & frame) const
{
    return getMomentInOtherRefFrame(BV::Geometry::Point(0., 0., 0.), frame) ;
}

Tools::Vector6d TorsorBase::getResultantMomentInLocalBasis(void) const
{
    tmp_.head<3>() = getResultantInLocalBasis().toArray() ;
    tmp_.tail<3>() = getMomentInLocalBasis().toArray() ;
    return tmp_ ;
}

Tools::Vector6d TorsorBase::getMomentResultantInLocalBasis(void) const
{
    tmp_.head<3>() = getMomentInLocalBasis().toArray() ;
    tmp_.tail<3>() = getResultantInLocalBasis().toArray() ;
    return tmp_ ;
}

Tools::Vector6d TorsorBase::getResultantMomentInGlobalBasis(void) const
{
    tmp_.head<3>() = referenceFrame_.localToGlobal(resultant_).toArray() ;
    tmp_.tail<3>() = referenceFrame_.localToGlobal(moment_).toArray() ;
    return tmp_ ;
}

Tools::Vector6d TorsorBase::getMomentResultantInGlobalBasis(void) const
{
    tmp_.head<3>() = referenceFrame_.localToGlobal(moment_).toArray() ;
    tmp_.tail<3>() = referenceFrame_.localToGlobal(resultant_).toArray() ;
    return tmp_ ;
}

Tools::Vector6d TorsorBase::getResultantMomentInOtherRefFrameBasis(const ReferenceFrame & frame) const
{
    tmp_.head<3>() = getResultantInOtherRefFrameBasis(frame).toArray() ;
    tmp_.tail<3>() = getMomentInOtherRefFrameBasis(frame).toArray() ;
    return tmp_ ;
}

Tools::Vector6d TorsorBase::getResultantMomentInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                                              const ReferenceFrame & frame) const
{
    tmp_.head<3>() = getResultantInOtherRefFrameBasis(frame).toArray() ;
    tmp_.tail<3>() = getMomentInOtherRefFrame(ptInFrame, frame).toArray() ;
    return tmp_ ;
}

Tools::Vector6d TorsorBase::getResultantMomentInOtherRefFrame(const ReferenceFrame & frame) const
{
    tmp_.head<3>() = getResultantInOtherRefFrameBasis(frame).toArray() ;
    tmp_.tail<3>() = getMomentInOtherRefFrame(frame).toArray() ;
    return tmp_ ;
}


} // End of namespace Mechanics
} // End of namespace BV

