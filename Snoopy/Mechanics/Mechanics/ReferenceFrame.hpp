#pragma once
#ifndef BV_Mechanics_ReferenceFrame_hpp
#define BV_Mechanics_ReferenceFrame_hpp

#include "MechanicsExport.hpp"

#include <memory>
#include <Eigen/Dense>
#include <type_traits>

#include "Tools/BVException.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"
#include "Geometry/Translation.hpp"
#include "Geometry/Rotation.hpp"
#include "Geometry/Transform3D.hpp"

// FIXME closed chains do __not__ work ! Do something to prevent
// setting them or deal with them...

namespace BV {
namespace Mechanics {

namespace Details {

template <typename T>
using GetType = std::remove_cv<typename std::remove_reference<T>::type> ;

} // End of namespace Details

struct InertialFrameException : public BV::Tools::Exceptions::BVException
{
    InertialFrameException(std::string message) : BV::Tools::Exceptions::BVException(message)
    {
    }
} ;

/*!
 * \brief ReferenceFrame class used to locate and orientate an object in 
 *      3D space.
 */
class MECHANICS_API ReferenceFrame
{
public:
    using TranslatorType = Geometry::Translation::Cartesian ;
    using RotatorType = Geometry::Rotation::Quaternion ;

protected:
    TranslatorType translator_ ;
    RotatorType rotator_ ;

    /* Optional reference to parent reference frame */
    const ReferenceFrame* parent_ ;

    /* Vector of unknowns, which size depends on the chosen translator and rotator */
    mutable Eigen::VectorXd unknowns_ ;
    /* Vector of constraints, which size depends on the chosen translator and rotator */
    mutable Eigen::VectorXd constraints_ ;

    mutable bool updateGlobalTransformNeeded_ ;
    mutable TranslatorType translatorInGlobal_ ;
    mutable RotatorType rotatorInGlobal_ ;
    mutable std::size_t globalTransformID_ ;
    mutable std::size_t parentGlobalTransformID_ ;

    /*!
     * \brief Help function to refresh the unknowns and constraints vectors
     *      when the translator or rotator changes as they don't necessarily 
     *      have the same number of unknowns and constraints
     */
    void refreshUnknownsAndConstraints_(void) const ;

    /*!
     * \brief Check if parentship is not closed.
     *
     * Basically parenthship ref1 -> ref2 -> ref1 is forbidden.
     *
     */
    void checkClosedChain_(const ReferenceFrame & ref,
                           const ReferenceFrame & fr) const ;

    /*!
     * \brief Composition of rotations from global to local: i.e. compose rotations by multiplying at the right \c this rotation matrix.
     *
     * This method compose the change of basis such that the rotator represents a change of basis from preceeding reference frames
     * up to the argument ReferenceFrame. The composeRotations_ method can then compute recursively a change of basis from local to global.
     *
     * Basically this methods composed the change of basis ( \f$R_{comp}\f$ ) such that for \f$R_{this}\f$ the change of basis of \c this from local to parent
     * and \f$R_{rot}\f$ an other change of basis from something to local:
     * \f[ R_{comp} = R_{this} . R_{rot} \f]
     *
     * \param[in] ref: a ReferenceFrame whose rotator represents a local to parent change of basis.
     * \param[in] rot: a Rotator which represents change of basis preceeding the change of basis of the ReferenceFrame.
     * \return Derived Type RotatorType representing a local to global change of basis.
     */
    void composeRotations_(const ReferenceFrame & ref, std::shared_ptr<Geometry::Rotation::ABC> rot) const ;

    bool needGlobalTransformUpdate_() const ;

    void updateGlobalTransform_() const ;
public:

    /*!
     * \brief Default constructor of the reference frame.
     *      This constructor takes no attributes, default values are set.
     *      Possible other initialisations are done using helper functions:
     *          - setTranslator
     *          - setRotator
     *          - setParent
     *      Please refer to their own documentation for more details.
     */
    ReferenceFrame(void) ;

    /*!
     * \brief Copy constructor.
     */
    ReferenceFrame(const ReferenceFrame & frame) ;

    template <typename Translator, typename Rotator>
    ReferenceFrame(const Translator & trans, const Rotator & rot,
                   const ReferenceFrame * parent=nullptr) :
        translator_(trans), rotator_(rot), parent_(parent),
        updateGlobalTransformNeeded_(true),
        globalTransformID_(0),
        parentGlobalTransformID_(0)
    {
        if (parent_ != nullptr)
        {
            parentGlobalTransformID_ = parent_->globalTransformID_ ;
        }
    }

    /*!
     * \brief Interpolation constructor.
     *        Interpolates between startRef and endRef at interpParam.
     *        Linear interpolation for positions.
     *        Quaternion slerp for rotations.
     *        Creates a ReferenceFrame with type Quaternion Rotator.
     *        The reference is expressed in global axis system (no parent).
     *        Set it yourself if needed.
     * \param[in] startRef The start reference frame
     * \param[in] endRef The end reference frame
     * \param[in] interpParam Must be defined in range 0., 1.
     *              0.: startRef
     *              1.: endRef
     */
    ReferenceFrame(const ReferenceFrame & startRef,
                   const ReferenceFrame & endRef,
                   const double & interpParam) ;

    /*!
     * \brief Assignement operator
     */
    ReferenceFrame & operator=(const ReferenceFrame & frame) ;

    /*!
     * \brief Enables to set a specific translator defined in
     *      Geometry::Translation.
     *      Note that old translator will be overwritten without looking at
     *      its state.
     *      This is kind of a re-initialisation of the Translator.
     *      Also note that the unknowns and constraints vectors will be
     *      updated to conform to the new translator.
     * \tparam TranslatorArgs, you don't have to care about this as a user, it is
     *      part of the implementation details. Basically, you can specify
     *      any type of arguments to this function, the compiler will guess
     *      by itself.
     */
    template <class ... TranslatorArgs>
    void setTranslator(const BV::Geometry::TranslatorTypeEnum & translatorTypeEnum, const TranslatorArgs &... translatorArgs)
    {
        std::shared_ptr<Geometry::Translation::ABC> ptr(
                BV::Geometry::Factories::TranslatorsFactory::create(translatorTypeEnum, translatorArgs...)) ;
        translator_ = TranslatorType(*ptr) ;
        updateGlobalTransformNeeded_ = true ;
    }

    void setTranslator(const Geometry::Translation::ABC & translator) ;

    void setTranslator(const TranslatorType & translator) ;

    void setTranslator(const Geometry::Point & pt) ;

    /*!
     * \brief Enables to set a specific rotator defined in
     *      Geometry::Rotation.
     *      Note that old rotator will be overwritten without looking at
     *      its state.
     *      This is kind of a re-initialisation of the Rotator.
     *      Also note that the unknowns and constraints vectors will be
     *      updated to conform to the new rotator.
     * \tparam Rotator, the type of rotator to set.
     * \tparam RotatorArgs, you don't have to care about this as a user, it is
     *      part of the implementation details. Basically, you can specify
     *      any type of arguments to this function, the compiler will guess
     *      by itself.
     * \param[in] rotatorArgs, the arguments that should be passed to the
     *      rotator for initialisation. This can be whatever the
     *      Geometry::Rotator allows in its constructor.
     */
    template <class ... RotatorArgs>
    void setRotator(const BV::Geometry::RotatorTypeEnum & rotatorTypeEnum, const RotatorArgs &... rotatorArgs)
    {
        // Assign pointer to new rotator
        std::shared_ptr<Geometry::Rotation::ABC> ptr(
                BV::Geometry::Factories::RotatorsFactory::create(rotatorTypeEnum, rotatorArgs...)) ;
        rotator_ = RotatorType(*ptr) ;
        updateGlobalTransformNeeded_ = true ;
    }

    void setRotator(const Geometry::Rotation::ABC & rotator) ;

    void setRotator(const RotatorType & rotator) ;

    /*!
     * \brief Return the origin position in __global__ reference frame.
     * \return Geometry::Point, a point describing this reference framem origin
     *      position in global reference frame.
     */
    Geometry::Point originInGlobal(void) const ;

    /*!
     * \brief Set this reference frame origin position in global reference frame.
     * \param[in] o, the new origin in global.
     */
    void originInGlobal(const Geometry::Point & o) ;

    /*!
     * \brief Get the origin position in parent reference frame.
     * \return Geometry::Point, a point describing this reference frame origin
     *      position in parent reference frame.
     */
    Geometry::Point originInParent(void) const ;

    /*!
     * \brief Set this reference frame origin position in parent reference frame.
     * \param[in] o, the new origin.
     */
    void originInParent(const Geometry::Point & o) ;

    /*!
     * \brief Get the origin position in given frame reference frame.
     * \return Geometry::Point, a point describing this reference frame origin
     *      position in parent reference frame.
     */
    Geometry::Point originInFrame(const ReferenceFrame & other) const ;

    /*!
     * \brief Set this reference frame origin position such that given point is described in
     * frame axis base.
     * \param[in] o, the new origin in frame axis.
     */
    void originInFrame(const Geometry::Point & o, const ReferenceFrame & other) ;

    /*!
     * \brief Specify a parent reference frame to this reference frame.
     *      This reference frame position and orientation (translator and
     *      rotator) will now be expressed in specified parent reference frame.
     *      Note that if this reference frame was already initialized to a
     *      specific position in the global reference frame or in another
     *      parent reference frame, its relative position according to previous
     *      parent will be transfered to the new parent.
     *      Also note that if this frame already has a parent, no more link
     *      will be taken into account with it after setting the new parent.
     * \param[in] parent, a reference to the parent reference frame.
     */
    void setParent(const ReferenceFrame & parent) ;

    /*!
     * \brief Specify a parent reference frame to this reference frame.
     *      This reference frame position and orientation (translator and
     *      rotator) will now be expressed in specified parent reference frame.
     *      Note that previous position of this reference frame in global
     *      reference frame will be kept after setting the new parent.
     * \tparam TranslatorType, the type of translator that will be used
     *      to represent translation in the new parent frame.
     * \tparam RotatorType, the type of rotator that will be used to
     *      represent rotation in the new parent frame.
     * \param[in] parent, a reference to the new parent frame.
     */
    void setParentKeepingPosition(const ReferenceFrame & parent) ;

    /*!
     * \brief Returns a const reference to the parent reference frame.
     *      Note that in case no parent is defined (you can check calling
     *      the hasParent() function), a reference to this reference frame
     *      is returned.
     * \return A const reference to the parent reference frame.
     */
    const ReferenceFrame & getParent(void) const ;

    /*!
     * \brief Return true if this reference frame has a parent set, false
     *      otherwise. This utility function should be used before calling
     *      the getParent function to make sure to get the correct parent, or
     *      at least an existing one.
     * \return A boolean stating wether this reference frame has a parent set.
     */
    bool hasParent(void) const ;

    /*!
     * \brief Translates the reference frame of the provided amount.
     *
     * The components of the given translator are in GLOBAL axis.
     *
     * \param[in] abc a translator object containing the translation.
     *      description to apply.
     */
    void translateWithTranslationInGlobal(const Geometry::Vector & vect) ;

    /*!
     * \copydoc translateWithTranslationInGlobal(const Geometry::Vector &)
     */
    void translateWithTranslationInGlobal(const Geometry::Translation::ABC & abc) ;


    /*!
     * \brief Translates the reference frame of the provided amount.
     *
     * The components of the given translator are in LOCAL axis.
     *
     * \param[in] abc a translator object containing the translation.
     *      description to apply.
     */
    void translateWithTranslationInLocal(const Geometry::Vector & vect) ;

    /*!
     * \copydoc translateWithTranslationInLocal(const Geometry::Vector &)
     */
    void translateWithTranslationInLocal(const Geometry::Translation::ABC & abc) ;


    /*!
     * \brief Translates the reference frame of the provided amount.
     *
     * The components of the given translator are in PARENT axis.
     *
     * \param[in] abc a translator object containing the translation.
     *      description to apply.
     */
    void translateWithTranslationInParent(const Geometry::Vector & vect) ;

    /*!
     * \copydoc translateWithTranslationInParent(const Geometry::Vector &)
     */
    void translateWithTranslationInParent(const Geometry::Translation::ABC & abc) ;

    /*!
     * \brief Translates the reference frame of the provided amount.
     *
     * The components of the given translator are in given FRAME axis.
     *
     * \param[in] abc a translator object containing the translation.
     *      description to apply.
     */
    void translateWithTranslationInFrame(const Geometry::Vector & vect, const ReferenceFrame & frame) ;

    /*!
     * \copydoc translateWithTranslationInFrame(const Geometry::Vector &, const ReferenceFrame &)
     */
    void translateWithTranslationInFrame(const Geometry::Translation::ABC & abc, const ReferenceFrame & frame) ;

    /*!
     * \brief Rotates the reference frame of the provided rotation amount at left.
     *
     * This method correspond to a rotation of the reference frame in the parent axis.
     * In other words it also correspond to a (virtual) rotation of the parent,
     * or a rotation added before the rotation of the local axis.
     *
     * Again in other words, the rotator in signature correspond to a change of basis
     * from local to parent. The initial stored rotator becomes a new local to local.
     *
     * The final rotator represents a change of basis from new local to parent.
     *
     * \param[in] abc the rotation object containing the rotation amount  to be multiplied at left.
     */
    void addOtherRotationAtLeft(const Geometry::Rotation::ABC & abc) ;

    /*!
     * \brief Rotates the reference frame of the provided rotation amount at right.
     *
     * This method correspond to a rotation of the reference frame in the local (final) axis.
     * In other words it also correspond to a rotation after the final local rotation.
     *
     * Again in other words, the rotator in signature correspond to a change of basis
     * from new local to local. The initial stored rotator represents a local to parent change of basis.
     *
     * The final rotator represents a change of basis from new local to parent.
     *
     * \param[in] abc the rotation object containing the rotation amount to be multiplied at right.
     */
    void addOtherRotationAtRight(const Geometry::Rotation::ABC & abc) ;

    /*!
     * \brief Returns a const reference to the translator object representing
     *      the translation of this reference frame. Note that the type is not
     *      known a-priori, hence a reference to the Translation::ABC is
     *      returned.
     *      Note that this translator object may convert in any translator object
     *      implemented in Geometry::Translation, you just have to use one
     *      of the conversion methods.
     * \return A const reference to the inner translator object.
     */
    const TranslatorType & getTranslatorInParent() const ;


     /*!
      * \brief Returns an instance of the translator representing this frame
      *      position in the global reference frame.
      * \tparam TranslatorType, the type of translator used to represent this
      *      global position. It can be any type defined in Geometry::Translation.
      * \return A translator representing this frame position in global frame.
      */
     const TranslatorType & getTranslatorInGlobal() const ;


     /*!
      * \brief Returns an instance of the translator representing this frame
      *      position according to provided other frame.
      * \tparam TranslatorType, the type of translator object that should be
      *      used to represent the translation. It can be any type defined
      *      in Geometry::Translation.
      * \param[in] other, the other reference frame into which the translation
      *      should be expressed.
      * \return A translator representing this reference frame position
      *      according to the other reference frame.
      */
     TranslatorType getTranslatorInFrame(const ReferenceFrame & other) const ;


    /*!
     * \brief Returns a copy to the rotator object representing
     *      the rotation of this reference frame. Note that the type is not
     *      known a-priori, hence a reference to the Rotation::ABC is
     *      returned.
     *      Note that this rotator object may convert in any rotation object
     *      implemented in Geometry::Rotation, you just have to use one
     *      of the conversion methods.
     * \tparam The requested type of the rotator.
     * \return A copy of type RotatorType to the inner rotator object.
     */
    const RotatorType & getRotatorInParent() const ;

    /*!
     * \brief Returns an instance of the rotator representing this frame
     *      orientation in the global reference frame.
     * \tparam RotatorType, the type of rotator used to represent this global
     *      rotation. It can be any type defined in Geometry::Rotation.
     * \return A rotator object representing this frame orientation in
     *      global frame.
     */
    const RotatorType & getRotatorInGlobal() const ;

    void composeRotations_(const ReferenceFrame & ref, RotatorType & rotator) const ;

    Eigen::Matrix3d getRotationMatrixInGlobal() const ;

    /*!
     * \brief Returns an instance of the rotator representing this frame
     *      orientation in provided other frame.
     * \tparam RotatorType, the type of rotator object that should be
     *      used to represent the rotation. It can be any type defined
     *      in Geometry::Rotation.
     * \param[in] other, the other reference frame into which the rotation
     *      should be expressed.
     * \return A rotator representing this reference frame orientation
     *      according to the other reference frame.
     */
    RotatorType getRotatorInFrame(const ReferenceFrame & other) const ;

    void setArgToRotatorInGlobal(RotatorType & rotator) const ;

    void setArgToRotatorInFrame(RotatorType & rotator,
                                const ReferenceFrame & other) const ;

    ///*!@{*/
    /*!
     * \brief Get the required axis orientation in global reference frame
     * \return A Vector describing the required axis given this reference frame
     *         orientation in global referenceFrame
     */
    Geometry::Vector d1InGlobal(void) const ;
    /*!
     * \copydoc ReferenceFrame::d1InGlobal(void) const
     */
    Geometry::Vector d2InGlobal(void) const ;

    /*!
     * \copydoc ReferenceFrame::d1InGlobal(void) const
     */
    Geometry::Vector d3InGlobal(void) const ;

    ///*!@}*/

    ///*!@{*/
    /*!
     * \brief Get the required axis orientation in parent reference frame
     * \return A Vector describing the required axis given this reference frame
     *         orientation in parent referenceFrame
     */
    Geometry::Vector d1InParent(void) const ;

    /*!
     * \copydoc ReferenceFrame::d1InParent(void) const
     */
    Geometry::Vector d2InParent(void) const ;

    /*!
     * \copydoc ReferenceFrame::d1InParent(void) const
     */
    Geometry::Vector d3InParent(void) const ;
    ///*!@}*/

    /*!
     * \brief Returns the position of a local xyz in the global axis system.
     * \param[in] xyz The local xyz to be converted in the global axis
     *      system. This could be a point or a vector.
     * \return A xyz which position is defined in the global axis system.
     */
    //template <typename T>
    //typename Details::GetType<T>::type localToGlobal(T && xyz) const
    //{
    //    if (hasParent())
    //    {
    //        // If "this" has a parent, then we ask him to calculate the point
    //        // position according to its own positioning system...
    //        return parent_->localToGlobal(localToParent(std::forward<T>(xyz))) ;
    //    }
    //    // ...otherwise we return the calculated point.
    //    return localToParent(std::forward<T>(xyz)) ;
    //}
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Point>::value, int>::type =0>
    typename Details::GetType<T>::type localToGlobal(T && xyz) const
    {
        return getTranslatorInGlobal().translation(getRotatorInGlobal().rotate(xyz)) ;
    }
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Vector>::value, int>::type =0>
    typename Details::GetType<T>::type localToGlobal(T && xyz) const
    {
        return getRotatorInGlobal().rotate(xyz) ;
    }

    /*!
     * \brief Returns a "local to global axis" Transform3D
     * \return A Transform3D to convert from local to global axis
     */
    Geometry::Transform3D getLocalToGlobalTransform3D(void) const ;

    /*!
     * \brief Returns the local position of a xyz which position is
     *          given in the global axis system.
     * \param[in] xyz The xyz expressed in the global axis system.
     * \return The xyz expressed in the local reference frame.
     */
    //template <typename T>
    //typename Details::GetType<T>::type globalToLocal(T && xyz) const
    //{
    //    // If 'this' has a parent, we first ask him to get the point position
    //    // in its local axis system.
    //    if (hasParent())
    //    {
    //        return parentToLocal(parent_->globalToLocal(std::forward<T>(xyz))) ;
    //    }
    //    return parentToLocal(std::forward<T>(xyz)) ;
    //}
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Point>::value, int>::type =0>
    typename Details::GetType<T>::type globalToLocal(T && xyz) const
    {
        return getRotatorInGlobal().inverseRotate(
                       getTranslatorInGlobal().inverseTranslation(xyz)
                                                 ) ;
    }
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Vector>::value, int>::type =0>
    typename Details::GetType<T>::type globalToLocal(T && xyz) const
    {
        return getRotatorInGlobal().inverseRotate(xyz) ;
    }

    /*!
     * \brief Returns a "global to local axis" Transform3D
     * \return A Transform3D to convert from global to local axis
     */
    Geometry::Transform3D getGlobalToLocalTransform3D(void) const ;

    /*!
     * \brief Returns the position of a local point into parent reference frame.
     * \param[in] point, the local point.
     * \return A point which position is expressed in parent frame.
     */
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Point>::value, int>::type =0>
    typename Details::GetType<T>::type localToParent(T && xyz) const
    {
        // First apply the rotation...
        Eigen::Vector3d resRot(rotator_.rotate(xyz.toArray())) ;
        // ... then the translation.
        return Geometry::Point(translator_.translation(resRot)) ;
    }

    /*!
     * \brief Returns the position of a local vector into parent reference frame.
     * \param[in] vector, the local vector.
     * \return A vector which position is expressed in parent frame.
     */
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Vector>::value, int>::type =0>
    typename Details::GetType<T>::type localToParent(T && xyz) const
    {
        // We apply the rotation to the local vector.
        return Geometry::Vector(rotator_.rotate(xyz.toArray())) ;
    }

    /*!
     * \brief Returns a "local to parent axis" Transform3D
     * \return A Transform3D to convert from local to parent axis
     */
    Geometry::Transform3D getLocalToParentTransform3D(void) const ;

    /*!
     * \brief Returns the position of a point expressed in parent frame into
     *      this reference frame.
     * \param[in] point, the point in parent frame.
     * \return A point which position is expressed in local frame.
     */
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Point>::value, int>::type =0>
    typename Details::GetType<T>::type parentToLocal(T && xyz) const
    {
        // Then we first apply the inverse translation...
        Eigen::Vector3d resTransm1(
                translator_.inverseTranslation(xyz.toArray())
                                    ) ;
        // ...and the inverse rotation.
        return Geometry::Point(rotator_.inverseRotate(std::move(resTransm1))) ;
    }

    /*!
     * \brief Returns the position of a vector expressed in parent frame into
     *      this reference frame.
     * \param[in] vector, the vector in parent frame.
     * \return A vector which position is expressed in local frame.
     */
    template <typename T,
              typename std::enable_if<std::is_same<typename Details::GetType<T>::type,
                                      BV::Geometry::Vector>::value, int>::type =0>
    typename Details::GetType<T>::type parentToLocal(T && xyz) const
    {
        // We apply the inverse rotation.
        return Geometry::Vector(rotator_.inverseRotate(xyz.toArray())) ;
    }

    /*!
     * \brief Returns a "parent to local axis" Transform3D.
     * \return A Transform3D to convert from parent to local axis
     */
    Geometry::Transform3D getParentToLocalTransform3D(void) const ;

    /*!
     * \brief Returns the position of a local xyz into provided frame.
     * \tparam XYZ, the type of the xyz (Geometry::Point or Geometry::Vector)
     * \param[in] xyz, the local xyz to convert into other frame.
     * \param[in] frame, the other frame into which the xyz will be expressed.
     * \return A xyz expressed in the provided other frame.
     */
    template <class XYZ>
    XYZ localToFrame(const XYZ & xyz, const ReferenceFrame & frame) const
    {
        // Small check to know if frame is this
        if (this == &frame)
        {
            return xyz ;
        }
        // Small check to know if frame is the parent one
        // to simplify the calculation if it is the case.
        if (&frame == parent_)
        {
            return localToParent(xyz) ;
        }
        // Otherwise, we convert to global axis system, and then back
        // to provided frame local axis system.
        XYZ resGlob(localToGlobal(xyz)) ;
        return frame.globalToLocal(resGlob) ;
    }

    /*!
     * \brief Returns a "local to frame axis" Transform3D
     * \param[in] frame, the other frame for transformation
     * \return A Transform3D to convert from local to frame axis
     */
    Geometry::Transform3D getLocalToFrameTransform3D(const ReferenceFrame & frame) const ;

    /*!
     * \brief Returns the position of an other frame xyz into this frame.
     * \tparam XYZ, the type of the xyz (Geometry::Point or Geometry::Vector)
     * \param[in] xyz, the other frame xyz to convert into this frame.
     * \param[in] frame, the other frame into which the xyz is expressed.
     * \return A xyz expressed in this local frame.
     */
    template <class XYZ>
    XYZ frameToLocal(const XYZ & xyz, const ReferenceFrame & frame) const
    {
        // Small check to know if frame is this
        if (this == &frame)
        {
            return xyz ;
        }
        // Small check to know if frame is the parent one
        // to simplify the calculation if it is the case.
        if (&frame == parent_)
        {
            return parentToLocal(xyz) ;
        }
        // Otherwise, we convert to global axis system, and then back
        // to provided frame local axis system.
        XYZ resGlob(frame.localToGlobal(xyz)) ;
        return globalToLocal(resGlob) ;
    }

    /*!
     * \brief Returns a "frame to local axis" Transform3D
     * \param[in] frame, the other frame for transformation
     * \return A Transform3D to convert from frame to local axis
     */
    Geometry::Transform3D getFrameToLocalTransform3D(const ReferenceFrame & frame) const ;

    /*!
     * \brief Returns the number of unknowns that characterize this frame.
     *      Note that this number depends on the type of translator and
     *      rotator used.
     * \return The number of unknowns.
     */
    unsigned int nUnknowns(void) const ;

    /*!
     * \brief Returns the number of constraints that characterize this frame.
     *      Note that this number depends on the type of translator and
     *      rotator used.
     * \return The number of constraints.
     */
    unsigned int nConstraints(void) const ;

    /*!
     * \brief Returns the vector containing the unknowns values.
     *      Note that the unknowns are expressed in the parent frame.
     * \return The vector containing the unknowns values.
     */
    Eigen::VectorXd unknowns(void) const ;

    /*!
     * \brief Set the unknowns to particular values.
     *      Note that the size of the provided vector must match the number
     *      of unknowns (depends on the translator and rotator used).
     * \param[in] The new vector of unknowns.
     */
    void unknowns(const Eigen::VectorXd & unknowns) ;

    /*!
     * \brief Returns the vector containing the constraints values.
     * \return The vector containing the constraints values.
     */
    Eigen::VectorXd constraints(void) ;

    ReferenceFrame inverseInParent(void) const ;

    ReferenceFrame inverseInGlobal(void) const ;

    // ReferenceFrame operator-(void) const ;

    ReferenceFrame operator-(const ReferenceFrame & other) const ;

} ;

} // end of namespace Mechanics
} // end of namespace BV

#endif // BV_Mechanics_ReferenceFrame_hpp
