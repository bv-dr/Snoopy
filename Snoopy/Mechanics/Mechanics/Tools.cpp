#include "Mechanics/Tools.hpp"
#include "Geometry/Translation.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace Mechanics {

namespace Details {

bool hasSameGlobalRepresentation(const BV::Mechanics::ReferenceFrame & ref1,
                                 const BV::Mechanics::ReferenceFrame & ref2)
{
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::RotationMatrix ;
    Cartesian cart1(ref1.getTranslatorInGlobal()) ;
    Cartesian cart2(ref2.getTranslatorInGlobal()) ;
    RotationMatrix rot1(ref1.getRotatorInGlobal()) ;
    RotationMatrix rot2(ref2.getRotatorInGlobal()) ;
    return (cart1 == cart2) && (rot1 == rot2) ;
}

bool hasStrictlySameRepresentation(const BV::Mechanics::ReferenceFrame & ref1,
                                   const BV::Mechanics::ReferenceFrame & ref2)
{
    //bool hasSameTranslatorTypeEnum(ref1.getTranslatorTypeEnum() == ref2.getTranslatorTypeEnum()) ;
    //bool hasSameRotatorTypeEnum(ref1.getRotatorTypeEnum() == ref2.getRotatorTypeEnum()) ;
    //return hasSameTranslatorTypeEnum && hasSameRotatorTypeEnum
    //        && hasSameRepresentationInParent(ref1, ref2)  && hasSameParent(ref1, ref2) ;
    return hasSameRepresentationInParent(ref1, ref2)  && hasSameParent(ref1, ref2) ;

}

bool hasSameParent(const BV::Mechanics::ReferenceFrame & ref1,
                   const BV::Mechanics::ReferenceFrame & ref2)
{
    bool sameParent(true) ;
    if(ref1.hasParent())
    {
        if(ref2.hasParent())
        {
            const ReferenceFrame & ref1Parent(ref1.getParent()) ;
            const ReferenceFrame & ref2Parent(ref2.getParent()) ;
            if( !(&ref1Parent == &ref2Parent) )
            {
                sameParent = false ;
            }
        }
        else
        {
            sameParent = false ;
        }
    }
    return sameParent ;
}

bool hasSameRepresentationInParent(const BV::Mechanics::ReferenceFrame & ref1,
                                   const BV::Mechanics::ReferenceFrame & ref2)
{
    using BV::Geometry::Translation::Cartesian ;
    using BV::Geometry::Rotation::RotationMatrix ;

    Cartesian cart1(ref1.getTranslatorInParent()) ;
    Cartesian cart2(ref2.getTranslatorInParent()) ;
    bool hasSameTranslatorInParent(cart1 == cart2) ;

    RotationMatrix rot1(ref1.getRotatorInParent()) ;
    RotationMatrix rot2(ref2.getRotatorInParent()) ;
    bool hasSameRotatorInParent(rot1 == rot2) ;
    return hasSameTranslatorInParent && hasSameRotatorInParent ;
}


} // End of namespace Details
} // End of namespace Mechanics
} // End of namespace BV
