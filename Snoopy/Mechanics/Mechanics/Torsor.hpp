/*
 * Torsor.hpp
 *
 *  Created on: 18 dec. 2015
 *      Author: smartin
 */

#ifndef BV_Mechanics_Torsor_hpp
#define BV_Mechanics_Torsor_hpp

#include "MechanicsExport.hpp"

#include <Eigen/Dense>

#include "Tools/EigenTypedefs.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"
#include "Mechanics/ReferenceFrame.hpp"

namespace BV {
namespace Mechanics {
/*!
 * \brief Compute increments of moment when changing reduction point.
 *
 * The increment of moment is computed such that:\n
 * m = distance ^ resultant\n
 * Where \a distance is the vector \f$\vec{BA}\f$ if the incremental moment corresponds to
 * a former reduction point at \f$A\f$ and a new reduction point at \f$B\f$.
 *
 * distance and resultant must be expressed in same basis.
 *
 * \param[in] distance a Geometry::Vector
 * \param[in] resultant a Geometry::Vector, the resultant of the torsor
 * \return the increment of moment
 */
Geometry::Vector MECHANICS_API DisplacedMoment(const Geometry::Vector & distance,
                                               const Geometry::Vector & resultant) ;


/*!
 * \brief TorsorBase base class used to define all torsor's attributes and generic member methods.
 *
 * A generic torsor has to contains attributes representing the necessary to be fully described:
 * - a basis (or axis system, or set of axis)
 * - a reduction point (where the moment is computed)
 * - a resultant
 * - a moment
 *
 * Basic functionalities of a generic torsor are the followings:
 * - change basis
 * - change application point
 *
 * A typical formulation for a torsor \f$\mathcal{T}\f$ with resultant \f$R\f$ and moment \f$M\f$ expressed at reduction
 * point \f$O\f$ in basis \f$\mathcal{B}\f$ is the following:
 * \f[
 * \mathcal{T} = \{R, M\}_{(O,\mathcal{B})}
 * \f]
 */
class MECHANICS_API TorsorBase
{
protected:
    mutable BV::Tools::Vector6d tmp_ ;
    /*!
     * \brief An instance of Mechanics::ReferenceFrame
     *
     * referenceFrame_ represents the coordinates system where the torsor is reduced.
     * The rotator corresponds to a \a basis (seen as a set of axis representing a base of R3).\n
     * The translator corresponds to the reduction point where the moment is computed.
     *
     */
    BV::Mechanics::ReferenceFrame referenceFrame_ ;

    /*!
     * \brief An instance of Geometry::Vector
     *
     * Contains the resultant of the torsor. This vector is expressed in referenceFrame_ basis (axis system).
     */
    Geometry::Vector resultant_ ;

    /*!
     * \brief An instance of Geometry::Vector
     *
     * Contains the moment of the torsor expressed at point described in referenceFrame_'s translator.
     * This vector is expressed in referenceFrame_ basis (axis system).
     */
    Geometry::Vector moment_ ;

    Geometry::Rotation::Quaternion quatTmp_ ;

public:
    /*!
     * \brief Constructor.
     *
     * \param[in] referenceFrame the Mechanics::ReferenceFrame representing the basis and reduction point
     * \param[in] resultant a Geometry::Vector
     * \param[in] moment a Geometry::Vector
     */
    TorsorBase(const BV::Mechanics::ReferenceFrame & referenceFrame,
               const Geometry::Vector & resultant,
               const Geometry::Vector & moment) :
        referenceFrame_(referenceFrame), resultant_(resultant), moment_(moment)
    {
    }

    /*!
     * \brief Copy constructor.
     *
     * \param[in] referenceFrame the Mechanics::ReferenceFrame representing the basis and reduction point
     * \param[in] resultant a Geometry::Vector
     * \param[in] moment a Geometry::Vector
     */
    TorsorBase(const BV::Mechanics::TorsorBase & other) :
        referenceFrame_(other.getReferenceFrame()),
        resultant_(other.getResultantInLocalBasis()),
        moment_(other.getMomentInLocalBasis())
    {
    }

    /*!
     * \brief Abstract destructor.
     */
     virtual ~TorsorBase(void)
     {
     }

    /*!
     * \brief Reset Resultant and Moment to zero Vectors.
     */
    void resetComponents(void) ;

    /*!
     * \brief Set the torsor referenceFrame as a copy of the given refFrame.
     */
    void setReferenceFrame(const ReferenceFrame & ref) ;

    /*!
     * \brief Set the torsor resultant to the given resultant.
     *
     * The given resultant is supposed expressed in the local basis
     * of the torsor reference frame.
     * The given resultant is copied and assigned.
     */
    void setResultantInLocalBasis(const Geometry::Vector & resultant) ;

    /*!
     * \brief Set the torsor moment to the given moment.
     *
     * The given moment is supposed expressed in the local basis
     * and at the reference point of the torsor reference frame.
     * The given moment is copied and assigned.
     */
    void setMomentInLocalBasis(const Geometry::Vector & moment) ;

    /*!
     * \brief Get the torsor referenceFrame as const reference.
     */
    const ReferenceFrame & getReferenceFrame(void) const ;

    /*!
     * \brief Get the torsor resultant as const reference.
     */
    const Geometry::Vector & getResultantInLocalBasis(void) const ;

    /*!
     * \brief Get the torsor moment as const reference.
     */
    const Geometry::Vector & getMomentInLocalBasis(void) const ;

    /*!
     * \brief Set the torsor's referenceFrame origin.
     *
     * The point in signature represents the point in parent's
     * axis of the torsor reference frame.
     *
     * This methods replace the current reference point without
     * performing any operation on the moment.
     *
     * \param[in] pt The point to be set to in parent axis system.
     */
    void setReferencePoint(const Geometry::Point & pt) ;

    /*!
     * \brief Set the torsor's referenceFrame rotator.
     *
     * The rotator in signature represents a change of basis from
     * a new local base to the torsor's parent reference frame.
     *
     * This methods replace the current rotator without
     * performing any operation on the resultant and moment.
     *
     * \param[in] newBasisInParent The rotator to be set to.
     */
    void setBasis(const Geometry::Rotation::ABC & newBasisInParent) ;

    /*!
     * \brief Change the torsor's referenceFrame origin.
     *
     * The point in signature represents the point in parent's
     * axis of the torsor reference frame.
     *
     * This methods replace the current reference point and
     * perform the displacement operation on the moment.
     *
     * \param[in] pt The point to be set to in parent axis system.
     */
    void changeReferencePointInParent(const Geometry::Point & pt) ;

    /*!
     * \brief Change the torsor's referenceFrame origin.
     *
     * The point in signature represents the point in the
     * axis of the given reference frame in signature.
     *
     * This methods replace the current reference point and
     * perform the displacement operation on the moment.
     *
     * \param[in] pt The point to be set to in frame axis system.
     * \param[in] frame The reference frame in which the point is expressed.
     */
    void changeReferencePointInFrame(const Geometry::Point & ptInFrame,
                                     const ReferenceFrame & frame) ;

    /*!
     * \brief Change the torsor's referenceFrame rotator.
     *
     * The rotator in signature represents a change of basis from
     * a new local base to the torsor's parent reference frame.
     *
     * This methods replace the current rotator and
     * perform the rotation of the resultant and the moment
     * without displacement of the moment.
     * It is only a change of basis of the vectors.
     * \param[in] newBasisInParent The rotator to be set to.
     */
    void changeBasisInParent(const Geometry::Rotation::ABC & newBasisInParent) ;

    /*!
     * \brief Change the torsor's referenceFrame rotator.
     *
     * The frame in signature represents an arbitrary change of basis.
     *
     * This methods replace the current rotator and
     * perform the rotation of the resultant and the moment
     * without displacement of the moment.
     * It is only a change of basis of the vectors.
     *
     * \param[in] frame The reference frame representing an orientation in space.
     * \param[in] copyRefFrame (optional) inform if the given referenceFrame has to be
     * copied and has to replace the torsor's reference frame.
     */
    void changeBasis(const ReferenceFrame & frame,
                     const bool & copyRefFrame=false) ;

    /*!
     * \brief Transport the torsor in an other reference frame.
     *
     * The point in signature represents the point in the
     * axis of the given reference frame in signature.
     * The frame in signature represents an arbitrary change of basis.
     *
     * This methods displace the torsor to the given point and perform a change
     * of basis of the resultant and moment to match the basis represented by the
     * given frame.
     *
     * \param[in] pt The point to be set to in frame axis system.
     * \param[in] frame The reference frame representing an orientation in space.
     * \param[in] copyRefFrame (optional) inform if the given referenceFrame has to be
     * copied and has to replace the torsor's reference frame.
     */
    void transportInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                  const BV::Mechanics::ReferenceFrame & frame,
                                  const bool & copyRefFrame=false) ;

    /*!
     * \brief Transport the torsor in an other reference frame.
     *
     * The frame in signature represents an arbitrary change of basis.
     * For the transport, the origin of the given reference frame is used.
     *
     * This methods displace the torsor to the given frame origin and perform a change
     * of basis of the resultant and moment to match the basis represented by the
     * given frame.
     *
     * \param[in] frame The reference frame representing an orientation in space.
     * \param[in] copyRefFrame (optional) inform if the given referenceFrame has to be
     * copied and has to replace the torsor's reference frame.
     */
    void transportInOtherRefFrame(const BV::Mechanics::ReferenceFrame & frame,
                                  const bool & copyRefFrame=false) ;

    /*!
     * \brief Transport the torsor in the parent of the torsor's reference frame.
     */
    void transportInParentRefFrame(void) ;

    /*!
     * \brief Operator* between two torsors.
     *
     * This operator perform the computation of the \b co-moment of the torsors.
     *
     * At the same reduction point and basis the comoment is computed has follow
     * for two torsors \f$\mathcal{T}_a = \{R_a, M_a\}_{(O,\mathcal{B})}\f$ and \f$\mathcal{T}_b = \{R_b, M_b\}_{(O,\mathcal{B})}\f$
     * \f[ P = R_a \cdot M_b + R_b \cdot M_a \f]
     * The result is independant of the reduction point and the basis choosen
     * for both torsors.
     *
     * \param[in] other an other torsor
     * \return double the comoment
     */
    double operator*(const TorsorBase & other) const ;



    // Other conveniant methods
    /*!
     * \brief Resultant getter in global axis system.
     *
     * \return Geometry::Vector, the resultant in global axis system
     */
    Geometry::Vector getResultantInGlobalBasis(void) const ;

    /*!
     * \brief Moment getter in global axis system.
     *
     * \return Geometry::Vector, the moment in global axis system (without displacement)
     */
    Geometry::Vector getMomentInGlobalBasis(void) const ;

    /*!
     * \brief Resultant getter in arbitrary axis system.
     *
     * \param[in] frame the axis system representation
     *  in which the resultant has to be expressed.
     * \return Geometry::Vector the resultant in given axis system
     */
    Geometry::Vector getResultantInOtherRefFrameBasis(const ReferenceFrame & frame) const ;

    /*!
     * \brief Moment getter in arbitrary axis system.
     *
     * \param[in] frame the axis system representation
     *  in which the moment has to be expressed.
     * \return Geometry::Vector the moment in given axis system (without displacement)
     */
    Geometry::Vector getMomentInOtherRefFrameBasis(const ReferenceFrame & frame) const ;

    /*!
     * \brief Moment getter in arbitrary reference frame.
     *
     * The moment is expressed in the axis system of the given reference frame
     * and displaced at the given point.
     *
     * \param[in] ptInFrame the point in frame axis system to displace the moment
     * \param[in] frame the axis system representation
     *  in which the moment has to be expressed.
     * \return Geometry::Vector the displaced moment in given axis system
     */
    Geometry::Vector getMomentInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                              const ReferenceFrame & frame) const ;

    /*!
     * \brief Moment getter in arbitrary reference frame.
     *
     * The moment is expressed in the axis system of the given reference frame
     * and displaced at the origin of the reference frame.
     *
     * \param[in] frame the axis system representation
     *  in which the moment has to be expressed.
     * \return Geometry::Vector the displaced moment in given axis system
     */
    Geometry::Vector getMomentInOtherRefFrame(const ReferenceFrame & frame) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the resultant
     * and the last three are the moment.
     *
     * The resultant/moment is expressed in local axis of the
     * torsor reference frame.
     *
     * \return Tools:Vector6d, the resultant/moment in local axis system.
     */
    Tools::Vector6d getResultantMomentInLocalBasis(void) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the moment
     * and the last three are the resultant.
     *
     * The moment/resultant is expressed in local axis of the
     * torsor reference frame.
     *
     * \return Tools:Vector6d, the moment/resultant in local axis system.
     */
    Tools::Vector6d getMomentResultantInLocalBasis(void) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the resultant
     * and the last three are the moment.
     *
     * The resultant/moment is expressed in global axis.
     *
     * \return Tools::Vector6d, the resultant/moment in global axis system (without displacement).
     */
    Tools::Vector6d getResultantMomentInGlobalBasis(void) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the moment
     * and the last three are the resultant.
     *
     * The moment/resultant is expressed in global axis.
     *
     * \return Tools::Vector6d, the moment/resultant in global axis system (without displacement).
     */
    Tools::Vector6d getMomentResultantInGlobalBasis(void) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the resultant
     * and the last three are the moment.
     *
     * The resultant/moment is expressed in given frame axis.
     *
     * \param[in] frame the reference frame axis system in which resultant/moment have to
     * be represented
     * \return Tools::Vector6d, the resultant/moment in given frame axis system (without displacement).
     */
    Tools::Vector6d getResultantMomentInOtherRefFrameBasis(const ReferenceFrame & frame) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the resultant
     * and the last three are the moment.
     *
     * The resultant/moment is expressed in given frame axis with the displacement
     * of the moment.
     *
     * \param[in] ptInFrame the point in frame axis system to displace the moment
     * \param[in] frame the reference frame axis system in which resultant/moment have to
     * be represented
     * \return Tools::Vector6d the resultant/moment in given frame axis system with moment displacement.
     */
    Tools::Vector6d getResultantMomentInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                                      const ReferenceFrame & frame) const ;

    /*!
     * \brief Get the torsor representation as a Vector6d
     * where the first three components are the resultant
     * and the last three are the moment.
     *
     * The resultant/moment is expressed in given frame axis with the displacement
     * of the moment.
     *
     * \param[in] frame the reference frame axis system in which resultant/moment have to
     * be represented
     * \return Tools::Vector6d the resultant/moment in given frame axis system with moment displacement.
     */
    Tools::Vector6d getResultantMomentInOtherRefFrame(const ReferenceFrame & frame) const ;

};

/*!
 * \brief Template class derived from TorsorBase.
 *
 * This class exists only to implements getters by copy and operators with a specific return type
 * (with a template derived type).
 *
 * A template class is used to be derived in specific types for objects instantiation and type handling.
 *
 */
template <typename Derived>
class DerivedTorsor : public TorsorBase
{

public:
    /*!
     * \copydoc Mechanics::TorsorBase::TorsorBase(const BV::Mechanics::ReferenceFrame &, const Geometry::Vector &, const Geometry::Vector &)
     */
    DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
                  const Geometry::Vector & resultant,
                  const Geometry::Vector & moment) :
        TorsorBase(referenceFrame, resultant, moment)
    {
    }

    /*!
     * \brief Constructor from a Mechanics::ReferenceFrame and Tools::Vector6d
     *
     * \param[in] referenceFrame a Mechanics::ReferenceFrame
     * \param[in] vect a Tools::Vector6d

     * First three components of vect are the resultant.
     * Last three components of vect are the moment.
     */
    DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
                  const Tools::Vector6d & vect) :
        TorsorBase(referenceFrame, Geometry::Vector(vect.head(3)), Geometry::Vector(vect.tail(3)))
    {
    }

    /*!
     * \brief Constructor from a Mechanics::ReferenceFrame
     *
     * \param[in] referenceFrame a Mechanics::ReferenceFrame
     *
     * Resultant and moment are initialized to zero.
     */
    DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame) :
        TorsorBase(referenceFrame, Geometry::Vector(0., 0., 0.), Geometry::Vector(0., 0., 0.))
    {
    }

    /*!
     * \brief Dummy constructor.
     */
    DerivedTorsor(void) :
        TorsorBase(BV::Mechanics::ReferenceFrame(), Geometry::Vector(0., 0., 0.), Geometry::Vector(0., 0., 0.))
    {
    }

    /*!
     * \copydoc Mechanics::TorsorBase::TorsorBase(const TorsorBase & other)
     */
    DerivedTorsor(const TorsorBase & other) :
        TorsorBase(other)
    {
    }

    /*!
     * \brief Abstract destructor.
     */
     virtual ~DerivedTorsor(void)
     {
     }

    /*!
     * \copydoc BV::Mechanics::TorsorBase::changeBasisInParent(const Geometry::Rotation::ABC &)
     * \return a copy of the torsor in global axis system (without moment displacement) of type Derived
     */
    Derived getInGlobalBasis(void) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp.changeBasis(ReferenceFrame()) ;
        return tmp ;
    }

    /*!
     * \copydoc BV::Mechanics::TorsorBase::changeBasis(const ReferenceFrame &, const bool &)
     * \return a copy of the torsor in given axis system (without moment displacement) of type Derived
     */
    Derived getInOtherRefFrameBasis(const ReferenceFrame & frame,
                                    const bool & copyRefFrame=false) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp.changeBasis(frame, copyRefFrame) ;
        return tmp ;
    }

    /*!
     * \copydoc BV::Mechanics::TorsorBase::transportInParentRefFrame(void)
     * \return a copy of the torsor in parent axis system with moment displaced of type Derived
     */
    Derived getTransportedInParentRefFrame(void) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp.transportInParentRefFrame() ;
        return tmp ;
    }

    /*!
     * \copydoc BV::Mechanics::TorsorBase::transportInOtherRefFrame(const ReferenceFrame &, const bool &)
     * \return a copy of the torsor in given axis system with moment displaced of type Derived
     */
    Derived getTransportedInOtherRefFrame(const ReferenceFrame & frame,
                                          const bool & copyRefFrame=false) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp.transportInOtherRefFrame(frame, copyRefFrame) ;
        return tmp ;
    }

    /*!
     * \copydoc BV::Mechanics::TorsorBase::transportInOtherRefFrame(const Point, const ReferenceFrame &, const bool &)
     * \return a copy of the torsor in given axis system with moment displaced of type Derived
     */
    Derived getTransportedInOtherRefFrame(const BV::Geometry::Point & ptInFrame,
                                          const ReferenceFrame & frame,
                                          const bool & copyRefFrame=false) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp.transportInOtherRefFrame(ptInFrame, frame, copyRefFrame) ;
        return tmp ;
    }

    /*!
     * \brief Assignement operator
     *
     * Copy and assign resultant and moment values and the reference frame
     * without performing any other operation.
     * \return *this
     */
    Derived & operator=(const Derived & other)
    {
        setResultantInLocalBasis(other.resultant_) ;
        setMomentInLocalBasis(other.moment_) ;
        setReferenceFrame(other.referenceFrame_) ;
        return static_cast<Derived &>(*this) ;
    }

    /*!
     * \brief operator+=
     *
     * Copy and transport other torsor in self referenceFrame,
     * then add resultant and moment to self.
     *
     * \param[in] other torsor to be added
     * \return *this
     */
    Derived & operator+=(const Derived & other)
    {
        Derived otherInMyFrame(other.getTransportedInOtherRefFrame(referenceFrame_)) ;
        resultant_ += otherInMyFrame.resultant_ ;
        moment_ += otherInMyFrame.moment_ ;
        return static_cast<Derived &>(*this) ;
    }

    /*!
     * \brief operator+
     *
     * Copy and transport other torsor in self referenceFrame,
     * then add resultant and moment to self.
     *
     * \param[in] other torsor to be added
     * \return a copy of the added torsors.
     */
    Derived operator+(const Derived & other) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp += other ;
        return tmp ;
    }

    /*!
     * \brief operator-=
     *
     * Copy and transport other torsor in self referenceFrame,
     * then subtract resultant and moment to self.
     *
     * \param[in] other torsor to be subtracted
     * \return *this
     */
    Derived & operator-=(const Derived & other)
    {
        Derived otherInMyFrame(other.getTransportedInOtherRefFrame(referenceFrame_)) ;
        resultant_ -= otherInMyFrame.resultant_ ;
        moment_ -= otherInMyFrame.moment_ ;
        return static_cast<Derived &>(*this) ;
    }

    /*!
     * \brief operator-
     *
     * Copy and transport other torsor in self referenceFrame,
     * then subtract resultant and moment to self.
     *
     * \param[in] other torsor to be subtracted
     * \return a copy of the subtracted torsors.
     */
    Derived operator-(const Derived & other) const
    {
        Derived tmp(static_cast<const Derived &>(*this)) ;
        tmp -= other ;
        return tmp ;
    }

    /*!
     * \brief operator*= with double
     *
     * Multiply self resultant and moment by the given double.
     *
     * \param[in] scale a double
     * \return *this
     */
    Derived & operator*=(const double & scale)
    {
        resultant_ = scale * resultant_ ;
        moment_ = scale * moment_ ;
        return static_cast<Derived &>(*this) ;
    }

    using BV::Mechanics::TorsorBase::operator* ;

    /*!
     * \brief operator* with double
     *
     * Copy self and multiply resultant and moment by the given double.
     *
     * \param[in] k a double
     * \return the copied multipled torsor
     */
    friend Derived operator*(const Derived & torsor, const double & k)
    {
        Derived tmp(torsor) ;
        tmp *= k ;
        return tmp ;
    }

    /*!
     * \brief operator/ with double
     *
     * Copy self and divide resultant and moment by the given double.
     *
     * \param[in] k a double
     * \return the copied divided torsor of type Derived
     */
    Derived operator/(const double & k) const
    {
        Derived torsor(static_cast<const Derived &>(*this)) ;
        torsor *= (1./k) ;
        return torsor ;
    }

    /*!
     * \brief operator-
     *
     * Copy self and return the opposite of resultant and moment.
     *
     * \return a copy of the opposite of self torsor of type Derived
     */
    Derived operator-(void) const
    {
        Derived torsor(static_cast<const Derived &>(*this)) ;
        torsor *= -1. ;
        return torsor ;
    }
} ;

template <class Derived>
inline Derived operator*(const double & k, const DerivedTorsor<Derived> & torsor)
{
    return torsor * k ;
}

class Torsor: public DerivedTorsor<Torsor>
{
public:
    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame, const Geometry::Vector & resultant, const Geometry::Vector & moment)
     */
    Torsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
           const Geometry::Vector & resultant,
           const Geometry::Vector & moment) :
        DerivedTorsor<Torsor>(referenceFrame, resultant, moment)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame, const Tools::Vector6d &)
     */
    Torsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
           const Tools::Vector6d & vect) :
        DerivedTorsor<Torsor>(referenceFrame, vect)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame)
     */
    Torsor(const BV::Mechanics::ReferenceFrame & referenceFrame) :
        DerivedTorsor<Torsor>(referenceFrame)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(void)
     */
    Torsor(void) :
        DerivedTorsor<Torsor>()
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const TorsorBase & other)
     */
    Torsor(const TorsorBase & other) :
        DerivedTorsor<Torsor>(other)
    {
    }

    /*!
     * \brief Abstract destructor.
     */
     virtual ~Torsor(void)
     {
     }

} ;

class StaticTorsor: public DerivedTorsor<StaticTorsor>
{
public:
    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame, const Geometry::Vector & resultant, const Geometry::Vector & moment)
     */
    StaticTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
                 const Geometry::Vector& resultant,
                 const Geometry::Vector& moment) :
        DerivedTorsor<StaticTorsor>(referenceFrame, resultant, moment)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame, const Tools::Vector6d &)
     */
    StaticTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
                 const Tools::Vector6d & vect) :
        DerivedTorsor<StaticTorsor>(referenceFrame, vect)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame)
     */
    StaticTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame) :
        DerivedTorsor<StaticTorsor>(referenceFrame)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(void)
     */
    StaticTorsor(void) :
        DerivedTorsor<StaticTorsor>()
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const TorsorBase & other)
     */
    StaticTorsor(const TorsorBase & other) :
        DerivedTorsor<StaticTorsor>(other)
    {
    }

    /*!
     * \brief Abstract destructor.
     */
     virtual ~StaticTorsor(void)
     {
     }

} ;

class KinematicTorsor: public DerivedTorsor<KinematicTorsor>
{
public:
    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame, const Geometry::Vector & resultant, const Geometry::Vector & moment)
     */
    KinematicTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
                    const Geometry::Vector& resultant,
                    const Geometry::Vector& moment) :
        DerivedTorsor<KinematicTorsor>(referenceFrame, resultant, moment)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame, const Tools::Vector6d &)
     */
    KinematicTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame,
                    const Tools::Vector6d & vect) :
        DerivedTorsor<KinematicTorsor>(referenceFrame, vect)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame)
     */
    KinematicTorsor(const BV::Mechanics::ReferenceFrame & referenceFrame) :
        DerivedTorsor<KinematicTorsor>(referenceFrame)
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(void)
     */
    KinematicTorsor(void) :
        DerivedTorsor<KinematicTorsor>()
    {
    }

    /*!
     * \copydoc Mechanics::DerivedTorsor::DerivedTorsor(const TorsorBase & other)
     */
    KinematicTorsor(const TorsorBase & other) :
        DerivedTorsor<KinematicTorsor>(other)
    {
    }

    /*!
     * \brief Abstract destructor.
     */
     virtual ~KinematicTorsor(void)
     {
     }

} ;

} // End of namespace Mechanics
} // End of namespace BV

#endif /* BV_Mechanics_Torsor_hpp */
