#pragma once

#include "MechanicsExport.hpp"

#include "Mechanics/ReferenceFrame.hpp"

namespace BV {
namespace Mechanics {

namespace Details {

MECHANICS_API bool hasSameGlobalRepresentation(const BV::Mechanics::ReferenceFrame & ref1,
                                                    const BV::Mechanics::ReferenceFrame & ref2) ;

MECHANICS_API bool hasStrictlySameRepresentation(const BV::Mechanics::ReferenceFrame & ref1,
                                                      const BV::Mechanics::ReferenceFrame & ref2) ;

MECHANICS_API bool hasSameParent(const BV::Mechanics::ReferenceFrame & ref1,
                                      const BV::Mechanics::ReferenceFrame & ref2) ;

MECHANICS_API bool hasSameRepresentationInParent(const BV::Mechanics::ReferenceFrame & ref1,
                                                      const BV::Mechanics::ReferenceFrame & ref2) ;

} // End of namespace Details
} // End of namespace Mechanics
} // End of namespace BV
