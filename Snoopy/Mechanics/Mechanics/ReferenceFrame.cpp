#include "Mechanics/ReferenceFrame.hpp"

namespace BV {
namespace Mechanics {

using namespace Geometry::Rotation ;

ReferenceFrame::ReferenceFrame(void) : 
    parent_(nullptr),
    updateGlobalTransformNeeded_(true),
    globalTransformID_(0),
    parentGlobalTransformID_(0)
{
}

ReferenceFrame::ReferenceFrame(const ReferenceFrame & other) :
    translator_(other.translator_), rotator_(other.rotator_),
    parent_(other.parent_),
    updateGlobalTransformNeeded_(true),
    globalTransformID_(other.globalTransformID_),
    parentGlobalTransformID_(other.parentGlobalTransformID_)
{
    if (!other.needGlobalTransformUpdate_())
    {
        translatorInGlobal_ = other.translatorInGlobal_ ;
        rotatorInGlobal_ = other.rotatorInGlobal_ ;
        updateGlobalTransformNeeded_ = false ;
    }
}

ReferenceFrame::ReferenceFrame(const ReferenceFrame & startRef,
                               const ReferenceFrame & endRef,
                               const double & interpParam) :
    parent_(nullptr),
    updateGlobalTransformNeeded_(true),
    globalTransformID_(0),
    parentGlobalTransformID_(0)
{
    // Positions
    Geometry::Vector pos((1.-interpParam) * startRef.originInGlobal().toVector()
                        + interpParam * endRef.originInGlobal().toVector()) ;
    // Rotations
    Quaternion qStart ;
    startRef.setArgToRotatorInGlobal(qStart) ;
    Quaternion qEnd ;
    endRef.setArgToRotatorInGlobal(qEnd) ;
    Quaternion qInterp(qStart.slerp(interpParam, qEnd)) ;
    translator_ = TranslatorType(pos) ;
    rotator_ = RotatorType(qInterp) ;
}

void ReferenceFrame::setTranslator(const Geometry::Translation::ABC & trans)
{
    translator_ = TranslatorType(trans) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::setTranslator(const TranslatorType & trans)
{
    translator_ = trans ;
    updateGlobalTransformNeeded_ = true ;
}
void ReferenceFrame::setTranslator(const Geometry::Point & pt)
{
    setTranslator(Geometry::Translation::Cartesian(pt)) ;
}

void ReferenceFrame::setRotator(const Geometry::Rotation::ABC & rotator)
{
    rotator_ = RotatorType(rotator) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::setRotator(const RotatorType & rotator)
{
    rotator_ = rotator ;
    updateGlobalTransformNeeded_ = true ;
}

ReferenceFrame & ReferenceFrame::operator=(const ReferenceFrame & other)
{
    if (this != &other)
    {
        translator_.unknowns(other.translator_.unknowns()) ;
        rotator_.copy(other.rotator_) ;
        parent_ = other.parent_ ;
        globalTransformID_ = other.globalTransformID_ ;
        parentGlobalTransformID_ = other.parentGlobalTransformID_ ;
        if (!other.needGlobalTransformUpdate_())
        {
            translatorInGlobal_ = other.translatorInGlobal_ ;
            rotatorInGlobal_ = other.rotatorInGlobal_ ;
            updateGlobalTransformNeeded_ = false ;
        }
        else
        {
            updateGlobalTransformNeeded_ = true ;
        }
    }
    return *this ;
}

Geometry::Point ReferenceFrame::originInGlobal(void) const
{
    Geometry::Point ctr(translator_.toPoint()) ;
    if (hasParent())
    {
        // As parent is not null,
        // we have to take parent position into account.
        return parent_->localToGlobal(ctr) ;
    }
    return ctr ;
}

void ReferenceFrame::originInGlobal(const Geometry::Point & o)
{
    if (hasParent())
    {
        translator_.set(parent_->globalToLocal(o)) ;
        updateGlobalTransformNeeded_ = true ;
        return ;
    }
    translator_.set(o) ;
    updateGlobalTransformNeeded_ = true ;

}

Geometry::Point ReferenceFrame::originInParent(void) const
{
    return translator_.toPoint() ;
}

void ReferenceFrame::originInParent(const Geometry::Point & o)
{
    translator_.set(o) ;
    updateGlobalTransformNeeded_ = true ;
}

Geometry::Point ReferenceFrame::originInFrame(const ReferenceFrame & other) const
{
    if (this == &other)
    {
        // No translation, return default translator !
        return Geometry::Point(0., 0., 0.) ;
    }
    if (&other == parent_)
    {
        return originInParent() ;
    }

    // An other possibility: to avoid a step of multiplying null Point(0., 0., 0.) by self local rotation
    // if no parent, translator is in global, then compute a global to local from other on point
    if (!hasParent())
    {
        return other.globalToLocal(translator_.toPoint()) ;
    }
    // else a parent exists then do a local to frame on a point from parent
    else
    {
        return parent_->localToFrame(translator_.toPoint(), other) ;
    }
}

void ReferenceFrame::originInFrame(const Geometry::Point & o,
                                   const ReferenceFrame & other)
{
    Geometry::Point oInGlobal(other.localToGlobal(o)) ;
    originInGlobal(oInGlobal) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::setParent(const ReferenceFrame & parent)
{
    checkClosedChain_(*this, parent) ;
    parent_ = &parent ;
    updateGlobalTransformNeeded_ = true ;
    parentGlobalTransformID_ = parent_->globalTransformID_ ;
}

void ReferenceFrame::setParentKeepingPosition(const ReferenceFrame & parent)
{
    if (this == &parent)
    {
        throw std::runtime_error(
            "ReferenceFrame: setting itself as parent is forbidden !") ;
    }
    // Set the relative position/orientation according to parent frame.
    translator_ = getTranslatorInFrame(parent) ;
    rotator_ = getRotatorInFrame(parent) ;
    // We then set directly the parent frame
    setParent(parent) ;
}

const ReferenceFrame & ReferenceFrame::getParent(void) const
{
    if (!hasParent())
    {
        throw InertialFrameException("The reference frame has no parent ! Use hasParent() to check before getting it.") ;
    }
    return *parent_ ;
}

bool ReferenceFrame::hasParent(void) const
{
    return parent_ != nullptr ;
}

void ReferenceFrame::translateWithTranslationInGlobal(const Geometry::Vector & vect)
{
    // if no parent, just add the vector to the translator (translator is already in global)
    if(!hasParent())
    {
        translator_.addTranslation(vect) ;
    }
    // else there is a parent, then convert the vector into parent local frame and add to self translator
    else
    {
        translator_.addTranslation(parent_->globalToLocal(vect)) ;
    }
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::translateWithTranslationInGlobal(const Geometry::Translation::ABC & abc)
{
    translateWithTranslationInGlobal(abc.toVector()) ;
}

void ReferenceFrame::translateWithTranslationInLocal(const Geometry::Vector & vect)
{
    translator_.addTranslation(localToParent(vect)) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::translateWithTranslationInLocal(const Geometry::Translation::ABC & abc)
{
    translateWithTranslationInLocal(abc.toVector()) ;
}

void ReferenceFrame::translateWithTranslationInParent(const Geometry::Vector & vect)
{
    translator_.addTranslation(vect) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::translateWithTranslationInParent(const Geometry::Translation::ABC & abc)
{
    translator_.addTranslation(abc) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::translateWithTranslationInFrame(const Geometry::Vector & vect,
                                                     const ReferenceFrame & frame)
{
    // if self has no parent, self translator is in global axis
    if(!hasParent())
    {
        translator_.addTranslation(frame.localToGlobal(vect)) ;
    }
    // else self has a parent, and we use frame to local of the parent with frame
    else
    {
        translator_.addTranslation(parent_->frameToLocal(vect, frame)) ;
    }
    updateGlobalTransformNeeded_ = true ;

}

void ReferenceFrame::translateWithTranslationInFrame(const Geometry::Translation::ABC & abc,
                                                     const ReferenceFrame & frame)
{
    translateWithTranslationInFrame(abc.toVector(), frame) ;
}

void ReferenceFrame::addOtherRotationAtLeft(const Geometry::Rotation::ABC & abc)
{
    rotator_.addOtherRotationAtLeft(abc) ;
    updateGlobalTransformNeeded_ = true ;
}

void ReferenceFrame::addOtherRotationAtRight(const Geometry::Rotation::ABC & abc)
{
    rotator_.addOtherRotationAtRight(abc) ;
    updateGlobalTransformNeeded_ = true ;
}

Geometry::Vector ReferenceFrame::d1InGlobal(void) const
{
    Geometry::Vector d1(rotator_.d1()) ;
    if (hasParent())
    {
        return parent_->localToGlobal(d1) ;
    }
    return d1 ;
}
Geometry::Vector ReferenceFrame::d2InGlobal(void) const
{
    Geometry::Vector d2(rotator_.d2()) ;
    if (hasParent())
    {
        return parent_->localToGlobal(d2) ;
    }
    return d2 ;
}
Geometry::Vector ReferenceFrame::d3InGlobal(void) const
{
    Geometry::Vector d3(rotator_.d3()) ;
    if (hasParent())
    {
        return parent_->localToGlobal(d3) ;
    }
    return d3 ;
}

Geometry::Vector ReferenceFrame::d1InParent( void ) const
{
    return rotator_.d1() ;
}
Geometry::Vector ReferenceFrame::d2InParent( void ) const
{
    return rotator_.d2() ;
}
Geometry::Vector ReferenceFrame::d3InParent( void ) const
{
    return rotator_.d3() ;
}

Geometry::Transform3D ReferenceFrame::getLocalToGlobalTransform3D(void) const
{
    return Geometry::Transform3D(getTranslatorInGlobal(), getRotatorInGlobal()) ;
}

Geometry::Transform3D ReferenceFrame::getGlobalToLocalTransform3D(void) const
{
    return getLocalToGlobalTransform3D().inverse() ;
}

Geometry::Transform3D ReferenceFrame::getLocalToParentTransform3D(void) const
{
    return Geometry::Transform3D(getTranslatorInParent(), getRotatorInParent()) ;
}

Geometry::Transform3D ReferenceFrame::getParentToLocalTransform3D(void) const
{
    return getLocalToParentTransform3D().inverse() ;
}

Geometry::Transform3D ReferenceFrame::getLocalToFrameTransform3D(const ReferenceFrame & frame) const
{
    return Geometry::Transform3D(getTranslatorInFrame(frame),
                                 getRotatorInFrame(frame)) ;
}

Geometry::Transform3D ReferenceFrame::getFrameToLocalTransform3D(const ReferenceFrame & frame) const
{
    return getLocalToFrameTransform3D(frame).inverse() ;
}

unsigned int ReferenceFrame::nUnknowns(void) const
{
    return translator_.nUnknowns() + rotator_.nUnknowns() ;
}

unsigned int ReferenceFrame::nConstraints(void) const
{
    return translator_.nConstraints() + rotator_.nConstraints() ;
}

Eigen::VectorXd ReferenceFrame::unknowns(void) const
{
    unsigned nTransUnk(translator_.nUnknowns()) ;
    unsigned nRotUnk(rotator_.nUnknowns()) ;
    if ((nTransUnk + nRotUnk) != static_cast<unsigned>(unknowns_.size()))
    {
        refreshUnknownsAndConstraints_() ;
    }
    // Translator unknowns
    unknowns_.head(nTransUnk) = translator_.unknowns() ;
    // Rotator unknowns
    unknowns_.tail(nRotUnk) = rotator_.unknowns() ;
    return unknowns_ ;
}

void ReferenceFrame::unknowns(const Eigen::VectorXd & unknowns)
{
    assert (unknowns.size() == nUnknowns()) ;
    // First set the translator unknowns...
    translator_.unknowns(unknowns.head(translator_.nUnknowns())) ;
    // ...then set the rotator ones
    rotator_.unknowns(unknowns.tail(rotator_.nUnknowns())) ;
    updateGlobalTransformNeeded_ = true ;
}

Eigen::VectorXd ReferenceFrame::constraints(void)
{
    unsigned nTransCons(translator_.nConstraints()) ;
    unsigned nRotCons(rotator_.nConstraints()) ;
    if ((nTransCons + nRotCons) != static_cast<unsigned>(constraints_.size()))
    {
        refreshUnknownsAndConstraints_() ;
    }
    // Translator constraints
    constraints_.head(nTransCons) = translator_.constraints() ;
    // Rotator constraints
    constraints_.tail(nRotCons) = rotator_.constraints() ;
    return constraints_ ;
}

ReferenceFrame ReferenceFrame::inverseInParent(void) const
{
    ReferenceFrame inv(*this) ;
    inv.translator_.inverse() ;
    inv.rotator_.inverse() ;
    inv.updateGlobalTransformNeeded_ = true ;
    return inv ;
}

ReferenceFrame ReferenceFrame::inverseInGlobal(void) const
{
    ReferenceFrame inv ;
    inv.translator_ = getTranslatorInGlobal() ;
    inv.rotator_ = getRotatorInGlobal() ;
    inv.translator_.inverse() ;
    inv.rotator_.inverse() ;
    if(hasParent())
    {
        inv.setParentKeepingPosition(getParent()) ;
    }
    return inv ;
}

ReferenceFrame ReferenceFrame::operator-(const ReferenceFrame & other) const
{
    ReferenceFrame tmp ;
    // Substract other translation from self
    tmp.translator_ = getTranslatorInFrame(other) ;
    tmp.rotator_ = getRotatorInFrame(other) ;
    return tmp ;
}

void ReferenceFrame::refreshUnknownsAndConstraints_(void) const
{
    unknowns_ = Eigen::VectorXd(nUnknowns()) ;
    constraints_ = Eigen::VectorXd(nConstraints()) ;
}

void ReferenceFrame::checkClosedChain_(const ReferenceFrame & ref,
                                       const ReferenceFrame & fr) const
{
    if (&ref == &fr)
    {
        throw std::runtime_error("ReferenceFrame: setting itself as parent is forbidden !\nClosed chains are also forbidden...") ;
    }
    if (fr.hasParent())
    {
        checkClosedChain_(ref, fr.getParent()) ;
    }
}

const ReferenceFrame::TranslatorType & ReferenceFrame::getTranslatorInParent() const
{
    return translator_ ;
}

const ReferenceFrame::TranslatorType & ReferenceFrame::getTranslatorInGlobal() const
{
    updateGlobalTransform_() ;
    return translatorInGlobal_ ;
    //// if no parent, translator is in global then return the translator according to requested type
    //if (!hasParent())
    //{
    //    return getTranslatorInParent() ;
    //}
    //// else a parent exists then do a local to global on a point from parent
    //else
    //{
    //    return TranslatorType(
    //                    parent_->localToGlobal(translator_.toPoint())
    //                                           ) ;
    //}

}

ReferenceFrame::TranslatorType  ReferenceFrame::getTranslatorInFrame(
                                                    const ReferenceFrame & other
                                                                    ) const
{
    if (&other == parent_)
    {
        return getTranslatorInParent() ;
    }

    if (this == &other)
    {
        // No translation, return default translator !
        return TranslatorType(0., 0., 0.) ;
    }

    // An other possibility: to avoid a step of multiplying null Point(0., 0., 0.) by self local rotation
    // if no parent, translator is in global, then compute a global to local from other on point
    if (!hasParent())
    {
        return TranslatorType(other.globalToLocal(translator_.toPoint())) ;
    }
    // else a parent exists then do a local to frame on a point from parent
    else
    {
        return TranslatorType(parent_->localToFrame(translator_.toPoint(), other)) ;
    }
}

const ReferenceFrame::RotatorType & ReferenceFrame::getRotatorInParent() const
{
    return rotator_ ;
}

bool ReferenceFrame::needGlobalTransformUpdate_() const
{
    if (updateGlobalTransformNeeded_)
    {
        return true ;
    }
    if (hasParent())
    {
        if (parent_->needGlobalTransformUpdate_())
        {
            return true ;
        }
        if (parent_->globalTransformID_ != parentGlobalTransformID_)
        {
            return true ;
        }
    }
    return false ;
}

void ReferenceFrame::updateGlobalTransform_() const
{
    if (!needGlobalTransformUpdate_())
    {
        return ;
    }
    rotatorInGlobal_.reset() ;
    translatorInGlobal_ = TranslatorType() ;
    rotatorInGlobal_.addOtherRotationAtLeft(rotator_) ;
    if (hasParent())
    {
        rotatorInGlobal_.addOtherRotationAtLeft(parent_->getRotatorInGlobal()) ;
        translatorInGlobal_.addTranslation(parent_->localToGlobal(translator_.toPoint())) ;
        parentGlobalTransformID_ = parent_->globalTransformID_ ;
    }
    else
    {
        translatorInGlobal_.addTranslation(translator_) ;
    }
    ++globalTransformID_ ;
    updateGlobalTransformNeeded_ = false ;
}

const ReferenceFrame::RotatorType & ReferenceFrame::getRotatorInGlobal() const
{
    updateGlobalTransform_() ;
    return rotatorInGlobal_ ;
    //RotatorType rot ;
    //composeRotations_(*this, rot) ;
    //return rot ;
}

void ReferenceFrame::composeRotations_(const ReferenceFrame & ref,
                                       ReferenceFrame::RotatorType & rotator) const
{
    rotator.addOtherRotationAtLeft(ref.rotator_) ;
    if (ref.hasParent())
    {
        composeRotations_(ref.getParent(), rotator) ;
    }
}

Eigen::Matrix3d ReferenceFrame::getRotationMatrixInGlobal() const
{
    return getRotatorInGlobal().getMatrix() ;
    //setArgToRotatorInGlobal(tmpRotator_) ;
    //return tmpRotator_.getMatrix() ;
    // FIXME direct construction for optimisation
    //Eigen::Matrix3d mat(Eigen::Matrix3d::Zero()) ;
    //mat.col(0) = d1InGlobal().toArray() ;
    //mat.col(1) = d2InGlobal().toArray() ;
    //mat.col(2) = d3InGlobal().toArray() ;
    //return mat ;
}

ReferenceFrame::RotatorType ReferenceFrame::getRotatorInFrame(
                                                    const ReferenceFrame & other
                                                             ) const
{
    if (this == &other)
    {
        // No rotation, return default rotator !
        return RotatorType() ;
    }
    if (&other == parent_)
    {
        return getRotatorInParent() ;
    }
    RotatorType tmp(getRotatorInGlobal()) ;
    tmp.subtractOtherRotationAtLeft(other.getRotatorInGlobal()) ;
    return tmp ;
}

void ReferenceFrame::setArgToRotatorInGlobal(RotatorType & rotator) const
{
    updateGlobalTransform_() ;
    rotator = rotatorInGlobal_ ;
    // FIXME deal with whatever rotator type ?
    //rotator.reset() ;
    //composeRotations_(*this, rotator) ;
}

void ReferenceFrame::setArgToRotatorInFrame(RotatorType & rotator,
                                            const ReferenceFrame & other) const
{
    rotator.reset() ;
    if (this == &other)
    {
        // No rotation, leave it like that
        return ;
    }
    if (&other == parent_)
    {
        RotatorType rot(getRotatorInParent()) ;
        rotator.copy(rot) ;
        return ;
    }
    setArgToRotatorInGlobal(rotator) ;
    RotatorType otherRot ;
    other.setArgToRotatorInGlobal(otherRot) ;
    rotator.subtractOtherRotationAtLeft(otherRot) ;
}

} // End of namespace Mechanics
} // End of namespace BV
