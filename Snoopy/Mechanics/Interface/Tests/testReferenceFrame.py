#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
import pytest

from Opera import Geometry
from Opera.Mechanics import ReferenceFrame

def isClose(val1, val2, eps=1.e-8):
    if type(val1) in (Geometry.Point, Geometry.Vector):
        v1 = val1.asNumpyArray()
    else:
        v1 = val1
    if type(val2) in (Geometry.Point, Geometry.Vector):
        v2 = val2.asNumpyArray()
    else:
        v2 = val2
    return numpy.all(numpy.abs(v1 - v2) < eps)

def testReferenceFrame():
    myOrigin = Geometry.Point(1., 2., 3.)
    rf = ReferenceFrame()
    rf.originInParent = myOrigin
    assert isClose(rf.originInParent, myOrigin)
    d1 = Geometry.Vector(1., 0., 0.)
    d2 = Geometry.Vector(0., 1., 0.)
    d3 = Geometry.Vector(0., 0., 1.)
    # Testing one after the other...
    assert isClose(rf.d1InGlobal, d1)
    assert isClose(rf.d2InGlobal, d2)
    assert isClose(rf.d3InGlobal, d3)
    # ... or asking everything in the same time.
    #assert isClose(rf.directors()[0], d1)
    #assert isClose(rf.directors()[1], d2)
    #assert isClose(rf.directors()[2], d3)
    zeroPt = Geometry.Point(0., 0., 0.)
    assert isClose(rf.localToGlobal(zeroPt), rf.originInGlobal)
    rf2 = ReferenceFrame()
    rf2.setParent(rf)
    rf2.originInParent = Geometry.Point(1., 1., 1.)
    assert isClose(rf2.originInParent, Geometry.Point(1., 1., 1.))
    assert isClose(rf2.originInGlobal, Geometry.Point(2., 3., 4.))
    # Playing with unknowns and constraints
    assert rf.nUnknowns() == len(rf.getUnknowns())
    assert rf.nConstraints() == len(rf.getConstraints())
    rf.setUnknowns(numpy.arange(rf.nUnknowns(), dtype=float))
    assert isClose(rf.getUnknowns(), numpy.arange(rf.nUnknowns(), dtype=float))
    # We may not do any other operation after this as the unknowns set
    # are probably all wrong (they do not represent a rotation at all...)

def testReferenceFrameTranslatorSetter():
    myOrigin = Geometry.Point( 1., 2., 3. )
    rf = ReferenceFrame()
    rf.originInParent = myOrigin
    three = ( 1., 2., 3. )
    rf.setTranslator(Geometry.Cartesian(*three))

def testBasisVectors():
    myOrigin = Geometry.Point(1., 2., 3.)
    d1 = Geometry.Vector(1., 0., 0.)
    d2 = Geometry.Vector(0., 1., 0.)
    d3 = Geometry.Vector(0., 0., 1.)
    rf = ReferenceFrame()
    rf.originInParent = myOrigin
    args = (d1, d2, d3)
    def TestDirectors( frame, d1, d2, d3 ):
        assert isClose(frame.d1InGlobal,  d1)
        assert isClose(frame.d2InGlobal,  d2)
        assert isClose(frame.d3InGlobal,  d3)
    rf.setRotator(Geometry.BasisVectors(d1, d2, d3))
    TestDirectors(rf, d1, d2, d3)
    rf.setRotator(Geometry.BasisVectors(*args))
    TestDirectors(rf, d1, d2, d3)
    rf.setRotator(Geometry.BasisVectors(d1.x, d1.y, d1.z,
                                        d2.x, d2.y, d2.z,
                                        d3.x, d3.y, d3.z))
    TestDirectors(rf, d1, d2, d3)

def _DefaultCoordinatesTester(rf, origin, d1, d2, d3):
    assert isClose(origin, rf.originInParent)
    assert isClose(d1, rf.d1InGlobal)
    assert isClose(d2, rf.d2InGlobal)
    assert isClose(d3, rf.d3InGlobal)

def testDefaultCoordinates():
    rotators = ( ( Geometry.RotatorTypeEnum.QUATERNION,Geometry.Quaternion() ),
                ( Geometry.RotatorTypeEnum.EULER_ANGLES_ZYX_i, Geometry.EulerAngles_ZYX_i() ),
                (Geometry.RotatorTypeEnum.BASIS_VECTORS, Geometry.BasisVectors()),
                (Geometry.RotatorTypeEnum.AXIS_AND_ANGLE, Geometry.AxisAndAngle()),
                (Geometry.RotatorTypeEnum.MRP, Geometry.MRP()),
                (Geometry.RotatorTypeEnum.ROTATION_MATRIX, Geometry.RotationMatrix()))
    origin = Geometry.Point(1., 2., 3.)
    d1 = Geometry.Vector(1., 0., 0.)
    d2 = Geometry.Vector(0., 1., 0.)
    d3 = Geometry.Vector(0., 0., 1.)
    for rEnum, r in rotators:
        rf = ReferenceFrame()
        rf.setRotator(r)
        rf.setTranslator(Geometry.Cartesian(origin))
        yield _DefaultCoordinatesTester, rf, origin, d1, d2, d3

def testReferenceFrameParent():
    parent = ReferenceFrame()
    child = ReferenceFrame()
    child.setParent(parent)
    assert child.hasParent()
    assert parent.hasParent() == False
    # FIXME child.getParent() method returns a copy of parent
    # Translate parent frame
    parent.translateWithTranslationInParent(Geometry.Vector(5., 6., 7.))
    # Check all origins
    assert isClose(parent.originInGlobal, Geometry.Point(5., 6., 7.))
    assert isClose(parent.originInParent, Geometry.Point(5., 6., 7.))
    assert isClose(child.originInGlobal, Geometry.Point(5., 6., 7.))
    assert isClose(child.originInParent, Geometry.Point(0., 0., 0.))
    assert isClose(parent.d1InGlobal, child.d1InGlobal)
    assert isClose(parent.d2InGlobal, child.d2InGlobal)
    assert isClose(parent.d3InGlobal, child.d3InGlobal)
    assert isClose(parent.d1InGlobal, child.d1InParent)
    assert isClose(parent.d2InGlobal, child.d2InParent)
    assert isClose(parent.d3InGlobal, child.d3InParent)
    # Check conversions from a frame to the other
    pt = Geometry.Point(1., 2., 3.)
    assert isClose(child.localToGlobal(pt), Geometry.Point(6., 8., 10.))
    assert isClose(child.localToParent(pt), pt)
    assert isClose(child.parentToLocal(pt), pt)
    assert isClose(child.globalToLocal(pt), Geometry.Point(-4., -4., -4))
    assert isClose(parent.localToGlobal(pt), Geometry.Point(6., 8., 10.))
    assert isClose(parent.localToParent(pt), Geometry.Point(6., 8., 10.))
    assert isClose(parent.parentToLocal(pt), Geometry.Point(-4., -4., -4))
    assert isClose(parent.globalToLocal(pt), Geometry.Point(-4., -4., -4))
    # Check again with another rotator
    child.setRotator(Geometry.Quaternion())
    assert isClose(child.localToGlobal(pt), Geometry.Point(6., 8., 10.))
    assert isClose(child.localToParent(pt), pt)
    assert isClose(child.parentToLocal(pt), pt)
    assert isClose(child.globalToLocal(pt), Geometry.Point(-4., -4., -4))
    assert isClose(parent.localToGlobal(pt), Geometry.Point(6., 8., 10.))
    assert isClose(parent.localToParent(pt), Geometry.Point(6., 8., 10.))
    assert isClose(parent.parentToLocal(pt), Geometry.Point(-4., -4., -4))
    assert isClose(parent.globalToLocal(pt), Geometry.Point(-4., -4., -4))
    # Translate child
    child.translateWithTranslationInParent(Geometry.Vector(1., 1., 1.))
    assert isClose(child.localToGlobal(pt), Geometry.Point(7., 9., 11.))
    assert isClose(child.localToParent(pt), Geometry.Point(2., 3., 4.))
    assert isClose(child.parentToLocal(pt), Geometry.Point(0., 1., 2.))
    assert isClose(child.globalToLocal(pt), Geometry.Point(-5., -5., -5.))
    # Rotate parent of 90° around z axis
    parent.addOtherRotationAtRight(Geometry.BasisVectors(Geometry.Vector(0., 1., 0.),
                                        Geometry.Vector(-1., 0., 0.),
                                        Geometry.Vector(0., 0., 1.)))
    # Now we check change of basis
    assert isClose(parent.localToGlobal(pt), Geometry.Point(3., 7., 10.))
    assert isClose(parent.localToParent(pt), Geometry.Point(3., 7., 10.))
    assert isClose(parent.parentToLocal(pt), Geometry.Point(-4., 4., -4.))
    assert isClose(parent.globalToLocal(pt), Geometry.Point(-4., 4., -4.))
    assert isClose(child.localToGlobal(pt), Geometry.Point(2., 8., 11.))
    assert isClose(child.localToParent(pt), Geometry.Point(2., 3., 4.))
    assert isClose(child.parentToLocal(pt), Geometry.Point(0., 1., 2.))
    assert isClose(child.globalToLocal(pt), Geometry.Point(-5., 3., -5.))
    # Try to set another parent to child (get rid of the other)
    otherParent = ReferenceFrame()
    otherParent.setTranslator(Geometry.Cartesian(-1., -1., -1.))
    child.setParent(otherParent)
    # Check that child's parent is now otherParent
    assert isClose(child.localToGlobal(pt), Geometry.Point(1., 2., 3.))
    assert isClose(child.localToParent(pt), Geometry.Point(2., 3., 4.))
    assert isClose(child.parentToLocal(pt), Geometry.Point(0., 1., 2.))
    assert isClose(child.globalToLocal(pt), Geometry.Point(1., 2., 3.))
    # Check parent setting keeping global position of child
    child.setParentKeepingPosition(parent)
    assert isClose(child.localToGlobal(pt), Geometry.Point(1., 2., 3.))
    assert isClose(child.localToParent(pt), Geometry.Point(-4., 4., -4.))
    assert isClose(child.parentToLocal(pt), Geometry.Point(3., 7., 10.))
    assert isClose(child.globalToLocal(pt), Geometry.Point(1., 2., 3.))

def testReferenceFrameToReferenceFrame():
    rf1 = ReferenceFrame()
    rf2 = ReferenceFrame()
    rf3 = ReferenceFrame()
    rf3.setParent(rf1)
    rf1.setTranslator(Geometry.Cartesian(1., 2., 3.))
    rf2.setTranslator(Geometry.Cartesian(4., 5., 6.))
    pt = Geometry.Point(1., 1., 1.)
    assert isClose(rf1.localToFrame(pt, rf2), Geometry.Point(-2., -2., -2.))
    assert isClose(rf1.frameToLocal(pt, rf2), Geometry.Point(4., 4., 4.))
    assert isClose(rf2.localToFrame(pt, rf1), Geometry.Point(4., 4., 4.))
    assert isClose(rf2.frameToLocal(pt, rf1), Geometry.Point(-2., -2., -2.))
    assert isClose(rf3.localToFrame(pt, rf2), Geometry.Point(-2., -2., -2.))
    assert isClose(rf3.frameToLocal(pt, rf2), Geometry.Point(4., 4., 4.))

#def testReferenceFrameUnknownsConstraints():
#    # We are mainly going to change the rotators and use the
#    # default Cartesian translator.
#    rf = ReferenceFrame()
#    rf.setRotator(Geometry.BasisVectors())
#    assert rf.nUnknowns() == (3+9)
#    assert rf.nConstraints() == 6
#    compUnknowns = numpy.zeros(rf.nUnknowns(), dtype=float)
#    # First unknowns concern the translator (3 unknowns)
#    compUnknowns[3] = 1.
#    compUnknowns[7] = 1.
#    compUnknowns[11] = 1.
#    compConstraints = numpy.zeros(rf.nConstraints(), dtype=float)
#    assert isClose(rf.getUnknowns(), compUnknowns)
#    assert isClose(rf.getConstraints(), compConstraints)
#    # Try to change the unknowns
#    tmp = compUnknowns + 0. # Copy
#    # First translate the frame
#    tmp[0] = 1.
#    tmp[1] = 2.
#    tmp[2] = 3.
#    rf.setUnknowns(tmp)
#    # We check the translation by checking the change of basis of a point
#    assert isClose(rf.localToGlobal(Geometry.Point(1., 1., 1.)),
#                   Geometry.Point(2., 3., 4.))
#    # Try to rotate the frame too of 90° around z axis
#    tmp2 = numpy.zeros(rf.nUnknowns(), dtype=float)
#    tmp2[4] = 1.
#    tmp2[6] = -1.
#    tmp2[11] = 1.
#    rf.setUnknowns(tmp2)
#    # We check the translation by checking the change of basis of a point
#    assert isClose(rf.localToGlobal(Geometry.Point(1., 1., 1.)),
#                   Geometry.Point(-1., 1., 1.))
#    # Reset the unknowns
#    rf.setUnknowns(compUnknowns)
#    # Check other rotators number of unknowns (refresh)
#    rf.setRotator(Geometry.AxisAndAngle())
#    assert rf.nUnknowns() == (3+4)
#    assert rf.nConstraints() == 1
#    rf.setRotator(Geometry.RotationMatrix())
#    assert rf.nUnknowns() == (3+9)
#    assert rf.nConstraints() == 6
#    rf.setRotator(Geometry.Quaternion())
#    assert rf.nUnknowns() == (3+4)
#    assert rf.nConstraints() == 1
#    rf.setRotator(Geometry.MRP())
#    assert rf.nUnknowns() == (3+3)
#    assert rf.nConstraints() == 0
#    rf.setRotator(Geometry.EulerAngles_ZXY_e())
#    assert rf.nUnknowns() == (3+3)
#    assert rf.nConstraints() == 0

def testReferenceFrameTranslatorAndRotatorGetSet():
    rf = ReferenceFrame()
    # Check rotator get/set
    bv = Geometry.BasisVectors(Geometry.Vector(0., 1., 0.),
                               Geometry.Vector(-1., 0., 0.),
                               Geometry.Vector(0., 0., 1.))
    rf.setRotator(bv)
    #assert rf.nUnknowns() == (3+9)
    #assert rf.nConstraints() == 6
    rot = rf.getRotatorInParent()
    assert isClose(rot.d1, bv.d1)
    assert isClose(rot.d2, bv.d2)
    assert isClose(rot.d3, bv.d3)
    assert isClose(rot.d1, rf.d1InGlobal)
    assert isClose(rot.d2, rf.d2InGlobal)
    assert isClose(rot.d3, rf.d3InGlobal)
    # Initialise quaternion from bv
    q = Geometry.Quaternion(bv)
    # Just change the type of rotator, it should remains to the same position
    #rf.setRotatorType(Geometry.RotatorTypeEnum.QUATERNION)
    assert rf.nUnknowns() == (3+4)
    assert rf.nConstraints() == 1
    rot2 = rf.getRotatorInParent().toMRP()
    assert isClose(rot2.d1, bv.d1)
    assert isClose(rot2.d2, bv.d2)
    assert isClose(rot2.d3, bv.d3)
    assert isClose(rot2.d1, rf.d1InGlobal)
    assert isClose(rot2.d2, rf.d2InGlobal)
    assert isClose(rot2.d3, rf.d3InGlobal)
    assert isClose(rot2.d1, q.d1)
    assert isClose(rot2.d2, q.d2)
    assert isClose(rot2.d3, q.d3)
    bv2 = Geometry.BasisVectors(rot2.toBasisVectors())
    assert isClose(bv2.d1, bv.d1)
    assert isClose(bv2.d2, bv.d2)
    assert isClose(bv2.d3, bv.d3)
    # Mini check for translator
    td = Geometry.Cartesian(1., 2., 3.)
    rf.setTranslator(td)
    assert isClose(rf.originInGlobal, td.center)
    #rf.setTranslatorType(Geometry.TranslatorTypeEnum.CARTESIAN)
    assert isClose(rf.originInGlobal, td.center)

def testReferenceFrameGetGlobalRotatorAndTranslator():
    rf = ReferenceFrame()
    rf2 = ReferenceFrame()
    rf2.setParent(rf)
    rf.setRotator(Geometry.Quaternion())
    rf.setTranslator(Geometry.Cartesian(1., 2., 3.))
    rf2.setRotator(Geometry.AxisAndAngle())
    rf2.setTranslator(Geometry.Cartesian(1., 1., 1.))
    # Check translations
    rfTrans = rf.getTranslatorInGlobal()
    rfTransInParent = rf.getTranslatorInParent()
    assert isClose(rfTrans.center, rf.originInGlobal)
    assert isClose(rfTransInParent.center, rf.originInGlobal)
    rf2Trans = rf2.getTranslatorInGlobal()
    rf2TransInParent = rf2.getTranslatorInParent()
    assert isClose(rf2Trans.center, Geometry.Point(2., 3., 4.))
    assert isClose(rfTransInParent.center, rf.originInParent)
    # Check rotations
    rfRot = rf.getRotatorInGlobal().toBasisVectors()
    assert isClose(rfRot.d1, rf.d1InGlobal)
    assert isClose(rfRot.d2, rf.d2InGlobal)
    assert isClose(rfRot.d3, rf.d3InGlobal)
    rf2Rot = rf2.getRotatorInGlobal().toBasisVectors()
    assert isClose(rf2Rot.d1, rf2.d1InGlobal)
    assert isClose(rf2Rot.d2, rf2.d2InGlobal)
    assert isClose(rf2Rot.d3, rf2.d3InGlobal)
    # Rotate rf2 of 90° around z axis
    rf2.addOtherRotationAtRight(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.), numpy.pi/2.))
    rf2Rot2 = rf2.getRotatorInGlobal().toBasisVectors()
    assert isClose(rf2Rot2.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rf2Rot2.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf2Rot2.d3, Geometry.Vector(0., 0., 1.))
    rf2RotInParent = rf2.getRotatorInParent().toAxisAndAngle()
    assert isClose(rf2RotInParent.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rf2RotInParent.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf2RotInParent.d3, Geometry.Vector(0., 0., 1.))
    # Then Rotate rf of 90° around z axis
    rf.addOtherRotationAtRight(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.), numpy.pi/2.))
    # Check rf rotators
    rfRot2 = rf.getRotatorInGlobal().toBasisVectors()
    assert isClose(rfRot2.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rfRot2.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rfRot2.d3, Geometry.Vector(0., 0., 1.))
    rfRotInParent = rf.getRotatorInParent().toRotationMatrix()
    assert isClose(rfRotInParent.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rfRotInParent.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rfRotInParent.d3, Geometry.Vector(0., 0., 1.))
    # Check rf2 rotators
    rf2Rot3 = rf2.getRotatorInGlobal().toBasisVectors()
    assert isClose(rf2Rot3.d1, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf2Rot3.d2, Geometry.Vector(0., -1., 0.))
    assert isClose(rf2Rot3.d3, Geometry.Vector(0., 0., 1.))
    rf2RotInParent2 = rf2.getRotatorInParent().toBasisVectors()
    assert isClose(rf2RotInParent2.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rf2RotInParent2.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf2RotInParent2.d3, Geometry.Vector(0., 0., 1.))
 
def testReferenceFrameGetRotatorAndTranslatorInFrame():
    rf = ReferenceFrame()
    rf2 = ReferenceFrame()
    rf3 = ReferenceFrame()
    rf.setTranslator(Geometry.Cartesian(1., 2., 3.))
    rf.setRotator(Geometry.Quaternion())
    rf2.setTranslator(Geometry.Cartesian(1., 1., 1.))
    rf2.setRotator(Geometry.AxisAndAngle())
    rf3.setParent(rf2)
    rf3.setTranslator(Geometry.Cartesian(2., 2., 2.))
    rf3.setRotator(Geometry.RotationMatrix())
    # Check translations
    rfTrans = rf.getTranslatorInFrame(rf2)
    assert isClose(rfTrans.center, Geometry.Point(0., 1., 2.))
    rf2Trans = rf2.getTranslatorInFrame(rf)
    assert isClose(rf2Trans.center, Geometry.Point(0., -1., -2.))
    rf3Trans = rf3.getTranslatorInFrame(rf)
    assert isClose(rf3Trans.center, Geometry.Point(2., 1., 0.))
    rf3Trans2 = rf3.getTranslatorInFrame(rf2)
    assert isClose(rf3Trans2.center, Geometry.Point(2., 2., 2.))
    assert isClose(rf3Trans2.center, rf3.getTranslatorInParent().center)
    # Check Rotations
    rfRot = rf.getRotatorInFrame(rf2).toBasisVectors()
    assert isClose(rfRot.d1, rf.d1InGlobal)
    assert isClose(rfRot.d2, rf.d2InGlobal)
    assert isClose(rfRot.d3, rf.d3InGlobal)
    assert isClose(rfRot.d1, rf2.d1InGlobal)
    assert isClose(rfRot.d2, rf2.d2InGlobal)
    assert isClose(rfRot.d3, rf2.d3InGlobal)
    rfRot2 = rf.getRotatorInFrame(rf3).toBasisVectors()
    assert isClose(rfRot.d1, rf3.d1InGlobal)
    assert isClose(rfRot.d2, rf3.d2InGlobal)
    assert isClose(rfRot.d3, rf3.d3InGlobal)
    rf2Rot = rf.getRotatorInFrame(rf2).toBasisVectors()
    assert isClose(rfRot.d1, rf.d1InGlobal)
    assert isClose(rfRot.d2, rf.d2InGlobal)
    assert isClose(rfRot.d3, rf.d3InGlobal)
    assert isClose(rfRot.d1, rf2.d1InGlobal)
    assert isClose(rfRot.d2, rf2.d2InGlobal)
    assert isClose(rfRot.d3, rf2.d3InGlobal)
    rf2Rot2 = rf.getRotatorInFrame(rf3).toBasisVectors()
    assert isClose(rfRot.d1, rf3.d1InGlobal)
    assert isClose(rfRot.d2, rf3.d2InGlobal)
    assert isClose(rfRot.d3, rf3.d3InGlobal)
    # Rotate rf2 of 90° around z axis
    rf2.addOtherRotationAtRight(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.), numpy.pi/2.))
    rf2Rot3 = rf2.getRotatorInFrame(rf).toBasisVectors()
    assert isClose(rf2Rot3.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rf2Rot3.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf2Rot3.d3, Geometry.Vector(0., 0., 1.))
    rfRot3 = rf.getRotatorInFrame(rf2).toBasisVectors()
    assert isClose(rfRot3.d1, Geometry.Vector(0., -1., 0.))
    assert isClose(rfRot3.d2, Geometry.Vector(1., 0., 0.))
    assert isClose(rfRot3.d3, Geometry.Vector(0., 0., 1.))
    rf3Rot = rf3.getRotatorInFrame(rf).toBasisVectors()
    assert isClose(rf3Rot.d1, Geometry.Vector(0., 1., 0.))
    assert isClose(rf3Rot.d2, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf3Rot.d3, Geometry.Vector(0., 0., 1.))
    rfRot4 = rf.getRotatorInFrame(rf3).toBasisVectors()
    assert isClose(rfRot4.d1, Geometry.Vector(0., -1., 0.))
    assert isClose(rfRot4.d2, Geometry.Vector(1., 0., 0.))
    assert isClose(rfRot4.d3, Geometry.Vector(0., 0., 1.))
    # Rotate rf3 of 90° around z axis
    rf3.addOtherRotationAtRight(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.), numpy.pi/2.))
    rf3Rot2 = rf3.getRotatorInFrame(rf).toBasisVectors()
    assert isClose(rf3Rot2.d1, Geometry.Vector(-1., 0., 0.))
    assert isClose(rf3Rot2.d2, Geometry.Vector(0., -1., 0.))
    assert isClose(rf3Rot2.d3, Geometry.Vector(0., 0., 1.))
    rfRot5 = rf.getRotatorInFrame(rf3).toBasisVectors()
    assert isClose(rfRot5.d1, Geometry.Vector(-1., 0., 0.))
    assert isClose(rfRot5.d2, Geometry.Vector(0., -1., 0.))
    assert isClose(rfRot5.d3, Geometry.Vector(0., 0., 1.))

def testReferenceFrameCheckChangeOfBasis():
    rf = ReferenceFrame()
    rf.setTranslator(Geometry.Cartesian(1., 2., 3.))
    rf.setRotator(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.),
                                                        numpy.pi/2.))
    # First check points change of basis
    pt = Geometry.Point(4., 5., 6.)
    pt1 = rf.localToGlobal(pt)
    assert isClose(pt1, Geometry.Point(-4., 6., 9.))
    pt2 = rf.globalToLocal(pt)
    assert isClose(pt2, Geometry.Point(3., -3., 3.))
    # Then check vectors change of basis
    v = Geometry.Vector(1., 1., 1.)
    v1 = rf.localToGlobal(v)
    assert isClose(v1, Geometry.Vector(-1., 1., 1.))
    v2 = rf.globalToLocal(v)
    assert isClose(v2, Geometry.Vector(1., -1., 1.))
    # add a child frame that matches global frame
    rf2 = ReferenceFrame()
    rf2.setParent(rf)
    rf2.setTranslator(Geometry.Cartesian(-2., 1., -3.))
    rf2.setRotator(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.),
                                                         -numpy.pi/2.))
    assert isClose(rf2.localToParent(pt), pt2)
    assert isClose(rf2.parentToLocal(pt), pt1)
    assert isClose(rf2.localToParent(v), v2)
    assert isClose(rf2.parentToLocal(v), v1)
    assert isClose(rf2.localToFrame(pt, rf), pt2)
    assert isClose(rf2.frameToLocal(pt, rf), pt1)
    assert isClose(rf2.localToFrame(v, rf), v2)
    assert isClose(rf2.frameToLocal(v, rf), v1)

def testChainedReferenceFrames():
    rf1 = ReferenceFrame()
    rf2 = ReferenceFrame()
    rf3 = ReferenceFrame()
    rf4 = ReferenceFrame()
    rf2.setParent(rf1)
    rf2.originInParent = Geometry.Point(1., 0., 0.)
    rf2.setRotator(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.),
                                         numpy.pi/2.))
    rf3.setParent(rf2)
    rf3.originInParent = Geometry.Point(1., 0., 0.)
    rf3.setRotator(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.),
                                         numpy.pi/2.))
    rf4.setParent(rf3)
    rf4.originInParent = Geometry.Point(1., 0., 0.)
    rf4.setRotator(Geometry.AxisAndAngle(Geometry.Vector(0., 0., 1.),
                                         numpy.pi/2.))
    # Test origins in global
    assert isClose(rf1.originInGlobal, Geometry.Point(0., 0., 0.))
    assert isClose(rf2.originInGlobal, Geometry.Point(1., 0., 0.))
    assert isClose(rf3.originInGlobal, Geometry.Point(1., 1., 0.))
    assert isClose(rf4.originInGlobal, Geometry.Point(0., 1., 0.))
    # Test directors in global
    d1 = Geometry.Vector(1., 0., 0.)
    d2 = Geometry.Vector(0., 1., 0.)
    d3 = Geometry.Vector(0., 0., 1.)
    assert isClose(rf1.d1InGlobal, d1) 
    assert isClose(rf1.d2InGlobal, d2) 
    assert isClose(rf1.d3InGlobal, d3) 
    assert isClose(rf2.d1InGlobal, d2) 
    assert isClose(rf2.d2InGlobal, -d1) 
    assert isClose(rf2.d3InGlobal, d3) 
    assert isClose(rf3.d1InGlobal, -d1) 
    assert isClose(rf3.d2InGlobal, -d2) 
    assert isClose(rf3.d3InGlobal, d3) 
    assert isClose(rf4.d1InGlobal, -d2)
    assert isClose(rf4.d2InGlobal, d1) 
    assert isClose(rf4.d3InGlobal, d3) 
    # Test directors in parent
    assert isClose(rf1.d1InParent, d1) 
    assert isClose(rf1.d2InParent, d2) 
    assert isClose(rf1.d3InParent, d3) 
    assert isClose(rf2.d1InParent, d2) 
    assert isClose(rf2.d2InParent, -d1) 
    assert isClose(rf2.d3InParent, d3) 
    assert isClose(rf3.d1InParent, d2) 
    assert isClose(rf3.d2InParent, -d1) 
    assert isClose(rf3.d3InParent, d3) 
    assert isClose(rf4.d1InParent, d2)
    assert isClose(rf4.d2InParent, -d1) 
    assert isClose(rf4.d3InParent, d3) 
    # Test change of basis point
    pt = Geometry.Point(1., 2., 3.)
    # localToGlobal
    assert isClose(rf1.localToGlobal(pt), Geometry.Point(1., 2., 3.))
    assert isClose(rf2.localToGlobal(pt), Geometry.Point(-1., 1., 3.))
    assert isClose(rf3.localToGlobal(pt), Geometry.Point(0., -1., 3.))
    assert isClose(rf4.localToGlobal(pt), Geometry.Point(2., 0., 3.))
    # globalToLocal
    assert isClose(rf1.globalToLocal(pt), Geometry.Point(1., 2., 3.))
    assert isClose(rf2.globalToLocal(pt), Geometry.Point(2., 0., 3.))
    assert isClose(rf3.globalToLocal(pt), Geometry.Point(0., -1., 3.))
    assert isClose(rf4.globalToLocal(pt), Geometry.Point(-1., 1., 3.))
    # localToParent
    assert isClose(rf1.localToParent(pt), Geometry.Point(1., 2., 3.))
    assert isClose(rf2.localToParent(pt), Geometry.Point(-1., 1., 3.))
    assert isClose(rf3.localToParent(pt), Geometry.Point(-1., 1., 3.))
    assert isClose(rf4.localToParent(pt), Geometry.Point(-1., 1., 3.))
    # parentToLocal
    assert isClose(rf1.parentToLocal(pt), Geometry.Point(1., 2., 3.))
    assert isClose(rf2.parentToLocal(pt), Geometry.Point(2., 0., 3.))
    assert isClose(rf3.parentToLocal(pt), Geometry.Point(2., 0., 3.))
    assert isClose(rf4.parentToLocal(pt), Geometry.Point(2., 0., 3.))
    # localToFrame
    assert isClose(rf1.localToFrame(pt, rf1), Geometry.Point(1., 2., 3.))
    assert isClose(rf1.localToFrame(pt, rf2), Geometry.Point(2., 0., 3.))
    assert isClose(rf1.localToFrame(pt, rf3), Geometry.Point(0., -1., 3.))
    assert isClose(rf1.localToFrame(pt, rf4), Geometry.Point(-1., 1., 3.))
    assert isClose(rf2.localToFrame(pt, rf1), Geometry.Point(-1., 1., 3.))
    assert isClose(rf2.localToFrame(pt, rf2), Geometry.Point(1., 2., 3.))
    assert isClose(rf2.localToFrame(pt, rf3), Geometry.Point(2., 0., 3.))
    assert isClose(rf2.localToFrame(pt, rf4), Geometry.Point(0., -1., 3.))
    assert isClose(rf3.localToFrame(pt, rf1), Geometry.Point(0., -1., 3.))
    assert isClose(rf3.localToFrame(pt, rf2), Geometry.Point(-1., 1., 3.))
    assert isClose(rf3.localToFrame(pt, rf3), Geometry.Point(1., 2., 3.))
    assert isClose(rf3.localToFrame(pt, rf4), Geometry.Point(2., 0., 3.))
    assert isClose(rf4.localToFrame(pt, rf1), Geometry.Point(2., 0., 3.))
    assert isClose(rf4.localToFrame(pt, rf2), Geometry.Point(0., -1., 3.))
    assert isClose(rf4.localToFrame(pt, rf3), Geometry.Point(-1., 1., 3.))
    assert isClose(rf4.localToFrame(pt, rf4), Geometry.Point(1., 2., 3.))
    # frameToLocal
    assert isClose(rf1.frameToLocal(pt, rf1), Geometry.Point(1., 2., 3.))
    assert isClose(rf1.frameToLocal(pt, rf2), Geometry.Point(-1., 1., 3.))
    assert isClose(rf1.frameToLocal(pt, rf3), Geometry.Point(0., -1., 3.))
    assert isClose(rf1.frameToLocal(pt, rf4), Geometry.Point(2., 0., 3.))
    assert isClose(rf2.frameToLocal(pt, rf1), Geometry.Point(2., 0., 3.))
    assert isClose(rf2.frameToLocal(pt, rf2), Geometry.Point(1., 2., 3.))
    assert isClose(rf2.frameToLocal(pt, rf3), Geometry.Point(-1., 1., 3.))
    assert isClose(rf2.frameToLocal(pt, rf4), Geometry.Point(0., -1., 3.))
    assert isClose(rf3.frameToLocal(pt, rf1), Geometry.Point(0., -1., 3.))
    assert isClose(rf3.frameToLocal(pt, rf2), Geometry.Point(2., 0., 3.))
    assert isClose(rf3.frameToLocal(pt, rf3), Geometry.Point(1., 2., 3.))
    assert isClose(rf3.frameToLocal(pt, rf4), Geometry.Point(-1., 1., 3.))
    assert isClose(rf4.frameToLocal(pt, rf1), Geometry.Point(-1., 1., 3.))
    assert isClose(rf4.frameToLocal(pt, rf2), Geometry.Point(0., -1., 3.))
    assert isClose(rf4.frameToLocal(pt, rf3), Geometry.Point(2., 0., 3.))
    assert isClose(rf4.frameToLocal(pt, rf4), Geometry.Point(1., 2., 3.))
    # getTranslatorInParent
    assert isClose(rf1.getTranslatorInParent().center, Geometry.Point(0., 0., 0.))
    assert isClose(rf2.getTranslatorInParent().center, Geometry.Point(1., 0., 0.))
    assert isClose(rf3.getTranslatorInParent().center, Geometry.Point(1., 0., 0.))
    assert isClose(rf4.getTranslatorInParent().center, Geometry.Point(1., 0., 0.))
    # getRotatorInParent (rotation of 90° around z axis)
    rot1p = rf1.getRotatorInParent().toMRP()
    assert isClose(rot1p.d1, d1)
    assert isClose(rot1p.d2, d2)
    assert isClose(rot1p.d3, d3)
    rot2p = rf2.getRotatorInParent().toQuaternion()
    assert isClose(rot2p.d1, d2)
    assert isClose(rot2p.d2, -d1)
    assert isClose(rot2p.d3, d3)
    rot3p = rf3.getRotatorInParent().toEulerAngles_XYX_e()
    assert isClose(rot3p.d1, d2)
    assert isClose(rot3p.d2, -d1)
    assert isClose(rot3p.d3, d3)
    rot4p = rf4.getRotatorInParent().toAxisAndAngle()
    assert isClose(rot4p.d1, d2)
    assert isClose(rot4p.d2, -d1)
    assert isClose(rot4p.d3, d3)
    # getTranslatorInGlobal
    assert isClose(rf1.getTranslatorInGlobal().center, Geometry.Point(0., 0., 0.))
    assert isClose(rf2.getTranslatorInGlobal().center, Geometry.Point(1., 0., 0.))
    assert isClose(rf3.getTranslatorInGlobal().center, Geometry.Point(1., 1., 0.))
    assert isClose(rf4.getTranslatorInGlobal().center, Geometry.Point(0., 1., 0.))
    # getRotatorInGlobal
    rot1g = rf1.getRotatorInGlobal()
    assert isClose(rot1g.d1, d1)
    assert isClose(rot1g.d2, d2)
    assert isClose(rot1g.d3, d3)
    rot2g = rf2.getRotatorInGlobal()
    assert isClose(rot2g.d1, d2)
    assert isClose(rot2g.d2, -d1)
    assert isClose(rot2g.d3, d3)
    rot3g = rf3.getRotatorInGlobal()
    assert isClose(rot3g.d1, -d1)
    assert isClose(rot3g.d2, -d2)
    assert isClose(rot3g.d3, d3)
    rot4g = rf4.getRotatorInGlobal()
    assert isClose(rot4g.d1, -d2)
    assert isClose(rot4g.d2, d1)
    assert isClose(rot4g.d3, d3)
    # getTranslatorInFrame
    assert isClose(rf1.getTranslatorInFrame(rf1).center, Geometry.Point(0., 0., 0.))
    assert isClose(rf1.getTranslatorInFrame(rf2).center, Geometry.Point(0., 1., 0.))
    assert isClose(rf1.getTranslatorInFrame(rf3).center, Geometry.Point(1., 1., 0.))
    assert isClose(rf1.getTranslatorInFrame(rf4).center, Geometry.Point(1., 0., 0.))
    assert isClose(rf2.getTranslatorInFrame(rf1).center, Geometry.Point(1., 0., 0.))
    assert isClose(rf2.getTranslatorInFrame(rf2).center, Geometry.Point(0., 0., 0.))
    assert isClose(rf2.getTranslatorInFrame(rf3).center, Geometry.Point(0., 1., 0.))
    assert isClose(rf2.getTranslatorInFrame(rf4).center, Geometry.Point(1., 1., 0.))
    assert isClose(rf3.getTranslatorInFrame(rf1).center, Geometry.Point(1., 1., 0.))
    assert isClose(rf3.getTranslatorInFrame(rf2).center, Geometry.Point(1., 0., 0.))
    assert isClose(rf3.getTranslatorInFrame(rf3).center, Geometry.Point(0., 0., 0.))
    assert isClose(rf3.getTranslatorInFrame(rf4).center, Geometry.Point(0., 1., 0.))
    assert isClose(rf4.getTranslatorInFrame(rf1).center, Geometry.Point(0., 1., 0.))
    assert isClose(rf4.getTranslatorInFrame(rf2).center, Geometry.Point(1., 1., 0.))
    assert isClose(rf4.getTranslatorInFrame(rf3).center, Geometry.Point(1., 0., 0.))
    assert isClose(rf4.getTranslatorInFrame(rf4).center, Geometry.Point(0., 0., 0.))
    # getRotatorInFrame
    rot1f1 = rf1.getRotatorInFrame(rf1)
    assert isClose(rot1f1.d1, d1)
    assert isClose(rot1f1.d2, d2)
    assert isClose(rot1f1.d3, d3)
    rot1f2 = rf1.getRotatorInFrame(rf2)
    assert isClose(rot1f2.d1, -d2)
    assert isClose(rot1f2.d2, d1)
    assert isClose(rot1f2.d3, d3)
    rot1f3 = rf1.getRotatorInFrame(rf3)
    assert isClose(rot1f3.d1, -d1)
    assert isClose(rot1f3.d2, -d2)
    assert isClose(rot1f3.d3, d3)
    rot1f4 = rf1.getRotatorInFrame(rf4)
    assert isClose(rot1f4.d1, d2)
    assert isClose(rot1f4.d2, -d1)
    assert isClose(rot1f4.d3, d3)
    rot2f1 = rf2.getRotatorInFrame(rf1)
    assert isClose(rot2f1.d1, d2)
    assert isClose(rot2f1.d2, -d1)
    assert isClose(rot2f1.d3, d3)
    rot2f2 = rf2.getRotatorInFrame(rf2)
    assert isClose(rot2f2.d1, d1)
    assert isClose(rot2f2.d2, d2)
    assert isClose(rot2f2.d3, d3)
    rot2f3 = rf2.getRotatorInFrame(rf3)
    assert isClose(rot2f3.d1, -d2)
    assert isClose(rot2f3.d2, d1)
    assert isClose(rot2f3.d3, d3)
    rot2f4 = rf2.getRotatorInFrame(rf4)
    assert isClose(rot2f4.d1, -d1)
    assert isClose(rot2f4.d2, -d2)
    assert isClose(rot2f4.d3, d3)
    rot3f1 = rf3.getRotatorInFrame(rf1)
    assert isClose(rot3f1.d1, -d1)
    assert isClose(rot3f1.d2, -d2)
    assert isClose(rot3f1.d3, d3)
    rot3f2 = rf3.getRotatorInFrame(rf2)
    assert isClose(rot3f2.d1, d2)
    assert isClose(rot3f2.d2, -d1)
    assert isClose(rot3f2.d3, d3)
    rot3f3 = rf3.getRotatorInFrame(rf3)
    assert isClose(rot3f3.d1, d1)
    assert isClose(rot3f3.d2, d2)
    assert isClose(rot3f3.d3, d3)
    rot3f4 = rf3.getRotatorInFrame(rf4)
    assert isClose(rot3f4.d1, -d2)
    assert isClose(rot3f4.d2, d1)
    assert isClose(rot3f4.d3, d3)
    rot4f1 = rf4.getRotatorInFrame(rf1)
    assert isClose(rot4f1.d1, -d2)
    assert isClose(rot4f1.d2, d1)
    assert isClose(rot4f1.d3, d3)
    rot4f2 = rf4.getRotatorInFrame(rf2)
    assert isClose(rot4f2.d1, -d1)
    assert isClose(rot4f2.d2, -d2)
    assert isClose(rot4f2.d3, d3)
    rot4f3 = rf4.getRotatorInFrame(rf3)
    assert isClose(rot4f3.d1, d2)
    assert isClose(rot4f3.d2, -d1)
    assert isClose(rot4f3.d3, d3)
    rot4f4 = rf4.getRotatorInFrame(rf4)
    assert isClose(rot4f4.d1, d1)
    assert isClose(rot4f4.d2, d2)
    assert isClose(rot4f4.d3, d3)

@pytest.mark.xfail(raises=RuntimeError)
def testSetSelfAsParent():
    rf = ReferenceFrame()
    rf.setParent(rf)

@pytest.mark.xfail(raises=RuntimeError)
def testClosedChainReferenceFrames():
    # Closed chains do not work...
    rf1 = ReferenceFrame()
    rf2 = ReferenceFrame()
    rf3 = ReferenceFrame()
    rf4 = ReferenceFrame()
    rf1.setParent(rf4)
    rf2.setParent(rf1)
    rf3.setParent(rf2)
    rf4.setParent(rf3)
