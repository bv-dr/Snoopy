#pragma once

#include <pybind11/eigen.h>
#include <pybind11/operators.h>
#include <pybind11/stl.h>

#include "Mechanics/Torsor.hpp"
#include "Mechanics/ReferenceFrame.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"

namespace BV {
namespace PythonInterface {
namespace Mechanics {
namespace Torsor {

namespace py = pybind11 ;

template <typename TorsorType>
void DefineDerivedTorsor_(py::module & m, const char * name,
                          const char * classMsg)
{
    py::class_<TorsorType, BV::Mechanics::TorsorBase>(m, name, classMsg)
        .def(py::init<>(),
             R"rst(
             Default initialisation.
             This initialisation sets a default reference frame in the global
             reference frame and null resultant and moment. 
             )rst")
        .def(py::init<const BV::Mechanics::ReferenceFrame &,
                      const BV::Geometry::Vector &,
                      const BV::Geometry::Vector &>(),
             R"rst(
             Initialisation with a reference frame, the resultant and the
             moment vectors.

             :param ref: the :py:class:`~Mechanics.ReferenceFrame` representing
                 the basis and reduction point.
             :param resultant: the :py:class:`~Geometry.Vector` containing the
                 resultant.
             :param moment: the :py:class:`~Geometry.Vector` containing the
                 moment.
             )rst")
        .def(py::init<const BV::Mechanics::ReferenceFrame &,
                      const Eigen::Matrix<double, 6, 1> &>(),
             R"rst(
             Initialisation with a reference frame and a 6 component vector
             containing the resultant and moment values.

             :param ref: the :py:class:`~Mechanics.ReferenceFrame` representing
                 the basis and reduction point.
             :param v: the :py:class:`numpy.ndarray` containing the resultant
                 and moment values: first 3 components are the resultant,
                 3 last ones are the moment.
             )rst")
        .def(py::init<const BV::Mechanics::ReferenceFrame &>(),
             R"rst(
             Initialisation with a reference frame, the resultant and moment
             vectors are initialised with null components.

             :param ref: the :py:class:`~Mechanics.ReferenceFrame` representing
                 the basis and reduction point.
             )rst")
        .def(py::init<const BV::Mechanics::TorsorBase &>(),
             R"rst(
             Initialisation from another torsor.

             :param other: the :py:class:`~Mechanics.Torsor` to be copied.
             )rst")
        .def("getInGlobalBasis", &TorsorType::getInGlobalBasis,
             R"rst(
             Return a new torsor which components are expressed in global
             basis **without** moment displacement.

             :return: a new torsor expressed in global basis.
             )rst")
        .def("getInOtherRefFrameBasis", &TorsorType::getInOtherRefFrameBasis,
             R"rst(
             Same as :py:meth:`changeBasis` but returns a copy of the torsor
             instead of modifying it.

             :return: a new torsor expressed in provided basis **without**
                 moment displacement.
             )rst")
        .def("getTransportedInParentRefFrame", &TorsorType::getTransportedInParentRefFrame,
             R"rst(
             Same as :py:meth:`transportInParentRefFrame` but returns a copy
             of the torsor instead of modifying it.

             :return: a new torsor expressed in parent basis **with**
                 moment displacement.
             )rst")
        .def("getTransportedInOtherRefFrame", py::overload_cast<const BV::Mechanics::ReferenceFrame &, const bool &>(&TorsorType::getTransportedInOtherRefFrame, py::const_),
             R"rst(
             Same as :py:meth:`transportInOtherRefFrame` but returns a copy
             of the torsor instead of modifying it.

             :return: a new torsor expressed in provided basis **with**
                 moment displacement.
             )rst")
        .def("getTransportedInOtherRefFrame", py::overload_cast<const BV::Geometry::Point &, const BV::Mechanics::ReferenceFrame &, const bool &>(&TorsorType::getTransportedInOtherRefFrame, py::const_),
             R"rst(
             Same as :py:meth:`transportInOtherRefFrame` but returns a copy
             of the torsor instead of modifying it.

             :return: a new torsor expressed in provided basis **with**
                 moment displacement.
             )rst")
        .def(py::self += TorsorType(),
             R"rst(
             Copy and transport other torsor in current reference frame,
             then add the other resultant and moment to current ones.

             :param other: the torsor to be added
             :return: the modified torsor
             )rst")
        .def(py::self + TorsorType(),
             R"rst(
             Copy and transport other torsor in current reference frame,
             then add the other resultant and moment to current ones.

             :param other: the torsor to be added
             :return: a modified copy of the torsor
             )rst")
        .def(py::self -=  TorsorType(),
             R"rst(
             Copy and transport other torsor in current reference frame,
             then subtract the other resultant and moment to current ones.

             :param other: the torsor to be subtracted
             :return: the modified torsor
             )rst")
        .def(py::self - TorsorType(),
             R"rst(
             Copy and transport other torsor in current reference frame,
             then subtract the other resultant and moment to current ones.

             :param other: the torsor to be subtracted
             :return: the modified copy of the torsor
             )rst")
        .def(py::self *=  double(),
             R"rst(
             Multiply the resultant and moment by provided value.

             :param float scale: the multiplication scale
             :return: the modified torsor
             )rst")
        .def(py::self * double(),
             R"rst(
             Multiply the resultant and moment by provided value.

             :param float scale: the multiplication scale
             :return: the modified copy of the torsor
             )rst")
        .def(py::self / double(),
             R"rst(
             Divide the resultant and moment by provided value.

             :param float scale: the division scale
             :return: the modified copy of the torsor
             )rst")
        .def(double() * py::self,
             R"rst(
             Multiply the resultant and moment by provided value.

             :param float scale: the multiplication scale
             :return: the modified copy of the torsor
             )rst")
        .def(-py::self,
             R"rst(
             Compute the opposite of the resultant and moment.

             :return: the opposite copy of the torsor
             )rst")
    ;
}

void exportModule(py::module & m)
{
    py::class_<BV::Mechanics::TorsorBase>(m, "_BV::Mechanics::TorsorBase")
        .def("resetComponents", &BV::Mechanics::TorsorBase::resetComponents,
             R"rst(
             Reset the resultant and moment vectors values to 0. 
             )rst")
        .def("setReferenceFrame", &BV::Mechanics::TorsorBase::setReferenceFrame,
             R"rst(
             Copies provided reference frame and assigns.

             :param ref: the :py:class:`~Mechanics.ReferenceFrame` to be copied
                 and assigned.
             )rst")
        .def("setResultantInLocalBasis", &BV::Mechanics::TorsorBase::setResultantInLocalBasis,
             R"rst(
             Set the resultant components to the provided one values (copy).
             
             The provided resultant is supposed expressed in the local basis
             of the torsor reference frame.

             :param resultant: the :py:class:`~Geometry.Vector` containing the
                 the resultant values to be copied.
             )rst")
        .def("setMomentInLocalBasis", &BV::Mechanics::TorsorBase::setMomentInLocalBasis,
             R"rst(
             Set the moment components to the provided one values (copy).
             
             The provided moment is supposed expressed in the local basis
             of the torsor reference frame.

             :param moment: the :py:class:`~Geometry.Vector` containing the
                 the moment values to be copied.
             )rst")
        .def("getReferenceFrame", &BV::Mechanics::TorsorBase::getReferenceFrame, py::return_value_policy::copy,
             R"rst(
             Returns a copy of the internal reference frame.
             The reference frame represents the basis and reduction point.

             :return: a copy of the internal :py:class:`~Mechanics.ReferenceFrame`
             )rst")
        .def("getResultantInLocalBasis", &BV::Mechanics::TorsorBase::getResultantInLocalBasis, py::return_value_policy::copy,
             R"rst(
             Returns a copy of the internal resultant vector, expressed in the
             torsor reference frame basis.

             :return: a :py:class:`~Geometry.Vector` containing the resultant
                 values in local reference frame.
             )rst")
        .def("getMomentInLocalBasis", &BV::Mechanics::TorsorBase::getMomentInLocalBasis, py::return_value_policy::copy,
             R"rst(
             Returns a copy of the internal moment vector, expressed in the
             torsor reference frame basis.

             :return: a :py:class:`~Geometry.Vector` containing the moment
                 values in local reference frame.
             )rst")
        .def("setReferencePoint", &BV::Mechanics::TorsorBase::setReferencePoint,
             R"rst(
             Set the torsor's reference frame origin.

             The point represents the origin in parent's axis of the torsor
             reference frame.

             This method replaces the current reference frame origin **without performing**
             any operation on the moment.

             :param pt: The :py:class:`~Geometry.Point` to be set as the
                 reference frame origin in parent axis system.
             )rst")
        .def("setBasis", &BV::Mechanics::TorsorBase::setBasis,
             R"rst(
             Set the reference frame rotator.

             Provided rotator represents a change of basis from a new local
             base to the torsor's parent reference frame.

             This method replaces the current rotator **without performing**
             any operation on the resultant and moment.

             :param newBasisInParent: a :ref:`rotation object<GeometryRotation>`
                 which copy will become the new torsor basis.
             )rst")
        .def("changeReferencePointInParent", &BV::Mechanics::TorsorBase::changeReferencePointInParent,
             R"rst(
             Change the reference frame origin in its parent.

             This method replaces the current reference frame origin
             **performing** the displacement operation on the moment.

             The point provided is supposed to be expressed in the parent
             reference frame.

             :param pt: the :py:class:`~Geometry.Point` which components will
                 become the new reduction point in parent reference frame.
             )rst")
        .def("changeReferencePointInFrame", &BV::Mechanics::TorsorBase::changeReferencePointInFrame,
             R"rst(
             Change the reference frame origin in its parent relatively to
             provided reference frame.

             This method replaces the current reference frame origin
             **performing** the displacement operation on the moment.

             The point provided is supposed to be expressed in provided
             reference frame.

             :param pt: the :py:class:`~Geometry.Point` which components will
                 become the new reduction point in parent reference frame.
             :param ref: the :py:class:`~Mechanics.ReferenceFrame` in which
                 the reduction point is expressed.
             )rst")
        .def("changeBasisInParent", &BV::Mechanics::TorsorBase::changeBasisInParent,
             R"rst(
             Set the reference frame rotator.

             Provided rotator represents a change of basis from a new local
             base to the torsor's parent reference frame.

             This method replaces the current rotator **performing**
             the rotation operations on the resultant and moment, but
             **without** displacement of the moment.
             It is only a change of basis of the vectors.

             :param newBasisInParent: a :ref:`rotation object<GeometryRotation>`
                 which copy will become the new torsor basis.
             )rst")
        .def("changeBasis", &BV::Mechanics::TorsorBase::changeBasis,
             R"rst(
             Set the reference frame rotator.

             Provided frame represents an arbitrary change of basis.

             This method replaces the reference frame rotator **performing**
             the rotation of the resultant and moment, but **without**
             displacement of the moment.
             It is only a change of basis of the vectors.

             :param frame: a :py:class:`~Mechanics.ReferenceFrame` representing
                 an orientation in space.
             :param bool copyRefFrame: inform if the given reference frame
                 has to be copied and has to replace the torsor's reference frame
             )rst")
        .def("transportInOtherRefFrame", py::overload_cast<const BV::Mechanics::ReferenceFrame &, const bool &>(&BV::Mechanics::TorsorBase::transportInOtherRefFrame),
             R"rst(
             Transport the torsor in another reference frame.

             Provided frame represents an arbitrary change of basis.
             For the transport, the origin of provided reference frame is used.

             This method displaces the torsor to the given point **performing**
             a change of basis of the resultant and moment to match the basis
             represented by provided frame.

             :param frame: The :py:class:`~Mechanics.ReferenceFrame`
                 representing an orientation in space.
             :param bool copyRefFrame: inform if provided reference frame has to
                 be copied and has to replace the torsor's reference frame.
             )rst")
        .def("transportInOtherRefFrame", py::overload_cast<const BV::Geometry::Point &, const BV::Mechanics::ReferenceFrame &, const bool &>(&BV::Mechanics::TorsorBase::transportInOtherRefFrame),
             R"rst(
             Transport the torsor in another reference frame.

             Provided point represents the point in the axis system of provided
             reference frame.
             Provided frame represents an arbitrary change of basis.

             This method displaces the torsor to the given point **performing**
             a change of basis of the resultant and moment to match the basis
             represented by provided frame.

             :param pt: The :py:class:`~Geometry.Point` to be set to in provided
                 frame axis system.
             :param frame: The :py:class:`~Mechanics.ReferenceFrame`
                 representing an orientation in space.
             :param bool copyRefFrame: inform if provided reference frame has to
                 be copied and has to replace the torsor's reference frame.
             )rst")
        .def("transportInParentRefFrame", &BV::Mechanics::TorsorBase::transportInParentRefFrame,
             R"rst(
             Transport the torsor in the parent of its reference frame. 
             )rst")
        .def("__mul__", [](const BV::Mechanics::TorsorBase & t1, const BV::Mechanics::TorsorBase & t2)
                        {
                            return t1 * t2 ;
                        },
             R"rst(
             Multiplication by another torsor.
             
             This operator performs the computation of the co-moment of the torsors.
             
             At the same reduction point and basis the comoment is computed has follow
             for two torsors :math:`\mathcal{T}_a = \{R_a, M_a\}_{(O,\mathcal{B})}`
             and :math:`\mathcal{T}_b = \{R_b, M_b\}_{(O,\mathcal{B})}`

             ..math::

                 `P = R_a \cdot M_b + R_b \cdot M_a`

             The result is independant of the reduction point and basis chosen
             for both torsors.
             
             :param other: an other :py:class:`~Mechanics.Torsor`
             :return: the comoment
             :rtype: float
             )rst")
        .def("getResultantInGlobalBasis", &BV::Mechanics::TorsorBase::getResultantInGlobalBasis,
             R"rst(
             Returns the resultant vector expressed in the global axis system.

             :return: the resultant as a :py:class:`~Geometry.Vector` expressed
                 in global basis
             )rst")
        .def("getMomentInGlobalBasis", &BV::Mechanics::TorsorBase::getMomentInGlobalBasis,
             R"rst(
             Returns the moment vector expressed in the global axis system
             (**without** displacement).

             :return: the moment as a :py:class:`~Geometry.Vector` expressed
                 in global basis
             )rst")
        .def("getResultantInOtherRefFrameBasis", &BV::Mechanics::TorsorBase::getResultantInOtherRefFrameBasis,
             R"rst(
             Returns the resultant vector expressed in an arbitrary axis system

             :param frame: the :py:class:`~Mechanics.ReferenceFrame` in which
                 the resultant has to be expressed.
             :return: the resultant as a :py:class:`~Geometry.Vector` expressed
                 in provided basis
             )rst")
        .def("getMomentInOtherRefFrameBasis", &BV::Mechanics::TorsorBase::getMomentInOtherRefFrameBasis,
             R"rst(
             Returns the moment vector expressed in an arbitrary axis system
             (**without** displacement).

             :param frame: the :py:class:`~Mechanics.ReferenceFrame` in which
                 the moment has to be expressed.
             :return: the moment as a :py:class:`~Geometry.Vector` expressed
                 in provided basis
             )rst")
        .def("getMomentInOtherRefFrame", py::overload_cast<const BV::Geometry::Point &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::TorsorBase::getMomentInOtherRefFrame, py::const_),
             R"rst(
             Returns the moment vector expressed in an arbitrary reference
             frame (**with** displacement at provided point).

             :param ptInFrame: the :py:class:`~Geometry.Point` representing
                 the reduction point in provided reference frame.
             :param frame: the :py:class:`~Mechanics.ReferenceFrame` in which
                 the moment has to be expressed.
             :return: the displaced moment as a :py:class:`~Geometry.Vector`
                 expressed in provided basis
             )rst")
        .def("getMomentInOtherRefFrame", py::overload_cast<const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::TorsorBase::getMomentInOtherRefFrame, py::const_),
             R"rst(
             Returns the moment vector expressed in an arbitrary reference
             frame (**with** displacement at provided reference frame origin).

             :param frame: the :py:class:`~Mechanics.ReferenceFrame` in which
                 the moment has to be expressed.
             :return: the displaced moment as a :py:class:`~Geometry.Vector`
                 expressed in provided axis system
             )rst")
        .def("getMomentResultantInLocalBasis", &BV::Mechanics::TorsorBase::getMomentResultantInLocalBasis,
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the moment, the last 3 components
             are the resultant.

             All components are expressed in the local axis system of the
             reference frame.

             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in the local axis system.
             )rst")
        .def("getMomentResultantInGlobalBasis", &BV::Mechanics::TorsorBase::getMomentResultantInGlobalBasis,
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the moment, the last 3 components
             are the resultant.

             All components are expressed in the global axis system **without**
             moment displacement.

             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in the global axis system without moment displacement.
             )rst")
        .def("getResultantMomentInLocalBasis", &BV::Mechanics::TorsorBase::getResultantMomentInLocalBasis,
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the resultant, the last 3 components
             are the moment.

             All components are expressed in the local axis system of the
             reference frame.

             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in the local axis system.
             )rst")
        .def("getResultantMomentInGlobalBasis", &BV::Mechanics::TorsorBase::getResultantMomentInGlobalBasis,
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the resultant, the last 3 components
             are the moment.

             All components are expressed in the global axis system **without**
             moment displacement.

             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in the global axis system without moment displacement.
             )rst")
        .def("getResultantMomentInOtherRefFrameBasis", &BV::Mechanics::TorsorBase::getResultantMomentInOtherRefFrameBasis,
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the resultant, the last 3 components
             are the moment.

             All components are expressed in provided axis system **without**
             moment displacement.

             :param frame: a :py:class:`~Mechanics.ReferenceFrame` providing the
                 basis in which to express the torsor components.
             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in provided axis system without moment displacement.
             )rst")
        .def("getResultantMomentInOtherRefFrame", py::overload_cast<const BV::Geometry::Point &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::TorsorBase::getResultantMomentInOtherRefFrame, py::const_),
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the resultant, the last 3 components
             are the moment.

             All components are expressed in provided axis system at
             provided reduction point **with** moment displacement.

             :param ptInFrame: a :py:class:`~Geometry.Point` representing the
                 reduction point in provided reference frame.
             :param frame: a :py:class:`~Mechanics.ReferenceFrame` providing the
                 basis in which to express the torsor components.
             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in provided axis system with moment displacement.
             )rst")
        .def("getResultantMomentInOtherRefFrame", py::overload_cast<const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::TorsorBase::getResultantMomentInOtherRefFrame, py::const_),
             R"rst(
             Get the torsor representation as a 6 components vector.

             The first 3 components are the resultant, the last 3 components
             are the moment.

             All components are expressed in provided axis system at
             its origin **with** moment displacement.

             :param frame: a :py:class:`~Mechanics.ReferenceFrame` providing the
                 basis in which to express the torsor components.
             :return: a :py:class:`numpy.ndarray` containing the 6 torsor
                 components in provided axis system with moment displacement.
             )rst")
     ;

    DefineDerivedTorsor_<BV::Mechanics::KinematicTorsor>(m, "_KinematicTorsor",
                                                         R"rst(
                                                         Resultant: circular velocities

                                                         Moment: linear velocities
                                                         )rst") ;
    DefineDerivedTorsor_<BV::Mechanics::StaticTorsor>(m, "_StaticTorsor",
                                                         R"rst(
                                                         Resultant: force

                                                         Moment: moment, torque
                                                         )rst") ;
    DefineDerivedTorsor_<BV::Mechanics::Torsor>(m, "_Torsor", "") ;
}

} // End of namespace Torsor
} // End of namespace Mechanics
} // End of namespace PythonInterface
} // End of namespace BV
