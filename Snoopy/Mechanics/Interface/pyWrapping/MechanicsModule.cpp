#include <pybind11/pybind11.h>

#include "Interface/pyWrapping/ReferenceFrame.hpp"
#include "Interface/pyWrapping/Torsor.hpp"

PYBIND11_MODULE(_Mechanics, m)
{
	BV::PythonInterface::Mechanics::ReferenceFrame::exportModule(m) ;
	BV::PythonInterface::Mechanics::Torsor::exportModule(m) ;
}
