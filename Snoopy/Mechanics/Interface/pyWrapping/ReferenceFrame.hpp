#pragma once
#include "Tools/SpdLogger.hpp"

#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include "Mechanics/ReferenceFrame.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Translation.hpp"
#include "Geometry/Rotation.hpp"

namespace BV {
namespace PythonInterface {
namespace Mechanics {
namespace ReferenceFrame {

namespace py = pybind11 ;

template <typename RotatorType, typename Scope>
void DefineRotatorSetters(Scope & scope, const BV::Geometry::RotatorTypeEnum & e,
                          const std::string & pythonName)
{
    std::string msg("Same as :py:meth:`setRotator` with a :py:class:`~Geometry."
                    + pythonName + "`") ;
    scope.def("setRotator",
              [e](BV::Mechanics::ReferenceFrame & rf, const RotatorType & r)
              {
                  rf.setRotator(e, r) ;
              },
              msg.c_str()) ;

}

void exportModule(py::module & m)
{
	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    py::class_<BV::Mechanics::ReferenceFrame,
               std::shared_ptr<BV::Mechanics::ReferenceFrame> > ref(m, "_ReferenceFrame") ;
    ref.def(py::init<>(),
            R"rst(
            Default constructor of the reference frame.
            This constructor takes no attributes, default values are set.
            Possible other initialisations are done using helper functions:
            
            - :py:meth:`~setTranslator`
            - :py:meth:`~setRotator`
            - :py:meth:`~setParent`
            
            )rst")
        .def(py::init<const BV::Mechanics::ReferenceFrame &>(),
            R"rst(
            Copy constructor of the reference frame.
            )rst")
        .def(py::init<const BV::Mechanics::ReferenceFrame &,
                      const BV::Mechanics::ReferenceFrame &,
                      const double &>(),
             R"rst(
             Interpolation constructor.
             Interpolates between startRef and endRef at interpParam.
             
             Linear interpolation for positions.
             Quaternion slerp for rotations. todo link to Quaternion
             
             Creates a ReferenceFrame with type Quaternion Rotator.
             The reference is expressed in global axis system (no parent).
             Set it yourself if needed.
             
             :param startRef: The start reference frame
             :paramtype startRef: :py:class:`~ReferenceFrame`
             :param endRef: The end reference frame
             :paramtype endRef: :py:class:`~ReferenceFrame`
             :param float interpParam: Must be defined in range 0. (startRef),
                     1. (endRef)
             )rst")
        .def_property("originInGlobal",
#ifdef __INTEL_COMPILER
                      [](const BV::Mechanics::ReferenceFrame & rf){return rf.originInGlobal();},
#else
                      py::overload_cast<>(&BV::Mechanics::ReferenceFrame::originInGlobal, py::const_),
#endif
                      py::overload_cast<const BV::Geometry::Point &>(&BV::Mechanics::ReferenceFrame::originInGlobal),
                      R"rst(The origin of the reference frame in global reference frame.)rst")
        .def_property("originInParent",
#ifdef __INTEL_COMPILER
                      [](const BV::Mechanics::ReferenceFrame & rf){return rf.originInParent();},
#else
                      py::overload_cast<>(&BV::Mechanics::ReferenceFrame::originInParent, py::const_),
#endif
                      py::overload_cast<const BV::Geometry::Point &>(&BV::Mechanics::ReferenceFrame::originInParent),
                      R"rst(The origin of the reference frame in parent reference frame.)rst")
        .def_property_readonly("d1InGlobal", &BV::Mechanics::ReferenceFrame::d1InGlobal,
                               R"rst(
                               The :math:`d_1` director in global reference frame.
                               :math:`d_1` correspond to the transformed vector
                               :math:`X = (1, 0, 0)` by the reference frame
                               global rotation :math:`R` such
                               that :math:`d_1 = R.X`
                               )rst")
        .def_property_readonly("d2InGlobal", &BV::Mechanics::ReferenceFrame::d2InGlobal,
                               R"rst(
                               The :math:`d_2` director in global reference frame.
                               :math:`d_2` correspond to the transformed vector
                               :math:`X = (0, 1, 0)` by the reference frame
                               global rotation :math:`R` such
                               that :math:`d_2 = R.X`
                               )rst")
        .def_property_readonly("d3InGlobal", &BV::Mechanics::ReferenceFrame::d3InGlobal,
                               R"rst(
                               The :math:`d_3` director in global reference frame.
                               :math:`d_3` correspond to the transformed vector
                               :math:`X = (0, 0, 1)` by the reference frame
                               global rotation :math:`R` such
                               that :math:`d_3 = R.X`
                               )rst")
        .def_property_readonly("d1InParent", &BV::Mechanics::ReferenceFrame::d1InParent,
                               R"rst(
                               The :math:`d_1` director in parent reference frame.
                               :math:`d_1` correspond to the transformed vector
                               :math:`X = (1, 0, 0)` by the reference frame
                               rotation in parent :math:`R` such
                               that :math:`d_1 = R.X`
                               )rst")
        .def_property_readonly("d2InParent", &BV::Mechanics::ReferenceFrame::d2InParent,
                               R"rst(
                               The :math:`d_2` director in parent reference frame.
                               :math:`d_2` correspond to the transformed vector
                               :math:`X = (0, 1, 0)` by the reference frame
                               rotation in parent :math:`R` such
                               that :math:`d_2 = R.X`
                               )rst")
        .def_property_readonly("d3InParent", &BV::Mechanics::ReferenceFrame::d3InParent,
                               R"rst(
                               The :math:`d_3` director in parent reference frame.
                               :math:`d_3` correspond to the transformed vector
                               :math:`X = (0, 0, 1)` by the reference frame
                               rotation in parent :math:`R` such
                               that :math:`d_3 = R.X`
                               )rst")
        .def("setParent", &BV::Mechanics::ReferenceFrame::setParent,
             py::keep_alive<1, 2>(),
             R"rst(
             Specify a parent reference frame to this reference frame.
             This reference frame position and orientation (translator and
             rotator) will now be expressed in specified parent reference frame.
             
             Note that if this reference frame was already initialised to a
             specific position in the global reference frame or in another
             parent reference frame, its relative position according to previous
             parent will be transfered to the new parent.
             
             Also note that if this frame already has a parent, no more link
             will be taken into account with it after setting the new parent.

             :param parent: the new parent reference frame.
             )rst")
        .def("setParentKeepingPosition", &BV::Mechanics::ReferenceFrame::setParentKeepingPosition,
             py::keep_alive<1, 2>(),
             R"rst(
             Specify a parent reference frame to this reference frame.
             This reference frame position and orientation (translator and
             rotator) will now be expressed in specified parent reference frame.
             
             Note that previous position of this reference frame in global
             reference frame will be kept after setting the new parent.

             :param parent: the new parent frame.
             )rst")
        .def("getParent", &BV::Mechanics::ReferenceFrame::getParent,
             py::return_value_policy::reference,
             R"rst(
             Returns a const reference to the parent reference frame.
             Note that in case no parent is defined (you can check calling
             the :py:meth:`hasParent` function), an exception is raised.

             :return: A reference to the parent reference frame.
             :rtype: :py:class:`~Mechanics.ReferenceFrame`
             )rst")
        .def("hasParent", &BV::Mechanics::ReferenceFrame::hasParent,
             R"rst(
             Return True if this reference frame has a parent set, False
             otherwise. This utility function should be used before calling
             the :py:meth:`getParent` function to make sure to get the correct
             parent, or at least an existing one.

             :return: A boolean stating whether this reference frame has a
                 parent set.
             :rtype: bool
             )rst")
#ifdef __INTEL_COMPILER
        .def("getUnknowns", [](BV::Mechanics::ReferenceFrame & rf){return rf.unknowns();},
             R"rst(
             Return the vector containing the unknowns values (translation the
             rotation).

             Note that the unknowns are expressed in the parent frame.

             :return: The vector containing the unknowns values.
             :rtype: :py:class:`numpy.ndarray`
             )rst")
            #else
        .def("getUnknowns", py::overload_cast<>(&BV::Mechanics::ReferenceFrame::unknowns, py::const_),
             R"rst(
             Return the vector containing the unknowns values (translation the
             rotation).

             Note that the unknowns are expressed in the parent frame.

             :return: The vector containing the unknowns values.
             :rtype: :py:class:`numpy.ndarray`
             )rst")
#endif
        .def("setUnknowns", py::overload_cast<const Eigen::VectorXd &>(&BV::Mechanics::ReferenceFrame::unknowns),
             R"rst(
             Set the unknowns to specific values.

             Note that the size of the provided vector must match the number
             of unknowns (depends on the translator and rotator used).
             Also note that the unknowns are expressed in the parent frame.

             :param unknowns: The new vector of unknowns.
             :paramtype unknowns: :py:class:`numpy.ndarray`
             )rst")
        .def("getConstraints", &BV::Mechanics::ReferenceFrame::constraints,
             R"rst(
             Returns the vector containing the constraints values.

             :return: a :py:class:`numpy.ndarray` containing the constrains of
                 both the translator and rotator.
             )rst")
        .def("nUnknowns", &BV::Mechanics::ReferenceFrame::nUnknowns,
             R"rst(
             Returns the number of unknowns that characterize this frame.

             Note that this number depends on the type of translator and
             rotator used.

             :return: The number of unknowns.
             :rtype: int
             )rst")
        .def("nConstraints", &BV::Mechanics::ReferenceFrame::nConstraints,
             R"rst(
             Returns the number of constraints that characterize this frame.

             Note that this number depends on the type of translator and
             rotator used.

             :return: The number of constraints.
             :rtype: int
             )rst")
        .def("getTranslatorInGlobal", &BV::Mechanics::ReferenceFrame::getTranslatorInGlobal)
        .def("getTranslatorInParent", &BV::Mechanics::ReferenceFrame::getTranslatorInParent)
        .def("getTranslatorInFrame", &BV::Mechanics::ReferenceFrame::getTranslatorInFrame)
        .def("getRotatorInGlobal", &BV::Mechanics::ReferenceFrame::getRotatorInGlobal)
        .def("getRotatorInParent", &BV::Mechanics::ReferenceFrame::getRotatorInParent)
        .def("getRotatorInFrame", &BV::Mechanics::ReferenceFrame::getRotatorInFrame)
        .def("translateWithTranslationInGlobal", py::overload_cast<const BV::Geometry::Translation::ABC &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInGlobal),
             R"rst(
             Apply a global translation to the reference frame.
             The components of the provided :ref:`translation object<GeometryTranslation>`
             are expressed in the **global** axis system.

             Note that if the reference frame has a parent, the global
             translation is converted in parent reference frame to match the
             global one.

             :param translator: a :ref:`translation object<GeometryTranslation>`
                 describing the global translation to perform.
             )rst")
        .def("translateWithTranslationInGlobal", py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInGlobal),
             R"rst(Same as :py:meth:`translateWithTranslationInGlobal` with a :py:class:`~Geometry.Vector`)rst")
        .def("translateWithTranslationInLocal", py::overload_cast<const BV::Geometry::Translation::ABC &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInLocal),
             R"rst(
             Apply a translation to the reference frame.
             The components of the provided :ref:`translation object<GeometryTranslation>`
             are expressed in the **local** axis system.

             :param translator: a :ref:`translation object<GeometryTranslation>`
                 describing the local translation to perform.
             )rst")
        .def("translateWithTranslationInLocal", py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInLocal),
             R"rst(Same as :py:meth:`translateWithTranslationInLocal` with a :py:class:`~Geometry.Vector`)rst")
        .def("translateWithTranslationInParent", py::overload_cast<const BV::Geometry::Translation::ABC &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInParent),
             R"rst(
             Apply a translation to the reference frame.
             The components of the provided :ref:`translation object<GeometryTranslation>`
             are expressed in the **parent** axis system.

             :param translator: a :ref:`translation object<GeometryTranslation>`
                 describing the translation to perform.
             )rst")
        .def("translateWithTranslationInParent", py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInParent),
             R"rst(Same as :py:meth:`translateWithTranslationInParent` with a :py:class:`~Geometry.Vector`)rst")
        .def("translateWithTranslationInFrame", py::overload_cast<const BV::Geometry::Translation::ABC &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInFrame),
             R"rst(
             Apply a translation to the reference frame.
             The components of the provided :ref:`translation object<GeometryTranslation>`
             are expressed in the **provided frame** axis system.

             :param translator: a :ref:`translation object<GeometryTranslation>`
                 describing the translation to perform.
             :param ref: a :py:class:`~Mechanics.ReferenceFrame` object in
                 which the translation is expressed.
             )rst")
        .def("translateWithTranslationInFrame", py::overload_cast<const BV::Geometry::Vector &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::ReferenceFrame::translateWithTranslationInFrame),
             R"rst(Same as :py:meth:`translateWithTranslationInFrame` with a :py:class:`~Geometry.Vector`)rst")
        .def("addOtherRotationAtLeft", &BV::Mechanics::ReferenceFrame::addOtherRotationAtLeft,
             R"rst(
             Rotate the reference frame of the provided rotation amount at left.
             
             This method correspond to a rotation of the reference frame in the
             parent axis.
             In other words it also correspond to a (virtual) rotation of the
             parent or a rotation added before the rotation of the local axis.
             
             In addition, the provided rotator corresponds to a change of basis
             from local to parent. The initial stored rotator becomes a new
             local to local.
             
             The final rotator represents a change of basis from new local to
             parent.

             Also see :py:meth:`~Geometry.Quaternion.addOtherRotationAtLeft`
             for more information.

             :param rotator: the :ref:`rotation object<GeometryRotation>`
                 containing the rotation amount to be multiplied at left.
             )rst")
        .def("addOtherRotationAtRight", &BV::Mechanics::ReferenceFrame::addOtherRotationAtRight,
             R"rst(
             Rotate the reference frame of the provided rotation amount at right.
             
             This method correspond to a rotation of the reference frame in the
             local (final) axis. In other words it also correspond to a rotation
             after the final local rotation.
             
             In addition, the rotator in signature correspond to a change of
             basis from new local to local. The initial stored rotator
             represents a local to parent change of basis.
             
             The final rotator represents a change of basis from new local to
             parent.
             
             Also see :py:meth:`~Geometry.Quaternion.addOtherRotationAtRight`
             for more information.

             :param rotator: the :ref:`rotation object<GeometryRotation>`
                 containing the rotation amount to be multiplied at right.
             )rst")
        .def("localToGlobal",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Point & pt) {return ref.localToGlobal(pt) ; },
             //py::overload_cast<const BV::Geometry::Point &>(&BV::Mechanics::ReferenceFrame::localToGlobal, py::const_),
             R"rst(
             Return the position of a local point in the global reference frame.

             :param pt: a :py:class:`~Geometry.Point` which position is defined
                 in the local reference frame.
             :return: a :py:class:`~Geometry.Point` which position is defined
                 in the global reference frame.
             )rst")
        .def("localToGlobal",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Vector & vec) {return ref.localToGlobal(vec) ; },
             //py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::localToGlobal, py::const_),
             R"rst(
             Return the position of a local vector in the global reference frame.

             :param pt: a :py:class:`~Geometry.Vector` which position is defined
                 in the local reference frame.
             :return: a :py:class:`~Geometry.Vector` which position is defined
                 in the global reference frame.
             )rst")
        .def("globalToLocal",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Point & pt) {return ref.globalToLocal(pt) ; },
             //py::overload_cast<const BV::Geometry::Point &>(&BV::Mechanics::ReferenceFrame::globalToLocal, py::const_),
             R"rst(
             Return the position of a global point in the local reference frame.

             :param pt: a :py:class:`~Geometry.Point` which position is defined
                 in the global reference frame.
             :return: a :py:class:`~Geometry.Point` which position is defined
                 in the local reference frame.
             )rst")
        .def("globalToLocal",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Vector & vec) {return ref.globalToLocal(vec) ; },
             //py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::globalToLocal, py::const_),
             R"rst(
             Return the position of a global vector in the local reference frame.

             :param pt: a :py:class:`~Geometry.Vector` which position is defined
                 in the global reference frame.
             :return: a :py:class:`~Geometry.Vector` which position is defined
                 in the local reference frame.
             )rst")
        .def("localToParent",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Point & pt) {return ref.localToParent(pt) ; },
             //py::overload_cast<const BV::Geometry::Point &>(&BV::Mechanics::ReferenceFrame::localToParent, py::const_),
             R"rst(
             Return the position of a point defined in local reference frame
             in the parent reference frame.

             :param pt: a :py:class:`~Geometry.Point` which position is defined
                 in the local reference frame.
             :return: a :py:class:`~Geometry.Point` which position is defined
                 in the parent reference frame.
             )rst")
        .def("localToParent",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Vector & vec) {return ref.localToParent(vec) ; },
             //py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::localToParent, py::const_),
             R"rst(
             Return the position of a vector defined in local reference frame
             in the parent reference frame.

             :param pt: a :py:class:`~Geometry.Vector` which position is defined
                 in the local reference frame.
             :return: a :py:class:`~Geometry.Vector` which position is defined
                 in the parent reference frame.
             )rst")
        .def("parentToLocal",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Point & pt) {return ref.parentToLocal(pt) ; },
             //py::overload_cast<const BV::Geometry::Point &>(&BV::Mechanics::ReferenceFrame::parentToLocal, py::const_),
             R"rst(
             Return the position of a point defined in parent reference frame
             in the local reference frame.

             :param pt: a :py:class:`~Geometry.Point` which position is defined
                 in the parent reference frame.
             :return: a :py:class:`~Geometry.Point` which position is defined
                 in the local reference frame.
             )rst")
        .def("parentToLocal",
             [](const BV::Mechanics::ReferenceFrame & ref,
                const BV::Geometry::Vector & vec) {return ref.parentToLocal(vec) ; },
             //py::overload_cast<const BV::Geometry::Vector &>(&BV::Mechanics::ReferenceFrame::parentToLocal, py::const_),
             R"rst(
             Return the position of a vector defined in parent reference frame
             in the local reference frame.

             :param pt: a :py:class:`~Geometry.Vector` which position is defined
                 in the parent reference frame.
             :return: a :py:class:`~Geometry.Vector` which position is defined
                 in the local reference frame.
             )rst")
        .def("localToFrame",
             py::overload_cast<const BV::Geometry::Point &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::ReferenceFrame::localToFrame<BV::Geometry::Point>, py::const_),
             R"rst(
             Return the position of a point defined in local reference frame
             in the provided reference frame.

             :param pt: a :py:class:`~Geometry.Point` which position is defined
                 in the local reference frame. 
             :param frame: a :py:class:`~Mechanics.ReferenceFrame` in which the
                 final point should be defined.
             :return: a :py:class:`~Geometry.Point` which position is defined
                 in the provided reference frame.
             )rst")
        .def("localToFrame",
             py::overload_cast<const BV::Geometry::Vector &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::ReferenceFrame::localToFrame<BV::Geometry::Vector>, py::const_),
             R"rst(
             Return the position of a vector defined in local reference frame
             in the provided reference frame.

             :param pt: a :py:class:`~Geometry.Vector` which position is defined
                 in the local reference frame. 
             :param frame: a :py:class:`~Mechanics.ReferenceFrame` in which the
                 final vector should be defined.
             :return: a :py:class:`~Geometry.Vector` which position is defined
                 in the provided reference frame.
             )rst")
        .def("frameToLocal",
             py::overload_cast<const BV::Geometry::Point &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::ReferenceFrame::frameToLocal<BV::Geometry::Point>, py::const_),
             R"rst(
             Return the position of a point defined in the provided reference
             frame in local reference frame.

             :param pt: a :py:class:`~Geometry.Point` which position is defined
                 in the provided reference frame. 
             :param frame: a :py:class:`~Mechanics.ReferenceFrame` in which the
                 provided point is defined.
             :return: a :py:class:`~Geometry.Point` which position is defined
                 in the local reference frame.
             )rst")
        .def("frameToLocal",
             py::overload_cast<const BV::Geometry::Vector &, const BV::Mechanics::ReferenceFrame &>(&BV::Mechanics::ReferenceFrame::frameToLocal<BV::Geometry::Vector>, py::const_),
             R"rst(
             Return the position of a vector defined in the provided reference
             frame in local reference frame.

             :param pt: a :py:class:`~Geometry.Vector` which position is defined
                 in the provided reference frame. 
             :param frame: a :py:class:`~Mechanics.ReferenceFrame` in which the
                 provided vector is defined.
             :return: a :py:class:`~Geometry.Vector` which position is defined
                 in the local reference frame.
             )rst")
        .def("setTranslator", [](BV::Mechanics::ReferenceFrame & rf, const BV::Geometry::Translation::Cartesian & t){rf.setTranslator(BV::Geometry::TranslatorTypeEnum::CARTESIAN, t) ;},
             R"rst(
             Set the reference frame translator to the provided
             :py:class:`~Geometry.Cartesian`.

             Note that old translator will be overwritten without looking at
             its state.
             This is kind of a re-initialisation of the translator.

             Also note that the unknowns and constraints vectors will be
             updated to conform to the new translator.

             :param translator: the new :py:class:`~Geometry.Cartesian`
                 describing the reference frame translation.
             )rst")
        .def("setTranslator",
             [](BV::Mechanics::ReferenceFrame & rf, const BV::Geometry::Translation::Spherical_CoLat & t){rf.setTranslator(BV::Geometry::TranslatorTypeEnum::SPHERICAL_COLAT, t) ;},
             R"rst(Same as :py:meth:`setTranslator` with a :py:class:`~Geometry.Spherical_CoLat`.)rst")
        .def("setTranslator",
             [](BV::Mechanics::ReferenceFrame & rf, const BV::Geometry::Translation::Spherical_Lat & t){rf.setTranslator(BV::Geometry::TranslatorTypeEnum::SPHERICAL_LAT, t) ;},
             R"rst(Same as :py:meth:`setTranslator` with a :py:class:`~Geometry.Spherical_CoLat`.)rst")
        .def("setRotator", [](BV::Mechanics::ReferenceFrame & rf, const BV::Geometry::Rotation::Quaternion & r){rf.setRotator(BV::Geometry::RotatorTypeEnum::QUATERNION, r) ;},
             R"rst(
             Set the reference frame rotator to the provided
             :py:class:`~Geometry.Quaternion`.

             Note that old rotator will be overwritten without looking at
             its state.
             This is kind of a re-initialisation of the rotator.

             Also note that the unknowns and constraints vectors will be
             updated to conform to the new rotator.

             :param rotator: the new :py:class:`~Geometry.Quaternion`
                 describing the reference frame rotation.
             )rst")
        ;
    DefineRotatorSetters<BV::Geometry::Rotation::AxisAndAngle>(ref, BV::Geometry::RotatorTypeEnum::AXIS_AND_ANGLE, "AxisAndAngle") ;
    DefineRotatorSetters<BV::Geometry::Rotation::BasisVectors>(ref, BV::Geometry::RotatorTypeEnum::BASIS_VECTORS, "BasisVectors") ;
    DefineRotatorSetters<BV::Geometry::Rotation::RotationMatrix>(ref, BV::Geometry::RotatorTypeEnum::ROTATION_MATRIX, "RotationMatrix") ;
    DefineRotatorSetters<BV::Geometry::Rotation::MRP>(ref, BV::Geometry::RotatorTypeEnum::MODIFIED_RODRIGUES_PARAMETERS, "MRP") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XYX_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_i, "EulerAngles_XYX_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XYZ_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYZ_i, "EulerAngles_XYZ_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XZX_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XZX_i, "EulerAngles_XZX_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XZY_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XZY_i, "EulerAngles_XZY_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YXY_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YXY_i, "EulerAngles_YXY_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YXZ_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YXZ_i, "EulerAngles_YXZ_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YZX_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YZX_i, "EulerAngles_YZX_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YZY_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YZY_i, "EulerAngles_YZY_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZXY_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZXY_i, "EulerAngles_ZXY_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZXZ_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZXZ_i, "EulerAngles_ZXZ_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZYX_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZYX_i, "EulerAngles_ZYX_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZYZ_i>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZYZ_i, "EulerAngles_ZYZ_i") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XYX_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYX_e, "EulerAngles_XYX_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XYZ_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XYZ_e, "EulerAngles_XYZ_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XZX_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XZX_e, "EulerAngles_XZX_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_XZY_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_XZY_e, "EulerAngles_XZY_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YXY_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YXY_e, "EulerAngles_YXY_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YXZ_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YXZ_e, "EulerAngles_YXZ_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YZX_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YZX_e, "EulerAngles_YZX_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_YZY_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_YZY_e, "EulerAngles_YZY_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZXY_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZXY_e, "EulerAngles_ZXY_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZXZ_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZXZ_e, "EulerAngles_ZXZ_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZYX_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZYX_e, "EulerAngles_ZYX_e") ;
    DefineRotatorSetters<BV::Geometry::Rotation::EulerAngles_ZYZ_e>(ref, BV::Geometry::RotatorTypeEnum::EULER_ANGLES_ZYZ_e, "EulerAngles_ZYZ_e") ;

}

} // End of namespace ReferenceFrame
} // End of namespace Mechanics
} // End of namespace PythonInterface
} // End of namespace BV
