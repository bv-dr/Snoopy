.. Mechanics documentation master file, created by
   sphinx-quickstart on Wed Feb 21 16:20:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _MechanicsDocumentation:

Welcome to Mechanics's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ReferenceFrame
   Torsor



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
