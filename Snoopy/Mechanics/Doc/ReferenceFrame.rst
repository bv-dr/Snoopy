===============
Reference frame
===============

.. module:: Mechanics

.. autoclass:: ReferenceFrame
   :members:
   :inherited-members:
   :special-members: __init__

   .. rubric:: Methods

   .. autoautosummary:: Mechanics.ReferenceFrame
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Mechanics.ReferenceFrame
      :attributes:

   .. rubric:: Details

