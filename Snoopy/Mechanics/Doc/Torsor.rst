======
Torsor
======

A generic torsor has to contain attributes representing the necessary to be
fully described:

- a basis (or axis system, or set of axis)
- a reduction point (where the moment is computed)
- a resultant
- a moment

Basic functionalities of a generic torsor are the following:

- change basis
- change application point

A typical formulation for a torsor :math:`\mathcal{T}` with resultant :math:`R`
and moment :math:`M` expressed at reduction point :math:`O` in basis
:math:`\mathcal{B}` is the following:

.. math::
    \mathcal{T} = \{R, M\}_{(O,\mathcal{B})}


.. module:: Mechanics

.. autoclass:: Mechanics.StaticTorsor
   :members:
   :inherited-members:
   :special-members: __init__, __add__, __iadd__, __imul__, __isub__, __mul__, __rmul__, __sub__, __truediv__

   .. rubric:: Methods

   .. autoautosummary:: Mechanics.StaticTorsor
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Mechanics.StaticTorsor
      :attributes:

   .. rubric:: Details

.. autoclass:: Mechanics.KinematicTorsor
   :members:
   :inherited-members:
   :special-members: __init__, __add__, __iadd__, __imul__, __isub__, __mul__, __rmul__, __sub__, __truediv__

   .. rubric:: Methods

   .. autoautosummary:: Mechanics.KinematicTorsor
      :methods:
   
   .. rubric:: Attributes

   .. autoautosummary:: Mechanics.KinematicTorsor
      :attributes:

   .. rubric:: Details
