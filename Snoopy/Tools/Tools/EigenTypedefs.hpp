#pragma once

#include <Eigen/Dense>

namespace BV {
namespace Tools {

// Specific Eigen vectors
typedef Eigen::Matrix<double, 6, 1> Vector6d ;
typedef Eigen::Matrix<double, 7, 1> Vector7d ;
typedef Eigen::Matrix<double, 6, 6> Matrix6d ;
typedef Eigen::Matrix<int, 6, 1> Vector6i ;
typedef Eigen::Matrix<bool, 6, 1> Vector6b ;
typedef Eigen::Matrix<bool, 3, 1> Vector3b ;
typedef Eigen::Matrix<double, 12, 1> Vector12d ;

// Specific Eigen matrices
typedef Eigen::Matrix<double, 6, 6> Matrix6d ;
typedef Eigen::Matrix<double, 7, 7> Matrix7d ;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixRowMajor ;
typedef Eigen::Matrix<double, 12, 12> Matrix12d ;

typedef Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> MatrixXu ;
typedef Eigen::Matrix<unsigned int, Eigen::Dynamic, 3> MatrixX3u ;
typedef Eigen::Matrix<unsigned int, Eigen::Dynamic, 4> MatrixX4u ;
typedef Eigen::Matrix<double, Eigen::Dynamic, 6> MatrixX6d ;

} // End of namespace Tools
} // End of namespace BV
