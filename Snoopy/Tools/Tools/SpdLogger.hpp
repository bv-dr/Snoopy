#pragma once

#include <functional>
#include <memory>
#include <mutex>
#include <string>
#include <vector>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/base_sink.h"
#include "spdlog/details/null_mutex.h"

namespace BV {
namespace Tools {

inline std::shared_ptr<spdlog::logger> get_logger(std::string name="")
{
    std::shared_ptr<spdlog::logger> p_logger ;
    if (name.empty())
    {
        p_logger = spdlog::default_logger() ;
    }
    else
    {
        p_logger = spdlog::get(name) ;
        if (p_logger == nullptr)
        {
            // FIXME hardcoded sinks
            p_logger = spdlog::stdout_color_mt(name) ;
            p_logger->debug("New {} c++ logger created", name) ;
        }
    }
    return p_logger ;
}

inline void set_logger_level(const int &lvl, std::string name="")
{
    /*
     * lvl number cooresponds to the python logger numbers:
     * there is not equivalent to SPDLOG_LEVEL_TRACE (0)
     * logging.NOTSET    =  0        : SPDLOG_LEVEL_OFF        = 6
     * logging.DEBUG    = 10        : SPDLOG_LEVEL_DEBUG    = 1
     * logging.INFO        = 20        : SPDLOG_LEVEL_INFO        = 2
     * logging.WARN        = 30        : SPDLOG_LEVEL_WARN        = 3
     * logging.ERROR    = 40        : SPDLOG_LEVEL_ERR        = 4
     * logging.CRITICAL = 50        : SPDLOG_LEVEL_CRITICAL    = 5
     */
    std::shared_ptr<spdlog::logger> p_logger(get_logger(name)) ;
    switch (lvl)
    {
    case  0:
        p_logger->set_level(spdlog::level::level_enum::off);
        break;
    case 10:
        p_logger->set_level(spdlog::level::level_enum::debug);
        break;
    case 20:
        p_logger->set_level(spdlog::level::level_enum::info);
        break;
    case 30:
        p_logger->set_level(spdlog::level::level_enum::warn);
        break;
    case 40:
        p_logger->set_level(spdlog::level::level_enum::err);
        break;
    case 50:
        p_logger->set_level(spdlog::level::level_enum::critical);
        break;
    default:
        p_logger->set_level(spdlog::level::level_enum::info);
        break;
    }
}

namespace Details {

template<typename Mutex>
class CallbackSink : public spdlog::sinks::base_sink<Mutex>
{
private:
    std::function<void(const std::string &, const std::string &)> callback_ ;
protected:
    void sink_it_(const spdlog::details::log_msg& msg) override
    {

        // log_msg is a struct containing the log entry info like level, timestamp, thread id etc.
        // msg.raw contains pre formatted log

        // If needed (very likely but not mandatory), the sink formats the message before sending it to its final destination:
        spdlog::memory_buf_t formatted ;
        spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted) ;
        std::string color ;
        if (msg.level == spdlog::level::level_enum::critical) {color = "blue" ;}
        else if (msg.level == spdlog::level::level_enum::err) {color = "red" ;}
        else if (msg.level == spdlog::level::level_enum::warn) {color = "orange" ;}
        else if (msg.level == spdlog::level::level_enum::debug) {color = "green" ;}
        else {color = "black" ;}
        callback_(fmt::to_string(formatted), color) ;
    }

    void flush_() override
    {
    }

public:
    CallbackSink(std::function<void(const std::string &,
                                    const std::string &)> callback) :
        callback_(callback)
    {
    }

} ;

using callback_sink_mt = CallbackSink<std::mutex> ;
using callback_sink_st = CallbackSink<spdlog::details::null_mutex> ;

} // End of namespace Details

inline void add_logger_callback(std::function<void(const std::string &,
                                            const std::string &)> callback,
                         std::string name="")
{
    std::shared_ptr<spdlog::logger> p_logger(get_logger(name)) ;
    p_logger->sinks().push_back(
            std::make_shared<Details::callback_sink_mt>(callback)
                               ) ;
}

} // End of namespace Tools
} // End of namespace BV
