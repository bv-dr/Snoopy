#ifndef BV_Tools_Array_ArrayBase_hpp
#define BV_Tools_Array_ArrayBase_hpp

#include <array>
#include <algorithm>
#include <iostream>
#include <type_traits>

#include <Eigen/Dense>

#include "Tools/Array/Details.hpp"
#include "Tools/Array/Indexing.hpp"
#include "Tools/Array/Storage.hpp"

namespace BV {
namespace Tools {
namespace Array {

namespace Details {

template<bool...>
struct BoolPack ;

template<bool... bs>
using AllTrue = std::is_same<BoolPack<bs..., true>, BoolPack<true, bs...> > ;

template<class R, class... Ts>
using AreAllConvertible = AllTrue<std::is_convertible<Ts, R>::value...> ;

} // End of namespace Details

/*!
 * \class ArrayBase Tools/Array/ArrayBase.hpp
 * \brief Class providing base functionalities for the Array class.
 *
 * \tparam Derived, the derived Array class (CRTP pattern)
 *
 */
template <typename Derived>
class ArrayBase
{
public:

    /*! The derived class type*/
    typedef Derived DerivedType ;
    /*! The derived class data type*/
    typedef typename Details::Traits<Derived>::DataType DataType ;
    /*! The derived class ordering type*/
    typedef typename Details::Traits<Derived>::OrderingType OrderingType ;
    /*! \enum
     *  \brief The derived class rank and whether it is a view or not
     */
    enum {
        Rank = Details::Traits<Derived>::Rank, /*!<Rank of Derived class*/
        IsAView = Details::Traits<Derived>::IsAView /*!< Derived is a view or not*/
         } ;

    /*! The storage class type*/
    typedef Storage<DataType, Rank, IsAView, OrderingType> StorageType ;

protected:
    StorageType storage_ ; /*!<The inner storage object*/

    /*!
     * \brief Constructor giving the Array dimensions.
     * \tparam Variadic template providing the indices types.
     * \param dims, variadic arguments which provide the indices
     *     of each dimension. Note that the total number of arguments
     *     should equal the Array Rank.
     *
     * This constructor allocates the memory via the inner storage class.
     */
    template <typename ...IndexTypes,
              typename = typename std::enable_if<sizeof...(IndexTypes)==Rank
                                                 && Details::AreAllConvertible<std::size_t, IndexTypes...>::value> >
    ArrayBase(IndexTypes ...dims) :
        storage_(Shape<Rank>(dims...))
    {
        //static_assert(sizeof...(dims) == Rank,
        //              "The number of indices differ from Array rank") ;
        static_assert(IsAView == false, "Please provide data of the view") ;
    }

    ArrayBase(const Shape<Rank> & shape) :
        storage_(shape)
    {
        //static_assert(IsAView == false, "Please provide data of the view") ;
    }

    ArrayBase(const Derived & other) :
        storage_(other.storage_)
    {
    }

    template <bool View>
    ArrayBase(const Array<DataType, Rank, View, OrderingType> & other) :
        storage_(other.storage())
    {
    }

    /*!
     * \brief Constructor from other storage. Mainly for internal use.
     * \tparam N, the rank of the other storage.
     *     Deducted by the compiler.
     * \tparam View, wheter the other storage is a view or not.
     *     Deducted by the compiler.
     * \tparam Order, the type of memory ordering of the other storage.
     *     Deducted by the compiler.
     * \tparam M, the number of indices used to describe the location of
     *     the top left corner of new array in other storage.
     *     Deducted by the compiler.
     * \tparam P, the number of indices used to describe which indices in
     *     other storage correspond to the new indices.
     *     Deducted by the compiler.
     * \param otherStorage, the other storage to use for construction.
     * \param shape, the shape of the new array.
     * \param innerStridesRatios, the ratios to apply to the strides.
     * \param topLeftCornerIndices, the indices of the top left corner of
     *     new array in other storage.
     * \param outerToInnerIndices, the map of indices from other to new array.
     *
     * Note that this constructor should mainly be used internally to get
     * views of existing arrays. Users would get these using the operator()
     * provided below.
     *
     */
    template <std::size_t N, bool View, typename Order, size_t M, size_t P>
    ArrayBase(const Storage<DataType, N, View, Order> & otherStorage,
              const Shape<Rank> & shape,
              const Indices<Rank> & innerStridesRatios,
              const Indices<M> & topLeftCornerIndices,
              const Indices<P> & outerToInnerIndices) :
        storage_(otherStorage, shape, innerStridesRatios,
                 topLeftCornerIndices, outerToInnerIndices)
    {
    }

    /*!
     * \brief Virtual destructor.
     *
     * Does nothing particular.
     *
     */
    virtual ~ArrayBase(void)
    {
    }

public:
    /*!
     * \brief Returns the size of the internal memory.
     * \return the size of used memory.
     *
     * Note that it is not bytes but the number of DataType elements stored.
     *
     */
    std::size_t size(void) const
    {
        return storage_.size() ;
    }

    /*!
     * \brief Returns the shape of the array.
     * \return the shape object describing the dimensions of the array.
     */
    const Shape<Rank> & shape(void) const
    {
        return storage_.shape() ;
    }

    /*!
     * \brief Returns the strides of the array.
     * \return the strides object describing the strides of the array.
     */
    const Strides<Rank> & strides(void) const
    {
        return storage_.strides() ;
    }

    /*!
     * \brief Returns a pointer to the first element of the array.
     * \return the pointer to the array first element.
     */
    DataType * data(void)
    {
        return storage_.data() ;
    }

    /*!
     * \brief Returns a constant pointer to the first element of the array.
     * \return the constant pointer to the array first element.
     */
    const DataType * data(void) const
    {
        return storage_.data() ;
    }

    /*!
     * \brief Assignement operator.
     * \param other, the other array to copy.
     *
     * Note that the type of the other array is the same as current one.
     *
     */
    Derived & operator=(const Array<DataType, Rank, !IsAView, OrderingType> & other)
    {
        storage_ = other.storage() ;
        return static_cast<Derived &>(*this) ;
    }

    Derived & operator=(const Derived & other)
    {
        if (this != &other)
        {
            storage_ = other.storage() ;
        }
        return static_cast<Derived &>(*this) ;
    }

    bool isRowMajor(void) const
    {
        return storage_.isRowMajor() ;
    }

    const StorageType & storage(void) const
    {
        return storage_ ;
    }

    template <typename ...IndexTypes>
    auto operator()(IndexTypes ...inds) const ->
        typename Details::ReturnTypeEvaluator<Array, DataType, Rank, true,
                                              OrderingType, void,
                                              IndexTypes...>::type
    {
        typedef typename Details::ReturnTypeEvaluator<
                  Array, DataType, Rank, true, OrderingType, void, IndexTypes...
                                                     >::type ReturnType ;
        constexpr std::size_t newRank(Details::Traits<ReturnType>::Rank) ;
        Indices<Rank> indices(Details::GetStartIndices(inds)...) ;
        Shape<newRank> newShape ;
        newShape.fill(0) ;
        Indices<newRank> ratios ;
        ratios.fill(1) ;
        Indices<Rank> outerToInnerIndices ;
        Details::ShapeEvaluator<0, 0, void, IndexTypes...>::set(newShape,
                                                                ratios,
                                                                outerToInnerIndices,
                                                                shape(),
                                                                inds...) ;
        return ReturnType(storage_, newShape, ratios,
                          indices, outerToInnerIndices) ;
    }

} ;

} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_ArrayBase_hpp
