#ifndef BV_Tools_Array_Details_hpp
#define BV_Tools_Array_Details_hpp

#include <array>
#include <algorithm>
#include <iostream>
#include <utility>

#include <Eigen/Dense>

#include "Tools/Array/Indexing.hpp"
#include "Tools/Array/Slicing.hpp"

namespace BV {
namespace Tools {
namespace Array {

/*!
 * \brief Forward declaration of the Array class.
 */
template <typename DataType_, std::size_t Rank_, bool IsAView_,
          typename OrderingType_>
class Array ;

namespace Details {

/*!
 * \class Traits Tools/Array/ArrayBase.hpp BV::Tools::Array::Details::Traits
 * \brief Utility class providing typedefs to the Array class types and enums.
 *
 */
template <typename Derived>
struct Traits ;

template <typename DataType_, std::size_t Rank_, bool IsAView_,
          typename OrderingType_>
struct Traits<Array<DataType_, Rank_, IsAView_, OrderingType_> >
{
    typedef DataType_ DataType ;
    typedef OrderingType_ OrderingType ;
    typedef Array<DataType_, Rank_, IsAView_, OrderingType_> DerivedType ;
    enum { Rank = Rank_, IsAView = IsAView_ } ;
} ;

template <typename T>
inline std::size_t GetStartIndices(const T & ind)
{
    return static_cast<std::size_t>(ind) ;
}
    
template <>
inline std::size_t GetStartIndices<Slice>(const Slice & slice)
{
    return static_cast<std::size_t>(slice.startIndex) ;
}

template <typename T>
using GetType = std::remove_cv<typename std::remove_reference<T>::type> ;

template <typename T>
using CheckIntegral = std::is_integral<typename GetType<T>::type> ;

template <typename T>
using CheckSlice = std::is_same<typename GetType<T>::type, Slice> ;

template <template<typename, std::size_t, bool, typename> class ArrayClass,
          typename DataType, std::size_t Rank, bool IsAView,
          typename OrderingType, class Enable=void, typename ...IndexTypes>
struct ReturnTypeEvaluator ;

template <template<typename, std::size_t, bool, typename> class ArrayClass,
          typename DataType, std::size_t Rank, bool IsAView,
          typename OrderingType, typename T>
struct ReturnTypeEvaluator<ArrayClass, DataType, Rank, IsAView,
                           OrderingType,
                           typename std::enable_if<CheckIntegral<T>::value>::type,
                           T>
{
    typedef ArrayClass<DataType, Rank-1, IsAView, OrderingType> type ;
} ;

template <template<typename, std::size_t, bool, typename> class ArrayClass,
          typename DataType, bool IsAView, typename OrderingType,
          typename T>
struct ReturnTypeEvaluator<ArrayClass, DataType, 0, IsAView,
                           OrderingType,
                           typename std::enable_if<CheckIntegral<T>::value>::type,
                           T>
{
    typedef DataType & type ;
} ;

template <template<typename, std::size_t, bool, typename> class ArrayClass,
          typename DataType, std::size_t Rank, bool IsAView,
          typename OrderingType, typename T>
struct ReturnTypeEvaluator<ArrayClass, DataType, Rank, IsAView,
                           OrderingType,
                           typename std::enable_if<CheckSlice<T>::value>::type,
                           T>
{
    typedef ArrayClass<DataType, Rank, IsAView, OrderingType> type ;
} ;

template <template<typename, std::size_t, bool, typename> class ArrayClass,
          typename DataType, std::size_t Rank, bool IsAView,
          typename OrderingType, typename T, typename ...IndexTypes>
struct ReturnTypeEvaluator<ArrayClass, DataType, Rank, IsAView, OrderingType,
                           typename std::enable_if<CheckSlice<T>::value>::type,
                           T, IndexTypes...>
{
    typedef typename ReturnTypeEvaluator<ArrayClass, DataType, Rank, IsAView,
                                         OrderingType, void, IndexTypes...>::type type ;
} ;

template <template<typename, std::size_t, bool, typename> class ArrayClass,
          typename DataType, std::size_t Rank, bool IsAView,
          typename OrderingType, typename T, typename ...IndexTypes>
struct ReturnTypeEvaluator<ArrayClass, DataType, Rank, IsAView, OrderingType,
                           typename std::enable_if<CheckIntegral<T>::value>::type,
                           T, IndexTypes...>
{
    typedef typename ReturnTypeEvaluator<ArrayClass, DataType, Rank-1, IsAView,
                                         OrderingType, void, IndexTypes...>::type type ;
} ;

template <std::size_t IPrevious, std::size_t INew, class Enable=void,
          typename T=int, typename ...IndexTypes>
struct ShapeEvaluator ;

template <std::size_t IPrevious, std::size_t INew, typename T>
struct ShapeEvaluator<IPrevious, INew,
                      typename std::enable_if<CheckIntegral<T>::value>::type,
                      T>
{
    template <std::size_t N, std::size_t M>
    static void set(Shape<N> & shape,
                    Indices<N> & ratios,
                    Indices<M> & outerToInnerIndices,
                    const Shape<M> & previousShape,
                    T ind)
    {
        outerToInnerIndices[IPrevious] = -1 ;
        for (std::size_t i=IPrevious+1, j=INew; i<M; ++i, ++j)
        {
            outerToInnerIndices[i] = j ;
            if (N > 0)
            {
                shape[j] = previousShape[i] ;
            }
        }
    }
} ;

template <std::size_t IPrevious, std::size_t INew, typename T>
struct ShapeEvaluator<IPrevious, INew,
                      typename std::enable_if<CheckSlice<T>::value>::type,
                      T>
{
    template <std::size_t N, std::size_t M>
    static void set(Shape<N> & shape,
                    Indices<N> & ratios,
                    Indices<M> & outerToInnerIndices,
                    const Shape<M> & previousShape,
                    T slice)
    {
        outerToInnerIndices[IPrevious] = INew ;
        shape[INew] = slice.getSize(previousShape[IPrevious]) ;
        ratios[INew] = slice.step ;
        for (std::size_t i=IPrevious+1, j=INew+1; i<M; ++i, ++j)
        {
            outerToInnerIndices[i] = j ;
            shape[j] = previousShape[i] ;
        }
    }
} ;

template <std::size_t IPrevious, std::size_t INew, typename T,
          typename ...IndexTypes>
struct ShapeEvaluator<IPrevious, INew,
                      typename std::enable_if<CheckIntegral<T>::value>::type,
                      T, IndexTypes...>
{
    template <std::size_t N, std::size_t M>
    static void set(Shape<N> & shape,
                    Indices<N> & ratios,
                    Indices<M> & outerToInnerIndices,
                    const Shape<M> & previousShape,
                    T ind, IndexTypes...inds)
    {
        outerToInnerIndices[IPrevious] = -1 ;
        ShapeEvaluator<IPrevious+1, INew, void, IndexTypes...>::set(shape, ratios,
                                                                    outerToInnerIndices,
                                                                    previousShape,
                                                                    inds...) ;
    }
} ;

template <std::size_t IPrevious, std::size_t INew, typename T,
          typename ...IndexTypes>
struct ShapeEvaluator<IPrevious, INew,
                      typename std::enable_if<CheckSlice<T>::value>::type,
                      T, IndexTypes...>
{
    template <std::size_t N, std::size_t M>
    static void set(Shape<N> & shape,
                    Indices<N> & ratios,
                    Indices<M> & outerToInnerIndices,
                    const Shape<M> & previousShape,
                    T slice, IndexTypes...inds)
    {
        outerToInnerIndices[IPrevious] = INew ;
        shape[INew] = slice.getSize(previousShape[IPrevious]) ;
        ratios[INew] = slice.step ;
        ShapeEvaluator<IPrevious+1, INew+1, void, IndexTypes...>::set(shape, ratios,
                                                                      outerToInnerIndices,
                                                                      previousShape,
                                                                      inds...) ;
    }
} ;

} // End of namespace Details

} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Details_hpp
