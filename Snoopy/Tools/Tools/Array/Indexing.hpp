#ifndef BV_Tools_Array_Indexing_hpp
#define BV_Tools_Array_Indexing_hpp

#include <array>
#include <algorithm>
#include <string>
#include <memory>
#include <vector>

#include <Eigen/Dense>

#include "Tools/BVException.hpp"

namespace BV {
namespace Tools {
namespace Array {

/*!
 * \brief Exception class throwing if a wrong index is provided
 */
struct WrongIndexException : public BV::Tools::Exceptions::BVException
{
    WrongIndexException(std::string message) : BVException(message)
    {
    }
} ;

template <std::size_t N, typename IndexType_=int>
class Indices
{
protected:
    std::array<IndexType_, N> indices_ ;
public:

    typedef IndexType_ IndexType ;
    enum {Size = N} ;

    Indices(void)
    {
        indices_.fill(0) ;
    }

    template <typename ...SizeTypes>
    Indices(const IndexType & ind, SizeTypes...sizes) :
        indices_({{ind, static_cast<IndexType_>(sizes)...}})
    {
        std::size_t nInds(sizeof...(SizeTypes)+1) ;
        // Fill with zeros the other indices
        for (std::size_t i=nInds; i<N; ++i)
        {
            indices_[i] = 0 ;
        }
    }

    Indices(const Indices<N> & other) :
        indices_(other.indices_)
    {
    }

    void fill(const IndexType & size)
    {
        indices_.fill(size) ;
    }

    template <typename T>
    IndexType_ & operator()(T ind)
    {
        if (static_cast<std::size_t>(ind) >= N)
        {
            throw WrongIndexException(std::to_string(ind) + " out of bounds") ;
        }
        return indices_[ind] ;
    }

    template <typename T>
    const IndexType_ & operator()(T ind) const
    {
        if (static_cast<std::size_t>(ind) >= N)
        {
            throw WrongIndexException(std::to_string(ind) + " out of bounds") ;
        }
        return indices_[ind] ;
    }

    template <typename T>
    IndexType_ & operator[](T ind)
    {
        return this->operator()(ind) ;
    }

    template <typename T>
    const IndexType_ & operator[](T ind) const
    {
        return this->operator()(ind) ;
    }

    std::size_t size(void) const
    {
        return N ;
    }

    bool isAllOfValue(IndexType_ value) const
    {
        for (const IndexType_ & ind : indices_)
        {
            if (ind != value)
            {
                return false ;
            }
        }
        return true ;
    }

    bool operator==(const Indices<N> & other) const
    {
        for (std::size_t i=0; i<N; ++i)
        {
            if (this->operator()(i) != other(i))
            {
                return false ;
            }
        }
        return true ;
    }

    bool operator!=(const Indices<N> & other) const
    {
        return !(this->operator==(other)) ;
    }

    const std::array<IndexType_, N> & get(void) const
    {
        return indices_ ;
    }
} ;

template <std::size_t N>
inline std::ostream & operator<<(std::ostream & f, const Indices<N> & indices)
{
    f << "(" ;
    for (std::size_t i=0; i<N; ++i)
    {
        f << indices[i] << " " ;
    }
    f << ")" << std::endl ;
    return f ;
}

template <std::size_t Rank>
class Shape : public Indices<Rank>
{
public:
    typedef typename Indices<Rank>::IndexType IndexType ;

    Shape(void) : Indices<Rank>()
    {
    }

    template <typename ...SizeTypes>
    Shape(SizeTypes...sizes) : Indices<Rank>(sizes...)
    {
    }

    Shape(const IndexType & value) : Indices<Rank>(value)
    {
    }

    std::size_t storageSize(void) const
    {
        std::size_t size(1) ;
        for (std::size_t i=0; i<Rank; ++i)
        {
            size *= static_cast<std::size_t>(this->operator()(i)) ;
        }
        return size ;
    }

} ;

template <std::size_t Rank>
class Strides : public Indices<Rank>
{
public:
    typedef typename Indices<Rank>::IndexType IndexType ;

    template <typename ...SizeTypes>
    Strides(SizeTypes...sizes) : Indices<Rank>(sizes...)
    {
    }

    Strides(const IndexType & value) : Indices<Rank>(value)
    {
    }

    IndexType productToInd(std::size_t ind, const Indices<Rank> & ratios) const
    {
        IndexType res(1) ;
        for (std::size_t i=0; i<=ind; ++i)
        {
            res *= this->operator()(i) * ratios[i] ;
        }
        return res ;
    }

    IndexType productFromInd(std::size_t ind, const Indices<Rank> & ratios) const
    {
        IndexType res(1) ;
        for (std::size_t i=ind; i<Rank; ++i)
        {
            res *= this->operator()(i) * ratios[i] ;
        }
        return res ;
    }

} ;

template <int Order=Eigen::ColMajor>
struct Ordering
{
    static const int type=Eigen::ColMajor ;
    static constexpr int IsColMajor = 1 ;
    static constexpr int IsRowMajor = 0 ;

    static constexpr bool isRowMajor(void)
    {
        return false ;
    }
} ;

template <>
struct Ordering<Eigen::RowMajor>
{
    static const int type=Eigen::RowMajor ;
    static constexpr int IsColMajor = 0 ;
    static constexpr int IsRowMajor = 1 ;

    static constexpr bool isRowMajor(void)
    {
        return true ;
    }
} ;

template <typename OrderingType>
struct StridesGetter ;

template <>
struct StridesGetter<Ordering<Eigen::RowMajor> >
{
    template <std::size_t N>
    static Strides<N> get(const Shape<N> & shape)
    {
        Strides<N> strides ;
        for (std::size_t i=1; i<N; ++i)
        {
            strides[i-1] = shape[i] ;
        }
        strides[N-1] = 1 ;
        return strides ;
    }

    template <std::size_t N>
    static bool isAligned(const Shape<N> & shape, const Strides<N> & strides)
    {
        if (get(shape) == strides)
        {
            return true ;
        }
        return false ;
    }

    template <std::size_t L, std::size_t M>
    static Strides<L> get(const Shape<L> & shape,
                          const Indices<M> & outerStrides,
                          const Indices<M> & outerToInnerIndices)
    {
        // FIXME here we assume outer strides have the same ordering !
        // FIXME I think this code can be better implemented...
        Strides<L> strides ;
        int iStrides(-1) ;
        for (std::size_t ind=0; ind<M; ++ind)
        {
            if (outerToInnerIndices[ind] == -1)
            {
                if (iStrides < 0)
                {
                    continue ;
                }
                strides[iStrides] *= outerStrides[ind] ;
            }
            else if (iStrides < static_cast<int>(M-1))
            {
                ++iStrides ;
                strides[iStrides] = outerStrides[ind] ;
            }
        }
        return strides ;
    }
} ;

template <>
struct StridesGetter<Ordering<Eigen::ColMajor> >
{
    template <std::size_t N>
    static Strides<N> get(const Shape<N> & shape)
    {
        Strides<N> strides ;
        strides[0] = 1 ;
        for (std::size_t i=0; i<N-1; ++i)
        {
            strides[i+1] = shape[i] ;
        }
        return strides ;
    }

    template <std::size_t N>
    static bool isAligned(const Shape<N> & shape, const Strides<N> & strides)
    {
        if (get(shape) == strides)
        {
            return true ;
        }
        return false ;
    }

    template <std::size_t L, std::size_t M>
    static Strides<L> get(const Shape<L> & shape,
                          const Indices<M> & outerStrides,
                          const Indices<M> & outerToInnerIndices)
    {
        Strides<L> strides ;
        // FIXME here we assume outer strides have the same ordering !
        int iStrides(L) ;
        for (int ind=static_cast<int>(M-1); ind>-1; --ind)
        {
            if (outerToInnerIndices[ind] == -1)
            {
                if (iStrides > static_cast<int>(L-1))
                {
                    continue ;
                }
                strides[iStrides] *= outerStrides[ind] ;
            }
            else if (iStrides > 0)
            {
                --iStrides ;
                strides[iStrides] = outerStrides[ind] ;
            }
        }
        return strides ;
    }
} ;

template <std::size_t N>
struct IndexIteratorBase
{
protected:
    Shape<N> shape_ ;
    std::size_t size_ ;
    typedef std::vector<Indices<N> > StorageType ;
    std::shared_ptr<StorageType> p_indices_ ;

public:

    IndexIteratorBase(const Shape<N> & shape) :
        shape_(shape), size_(shape.storageSize())
    {
        p_indices_ = std::make_shared<StorageType>(size_) ;
    }

    typedef typename StorageType::iterator iterator ;
    typedef typename StorageType::const_iterator const_iterator ;

    //virtual ~IndexIteratorBase(void)
    //{
    //    delete [] p_indices_ ;
    //}

    //typedef std::size_t size_type ;

    //class iterator
    //{
    //public:
    //    typedef iterator self_type ;
    //    typedef Indices<N> value_type ;
    //    typedef Indices<N> & reference ;
    //    typedef Indices<N> * pointer ;
    //    typedef std::forward_iterator_tag iterator_category ;
    //    typedef std::size_t difference_type ;
    //    iterator(pointer ptr) : ptr_(ptr) {}
    //    self_type operator++(void) {self_type i = *this ; ptr_++ ; return i ; }
    //    self_type operator++(int dummy) {ptr_++ ; return *this ; }
    //    reference operator*() {return *ptr_ ;}
    //    pointer operator->() {return ptr_ ;}
    //    bool operator==(const self_type & other) { return ptr_ == other.ptr_ ;}
    //    bool operator!=(const self_type & other) { return ptr_ != other.ptr_ ;}
    //private:
    //    pointer ptr_ ;
    //} ;

    //class const_iterator
    //{
    //public:
    //    typedef const_iterator self_type ;
    //    typedef Indices<N> value_type ;
    //    typedef Indices<N> & reference ;
    //    typedef Indices<N> * pointer ;
    //    typedef std::forward_iterator_tag iterator_category ;
    //    typedef std::size_t difference_type ;
    //    const_iterator(pointer ptr) : ptr_(ptr) {}
    //    self_type operator++(void) {self_type i = *this ; ptr_++ ; return i ; }
    //    self_type operator++(int dummy) {ptr_++ ; return *this ; }
    //    const reference operator*() {return *ptr_ ;}
    //    const pointer operator->() {return ptr_ ;}
    //    bool operator==(const self_type & other) { return ptr_ == other.ptr_ ;}
    //    bool operator!=(const self_type & other) { return ptr_ != other.ptr_ ;}
    //private:
    //    pointer ptr_ ;
    //} ;

    iterator begin(void)
    {
        return p_indices_->begin() ; //iterator(p_indices_) ;
    }

    iterator end(void)
    {
        return p_indices_->end() ; //iterator(p_indices_ + size_) ;
    }

    const_iterator begin(void) const
    {
        return p_indices_->begin() ; //iterator(p_indices_) ;
    }

    const_iterator end(void) const
    {
        return p_indices_->end() ; //iterator(p_indices_ + size_) ;
    }

} ;

template <typename OrderingType, std::size_t N>
struct IndexIterator ;

template <std::size_t N>
struct IndexIterator<Ordering<Eigen::RowMajor>, N> : public IndexIteratorBase<N>
{
private:

    bool iterate_(Indices<N> & indices, int currentIndex)
    {
        if (currentIndex == -1)
        {
            return false ;
        }
        if (indices(currentIndex) < (this->shape_(currentIndex)-1))
        {
            ++indices(currentIndex) ;
            return true ;
        }
        indices(currentIndex) = 0 ;
        return iterate_(indices, currentIndex-1) ;
    }

public:
    IndexIterator(const Shape<N> & shape) : IndexIteratorBase<N>(shape)
    {
        Indices<N> inds ;
        inds.fill(0) ;
        inds[N-1] = -1 ;
        std::size_t count(0) ;
        while (iterate_(inds, N-1))
        {
            this->p_indices_->at(count) = inds ;
            ++count ;
        }
    }
} ;

template <std::size_t N>
struct IndexIterator<Ordering<Eigen::ColMajor>, N> : public IndexIteratorBase<N>
{
private:
    bool iterate_(Indices<N> & indices, int currentIndex)
    {
        if (currentIndex == N)
        {
            return false ;
        }
        if (indices(currentIndex) < (this->shape_(currentIndex)-1))
        {
            ++indices(currentIndex) ;
            return true ;
        }
        indices(currentIndex) = 0 ;
        return iterate_(indices, currentIndex+1) ;
    }

public:
    IndexIterator(const Shape<N> & shape) : IndexIteratorBase<N>(shape)
    {
        Indices<N> inds ;
        inds.fill(0) ;
        inds[0] = -1 ;
        std::size_t count(0) ;
        while (iterate_(inds, 0))
        {
            this->p_indices_->at(count) = inds ;
            ++count ;
        }
    }
} ;

template <typename=Ordering<Eigen::ColMajor> >
struct Indexer
{
    template <std::size_t N>
    static typename Indices<N>::IndexType get(const Indices<N> & indices,
                                              const Strides<N> & strides,
                                              const Indices<N> & innerStridesRatios)
    {
        typename Indices<N>::IndexType res(0) ;
        for (std::size_t k=0; k<N; ++k)
        {
            res += strides.productToInd(k, innerStridesRatios) * indices[k] ;
        }
        return res ;
    }

} ;

template <>
struct Indexer<Ordering<Eigen::RowMajor> >
{
    template <std::size_t N>
    static typename Indices<N>::IndexType get(const Indices<N> & indices,
                                              const Strides<N> & strides,
                                              const Indices<N> & innerStridesRatios)
    {
        typename Indices<N>::IndexType res(0) ;
        for (std::size_t k=0; k<N; ++k)
        {
            res += strides.productFromInd(k, innerStridesRatios) * indices[k] ;
        }
        return res ;
    }

} ;

//template <typename Ordering>
//struct Converter ;
//
//template <>
//struct Converter<Ordering<Eigen::ColMajor> >
//{
//    template <typename DataType, typename L, typename M, typename N>
//    static void convert(const Shape<L> & shape,
//                        const Strides<L> & strides,
//                        const Indices<M> & topLeftCornerIndices,
//                        const Indices<N> & outerToInnerIndices,
//                        const DataType * originalData,
//                        const std::size_t & size,
//                        DataType * convertedData,
//                        bool isOriginalRowMajor)
//    {
//        bool isAligned(IsAligned(shape, strides)) ;
//        if ((!isOriginalRowMajor) && isAligned)
//        {
//            // Nothing to do, just copy the data in provided array
//            std::copy(originalData, originalData+size, convertedData) ;
//            return ;
//        }
//        constexpr std::size_t N(shape.size()) ;
//        std::array<int, N> indices ;
//        std::array<std::size_t, N> topLeft0 ;
//        std::array<int, N> outToIn0 ;
//        for (std::size_t i=0; i<N; ++i)
//        {
//            indices[i] = 0 ;
//            topLeft0[i] = 0 ;
//            outToIn0[i] = i ;
//        }
//        indices[N-1] = -1 ;
//        while (IterateOnIndices(indices, shape, static_cast<int>(N)-1))
//        {
//            std::size_t iCol(Indexer<Ordering<Eigen::ColMajor> >::get(
//                                                          indices, strides,
//                                                          topLeftCornerIndices,
//                                                          outerToInnerIndices
//                                                                     )) ;
//            std::size_t iColNew(iCol) ;
//            if (!isAligned)
//            {
//                iColNew = Indexer<Ordering<Eigen::ColMajor> >::get(
//                                              indices, shape, topLeft0, outToIn0
//                                                                  ) ;
//            }
//            if (isOriginalRowMajor)
//            {
//                // Conversion from row to col major
//                std::size_t iRow(Indexer<Ordering<Eigen::RowMajor> >::get(
//                                                          indices, strides,
//                                                          topLeftCornerIndices,
//                                                          outerToInnerIndices
//                                                                         )) ;
//                convertedData[iColNew] = originalData[iRow] ;
//            }
//            else
//            {
//                convertedData[iColNew] = originalData[iCol] ;
//            }
//        }
//    }
//} ;
//
//template <>
//struct Converter<Ordering<Eigen::RowMajor> >
//{
//    template <typename DataType, typename U, typename V, typename W, typename X>
//    static void convert(const U & shape,
//                        const V & strides,
//                        const W & topLeftCornerIndices,
//                        const X & outerToInnerIndices,
//                        const DataType * originalData,
//                        const std::size_t & size,
//                        DataType * convertedData,
//                        bool isOriginalRowMajor)
//    {
//        bool isAligned(IsAligned(shape, strides)) ;
//        if (isOriginalRowMajor && isAligned)
//        {
//            // Nothing to do, just copy the data in provided array
//            std::copy(originalData, originalData+size, convertedData) ;
//            return ;
//        }
//        constexpr std::size_t N(shape.size()) ;
//        std::array<int, N> indices ;
//        std::array<std::size_t, N> topLeft0 ;
//        std::array<int, N> outToIn0 ;
//        for (std::size_t i=0; i<N; ++i)
//        {
//            indices[i] = 0 ;
//            topLeft0[i] = 0 ;
//            outToIn0[i] = i ;
//        }
//        indices[N-1] = -1 ;
//        while (IterateOnIndices(indices, shape, static_cast<int>(N)-1))
//        {
//            std::size_t iRow(Indexer<Ordering<Eigen::RowMajor> >::get(
//                                                          indices, strides,
//                                                          topLeftCornerIndices,
//                                                          outerToInnerIndices
//                                                                     )) ;
//            std::size_t iRowNew(iRow) ;
//            if (!isAligned)
//            {
//                iRowNew = Indexer<Ordering<Eigen::RowMajor> >::get(
//                                              indices, shape, topLeft0, outToIn0
//                                                                  ) ;
//            }
//            if (!isOriginalRowMajor)
//            {
//                // Conversion from col to row major
//                std::size_t iCol(Indexer<Ordering<Eigen::ColMajor> >::get(
//                                                          indices, strides,
//                                                          topLeftCornerIndices,
//                                                          outerToInnerIndices
//                                                                         )) ;
//                convertedData[iRowNew] = originalData[iCol] ;
//            }
//            else
//            {
//                convertedData[iRowNew] = originalData[iRow] ;
//            }
//        }
//    }
//} ;

} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Indexing_hpp
