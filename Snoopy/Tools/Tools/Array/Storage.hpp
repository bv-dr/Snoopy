#ifndef BV_Tools_Array_Storage_hpp
#define BV_Tools_Array_Storage_hpp

#include <Eigen/Dense>
#include <array>
#include <memory>
#include <vector>
#include <algorithm>
#include <functional>

#include "Tools/BVException.hpp"
#include "Tools/Array/Indexing.hpp"

namespace BV {
namespace Tools {
namespace Array {

struct OutOfBoundsException : public BV::Tools::Exceptions::BVException
{
    OutOfBoundsException(std::string message) : BVException(message)
    {
    }
} ;

struct IncorrectDataAssignmentException :
    public BV::Tools::Exceptions::BVException
{
    IncorrectDataAssignmentException(std::string message) : BVException(message)
    {
    }
} ;

template <typename DataType, std::size_t Rank, bool IsAView,
          typename OrderingType>
class Storage ;

template <typename DataType, std::size_t Rank, typename OrderingType>
class Storage<DataType, Rank, false, OrderingType>
{
private:
    typedef std::vector<DataType> DataStorageType ;
    typedef std::shared_ptr<std::vector<DataType> > p_DataStorageType ;

    Shape<Rank> shape_ ;
    std::size_t size_ ;
    Strides<Rank> strides_ ;
    p_DataStorageType p_data_ ;
    Indices<Rank> innerStridesRatios_ ;

    typedef Storage<DataType, Rank, false, OrderingType> ThisType ;

public:

    Storage(const Shape<Rank> & shape) :
        shape_(shape), size_(shape.storageSize()),
        strides_(StridesGetter<OrderingType>::get(shape)),
        p_data_(std::make_shared<DataStorageType>(size_))
    {
        innerStridesRatios_.fill(1) ;
    }

    Storage(const ThisType & other) :
        shape_(other.shape()), size_(other.size()),
        strides_(other.strides()),
        p_data_(std::make_shared<DataStorageType>(*(other.getDataStorage())))
    {
        innerStridesRatios_.fill(1) ;
    }

    // TODO implement the change in ordering type !

    Storage(const Storage<DataType, Rank, true, OrderingType> & other) :
        shape_(other.shape()), size_(other.size()),
        strides_(StridesGetter<OrderingType>::get(shape_)),
        p_data_(std::make_shared<DataStorageType>(other.begin(), other.end()))
    {
        innerStridesRatios_.fill(1) ;
        //auto itThis(begin()) ;
        //auto itThisEnd(end()) ;
        //auto itOther(other.begin()) ;
        //while (itThis != itThisEnd)
        //{
        //    *itThis = *itOther ;
        //    ++itThis ;
        //    ++itOther ;
        //}
    }

    template <std::size_t N, bool View, std::size_t L, std::size_t M>
    Storage(const Storage<DataType, N, View, OrderingType> & other,
            const Shape<L> & shape,
            const Indices<L> & innerStridesRatios,
            const Indices<M> & topLeftCornerIndices,
            const Indices<N> & outerToInnerIndices) :
        shape_(shape), size_(shape.storageSize()),
        strides_(StridesGetter<OrderingType>::get(shape)),
        p_data_(std::make_shared<std::vector<DataType> >(size_))
    {
        innerStridesRatios_.fill(1) ;
        // Now we have to copy the data in p_data_
        Strides<Rank> strides = StridesGetter<OrderingType>::get(
                                                          shape,
                                                          other.strides(),
                                                          outerToInnerIndices
                                                                ) ;
        Indices<N> ratios(other.innerStridesRatios()) ;
        typename Indices<Rank>::IndexType startInd(
                          Indexer<OrderingType>::get(topLeftCornerIndices,
                                                     other.strides(),
                                                     ratios)
                                                  ) ;
        typename Indices<Rank>::IndexType iNew(0), iOther ;
        IndexIterator<OrderingType, Rank> iterator(shape_) ;
        p_DataStorageType otherData(other.getDataStorage()) ;
        for (const Indices<Rank> & ind : iterator)
        {
            iOther = startInd+other.getIStart()
                        +Indexer<OrderingType>::get(ind, strides,
                                                    innerStridesRatios) ;
            (*p_data_)[iNew] = (*otherData)[iOther] ;
            ++iNew ;
        }
    }
              
    ~Storage(void)
    {
    }

    DataType * data(void)
    {
        return p_data_->data() ;
    }

    const DataType * data(void) const
    {
        return p_data_->data() ;
    }

    p_DataStorageType getDataStorage(void) const
    {
        return p_data_ ;
    }

    typename Indices<Rank>::IndexType getIStart(void) const
    {
        return 0 ;
    }

    std::size_t size(void) const
    {
        return size_ ;
    }

    const Strides<Rank> & strides(void) const
    {
        return strides_ ;
    }

    const Shape<Rank> & shape(void) const
    {
        return shape_ ;
    }

    const Indices<Rank> & innerStridesRatios(void) const
    {
        return innerStridesRatios_ ;
    }

    bool isAligned(void) const
    {
        return true ;
    }

    typename Indices<Rank>::IndexType getIndexInData(
                                     const Indices<Rank> & indices
                                                    ) const
    {
        return Indexer<OrderingType>::get(indices, strides_,
                                          innerStridesRatios_) ;
    }

    typename DataStorageType::iterator getIteratorToData(const Indices<Rank> & indices)
    {
        typename Indices<Rank>::IndexType ind(getIndexInData(indices)) ;
        if (static_cast<std::size_t>(ind) >= size_)
        {
            throw OutOfBoundsException("Index is out of storage bounds") ;
        }
        return p_data_->begin() + ind ;
    }

    typename DataStorageType::const_iterator getIteratorToData(const Indices<Rank> & indices) const
    {
        typename Indices<Rank>::IndexType ind(getIndexInData(indices)) ;
        if (static_cast<std::size_t>(ind) >= size_)
        {
            throw OutOfBoundsException("Index is out of storage bounds") ;
        }
        return p_data_->begin() + ind ;
    }

    bool isRowMajor(void) const
    {
        return OrderingType::isRowMajor() ;
    }

    ThisType & operator=(const ThisType & other)
    {
        if (shape_ != other.shape())
        {
            throw IncorrectDataAssignmentException(
                            "Impossible to set values, shapes differ"
                                                  ) ;
        }
        //strides_ = other.strides_ ; // FIXME
        *p_data_ = *(other.getDataStorage()) ;
        return *this ;
    }

    ThisType & operator=(const Storage<DataType, Rank, true, OrderingType> & other)
    {
        if (shape_ != other.shape())
        {
            throw IncorrectDataAssignmentException(
                            "Impossible to set values, shapes differ"
                                                  ) ;
        }
        //strides_ = other.strides_ ; // FIXME
        auto itThis(begin()) ;
        auto itThisEnd(end()) ;
        auto itOther(other.begin()) ;
        while (itThis != itThisEnd)
        {
            *itThis = *itOther ;
            ++itThis ;
            ++itOther ;
        }
        return *this ;
    }

    typename DataStorageType::iterator begin(void)
    {
        return p_data_->begin() ;
    }

    typename DataStorageType::iterator end(void)
    {
        return p_data_->end() ;
    }

    typename DataStorageType::const_iterator begin(void) const
    {
        return p_data_->begin() ;
    }

    typename DataStorageType::const_iterator end(void) const
    {
        return p_data_->end() ;
    }

} ;

template <typename DataType, std::size_t Rank, typename OrderingType>
class Storage<DataType, Rank, true, OrderingType>
{
private:
    typedef std::vector<DataType> DataStorageType ;
    typedef std::shared_ptr<std::vector<DataType> > p_DataStorageType ;

    Shape<Rank> shape_ ;
    std::size_t size_ ;
    Strides<Rank> strides_ ;
    p_DataStorageType p_data_ ;
    typename Indices<Rank>::IndexType iStart_ ;
    Indices<Rank> innerStridesRatios_ ;
    bool isRowMajor_ ;

    typedef Storage<DataType, Rank, true, OrderingType> ThisType ;

public:

    Storage(const ThisType & other) :
        shape_(other.shape()),
        size_(other.size()),
        strides_(other.strides()),
        p_data_(other.getDataStorage()),
        iStart_(other.getIStart()),
        innerStridesRatios_(other.innerStridesRatios()),
        isRowMajor_(other.isRowMajor())
    {
    }

    Storage(const Storage<DataType, Rank, false, OrderingType> & other) :
        shape_(other.shape()),
        size_(other.size()),
        strides_(other.strides()),
        p_data_(other.getDataStorage()),
        iStart_(other.getIStart()),
        innerStridesRatios_(other.innerStridesRatios()),
        isRowMajor_(other.isRowMajor())
    {
    }

    // TODO implement the change in ordering type !

    template <std::size_t N, bool View, std::size_t L, std::size_t M>
    Storage(const Storage<DataType, N, View, OrderingType> & other,
            const Shape<L> & shape,
            const Indices<L> & innerStridesRatios,
            const Indices<M> & topLeftCornerIndices,
            const Indices<N> & outerToInnerIndices) :
        shape_(shape), size_(shape.storageSize()),
        strides_(StridesGetter<OrderingType>::get(shape,
                                                  other.strides(),
                                                  outerToInnerIndices)),
        p_data_(other.getDataStorage()),
        iStart_(other.getIndexInData(topLeftCornerIndices)+other.getIStart()),
        innerStridesRatios_(innerStridesRatios),
        isRowMajor_(other.isRowMajor())
    {
    }

    ~Storage(void)
    {
    }

    DataType * data(void)
    {
        return p_data_->data() + iStart_ ;
    }

    const DataType * data(void) const
    {
        return p_data_->data() + iStart_ ;
    }

    p_DataStorageType getDataStorage(void) const
    {
        return p_data_ ;
    }

    typename Indices<Rank>::IndexType getIStart(void) const
    {
        return iStart_ ;
    }

    std::size_t size(void) const
    {
        return size_ ;
    }

    const Strides<Rank> & strides(void) const
    {
        return strides_ ;
    }

    const Shape<Rank> & shape(void) const
    {
        return shape_ ;
    }

    const Indices<Rank> & innerStridesRatios(void) const
    {
        return innerStridesRatios_ ;
    }

    bool isAligned(void) const
    {
        if (isRowMajor())
        {
            return StridesGetter<Ordering<Eigen::RowMajor> >::isAligned(shape_,
                                                                        strides_) ;
        }
        return StridesGetter<Ordering<Eigen::ColMajor> >::isAligned(shape_,
                                                                    strides_) ;
    }

    typename Indices<Rank>::IndexType getEndIndexInData(void) const
    {
        Indices<Rank> endInd ;
        for (std::size_t i=0; i<Rank; ++i)
        {
            endInd(i) = shape_(i) - 1;
        }
        if (isRowMajor())
        {
            endInd(Rank-1) += 1 ;
        }
        else
        {
            endInd(0) += 1 ;
        }
        return Indexer<OrderingType>::get(endInd, strides_,
                                          innerStridesRatios_) ;
    }

    typename Indices<Rank>::IndexType getIndexInData(
                                      const Indices<Rank> & indices
                                                    ) const
    {
        return Indexer<OrderingType>::get(indices, strides_,
                                          innerStridesRatios_) ;
    }

    typename DataStorageType::iterator getIteratorToData(const Indices<Rank> & indices)
    {
        typename Indices<Rank>::IndexType ind(getIndexInData(indices));
        if (static_cast<std::size_t>(ind) >= p_data_->size())
        {
            throw OutOfBoundsException("Index is out of storage bounds") ;
        }
        return p_data_->begin() + iStart_ + ind ;
    }

    typename DataStorageType::const_iterator getIteratorToData(const Indices<Rank> & indices) const
    {
        typename Indices<Rank>::IndexType ind(getIndexInData(indices));
        if (static_cast<std::size_t>(ind) >= p_data_->size())
        {
            throw OutOfBoundsException("Index is out of storage bounds") ;
        }
        return p_data_->begin() + iStart_ + ind ;
    }

    bool isRowMajor(void) const
    {
        return isRowMajor_ ;
    }

    ThisType & operator=(const ThisType & other)
    {
        if (shape_ != other.shape())
        {
            throw IncorrectDataAssignmentException(
                             "Cannot assign data, shapes differ"
                                                  ) ;
        }
        // We have to copy the values in this data storage
        auto itOther(other.begin()) ;
        auto it(begin()) ;
        auto itEnd(end()) ;
        for (; it!=itEnd; ++it, ++itOther)
        {
            *it = *itOther ;
        }
        return *this ;
    }

    ThisType & operator=(const Storage<DataType, Rank, false, OrderingType> & other)
    {
        if (shape_ != other.shape())
        {
            throw IncorrectDataAssignmentException(
                             "Cannot assign data, shapes differ"
                                                  ) ;
        }
        // We have to copy the values in this data storage
        auto itOther(other.begin()) ;
        auto it(begin()) ;
        auto itEnd(end()) ;
        for (; it!=itEnd; ++it, ++itOther)
        {
            *it = *itOther ;
        }
        return *this ;
    }

    class StorageIterator
    {
    private:
        typedef std::function<typename DataStorageType::iterator
                              (const Indices<Rank> &)> FunctionType ;

        p_DataStorageType p_data_ ;
        typename DataStorageType::iterator it_ ;
        IndexIterator<OrderingType, Rank> indexIterator_ ;
        typename IndexIterator<OrderingType, Rank>::iterator indexIt_ ;
        FunctionType fGet_ ;
        bool isAligned_ ;
    public:
        typedef StorageIterator self_type ;
        typedef DataType value_type ;
        typedef DataType & reference ;
        typedef DataType * pointer ;
        typedef std::forward_iterator_tag iterator_category ;
        typedef std::size_t difference_type ;

        StorageIterator(p_DataStorageType p_data,
                        typename Indices<Rank>::IndexType iStart,
                        const Shape<Rank> & shape,
                        FunctionType fGet,
                        bool isAligned):
            p_data_(p_data), it_(p_data->begin()+iStart),
            indexIterator_(shape), indexIt_(indexIterator_.begin()),
            fGet_(fGet), isAligned_(isAligned)
        {
        }

        reference operator*(void) { return *it_ ; }
        pointer operator->(void) { return it_ ; }
        bool operator==(const self_type & other) { return it_ == other.it_ ; }
        bool operator!=(const self_type & other) { return it_ != other.it_ ; }

        self_type operator++(void)
        {
            self_type it = *this ;
            if (isAligned_)
            {
                it_++ ;
            }
            else
            {
                indexIt_++ ;
                if (indexIt_ == indexIterator_.end())
                {
                    it_ = p_data_->end() ;
                }
                else
                {
                    it_ = fGet_(*indexIt_) ;
                }
            }
            return it ;
        }

        self_type operator++(int dummy)
        {
            if (isAligned_)
            {
                it_++ ;
            }
            else
            {
                indexIt_++ ;
                if (indexIt_ == indexIterator_.end())
                {
                    it_ = p_data_->end() ;
                }
                else
                {
                    it_ = fGet_(*indexIt_) ;
                }
            }
            return *this ;
        }

    } ;


    class StorageConstIterator
    {
    private:

        typedef std::function<typename DataStorageType::const_iterator
                              (const Indices<Rank> &)> FunctionType ;

        p_DataStorageType p_data_ ;
        typename DataStorageType::const_iterator it_ ;
        IndexIterator<OrderingType, Rank> indexIterator_ ;
        typename IndexIterator<OrderingType, Rank>::iterator indexIt_ ;
        FunctionType fGet_ ;
        bool isAligned_ ;
    public:
        typedef StorageConstIterator self_type ;
        typedef DataType value_type ;
        typedef DataType & reference ;
        typedef DataType * pointer ;
        typedef std::forward_iterator_tag iterator_category ;
        typedef std::size_t difference_type ;

        StorageConstIterator(p_DataStorageType p_data,
                             typename Indices<Rank>::IndexType iStart,
                             const Shape<Rank> & shape,
                             FunctionType fGet,
                             bool isAligned):
            p_data_(p_data), it_(p_data->begin()+iStart),
            indexIterator_(shape), indexIt_(indexIterator_.begin()),
            fGet_(fGet), isAligned_(isAligned)
        {
        }

        const value_type operator*(void) const { return *it_ ; }
        const pointer operator->(void) const { return it_ ; }
        bool operator==(const self_type & other) { return it_ == other.it_ ; }
        bool operator!=(const self_type & other) { return it_ != other.it_ ; }

        self_type operator++(void)
        {
            self_type it(*this) ;
            if (isAligned_)
            {
                it_++ ;
            }
            else
            {
                indexIt_++ ;
                if (indexIt_ == indexIterator_.end())
                {
                    it_ = p_data_->end() ;
                }
                else
                {
                    it_ = fGet_(*indexIt_) ;
                }
            }
            return it ;
        }

        self_type operator++(int dummy)
        {
            if (isAligned_)
            {
                it_++ ;
            }
            else
            {
                indexIt_++ ;
                if (indexIt_ == indexIterator_.end())
                {
                    it_ = p_data_->end() ;
                }
                else
                {
                    it_ = fGet_(*indexIt_) ;
                }
            }
            return *this ;
        }
    } ;

    StorageIterator begin(void)
    {
        typedef typename DataStorageType::iterator
                    (ThisType::*fptr) (const Indices<Rank> &) ;
        return StorageIterator(p_data_, iStart_, shape_,
                               std::bind((fptr)&ThisType::getIteratorToData,
                                         this, std::placeholders::_1),
                               isAligned()) ;
    }

    StorageIterator end(void)
    {
        typedef typename DataStorageType::iterator
                    (ThisType::*fptr) (const Indices<Rank> &) ;
        return StorageIterator(p_data_, p_data_->size(), shape_,
                               std::bind((fptr)&ThisType::getIteratorToData,
                                         this, std::placeholders::_1),
                               isAligned()) ;
    }

    StorageConstIterator begin(void) const
    {
        typedef typename DataStorageType::const_iterator (ThisType::*fptr)
                   (const Indices<Rank> &) const ;
        return StorageConstIterator(p_data_, iStart_, shape_,
                                    std::bind((fptr)&ThisType::getIteratorToData,
                                              this, std::placeholders::_1),
                                    isAligned()) ;
    }

    StorageConstIterator end(void) const
    {
        typedef typename DataStorageType::const_iterator (ThisType::*fptr)
                   (const Indices<Rank> &) const ;
        return StorageConstIterator(p_data_, p_data_->size(), shape_,
                                    std::bind((fptr)&ThisType::getIteratorToData,
                                              this, std::placeholders::_1),
                                    isAligned()) ;
    }

} ;

// FIXME the ordering type is actually the one of p_dataOwner_ !
template <typename DataType, typename OrderingType>
class Storage<DataType, 0, true, OrderingType>
{
private:
    typedef std::vector<DataType> DataStorageType ;
    typedef std::shared_ptr<std::vector<DataType> > p_DataStorageType ;

    Shape<0> shape_ ;
    std::size_t size_ ;
    Strides<0> strides_ ;
    p_DataStorageType p_data_ ;
    typename Indices<0>::IndexType iStart_ ;
    Indices<0> innerStridesRatios_ ;
    bool isRowMajor_ ;

    typedef Storage<DataType, 0, true, OrderingType> ThisType ;

public:

    Storage(const ThisType & other) :
        shape_(other.shape()),
        size_(other.size()),
        strides_(other.strides()),
        p_data_(other.getDataStorage()),
        iStart_(other.getIStart()),
        innerStridesRatios_(other.innerStridesRatios()),
        isRowMajor_(other.isRowMajor())
    {
    }

    Storage(const Storage<DataType, 0, false, OrderingType> & other) :
        shape_(other.shape()),
        size_(other.size()),
        strides_(other.strides()),
        p_data_(other.getDataStorage()),
        iStart_(other.getIStart()),
        innerStridesRatios_(other.innerStridesRatios()),
        isRowMajor_(other.isRowMajor())
    {
    }

    template <std::size_t N, bool View, std::size_t L, std::size_t M>
    Storage(const Storage<DataType, N, View, OrderingType> & other,
            const Shape<L> & shape,
            const Indices<L> & innerStridesRatios,
            const Indices<M> & topLeftCornerIndices,
            const Indices<N> & outerToInnerIndices) :
        shape_(shape), size_(1),
        p_data_(other.getDataStorage()),
        iStart_(other.getIndexInData(topLeftCornerIndices)+other.getIStart()),
        innerStridesRatios_(innerStridesRatios),
        isRowMajor_(other.isRowMajor())
    {
    }

    ~Storage(void)
    {
    }

    DataType * data(void)
    {
        return p_data_->data() + iStart_ ;
    }

    const DataType * data(void) const
    {
        return p_data_->data() + iStart_ ;
    }

    p_DataStorageType getDataStorage(void) const
    {
        return p_data_ ;
    }

    typename Indices<0>::IndexType getIStart(void) const
    {
        return iStart_ ;
    }

    std::size_t size(void) const
    {
        return size_ ;
    }

    const Strides<0> & strides(void) const
    {
        return strides_ ;
    }

    const Shape<0> & shape(void) const
    {
        return shape_ ;
    }

    const Indices<0> & innerStridesRatios(void) const
    {
        return innerStridesRatios_ ;
    }

    bool isAligned(void) const
    {
        return true ;
    }

    typename Indices<0>::IndexType getIndexInData(
                                      const Indices<0> & indices
                                                 ) const
    {
        return 0 ;
    }

    typename DataStorageType::iterator getIteratorToData(const Indices<0> & indices)
    {
        return p_data_->begin() + iStart_ ;
    }

    typename DataStorageType::const_iterator getIteratorToData(const Indices<0> & indices) const
    {
        return p_data_->begin() + iStart_ ;
    }

    bool isRowMajor(void) const
    {
        return isRowMajor_ ;
    }

    ThisType & operator=(const ThisType & other)
    {
        if (shape_ != other.shape())
        {
            throw IncorrectDataAssignmentException(
                             "Cannot assign data, shapes differ"
                                                  ) ;
        }
        *data() = *other.data() ;
        //shape_ = other.shape() ;
        //size_ = other.size() ;
        //strides_ = other.strides() ;
        //p_data_ = other.getDataStorage() ;
        //iStart_ = other.getIStart() ;
        //innerStridesRatios_ = other.innerStridesRatios() ;
        //isRowMajor_ = other.isRowMajor() ;
        return *this ;
    }

    ThisType & operator=(const Storage<DataType, 0, false, OrderingType> & other)
    {
        if (shape_ != other.shape())
        {
            throw IncorrectDataAssignmentException(
                             "Cannot assign data, shapes differ"
                                                  ) ;
        }
        *data() = *other.data() ;
        //shape_ = other.shape() ;
        //size_ = other.size() ;
        //strides_ = other.strides() ;
        //p_data_ = other.getDataStorage() ;
        //iStart_ = other.getIStart() ;
        //innerStridesRatios_ = other.innerStridesRatios() ;
        //isRowMajor_ = other.isRowMajor() ;
        return *this ;
    }

    typename DataStorageType::iterator begin(void)
    {
        return p_data_->begin() + iStart_ ;
    }

    typename DataStorageType::iterator end(void)
    {
        return p_data_->begin() + iStart_ + 1 ;
    }

    typename DataStorageType::const_iterator begin(void) const
    {
        return p_data_->begin() + iStart_ ;
    }

    typename DataStorageType::const_iterator end(void) const
    {
        return p_data_->begin() + iStart_ + 1;
    }

} ;

} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Storage_hpp
