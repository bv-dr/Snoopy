#ifndef BV_Tools_Array_Slicing_hpp
#define BV_Tools_Array_Slicing_hpp

#include "Tools/BVException.hpp"

namespace BV {
namespace Tools {
namespace Array {

namespace Details {

struct InvalidSlicingException : public BV::Tools::Exceptions::BVException
{
    InvalidSlicingException(std::string message) : BVException(message)
    {
    }
} ;

inline std::size_t SizeChecker(const int & num, const int & den)
{
    if ((num % den) != 0)
    {
        throw InvalidSlicingException(
                       "Invalid slicing exception, size not integral"
                                     ) ;
    }
    return static_cast<std::size_t>(num / den) ;
}

} // End of namespace Details

struct Slice
{
    enum {START=0, END = -1} ;

    int startIndex ;
    int endIndex ;
    int step ;

    Slice(int start, int end, int step) :
        startIndex(start), endIndex(end), step(step)
    {
    }

    Slice(int start, int end) : Slice(start, end, 1)
    {
    }

    Slice(const Slice & other) :
        startIndex(other.startIndex), endIndex(other.endIndex),
        step(other.step)
    {
    }

    Slice(const std::size_t & ind) : Slice(ind, ind, 1)
    {
    }

    Slice & operator=(const Slice & other)
    {
        startIndex = other.startIndex ;
        endIndex = other.endIndex ;
        step = other.step ;
        return *this ;
    }

    Slice & operator=(const std::size_t & ind)
    {
        startIndex = ind ;
        endIndex = ind ;
        step = 1 ;
        return *this ;
    }

    std::size_t getSize(const std::size_t & dataSize) const
    {
        std::size_t size ;
        if (endIndex == Slice::END)
        {
            size = Details::SizeChecker(dataSize - startIndex, step) ;
        }
        else
        {
            size = Details::SizeChecker(endIndex - startIndex + 1, step) ;
        }
        return size ;
    }

} ;

} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Slicing_hpp
