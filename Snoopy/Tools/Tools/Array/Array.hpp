#ifndef BV_Tools_Array_Array_hpp
#define BV_Tools_Array_Array_hpp

#include <array>
#include <algorithm>
#include <iostream>
#include <utility>

#include <Eigen/Dense>

#include "Tools/BVException.hpp"
#include "Tools/Array/ArrayBase.hpp"
#include "Tools/Array/Details.hpp"
#include "Tools/Array/Indexing.hpp"
#include "Tools/Array/Slicing.hpp"
#include "Tools/Array/Storage.hpp"

namespace BV {
namespace Tools {
namespace Array {

#define PRINT_EIGEN 1

struct InvalidOperationException : public BV::Tools::Exceptions::BVException
{
    InvalidOperationException(std::string message) : BVException(message)
    {
    }
} ;

struct InvalidEigenTypeException : public BV::Tools::Exceptions::BVException
{
    InvalidEigenTypeException(std::string message) : BVException(message)
    {
    }
} ;

inline void ErrorCheckerEigen(bool error, std::string messageIfWrong)
{
    if (error)
    {
        throw InvalidEigenTypeException(messageIfWrong) ;
    }
}

template <typename DataType_, std::size_t Rank_, bool IsAView_=false,
          typename OrderingType_=Ordering<Eigen::ColMajor> >
class Array : public ArrayBase<Array<DataType_, Rank_, IsAView_,
                                     OrderingType_> >
{
private:
    template <bool View>
    void operationChecker_(const Array<DataType_, Rank_, View,
                                       OrderingType_> & other)
    {
        if (this->shape() != other.shape())
        {
            throw InvalidOperationException(
                      "Impossible to perform operation, shapes differ"
                                           ) ;
        }
    }

public:
    typedef Array<DataType_, Rank_, IsAView_, OrderingType_> ThisType ;
    typedef Array<DataType_, Rank_, false, OrderingType_> ThisTypeNoView ;

    template <typename ...IndexTypes>
    Array(IndexTypes ...dims) : ArrayBase<ThisType>(dims...)
    {
    }

    Array(const Shape<Rank_> & shape) : ArrayBase<ThisType>(shape)
    {
    }

    template <std::size_t N, bool View, typename Order, size_t M, size_t P>
    Array(const Storage<DataType_, N, View, Order> & otherStorage,
          const Shape<Rank_> & shape,
          const Indices<Rank_> & innerStridesRatios,
          const Indices<M> & topLeftCornerIndices,
          const Indices<P> & outerToInnerIndices) :
        ArrayBase<ThisType>(otherStorage, shape, innerStridesRatios,
                            topLeftCornerIndices, outerToInnerIndices)
    {
    }

    Array(const ThisType & other) : ArrayBase<ThisType>(other)
    {
    }

    template <bool View>
    Array(const Array<DataType_, Rank_, View, OrderingType_> & other) :
        ArrayBase<ThisType>(other)
    {
    }

    ~Array(void)
    {
    }

    void fill(const DataType_ & value)
    {
        std::fill(this->storage_.begin(), this->storage_.end(), value) ;
    }

    // FIXME should I allow the operations for other ordering type ?

    template <bool View>
    ThisType & operator+=(const Array<DataType_, Rank_, View,
                                      OrderingType_> & other)
    {
        operationChecker_(other) ;
        auto itThis(this->storage_.begin()) ;
        auto itThisEnd(this->storage_.end()) ;
        auto itOther(other.storage().begin()) ;
        while (itThis != itThisEnd)
        {
            *itThis += *itOther ;
            ++itThis ;
            ++itOther ;
        }
        return *this ;
    }

    template <bool View>
    ThisTypeNoView operator+(const Array<DataType_, Rank_, View,
                                         OrderingType_> & other) const
    {
        ThisTypeNoView tmp(*this) ;
        tmp += other ;
        return tmp ;
    }

    template <bool View>
    ThisType & operator-=(const Array<DataType_, Rank_, View,
                                      OrderingType_> & other)
    {
        operationChecker_(other) ;
        auto itThis(this->storage_.begin()) ;
        auto itThisEnd(this->storage_.end()) ;
        auto itOther(other.storage().begin()) ;
        while (itThis != itThisEnd)
        {
            *itThis -= *itOther ;
            ++itThis ;
            ++itOther ;
        }
        return *this ;
    }

    template <bool View>
    ThisTypeNoView operator-(const Array<DataType_, Rank_, View,
                                         OrderingType_> & other) const
    {
        ThisTypeNoView tmp(*this) ;
        tmp -= other ;
        return tmp ;
    }

    ThisTypeNoView operator-(void) const
    {
        ThisTypeNoView tmp(*this) ;
        auto itTmp(tmp.storage().begin()) ;
        auto itTmpEnd(tmp.storage().end()) ;
        while (itTmp != itTmpEnd)
        {
            *itTmp *= -1 ;
            ++itTmp ;
        }
        return tmp ;
    }

    ThisType & operator*=(const DataType_ & val)
    {
        auto itThis(this->storage_.begin()) ;
        auto itThisEnd(this->storage_.end()) ;
        while (itThis != itThisEnd)
        {
            *itThis *= val ;
            ++itThis ;
        }
        return *this ;
    }

    ThisTypeNoView operator*(const DataType_ & val) const
    {
        ThisTypeNoView tmp(*this) ;
        tmp *= val ;
        return tmp ;
    }

    ThisType & operator/=(const DataType_ & val)
    {
        auto itThis(this->storage_.begin()) ;
        auto itThisEnd(this->storage_.end()) ;
        while (itThis != itThisEnd)
        {
            *itThis /= val ;
            ++itThis ;
        }
        return *this ;
    }

    ThisTypeNoView operator/(const DataType_ & val) const
    {
        ThisTypeNoView tmp(*this) ;
        tmp /= val ;
        return tmp ;
    }

    // TODO transpose !
    // TODO coef wise operations ?

} ;

template <typename DataType_, std::size_t Rank_, bool View,
          typename OrderingType_>
inline Array<DataType_, Rank_, false, OrderingType_> operator*(
                      const double & val,
                      const Array<DataType_, Rank_, View, OrderingType_> & array
                                                              )
{
    Array<DataType_, Rank_, false, OrderingType_> tmp(array) ;
    tmp *= val ;
    return tmp ;
}

// Partial specialization for rank 0, 1 and 2 arrays

template <typename DataType_, bool IsAView_, typename OrderingType_>
class Array<DataType_, 0, IsAView_, OrderingType_> :
    public ArrayBase<Array<DataType_, 0, IsAView_, OrderingType_> >
{
private:
    typedef Array<DataType_, 0, IsAView_, OrderingType_> ThisType ;

public:

    Array(const Shape<0> & shape) : ArrayBase<ThisType>(shape)
    {
    }

    Array(const ThisType & other) :
        ArrayBase<ThisType>(other)
    {
    }

    template <bool View>
    Array(const Array<DataType_, 0, View, OrderingType_> & other) :
        ArrayBase<ThisType>(other)
    {
    }

    template <std::size_t N, bool View, typename Order, size_t M, size_t P>
    Array(const Storage<DataType_, N, View, Order> & otherStorage,
          const Shape<0> & shape,
          const Indices<0> & innerStridesRatios,
          const Indices<M> & topLeftCornerIndices,
          const Indices<P> & outerToInnerIndices) :
        ArrayBase<ThisType>(otherStorage, shape, innerStridesRatios,
                            topLeftCornerIndices, outerToInnerIndices)
    {
    }

    ~Array(void)
    {
    }

    ThisType & operator=(const ThisType & other)
    {
        return ArrayBase<ThisType>::operator=(other) ;
    }

    template <bool View>
    ThisType & operator=(const Array<DataType_, 0, View, OrderingType_> & other)
    {
        return ArrayBase<ThisType>::operator=(other) ;
    }

    ThisType & operator=(const DataType_ & val)
    {
        *this->storage_.data() = val ;
        return *this ;
    }

    operator const DataType_ &(void) const
    {
        return *this->storage_.data() ;
    }

    operator DataType_ &(void)
    {
        return *this->storage_.data() ;
    }

} ;

template <typename DataType_, bool IsAView_, typename OrderingType_>
class Array<DataType_, 1, IsAView_, OrderingType_> :
    public ArrayBase<Array<DataType_, 1, IsAView_, OrderingType_> >,
    public Eigen::Map<Eigen::Matrix<DataType_, Eigen::Dynamic,
                                    1, OrderingType_::type>,
                      Eigen::Unaligned,
                      Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic> >
{
private:
    typedef Array<DataType_, 1, IsAView_, OrderingType_> ThisType ;

    typedef Eigen::Matrix<DataType_, Eigen::Dynamic,
                          1, OrderingType_::type> EigenInnerType ;

    typedef Eigen::Map<EigenInnerType, Eigen::Unaligned,
                       Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic> > EigenBase ;

public:

    typedef EigenBase EigenType ;

    Array(const Shape<1> & shape) :
        ArrayBase<ThisType>(shape),
        EigenBase(this->storage_.data(), this->size(),
                  Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(0, this->storage_.strides()[0]))
    {
    }

    Array(std::size_t dim) :
        ArrayBase<ThisType>(dim),
        EigenBase(this->storage_.data(), this->size(),
                  Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(0, this->storage_.strides()[0]))
    {
    }

    Array(const ThisType & other) :
        ArrayBase<ThisType>(other),
        EigenBase(this->storage_.data(), this->size(),
                  Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(0, this->storage_.strides()[0]))
    {
    }

    template <bool View>
    Array(const Array<DataType_, 1, View, OrderingType_> & other) :
        ArrayBase<ThisType>(other),
        EigenBase(this->storage_.data(), this->size(),
                  Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(0, this->storage_.strides()[0]))
    {
    }

    template <typename Derived>
    Array(const Eigen::MatrixBase<Derived> & vect) :
        Array(vect.size())
    {
        this->operator=(vect) ;
    }

    template <std::size_t N, bool View, size_t M, size_t P>
    Array(const Storage<DataType_, N, View, OrderingType_> & otherStorage,
          const Shape<1> & shape,
          const Indices<1> & innerStridesRatios,
          const Indices<M> & topLeftCornerIndices,
          const Indices<P> & outerToInnerIndices) :
        ArrayBase<ThisType>(otherStorage, shape, innerStridesRatios,
                            topLeftCornerIndices, outerToInnerIndices),
        EigenBase(this->storage_.data(), this->size(),
                  Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic>(0, this->storage_.strides()[0]))
    {
    }

    ~Array(void)
    {
    }

    using ArrayBase<ThisType>::size ;
    using ArrayBase<ThisType>::data ;
    using ArrayBase<ThisType>::operator() ;

    ThisType & operator=(const ThisType & other)
    {
        return ArrayBase<ThisType>::operator=(other) ;
    }

    template <bool View>
    ThisType & operator=(const Array<DataType_, 1, View, OrderingType_> & other)
    {
        return ArrayBase<ThisType>::operator=(other) ;
    }

    template <typename Derived>
    ThisType & operator=(const Eigen::MatrixBase<Derived> & vect)
    {
        ErrorCheckerEigen((vect.rows() > 1 ) && (vect.cols() > 1),
                          "Cannot assign a matrix to a rank 1 array") ;
        std::size_t size(vect.size()) ;
        ErrorCheckerEigen(size != this->size(),
                          "Cannot assign vector of size"+std::to_string(size)
                          +"to an array of size"+std::to_string(this->size())) ;
        Indices<1> ind(0) ;
        for (std::size_t i=0; i<size; ++i, ++ind[0])
        {
            *this->storage_.getIteratorToData(ind) = vect(i) ;
        }
        return *this ;
    }

} ;

template <typename DataType_, bool IsAView_, typename OrderingType_>
class Array<DataType_, 2, IsAView_, OrderingType_> :
    public ArrayBase<Array<DataType_, 2, IsAView_, OrderingType_> >,
    public Eigen::Map<Eigen::Matrix<DataType_, Eigen::Dynamic,
                                    Eigen::Dynamic, OrderingType_::type>,
                      Eigen::Unaligned,
                      Eigen::Stride<Eigen::Dynamic, Eigen::Dynamic> >
{
private:
    typedef Array<DataType_, 2, IsAView_, OrderingType_> ThisType ;

    typedef Eigen::Matrix<DataType_, Eigen::Dynamic,
                          Eigen::Dynamic, OrderingType_::type> EigenInnerType ;

    typedef Eigen::Map<EigenInnerType, Eigen::Unaligned,
                       Eigen::Stride<Eigen::Dynamic,
                                     Eigen::Dynamic> > EigenBase ;


public:

    typedef EigenBase EigenType ;

    Array(const Shape<2> & shape) :
        ArrayBase<ThisType>(shape),
        EigenBase(this->storage_.data(),
                  this->storage_.shape()[0],
                  this->storage_.shape()[1],
                  Eigen::Stride<Eigen::Dynamic,
                                Eigen::Dynamic>(this->storage_.strides()[1]
                                                 *this->storage_.strides()[0]
                                                 *this->storage_.innerStridesRatios()[1],
                                                this->storage_.strides()[0]
                                                *this->storage_.innerStridesRatios()[0]))
    {
    }

    Array(std::size_t dim1, std::size_t dim2) :
        ArrayBase<ThisType>(dim1, dim2),
        EigenBase(this->storage_.data(),
                  this->storage_.shape()[0],
                  this->storage_.shape()[1],
                  Eigen::Stride<Eigen::Dynamic,
                                Eigen::Dynamic>(this->storage_.strides()[1]
                                                 *this->storage_.strides()[0]
                                                 *this->storage_.innerStridesRatios()[1],
                                                this->storage_.strides()[0]
                                                *this->storage_.innerStridesRatios()[0]))
    {
    }

    Array(const ThisType & other) :
        ArrayBase<ThisType>(other),
        EigenBase(this->storage_.data(),
                  this->storage_.shape()[0],
                  this->storage_.shape()[1],
                  Eigen::Stride<Eigen::Dynamic,
                                Eigen::Dynamic>(this->storage_.strides()[1]
                                                 *this->storage_.strides()[0]
                                                 *this->storage_.innerStridesRatios()[1],
                                                this->storage_.strides()[0]
                                                *this->storage_.innerStridesRatios()[0]))
    {
    }

    template <bool View>
    Array(const Array<DataType_, 2, View, OrderingType_> & other) :
        ArrayBase<ThisType>(other),
        EigenBase(this->storage_.data(),
                  this->storage_.shape()[0],
                  this->storage_.shape()[1],
                  Eigen::Stride<Eigen::Dynamic,
                                Eigen::Dynamic>(this->storage_.strides()[1]
                                                 *this->storage_.strides()[0]
                                                 *this->storage_.innerStridesRatios()[1],
                                                this->storage_.strides()[0]
                                                *this->storage_.innerStridesRatios()[0]))
    {
    }

    template <typename Derived>
    Array(const Eigen::MatrixBase<Derived> & mat) :
        Array(mat.rows(), mat.cols())
    {
        this->operator=(mat) ;
    }

    template <std::size_t N, bool View, typename Order, size_t M, size_t P>
    Array(const Storage<DataType_, N, View, Order> & otherStorage,
          const Shape<2> & shape,
          const Indices<2> & innerStridesRatios,
          const Indices<M> & topLeftCornerIndices,
          const Indices<P> & outerToInnerIndices) :
        ArrayBase<ThisType>(otherStorage, shape, innerStridesRatios,
                            topLeftCornerIndices, outerToInnerIndices),
        EigenBase(this->storage_.data(),
                  this->storage_.shape()[0],
                  this->storage_.shape()[1],
                  Eigen::Stride<Eigen::Dynamic,
                                Eigen::Dynamic>(this->storage_.strides()[1]
                                                 *this->storage_.strides()[0]
                                                 *this->storage_.innerStridesRatios()[1],
                                                this->storage_.strides()[0]
                                                *this->storage_.innerStridesRatios()[0]))
    {
    }

    ~Array(void)
    {
    }

    using ArrayBase<ThisType>::size ;
    using ArrayBase<ThisType>::data ;
    using ArrayBase<ThisType>::operator() ;

    ThisType & operator=(const ThisType & other)
    {
        return ArrayBase<ThisType>::operator=(other) ;
    }

    template <bool View>
    ThisType & operator=(const Array<DataType_, 2, View, OrderingType_> & other)
    {
        return ArrayBase<ThisType>::operator=(other) ;
    }

    template <typename Derived>
    ThisType & operator=(const Eigen::MatrixBase<Derived> & mat)
    {
        int nRows(mat.rows()) ;
        int nCols(mat.cols()) ;
        Shape<2> shape(this->storage_.shape()) ;
        ErrorCheckerEigen((nRows != shape[0])
                          || (nCols != shape[1]),
                          "Cannot assign matrix of shape ("
                          +std::to_string(nRows) + ", "
                          + std::to_string(nCols) + ") "
                          +"to an array of shape ("
                          + std::to_string(shape[0]) + ", "
                          + std::to_string(shape[1]) + ")") ;
        IndexIterator<OrderingType_, 2> iterator(shape) ;
        for (const Indices<2> & ind : iterator)
        {
            *this->storage_.getIteratorToData(ind) = mat(ind[0], ind[1]) ;
        }
        return *this ;
    }

} ;

template <typename DataType, bool IsAView, typename OrderingType>
inline std::ostream & operator<<(std::ostream & f,
                                 Array<DataType, 0, IsAView,
                                       OrderingType> array)
{
    f << (DataType) array ;
    return f ;
}

template <typename DataType, bool IsAView, typename OrderingType>
inline std::ostream & operator<<(std::ostream & f,
                                 Array<DataType, 1, IsAView,
                                       OrderingType> array)
{
#if PRINT_EIGEN
    f << static_cast<typename Array<DataType, 1,
                                    IsAView, OrderingType>::EigenType>(array) ;
#else
    for (int i=0; i<static_cast<int>(array.size()); ++i)
    {
        f << array(i) << " " ;
    }
    f << std::endl ;
#endif
    return f ;
}

template <typename DataType, bool IsAView, typename OrderingType>
inline std::ostream & operator<<(std::ostream & f,
                                 Array<DataType, 2, IsAView,
                                       OrderingType> array)
{
#if PRINT_EIGEN
    f << static_cast<typename Array<DataType, 2, IsAView,
                                    OrderingType>::EigenType>(array) << std::endl ;
#else
    int nRows(array.shape()[0]) ;
    for (int i=0; i<nRows; ++i)
    {
        f << array(i) ;
    }
#endif
    f << std::endl ;
    return f ;
}

#define BV_PRINT_ARRAY(rank)                                           \
    template <typename DataType, bool IsAView, typename OrderingType>  \
    inline std::ostream & operator<<(std::ostream & f,                 \
                                     Array<DataType, rank, IsAView,    \
                                           OrderingType> array)        \
    {                                                                  \
        int nVals(array.shape()[0]) ;                                  \
        for (int i=0; i<nVals; ++i)                                    \
        {                                                              \
            f << array(i) ;                                            \
        }                                                              \
        f << std::endl ;                                               \
        return f ;                                                     \
    }

BV_PRINT_ARRAY(3)
BV_PRINT_ARRAY(4)
BV_PRINT_ARRAY(5)
BV_PRINT_ARRAY(6)

#undef PRINT_EIGEN
#undef BV_PRINT_ARRAY

} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Array_hpp
