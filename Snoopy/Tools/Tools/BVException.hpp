#pragma once

#include <exception>
#include <string>

namespace BV {
namespace Tools {
namespace Exceptions {
class BVException: public std::exception
{
protected:
    std::string message_ ;
public:
    BVException(std::string message) :
        message_(message)
    {
    }

    virtual ~BVException()
    {
    }

    const char* what() const noexcept
    {
        return message_.c_str() ;
    }

    std::string getMessage() const noexcept
    {
        return message_ ;
    }
} ;

}
}
}
