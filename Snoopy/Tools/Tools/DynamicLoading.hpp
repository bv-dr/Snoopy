#pragma once

#if defined _WIN32 || defined __CYGWIN__ || defined __MINGW32__ || defined _WIN64
    #define BV_WIN true
    #include <windows.h>
    #define BV_DLOPEN LoadLibrary
    #define BV_DLSYM GetProcAddress
    #define BV_DLCLOSE FreeLibrary
#else
    #define BV_WIN false
    #include <dlfcn.h>
    #define BV_DLOPEN dlopen
    #define BV_DLSYM dlsym
    #define BV_DLCLOSE dlclose
    #define HMODULE void*
    #define HINSTANCE void*
#endif
