#pragma once

#include <string>
#include <chrono>

namespace BV {
namespace Tools {

class SimpleChrono
{
    std::chrono::high_resolution_clock::time_point startInstant_ ;
public:
    SimpleChrono()
    {
    }

    void start()
    {
        startInstant_ = std::chrono::high_resolution_clock::now() ;
    }

    std::string getFormattedDuration() const
    {
        std::chrono::duration<float> durationSec =
            std::chrono::high_resolution_clock::now() - startInstant_ ;
        std::chrono::milliseconds durationMsec = std::chrono::duration_cast<
            std::chrono::milliseconds>(durationSec) ;
        auto msec(durationMsec.count()) ;
        std::string strDuration = "s" + std::to_string(msec % 1000) ;
        int sec = static_cast<int>(msec / 1000.) ;
        if (sec < 60)
        {
            strDuration = std::to_string(sec) + strDuration ;
        }
        else
        {
            int min = static_cast<int>(sec / 60) ;
            if (min < 60)
            {
                strDuration = std::to_string(min) + "min"
                    + std::to_string(sec % 60) + strDuration ;
            }
            else
            {
                strDuration = std::to_string(static_cast<int>(min / 60.)) + "h"
                    + std::to_string(min % 60) + "min"
                    + std::to_string(sec % 60) + strDuration ;
            }
        }
        return strDuration ;
    }
} ;
}
}
