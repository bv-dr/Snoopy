#pragma once
#ifndef BV_Tools_AbstractFactory_AbstractFactory_hpp
#define BV_Tools_AbstractFactory_AbstractFactory_hpp

#include <memory>
#include <map>
#include <iostream>

#include "Tools/BVException.hpp"

namespace BV {
namespace Tools {
namespace AbstractFactory {

struct UnregisteredException : public Exceptions::BVException
{
    UnregisteredException(std::string message): Exceptions::BVException(message)
    {
    }
} ;

struct AlreadyRegisteredException : public Exceptions::BVException
{
    AlreadyRegisteredException(std::string message): Exceptions::BVException(message)
    {
    }
} ;

template <typename BaseType, typename KeyType, typename...Args>
struct AbstractFactory
{
    typedef std::shared_ptr<BaseType> (*CreateBaseTypeFunc)(const Args &...) ;
    typedef std::map<KeyType, CreateBaseTypeFunc> Registry ;

    static inline Registry & getRegistry(void)
    {
        static Registry reg ;
        return reg ;
    }

    static inline void printRegisteredKeys(void)
    {
        std::cout << "Registered keys:" << std::endl ;
        const Registry & reg(getRegistry()) ;
        for(typename Registry::const_iterator it=reg.begin(); it!=reg.end(); ++it)
        {
             std::cout << it->first << std::endl ;
        }
    }

    template <typename Derived>
    static std::shared_ptr<BaseType> createFunc(Args ...args)
    {
        return std::shared_ptr<BaseType>(new Derived(args...)) ;
    }

    static std::shared_ptr<BaseType> create(const KeyType & key,
                                            Args ...args)
    {
        Registry & reg(getRegistry()) ;
        typename Registry::iterator it(reg.find(key)) ;

        if (it == reg.end())
        {
            std::cout << "No such factory entry: " << key << std::endl ;
            throw UnregisteredException("No such factory entry") ;
            return nullptr ;
        }

        CreateBaseTypeFunc func = it->second ;
        return func(args...) ;
    }

} ;

namespace Details {

template<typename BaseClass,
         typename DerivedType,
         typename KeyType,
         typename... Args>
struct RegistryEntry
{
public:
    static RegistryEntry<BaseClass, DerivedType,
                         KeyType, Args...> & Instance(const KeyType & key)
    {
        //std::cout << "key registration: " << key << std::endl ;
        static RegistryEntry<BaseClass, DerivedType, KeyType, Args...> inst(key) ;
        return inst ;
    }

private:
    RegistryEntry(const KeyType & key)
    {
        //std::cout << "Registering key: " << key << std::endl ;
        typename AbstractFactory<BaseClass, KeyType, Args...>::Registry & reg(
                     AbstractFactory<BaseClass, KeyType, Args...>::getRegistry()
                                                                             ) ;
        typename AbstractFactory<BaseClass, KeyType, Args...>::CreateBaseTypeFunc func(
                            AbstractFactory<BaseClass, KeyType, Args...>::template createFunc<DerivedType>
                                                                                      ) ;
        std::pair<typename AbstractFactory<BaseClass, KeyType, Args...>::Registry::iterator, bool> ret(
                     reg.insert(typename AbstractFactory<BaseClass, KeyType, Args...>::Registry::value_type(key, func))
                                                                                                      ) ;

        if (ret.second == false)
        {
            std::cout << key << " already registered" << std::endl ;
            throw AlreadyRegisteredException("Key already registered in the factory") ;
        }
    }

    RegistryEntry(const RegistryEntry<BaseClass, DerivedType, KeyType, Args...> &) = delete ;
    RegistryEntry & operator=(const RegistryEntry<BaseClass, DerivedType, KeyType, Args...> &) = delete ;
} ;

} // End of namespace Details

} // End of namespace AbstractFactory
} // End of namespace Tools
} // End of namespace BV

#define BV_FACTORY_REGISTER(BaseType, DerivedType, KeyType, key, ...)          \
    namespace {                                                                \
                                                                               \
    using ::BV::Tools::AbstractFactory::Details::RegistryEntry ;               \
                                                                               \
    template <typename T1, typename T2, typename T3>                           \
    struct BaseType ## Registration ;                                          \
                                                                               \
    template <>                                                                \
    struct BaseType ## Registration<BaseType, DerivedType, KeyType>            \
    {                                                                          \
        static const RegistryEntry<BaseType, DerivedType,                      \
                                   KeyType, __VA_ARGS__> & reg ;               \
        void dummy(void) const                                                 \
        {                                                                      \
            (void)reg ;                                                        \
        }                                                                      \
    } ;                                                                        \
                                                                               \
    const RegistryEntry<BaseType, DerivedType, KeyType, __VA_ARGS__> &         \
        BaseType ## Registration<BaseType, DerivedType, KeyType>::reg =        \
		    RegistryEntry<BaseType, DerivedType,                               \
                          KeyType, __VA_ARGS__>::Instance(key) ;               \
    }

#define BV_FACTORY_REGISTER_NO_ARGS(BaseType, DerivedType, KeyType, key)       \
    namespace {                                                                \
                                                                               \
    using ::BV::Tools::AbstractFactory::Details::RegistryEntry ;               \
                                                                               \
    template <typename T1, typename T2, typename T3>                           \
    struct BaseType ## Registration ;                                          \
                                                                               \
    template <>                                                                \
    struct BaseType ## Registration<BaseType, DerivedType, KeyType>            \
    {                                                                          \
        static const RegistryEntry<BaseType, DerivedType, KeyType> & reg ;     \
        void dummy(void) const                                                 \
        {                                                                      \
            (void)reg ;                                                        \
        }                                                                      \
    } ;                                                                        \
                                                                               \
    const RegistryEntry<BaseType, DerivedType, KeyType> &                      \
        BaseType ## Registration<BaseType, DerivedType, KeyType>::reg =        \
		    RegistryEntry<BaseType, DerivedType, KeyType>::Instance(key) ;     \
    }

#define BV_FACTORY_REGISTER_TEMPLATE_BASE(name, BaseType, DerivedType, KeyType, key, ...) \
    namespace {                                                                \
                                                                               \
    using ::BV::Tools::AbstractFactory::Details::RegistryEntry ;               \
                                                                               \
    template <typename T1, typename T2, typename T3>                           \
    struct name ## Registration ;                                              \
                                                                               \
    template <>                                                                \
    struct name ## Registration<BaseType, DerivedType, KeyType>                \
    {                                                                          \
        static const RegistryEntry<BaseType, DerivedType,                      \
                                   KeyType, __VA_ARGS__> & reg ;               \
        void dummy(void) const                                                 \
        {                                                                      \
            (void)reg ;                                                        \
        }                                                                      \
    } ;                                                                        \
                                                                               \
    const RegistryEntry<BaseType, DerivedType, KeyType, __VA_ARGS__> &         \
        name ## Registration<BaseType, DerivedType, KeyType>::reg =            \
		    RegistryEntry<BaseType, DerivedType,                               \
                          KeyType, __VA_ARGS__>::Instance(key) ;               \
    }


/*
 * Define macros when we need several constructors for the same BaseType, DerivedType, KeyType
 */

#define BV_FACTORY_REGISTER_CONSTRUCTOR(BaseType, DerivedType, KeyType, iConstructor, key, ...)     \
    namespace {                                                                                     \
                                                                                                    \
    using ::BV::Tools::AbstractFactory::Details::RegistryEntry ;                                    \
                                                                                                    \
    template <typename T1, typename T2, typename T3, unsigned iC>                                   \
    struct BaseType ## Registration ;                                                               \
                                                                                                    \
    template <>                                                                                     \
    struct BaseType ## Registration<BaseType, DerivedType, KeyType, iConstructor>                   \
    {                                                                                               \
        static const RegistryEntry<BaseType, DerivedType,                                           \
                                   KeyType, __VA_ARGS__> & reg ;                                    \
        void dummy(void) const                                                                      \
        {                                                                                           \
            (void)reg ;                                                                             \
        }                                                                                           \
    } ;                                                                                             \
                                                                                                    \
    const RegistryEntry<BaseType, DerivedType, KeyType, __VA_ARGS__> &                              \
        BaseType ## Registration<BaseType, DerivedType, KeyType, iConstructor>::reg =               \
            RegistryEntry<BaseType, DerivedType,                                                    \
                          KeyType, __VA_ARGS__>::Instance(key) ;                                    \
    }

#define BV_FACTORY_REGISTER_CONSTRUCTOR_NO_ARGS(BaseType, DerivedType, KeyType, iConstructor, key) \
    namespace {                                                                                    \
                                                                                                   \
    using ::BV::Tools::AbstractFactory::Details::RegistryEntry ;                                   \
                                                                                                   \
    template <typename T1, typename T2, typename T3, unsigned iC>                                  \
    struct BaseType ## Registration ;                                                              \
                                                                                                   \
    template <>                                                                                    \
    struct BaseType ## Registration<BaseType, DerivedType, KeyType, iConstructor>                  \
    {                                                                                              \
        static const RegistryEntry<BaseType, DerivedType, KeyType> & reg ;                         \
        void dummy(void) const                                                                     \
        {                                                                                          \
            (void)reg ;                                                                            \
        }                                                                                          \
    } ;                                                                                            \
                                                                                                   \
    const RegistryEntry<BaseType, DerivedType, KeyType> &                                          \
        BaseType ## Registration<BaseType, DerivedType, KeyType, iConstructor>::reg =              \
            RegistryEntry<BaseType, DerivedType, KeyType>::Instance(key) ;                         \
    }

#endif // BV_Tools_AbstractFactory_AbstractFactory_hpp

//template <typename Object>
//class Singleton : boost::noncopyable
//{
//public:
//    static Object & instance(void)
//    {
//        static Object object ;
//        return object ;
//    }
//
//    virtual ~Singleton(void)
//    {
//    }
//} ;
//
//template<typename KeyType,class T>
//std::vector<KeyType> GetKeys(const std::map<KeyType,T> & map) {
//
//    std::vector<KeyType> keys ;
//    typedef typename std::map<KeyType,T>::const_iterator ItType ;
//    ItType itEnd(map.end());
//    for(ItType it(map.begin()); it!=itEnd; ++it)
//    {
//        keys.push_back(it->first) ;
//    }
//    return keys ;
//}

//template <typename BaseClassType,
//          typename KeyType=std::string>
//struct RegisteredBase
//{
//    //virtual std::shared_ptr<BaseClassType> Create(...) = 0 ;
//    virtual ~RegisteredBase(void)
//    {
//    }
//} ;

//template <template <class BaseClassType, class KeyType> class RegisteredBase,
//          typename BaseClassType,
//          typename KeyType=std::string>
//struct AbstractFactory : public Singleton<AbstractFactory<RegisteredBase,
//                                                          BaseClassType, KeyType> >
//{
//private:
//    typedef typename std::map<KeyType, std::shared_ptr<RegisteredBase<
//                                                        BaseClassType, KeyType
//                                                                     > > > InnerMap ;
//    typedef typename InnerMap::const_iterator InnerMapIterator ;
//    std::unique_ptr<InnerMap> p_innerMap_ ;
//public:
//    friend class Singleton<AbstractFactory<RegisteredBase, BaseClassType, KeyType> > ;
//
//    bool Register(const KeyType & key,
//                  std::shared_ptr<RegisteredBase<BaseClassType, KeyType> > registered)
//    {
//        return p_innerMap_->insert(typename InnerMap::value_type(key, registered)).second ;
//    }
//
//    bool Register(const AbstractFactory<RegisteredBase, BaseClassType, KeyType> & other)
//    {
//        std::unique_ptr<InnerMap> tmp(new InnerMap(*p_innerMap_)) ;
//        InnerMapIterator it(other.p_innerMap_->begin()) ;
//        while (it != other.p_innerMap_->end())
//        {
//            if (!tmp->insert(typename InnerMap::value_type(*it++)).second)
//            {
//                return false ;
//            }
//        }
//        std::swap(p_innerMap_, tmp) ;
//        return true ;
//    }
//
//    template <typename...Args>
//    std::shared_ptr<BaseClassType> Create(const KeyType & key, Args...args) const
//    {
//        InnerMapIterator it(p_innerMap_->find(key)) ;
//        if (it == p_innerMap_->end())
//        {
//            throw UnregisteredException("Creator key not registered in factory") ;
//        }
//        return it->second->Create(args...) ;
//    }
//
//    std::vector<KeyType> GetKeys(void) const
//    {
//        return GetKeys(*p_innerMap_) ;
//    }
//} ;
//
//template <template <class RegisteredBase, class BaseClassType, class DerivedType, class KeyType> class Derived,
//          template <class BaseClassType, class KeyType> class RegisteredBase,
//          typename BaseClassType,
//          typename DerivedType,
//          typename KeyType=std::string>
//struct Registered : public RegisteredBase<BaseClassType, KeyType>
//{
//protected:
//
//    template <typename... Args>
//    std::shared_ptr<BaseClassType> Create(Args...args)
//    {
//        return std::shared_ptr<BaseClassType>(new DerivedType(args...)) ;
//    }
//
//    Registered(void)
//    {
//    }
//
//    Registered(const Registered<Derived, RegisteredBase, BaseClassType, DerivedType, KeyType> &) ;
//
//    Registered<Derived, RegisteredBase, BaseClassType, DerivedType, KeyType> &
//        operator=(const Registered<Derived, RegisteredBase, BaseClassType, DerivedType, KeyType> &) ;
//public:
//    Registered(const KeyType & key)
//    {
//        try
//        {
//            AbstractFactory<RegisteredBase, BaseClassType, KeyType>::instance().Register(
//                    key,
//                    std::shared_ptr<RegisteredBase<BaseClassType, KeyType> >(
//                            new Derived<RegisteredBase, BaseClassType, DerivedType, KeyType>
//                                                                            )
//                                                                                        ) ;
//        }
//        catch (...)
//        {
//        }
//    }
//
//    virtual ~Registered(void)
//    {
//    }
//} ;

