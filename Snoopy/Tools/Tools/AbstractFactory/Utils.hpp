#pragma once

#include <type_traits>
#include <memory>
#include <utility>

#include "Tools/BVException.hpp"

namespace BV {
namespace Tools {
namespace AbstractFactory {

class InvalidFactoryEntryException : public Exceptions::BVException
{
    InvalidFactoryEntryException(std::string message): Exceptions::BVException(message)
    {
    }
} ;

template <typename T, typename ...Args>
static std::enable_if_t<
        std::is_constructible<T, Args...>::value, std::shared_ptr<T>
                       > Create(Args && ... args)
{
    return std::make_shared<T>(std::forward<Args>(args)...) ;
}

// impossible to construct
template <typename T, typename... Args>
static std::enable_if_t<
        !std::is_constructible<T, Args...>::value, std::shared_ptr<T>
                       > Create(Args&&...)
{
    throw "Invalid initialization" ; // TODO create an exception
    return nullptr ;
}

} // End of namespace AbstractFactory
} // End of namespace Tools
} // End of namespace BV
