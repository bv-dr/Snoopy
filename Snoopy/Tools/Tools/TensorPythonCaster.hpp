#pragma once

#include <memory>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <iostream>

namespace py = pybind11 ;

namespace BV{
namespace Tools{
namespace Details {

template <int EigenOrder>
struct EigenToPyOrder ;

template <>
struct EigenToPyOrder<Eigen::ColMajor>
{
    enum {order = py::array::f_style} ;
} ;

template <>
struct EigenToPyOrder<Eigen::RowMajor>
{
    enum {order = py::array::c_style} ;
} ;

template <int PyOrder>
struct PyToEigenOrder ;

template <>
struct PyToEigenOrder<py::array::f_style>
{
    enum {order = Eigen::ColMajor} ;
} ;

template <>
struct PyToEigenOrder<py::array::c_style>
{
    enum {order = Eigen::RowMajor} ;
} ;

} // End of namespace Details
}
}

namespace pybind11 {
namespace detail {

template <typename T, int Rank, int Ordering, typename IndexType>
struct type_caster<Eigen::Tensor<T, Rank, Ordering, IndexType> >
{
public:
    using TensorType = Eigen::Tensor<T, Rank, Ordering, IndexType> ;

    PYBIND11_TYPE_CASTER(TensorType, _("Eigen::Tensor<T, R, 0>")) ;

    // Conversion part 1 (Python -> C++)
    bool load(py::handle src, bool convert)
    {
        if (!convert && !py::array_t<T>::check_(src))
        {
            return false ;
        }

        //auto buf = py::array_t<T, Details::BVToPyOrder<Ordering>::order | py::array::forcecast>::ensure(src) ;
        auto buf = py::array_t<T, BV::Tools::Details::EigenToPyOrder<Ordering>::order>::ensure(src) ;
        if (!buf)
        {
            // TODO convert the ordering type !!
            std::cout << "Order may differ between python and c++ array" << std::endl ;
            return false ;
        }

        auto nDims = buf.ndim() ;
        if (nDims != Rank )
        {
            return false ;
        }

        typename TensorType::Dimensions shape ;

        for (Eigen::Index i=0; i<Rank; ++i)
        {
            shape[i] = buf.shape()[i] ;
        }

        value = Eigen::TensorMap<TensorType>(buf.mutable_data(), shape) ;

        return true ;
    }

    //Conversion part 2 (C++ -> Python)
    static py::handle cast(const TensorType & src,
                           py::return_value_policy policy,
                           py::handle parent)
    {
        // TODO deal with return value policy !!
        std::vector<size_t> shape(Rank) ;

        for (std::size_t i=0; i<Rank; ++i)
        {
            shape[i] = src.dimensions()[i] ;
        }
        py::array_t<T, BV::Tools::Details::EigenToPyOrder<Ordering>::order> a(
                                                std::move(shape), src.data()
                                                                  ) ;
        return a.release() ;
    }
} ;

} // End of namespace details
} // End of namespace pybind11

