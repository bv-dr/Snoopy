#pragma once

#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

// #include "Geometry/GeometryTypedefs.hpp"
// #include "Geometry/Vector.hpp"
// #include "Geometry/Rotation/RotationMatrix.hpp"

// using namespace BV::Geometry ;

namespace BV {
namespace Tools {
namespace Tests {

template <typename T>
inline void CheckEigenMatrix(const T & m1, const T & m2,
                      const double epsilon=1.e-9)
{
    using IndexType = typename Eigen::EigenBase<T>::Index ;
    IndexType nRows(m1.rows()) ;
    IndexType nCols(m1.cols()) ;
    for (IndexType i=0; i<nRows; ++i)
    {
        for (IndexType j=0; j<nCols; ++j)
        {
            BOOST_CHECK_SMALL(m1(i, j)-m2(i, j), epsilon) ;
        }
    }
}

inline void CheckEigenVectors(const Eigen::VectorXd & v1,
                       const Eigen::VectorXd & v2,
                       const double epsilon=1.e-9)
{
    assert (v1.size() == v2.size()) ;
    for (unsigned i=0; i<v1.size(); ++i)
    {
        BOOST_CHECK_SMALL(v1(i)-v2(i), epsilon) ;
    }
}

inline void CheckArrays(const double * ar1, const double * ar2,
                 const std::size_t & size,
                 const double epsilon=1.e-9)
{
    for (std::size_t i=0; i<size; ++i)
    {
        BOOST_CHECK_SMALL(ar1[i] - ar2[i], epsilon) ;
    }
}

} // End of namespace Tests
} // End of namespace Tools
} // End of namespace BV
