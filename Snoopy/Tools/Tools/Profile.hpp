/*
 * Profile.hpp
 *
 *  Created on: 15 nov. 2017
 *      Author: cbrun
 */

#pragma once

#include <chrono>

class Profile
{
    std::string pName_ ;
    std::chrono::high_resolution_clock::time_point t0_ ;
    std::chrono::high_resolution_clock::time_point t1_ ;
    bool t1Supt0_ ;

public:
    Profile(const std::string & name) :
        pName_(name), t0_(std::chrono::high_resolution_clock::now()),
        t1_(std::chrono::high_resolution_clock::now()), t1Supt0_(true)
    {
    }

    void print(const std::string & text)
    {
        if (t1Supt0_)
        {
            t1_ = std::chrono::high_resolution_clock::now() ;
            std::cout << "(" << pName_ << "): " << text << " "
                << std::chrono::duration<double, std::milli>(t1_ - t0_).count()
                << std::endl ;
        }
        else
        {
            t0_ = std::chrono::high_resolution_clock::now() ;
            std::cout << "(" << pName_ << "): " << text << " "
                << std::chrono::duration<double, std::milli>(t0_ - t1_).count()
                << std::endl ;
        }
        t1Supt0_ = !t1Supt0_ ;
    }

} ;
