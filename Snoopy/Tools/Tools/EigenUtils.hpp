#pragma once

#include <Eigen/Dense>
#include <vector>

#include "Tools/BVException.hpp"

namespace BV {
namespace Tools {

// Here we check implicitly that T is an Eigen type...
template <typename T>
typename T::PlainObject Unique(const T & values)
{
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(T)
    Eigen::Index nValues(values.size()) ;
    std::vector<typename T::Scalar> vals(values.data(), values.data()+nValues) ;
    std::sort(vals.begin(), vals.end()) ;
    auto last = std::unique(vals.begin(), vals.end()) ;
    vals.erase(last, vals.end()) ;
    return Eigen::Map<typename T::PlainObject>(vals.data(), vals.size()) ;
}

template <typename Derived>
void applyWhere(const Eigen::DenseBase<Derived> & values,
                const Eigen::Ref<const Eigen::Array<bool, Eigen::Dynamic, 1> > & condArray,
                std::function<void(const typename Derived::Scalar &)> func)
{
    // FIXME we could easily extend this function for 2 dimensions values
    EIGEN_STATIC_ASSERT_VECTOR_ONLY(Derived)
    if (values.size() != condArray.size())
    {
        throw BV::Tools::Exceptions::BVException("Wrong array sizes in EigenUtils::Where") ;
    }
    Eigen::Index nVals(condArray.size()) ;
    for (Eigen::Index iVal=0; iVal<nVals; ++iVal)
    {
        if (!condArray(iVal))
        {
            continue ;
        }
        func(values(iVal)) ;
    }
}

// Usage:
// Eigen::ArrayXd a(Eigen::ArrayXd::Ones(10)) ;
// a(1) = 2. ;
// Eigen::ArrayXd freqs(BV::Tools::Unique(a)) ;
// Eigen::Index nFreqs(freqs.size()) ;
// Eigen::ArrayXd b(Eigen::ArrayXd::Ones(10)) ;
// Eigen::ArrayXd cvalues(nFreqs) ;
// for (Eigen::Index iFreq=0; iFreq<nFreqs; ++iFreq)
// {
//     double sum(0.) ;
//     BV::Tools::Where(b, (a-freqs[iFreq]).abs() < 1.e-8,
//                      [&sum](const double & val)
//                      {
//                           sum += val ;
//                      }) ;
//      cvalues(iFreq) = sum ;
// }
// std::cout << cvalues.transpose() << std::endl ;

} // End of namespace Tools
} // End of namespace BV
