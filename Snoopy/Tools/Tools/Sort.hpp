#pragma once

#include <iostream>
#include <memory>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

/*
 * Here there are functions to provide the sorting of the Eigen::Tensor
 *
 * Let we have a tensor of the coefficients, where
 * the first index corresponds to the parameters p1 of size d1
 * the second one -- p2 of size d2
 * ...
 * the n-th -- pn of size dn
 * Eigen::ArrayXd  p1(d1)
 * Eigen::ArrayXd  p2(d2)
 * ...
 * Eigen::ArrayXd  pn(dn)
 * Eigen::Tensor<T, n> tensor(d1,d2,...,dn)
 *
 * We want to sort the parameters pk, in this case the tensor must be modified as well
 *
 * Eigen::ArrayXi ind = sortArray(pk)   // sorting the array pk. ind is the array of indices of the original array
 * sortTensor(tensor, k, ind)           // rearrange the tensor according to the sorting result
 */
namespace BV {
namespace Tools {
/*
 * The first two functions are written for a generic array of type T, which has at least operator< ("less") defined
 */
/**
 * sortArray(T *b, const int &n) sorts the array b of length n.
 * @param[in] *b, pointer to array b of type T
 * @param[in] n, size of the array b
 * @return std::unique_ptr<int[]> which is an array of the original indices after the sorting
 * Note: type T must have possibility to compare "less" (operator<)
 */
template<typename T>
std::unique_ptr<int[]> sortArray(T *b, int n)
{
    std::unique_ptr<int[]> index = std::make_unique<int[]>(n);
    for(int i = 0; i < n; i++)
        index[i] = i;
    for(int i = 0; i < n; ++i)
    {
        T min = b[i];
        for(int j = i+1; j < n; j++)
        {
            if (b[j] < min)
            {
                min = b[j];
                std::swap(b[j], b[i]);
                std::swap(index[i], index[j]);
            }
        }
    }
    return index;
}
/**
 * sortArray(T *b, std::unique_ptr<int[]> const &index, const int &n) rearrange the array b of length n
 * according to the given array of indeces "index"
 * For example: b = {1, 4, 3, 5, 1.2} and index = {0, 4, 3, 1, 2} will produce
 * b == {1, 1.2, 3, 4, 5}
 */
template<typename T>
void sortArray(T *b, std::unique_ptr<int[]> const &index, int n)
{
    T *b_cp = new T[n];
    for(int i = 0; i < n; ++i)
        b_cp[i] = b[i];
    for(int i = 0; i < n; ++i)
        b[i] = b_cp[index[i]];
    delete[] b_cp;
}

/*
 * The last functions are for Eigen::ArrayXd types
 */
/**
 * @brief sortArray(Eigen::Ref<Eigen::ArrayXd> b) sorts the array b
 * @param b is an array to be sorted
 * @return Eigen::ArrayXi index array of modified original indeces
 * For example, if b = {1, 4, 3, 5, 1.2}, then after the execution we have
 * b == {1, 1.2, 3, 4, 5}
 * return value == {0, 4, 3, 1, 2}
 */
template<typename T = void>
Eigen::ArrayXi sortArray(Eigen::Ref<Eigen::ArrayXd> b)
{
    Eigen::ArrayXi index(b.size());
    Eigen::Index n = b.size();
    for(Eigen::Index i = 0; i < n; i++)
        index[i] = static_cast<int>(i);
    for(Eigen::Index i = 0; i < n; ++i)
    {
        double min = b[i];
        for(Eigen::Index j = i+1; j < n; j++)
        {
            if (b[j] < min)
            {
                min = b[j];
                std::swap(b[j], b[i]);
                std::swap(index[i], index[j]);
            }
        }
    }
    return index;
}
/**
 * @brief sortArray sorts array b with respect to the indeces (index) list
 * @param b
 * @param index
 */
template<typename T = void>
void sortArray(Eigen::Ref<Eigen::ArrayXd> b, Eigen::Ref<Eigen::ArrayXi> index)
{
    Eigen::ArrayXd b_cp(b);
    auto n = b.size();
    for(decltype(n) i = 0; i < n; ++i)
        b[i] = b_cp[index[i]];
}
/**
 * sortTensorByCol(Eigen::Tensor<T,n> &t, const int &col, Eigen::Ref<Eigen::ArrayXi> index)
 * rearrange the tensor t.
 * @param t Eigen::Tensor<T,n> to be rearranged
 * @param col  index of the tensor, which corresponds to the parameter vector p_col
 * @param index list of indices how to rearrange.
 *
 * Let we have a tensor of the coefficients, where
 * the first index corresponds to the parameters p1 of size d1
 * the second one -- p2 of size d2
 * ...
 * the n-th -- pn of size dn
 * Eigen::ArrayXd  p1(d1)
 * Eigen::ArrayXd  p2(d2)
 * ...
 * Eigen::ArrayXd  pn(dn)
 * Eigen::Tensor<T, n> tensor(d1,d2,...,dn)
 *
 * We want to sort the parameters pk, in this case the tensor must be modified as well
 *
 * Eigen::ArrayXi ind = sortArray(pk)   // sorting the array pk. ind is the array of indices of the original array
 * sortTensor(tensor, k, ind)           // rearrange the tensor according to the sorting result
 */
template<typename T, int n>
//void sortTensorByCol(Eigen::Ref<Eigen::Tensor<T,n>> t, ... does not work!
void sortTensorByCol(Eigen::Tensor<T,n> &t, const int &col, Eigen::Ref<Eigen::ArrayXi> index)
{
    // creating offset and extent arrays:
    Eigen::array<long long, n> offset_tmp;   // slice offset for tmp
    Eigen::array<long long, n> offset_t;     // slice offset for t
    Eigen::array<long long, n> extent;       // slice values to be taken
    // pre-fill offset and extent arrays
    for (Eigen::Index i = 0; i < n; ++i) {
        offset_t[i] = 0;
        offset_tmp[i] = 0;
        extent[i] = t.dimension(i);
    }
    extent[col] = 1;
    Eigen::Tensor<T,n> tmp(t);
    for (Eigen::Index i = 0; i < t.dimension(col); ++i)
    {
        offset_t[col] = i;
        offset_tmp[col] = index[i];
        t.slice(offset_t, extent) = tmp.slice(offset_tmp, extent);
    }
}

/**
 * boundTensor(Eigen::Index col, Eigen::ArrayXd &b, Eigen::Tensor<T,n> &t, T lowerBound, T upperBound, bool periodic, bool adjustB)
 * If the parameters vector b is in (b_min, b_max) or (b_min, b_max] or [b_min, b_max), then the tensor t is extended, so
 * the parameter vector b is in [b_min, b_max]
 * @param col index of the tensor t, which corresponds to the parameter vector b
 * @param b parameter vector. It is assumed that b_min <= b[0] <= b[1] <= ... <= b[b.size() -1] <= b_max
 * @param t tensor
 * @param lowerBound is b_min
 * @param upperBound is b_max
 * @param periodic is periodic condition
 * @param adjustB is condition whether b must be changed or not (by default it is true)
 *
 * The tensor t is extended as the following:
 * if distance(pk[0], pk_min) > 1.e-8 => lowerBound is added
 * if distance(pk[dk-1], pk_max) > 1.e-8 => upperBound is added
 * if periodic is false, then
 *  lowerBound == value at pk[0]
 *  upperBound == value at pk[dk-1]
 * otherwise, for periodic == true
 *      if distance(pk[0], pk_min) < distance(pk[dk-1], pk_max) => lowerBound == upperBound == value at pk[0]
 *      otherwise lowerBound == upperBound == value at pk[dk-1]
 * if only one must be added (for example upperBound)
 * the bound equals to another one (upperBound == lowerBound)
 *
 * adjustB is necessary in a case, then pk == pm. In this case we use boundTensor first time with adjustB == false,
 * and the second adjustB == true.
 * In a case we do not want change pk vector, adjustB is set to false
 *
 * Example:
 * Eigen::ArrayXd p1(d1)
 * Eigen::ArrayXd p2(d2)
 * ...
 * Eigen::ArrayXd pn(dn)
 * Eigen::Tensor<double, n> t(d1, d2, ..., dn)
 *
 * We need to interpolate the values t for pk, but pk is in pk_min < pk[0] <= ... <= pk[dk-1] < pk_max
 * and thus, we may interplate on the interval [pk[0], pk[dn-1]].
 * But we want values for p \in [p_min,pk[0]] and we assume that t(p1,..,p,..,pn) == t(p1,..,p[0]..,pn) and
 * the same for the upper boundary. With this function we get:
 * boundTensor(k, pk, t, pk_min, pk_max, false)
 * as the result, we obtain:
 * pk = {pk_min, pk[0], pk[1], ..., pk[dk-1], pk_max}
 * t.dimension(k) == dk+2;
 */
template<typename T, int n>
void boundTensor(Eigen::Index col, Eigen::ArrayXd &b, Eigen::Tensor<T,n> &t, double lowerBound, double upperBound, bool periodic, bool adjustB = true)
{
    double eps = 1.e-8;
    // assume, that lowerBound <= b[0] <= b[1] <= ... <= b[b.size() -1] <= upperBound
    double lowerDistance = abs(b[0] -lowerBound);
    double upperDistance = abs(b[b.size()-1] -upperBound);
    bool addLowerBound = lowerDistance > eps;
    bool addUpperBound = upperDistance > eps;

    if ((!addLowerBound) && (!addUpperBound))   return;   // nothing to do. It is already well bounded

    // at least one bound must be added

    Eigen::array<Eigen::Index, n> sizes;
    for(Eigen::Index i = 0; i < n; ++i)
        sizes[i] = t.dimension(i);
    if (addLowerBound) sizes[col] ++;
    if (addUpperBound) sizes[col] ++;
    Eigen::ArrayXd b_(sizes[col]);

    // creating offset and extent arrays:
    Eigen::array<long long, n> offset_tmp;   // slice offset
    Eigen::array<long long, n> offset_t; // slice offset for the bound
    Eigen::array<long long, n> extent;   // slice values to be taken
    // pre-fill offset and extent arrays
    for(Eigen::Index i = 0; i < n; ++i)
    {
        offset_tmp[i] = 0;
        offset_t  [i] = 0;
        extent    [i] = t.dimension(i);
    }
    offset_t  [col] = 0;

    Eigen::Tensor<T,n> tmp(sizes);
    if (addLowerBound)
    {
        offset_tmp[col] = 1;
        tmp.slice(offset_tmp, extent) = t;
        extent[col] = 1;
        b_.segment(1,b.size()) = b;
        b_[0] = lowerBound;
        if (addUpperBound)
        {
            // add both bounds
            b_[b_.size()-1] = upperBound;
            if (periodic)
            {
                // add the both bounds periodically
                // lower and upper Bound = at b[0] if lowerDistance < upperDistance
                //                         at b[b.size() -1] if lowerDistance > upperDistance
                if (lowerDistance < upperDistance)
                {
                    offset_t  [col] = 0;
                    offset_tmp[col] = 0;
                    tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
                    offset_tmp[col] = tmp.dimension(col) -1;
                    tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
                }
                else
                {
                    offset_t  [col] = t.dimension(col) -1;
                    offset_tmp[col] = 0;
                    tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
                    offset_tmp[col] = tmp.dimension(col) -1;
                    tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
                }
            }
            else
            {
                // add the both bounds not periodically =>
                // lowerBound at b[0]
                // upperBound at b[b.size() -1]
                offset_tmp[col] = 0;
                offset_t  [col] = 0;
                tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
                offset_tmp[col] = tmp.dimension(col) -1;
                offset_t  [col] = t.dimension(col)   -1;
                tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
            }
        }
        else
        {
            // add only lowerBound
            if (periodic)
            {
                // lowerBound = at b[b.size() -1] which is already the upper bound
                offset_t  [col] = t.dimension(col) -1;
            }
            else
            {
                // lowerBound = at b[0]
                offset_t  [col] = 0;
            }
            offset_tmp[col] = 0;
            tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);

        }
    }
    else
    {
        // add only upperBound , because at least one bound must be added
        offset_tmp[col] = 0;
        tmp.slice(offset_tmp, extent) = t;
        extent[col] = 1;
        b_.segment(0, b.size()) = b;
        b_[b_.size()-1] = upperBound;
        if (periodic)
        {
            // upperBound = at b[0], which is already the lower bound
            offset_t[col] = 0;
        }
        else
        {
            // upperBound = at b[b.size()-1]
            offset_t[col] = t.dimension(col) -1;
        }
        offset_tmp[col] = tmp.dimension(col) -1;
        tmp.slice(offset_tmp, extent) = t.slice(offset_t, extent);
    }
    if (adjustB) b = b_;
    t = tmp;
}

/**
 * @brief sort_test simple example how it works
 */
template<typename T = void>
void sort_test()
{
    Eigen::ArrayXd a0(5);
    a0 << 1, 4, 3, 5, 1.2;
    auto ind = sortArray(a0);
    int i0_ = 5;
    int i1_ = 2;
    int i2_ = 5;
    int i3_ = 2;
    Eigen::Tensor<double, 4> t(i0_, i1_, i2_, i3_);
    for(int i0 = 0; i0 < i0_; i0 ++)
        for (int i1 = 0; i1 < i1_; i1 ++)
            for(int i2 = 0; i2 < i2_; i2 ++)
                for(int i3 = 0; i3 < i3_; i3 ++)
                    t(i0,i1,i2,i3) = (i0+1)*1000. +(i1+1)*100 + (i2+1)*10 +(i3+1);
    std::cout << t << "\n";
    sortTensorByCol(t, 2, ind);
    sortTensorByCol(t, 0, ind);

    // let the indices 0 and 2 represent the frequencies, 0 index - w_1, 2 index - w_2.
    // the frequency range a0 is already sorted.
    // Now we want to bound a0 from 0.9 to 5.1 (not periodically)
    // In a case 0 index - heading_1 and 2 index - heading_2, we could use
    // lowerBound = 0.0, upperBound = 2*pi and periodic = true
    boundTensor(0, a0, t, 0.9, 5.1, false, false);  // because we use only one vector of frequencies for w_1 and w_2, we do not change it
    boundTensor(2, a0, t, 0.9, 5.1, false);         // now, since min(a0) was 1.0 and max(a0) was 5, a0 = {0.9, 1, 1.2, 3, 4, 5, 5.1}
    std::cout << a0 << "\n";

    std::cout << "d0 : " << t.dimension(0) << "\n";
    std::cout << "d1 : " << t.dimension(1) << "\n";
    std::cout << "d2 : " << t.dimension(2) << "\n";
    std::cout << "d3 : " << t.dimension(3) << "\n";

    for (int i0 = 0; i0 < t.dimension(0); i0 ++)
        for(int i1 = 0; i1 < t.dimension(1); i1++)
            for(int i2 = 0; i2 < t.dimension(2); i2++)
                for(int i3 = 0; i3 < t.dimension(3); i3++)
                    std::cout << "["<<i0<<","<<i1<<","<<i2<<","<<i3<<"] = " << t(i0,i1,i2,i3) << "\n";
}

} // End of namespace Tools
} // End of namespace BV
