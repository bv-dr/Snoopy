#pragma once

#include <memory>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

namespace BV {
namespace Tools {
namespace Tensor {

template<typename T>
using  MatrixType = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> ;

template<typename T>
using  ArrayType = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic> ;

template<typename Scalar,int Rank, typename SizeType>
auto TensorToMatrix(const Eigen::Tensor<Scalar,Rank> & tensor,
                    const SizeType rows, const SizeType cols)
{
    return Eigen::Map<const MatrixType<Scalar> >(tensor.data(), rows, cols) ;
}

template<typename Scalar,int Rank, typename SizeType>
auto TensorToArray(const Eigen::Tensor<Scalar,Rank> & tensor,
                   const SizeType rows, const SizeType cols)
{
    return Eigen::Map<const ArrayType<Scalar> >(tensor.data(), rows, cols) ;
}

template<typename Scalar, typename... Dims>
auto MatrixToTensor(const MatrixType<Scalar> & matrix, Dims... dims)
{
    constexpr int Rank = sizeof... (Dims) ;
    return Eigen::TensorMap<Eigen::Tensor<const Scalar, Rank> >(matrix.data(), {dims...}) ;
}

template<typename Scalar, typename... Dims>
auto ArrayToTensor(const ArrayType<Scalar> & array, Dims... dims)
{
    constexpr int Rank = sizeof... (Dims) ;
    return Eigen::TensorMap<Eigen::Tensor<const Scalar, Rank> >(array.data(), {dims...}) ;
}

} // End of namespace Tensor
} // End of namespace Tools
} // End of namespace BV
