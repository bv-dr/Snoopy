from Snoopy.Tools import renamed_class, renamed_function, deprecated_alias
import warnings

def test_fun():


    with warnings.catch_warnings(record = True) as w:
        #---- Test function renaming
        def test_new():
            return "1"

        test_old = renamed_function(test_new, "test_old")
        test_old()


        #---- Test method renaming
        class TestClass():
            def new_method_name(self, i) :
                return "2"

        TestClass.oldMethodName = renamed_function(TestClass.new_method_name, "oldMethodName")
        a = TestClass()
        a.oldMethodName(2)

    assert( len(w) == 2 )

def test_argument():

    with warnings.catch_warnings(record = True) as w:
        #---- Test argument renaming
        class MyClass(object):
            @deprecated_alias(object_id='id_object')
            def __init__(self, id_object):
                self.id = id_object

        a = MyClass( object_id = 1 )

    assert( len(w) == 1 )


def test_class():
    #---- Test class renaming
    with warnings.catch_warnings(record = True) as w :
        class NewClass() :
            def __init__(self):
                self.test = 1.0

        OldClass = renamed_class(NewClass, old_name = "OldClass")

        a = OldClass()

        class Sub( OldClass ):
            pass

    assert( len(w) == 2 )



if __name__ == "__main__" :

    test_fun()
    test_argument()
    test_class()






