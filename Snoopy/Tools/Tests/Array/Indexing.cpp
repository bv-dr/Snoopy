#include "Tests/Array/Indexing.hpp"
#include "Tools/Array/Indexing.hpp"
#include <boost/test/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <array>

#include <Eigen/Dense>


namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

using namespace boost::unit_test ;

void DisplayArray(const double * array, const std::size_t & size)
{
    for (std::size_t i=0; i<size; ++i)
    {
        std::cout << array[i] << " " ;
    }
    std::cout << std::endl ;
}

void CheckArray(const double * ar1, const double * ar2,
                const std::size_t & size)
{
    for (std::size_t i=0; i<size; ++i)
    {
        BOOST_CHECK_CLOSE(ar1[i], ar2[i], 1.e-8) ;
    }
}

void testIndexing(void)
{
    {
        // Indices iteration 2D array
        Shape<2> shape(3, 2) ;
        IndexIterator<Ordering<Eigen::RowMajor>, 2> iterator(shape) ;
        std::size_t count(0) ;
        for (const Indices<2> & inds : iterator)
        {
            if (count < 2)
            {
                BOOST_CHECK_EQUAL(inds[0], 0) ;
            }
            else if (count < 4)
            {
                BOOST_CHECK_EQUAL(inds[0], 1) ;
            }
            else
            {
                BOOST_CHECK_EQUAL(inds[0], 2) ;
            }
            BOOST_CHECK_EQUAL(inds[1], (count%2)!=0) ;
            ++count ;
        }
    }
    {
        // Indices iteration 3D array
        Shape<3> shape(3, 2, 3) ;
        IndexIterator<Ordering<Eigen::RowMajor>, 3> iterator(shape) ;
        int count(0) ;
        for (const Indices<3> & inds : iterator)
        {
            if (count < 6)
            {
                if (count < 3)
                {
                    BOOST_CHECK_EQUAL(inds[1], 0) ;
                    BOOST_CHECK_EQUAL(inds[2], count) ;
                }
                else
                {
                    BOOST_CHECK_EQUAL(inds[1], 1) ;
                    BOOST_CHECK_EQUAL(inds[2], count-3) ;
                }
                BOOST_CHECK_EQUAL(inds[0], 0) ;
            }
            else if (count < 12)
            {
                if (count < 9)
                {
                    BOOST_CHECK_EQUAL(inds[1], 0) ;
                    BOOST_CHECK_EQUAL(inds[2], count-6) ;
                }
                else
                {
                    BOOST_CHECK_EQUAL(inds[1], 1) ;
                    BOOST_CHECK_EQUAL(inds[2], count-9) ;
                }
                BOOST_CHECK_EQUAL(inds[0], 1) ;
            }
            else
            {
                if (count < 15)
                {
                    BOOST_CHECK_EQUAL(inds[1], 0) ;
                    BOOST_CHECK_EQUAL(inds[2], count-12) ;
                }
                else
                {
                    BOOST_CHECK_EQUAL(inds[1], 1) ;
                    BOOST_CHECK_EQUAL(inds[2], count-15) ;
                }
                BOOST_CHECK_EQUAL(inds[0], 2) ;
            }
            ++count ;
        }
    }
}

void testBlockIndexing(void)
{
    {
        // Indices iteration 2D array
        constexpr std::size_t nRows(5) ;
        constexpr std::size_t nCols(4) ;

        // Create 2 Eigen array with both ordering conventions
        Eigen::Matrix<double, nRows, nCols> matColMajor ;
        for (unsigned i=0; i<nRows; ++i)
        {
            for (unsigned j=0; j<nCols; ++j)
            {
                matColMajor(i, j) = i*nRows+(j+1) ;
            }
        }
        double * matColMajorData(matColMajor.data()) ;

        Eigen::Matrix<double, nRows, nCols, Eigen::RowMajor> matRowMajor ;
        for (unsigned i=0; i<nRows; ++i)
        {
            for (unsigned j=0; j<nCols; ++j)
            {
                matRowMajor(i, j) = i*nRows+(j+1) ;
            }
        }
        double * matRowMajorData(matRowMajor.data()) ;

        // Isolate a block in them, it should be identical for both
        Shape<2> shape(5, 4) ;
        Shape<2> subShape(3, 2) ;
        Strides<2> stridesRow(StridesGetter<Ordering<Eigen::RowMajor> >::get(shape)) ;
        Strides<2> stridesCol(StridesGetter<Ordering<Eigen::ColMajor> >::get(shape)) ;

        Indices<2> topLeftIndex(1, 2) ;
        Eigen::Matrix<double, 3, 2> blockCol(matColMajor.block(1, 2, 3, 2)) ;
        Eigen::Matrix<double, 3, 2, Eigen::RowMajor> blockRow(
                                              matRowMajor.block(1, 2, 3, 2)
                                                             ) ;
        Indices<2> ratios(1, 1) ;
        int startIndRow(Indexer<Ordering<Eigen::RowMajor> >::get(topLeftIndex,
                                                                 stridesRow,
                                                                 ratios)) ;
        IndexIterator<Ordering<Eigen::RowMajor>, 2> iteratorRow(subShape) ;
        for (const Indices<2> & inds : iteratorRow)
        {
            int iRow(Indexer<Ordering<Eigen::RowMajor> >::get(inds,
                                                              stridesRow,
                                                              ratios)) ;
            iRow += startIndRow ;
            BOOST_CHECK_SMALL(matRowMajorData[iRow] - blockRow(inds[0], inds[1]),
                              1.e-8) ;
        }
        int startIndCol(Indexer<Ordering<Eigen::ColMajor> >::get(topLeftIndex,
                                                                 stridesCol,
                                                                 ratios)) ;
        IndexIterator<Ordering<Eigen::ColMajor>, 2> iteratorCol(subShape) ;
        for (const Indices<2> & inds : iteratorCol)
        {
            int iCol(Indexer<Ordering<Eigen::ColMajor> >::get(inds,
                                                              stridesCol,
                                                              ratios)) ;
            iCol += startIndCol ;
            BOOST_CHECK_SMALL(matColMajorData[iCol] - blockCol(inds[0], inds[1]),
                              1.e-8) ;
        }
    }
}

//void testOrdering(void)
//{
//    {
//        // 2D matrix row to col.
//        Eigen::Matrix3d matColMajor(3, 3) ;
//        for (unsigned i=0; i<3; ++i)
//        {
//            for (unsigned j=0; j<3; ++j)
//            {
//                matColMajor(i, j) = i*3+(j+1) ;
//            }
//        }
//
//        Eigen::Matrix<double, 3, 3, Eigen::RowMajor> matRowMajor(3, 3) ;
//        for (unsigned i=0; i<3; ++i)
//        {
//            for (unsigned j=0; j<3; ++j)
//            {
//                matRowMajor(i, j) = i*3+(j+1) ;
//            }
//        }
//
//        double * dataRowMajor(new double[9]()) ;
//        double * dataColMajor(new double[9]()) ;
//        double * dataRowMajor2(new double[9]()) ;
//        double * dataColMajor2(new double[9]()) ;
//        std::array<std::size_t, 2> shape {{3, 3}} ;
//        std::array<std::size_t, 2> topLeft {{0, 0}} ;
//        std::array<int, 2> outerToInnerStrides {{0, 1}} ;
//        std::size_t size(9) ;
//        Converter<Ordering<Eigen::RowMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       matColMajor.data(),
//                                                       size,
//                                                       dataRowMajor,
//                                                       false) ;
//        CheckArray(dataRowMajor, matRowMajor.data(), size) ;
//        Converter<Ordering<Eigen::RowMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       matRowMajor.data(),
//                                                       size,
//                                                       dataRowMajor2,
//                                                       true) ;
//        CheckArray(dataRowMajor2, matRowMajor.data(), size) ;
//        Converter<Ordering<Eigen::ColMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       matRowMajor.data(),
//                                                       size,
//                                                       dataColMajor,
//                                                       true) ;
//        CheckArray(dataColMajor, matColMajor.data(), size) ;
//        Converter<Ordering<Eigen::ColMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       matColMajor.data(),
//                                                       size,
//                                                       dataColMajor2,
//                                                       false) ;
//        CheckArray(dataColMajor2, matColMajor.data(), size) ;
//        delete[] dataRowMajor ;
//        delete[] dataColMajor ;
//        delete[] dataRowMajor2 ;
//        delete[] dataColMajor2 ;
//    }
//    {
//        // 3D array
//        std::array<std::size_t, 3> shape {{2, 3, 2}} ;
//        std::array<std::size_t, 3> topLeft {{0, 0, 0}} ;
//        std::array<int, 3> outerToInnerStrides {{0, 1, 2}} ;
//        double * dataRowMajorRef(new double[12]()) ;
//        for (std::size_t i=0; i<12; ++i)
//        {
//            dataRowMajorRef[i] = (double)(i+1) ;
//        }
//        double * dataColMajorRef(new double[12]()) ;
//        dataColMajorRef[0] = 1. ;
//        dataColMajorRef[1] = 7. ;
//        dataColMajorRef[2] = 3. ;
//        dataColMajorRef[3] = 9. ;
//        dataColMajorRef[4] = 5. ;
//        dataColMajorRef[5] = 11. ;
//        dataColMajorRef[6] = 2. ;
//        dataColMajorRef[7] = 8. ;
//        dataColMajorRef[8] = 4. ;
//        dataColMajorRef[9] = 10. ;
//        dataColMajorRef[10] = 6. ;
//        dataColMajorRef[11] = 12. ;
//        double * dataRowMajor(new double[12]()) ;
//        double * dataColMajor(new double[12]()) ;
//        double * dataRowMajor2(new double[12]()) ;
//        double * dataColMajor2(new double[12]()) ;
//        std::size_t size(12) ;
//        Converter<Ordering<Eigen::RowMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       dataColMajorRef,
//                                                       size,
//                                                       dataRowMajor,
//                                                       false) ;
//        CheckArray(dataRowMajor, dataRowMajorRef, size) ;
//        Converter<Ordering<Eigen::RowMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       dataRowMajorRef,
//                                                       size,
//                                                       dataRowMajor2,
//                                                       true) ;
//        CheckArray(dataRowMajor2, dataRowMajorRef, size) ;
//        Converter<Ordering<Eigen::ColMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       dataRowMajorRef,
//                                                       size,
//                                                       dataColMajor,
//                                                       true) ;
//        CheckArray(dataColMajor, dataColMajorRef, size) ;
//        Converter<Ordering<Eigen::ColMajor> >::convert(shape, shape, topLeft,
//                                                       outerToInnerStrides,
//                                                       dataColMajorRef,
//                                                       size,
//                                                       dataColMajor2,
//                                                       false) ;
//        CheckArray(dataColMajor2, dataColMajorRef, size) ;
//        delete[] dataRowMajorRef ;
//        delete[] dataColMajorRef ;
//        delete[] dataRowMajor ;
//        delete[] dataColMajor ;
//        delete[] dataRowMajor2 ;
//        delete[] dataColMajor2 ;
//    }
//}

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV
