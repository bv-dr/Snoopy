#include "Tests/Array/Tools.hpp"

#include <iostream>


namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

//void CheckEigenMatrix(const Eigen::Ref<const Eigen::MatrixXd> & m1,
//                      const Eigen::Ref<const Eigen::MatrixXd> & m2,
//                      const double epsilon)
//{
//    for (unsigned i=0; i<m1.rows(); ++i)
//    {
//        for (unsigned j=0; j<m1.cols(); ++j)
//        {
//            BOOST_CHECK_SMALL(m1(i, j)-m2(i, j), epsilon) ;
//        }
//    }
//}

void CheckEigenVectors(const Eigen::Ref<const Eigen::VectorXd> & v1,
                       const Eigen::Ref<const Eigen::VectorXd> & v2,
                       const double epsilon)
{
    assert (v1.size() == v2.size()) ;
    for (unsigned i=0; i<v1.size(); ++i)
    {
        BOOST_CHECK_SMALL(v1(i)-v2(i), epsilon) ;
    }
}

void CheckArrays(const double * ar1, const double * ar2,
                 const std::size_t & size,
                 const double epsilon)
{
    for (std::size_t i=0; i<size; ++i)
    {
        BOOST_CHECK_SMALL(ar1[i] - ar2[i], epsilon) ;
    }
}

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV
