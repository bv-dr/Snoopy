#ifndef BV_Tools_Array_Tests_Array_hpp
#define BV_Tools_Array_Tests_Array_hpp

namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

//void testArray0(void) ;
void testArray1(void) ;
void testArray2(void) ;
void testArray3(void) ;

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Tests_Array_hpp
