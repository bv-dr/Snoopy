#include "Tests/Array/Storage.hpp"
#include "Tools/Array/Storage.hpp"
#include <boost/test/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <array>
#include <memory>
#include <algorithm>

#include <Eigen/Dense>

#include "Tests/Array/Tools.hpp"
#include "Tools/Array/Indexing.hpp"

namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

template <typename StorageType>
void CheckData1D(double * dataRef, StorageType & storage, std::size_t size)
{
    Indices<1> index ;
    for (std::size_t i=0; i<size; ++i)
    {
        index[0] = i ;
        BOOST_CHECK_SMALL(
             *storage.getIteratorToData(index) - dataRef[i], 1.e-8
                         ) ;
        //BOOST_CHECK_SMALL(
        //     storage.data()[storage.getIndexInData(index)] - dataRef[i], 1.e-8
        //                 ) ;
    }
}

template <typename Order, std::size_t N, typename Store>
void DisplayStorage(const Shape<N> & shape, Store & storage)
{
    IndexIterator<Order, N> iterator(shape) ;
    for (const Indices<N> & ind : iterator)
    {
        std::cout << *storage.getIteratorToData(ind) << " " ;
    }
    std::cout << std::endl ;
}

void testStorage1D(void)
{
    typedef Storage<double, 1, false,
                    Ordering<Eigen::RowMajor> > StorageRowMajor ;
    typedef Storage<double, 1, true,
                    Ordering<Eigen::RowMajor> > ViewRowMajor ;
    //typedef Storage<double, 1, false,
    //                Ordering<Eigen::ColMajor> > StorageColMajor ;
    //typedef Storage<double, 1, true,
    //                Ordering<Eigen::ColMajor> > ViewColMajor ;
    {
        // Test base constructor
        Shape<1> shape(10) ;
        StorageRowMajor storage(shape) ;
        BOOST_CHECK_EQUAL(storage.size(), (std::size_t)10) ;
        BOOST_CHECK(storage.isRowMajor() == true) ;
        double * data(storage.data()) ;
        double * dataRef(new double[10]) ;
        for (std::size_t i=0; i<10; ++i)
        {
            double val(static_cast<double>(i+1)) ;
            data[i] = val ;
            dataRef[i] = val ;
        }
        CheckData1D(dataRef, storage, 10) ;
        // Test copy constructor
        StorageRowMajor storage2(storage) ;
        BOOST_CHECK_EQUAL(storage2.size(), (std::size_t)10) ;
        CheckData1D(dataRef, storage2, 10) ;
        // Test other constructor
        Indices<1> ratios(1) ;
        Indices<1> topLeft0(0) ;
        Indices<1> outerToInner(0) ;
        StorageRowMajor storage3(storage2, shape, ratios, topLeft0, outerToInner) ;
        BOOST_CHECK_EQUAL(storage3.size(), (std::size_t)10) ;
        CheckData1D(dataRef, storage3, 10) ;
        // Test other constructor with other shape
        Shape<1> shape2(6) ;
        Indices<1> topLeft(3) ;
        StorageRowMajor storage4(storage2, shape2, ratios, topLeft, outerToInner) ;
        BOOST_CHECK_EQUAL(storage4.size(), (std::size_t)6) ;
        CheckData1D(dataRef+3, storage4, 6) ;
        // Test to get a view
        ViewRowMajor view(storage2, shape2, ratios, topLeft, outerToInner) ;
        CheckData1D(dataRef+3, view, 6) ;
        // Try to change something in view and check that it changes storage2
        Indices<1> indInView(2) ;
        Indices<1> indInStorage(5) ;
        *view.getIteratorToData(indInView) += 1. ;
        BOOST_CHECK_SMALL(*storage2.getIteratorToData(indInStorage)
                          - *view.getIteratorToData(indInView), 1.e-8) ;
        // Create another view
        ViewRowMajor view2(storage, shape, ratios, topLeft0, outerToInner) ;
        BOOST_CHECK_EQUAL(view2.size(), (std::size_t)10) ;
        CheckData1D(dataRef, view2, 10) ;
        // Test copy of view constructor
        StorageRowMajor storage5(view2, shape, ratios, topLeft0, outerToInner) ;
        BOOST_CHECK_EQUAL(storage5.size(), (std::size_t)10) ;
        CheckData1D(dataRef, storage5, 10) ;
        // Same test with a new shape
        StorageRowMajor storage6(view2, shape2, ratios, topLeft, outerToInner) ;
        BOOST_CHECK_EQUAL(storage6.size(), (std::size_t)6) ;
        CheckData1D(dataRef+3, storage6, 6) ;
        // Create a view of a view
        ViewRowMajor view3(view2, shape2, ratios, topLeft, outerToInner) ;
        BOOST_CHECK_EQUAL(view3.size(), (std::size_t)6) ;
        CheckData1D(dataRef+3, view3, 6) ;
        // Test copy of a view of a view constructor
        StorageRowMajor storage7(view3, shape2, ratios, topLeft0, outerToInner) ;
        BOOST_CHECK_EQUAL(storage7.size(), (std::size_t)6) ;
        CheckData1D(dataRef+3, storage7, 6) ;
        // Test copy of a view of a view constructor with reshape
        Shape<1> shape3(4) ;
        Indices<1> topLeft2(1) ;
        StorageRowMajor storage8(view3, shape3, ratios, topLeft2, outerToInner) ;
        BOOST_CHECK_EQUAL(storage8.size(), (std::size_t)4) ;
        CheckData1D(dataRef+4, storage8, 4) ;
        // Small checks to ensure that col major doesn't change anything
        // TODO below is not possible at the moment as we didn't implement
        // the conversions yet...
        //StorageColMajor storage9(view3, shape3, ratios, topLeft2, outerToInner) ;
        //BOOST_CHECK_EQUAL(storage9.size(), (std::size_t)4) ;
        //CheckData1D(dataRef+4, storage9, 4) ;
        //// Test assignement operator
        //StorageColMajor storage10(shape3) ;
        //storage10 = storage9 ;
        //BOOST_CHECK_EQUAL(storage10.size(), (std::size_t)4) ;
        //CheckData1D(dataRef+4, storage10, 4) ;

        //// TODO add tests on data setting !
        delete[] dataRef ;
    }
}

template <typename EigenType, typename StorageType>
void CheckData2D(const EigenType & ref, StorageType & storage)
{
    Shape<2> shape(ref.rows(), ref.cols()) ;
    IndexIterator<Ordering<Eigen::RowMajor>, 2> iterator(shape) ;
    for (const Indices<2> & inds : iterator)
    {
        double reference(ref(inds[0], inds[1])) ;
        BOOST_CHECK_SMALL(
             *storage.getIteratorToData(inds) - reference, 1.e-8
                         ) ;
        //BOOST_CHECK_SMALL(
        //     storage.data()[storage.getIndexInData(indices)] - reference, 1.e-8
        //                 ) ;
    }
}

void printData(const double * data, std::size_t size)
{
    for (std::size_t i=0; i<size; ++i)
    {
        std::cout << data[i] << std::endl ;
    }
}

void testStorage2D(void)
{
    typedef Storage<double, 2, false,
                    Ordering<Eigen::RowMajor> > StorageRowMajor ;
    typedef Storage<double, 2, true,
                    Ordering<Eigen::RowMajor> > ViewRowMajor ;
    typedef Storage<double, 2, false,
                    Ordering<Eigen::ColMajor> > StorageColMajor ;
    //typedef Storage<double, 2, true,
    //                Ordering<Eigen::ColMajor> > ViewColMajor ;
    // Test base constructor
    Shape<2> shape(10, 4) ;
    Indices<2> topLeft0(0, 0) ;
    Indices<2> ratios(1, 1) ;
    Indices<2> outerToInner(0, 1) ;
    StorageRowMajor storageRow(shape) ;
    BOOST_CHECK_EQUAL(storageRow.size(), (std::size_t)40) ;
    BOOST_CHECK(storageRow.isRowMajor() == true) ;
    StorageColMajor storageCol(shape) ;
    BOOST_CHECK_EQUAL(storageCol.size(), (std::size_t)40) ;
    BOOST_CHECK(storageCol.isRowMajor() == false) ;
    Eigen::Matrix<double, 10, 4, Eigen::RowMajor> refRowMajor ;
    Eigen::Matrix<double, 10, 4, Eigen::ColMajor> refColMajor ;
    Indices<2> indices(0, 0) ;
    for (std::size_t i=0; i<10; ++i)
    {
        indices[0] = i ;
        for (std::size_t j=0; j<4; ++j)
        {
            indices[1] = j ;
            double val(static_cast<double>(i*4+j)) ;
            *storageRow.getIteratorToData(indices) = val ;
            *storageCol.getIteratorToData(indices) = val ;
            refRowMajor(i, j) = val ;
            refColMajor(i, j) = val ;
        }
    }
    CheckArrays(refRowMajor.data(), storageRow.data(), 40) ;
    CheckArrays(refColMajor.data(), storageCol.data(), 40) ;
    CheckData2D(refRowMajor, storageRow) ;
    CheckData2D(refColMajor, storageCol) ;
    // Test copy constructor
    StorageRowMajor storageRow2(storageRow) ;
    BOOST_CHECK_EQUAL(storageRow2.size(), (std::size_t)40) ;
    CheckData2D(refRowMajor, storageRow2) ;
    StorageColMajor storageCol2(storageCol) ;
    BOOST_CHECK_EQUAL(storageCol2.size(), (std::size_t)40) ;
    CheckData2D(refColMajor, storageCol2) ;
    // Test other constructor
    StorageRowMajor storageRow3(storageRow, shape, ratios, topLeft0, outerToInner) ;
    BOOST_CHECK_EQUAL(storageRow3.size(), (std::size_t)40) ;
    CheckData2D(refRowMajor, storageRow3) ;
    StorageColMajor storageCol3(storageCol, shape, ratios, topLeft0, outerToInner) ;
    BOOST_CHECK_EQUAL(storageCol3.size(), (std::size_t)40) ;
    CheckData2D(refColMajor, storageCol3) ;
    // Test conversion row/col
    // FIXME below is not possible at the moment, we need to implement the
    // functionality !
    // StorageRowMajor storageRow4(p_storageCol, shape, topLeft0, outerToInnerStrides) ;
    // BOOST_CHECK_EQUAL(storageRow4.size(), 40) ;
    // CheckData2D(refRowMajor, storageRow4) ;
    // StorageColMajor storageCol4(p_storageRow, shape, topLeft0, outerToInnerStrides) ;
    // BOOST_CHECK_EQUAL(storageCol4.size(), 40) ;
    // CheckData2D(refColMajor, storageCol4) ;
    // Test pointer constructor with other shape
    Shape<2> shape2(6, 2) ;
    Shape<2> topLeft(3, 2) ;
    StorageRowMajor storageRow4(storageRow, shape2, ratios, topLeft, outerToInner) ;
    BOOST_CHECK_EQUAL(storageRow4.size(), (std::size_t)12) ;
    CheckData2D(refRowMajor.block(3, 2, 6, 2), storageRow4) ;
    // Test to get a view
    ViewRowMajor view(storageRow, shape2, ratios, topLeft, outerToInner) ;
    CheckData2D(refRowMajor.block(3, 2, 6, 2), view) ;
    // Try to change something in view and check that it changes p_storage2
    Indices<2> indInView(2, 1) ;
    Indices<2> indInStorage(5, 3) ;
    *view.getIteratorToData(indInView) += 1. ;
    BOOST_CHECK_SMALL(*storageRow.getIteratorToData(indInStorage)
                      - *view.getIteratorToData(indInView), 1.e-8) ;
    *view.getIteratorToData(indInView) -= 1. ;
    // Test copy of view constructor
    ViewRowMajor view2(storageRow, shape, ratios, topLeft0, outerToInner) ;
    StorageRowMajor storage5(view2, shape, ratios, topLeft0, outerToInner) ;
    BOOST_CHECK_EQUAL(storage5.size(), (std::size_t)40) ;
    CheckData2D(refRowMajor, storage5) ;
    // Same test with a new shape
    StorageRowMajor storage6(view2, shape2, ratios, topLeft, outerToInner) ;
    BOOST_CHECK_EQUAL(storage6.size(), (std::size_t)12) ;
    CheckData2D(refRowMajor.block(3, 2, 6, 2), storage6) ;
    // Create a pointer to a view of a view
    ViewRowMajor view3(view2, shape2, ratios, topLeft, outerToInner);
    BOOST_CHECK_EQUAL(view3.size(), (std::size_t)12) ;
    CheckData2D(refRowMajor.block(3, 2, 6, 2), view3) ;
    // Test copy of a view of a view constructor
    StorageRowMajor storage7(view3, shape2, ratios, topLeft0, outerToInner) ;
    BOOST_CHECK_EQUAL(storage7.size(), (std::size_t)12) ;
    CheckData2D(refRowMajor.block(3, 2, 6, 2), storage7) ;
    // Test copy of a view of a view constructor with reshape
    Shape<2> shape3(4, 1) ;
    Indices<2> topLeft2(1, 1) ;
    StorageRowMajor storage8(view3, shape3, ratios, topLeft2, outerToInner) ;
    BOOST_CHECK_EQUAL(storage8.size(), (std::size_t)4) ;
    CheckData2D(refRowMajor.block(4, 3, 4, 1), storage8) ;
    // Test copy of a view of a view constructor with reshape and ratios
    Shape<2> shape4(2, 1) ;
    Indices<2> ratios2(2, 1) ;
    StorageRowMajor storage9(view3, shape4, ratios2, topLeft2, outerToInner) ;
    BOOST_CHECK_EQUAL(storage9.size(), (std::size_t)2) ;
    Indices<2> inds(0, 0) ;
    BOOST_CHECK_SMALL(*storage9.getIteratorToData(inds) - refRowMajor(4, 3),
                      1.e-8) ;
    inds[0] += 1 ;
    BOOST_CHECK_SMALL(*storage9.getIteratorToData(inds) - refRowMajor(6, 3),
                      1.e-8) ;
    // TODO add some tests on ColMajor views !
    // TODO add tests on data setting !
}

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

