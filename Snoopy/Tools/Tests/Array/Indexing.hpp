#ifndef BV_Tools_Array_Tests_Indexing_hpp
#define BV_Tools_Array_Tests_Indexing_hpp

namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

void testIndexing(void) ;
void testBlockIndexing(void) ;
void testOrdering(void) ;

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Tests_Indexing_hpp
