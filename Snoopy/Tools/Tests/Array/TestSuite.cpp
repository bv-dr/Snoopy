#include <boost/test/unit_test.hpp>

#include "Tests/Array/Array.hpp"
#include "Tests/Array/Indexing.hpp"
#include "Tests/Array/Storage.hpp"

using namespace boost::unit_test ;

#define BV_ADD_ARRAY_TEST( testName )                         \
    ArrayTestSuite->add(BOOST_TEST_CASE( &(                   \
                            BV::Tools::Array::Tests::testName \
                                          ) ) ) ;

test_suite * init_unit_test_suite(int argc, char* argv[])
{
    test_suite * ArrayTestSuite = BOOST_TEST_SUITE("ArrayTestSuite") ;

    BV_ADD_ARRAY_TEST(testIndexing)
    BV_ADD_ARRAY_TEST(testBlockIndexing)
    //BV_ADD_ARRAY_TEST(testOrdering)
    BV_ADD_ARRAY_TEST(testStorage1D)
    BV_ADD_ARRAY_TEST(testStorage2D)
    //BV_ADD_ARRAY_TEST(testArray0)
    BV_ADD_ARRAY_TEST(testArray1)
    BV_ADD_ARRAY_TEST(testArray2)
    BV_ADD_ARRAY_TEST(testArray3)

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV Array test suite" ;
    framework::master_test_suite().add( ArrayTestSuite ) ;

  return 0 ;
}

#undef BV_ADD_ARRAY_TEST
