#include "Tests/Array/Array.hpp"
#include "Tools/Array/Array.hpp"
#include <boost/test/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <array>

#include <Eigen/Dense>

#include "Tests/Array/Tools.hpp"
#include "Tools/Array/Indexing.hpp"

namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

using namespace boost::unit_test ;

//void testArray0(void)
//{
//    Array<double, 0> array ;
//    BOOST_CHECK_EQUAL(array.size(), 1) ;
//    // Nothing to check on shape appart from type...
//    Shape<0> shape(array.shape()) ;
//    // Test assignment operator
//    array = 2. ;
//    // Test array explicit conversion to double
//    BOOST_CHECK_CLOSE((double)array, 2., 1.e-8) ;
//    double val(array) ;
//    BOOST_CHECK_CLOSE(val, 2., 1.e-8) ;
//    // Test data method
//    double * data(array.data()) ;
//    BOOST_CHECK_CLOSE(*data, 2., 1.e-8) ;
//    // Test copy constructor from other
//    Array<double, 0> array3(array) ;
//    BOOST_CHECK_CLOSE((double)array3, 2., 1.e-8) ;
//    // Test constructor with pointer
//    Array<double, 0, true> array4(data) ; // No data ownership
//    BOOST_CHECK_CLOSE((double)array4, 2., 1.e-8) ;
//    //// If we change array4, it also changes array2 because of data ownership !
//    array4 = 3. ;
//    BOOST_CHECK_CLOSE((double)array4, 3., 1.e-8) ;
//    BOOST_CHECK_CLOSE((double)array, 3., 1.e-8) ;
//    // Same test with data ownership
//    Array<double, 0, false> array5(data) ; // Data ownership
//    BOOST_CHECK_CLOSE((double)array5, 3., 1.e-8) ;
//    // If we change array4, it doesn't change array2 !
//    array5 = 4. ;
//    BOOST_CHECK_CLOSE((double)array5, 4., 1.e-8) ;
//    BOOST_CHECK_CLOSE((double)array, 3., 1.e-8) ;
//}

void testArray1(void)
{
    Array<double, 1> array(10) ;
    BOOST_CHECK_EQUAL(array.size(), (std::size_t)10) ;
    Shape<1> shape(array.shape()) ;
    BOOST_CHECK_EQUAL(shape[0], 10) ;
    // Test Eigen::VectorXd assigment
    Eigen::VectorXd vec(Eigen::VectorXd::Ones(10)) ;
    array = vec ;
    CheckEigenVectors(array, vec) ;
    // Test copy constructor
    Array<double, 1> array3(array) ;
    CheckEigenVectors(array3, array) ;
    // Test array conversion to Eigen::VectorXd
    typename Array<double, 1>::EigenType test(array3) ;
    // Try to change test and see if it changes array3
    test(1) = 2. ;
    CheckEigenVectors(array3, test) ;
    // Test data method
    double * data(array3.data()) ;
    BOOST_CHECK_SMALL(data[0]-1., 1.e-8) ;
    // Test assignement operator
    Array<double, 1> array4(10) ;
    array4 = array3 ;
    CheckEigenVectors(array3, array4) ;
    // Test Eigen vector assignement
    Eigen::VectorXd vec2(10) ;
    for (std::size_t i=0; i<10; ++i)
    {
        vec2(i) = (double)(i+1) ;
    }
    Array<double, 1> array5(10) ;
    array5 = vec2 ;
    CheckEigenVectors(vec2, array5) ;
    // Test accessing data
    double & val(array5(3)) ;
    BOOST_CHECK_SMALL(val-4., 1.e-8) ;
    val += 1 ;
    BOOST_CHECK_SMALL(array5(3)-5., 1.e-8) ;
    val -= 1 ;
    BOOST_CHECK_SMALL(array5(3)-4., 1.e-8) ;
    // Test accessing data with a slice
    Array<double, 1, true> array6(array5(Slice(1, 3))) ;
    BOOST_CHECK_EQUAL(array6.size(), (std::size_t)3) ;
    BOOST_CHECK_EQUAL(array6.shape()[0], 3) ;
    CheckEigenVectors(vec2.segment(1, 3), array6) ;
    // Test a view of a view
    Array<double, 1, true> array7(array6(Slice(1, 2))) ;
    BOOST_CHECK_EQUAL(array7.size(), (std::size_t)2) ;
    BOOST_CHECK_EQUAL(array7.shape()[0], 2) ;
    CheckEigenVectors(vec2.segment(2, 2), array7) ;
    // Test other slice features
    Array<double, 1, true> array8(array5(Slice(3, Slice::END))) ;
    BOOST_CHECK_EQUAL(array8.size(), (std::size_t)7) ;
    CheckEigenVectors(vec2.tail(7), array8) ;
    Array<double, 1, true> array9(array5(Slice(Slice::START, Slice::END))) ;
    BOOST_CHECK_EQUAL(array9.size(), (std::size_t)10) ;
    CheckEigenVectors(vec2, array9) ;
    Array<double, 1, true> array10(array5(Slice(Slice::START, 1))) ;
    BOOST_CHECK_EQUAL(array10.size(), (std::size_t)2) ;
    CheckEigenVectors(vec2.head(2), array10) ;
    // Finally test the data setter on a view
    //double data2[2] ;
    //data2[0] = 10. ;
    //data2[1] = 20. ;
    //std::array<std::size_t, 1> shapeTmp {{2}} ;
    //array7.set(data2, shapeTmp, false) ;
    //BOOST_CHECK_SMALL(array7(0)-10., 1.e-8) ;
    //BOOST_CHECK_SMALL(array7(1)-20., 1.e-8) ;
    //BOOST_CHECK_SMALL(array7(0)-10., 1.e-8) ;
    //BOOST_CHECK_SMALL(array7(1)-20., 1.e-8) ;
    //BOOST_CHECK_SMALL(array6(1)-10., 1.e-8) ;
    //BOOST_CHECK_SMALL(array6(2)-20., 1.e-8) ;
    //BOOST_CHECK_SMALL(array5(2)-10., 1.e-8) ;
    //BOOST_CHECK_SMALL(array5(3)-20., 1.e-8) ;
}

void testArray2(void)
{
    Array<double, 2> array(10, 5) ;
    BOOST_CHECK_EQUAL(array.size(), (std::size_t)50) ;
    Shape<2> shape(array.shape()) ;
    BOOST_CHECK_EQUAL(shape[0], 10) ;
    BOOST_CHECK_EQUAL(shape[1], 5) ;
    // Test Eigen::MatrixXd assigment
    Eigen::MatrixXd mat(Eigen::MatrixXd::Ones(10, 5)) ;
    array = mat ;
    CheckEigenMatrix(array, mat) ;
    // Test copy constructor
    Array<double, 2> array3(array) ;
    CheckEigenMatrix(array3, array) ;
    // Test array conversion to Eigen::MatrixXd
    typename Array<double, 2>::EigenType test(array3) ;
    //Eigen::MatrixXd test2(array3) ;
    // Try to change test and see if it changes array3
    test.col(1) = Eigen::VectorXd::Constant(10, 2.) ;
    CheckEigenMatrix(array3, test) ;
    // Test data method
    double * data(array3.data()) ;
    BOOST_CHECK_SMALL(data[0]-1., 1.e-8) ;
    // Test assignement operator
    Array<double, 2> array4(10, 5) ;
    array4 = array3 ;
    //CheckEigenMatrix(array3, array4) ;
    // Test Eigen matrix assignement
    Eigen::MatrixXd mat2(10, 5) ;
    for (std::size_t i=0; i<10; ++i)
    {
        for (std::size_t j=0; j<5; ++j)
        {
            mat2(i, j) = (double)(i*5+j+1) ;
        }
    }
    Array<double, 2> array5(10, 5) ;
    array5 = mat2 ;
    CheckEigenMatrix(mat2, array5) ;
    // Test accessing data
    double & val(array5(3, 2)) ;
    BOOST_CHECK_SMALL(val-18., 1.e-8) ;
    val += 1 ;
    BOOST_CHECK_SMALL(array5(3, 2)-19., 1.e-8) ;
    val -= 1 ;
    BOOST_CHECK_SMALL(array5(3, 2)-18., 1.e-8) ;
    //// Test accessing a row
    Array<double, 1, true> row3(array5(3)) ;
    BOOST_CHECK_EQUAL(row3.size(), (std::size_t)5) ;
    Eigen::VectorXd rowMat(mat2.row(3)) ;
    CheckEigenVectors(rowMat, row3) ;
    // Test accessing a block of all columns
    Array<double, 2, true> block(array5(Slice(3, 6))) ;
    Eigen::MatrixXd blockMat(mat2.block(3, 0, 4, 5)) ;
    CheckEigenMatrix(blockMat, block) ;
    // Test accessing a block column
    Array<double, 1, true> block2(array5(Slice(3, 6), 2)) ;
    Eigen::VectorXd blockMat2(mat2.block(3, 2, 4, 1)) ;
    CheckEigenVectors(blockMat2, block2) ;
    // Test accessing a block row
    Array<double, 1, true> block3(array5(3, Slice(1, 3))) ;
    BOOST_CHECK_EQUAL(block3.size(), (std::size_t)3) ;
    Eigen::VectorXd blockMat3(mat2.block(3, 1, 1, 3).transpose()) ;
    CheckEigenVectors(blockMat3, block3) ;
    // Test accessing data with slices
    Array<double, 2, true> block4(array5(Slice(3, 6), Slice(1, 3))) ;
    BOOST_CHECK_EQUAL(block4.size(), (std::size_t)12) ;
    BOOST_CHECK_EQUAL(block4.shape()[0], 4) ;
    BOOST_CHECK_EQUAL(block4.shape()[1], 3) ;
    CheckEigenMatrix(block4, mat2.block(3, 1, 4, 3)) ;
    // Test accessing data with slices and step
    Array<double, 2, true> block4_2(array5(Slice(3, 6, 2), Slice(1, 3))) ;
    BOOST_CHECK_EQUAL(block4_2.size(), (std::size_t)6) ;
    BOOST_CHECK_EQUAL(block4_2.shape()[0], 2) ;
    BOOST_CHECK_EQUAL(block4_2.shape()[1], 3) ;
    // Test a view of a view
    Array<double, 2, true> block5(block4(Slice(1, 2), Slice(1, 2))) ;
    BOOST_CHECK_EQUAL(block5.size(), (std::size_t)4) ;
    BOOST_CHECK_EQUAL(block5.shape()[0], 2) ;
    BOOST_CHECK_EQUAL(block5.shape()[1], 2) ;
    CheckEigenMatrix(mat2.block(4, 2, 2, 2), block5) ;
    Array<double, 1, true> block6(block4(Slice(1, 2), 1)) ;
    BOOST_CHECK_EQUAL(block6.size(), (std::size_t)2) ;
    BOOST_CHECK_EQUAL(block6.shape()[0], 2) ;
    Eigen::VectorXd blockMat4(mat2.block(4, 2, 2, 1)) ;
    CheckEigenVectors(blockMat4, block6) ;
    Array<double, 1, true> block7(block4(1, Slice(1, 2))) ;
    BOOST_CHECK_EQUAL(block7.size(), (std::size_t)2) ;
    BOOST_CHECK_EQUAL(block7.shape()[0], 2) ;
    Eigen::VectorXd blockMat5(mat2.block(4, 2, 1, 2).transpose()) ;
    CheckEigenVectors(blockMat5, block7) ;
    double & val2(block4(2, 1)) ;
    BOOST_CHECK_SMALL(val2-28., 1.e-8) ;
    // Test some eigen operations, even on views !
    Eigen::Vector2d test3(block7 + block6) ;
    CheckEigenVectors(blockMat5+blockMat4, test3) ;
    double test4(block7.dot(block6)) ;
    BOOST_CHECK_SMALL(blockMat5.dot(blockMat4) - test4, 1.e-8) ;
}

void testArray3(void)
{
    {
        // Creation of a 3D array
        Array<double, 3> array(3, 3, 4) ;
        BOOST_CHECK_EQUAL(array.size(), (std::size_t)(3*3*4)) ;
        Shape<3> shape(array.shape()) ;
        BOOST_CHECK_EQUAL(shape[0], 3) ;
        BOOST_CHECK_EQUAL(shape[1], 3) ;
        BOOST_CHECK_EQUAL(shape[2], 4) ;
        Eigen::MatrixXd mat(Eigen::MatrixXd::Ones(3, 4)) ;
        for (int i=0; i<3; ++i)
        {
            array(i) = mat ;
        }
        Array<double, 3> array2(array) ;
        //std::cout << array(Slice(Slice::START, Slice::END), 0, Slice(Slice::START, Slice::END)) << std::endl ;
        //std::cout << array(0) + array(Slice(Slice::START, Slice::END), 0, Slice(Slice::START, Slice::END)) << std::endl ;
        //std::cout << array(0) - array(Slice(Slice::START, Slice::END), 0, Slice(Slice::START, Slice::END)) / 2. << std::endl ;
        auto T2(array(1)) ;
        //std::cout << T2 << std::endl ;
        T2.fill(2.5) ;
        //std::cout << array << std::endl ;
        Array<double, 1, true> T1(array(1, 1)) ;
        //std::cout << T1 << std::endl ;
        Array<double, 0, true> T0(array(1, 1, 1)) ;
        //std::cout << T0 << std::endl ;
    }
}

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV
