#ifndef BV_Tools_Array_Tests_Storage_hpp
#define BV_Tools_Array_Tests_Storage_hpp

namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

void testStorage1D(void) ;
void testStorage2D(void) ;

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Tests_Storage_hpp
