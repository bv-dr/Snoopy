#pragma once
#ifndef BV_Tools_Array_Tests_Tools_hpp
#define BV_Tools_Array_Tests_Tools_hpp

#include <boost/test/unit_test.hpp>

#include <Eigen/Dense>

namespace BV {
namespace Tools {
namespace Array {
namespace Tests {

template <typename Derived, typename Derived2>
void CheckEigenMatrix(const Eigen::MatrixBase<Derived> & m1,
                      const Eigen::MatrixBase<Derived2> & m2,
                      const double epsilon=1.e-9)
{
    for (unsigned i=0; i<m1.rows(); ++i)
    {
        for (unsigned j=0; j<m1.cols(); ++j)
        {
            BOOST_CHECK_SMALL(m1(i, j)-m2(i, j), epsilon) ;
        }
    }
}

//void CheckEigenMatrix(const Eigen::Ref<const Eigen::MatrixXd> & m1,
//                      const Eigen::Ref<const Eigen::MatrixXd> & m2,
//                      const double epsilon=1.e-9) ;

void CheckEigenVectors(const Eigen::Ref<const Eigen::VectorXd> & v1,
                       const Eigen::Ref<const Eigen::VectorXd> & v2,
                       const double epsilon=1.e-9) ;

void CheckArrays(const double * ar1, const double * ar2,
                 const std::size_t & size,
                 const double epsilon=1.e-9) ;

} // End of namespace Tests
} // End of namespace Array
} // End of namespace Tools
} // End of namespace BV

#endif // BV_Tools_Array_Tests_Tools_hpp
