from Snoopy import Fatigue as ft
import numpy as np

def compare_to_pluto():
    from Snoopy import Spectral as sp
    from Snoopy import TimeDomain as td
    from Pluto.TimeDomain import rainflow

    wif = sp.Wif.Jonswap(1,10,1, wifArgs = {"seed" : 16289})  # 18->Cond3, 15->Cond1, 30->Cond2, 16289->Cond4  All ok
    ts = td.ReconstructionWifLocal(wif)(np.arange(0,10800,0.5))

    cycleSnoopy = ft.Rainflow(signal = ts)()
    cyclePluto = rainflow(ts)

    #Compare Snoopy and Pluto
    if (np.isclose( cyclePluto, cycleSnoopy ).all()) :
        print("Ok")
    else:
        print("Problem")


if __name__ == "__main__" :
    compare_to_pluto()
