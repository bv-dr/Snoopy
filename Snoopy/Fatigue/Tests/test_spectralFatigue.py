from Snoopy import Fatigue as ft
import numpy as np
from scipy.stats import rayleigh

def test_sht() :
    data = np.array( [[53.4, 3,  1.520E12],
                      [0.1, 5, 4.330E15],], dtype = float )

    sn = ft.SnCurve(data)

    #StarSpec data
    rs, tz, fatigueLife_ref = 3.00500E+01, 7.67900E+00,7.42000E+01

    #Snoopy calculation
    fatigueLife = sn.fatigueLife_from_RSRTZ( rs, tz)

    rangeDist = rayleigh(0, rs/2  )
    fatigueLifeNum = sn.fatigueLife_from_distribution( rangeDist.pdf, rtz = tz)

    print ("Snoopy:",  fatigueLife)
    print ("Snoopy num:",  fatigueLifeNum)
    print ("StarSpec:",  fatigueLife_ref)

    assert(  np.isclose( fatigueLife, fatigueLifeNum, rtol = 0.001 ) )
    assert(  np.isclose( fatigueLife, fatigueLife_ref, rtol = 0.01 ) )


if __name__ == "__main__" :
    test_sht()
