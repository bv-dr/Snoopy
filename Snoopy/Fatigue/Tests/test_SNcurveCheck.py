# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 18:08:46 2019

@author: fbigot
"""

from Snoopy import Fatigue as ft
import numpy as np

if __name__ == "__main__" :
    data = np.array( [[0.01,  5, 4.330E15],
                       [50.,  3, 1.520E12]], dtype = float )

    sn = ft.SnCurve(data)
