# -*- coding: utf-8 -*-
"""
Created on Thu Apr 19

@author: fbigot
"""

from Snoopy import Fatigue as ft
import numpy as np

if __name__ == "__main__" :
    data = np.array( [[0.01,  5, 4.330E15],
                       [53.4,  3, 1.520E12],
                       [533.7, 5, 4.329E17]], dtype = float )

    sn = ft.SnCurve(data)
    
    # reference values computed with Excel
    refDam1 = 0.002309469
    refDam2 = 6.578947368
    refDam3 = 2.309904474
    
    dam1 = sn.ConstantAmplitudeDamage(N=1e8,DS=10.)
    print("Snoopy dammage for 1.e8 cycles of 10. MPa:"+str(dam1))
    print("ref.   dammage for 1.e8 cycles of 10. MPa:"+str(refDam1))
    dam2 = sn.ConstantAmplitudeDamage(N=1e7,DS=100.)
    print("Snoopy dammage for 1.e7 cycles of 100. MPa:"+str(dam2))
    print("ref.   dammage for 1.e7 cycles of 100. MPa:"+str(refDam2))
    dam3 = sn.ConstantAmplitudeDamage(N=1e3,DS=1000.)
    print("Snoopy dammage for 1.e3 cycles of 1000. MPa:"+str(dam3))
    print("ref.   dammage for 1.e3 cycles of 1000. MPa:"+str(refDam3))
