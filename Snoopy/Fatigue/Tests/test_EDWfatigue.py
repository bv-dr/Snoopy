from Snoopy import Fatigue as ft
import numpy as np

def test_EDWfatigue(ksi=2.):
    """ Test SN_curve.damage_from_multiple_EDW
    Generate stress ranges at several probability levels from a Weibull
    Compare the damage with the one obtained by .damage_from_weibull
    """
    # SN curve
    data = np.array([[0.01, 5, 4.330E15],
                     [53.4, 3, 1.520E12]], dtype=float )
    sn = ft.SnCurve(data)

    # Tested Weibull
    Sref = 100.
    Pref = 0.01
    N = 1.e8
    lmbda = Sref / (np.log(1./Pref))**(1./ksi)

    # using .damage_from_weibull as a ref
    refdam = sn.damage_from_weibull(N, Sref, Pref, ksi)
    # coarse discretization
    probas = np.array([1., 0.1, 0.01, 0.001, 0.0001, 0.00001])
    ranges = lmbda * (np.log(1./probas))**(1./ksi)
    dam1 = sn.damage_from_multiple_EDW(ranges, probas, N)
    # medium discretization
    probas = np.power(10, np.arange(0., -5.01, -0.5))
    ranges = lmbda * (np.log(1./probas))**(1./ksi)
    dam2 = sn.damage_from_multiple_EDW(ranges, probas, N)
    # fine discretization
    probas = np.power(10, np.arange(0., -5.01, -0.1))
    ranges = lmbda * (np.log(1./probas))**(1./ksi)
    dam3 = sn.damage_from_multiple_EDW(ranges, probas, N)

    print("Test with ksi=", ksi)
    print("multi EDW LT dammage ( 5 EDW):"+str(dam1))
    print("multi EDW LT dammage (10 EDW):"+str(dam2))
    print("multi EDW LT dammage (50 EDW):"+str(dam3))
    print("ref.      LT dammage         :"+str(refdam))
    assert(np.isclose(dam3, refdam, rtol=0.01))


if __name__ == "__main__":
    test_EDWfatigue(ksi=2.)
    test_EDWfatigue(ksi=1.2)
