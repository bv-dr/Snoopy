from Snoopy import Fatigue as ft
import numpy as np

def test_Weibull() :
    
    from scipy.stats import weibull_min
    
    data = np.array( [[0.01, 5, 4.330E15],
                      [53.4, 3, 1.520E12]], dtype = float )

    sn = ft.SnCurve(data)

    # reference values have been computed with structure\CrackPy\SN_approach.py, and crosschecked with Snoopy
    refDam1 = 3.2567675235663542
    refDam2 = 3.623039836192742
    refDam3 = 4.274001799245581
    
    dam1 = sn.damage_from_weibull(1.e8,100.,0.01,0.8)
    print("Snoopy LT dammage ksi 0.8:"+str(dam1))
    print("ref.   LT dammage ksi 0.8:"+str(refDam1))
    dam2 = sn.damage_from_weibull(1.e8,100.,0.01,1.)
    print("Snoopy LT dammage ksi 1.0:"+str(dam2))
    print("ref.   LT dammage ksi 1.0:"+str(refDam2))
    dam3 = sn.damage_from_weibull(1.e8,100.,0.01,1.2)
    print("Snoopy LT dammage ksi 1.2:"+str(dam3))
    print("ref.   LT dammage ksi 1.2:"+str(refDam3))

    assert(  np.isclose( dam1, refDam1, rtol = 0.001 ) )
    assert(  np.isclose( dam2, refDam2, rtol = 0.001 ) )
    assert(  np.isclose( dam3, refDam3, rtol = 0.001 ) )


    # check that damage_from_weibull and numerical integration with weibull_min are consistent:
    def weibull_xn_to_scale(x_n , n , b ):
        return x_n / np.log( n )**(1/b)
    ds_ref = 130.
    p_ref = 1e-2
    ksi = 1.1
    nb_cycles = 1e8
    scale  = weibull_xn_to_scale(ds_ref, 1/p_ref , ksi)
    dam4 = sn.damage_from_distribution( weibull_min( ksi , 0.0 , scale ).pdf , nb_cycles)
    dam4_check = sn.damage_from_weibull(  nb_cycles, ds_ref, p_ref, ksi)
    assert(np.isclose( dam4, dam4_check , rtol = 1e-3))
    
    


if __name__ == "__main__" :
    test_Weibull()
