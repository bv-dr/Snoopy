#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <pybind11/functional.h>

#include "Spectral/Wif.hpp"
#include "Spectral/Rao.hpp"
#include "Spectral/Qtf0.hpp"
#include "Spectral/MQtf.hpp"

// WaveKinematic related headers
#include "TimeDomain/Reconstruction.hpp"
#include "TimeDomain/ReconstructionRao.hpp"
#include "TimeDomain/ReconstructionQtf.hpp"
#include "TimeDomain/ReconstructionMQtf.hpp"
#include "TimeDomain/ReconstructionQtf0_BV.hpp"
#include "TimeDomain/ReconstructionQtf_WaveCurrentInteraction.hpp"
#include "TimeDomain/ReconstructionQtf_WaveCurrentInteractionKim.hpp"
#include "TimeDomain/ReconstructionQtf0_WaveCurrentInteraction.hpp"
#include "TimeDomain/Rainflow.hpp"
#include "TimeDomain/Radiation/RetardationParameters.hpp"
#include "TimeDomain/Radiation/RetardationFunction.hpp"
#include "TimeDomain/Radiation/RetardationFunctionsHistory.hpp"
#include "TimeDomain/Radiation/VelocityHistory.hpp"
#include "TimeDomain/Radiation/Radiation.hpp"
#include "Tools/TensorPythonCaster.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;
using namespace BV::TimeDomain;

PYBIND11_MODULE(_TimeDomain, m)
{
    m.doc() = "Time domain module";

    py::module::import("_Spectral") ; // For enums to be accessible

	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    // Time signal processing
    m.def("cptrf1", &cptrf1, "Rainflow counting, return ixtrt such as (extrema(i), extrema(ixtrt(i)) is an half cycle", "extrema"_a);

    m.def("getExtrema", &getExtrema, R"rst(Extract extrema from signal)rst", "signal"_a);


    // Reconstruction from Wif
    py::class_<ReconstructionABC> reconstructionABC(m, "ReconstructionABC");
    reconstructionABC
        .def("getWif", &ReconstructionABC::getWif)
        .def("getModes", &ReconstructionABC::getModes) 
        ;

    py::class_<ReconstructionWif>(m, "ReconstructionWif", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, unsigned int>(), py::keep_alive<1, 2>(), "wif"_a, "numThreads"_a = 1)
        .def("__call__", py::vectorize(py::overload_cast<double, double, double>(&ReconstructionWif::operator(), py::const_)), "time"_a, "xGlobal"_a=0., "yGlobal"_a=0.)
        .def("parallel_call", py::overload_cast<const Eigen::ArrayXd &, const Eigen::ArrayXd &, const Eigen::ArrayXd &>(&ReconstructionWif::operator(), py::const_), "time"_a, "xGlobal"_a, "yGlobal"_a)
        .def("evalProjected", &ReconstructionWif::evalProjected)
        ;

    py::class_<ReconstructionWifLocal>(m, "ReconstructionWifLocal", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, double, unsigned int>(), py::keep_alive<1, 2>(), "wif"_a, "speed"_a = 0.0, "numThreads"_a = 1)
        .def("__call__", py::overload_cast<double>(&ReconstructionWifLocal::operator(), py::const_), "time"_a)
        .def("__call__", py::overload_cast<const Eigen::ArrayXd &>(&ReconstructionWifLocal::operator(), py::const_), "timeVect"_a)
        .def("__call__", py::vectorize(py::overload_cast<double,double, double>(&ReconstructionWifLocal::operator(), py::const_)), "time"_a , "x"_a , "y"_a)
        ;

    py::class_<ReconstructionRao>(m, "ReconstructionRao", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, const Rao&,
            const BV::Math::Interpolators::InterpScheme&,
            const BV::Spectral::ComplexInterpolationStrategies&,
            BV::Math::Interpolators::ExtrapolationType, unsigned int, bool>(), 
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Rao"_a,
            "InterpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "ComplexInterpolationStrategies"_a = BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
            "ExtrapolationType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "numThreads"_a = 1,
            "forceRun"_a = false,
            pybind11::call_guard<pybind11::gil_scoped_release>())

        .def("getData", &ReconstructionRao::getData)
        .def("__call__", py::overload_cast<double>(&ReconstructionRao::operator(), py::const_), "azimuth"_a)
        .def("__call__", py::overload_cast<double, double, double, double>(&ReconstructionRao::operator(), py::const_), "time"_a, "xGlobal"_a = 0., "yGlobal"_a = 0., "azimuth"_a)
        .def("__call__", py::overload_cast<double, const Eigen::Vector3d&>(&ReconstructionRao::operator(), py::const_), "time"_a, "positionAndAzimuth"_a)
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd&>(&ReconstructionRao::operator(), py::const_), "time"_a, "xGlobal"_a, "yGlobal"_a, "azimuth"_a, pybind11::call_guard<pybind11::gil_scoped_release>())
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionRao::operator(), py::const_), pybind11::call_guard<pybind11::gil_scoped_release>())
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayX3d&>(&ReconstructionRao::operator(), py::const_), "time"_a, "positionAndAzimuth"_a, pybind11::call_guard<pybind11::gil_scoped_release>())
        ;

    py::class_<ReconstructionRaoLocal>(m, "ReconstructionRaoLocal", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, const Rao&, const BV::Math::Interpolators::InterpScheme&, 
            const BV::Spectral::ComplexInterpolationStrategies&, BV::Math::Interpolators::ExtrapolationType, unsigned int, bool >(),
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Rao"_a,
            "InterpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "ComplexInterpolationStrategies"_a = BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
            "ExtrapolationType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "numThreads"_a = 1, 
            "forceRun"_a = false,
            pybind11::call_guard<pybind11::gil_scoped_release>())

        .def("getFourierCoefficients", &ReconstructionRaoLocal::getFourierCoefficients)
        .def("getData", &ReconstructionRaoLocal::getData)
        .def("__call__", py::overload_cast<>(&ReconstructionRaoLocal::operator(), py::const_))
        .def("__call__", py::overload_cast<double>(&ReconstructionRaoLocal::operator(), py::const_), "time"_a)
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionRaoLocal::operator(), py::const_), "timeInstants"_a, pybind11::call_guard<pybind11::gil_scoped_release>())
        ;
    
    py::class_<ReconstructionQtf>(m, "ReconstructionQtf", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, const Qtf &,
            const BV::Math::Interpolators::InterpScheme &,
            const BV::Spectral::ComplexInterpolationStrategies &,
            const BV::Spectral::FrequencyInterpolationStrategies &,
            const BV::Math::Interpolators::ExtrapolationType &, double, unsigned int>(),
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Qtf"_a,
            "interpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "interpStrategy"_a = BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
            "frequencyInterpStrategy"_a = BV::Spectral::FrequencyInterpolationStrategies::W_DW,
            "extrapType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "dwMax"_a = 1e6,
            "numThreads"_a = 1)
        .def("getQtf", &ReconstructionQtf::getQtf)
        .def("getDwMax", &ReconstructionQtf::getDwMax)
        .def("__call__", py::overload_cast<double>(&ReconstructionQtf::operator(), py::const_))
        .def("__call__", py::overload_cast<double, double, double, double>(&ReconstructionQtf::operator(), py::const_))
        .def("__call__", py::overload_cast<double, const Eigen::Vector3d &>(&ReconstructionQtf::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd&>(&ReconstructionQtf::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionQtf::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayX3d&>(&ReconstructionQtf::operator(), py::const_))
        ;

    py::class_<ReconstructionQtf_WaveCurrentInteraction>(m, "ReconstructionQtf_WaveCurrentInteraction", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, const Qtf &,
            const BV::Math::Interpolators::InterpScheme &,
            const BV::Spectral::ComplexInterpolationStrategies &,
            const BV::Spectral::FrequencyInterpolationStrategies &,
            const BV::Math::Interpolators::ExtrapolationType &, double, unsigned int>(),
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Qtf"_a,
            "interpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "interpStrategy"_a = BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
            "frequencyInterpStrategy"_a = BV::Spectral::FrequencyInterpolationStrategies::W_DW,
            "extrapType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "dwMax"_a = 1e6,
            "numThreads"_a = 1)
        .def("getQtf", &ReconstructionQtf_WaveCurrentInteraction::getQtf)
        .def("getDwMax", &ReconstructionQtf_WaveCurrentInteraction::getDwMax)
        .def("__call__", py::overload_cast<double, double, double>(&ReconstructionQtf_WaveCurrentInteraction::operator(), py::const_))
        .def("__call__", py::overload_cast<double, double, double, double, double, double>(&ReconstructionQtf_WaveCurrentInteraction::operator(), py::const_))
        .def("__call__", py::overload_cast<double, const Eigen::Vector3d &, const Eigen::Vector2d &>(&ReconstructionQtf_WaveCurrentInteraction::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd&, const Eigen::ArrayXd &, const Eigen::ArrayXd &>(&ReconstructionQtf_WaveCurrentInteraction::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionQtf_WaveCurrentInteraction::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayX3d&, const Eigen::ArrayX2d &>(&ReconstructionQtf_WaveCurrentInteraction::operator(), py::const_))
        ;

    py::class_<ReconstructionQtf_WaveCurrentInteractionKim,
               ReconstructionQtf_WaveCurrentInteraction>(m, "ReconstructionQtf_WaveCurrentInteractionKim")
        .def(py::init<std::shared_ptr<const Wif>, const Qtf &,
            const BV::Math::Interpolators::InterpScheme &,
            const BV::Spectral::ComplexInterpolationStrategies &,
            const BV::Spectral::FrequencyInterpolationStrategies &,
            const BV::Math::Interpolators::ExtrapolationType &, double, unsigned int>(),
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Qtf"_a,
            "interpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "interpStrategy"_a = BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
            "frequencyInterpStrategy"_a = BV::Spectral::FrequencyInterpolationStrategies::W_DW,
            "extrapType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "dwMax"_a = 1e6,
            "numThreads"_a = 1)
        .def("__call__", py::overload_cast<double, double, double, double, double, double>(&ReconstructionQtf_WaveCurrentInteractionKim::operator(), py::const_))
        ;

    py::class_<ReconstructionQtfLocal>(m, "ReconstructionQtfLocal", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>, const Qtf &,
             const BV::Math::Interpolators::InterpScheme &,
             const BV::Spectral::ComplexInterpolationStrategies &,
             const BV::Spectral::FrequencyInterpolationStrategies &,
             const BV::Math::Interpolators::ExtrapolationType &, double, unsigned int>(),
             py::keep_alive<1, 2>(),
             py::keep_alive<1, 3>(),
             "wif"_a, "Qtf"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
             "frequencyInterpStrategy"_a=BV::Spectral::FrequencyInterpolationStrategies::W_DW,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             "dwMax"_a = 1e6,
             "numThreads"_a=1)
        .def("getQtf", &ReconstructionQtfLocal::getQtf)
        .def("getDwMax", &ReconstructionQtfLocal::getDwMax)
        
        .def("__call__", py::overload_cast<>(&ReconstructionQtfLocal::operator(), py::const_))
        .def("__call__", py::overload_cast<double>(&ReconstructionQtfLocal::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionQtfLocal::operator(), py::const_))
        ;

    py::class_<ReconstructionQtf0_BVLocal>(m, "ReconstructionQtf0_BVLocal")
        .def(py::init<std::shared_ptr<const Wif>, const Qtf0 &,
            const BV::Math::Interpolators::InterpScheme &,
            BV::Math::Interpolators::ExtrapolationType, unsigned int>() ,
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Qtf"_a,
            "interpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "extrapType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "numThreads"_a = 1)
        .def("__call__", py::overload_cast<>(&ReconstructionQtf0_BVLocal::operator(), py::const_))
        .def("__call__", py::overload_cast<double>(&ReconstructionQtf0_BVLocal::operator(), py::const_))
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionQtf0_BVLocal::operator(), py::const_))
        ;

    py::class_<ReconstructionQtf0_BV>(m, "ReconstructionQtf0_BV")
        .def(py::init<std::shared_ptr<const Wif>, const Qtf0 &,
            const BV::Math::Interpolators::InterpScheme &,
            BV::Math::Interpolators::ExtrapolationType, unsigned int>(),
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
           "wif"_a, "Qtf"_a,
            "interpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "extrapType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "numThreads"_a = 1)
        .def("__call__", py::overload_cast<double>(&ReconstructionQtf0_BV::operator(), py::const_))
        .def("__call__", py::overload_cast<double, double, double, double>(&ReconstructionQtf0_BV::operator(), py::const_))
        ;

    py::class_<ReconstructionQtf0_WaveCurrentInteraction>(m, "ReconstructionQtf0_WaveCurrentInteraction")
        .def(py::init<std::shared_ptr<const Wif>, const Qtf0 &,
            const BV::Math::Interpolators::InterpScheme &,
            BV::Math::Interpolators::ExtrapolationType, unsigned int>(),
            py::keep_alive<1, 2>(),
            py::keep_alive<1, 3>(),
            "wif"_a, "Qtf"_a,
            "interpScheme"_a = BV::Math::Interpolators::InterpScheme::LINEAR,
            "extrapType"_a = BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
            "numThreads"_a = 1)
        .def("__call__", py::overload_cast<double, double, double>(&ReconstructionQtf0_WaveCurrentInteraction::operator(), py::const_))
        .def("__call__", py::overload_cast<double, double, double, double, double, double>(&ReconstructionQtf0_WaveCurrentInteraction::operator(), py::const_))
        ;

    py::class_<ReconstructionMQtfLocal>(m, "ReconstructionMQtfLocal", reconstructionABC)
        .def(py::init<std::shared_ptr<const Wif>,
             const MQtf &,
             const int & ,
             const BV::Spectral::ComplexInterpolationStrategies &,
             const double &,
             unsigned int>(),
             "wif"_a,
             "mqtf"_a,
             "degree"_a = 1,
             "interpStrategy"_a = BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
             "dwMax"_a = 1.e6,
             "numThreads"_a = 1)
        .def("getQtf", &ReconstructionMQtfLocal::getQtf)
        .def("getDwMax", &ReconstructionMQtfLocal::getDwMax)
        .def("__call__", py::overload_cast<double>(&ReconstructionMQtfLocal::operator(), py::const_), "time"_a)
        .def("__call__", py::overload_cast<const Eigen::ArrayXd&>(&ReconstructionMQtfLocal::operator(), py::const_))
        ;

    py::class_<RetardationFunction>(m, "RetardationFunction")
        .def(py::init<const unsigned &, const double &>(),
             "extrapolationType"_a, "wInf"_a)
        .def("get", &RetardationFunction::get)
    ;

    py::class_<RetardationParameters>(m, "RetardationParameters")
        .def(py::init<const double &, const double & , const double &,
                      const double &, const int &>(), "trad"_a, "dt"_a, "wCut"_a, "wInf"_a, "extrapolationType"_a = 1)
        .def("getRetardationDuration", &RetardationParameters::getRetardationDuration)
        .def("getRetardationTimeStep", &RetardationParameters::getRetardationTimeStep)
        .def("getCutFrequency", &RetardationParameters::getCutFrequency)
        .def("getInfiniteFrequency", &RetardationParameters::getInfiniteFrequency)
        .def("getExtrapolationType", &RetardationParameters::getExtrapolationType)
    ;

    py::class_<RetardationFunctionsHistory>(m, "RetardationFunctionsHistory")
        .def(py::init<const Eigen::Ref<const Eigen::VectorXd> &,
                      const Eigen::Tensor<double, 3> &,
                      const RetardationParameters &>(), "frequencies"_a, "waveDaming"_a, "parameters"_a)
        .def("getHistory", &RetardationFunctionsHistory::getHistory , "maxTime"_a)
        .def("getTimeInstants", &RetardationFunctionsHistory::getTimeInstants)
        .def("getFrequencies", &RetardationFunctionsHistory::getFrequencies)
        .def("getDamping", &RetardationFunctionsHistory::getDamping)
        .def("reComputeDamping", &RetardationFunctionsHistory::reComputeDamping , "frequencies"_a)
        .def("reComputeAddedMass", &RetardationFunctionsHistory::reComputeAddedMass , "frequencies"_a, "infiniteAddedMass"_a)
        .def("getRetardationDuration", &RetardationFunctionsHistory::getRetardationDuration)
        .def("getRetardationTimeStep", &RetardationFunctionsHistory::getRetardationTimeStep)
    ;

    py::class_<VelocityHistory>(m, "VelocityHistory")
        .def(py::init<const double &, const double &, const Eigen::Index &,
                      Eigen::Index>(),
             "retardationTimeStep"_a, "retardationDuration"_a, "nModes"_a,
             "historySizeFactor"_a=10)
        .def("add", &VelocityHistory::add)
        .def("get", &VelocityHistory::get)
    ;

    py::class_<Radiation>(m, "Radiation")
        .def(py::init<const RetardationFunctionsHistory &>())
        .def("setVelocity", &Radiation::setVelocity , "time"_a , "velocity"_a)
        .def("get", &Radiation::get , "time"_a)
    ;

    m.def("Convolution", py::overload_cast<
                         const Eigen::Tensor<double, 3> &,
                         const Eigen::Ref<const Eigen::MatrixXd> &,
                         const double &
                                          >(&Convolution)) ;

    m.def("Convolution", py::overload_cast<
                         const Eigen::Tensor<double, 3> &,
                         const Eigen::Ref<const Eigen::MatrixXd> &,
                         const Eigen::Ref<const Eigen::VectorXd> &
                                          >(&Convolution)) ;

}
