import numpy as np
from matplotlib import pyplot as plt
from Snoopy import Spectral as sp
from Snoopy import TimeDomain as td

if __name__ == "__main__" : 

    ss = sp.SeaState.Jonswap(1.0 , 10.0 , 1.0 , 0.0)
    
    wif = sp.Wif(ss)
    
    ts =      td.ReconstructionWif(wif).evalSe( np.arange(0, 100000, 0.2) )
    ts_long = td.ReconstructionWif(wif).evalSe( np.arange(0, 1000000, 0.2) )


    dw = 0.02
    psd = td.getPSD(ts, dw = dw, ci_level = 0.95)
    psd_long = td.getPSD(ts_long, dw = dw, ci_level = 0.95)
 
    
    fig, ax = plt.subplots()
    psd.iloc[:,0].plot(ax=ax)
    ax.fill_between( psd.index.values , psd.iloc[:,1].values , psd.iloc[:,2].values, alpha = 0.3)
    ax.set(xlim = [0.2 , 1.8] )
    ss.plot(ax=ax)
    
    
    

    psd_long.iloc[:,0].plot(ax=ax, color = "red")
    ax.fill_between( psd_long.index.values , psd_long.iloc[:,1].values , psd_long.iloc[:,2].values, alpha = 0.3, color ="red")
    ax.set(xlim = [0.2 , 1.8] )
    


