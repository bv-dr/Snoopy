from Snoopy import logger
from Snoopy import TimeDomain as td
from Snoopy import Spectral as sp
import numpy as np



def test_reconstructionRaoPerf():
    """Test reconstructor performances
    """

    spec = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.pi)
    ss = sp.SeaState( spec )

    wif = sp.Wif( ss , w = np.arange(0.1,2.0, 0.005), seed = 15)
    wifm = sp.Wifm([wif, wif], [ [0,500], [500,1000] ])
    time = np.arange(0, 1000, 0.1 )
    rao = sp.Rao( "../../Spectral/Tests/test_data/my_10_0speed.rao" ).getSorted()
    xwav, ywav = rao.getWaveReferencePoint()
    xref, yref, zref = rao.getReferencePoint()

    logger.debug("Reconstruction local... START")
    recLoc  = td.ReconstructionRaoLocal( wif, rao )
    loc_ts = recLoc( time )
    logger.debug("Reconstruction local done STOP")

    logger.debug("Reconstruction local FFT ... START")
    recLocFFT  = td.ReconstructionRaoLocalFFT.WithBackup(wif, rao, dt_approx=0.1)
    locFFT_ts = recLocFFT( time )
    logger.debug("Reconstruction local FFT done STOP")

    logger.debug("Reconstruction... START")
    recGlob = td.ReconstructionRao( wif, rao  )
    glob_ts = recGlob( time, np.full(time.shape, xref-xwav) , np.full(time.shape, yref-ywav), np.full(time.shape, 0.) )
    logger.debug("Reconstruction done STOP")

    logger.debug("Reconstruction multi... START")
    recGlobMulti = td.ReconstructionMulti( td.ReconstructionRao, wifm, rao )
    glob_ts_multi = recGlobMulti( time, np.full(time.shape, xref-xwav) , np.full(time.shape, yref-ywav), np.full(time.shape, 0.) )
    logger.debug("Reconstruction multi done STOP")


    assert( np.isclose( loc_ts, glob_ts, rtol = 1e-2 ).all() )
    assert( np.isclose( locFFT_ts, loc_ts, rtol = 1e-2 ).all() )
    assert( np.isclose( glob_ts_multi, glob_ts, rtol = 1e-2 ).all() )



if __name__ == "__main__" :
    logger.setLevel(10)
    test_reconstructionRaoPerf()
