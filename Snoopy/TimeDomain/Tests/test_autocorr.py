
from Snoopy import Spectral as sp
from Snoopy import TimeDomain as td
from Snoopy import WaveKinematic as wk
from matplotlib import pyplot as plt
import numpy as np

def test_autocorr(display = False):
    """Test autocorrelation routine.
    
    Auto-correlation of wave elevation should be equal to the new wave
    """
    
    ss = sp.SeaState.Jonswap(1,10,1.5 , np.pi) 
    wif = sp.Wif(ss, nbSeed = 100, wmax = 2)
    ts = td.ReconstructionWif(wif).evalSe(np.arange(0., 10000., 0.05))
    autocorr = td.TimeSignals.getAutoCorrelation(ts)
    
    edw = sp.edw.NewWave(1., ss, waveModel=wk.FirstOrderKinematic,  wmin=0.1, wmax = 3.0, dw = 0.01)
    edw_ts = td.ReconstructionWif(edw.wif).evalSe(np.arange(0, 100, 0.05))
    
   
    np.isclose(autocorr.values[:500] , edw_ts.values[:500], rtol = 1e-3, atol = 1e-2).all()
    
    if display:   
        fig ,ax = plt.subplots()
        edw_ts.plot(ax=ax, marker = "+", linestyle = "", label = "New wave")
        autocorr.loc[:100].plot(ax=ax, label = "Auto-correlation")
        ax.set(xlim = [0,100])
        ax.legend()


if __name__ == "__main__": 
    
    test_autocorr(True)