import pytest
import numpy
import math
import numpy as np
from matplotlib import pyplot as plt
from Snoopy import TimeDomain as td


def RefTriangularDamping(time, wMax, iMode1, iMode2, nModes1, nModes2):
    coef = ((iMode1+1)*nModes2+(iMode2+1))
    if (abs(time) < 1.e-8):
        return coef * (2. / math.pi) * wMax / 2.
    return coef * (2. / math.pi) * 4. * ((math.cos(0.5 * time * wMax)
                                         - math.cos(0.5 * time * wMax)**2)
                                         / (wMax * time**2))


def GetTriangularDamping(frequencies, nModes1, nModes2, wMax):
    nFreqs = len(frequencies)
    damping = numpy.empty((nFreqs, nModes1, nModes2), dtype=float)
    middleIndex = numpy.where(numpy.abs(frequencies-wMax/2.) < 1.e-3)[0][0]
    for iMode1 in range(nModes1):
        for iMode2 in range(nModes2):
            coef = ((iMode1+1)*nModes2+(iMode2+1))
            damping[:middleIndex, iMode1, iMode2] = \
                            coef * 2. * frequencies[:middleIndex] / wMax
            damping[middleIndex:, iMode1, iMode2] = \
                            coef * 2. * (1.-frequencies[middleIndex:] / wMax)
    return damping


def RefExponentialDamping(time, wMax, iMode1, iMode2, nModes1, nModes2):
    coef = ((iMode1+1)*nModes2+(iMode2+1))
    return coef * (2./(math.pi * (1. + time**2)))


def GetExponentialDamping(frequencies, nModes1, nModes2, wMax):
    nFreqs = len(frequencies)
    damping = numpy.empty((nFreqs, nModes1, nModes2), dtype=float)
    for iMode1 in range(nModes1):
        for iMode2 in range(nModes2):
            coef = ((iMode1+1)*nModes2+(iMode2+1))
            damping[:, iMode1, iMode2] = coef*numpy.exp(-frequencies[:])
    return damping


class Reference:
    def __init__(self, wMax, wCut, dampingFct, referenceFct,
                 nFreqs=50, nModes1=2, nModes2=3, retardationDuration=100.,
                 timeStep=0.1):
        self.wMax = wMax
        self._referenceFct = referenceFct
        self.nFreqs = nFreqs
        self.nModes1 = nModes1
        self.nModes2 = nModes2
        self.retardationDuration = retardationDuration
        self.timeStep = timeStep
        self.nTimeInstants = int(self.retardationDuration/self.timeStep) + 1
        self.timeInstants = numpy.arange(self.nTimeInstants) * timeStep
        # Set up the frequencies
        freqs = numpy.arange(self.nFreqs) * wMax / self.nFreqs
        # Get the first index above wCut
        indsSup = numpy.nonzero(freqs > wCut)[0]
        if len(indsSup):
            self.freqs = freqs[:indsSup[0]]
        else:
            self.freqs = freqs
        self.nFreqs = len(self.freqs)
        # Set up damping
        self.damping = dampingFct(self.freqs, nModes1, nModes2, self.wMax)

    def get(self, time, iMode1, iMode2):
        return self._referenceFct(time, self.wMax, iMode1, iMode2,
                                  self.nModes1, self.nModes2)
    
    def getAtTimes(self, iMode1, iMode2):
        res = numpy.empty(self.nTimeInstants, dtype=float)
        for iTime, time in enumerate(self.timeInstants):
            res[iTime] = self.get(time, iMode1, iMode2)
        return res


def IsClose(val1, val2, eps=1.e-8):
    return abs(val2-val1) < eps


def testInitDestruct():
    extrapType = 2
    wInf = -1.
    rf = td.RetardationFunction(extrapType, wInf)


def testComputeTriangularDampingNoExtrap():
    extrapType = 0
    wInf = -1.
    wMax = 2.
    wCut = 2.
    rf = td.RetardationFunction(extrapType, wInf)
    ref = Reference(wMax, wCut, GetTriangularDamping, RefTriangularDamping)
    valsRef = []
    valsComp = []
    for time in ref.timeInstants:
        resComp = rf.get(time, ref.freqs, ref.damping)[0][0]
        resRef = ref.get(time, 0, 0)
        valsComp.append(resComp)
        valsRef.append(resRef)
        #assert(numpy.isclose(rf.get(time, ref.freqs, ref.damping),
        #                     ref.get(time, 0, 0), rtol=1.e-2, atol=1.e-1).all())
        assert(IsClose(rf.get(time, ref.freqs, ref.damping)[0][0],
                       ref.get(time, 0, 0), eps=1.e-2))
    #plt.plot(ref.timeInstants, valsRef, label="ref")
    #plt.plot(ref.timeInstants, valsComp, label="comp")
    #plt.legend()
    #plt.show()
    #plt.plot(ref.freqs, ref.damping[:, 0, 0], label="damp")
    #plt.show()


def testComputeExponentialDampingExpExtrap():
    extrapType = 2
    wInf = -1.
    wMax = 2.
    wCut = 2.
    rf = td.RetardationFunction(extrapType, wInf)
    ref = Reference(wMax, wCut, GetExponentialDamping, RefExponentialDamping)
    valsRef = []
    valsComp = []
    for time in ref.timeInstants:
        resComp = rf.get(time, ref.freqs, ref.damping)[0, 0]
        resRef = ref.get(time, 0, 0)
        valsComp.append(resComp)
        valsRef.append(resRef)
        assert(IsClose(rf.get(time, ref.freqs, ref.damping)[0][0],
                       ref.get(time, 0, 0), eps=1.e-2))
        #assert(numpy.isclose(rf.get(time, ref.freqs, ref.damping),
        #                     ref.get(time), rtol=1.e-5, atol=1.e-2).all())
    #plt.plot(ref.timeInstants, valsRef, label="ref")
    #plt.plot(ref.timeInstants, valsComp, label="comp")
    #plt.legend()
    #plt.show()
    #plt.plot(ref.freqs, ref.damping[:, 0, 0], label="damp")
    #plt.show()


def testComputeTriangularDampingLinearExtrap():
    extrapType = 1
    wInf = 2.
    wMax = 2.
    wCut = 1.5
    rf = td.RetardationFunction(extrapType, wInf)
    ref = Reference(wMax, wCut, GetTriangularDamping, RefTriangularDamping)
    valsRef = []
    valsComp = []
    for time in ref.timeInstants:
        valsComp.append(rf.get(time, ref.freqs, ref.damping)[0][0])
        valsRef.append(ref.get(time, 0, 0))
        assert(IsClose(rf.get(time, ref.freqs, ref.damping)[0][0],
                       ref.get(time, 0, 0), eps=1.e-2))
        #assert(numpy.isclose(rf.get(time, ref.freqs, ref.damping),
        #                     ref.get(time, 0, 0), 1.e-2).all())
    #plt.plot(ref.timeInstants, valsRef, label="ref")
    #plt.plot(ref.timeInstants, valsComp, label="comp")
    #plt.legend()
    #plt.show()
    #plt.plot(ref.freqs, ref.damping[:, 0, 0], label="damp")
    #plt.show()


def testRetardationFunctionsHistoryInit():
    wMax = 2.
    wCut = 2.
    # TODO get a proper reference
    ref = Reference(wMax, wCut, GetTriangularDamping, RefTriangularDamping)
    calcParams = td.RetardationParameters(ref.retardationDuration, ref.timeStep,
                                          wMax, wCut, 0)
    rfh = td.RetardationFunctionsHistory(ref.freqs, ref.damping, calcParams)
    timeInstants = rfh.getTimeInstants(ref.retardationDuration)
    history = rfh.getHistory(ref.retardationDuration)
    refHistory = ref.getAtTimes(0, 0)
    #plt.plot(timeInstants, history[:, 0, 0], label="comp")
    #plt.plot(ref.timeInstants, refHistory, label="ref")
    #plt.legend()
    #plt.show()
    assert(numpy.isclose(history[:, 0, 0], refHistory,
                         rtol=1.e-5, atol=1.e-2).all())


def testRetardationFunctionsHistoryReComputeDampingTriangle():
    wInf = -1.
    wMax = 2.
    wCut = 2.
    calcParams = td.RetardationParameters(50., 0.1, wCut, wInf, 1)
    ref = Reference(wMax, wCut, GetTriangularDamping, RefTriangularDamping)
    rfh = td.RetardationFunctionsHistory(ref.freqs, ref.damping, calcParams)
    damping = rfh.reComputeDamping(ref.freqs)
    timeInstants = rfh.getTimeInstants(ref.retardationDuration)
    #plt.plot(ref.freqs, damping[:, 0, 0], label="comp")
    #plt.plot(ref.freqs, ref.damping[:, 0, 0], label="ref")
    #plt.legend()
    #plt.show()
    #print(damping[:, 0, 0]-ref.damping[:, 0, 0])
    assert(numpy.isclose(damping[:, 0, 0], ref.damping[:, 0, 0],
                         rtol=1.e-2, atol=1.e-1).all())


def testRetardationFunctionsHistoryReComputeDampingExp():
    wMax = 2.
    wCut = 2.
    wInf = -1.
    calcParams = td.RetardationParameters(100., 0.1, wCut, wInf, 2)
    ref = Reference(wMax, wCut, GetExponentialDamping, RefExponentialDamping)
    rfh = td.RetardationFunctionsHistory(ref.freqs, ref.damping, calcParams)
    damping = rfh.reComputeDamping(ref.freqs)
    timeInstants = rfh.getTimeInstants(ref.retardationDuration)
    #plt.plot(ref.freqs, damping[:, 0, 0], label="comp")
    #plt.plot(ref.freqs, ref.damping[:, 0, 0], label="ref")
    #plt.legend()
    #plt.show()
    assert(numpy.isclose(damping[:, 0, 0], ref.damping[:, 0, 0],
                         rtol=1.e-2, atol=1.e-1).all())


def testVelocityHistoryUniformTimeStep():
    simuTimeStep = 0.4
    simuDuration = 200.
    retardTimeStep = 0.1
    retardDuration = 50.
    nModes = 2
    velHistory = td.VelocityHistory(retardTimeStep, retardDuration, nModes, 3)
    for iTime, time in enumerate(numpy.arange(0., simuDuration+simuTimeStep/2., simuTimeStep)):
        velHistory.add(time, numpy.array([time,]*nModes, dtype=float))
        res = velHistory.get(time)
        #print("time: ", time)
        #print("res: ", res.transpose())
        # Compare with windowed history
        if time < retardDuration+retardTimeStep/2.:
            ref = numpy.empty(((iTime*int(simuTimeStep/retardTimeStep))+1, nModes), dtype=float)
            for iMode in range(nModes):
                ref[:, iMode] = numpy.arange(0., time+retardTimeStep/2., retardTimeStep)
            assert(numpy.isclose(res, ref).all())
        else:
            ref = numpy.empty((int(retardDuration/retardTimeStep)+1, nModes), dtype=float)
            for iMode in range(nModes):
                ref[:, iMode] = numpy.arange(time-retardDuration, time+retardTimeStep/2., retardTimeStep)
            assert(numpy.isclose(res, ref).all())


def testVelocityHistoryNonUniformTimeStep():
    simuDuration = 200.
    retardTimeStep = 0.1
    retardDuration = 50.
    nModes = 1
    velHistory = td.VelocityHistory(retardTimeStep, retardDuration, nModes, 2)
    simuTime = numpy.random.random(int(simuDuration)) * simuDuration
    simuTime.sort()
    simuTime = numpy.insert(simuTime, 0, 0.)

    for iTime, time in enumerate(simuTime):
        velHistory.add(time, numpy.array([time,]*nModes, dtype=float))
        res = velHistory.get(time)
        if time > retardDuration:
            assert(res.shape == (int(retardDuration/retardTimeStep)+1, nModes))
        else:
            assert(res.shape == (int(time/retardTimeStep)+1, nModes))


def testVelocityHistoryNonUniformTimeStep2():
    simuDuration = 200.
    retardTimeStep = 0.1
    retardDuration = 0.5
    nModes = 1
    velHistory = td.VelocityHistory(retardTimeStep, retardDuration, nModes, 2)
    simuTime = [0., 0.05, 0.15, 0.2, 0.42, 0.51, 0.6, 0.75, 1., 1.23, 2.]

    for iTime, time in enumerate(simuTime):
        velHistory.add(time, numpy.array([time,]*nModes, dtype=float))
        res = velHistory.get(time)
        if time < retardDuration:
            windowSize = int(time/retardTimeStep)+1
            startTime = time - windowSize * retardTimeStep
            if startTime < 0.:
                startTime += retardTimeStep
            ref = numpy.empty((windowSize, nModes), dtype=float)
            for iMode in range(nModes):
                ref[:, iMode] = numpy.arange(startTime, time+retardTimeStep/2., retardTimeStep)
                assert(numpy.isclose(res, ref).all())
        else:
            ref = numpy.empty(((int(retardDuration/retardTimeStep))+1, nModes), dtype=float)
            for iMode in range(nModes):
                ref[:, iMode] = numpy.arange(time-retardDuration, time+retardTimeStep/2., retardTimeStep)
                assert(numpy.isclose(res, ref).all())


def testConvolution():
    wMax = 2.
    wCut = 2.
    retardationTimeStep = 0.1
    retardationDuration = 50.
    calcParams = td.RetardationParameters(retardationDuration, retardationTimeStep,
                                          wMax, wCut, 0)
    ref = Reference(wMax, wCut, GetTriangularDamping, RefTriangularDamping)
    rfh = td.RetardationFunctionsHistory(ref.freqs, ref.damping, calcParams)
    velHistory = td.VelocityHistory(retardationTimeStep, retardationDuration,
                                    ref.nModes2)
    radiation = td.Radiation(rfh)
    for time in numpy.arange(0., retardationDuration+retardationTimeStep/2.,
                             retardationTimeStep):
        vel = numpy.array([math.sin(0.2*time),]*ref.nModes2)
        velHistory.add(time, vel)
        radiation.setVelocity(time, vel)
    uniform = td.Convolution(rfh.getHistory(ref.retardationDuration),
                             velHistory.get(ref.retardationDuration),
                             ref.timeStep)
    nonUniform = td.Convolution(rfh.getHistory(ref.retardationDuration),
                                velHistory.get(ref.retardationDuration),
                                rfh.getTimeInstants(ref.retardationDuration))
    assert(numpy.isclose(uniform, nonUniform).all())
    rad = radiation.get(ref.retardationDuration)
    assert(numpy.isclose(rad, nonUniform).all())


def testRadiation():
    wMax = 2.
    wCut = 2.
    retardationTimeStep = 0.1
    retardationDuration = 10.
    calcParams = td.RetardationParameters(retardationDuration, retardationTimeStep,
                                          wMax, wCut, 0)
    ref = Reference(wMax, wCut, GetTriangularDamping, RefTriangularDamping)
    rfh = td.RetardationFunctionsHistory(ref.freqs, ref.damping, calcParams)
    radiationUniform = td.Radiation(rfh)
    for time in numpy.arange(0., retardationDuration+retardationTimeStep/2.,
                             retardationTimeStep):
        vel = numpy.array([math.sin(0.2*time),]*ref.nModes2)
        radiationUniform.setVelocity(time, vel)
    radiationNonUniform = td.Radiation(rfh)
    pattern = numpy.array([0., 0.05, 0.15, 0.2, 0.32, 0.35, 0.4, 0.5, 0.65, 0.7,
                           0.73, 0.8, 0.82, 0.87, 0.94], dtype=float)
    timeConcat = (pattern,)
    for i in range(1, int(retardationDuration), 1):
        timeConcat += (pattern+i,)
    timeConcat += (numpy.array((retardationDuration,), dtype=float),)
    timeNonUniform = numpy.concatenate(timeConcat, axis=0)
    for time in timeNonUniform:
        vel = numpy.array([math.sin(0.2*time),]*ref.nModes2)
        radiationNonUniform.setVelocity(time, vel)
    radUni = radiationUniform.get(retardationDuration)
    radNonUni = radiationNonUniform.get(retardationDuration)
    assert(numpy.isclose(radUni, radNonUni, atol=1.e-4, rtol=1.e-4).all())


def test_with_hydrostar_data( display = False ):
    ret = td.RetardationFunctionsHistory.FromHydroStar( f"{td.TEST_DIR:}/hsmcn_b31.h5", 
                                                    trad = 40.0 , 
                                                    dt   = 0.5 , 
                                                    wCut = 1.8,
                                                    wInf = 3.0 ) 
    
    re_calc = ret.reComputeDamping( ret.getFrequencies() )
    ori = ret.getDamping()
    for i in range(6) :
        j = i
        maxErr = ((re_calc[ :,i , j ] - ori[ :,i , j ])  / ori[ :,i , j ].max() ).max()
        assert( maxErr < 0.15 )

    if display : 
        ret.plot()
        ret.plotDampingRecalculation(5,5)
        
        

def test_compare_reconstruction( display = False ):        
    """Reconstruct time domain
    """
    import pandas as pd
    from Snoopy import Reader as rd
    from Snoopy import Spectral as sp
    
    trad = 100.
    ret = td.RetardationFunctionsHistory.FromHydroStar( f"{td.TEST_DIR:}/hsmcn_b31.h5", 
                                                    trad = trad , 
                                                    dt   = 0.5 , 
                                                    wCut = 1.8, 
                                                    wInf = 3.0
                                                    )

    raos = rd.hydrostar.read_hsmcn_h5( f"{td.TEST_DIR:}/hsmcn_b31.h5" )[0]
    raos = raos.getDerivate()
    
    rad_raos = rd.hydrostar.read_hsmcn_h5( f"{td.TEST_DIR:}/hsmcn_b31.h5" , kind = "Radiation_wo_inf" )[0]

    wif = sp.Wif.Jonswap( 1, 10 , 1.0 , np.pi/4 , wifArgs = {"seed" : 15})
    rec = td.ReconstructionRaoLocal( wif, raos.getSorted() )
    recRad = td.ReconstructionRaoLocal( wif, rad_raos.getSorted() )

    time =  np.arange( 0., 200. , 0.5)   

    vel = rec.evalDf(time)
    rad_ts_ref = recRad.evalDf(time)
    
    rad = td.Radiation(ret) 
    
    rad_ts = pd.DataFrame(index = time , columns = range(6))
    
    for i, itime in enumerate(time):
        rad.setVelocity( time[i] , vel.values[i,:] )
        rad_ts.iloc[ i ,: ] = rad.get( itime )
        
    for imode in range( raos.getNModes() ):
        err = np.abs( ( rad_ts.loc[trad:,:].iloc[:,imode] -  rad_ts_ref.loc[trad:,:].iloc[:,imode]) / rad_ts_ref.iloc[:,imode].max())
        assert( np.max(err) < 0.3 )
        print (imode , f"ok err_rel_max = {np.max(err):.1%}")
        
    #Compare time trace
    if display :
        fig, ax = plt.subplots()    
        rad_ts.iloc[:,2].plot(ax=ax)
        rad_ts_ref.iloc[:,2].plot(ax=ax)


if __name__ == "__main__":
    
    test_with_hydrostar_data( display = True )
    test_compare_reconstruction( display = True )











