import numpy as np
import pandas as pd
from Snoopy.TimeDomain import getDownCrossID
from Snoopy import TimeDomain as td


if __name__ == "__main__" :

    from Snoopy import Spectral as sp
    from Snoopy import TimeDomain as td
    from Snoopy.Statistics.returnLevel import ReturnLevel
    from Snoopy.TimeDomain import UpCrossAnalysis
    from matplotlib import pyplot as plt

    pos_z = 5.   # Position of the impact point, from the mean surface. If pos_z < 0, the point is wetted at rest.

    rwe_rao = sp.Rao( "../../../Snoopy/Spectral/SpectralTests/test_data/rao/RWE_175m.rao" ).getRaoForSpectral( wmin=0.2, wmax = 1.8, dw = 0.05 )
    vz_rao = rwe_rao.getDerivate(n = 1)

    hs = 5.0
    gamma = 1.5
    tp = sp.Jonswap.t0m12tp( 9 , gamma )

    heading = np.pi
    ss = sp.SeaState( sp.Jonswap( hs , tp , gamma , heading  ) )

    wif = sp.Wif( ss , seed = 15)
    duration = 10 * 3600

    fig, ax = plt.subplots()
    for dt  in [0.01 , 0.5] :
        time = np.arange(0. , duration , dt)
        rwe_ts = td.ReconstructionRaoLocal( wif , rwe_rao ).evalSe(time)
        vz_ts = td.ReconstructionRaoLocal( wif , vz_rao ).evalSe(time)

        vz_upCross = UpCrossAnalysis.FromTs( vz_ts )

        impact_up = td.getSlammingVelocity( rwe_ts , vz_ts=vz_ts , pos_z = pos_z , method = "map_up" )
        impact_down = td.getSlammingVelocity( rwe_ts , vz_ts=vz_ts , pos_z = pos_z , method = "map_down" )
        impact_interp = td.getSlammingVelocity( rwe_ts , vz_ts=vz_ts , pos_z = pos_z , method = "interpolate" )
        impact_diff = td.getSlammingVelocity( rwe_ts , vz_ts=vz_ts , pos_z = pos_z , method = "diff" )

        rp_impact_up = ReturnLevel( impact_up, duration = duration )
        rp_impact_down = ReturnLevel( impact_down, duration = duration )
        rp_impact_interp = ReturnLevel( impact_interp, duration = duration )
        rp_impact_diff = ReturnLevel( impact_diff, duration = duration )

        rp_velocity = ReturnLevel( vz_upCross.Maximum, duration )

        if False :
            fig ,ax = plt.subplots()
            vz_ts.plot( ax=ax  )
            impact_interp.plot(ax=ax, marker = "s", linestyle = "")
            impact_diff.plot(ax=ax, marker = "s", linestyle = "")
            rwe_ts.plot(ax=ax, linestyle = "--", marker = "+")
            ax.set_xlim([200, 300])

        if dt == 0.01 :
            ls = "--"
            rp_impact_up.plot( ax=ax  ,scale_rp = 1/3600, label = "Ref" , linewidth = 2)
        else :
            rp_impact_up.plot( ax=ax  ,scale_rp = 1/3600, label = "Impact velocity up")
            rp_impact_down.plot( ax=ax  ,scale_rp = 1/3600, label = "Impact velocity down")
            rp_impact_interp.plot( ax=ax  ,scale_rp = 1/3600, label = "Impact velocity interp")
            rp_impact_diff.plot( ax=ax  ,scale_rp = 1/3600, label = "Impact velocity diff")
            ax.legend()
