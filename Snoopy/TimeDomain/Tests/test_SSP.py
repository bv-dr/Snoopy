import pandas as pd
import numpy as np

from Snoopy.TimeDomain.ssp import SSP

def test_ssp():

    t = np.linspace(-10, 10, 200)
    signal = np.cos(t)+np.sin(2*t)
    signal = pd.Series(signal, index=t)
    assert(np.isclose(SSP(signal, signal , removeAverage=True), 0., 1e-6))

    signal2 = pd.Series(np.cos(t+np.pi)+np.sin(2*t+np.pi), index=t)
    assert(np.isclose(SSP(signal, signal2, removeAverage=False), 1., 1e-6))
    
    
def illustrate_ssp() : 
    from Snoopy import WaveKinematic as wkm
    from Snoopy import Spectral as sp    
    for hs in np.arange(1,10,1):
        wif = sp.Wif.Jonswap( hs, 10 , 1.5 , np.pi, wifArgs= {"seed" : 15})
        kin1 = wkm.FirstOrderKinematic(wif)
        kin2 = wkm.SecondOrderKinematic(wif)   
        ts_1 = kin1.getElevation_SE(time = np.arange(0. , 800, 0.5), x=0, y=0)
        ts_2 = kin2.getElevation_SE(time = np.arange(0. , 800, 0.5), x=0, y=0)
        print( hs , SSP(ts_1, ts_2)  )
                                
                                
if __name__ == "__main__" :

    test_ssp()
    illustrate_ssp()
