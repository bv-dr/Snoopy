'''
Reconstruction tests
'''

import os
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from Snoopy import Spectral as sp
from Snoopy import TimeDomain as td
import pytest



def basic() :
    spec = sp.Harris(10., 0.002, 1800., -45.)
    spec = sp.Jonswap(3., 12., 3.3, 0.)
    wif = sp.Wif(spec, np.linspace(0.1, 2., 30, dtype=float))
    wif = sp.Wif([1.,],[1.,], [np.pi/2.,], [0.,])
    r = td.ReconstructionWifLocal(wif)
    a = np.linspace(0., 360., 20, dtype=float) #w
    b = np.linspace(0.1, 2., 10, dtype=float) # b
    c = 2.*np.ones((20, 10, 6), dtype=float) # modules
    d = 0.*np.ones((20, 10, 6), dtype=float) # phases
    e = [0., 0., 0.]
    f = [0., 0.]
    rao = sp.Rao(a, b, c, d, e, f)
    print (rao.getComplexData()[1, 2, 3])
    raoInterp = rao.getRaoAtFrequencies(wif.getFrequencies()).getSorted()
    rRao = td.ReconstructionRaoLocal(wif, rao)
    #rRao.plot()
    ax = rRao.plot(None, 0., 12., 0.1)
    r.plot(ax, 0., 12., 0.1)
    wif.setAmplitudes([2.,])
    rRao.plot(ax, 0., 12., 0.1)
    plt.show()


def test_ReconstructionRao_reg(plot = False) :
    """ Test reconstruction on regular wave
    """

    rao_file = os.path.join(r"../../Spectral/Tests/test_data/my_10_0speed.rao")
    rao = sp.Rao(  rao_file ).getSorted()
    w = rao.freq[10]

    headRad = np.pi
    sp_wif = sp.Wif( w = [w] , a = [1.0] , b =  [headRad] , phi = [0.])

    tmin , tmax , dt = -100 , 100 , 0.5
    time = np.arange(tmin, tmax, dt)

    #RAO
    recRao = td.ReconstructionRaoLocal( sp_wif , rao )
    sp_ts = recRao.evalSe( time )

    interp = rao.getRaoAtFrequencies(  [w] )

    ihead = np.where(rao.head == headRad)[0][0]

    reconstruction = interp.module[ihead,0,0] * np.cos( w * time + interp.phasis[ihead,0,0] )

    if plot :
        fig, ax = plt.subplots()
        sp_ts.plot(ax=ax, label = "Snoopy")
        ax.plot( time, reconstruction  , "o", label = "manual")
        ax.legend()
        plt.show()

    assert( (sp_ts.values / reconstruction < 1.005).all() )
    assert( (sp_ts.values / reconstruction > 0.995).all() )

def test_ReconstructionRao_regCompKX(plot = False) :
    """ Test reconstruction on regular wave
    Comparison between encounter frequency and moving ship
    """

    rao_file = os.path.join(r"../../Spectral/Tests/test_data/my_10.rao")
    rao = sp.Rao(  rao_file ).getSorted()
    w = rao.freq[10]

    headRad = np.pi
    sp_wif = sp.Wif( w = [w] , a = [1.0] , b =  [headRad] , phi = [0.])

    tmin , tmax , dt = -100 , 100 , 0.5
    time = np.arange(tmin, tmax, dt)

    #RAO
    recRao = td.ReconstructionRaoLocal( sp_wif , rao )
    sp_ts = recRao.evalSe( time )

    recRao = td.ReconstructionRao( sp_wif , rao )
    xs = time * rao.getForwardSpeed() + rao.getReferencePoint()[0] - rao.getWaveReferencePoint()[0]
    ys = time * 0.
    hs = time * 0.
    sp_tx = pd.DataFrame(index=time, data=recRao(time, xs, ys, hs)).iloc[:,0]

    if plot :
        fig, ax = plt.subplots()
        sp_ts.plot(ax=ax, label="Snoopy enc")
        sp_tx.plot(ax=ax, label="Snoopy kx", style="o")
        ax.legend()
        plt.show()

    assert( (sp_ts.values / sp_tx.values < 1. + 1.e-8).all() )
    assert( (sp_ts.values / sp_tx.values > 1. - 1.e-8).all() )


def demoReconstructionWif(plot = False):
    """ Test wave elevation reconstruction (from Wif)
    """
    wif = sp.Wif(f"{td.TEST_DATA:}/my_10_rcw.wif")
    rec = td.ReconstructionWifLocal(wif)
    time = np.arange(-50 , 50 , 0.5)
    ts = rec.evalDf( time )

    if plot :
        ts.plot()



def pluto_vs_snoopy():
    """
    Compare Pluto and Snoopy reconstruction
    """
    from Pluto import Spectral as plsp

    rao_file = os.path.join(r"../../Spectral/Tests/test_data/my_10.rao")
    rao_file = r"D:\Etudes\Basic\rao\pitch.rao"

    sp_rao = sp.Rao( rao_file ).getSorted()
    pl_rao = plsp.Rao( rao_file ).getSorted()

    headDeg = 0.0
    headRad = np.deg2rad(headDeg)
    spec = sp.Jonswap( 1.0 , 10.0 , 1. , headRad )
    seaState = sp.SeaState(spec)

    sp_wif = sp.Wif( seaState, nbSeed = 50, wmax = 1.8 , seed = 15 )

    sp_wif.write("wif.wif")

    pl_wif = plsp.Wif( "wif.wif" )

    tmin , tmax , dt = 0,50800, 0.5
#    tmin , tmax , dt = -100 , 100 , 0.5
    time = np.arange(tmin, tmax, dt)

    #Wave elevation
    pl_eta = pl_wif.timeC(tmin, tmax, dt)
    sp_eta = td.ReconstructionWifLocal( sp_wif ).evalDf( time )
    fig, ax = plt.subplots()
    pl_eta.iloc[:,0].plot(ax=ax, label = "Pluto", marker = "+")
    sp_eta.iloc[:,0].plot(ax=ax, label = "Snoopy")
    ax.legend()

    #RAO
    from Pluto.System import Chrono
    c = Chrono(start=True)
    pl_ts = pl_wif.raoTimeC( pl_rao, tmin , tmax, dt)
    c.printLap("Pluto")
    recRao = td.ReconstructionRaoLocal( sp_wif , sp_rao , ComplexInterpolationStrategies = sp.ComplexInterpolationStrategies.AMP_PHASE, numThreads = 20 )
    sp_ts = recRao.evalDf( time )
    c.printLap("Snoopy")
    fig, ax = plt.subplots()
    pl_ts.iloc[:,0].plot(ax=ax, label = "Pluto")
    sp_ts.iloc[:,0].plot(ax=ax, label = "Snoopy")
    ax.legend()



@pytest.mark.parametrize( "plot, multimodal", ( [False, False], [False, True])  )
def test_ReconstructionRao_irreg(plot, multimodal ):

    spec = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.deg2rad(90))
    spec2 = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.deg2rad(30))

    if multimodal :
        ss = sp.SeaState( [spec, spec2] )
    else :
        ss = sp.SeaState( spec )

    wif = sp.Wif( ss , nbSeed = 200, seed = 15)

    print (wif)

    rao = sp.Rao( "../../Spectral/Tests/test_data/my_10.rao" )
    rao = rao.getSymmetrized()

    #Response spectrum
    rSpec = sp.ResponseSpectrum(ss, rao)
    sp_std = rSpec.getM0()**0.5

    rec = td.ReconstructionRaoLocal( wif, rao.getSorted() )
    rao_ts = rec.evalDf( np.arange( 0. , 5000 , 0.2 )  )

    ts_std = rao_ts.iloc[:,0].std()

    print( "Std from spectral calculation = {:}\nStd from time domain reconstruction = {:}".format( sp_std, ts_std ) )
    assert( np.isclose( sp_std , ts_std, rtol = 0.02 ) )

    #Test distribution
    if plot :
        from Snoopy.TimeDomain import upCrossMinMax
        updata = upCrossMinMax( rao_ts.iloc[:,0] )

        from Pluto.statistics import distPlot
        distPlot( updata.Maximum , rSpec.getMaxDistribution() )



def test_ReconstructionRao_reg_ampOnly():
    """ Reconstruct signal, and check amplitude (phase is not checked)
    """

    w = 0.5852
    headDeg = 0.
    wif = sp.Wif(  w = [w], a = [1], phi = [0] , b = [np.deg2rad(headDeg)] )

    rao = sp.Rao( "../../Spectral/Tests/test_data/my_10.rao" ).getSorted()
    rao_df = rao.toDataFrame("module")

    t = np.arange( 0., 6.28 / 0.25, 0.1 )

    rec = td.ReconstructionRaoLocal( wif, rao )
    rao_ts = rec.evalDf( t )

    assert(  np.isclose( float(rao_ts.max().iloc[0]) , rao_df.loc[ w, np.deg2rad(headDeg) ] ) )


@pytest.mark.parametrize( "display, relativeHeading, spreading, uniform", ( [False, 135, False, False], [False, 150-360, True, True], [False, 150-360, True, False] )  )
def test_local_global(display, relativeHeading, spreading, uniform ):

    absoluteHeading1 = relativeHeading
    absoluteHeading2 = 180.

    azimuth1 = absoluteHeading1 - relativeHeading
    azimuth2 = absoluteHeading2 - relativeHeading


    if spreading :
        spec = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.deg2rad(relativeHeading) , spreading_type = sp.SpreadingType.Cosn, spreading_value = 4.0)
        spec2 = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.deg2rad( absoluteHeading2 ) , spreading_type = sp.SpreadingType.Cosn, spreading_value = 4.0)
    else :
        spec = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.deg2rad(relativeHeading) )
        spec2 = sp.Jonswap( hs = 3., tp = 12., gamma = 1.2, heading = np.deg2rad( absoluteHeading2 ) )


    ss = sp.SeaState( spec )
    ss2 = sp.SeaState( spec2 )

    if uniform :
        wif = sp.Wif( ss , w = np.arange(0.1, 1.8, 0.05), b = np.linspace(0, 2*np.pi, 12, endpoint = False  ))
        wif2 = sp.Wif( ss2 , w = np.arange(0.1, 1.8, 0.05), b = np.linspace(0, 2*np.pi, 12, endpoint = False  ))
    else :
        wif = sp.Wif( ss , nbSeed = 100, seed = 15)
        wif2 = sp.Wif( ss2 , nbSeed = 100, seed = 15)
    rao = sp.Rao( "../../Spectral/Tests/test_data/my_10_0speed.rao" ).getSorted()

    xwav, ywav = rao.getWaveReferencePoint()
    xref, yref, zref = rao.getReferencePoint()

    # evaluated at application point (equals wave reference point here)
    recLoc = td.ReconstructionRaoLocal( wif, rao )
    # evaluated at application point (to compare to wave reference point!!)
    recGlob = td.ReconstructionRao( wif, rao  ) 
    recGlob2 = td.ReconstructionRao( wif2, rao  )

    time = np.arange(0, 300, 0.5 )

    loc_ts = recLoc( time )
    glob_ts = recGlob( time, np.full(time.shape, xref-xwav) , np.full(time.shape, yref-ywav), np.full(time.shape, azimuth1) )

    cosa = np.cos(np.deg2rad(azimuth2))
    sina = np.sin(np.deg2rad(azimuth2))
    glob_ts2 = recGlob2( time, np.full(time.shape, -((xwav-xref)*cosa-(ywav-yref)*sina)),
                               np.full(time.shape, -((xwav-xref)*sina+(ywav-yref)*cosa)),
                               np.full(time.shape, np.deg2rad(azimuth2)) )


    if display:
        fix, ax = plt.subplots()
        ax.plot(time, loc_ts, label = "Loc")
        ax.plot(time, glob_ts, label = "Glob 1")
        if not spreading :
            ax.plot(time, glob_ts2, "+" , label = "Glob 2")
        ax.legend()
        plt.show()

    assert( np.isclose( loc_ts, glob_ts, rtol = 1e-2, atol = loc_ts.max() / 200 ).all() )

    if not spreading :  #Difference are to be expected id spreading is used
        assert( np.isclose( glob_ts, glob_ts2, rtol = 1e-2 ).all() )


def test_reconstructionWif():

    hs = 1.0
    wif = sp.Wif( sp.Jonswap( hs , 10 , 1.0 ), seed = 15)
    ts = td.ReconstructionWif( wif )( np.arange(0,1000,0.1) )
    assert(  np.isclose( 4*np.std(ts) , hs , 0.05 ) )

if __name__ == "__main__" :

    print ("Run")
    test_reconstructionWif()
    test_local_global(True , 135 , True, False)
    # test_local_global(True, 135-360, False, False)


    #pluto_vs_snoopy()
    # test_ReconstructionRao_reg_ampOnly()
    # test_ReconstructionRao_irreg(False, True)
    # test_ReconstructionRao_reg(True)
    # test_ReconstructionRao_regCompKX(True)


