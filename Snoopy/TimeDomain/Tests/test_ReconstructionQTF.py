import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from Snoopy import TimeDomain as td
from Snoopy import Spectral as sp
from Snoopy import logger



def test_ReconstructionQtf_global_local(mode = "diff", plot = False ):
    """
    """
    spec = sp.Jonswap( 1.0 , 10.0 , 1. , 0.)

    if mode == "diff" :
        qtf = sp.Qtf(f"{td.TEST_DIR:}/qtf_fx.qtf")
    else:
        qtf = sp.Qtf(f"{td.TEST_DIR:}/spgMode01.qtf")

    #------------ Time domain part
    wif = sp.Wif( spec, nbSeed = 50, wmax = 1.8 )
    time = np.arange( 0., 500. , 0.5 )

    xwav, ywav = qtf.getWaveReferencePoint()
    xref, yref, zref = qtf.getReferencePoint()
    recQtfLocal = td.ReconstructionQtfLocal( wif , qtf )
    recQtfGlobal = td.ReconstructionQtf( wif , qtf )

    tsLocal = recQtfLocal.evalDf( time  ).iloc[:,0]
    tsGlobal = recQtfGlobal.evalDf( time,  np.full(time.shape, xref-xwav) , np.full(time.shape, yref-ywav) ,  np.full(time.shape, 0.0)   ).iloc[:,0]

    assert( np.isclose( tsLocal, tsGlobal ).all() )

    if plot :
        fig, ax = plt.subplots()
        tsLocal.plot(ax=ax)
        tsGlobal.plot(ax=ax)

def test_ReconstructionQtf_global_local_sum(*args, **kwargs):
    test_ReconstructionQtf_global_local("sum", *args, **kwargs)


def test_mean():
    """Check mean loads calculation by three way are consistent
    - Spectral
    - Mean from time domain
    - Discretize spectrum, sum over component.
    """
    ss = sp.SeaState.Jonswap( 1.0 , 10.0 , 1. , 0.) 

    qtf = sp.Qtf(f"{td.TEST_DIR:}/qtf_fx.qtf")

    spec2 = sp.ResponseSpectrum2nd(ss, qtf)
    spec_mean = spec2.getMean()

    #------------ Time domain part
    wif = sp.Wif( ss, nbSeed = 50, wmax = 1.8 )
    
    recQtfLocal = td.ReconstructionQtfLocal( wif , qtf , numThreads = 20)
    
    wif_mean = recQtfLocal()

    time_mean = np.mean(recQtfLocal( np.arange(0,10800, 0.5) ))
    
    assert( np.isclose( time_mean , wif_mean, rtol = 0.01 ))
    assert( np.isclose( spec_mean , wif_mean, rtol = 0.01 ))
    
    

def test_qtf_reconstruction_and_spec( mode = "diff", plot = False ):

    spec = sp.Jonswap( 1.0 , 10.0 , 1. , 0.)
    seaState = sp.SeaState(spec)

    if mode == "diff" :
        qtf = sp.Qtf(f"{td.TEST_DIR:}/qtf_fx.qtf")
    else:
        qtf = sp.Qtf(f"{td.TEST_DIR:}/spgMode01.qtf")

    #----------  Compute response spectra
    rsp = sp.ResponseSpectrum2nd(seaState, qtf)
    frq = rsp.getFrequencies()
    rsp = rsp.get()
    rspec = pd.Series(index = frq, data = rsp)

    #------------ Time domain part
    wif = sp.Wif( spec, nbSeed = 50, wmax = 1.8 )
    recQtf = td.ReconstructionQtfLocal( wif , qtf )
    t = np.arange( 0., 20000. , 0.5 )
    ts = recQtf.evalDf( t  )

    #----------- Compare Time domain and frequency domain
    from Snoopy.TimeDomain import getPSD
    psd_time = getPSD(ts, dw = 0.05)
    psd = pd.DataFrame(psd_time)
    if plot :
        ax = rspec.plot()
        psd.plot(ax = ax)



if __name__ == "__main__" :

    test_qtf_reconstruction_and_spec(mode = "diff", plot = True)
    test_qtf_reconstruction_and_spec(mode = "sum", plot = True)
    test_ReconstructionQtf_global_local( mode = "diff")
    test_mean()

