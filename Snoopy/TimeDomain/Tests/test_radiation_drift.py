import numpy as np
from Snoopy.TimeDomain.oneDof import OneDof
from Snoopy import TimeDomain as td
from Snoopy import Spectral as sp
from Snoopy import Reader as rd




if __name__ == "__main__" :


    ret = td.RetardationFunctionsHistory.FromHydroStar( r"test_data/hsmcn_b31.h5", 
                                                       trad = 40.0 , 
                                                       dt   = 0.5 , 
                                                       wCut = 1.8, 
                                                       wInf = 3.0
                                                      )
    
    import xarray
    d = xarray.open_dataset( r"test_data/hsmcn_b31.h5" )
    
    amss0 = d.AddedMass[ 0,0,0,0,0,0 ].values
    amssInf = d.AddedMassInf[0,0,0,0].values
    
    odof = OneDof( m = 2e8 + amss0, k = 8e5 , bl = 2e6, bq = 0.0 )
    odof_rad = OneDof( m = 2e8 + amssInf , k = 8e5 , bl = 2e6, bq = 0.0 )
    
    print (odof)
    
    rao = rd.hydrostar.read_hsmcn_h5( r"test_data/hsmcn_b31.h5" , kind = "Excitation" )[0].getRaoAtMode(imode = 0)
    
    qtf = sp.Qtf( 'test_data/qtf_fx.qtf' )
    
    wif = sp.Wif.Jonswap( 10.0 , 15.0 , 1.0 , np.pi , wifArgs = {"seed" : 15})
    rec = td.ReconstructionRaoLocal( wif , rao )
    
    rec_qtf = td.ReconstructionQtfLocal( wif , qtf )
    
    
    class RadiationLoad(object):
        def __init__(self , retardationFunctionsHistory):
            
            self.rad = td.Radiation( retardationFunctionsHistory ) 
            
        def __call__( self , t , y ):
            vel = y[1]
            self.rad.setVelocity( t , np.array( [vel,0,0,0,0,0] )  )
            return self.rad.get( t )[0]
        
    
    def ramp(t):
        tramp = 30.
        if t < tramp : 
            return  1- ( np.cos( (t/tramp)*np.pi*0.5 ) )**2
        else : 
            return 1.0
        
    
    def force_qtf( t , y ) : 
        return ( rec_qtf(t)[0] ) * ramp(t)
    
    
    radLoad1 = RadiationLoad( ret )
    def force_qtf_1st_rad( t , y ) : 
        return (rec(t)[0] + radLoad1(t, y) + rec_qtf(t)[0]) * ramp(t)
    
    def force_qtf_1st( t , y ) : 
        return (rec(t)[0] +  rec_qtf(t)[0]) * ramp(t)
    
    def force_1st( t , y ) : 
        return (rec(t)[0] ) * ramp(t)
    
    
    radLoad2 = RadiationLoad( ret )
    def force_1st_rad( t , y ) : 
        return (rec(t)[0] + radLoad2(t, y)) * ramp(t)
    
    
    t_max = 5000.
    time = np.arange(0, t_max , 0.1) 
    
    motion_1st_rad = odof_rad.forcedMotion( 0, t_max, [0,0] , force_1st_rad , t_eval = time) 
    motion_qtf = odof.forcedMotion( 0, t_max, [0,0] , force_qtf , t_eval = time) 
    motion_qtf_1st = odof.forcedMotion( 0, t_max, [0,0] , force_qtf_1st , t_eval = time) 
    motion_qtf_1st_rad = odof_rad.forcedMotion( 0, t_max, [0,0] , force_qtf_1st_rad , t_eval = time) 
    motion_1st = odof.forcedMotion( 0, t_max, [0,0] , force_1st , t_eval = time) 
    
    
    from Snoopy.TimeDomain import bandPass
    from matplotlib import pyplot as plt
    fig, ax = plt.subplots()
    bandPass( motion_qtf ,fmax = 0.2, unit = "rad/s" ).plot(ax=ax, label = "QTF")
    bandPass( motion_qtf_1st ,fmax = 0.2, unit = "rad/s" ).plot(ax=ax, label = "QTF + 1st")
    bandPass( motion_qtf_1st_rad ,fmax = 0.2, unit = "rad/s" ).plot(ax=ax, label = "QTF + 1st + Rad")
    ax.legend()
    
    
    fig, ax = plt.subplots()
    bandPass( motion_1st_rad ,fmax = 0.2, unit = "rad/s" ).plot(ax=ax, label = "1st")
    bandPass( motion_1st ,fmax = 0.2, unit = "rad/s" ).plot(ax=ax, label = "1st + rad")
    ax.legend()
    
    
    
    
    
    
    
    
