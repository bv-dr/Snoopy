import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from Snoopy.TimeDomain import slidingFFT, bandPass, rampDf, fftDf



def fillCos(A=1.0, T=10., tMin=0., tMax=50., n=200):
    """ for testing purpose, fill signal with a cosine """
    xAxis = np.linspace(tMin, tMax, n)
    data = np.zeros((n, 2))
    data[:, 0] = A * np.cos(2 * np.pi * xAxis / T)
    data[:, 1] = A * np.sin(2 * np.pi * xAxis / T)
    return pd.DataFrame(data=data, index=xAxis, columns=["Cos", "Sin"])




def test_sliding_FFT(display=False):
    tsCos = fillCos(T=10., tMin=0, tMax=50, n=400)
    tsCos2 = fillCos(A=0.5,  T=5, tMin=0, tMax=50, n=400)
    tsSum = tsCos + tsCos2
    tsSum += 0.25

    tsSum = rampDf(tsSum,  0, 10)
    
    sFFT = slidingFFT(tsSum.Cos,  T=10., n=1,  preSample=True, kind=np.abs)
    
    assert( np.isclose( sFFT.loc[15:25, "Harmo 0 (Cos)"].mean() , 0.25 , 0.01) )
    assert( np.isclose( sFFT.loc[15:25, "Harmo 1 (Cos)"].mean() , 1.0 , 0.01) )
    assert( np.isclose( sFFT.loc[15:25, "Harmo 2 (Cos)"].mean() , 0.5 , 0.01) )
    

    if display:
        fig, ax = plt.subplots()
        tsSum.Cos.plot(ax=ax)
        sFFT.plot(ax=ax)




def test_fftDf(display=False):
    tsCos = fillCos(A=1.0, T=10., tMin=0, tMax=100, n=1001).Cos
    tsCos2 = fillCos(A=0.5,  T=5., tMin=0, tMax=100, n=1001).Cos
    tsCos3 = fillCos(A=0.5,  T=2., tMin=0, tMax=100, n=1001).Cos
    tsSum = tsCos + tsCos2 + tsCos3

    df = fftDf(tsSum)

    eps = 1e-3
    
    assert( np.isclose( df.loc[ 0.1-eps:0.1+eps ].abs().values[0]  , 1.0, 0.005))
    assert( np.isclose( df.loc[ 0.2-eps:0.2+eps ].abs().values[0]  , 0.5, 0.005))
    
    if display:
        df.abs().plot(marker = "o", linestyle = "")



def test_bandpass(display=False):
    tsCos = fillCos(A=1.0, T=10., tMin=0, tMax=100, n=1000).Cos
    tsCos2 = fillCos(A=0.5,  T=5., tMin=0, tMax=100, n=1000).Cos
    tsCos3 = fillCos(A=0.5,  T=2., tMin=0, tMax=100, n=1000).Cos
    tsSum = tsCos + tsCos2 + tsCos3

    filtered = bandPass(tsSum, fmin = 1. / 5.1,  fmax=1. / 4.9)
    
    assert(  np.isclose( tsCos2.loc[50:60], filtered.loc[50:60], rtol = 0.01 , atol = 0.01 ).all() )
    
    if display:
        ax = tsSum.plot( label ="Total signal" )
        filtered.plot(ax=ax, linestyle="", marker="o", label = "Filtered")
        tsCos2.plot(ax=ax, label = "Reference")
        tsCos.plot(ax=ax, label = "10s")
        tsCos3.plot(ax=ax, label = "2s")
        ax.legend()
        ax.set_xlim([50, 60])



if __name__ == '__main__':

    # Launch the test
    display = True

    test_fftDf(display)
    test_bandpass(display)
    test_sliding_FFT(display)
    plt.show()
    print("Test TimeSignals done")