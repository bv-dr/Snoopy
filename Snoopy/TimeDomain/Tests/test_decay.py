import numpy as np
from matplotlib import pyplot as plt
from Snoopy.TimeDomain import DecayAnalysis


def test(display=False):
    from Snoopy.TimeDomain.oneDof import OneDof
    m = 15
    bl = 0.5
    bq = 2.0
    k = 10

    # Generate a mcnSolve test
    mcnSolve = OneDof(m=m, bl=bl, bq=bq, k=k)
    decayMotion = mcnSolve.decay(tMin=0.0, tMax=100.0, X0 = np.array([10.0, 0.]), t_eval = np.arange(0., 100., 0.1) )

    for method in [  "min", "max", "minmax_quad", "minmax_ari", "semi_quad" , "semi_ari"]:
        test2 = DecayAnalysis( decayMotion, method = method )
        bl_test, bq_test = test2.getDimDampingCoef(T0=2*np.pi*(m/k)**0.5, k=k)
        print( f"Error {method:}  :  { (bl_test/bl-1):.2%} , {(bq_test/bq-1):.2%}")
        assert(  np.isclose( bl_test,bl, rtol= 0.05))
        assert(  np.isclose( bq_test,bq, rtol= 0.05))

        if display :
            test2.plotRegression()

    if display:
        test2.compareWithFitted()

    assert( np.isclose( test2.getDimEqDamping(bl, bq, a=1.0) / test2.getCriticalDamping(k=k), test2.coef[1] + test2.coef[0] * 1.0, rtol = 0.01))
    print("DecayTest ok")


if __name__ == "__main__":

    test(True)


