
from Snoopy import Spectral as sp
from Snoopy import TimeDomain as td
import numpy as np
import pandas as pd




def test_reconstructionMulti_FFT(plot = False):
    from matplotlib import pyplot as plt
    wif = sp.Wif.Jonswap( hs = 10, tp = 12, gamma = 1.5 )

    rec = td.ReconstructionWifLocal( wif )

    time = np.arange(0, 5000, 0.5)
    resp = rec(time)

    wifm = sp.Wifm.FromTs( pd.Series(index = time , data = resp), windows_net = 500, overlap = 100, method = "FFT" )
    recMulti = td.ReconstructionMulti( td.ReconstructionWif, wifm )
    resp_re = recMulti(time)

    assert( (resp_re - resp).max() < resp_re.max() / 1000 )

    if plot :
        fig, ax = plt.subplots()
        ax.plot(time, resp, "-", label = "Original")
        ax.plot(time, resp_re, "+", label = "Reconstructed")
    print ("ReconstructionMulti ok")



def test_reconstructionMulti_QTF(display = False):
    wif = sp.Wif.Jonswap( 1.0 , 10.0 , 1.0 , np.pi , wifArgs= {"seed":15})
    qtf = sp.Qtf( f"{td.TEST_DIR:}/qtf_fx.qtf" )

    recQtf = td.ReconstructionQtfLocal( wif , qtf )
    recEta = td.ReconstructionWifLocal( wif  )

    tmax = 2000
    overlap = 200.
    time = np.arange( 0 , tmax , 1.0)

    eta = recEta.evalSe( time )

    #FFT to get the wif by segment
    wif_re = sp.Wifm.FromTs( eta , windows_net = 400 , overlap = overlap , b  = np.pi )
    wif_re = wif_re.optimize(0.995)

    #Reconstructor
    rec_qtf_re = td.ReconstructionMulti( td.ReconstructionQtfLocal , wif_re , qtf )

    qtf_ts = recQtf.evalDf( time )
    qtf_ts_re = rec_qtf_re.evalDf( time )

    ref = qtf_ts.iloc[:,0]
    re =  qtf_ts_re.iloc[:,0]
    err = (re-ref) / ref.abs().max()
    errMax = err.loc[overlap:tmax-overlap].abs().max()

    print ( f"Err max = {errMax:.1%}" )

    if display :
        #Comparison plot
        from matplotlib import pyplot as plt
        fig, ax = plt.subplots()
        qtf_ts.plot(ax=ax)
        qtf_ts_re.plot(ax=ax)
        wif_re.plotBounds(ax=ax)
    assert( errMax < 0.03)

if __name__ == "__main__" :

    from Snoopy import logger
    logger.setLevel(10)

    test_reconstructionMulti_FFT()
    test_reconstructionMulti_QTF(True)




