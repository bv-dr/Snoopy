#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 10:15:13 2020

@author: iten
"""

# To capture the C++ output, run %load_ext wurlitzer in the ipython console
#%load_ext wurlitzer

import Snoopy.Spectral as sp
import Snoopy.TimeDomain as td
import numpy as np
import matplotlib.pyplot as plt
import time
import pandas as pd
from Snoopy.TimeDomain import getPSD
    

def t_qtf_reconstruction(mode = "diff", plot = False, qtfFile = None):
    """
    Test the qtf functionality of the MQtf
    """
    if qtfFile is None:
        if mode == "sum":
            print("ResponseSpectrum2ndMQtf does not support the 'sum' mode")
            return
            #qtfFile = "test_data/spgMode01.qtf"
        else:
            qtfFile = "test_data/qtf_fx.qtf"

    print("Reading qtf (Qtf class)")
    start = time.time()
    qtf = sp.Qtf(qtfFile)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    print("Reading qtf (MQtf class)")
    start = time.time()
    if True:
        mqtf = sp.MQtf()
        mqtf.readQtfFromFile(qtfFile)
    else:
        mqtf = sp.MQtf(qtf)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    
    
    print("Constructing Spectrum")
    start = time.time()
    spec_spr = sp.Jonswap( 1.0, 10.0, 1., 0., sp.SpreadingType.No, 0)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    print("Constructing wif structure")
    start = time.time()
    wif = sp.Wif( spec_spr, nbSeed = 50, wmax = 1.8 )
    end = time.time()
    print("wif's n = ", len(wif.freq))
    print("\tDone. Time elapsed : ", end -start)

    print("wif optimization")
    start = time.time()
    wif = wif.optimize(0.99)
    end = time.time()
    print("wif's n = ", len(wif.freq))
    print("\tDone. Time elapsed : ", end -start)

    print("Constructing ReconstructionMQtfLocal structure")
    start = time.time()
    reconMQtf = td.ReconstructionMQtfLocal(wif, mqtf)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    print("Constructing ReconstructionQtfLocal structure")
    start = time.time()
    reconQtf = td.ReconstructionQtfLocal(wif, qtf)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    t = np.arange( 0., 10000. , .5 )
    print("Computing reconstruction signal for ", t.shape, " time instances")
    print(" 1. ReconstructionMQtfLocal")
    start = time.time()
    mts = reconMQtf( t  )
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)
    print(" 2. ReconstructionQtfLocal")
    start = time.time()
    ts = reconQtf( t  )
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)
    tsMQtf = pd.DataFrame(index = t, data = mts, columns = reconMQtf.getQtf().getModes())
    tsQtf  = pd.DataFrame(index = t, data = ts , columns = reconQtf.getQtf().getModes())

    if plot:
        fig, ax = plt.subplots()
        tsMQtf.plot(ax = ax, label = "MQtf", marker = "o")
        tsQtf.plot(ax = ax, label = "Qtf")
    

    print("getPSD")
    start = time.time()
    psd_timeMQtf = td.getPSD(tsMQtf, dw = 0.025)
    psd_timeQtf  = td.getPSD(tsQtf , dw = 0.025)
    psdMQtf = pd.DataFrame(psd_timeMQtf)
    psdQtf  = pd.DataFrame(psd_timeQtf)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    seaState = sp.SeaState(spec_spr)

    print("Response spectrum MQtf")
    start = time.time()    
    rspMQtf = sp.ResponseSpectrum2ndMQtf(seaState, mqtf)
    frq = rspMQtf.getFrequencies()
    respMQtf = rspMQtf.get()
    rspecMQtf = pd.Series(index = frq, data = respMQtf)
    end = time.time()
    m0MQtf, m2MQtf = rspMQtf.getM0M2()
    print("\tDone. Time elapsed : ", end -start)
    print("m0S = ", m0MQtf, ", m2S = ", m2MQtf)

    print("Response spectrum Qtf")
    start = time.time()    
    rsp = sp.ResponseSpectrum2nd(seaState, qtf)
    frq = rsp.getFrequencies()
    #frq = mqtf.getDeltaFrequencies()
    resp = rsp.get()
    rspec = pd.Series(index = frq, data = resp)
    end = time.time()
    m0, m2 = rsp.getM0M2()
    print("\tDone. Time elapsed : ", end -start)
    print("m0 = ", m0, ", m2 = ", m2)
 
    if plot :
        fig, ax = plt.subplots()
        rspec.plot(ax = ax, label = "Response based on Qtf", marker = "o")
        rspecMQtf.plot(ax = ax, label = "Response based on MQtf")
        psdMQtf.plot(ax = ax, label = "Reconstruction MQtf Local", marker = "*")
        psdQtf.plot(ax = ax, label = "Reconstruction Qtf Local")
        ax.grid()
        ax.legend()
        plt.show()
            
    assert( np.allclose( rspecMQtf , rspec , rtol=0.001 ) )
    assert( np.allclose(psdMQtf, psdQtf))
 
def t_mqtf_reconstruction(mode = "diff", plot = False, qtfFile = None, nbSeed = 50, spreadingNbheading = 100, tmax = 1000, dt = 0.5):
    """
    Test ResponseSpectrumMQtf and ReconstructionMQtfLocal with PSD
    """
    if qtfFile is None:
        if mode == "sum":
            print("ResponseSpectrum2ndMQtf does not support the 'sum' mode")
            return
        else:
            qtfFile = "test_data/munit_00d.qtf"

    print("Reading mqtf")
    start = time.time()
    if False:
        mqtf = sp.MQtf()
        mqtf.readQtfFromFile(qtfFile)   # C++ reader
    else:
        mqtf = sp.MQtf(qtfFile)         # python reader
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)
    
    print("Constructing Spectrum")
    start = time.time()
    spec_spr = sp.Jonswap( 1.0, 10.0, 1., 0., sp.SpreadingType.Cosn, 4)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)
    
    print("Constructing wif structure")
    start = time.time()
    wif = sp.Wif( spec_spr, nbSeed = nbSeed, wmax = 1.8, spreadNbheading = spreadingNbheading )
    end = time.time()
    print("wif's n = ", len(wif.freq))
    print("\tDone. Time elapsed : ", end -start)

    print("wif optimization")
    start = time.time()
    wif = wif.optimize(0.99)
    end = time.time()
    print("wif's n = ", len(wif.freq))
    print("\tDone. Time elapsed : ", end -start)

    print("Constructing ReconstructionMQtfLocal structure")
    start = time.time()
    reconMQtf = td.ReconstructionMQtfLocal(wif, mqtf, degree = 1)
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    t = np.arange( 0., tmax , dt )
    print("Computing reconstruction signal for ", t.shape, " time instances")
    start = time.time()
    ts = reconMQtf( t  )
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)
    #print("Modes:", reconMQtf.getQtf().getModes())
    #ts = 2*np.cos(0.2*t) +np.cos(t) +0.5*np.sin(t*0.8)
    ts = pd.DataFrame(index = t, data = ts, columns = reconMQtf.getQtf().getModes())
    
    
    if plot:
        fig, ax = plt.subplots()
        ts.plot(ax = ax)
    

    print("getPSD")
    start = time.time()
    psd1 = pd.DataFrame(td.getPSD(ts, dw = 0.025))
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)

    seaState = sp.SeaState(spec_spr)

    print("Response spectrum MQtf")
    start = time.time()    
    rsp = sp.ResponseSpectrum2ndMQtf(seaState, mqtf)
    rspec = pd.Series(index = rsp.getFrequencies(), data = rsp.get())
    end = time.time()
    print("\tDone. Time elapsed : ", end -start)
 
    if plot :
        fig, ax = plt.subplots()
        ax.set_title(str(tmax)+"/"+str(dt))
        rspec.plot(ax = ax, label = "Response Spectrum MQtf")
        psd1.plot(ax = ax)
        ax.grid()
        ax.legend()
    if plot:
        plt.show()
        
def mqtf_statistic():
    nbSpread = []
    MQtfS_Interp = []
    MQtfS_Rec = []
    wif_waves = []          # after wif.optimise(0.99)
    
    nbSpread.append(  2);       wif_waves.append(  38);     MQtfS_Interp.append(  23.8157); MQtfS_Rec.append(   0.2005)
    nbSpread.append( 10);       wif_waves.append( 129);     MQtfS_Interp.append(  23.8384); MQtfS_Rec.append(   2.0188)
    nbSpread.append( 20);       wif_waves.append( 272);     MQtfS_Interp.append(  23.8961); MQtfS_Rec.append(   9.8419)
    nbSpread.append( 50);       wif_waves.append( 688);     MQtfS_Interp.append(  25.962);  MQtfS_Rec.append(  62.84)
    nbSpread.append(100);       wif_waves.append(1377);     MQtfS_Interp.append(  37.7609); MQtfS_Rec.append( 251.0375)
    nbSpread.append(150);       wif_waves.append(2073);     MQtfS_Interp.append(  64.5390); MQtfS_Rec.append( 573.4128)
    nbSpread.append(200);       wif_waves.append(2753);     MQtfS_Interp.append( 109.9142); MQtfS_Rec.append(1008.9461)
    nbSpread.append(300);       wif_waves.append(4129);     MQtfS_Interp.append( 466.6722); MQtfS_Rec.append(2268.4533)
    nbSpread.append(500);       wif_waves.append(6884);     MQtfS_Interp.append(3133.4936); MQtfS_Rec.append(6317.5298)
    MQtfS_Interp_shifted = []
    for v in MQtfS_Interp:
        MQtfS_Interp_shifted.append(v - MQtfS_Interp[0])
    MQtfS_Interp_Ratio = []
    MQtfS_Rec_Ratio = []
    wif_waves_Ratio = []
    for i, v in enumerate(MQtfS_Interp):
        if i == 0:
            wif_waves_Ratio.append(1)
            MQtfS_Interp_Ratio.append(1)
            MQtfS_Rec_Ratio.append(1)
        elif i == 1:
            wif_waves_Ratio.append(wif_waves[i]/wif_waves[i-1])
            MQtfS_Interp_Ratio.append(1)
            MQtfS_Rec_Ratio.append(MQtfS_Rec[i]/MQtfS_Rec[i-1])
        else:
            wif_waves_Ratio.append(wif_waves[i]/wif_waves[i-1])
            MQtfS_Interp_Ratio.append(MQtfS_Interp[i]/MQtfS_Interp[i-1])
            MQtfS_Rec_Ratio.append(MQtfS_Rec[i]/MQtfS_Rec[i-1])
    for i in range(len(wif_waves_Ratio)):
        print("Series : ", i +1)
        print("\tn_2/n_1       = ", wif_waves_Ratio[i])
        print("\t(n_2/n_1)^2/2 = ", wif_waves_Ratio[i]**2/2.)
        print("\t(n_2/n_1)^2   = ", wif_waves_Ratio[i]**2)
        print("\tInt_2/Int_1   = ", MQtfS_Interp_Ratio[i])
        print("\tRec_2/Rec_1   = ", MQtfS_Rec_Ratio[i])
    fig, ax = plt.subplots()
#    ax.plot( wif_waves, MQtfS_Interp, label = "MQtf Interpolation", marker = "*")
    ax.plot( wif_waves, MQtfS_Interp_shifted, label = "MQtf Interpolation (shifted to zero)", marker = "*")
#    ax.plot( wif_waves_Ratio, MQtfS_Interp_Ratio, label = "MQtf Interpolation Ratio", marker = "*")
    ax.plot( wif_waves, MQtfS_Rec, label = "MQtf Reconstruction", marker = "o")
#    ax.plot( wif_waves_Ratio, MQtfS_Rec_Ratio, label = "MQtf Reconstraction Ratio", marker = "o")
    plt.grid()
    plt.legend()
    
             
def t_sym(mode = "sum", plot = False, qtfFile = None):
    if qtfFile is None:
        if mode == "sum":
            qtfFile = "test_data/munit_90s.qtf"
        else:
            qtfFile = "test_data/munit_90d.qtf"
    mqtf = sp.MQtf()
    mqtf.readQtfFromFile(qtfFile)
             
    nwaves = 2
    amps = np.ones(nwaves)
    phis = np.zeros(nwaves)
    hdns = np.zeros(nwaves)
    frqs = np.zeros(nwaves)

    ts = np.linspace(0.0, 20.0, 200)
    
    # case 1
    frqs[0] = 1.0;      amps[0] = 1.0;      phis[0] =  1.0;      hdns[0] =  0.
    frqs[1] = 1.5;      amps[1] = 0.5;      phis[1] = -0.5;      hdns[1] = -np.pi/4.
    wif1 = sp.Wif(frqs, amps, phis, hdns, 0.0)
    reconMQtf1 = td.ReconstructionMQtfLocal(wif1, mqtf)
    res1 = reconMQtf1(ts)

    # case 2
    frqs[0] = 1.5;      amps[0] = 0.5;      phis[0] = -0.5;      hdns[0] = -np.pi/4.
    frqs[1] = 1.0;      amps[1] = 1.0;      phis[1] =  1.0;      hdns[1] =  0.
    wif2 = sp.Wif(frqs, amps, phis, hdns, 0.0)
    reconMQtf2 = td.ReconstructionMQtfLocal(wif2, mqtf)
    res2 = reconMQtf2(ts)
    
    if plot:
        fig, ax = plt.subplots()
        plt.plot(ts, res1, label = "Case 1")
        plt.plot(ts, res2, label = "Case 2")
        #ax.set_xticks(np.linspace(0, 20, 9))
        #ax.set_yticks(np.linspace(-100, 100, 81))
        #ax.set_yticks(np.linspace(-100, 100, 201))
        #ax.set_ylim(-5, 5)
        #ax.set_xlim(0,20)
        plt.grid()
        plt.legend()
        plt.show()
        
    assert (np.allclose(res1, res2))
        
def t_2vs1(a1 = 0.3, a2 = 0.8, mode = "diff", qtfFile = None, plot = False):
    if qtfFile is None:
        if mode == "diff":
            qtfFile = "test_data/munit_00d.qtf"
        else:
            qtfFile = "test_data/munit_00s.qtf"
    mqtf = sp.MQtf()
    mqtf.readQtfFromFile(qtfFile)

    ts = np.arange(0.0, 82.0, 0.1)
    
    nwaves = 2
    amps = np.ones(nwaves)
    phis = np.zeros(nwaves)
    hdns = np.zeros(nwaves)
    frqs = np.zeros(nwaves)
    
    frqs[0] = 1.0;      amps[0] = a1;      phis[0]  =  0.0;    hdns[0] = 0.0
    frqs[1] = 1.0;      amps[1] = a2;      phis[1]  =  0.0;    hdns[1] = 0.0
    
    wif1 = sp.Wif(frqs, amps, phis, hdns, -1.0)
    reconMQtf1 = td.ReconstructionMQtfLocal(wif1, mqtf)
    res1 = reconMQtf1(ts)

    nwaves = 1
    amps = np.ones(nwaves)
    phis = np.zeros(nwaves)
    hdns = np.zeros(nwaves)
    frqs = np.zeros(nwaves)
    
    frqs[0] = 1.0;      amps[0] = a1 +a2;      phis[0]  =  0.0;    hdns[0] = 0.0
    
    wif2 = sp.Wif(frqs, amps, phis, hdns, -1.0)
    reconMQtf2 = td.ReconstructionMQtfLocal(wif2, mqtf)
    res2 = reconMQtf2(ts)
    
    if plot:
        fig, ax = plt.subplots()
        plt.plot(ts, res1, label = "Rec 1", marker = "*")
        plt.plot(ts, res2, label = "Rec 2")
        plt.grid()
        plt.legend()
        plt.show()
        
    assert( np.allclose(res1, res2) )

def test_qtf_reconstruction(plot = False):
    t_qtf_reconstruction(mode = "diff", plot = plot, qtfFile = None)
    
def test_mqtf_reconstruction(plot = False):
    #t_mqtf_reconstruction(mode = "diff", plot = plot, qtfFile = None, nbSeed = 50, spreadingNbheading = 50, tmax = 2000, dt = 0.5)
    t_mqtf_reconstruction(mode = "diff", plot = plot, qtfFile = "test_data/mqtf_fx.qtf", nbSeed = 50, spreadingNbheading = 50, tmax = 2000, dt = 0.5)

def test_2vs1(plot = False):
    # 2 waves of amps a1 and a2 vs 1 wave of amp a1 +a2
    t_2vs1(a1 = 0.3, a2 = 0.8, mode = "diff", qtfFile = None, plot = plot)   # result: line of the value     : (1.1)^2 = 1.21
    #t_2vs1(a1 = 0.3, a2 = 0.8, mode = "sum", qtfFile = None, plot = plot)    #         wave of the amplitude : (1.1)^2 = 1.21
    t_2vs1(a1 = 0.3, a2 = 0.8, qtfFile = "test_data/mqtf_fx.qtf", plot = plot)

def test_sym(plot = False):
    t_sym(mode = "sum", plot = plot)
    #t_sym(mode = "diff", plot = plot)
    t_sym(mode = "diff", plot = plot, qtfFile = "test_data/mqtf_fx.qtf")

def spreading(qtfFile = None, plot = False):
    """
    Test how the response spectrum depends on the spreading coefficient
    for the n >=45 the integral of the spreading function is more than 1 for the test's mqtf data file (d\beta = 15°)
    
    The test is valid only for the difference mode, since ResponseSpectrum2ndMQtf is not implemented for the sum mode
    """
    specs = []
    cosns = [2, 4, 8, 16, 32, 45, 0]  # n > 45 The integral of the Spreading function is more than 1
    mks   = ['*', 'o', '.', '', '^', '>', '<', 'v']
    for cosn in cosns:
        if cosn <= 0:
            specs.append(sp.Jonswap(1.0, 10.0, 1., 0., sp.SpreadingType.No, 0))
        else:
            specs.append(sp.Jonswap(1.0, 10.0, 1., 0., sp.SpreadingType.Cosn, cosn))
    
    if qtfFile is None:
        qtfFile = "test_data/munit_00d.qtf"
    mqtf = sp.MQtf()
    mqtf.readQtfFromFile(qtfFile)
    
    seaStates = []
    for spec in specs:
        seaStates.append(sp.SeaState(spec))
    rsps = []
    for iseaState, seaState in enumerate(seaStates):
        rsps.append(sp.ResponseSpectrum2ndMQtf(seaState, mqtf))

    frq = rsps[0].getFrequencies()
    rspecs = []
    for irsp, rsp in enumerate(rsps):
        rspecs.append(pd.Series(index = frq, data = rsp.get()))
        
    if plot:
        fig, ax = plt.subplots()
        for irspec, rspec in enumerate(rspecs):
            rspec.plot(ax = ax, label = "Response "+str(cosns[irspec]), marker = mks[irspec])
        ax.grid()
        ax.legend()
        plt.show()

    

if __name__ == "__main__":
    plot = False
    #plot = True
    
    test_qtf_reconstruction(plot = plot)

    test_mqtf_reconstruction(plot = plot)
    
    # time used for the reconstruction and interpolation (construction of the object and interpolation) vs number of waves
    #mqtf_statistic()
    
    # symmetry test
    test_sym(plot = plot)
    
    # 2 waves of amps a1 and a2 vs 1 wave of amp a1 +a2
    test_2vs1(plot = plot)
    
    #spreading(plot = plot)
    #spreading(qtfFile = "test_data/mqtf_fx.qtf", plot = plot)