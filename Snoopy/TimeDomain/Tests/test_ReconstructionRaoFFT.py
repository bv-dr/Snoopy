import time
import numpy as np
from matplotlib import pyplot as plt
from Snoopy import Spectral as sp
from Snoopy.TimeDomain import ReconstructionRaoLocalFFT, ReconstructionRaoLocal

def test_reconstructionRaoFFT(display = False):
    spec = sp.Jonswap( 1, 10, 1 )
    ss = sp.SeaState( spec )
    
    rao = sp.Rao(r"../../Spectral/Tests/test_data/my_10_0speed.rao").getSorted()
    wif = sp.Wif( ss, w = np.arange( 0.2, 1.80, 0.01 ) , seed = 1)

    timeVect = np.arange( 0., 3600, 0.5 )

    t0 = time.time()
    recRaoFFT = ReconstructionRaoLocalFFT( wif, rao, dt_approx = 0.1, k = 3 )
    ts_fft = recRaoFFT(timeVect)

    t1 = time.time()

    recRao = ReconstructionRaoLocal( wif, rao )
    ts_wif = recRao( timeVect )
    t2 = time.time()

    print (f"Classic {t2-t1:.2f}s" )
    print (f"FFT {t1-t0:.2f}s" )

    assert (np.isclose( ts_wif[:,0], ts_fft[:,0], rtol = 3e-2 ).all())

    if display :
        fig, ax = plt.subplots()
        ax.plot( timeVect, ts_fft , label = "FFT")
        ax.plot( timeVect, ts_fft , label = "wif" )
        ax.legend(loc = 1)

if __name__ == "__main__" :

    test_reconstructionRaoFFT(display = True)
