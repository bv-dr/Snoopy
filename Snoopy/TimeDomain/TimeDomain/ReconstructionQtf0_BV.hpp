#pragma once

#include <memory>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/Qtf0.hpp"
#include "TimeDomain/Reconstruction.hpp"

namespace BV {
namespace TimeDomain {

class ReconstructionQtf0ABC: public ReconstructionABC
{
protected:
    Spectral::Qtf0 data_ ;
    BV::Math::Interpolators::InterpScheme interpScheme_ ;
    BV::Math::Interpolators::ExtrapolationType extrapType_ ;
public:
    ReconstructionQtf0ABC(std::shared_ptr<const Spectral::Wif> p_wif,
                          const Spectral::Qtf0 & data,
                          const Math::Interpolators::InterpScheme & interpScheme=
                              Math::Interpolators::InterpScheme::LINEAR,
                          Math::Interpolators::ExtrapolationType extrapType=
                              Math::Interpolators::ExtrapolationType::BOUNDARY,
                          unsigned int numThreads=1) :
        ReconstructionABC(p_wif, numThreads),
        data_(data), interpScheme_(interpScheme), extrapType_(extrapType)
    {
    }
} ;

class TIME_DOMAIN_API ReconstructionQtf0_BVLocal : public ReconstructionQtf0ABC
{
protected:
    Eigen::ArrayXXd dataInterpolated_ ; // Frequencies and headings interpolation

    Eigen::ArrayXd getArgument_(double time) const ;

public:
    ReconstructionQtf0_BVLocal(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Qtf0 & data,
        const Math::Interpolators::InterpScheme & interpScheme =
            Math::Interpolators::InterpScheme::LINEAR,
        Math::Interpolators::ExtrapolationType extrapType=
            Math::Interpolators::ExtrapolationType::BOUNDARY,
        unsigned int numThreads=1) ;

    virtual ~ReconstructionQtf0_BVLocal() {}

    Eigen::ArrayXd operator()() const ;

    virtual Eigen::ArrayXd operator()(double time) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;

} ;

class TIME_DOMAIN_API ReconstructionQtf0_BV: public ReconstructionQtf0ABC
{
protected:
    Spectral::Qtf0 dataInterpolated_ ; // Frequencies interpolation

    Eigen::ArrayXd getArgument_(double time, double bodyX, double bodyY,
                                double bodyHeading) const ;

public:
    ReconstructionQtf0_BV(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Qtf0 & data,
        const Math::Interpolators::InterpScheme & interpScheme =
            Math::Interpolators::InterpScheme::LINEAR,
        Math::Interpolators::ExtrapolationType extrapType=
            Math::Interpolators::ExtrapolationType::BOUNDARY,
        unsigned int numThreads=1) ;

    virtual ~ReconstructionQtf0_BV() {}

    virtual Eigen::ArrayXd operator()(double heading) const ;

    virtual Eigen::ArrayXd operator()(double time, double xGlobal,
                                      double yGlobal, double heading) const ;
    Eigen::ArrayXd operator()(double time,
                              const Eigen::Vector3d & posHeading) const ;

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times,
                               const Eigen::ArrayXd & xs,
                               const Eigen::ArrayXd & ys,
                               const Eigen::ArrayXd & headings) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times,
                               const Eigen::ArrayX3d & posHeadings) const ;

} ;

}
}
