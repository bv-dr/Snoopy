#include "ReconstructionQtf_WaveCurrentInteractionKim.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"
#include "Spectral/Dispersion.hpp"

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

Eigen::ArrayXd ReconstructionQtf_WaveCurrentInteractionKim::operator()(
    double time, double xGlobal, double yGlobal, double bodyHeading,
    double relativeVelocityLocalX, double relativeVelocityLocalY) const
{
    Eigen::ArrayXd headings(p_wif_->getIndependentHeadings()) ;
    if (headings.size() > 1)
    {
        throw BV::Tools::Exceptions::BVException(
            "No spreading allowed for Qtf reconstruction.") ;
    }
    const double relativeHeading = WrapAngle0_2PI(headings(0) - bodyHeading) ;
    QtfTensorComplex complexData(
        dataInterpolated_.getComplexAtHeading(relativeHeading, interpScheme_,
                                              interpStrategy_, extrapType_)) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero (nModes_)) ;
    //const Eigen::Index nws(p_wif_->getFrequencies().size()) ;
    const Eigen::Index nws(WJ_WI_.size());
// Fill the unchanged part of full qtf loads
// resFrequency interpolation only valid for surge and sway motions
// FIXME copy/paste from Reconstruction_Qtf  -> to factorise
    double projDistance(updateProjDistance_(xGlobal, yGlobal, bodyHeading)) ;
    for (Eigen::Index iw = 0; iw < nws; ++iw)
    {
        Eigen::ArrayXd argument(getArgument_(iw, time, projDistance)) ;
        std::complex<double> val ;
        Eigen::ArrayXd dws(WJ_WI_[iw]) ;
        Eigen::Index ndws(dws.size()) ;
        for (Qtf::IndexType iMode = 2; iMode < nModes_; ++iMode)
        {
            val = complexData.getFromiWjDW(0, iw, 0, iMode) ;
            double carg(std::cos(std::arg(val))) ;
            if (qtfMode_ > 0.)
            {
                carg = std::cos(argument(0) + std::arg(val)) ;
            }
            res(iMode) += AJ_AI_[iw](0) * std::abs(val) * carg ;
            for (Eigen::Index idw = 1; idw < ndws; ++idw)
            {
                if (WJmWI_[iw](idw) <= dwMax_)
                {
                    val = complexData.getFromiWjDW(0, iw, idw, iMode) ;
                    res(iMode) += AJ_AI_[iw](idw) * 2. * std::abs(val)
                        * std::cos(argument(idw) + qtfMode_ * std::arg(val)) ;
                }
            }
        }
    }
// Fill the W C interaction part
    const Eigen::ArrayXd & ws(p_wif_->getFrequencies()) ;
    const Eigen::VectorXd & qtfHeadings(data_.getHeadings()) ;
    const Eigen::VectorXd & qtfFrequencies(data_.getFrequencies()) ;
    const Eigen::VectorXd & qtfDFrequencies(data_.getDeltaFrequencies()) ;
//    const BV::Tools::Array::Array<double, 4> & qtfArrayValues(
//        data_->getArrayValues()) ;
// Get uL and uT
    const double longitudinalVelocity(
        getLongitudinalVelocity_(relativeVelocityLocalX, relativeVelocityLocalY,
                                 relativeHeading)) ;
    const double transversalVelocity(
        getTransversalVelocity_(relativeVelocityLocalX, relativeVelocityLocalY,
                                relativeHeading)) ;
    for (Eigen::Index iw = 0; iw < nws; ++iw)
    {
        const double omega1(ws(iw)) ;
        const double invCp1(invPhaseVelocity_(iw)) ;
        const double encounterFreq1(
            omega1 * (1. - longitudinalVelocity * invCp1)) ;
        const double encounterHead1(
            BV::Math::WrapAngle0_2PI(
                relativeHeading - 2. * transversalVelocity * invCp1)) ;
        Eigen::ArrayXd argument(getArgument_(iw, time, projDistance)) ;
        std::complex<double> qtfValue ;
        Eigen::ArrayXd dws(WJ_WI_[iw]) ;
        Eigen::Index ndws(dws.size()) ;
        Eigen::Tensor<std::complex<double>, 1> qtfTensor(
            data_.get(encounterHead1, encounterFreq1, 0, interpScheme_,
                       interpStrategy_, frequencyInterpStrategy_,
                       extrapType_)) ;
        const double Ak1(1. - 4. * longitudinalVelocity * invCp1) ;
        for (Qtf::IndexType iMode = 0; iMode < 2; ++iMode)
        {
            qtfValue = qtfTensor(iMode) ;
            qtfValue *= Ak1 ;
            double carg(std::cos(std::arg(qtfValue))) ;
            if (qtfMode_ > 0.)
            {
                carg = std::cos(argument(0) + std::arg(qtfValue)) ;
            }
            res(iMode) += AJ_AI_[iw](0) * std::abs(qtfValue) * carg ;
        }
        for (Eigen::Index idw = 1; idw < ndws; ++idw)
        {
            if (WJmWI_[iw](idw) <= dwMax_)
            {
                const auto iW2(iw + idw) ;
                const double omega2(ws(iW2)) ;
                const double invCp2(invPhaseVelocity_(iW2)) ;
                const double encounterFreq2(
                    omega2 * (1. - longitudinalVelocity * invCp2)) ;
                const double encounterHead(
                    BV::Math::WrapAngle0_2PI(
                        relativeHeading
                            - 2. * transversalVelocity * (invCp1 + invCp2))) ;
                const double Akij(
                    1. - 2. * longitudinalVelocity * (invCp1 + invCp2)) ;
                Eigen::Tensor<std::complex<double>, 1> qtfTensor(
                    data_.get(encounterHead, encounterFreq1,
                               encounterFreq2 - encounterFreq1, interpScheme_,
                               ComplexInterpolationStrategies::AMP_PHASE,
                               frequencyInterpStrategy_, extrapType_)) ;
                for (Qtf::IndexType iMode = 0; iMode < 2; ++iMode)
                {
                    qtfValue = qtfTensor(iMode) ;
                    qtfValue *= Akij ;
                    res(iMode) += AJ_AI_[iw](idw) * 2. * std::abs(qtfValue)
                        * std::cos(
                            argument(idw) + qtfMode_ * std::arg(qtfValue)) ;
                }
            }
        }
    }
    return res ;
}
