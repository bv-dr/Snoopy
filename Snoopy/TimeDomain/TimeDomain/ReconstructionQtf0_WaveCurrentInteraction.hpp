#pragma once

#include <memory>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"
#include "Math/Functions/ABC.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/Qtf0.hpp"
#include "TimeDomain/ReconstructionQtf0_BV.hpp"

namespace BV {
namespace TimeDomain {

// TODO local reconstruction must set constant relative current velocity...

class TIME_DOMAIN_API ReconstructionQtf0_WaveCurrentInteraction: public ReconstructionQtf0_BV
{
    /*!
     * \brief Vessel relative velocity / current projected along the wave spectrum directional vector.
     */
    inline const Eigen::ArrayXd getLongitudinalVelocities_(
        double vx, double vy, const Eigen::ArrayXd & relativeHeadings) const
    {
        return vx * Eigen::cos(relativeHeadings)
            + vy * Eigen::sin(relativeHeadings) ;
    }

    /*!
     * \brief Vessel relative velocity / current projected along the wave spectrum orthogonal vector.
     */
    inline const Eigen::ArrayXd getTransversalVelocities_(
        double vx, double vy, const Eigen::ArrayXd & relativeHeadings) const
    {
        return -vx * Eigen::sin(relativeHeadings)
            + vy * Eigen::cos(relativeHeadings) ;
    }

public:
    ReconstructionQtf0_WaveCurrentInteraction(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Qtf0 & data,
        const Math::Interpolators::InterpScheme & interpScheme =
            Math::Interpolators::InterpScheme::LINEAR,
        Math::Interpolators::ExtrapolationType extrapType=
            Math::Interpolators::ExtrapolationType::BOUNDARY,
        unsigned int numThreads=1) ;

    Eigen::ArrayXd operator()(double bodyHeading,
                              double relativeVelocityLocalX,
                              double relativeVelocityLocalY) const ;

    Eigen::ArrayXd operator()(double time, double xGlobal, double yGlobal,
                              double heading, double relativeVelocityLocalX,
                              double relativeVelocityLocalY) const ;
    Eigen::ArrayXd operator()(
        double time, const Eigen::Vector3d & posHeading,
        const Eigen::Vector2d & relativeVelocityLocal) const ;
    Eigen::ArrayXXd operator()(
        const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
        const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings,
        const Eigen::ArrayXd & relativeVelocitiesLocalX,
        const Eigen::ArrayXd & relativeVelocitiesLocalY) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;
    Eigen::ArrayXXd operator()(
        const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings,
        const Eigen::ArrayX2d & relativeVelocitiesLocal) const ;
} ;

}
}
