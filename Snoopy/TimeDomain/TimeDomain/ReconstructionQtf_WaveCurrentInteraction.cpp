#include "ReconstructionQtf_WaveCurrentInteraction.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"
#include "Spectral/Dispersion.hpp"

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

ReconstructionQtf_WaveCurrentInteraction::ReconstructionQtf_WaveCurrentInteraction(
    std::shared_ptr<const Wif> p_wif, const Qtf & data,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const FrequencyInterpolationStrategies & frequencyInterpStrategy,
    Interpolators::ExtrapolationType extrapType, double dwMax,
    unsigned int numThreads) :
    ReconstructionQtf(p_wif, data, interpScheme, interpStrategy,
                      frequencyInterpStrategy, extrapType, dwMax, numThreads)
{
    const Eigen::Index nws(p_wif_->getFrequencies().size()) ;
    const double g(9.81) ; // FIXME set in parameters
    double h(p_wif_->getDepth()) ;
    const Eigen::ArrayXd & ks(p_wif_->getWaveNumbers()) ;
    Eigen::VectorXd invWavePhaseVelocity(nws) ;
    Eigen::VectorXd invWaveGroupVelocity(nws) ;
    Eigen::VectorXd dAlphaDOmegaRatio(nws) ;
    for (int iFreq = 0; iFreq < nws; ++iFreq)
    {
        const double k(ks(iFreq)) ;
        if (IsNull(k))
        {
            invWavePhaseVelocity(iFreq) = 0. ;
            invWaveGroupVelocity(iFreq) = 0. ;
            dAlphaDOmegaRatio(iFreq) = 0. ;
            continue ;
        }
        invWavePhaseVelocity(iFreq) = 1. / k2Cp(k, h, g) ;
        invWaveGroupVelocity(iFreq) = 1. / k2Cg(k, h, g) ;
        if (h < 1.e-4)
        {
            dAlphaDOmegaRatio(iFreq) = 0. ;
        }
        else
        {
            const double kh(k * h) ;
            dAlphaDOmegaRatio(iFreq) = h * (1. - 2. * kh / std::tanh(2. * kh))
                * invWaveGroupVelocity(iFreq) / std::sinh(2. * kh) ;
        }
    }
    invPhaseVelocity_ = invWavePhaseVelocity ;
    invGroupVelocity_ = invWaveGroupVelocity ;
    dAlfaOmegaRatio_ = dAlphaDOmegaRatio ;
}

Eigen::ArrayXd ReconstructionQtf_WaveCurrentInteraction::operator()(
    double bodyHeading,
    double relativeVelocityLocalX,
    double relativeVelocityLocalY) const
{
    if (qtfMode_ > 0.)
    {
        throw BV::Tools::Exceptions::BVException("Mean load not implemented yet for sum frequencies") ;
    }
    const double gravity(9.81) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes_)) ;

    Eigen::ArrayXd headings(p_wif_->getIndependentHeadings()) ;
    if (headings.size() > 1)
    {
        throw BV::Tools::Exceptions::BVException(
            "No spreading allowed for Qtf reconstruction.") ;
    }
    double relativeHeading = WrapAngle0_2PI(headings(0) - bodyHeading) ;
    QtfTensorComplex complexData(
        dataInterpolated_.getComplexAtHeading(relativeHeading, interpScheme_,
                                              interpStrategy_, extrapType_)) ;// Fill the unchanged part of full qtf loads
// resFrequency interpolation only valid for surge and sway motions
// FIXME copy/paste from Reconstruction_Qtf  -> to factorise
    const Eigen::Index nws(WJ_WI_.size()); 
    for (Eigen::Index iw = 0; iw < nws; ++iw)
    {
        std::complex<double> val ;
        for (Qtf::IndexType iMode = 2; iMode < nModes_; ++iMode)
        {
            val = complexData.getFromiWjDW(0, iw, 0, iMode) ;
            double carg(std::cos(std::arg(val))) ;
            meanLoad(iMode) += AJ_AI_[iw](0) * std::real(val) ;
        }
    }
// Fill the W C interaction part
    const Eigen::ArrayXd & ws(p_wif_->getFrequencies()) ;
    // Get uL and uT
    const double longitudinalVelocity(
        getLongitudinalVelocity_(relativeVelocityLocalX, relativeVelocityLocalY,
                                 relativeHeading)) ;
    const double transversalVelocity(
        getTransversalVelocity_(relativeVelocityLocalX, relativeVelocityLocalY,
                                relativeHeading)) ;
    for (Eigen::Index iw = 0; iw < nws; ++iw)
    {
        const double omega1(ws(iw)) ;
        const double invCp1(invPhaseVelocity_ (iw)) ;
        const double invCg1(invGroupVelocity_ (iw)) ;
        const double dAlfadOmegaRatio1(dAlfaOmegaRatio_ (iw)) ;
        const double encounterFreq1(
            omega1 * (1. - longitudinalVelocity * invCp1)) ;
        const double encounterHead1(
            BV::Math::WrapAngle0_2PI(
                relativeHeading - transversalVelocity * invCg1)) ;
        const double Ak1(
            1.
                + (omega1 * dAlfadOmegaRatio1 - 2.) * longitudinalVelocity
                    * invCg1) ;
        std::complex<double> qtfValue ;
        Eigen::ArrayXd dws(WJ_WI_[iw]) ;
        Eigen::Index ndws(dws.size()) ;
        Eigen::Tensor<std::complex<double>, 1> qtfTensor(
            data_.get(encounterHead1, encounterFreq1, 0, interpScheme_,
                       interpStrategy_, frequencyInterpStrategy_, extrapType_)) ;
        for (Qtf::IndexType iMode = 0; iMode < 2; ++iMode)
        {
            qtfValue = qtfTensor(iMode) ;
            qtfValue *= Ak1 ;
            meanLoad(iMode) += AJ_AI_[iw](0) * std::real(qtfValue) ;
        }
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtf_WaveCurrentInteraction::operator()(
    double time, double xGlobal, double yGlobal, double bodyHeading,
    double relativeVelocityLocalX, double relativeVelocityLocalY) const
{
    Eigen::ArrayXd headings(p_wif_->getIndependentHeadings()) ;
    if (headings.size() > 1)
    {
        throw BV::Tools::Exceptions::BVException(
            "No spreading allowed for Qtf reconstruction.") ;
    }
    double relativeHeading = WrapAngle0_2PI(headings(0) - bodyHeading) ;
    QtfTensorComplex complexData(
        dataInterpolated_.getComplexAtHeading(relativeHeading, interpScheme_,
                                              interpStrategy_, extrapType_)) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero (nModes_)) ;
    const Eigen::Index nws(WJ_WI_.size());
// Fill the unchanged part of full qtf loads
// resFrequency interpolation only valid for surge and sway motions
// FIXME copy/paste from Reconstruction_Qtf  -> to factorise
    double projDistance(updateProjDistance_(xGlobal, yGlobal, bodyHeading)) ;
    for (Eigen::Index iw = 0; iw < nws; ++iw)
    {
        Eigen::ArrayXd argument(getArgument_(iw, time, projDistance)) ;
        std::complex<double> val ;
        Eigen::ArrayXd dws(WJ_WI_[iw]) ;
        Eigen::Index ndws(dws.size()) ;
        for (Qtf::IndexType iMode = 2; iMode < nModes_; ++iMode)
        {
            val = complexData.getFromiWjDW(0, iw, 0, iMode) ;
            double carg(std::cos(std::arg(val))) ;
            if (qtfMode_ > 0.)
            {
                carg = std::cos(argument(0) + std::arg(val)) ;
            }
            res(iMode) += AJ_AI_[iw](0) * std::abs(val) * carg ;
            for (Eigen::Index idw = 1; idw < ndws; ++idw)
            {
                if (WJmWI_[iw](idw) <= dwMax_)
                {
                    val = complexData.getFromiWjDW(0, iw, idw, iMode) ;
                    res(iMode) += AJ_AI_[iw](idw) * 2. * std::abs(val)
                        * std::cos(argument(idw) + qtfMode_ * std::arg(val)) ;
                }
            }
        }
    }
// Fill the W C interaction part
    const Eigen::ArrayXd & ws(p_wif_->getFrequencies()) ;
    const Eigen::VectorXd & qtfHeadings(data_.getHeadings()) ;
    const Eigen::VectorXd & qtfFrequencies(data_.getFrequencies()) ;
    const Eigen::VectorXd & qtfDFrequencies(data_.getDeltaFrequencies()) ;
    // Get uL and uT
    const double longitudinalVelocity(
        getLongitudinalVelocity_(relativeVelocityLocalX, relativeVelocityLocalY,
                                 relativeHeading)) ;
    const double transversalVelocity(
        getTransversalVelocity_(relativeVelocityLocalX, relativeVelocityLocalY,
                                relativeHeading)) ;
    for (Eigen::Index iw = 0; iw < nws; ++iw)
    {
        const double omega1(ws(iw)) ;
        const double invCp1(invPhaseVelocity_ (iw)) ;
        const double invCg1(invGroupVelocity_ (iw)) ;
        const double dAlfadOmegaRatio1(dAlfaOmegaRatio_ (iw)) ;
        const double encounterFreq1(
            omega1 * (1. - longitudinalVelocity * invCp1)) ;
        const double encounterHead1(
            BV::Math::WrapAngle0_2PI(
                relativeHeading - transversalVelocity * invCg1)) ;
        const double Ak1(
            1.
                + (omega1 * dAlfadOmegaRatio1 - 2.) * longitudinalVelocity
                    * invCg1) ;
        Eigen::ArrayXd argument(getArgument_(iw, time, projDistance)) ;
        std::complex<double> qtfValue ;
        Eigen::ArrayXd dws(WJ_WI_[iw]) ;
        Eigen::Index ndws(dws.size()) ;
        Eigen::Tensor<std::complex<double>, 1> qtfTensor(
            data_.get(encounterHead1, encounterFreq1, 0, interpScheme_,
                       interpStrategy_, frequencyInterpStrategy_,
                       extrapType_)) ;
        for (Qtf::IndexType iMode = 0; iMode < 2; ++iMode)
        {
            qtfValue = qtfTensor(iMode) ;
            qtfValue *= Ak1 ;
            double carg(std::cos(std::arg(qtfValue))) ;
            if (qtfMode_ > 0.)
            {
                carg = std::cos(argument(0) + std::arg(qtfValue)) ;
            }
            res(iMode) += AJ_AI_[iw](0) * std::abs(qtfValue) * carg ;
        }
        for (Eigen::Index idw = 1; idw < ndws; ++idw)
        {
            if (WJmWI_[iw](idw) <= dwMax_)
            {
                const auto iW2(iw + idw) ;
                const double omega2(ws(iW2)) ;
                const double invCp2(invPhaseVelocity_(iW2)) ;
                const double invCg2(invGroupVelocity_(iW2)) ;
                const double dAlfadOmegaRatio2(dAlfaOmegaRatio_(iW2)) ;
                const double encounterFreq2(
                    omega2 * (1. - longitudinalVelocity * invCp2)) ;
                const double encounterHead(
                    BV::Math::WrapAngle0_2PI(
                        relativeHeading
                            - transversalVelocity * (invCg1 + invCg2) / 2.)) ;
                const double Ak2(
                    1.
                        + (omega2 * dAlfadOmegaRatio2 - 2.)
                            * longitudinalVelocity * invCg2) ;
                const double Akij((Ak1 + Ak2) / 2.) ;
                Eigen::Tensor<std::complex<double>, 1> qtfTensor(
                    data_.get(encounterHead, encounterFreq1,
                               encounterFreq2 - encounterFreq1, interpScheme_,
                               ComplexInterpolationStrategies::AMP_PHASE, frequencyInterpStrategy_,
                               extrapType_)) ;
                for (Qtf::IndexType iMode = 0; iMode < 2; ++iMode)
                {
                    qtfValue = qtfTensor(iMode) ;
                    qtfValue *= Akij ;
                    res(iMode) += AJ_AI_[iw](idw) * 2. * std::abs(qtfValue)
                        * std::cos(
                            argument(idw) + qtfMode_ * std::arg(qtfValue)) ;
                }
            }
        }
    }
    return res ;
}

Eigen::ArrayXd ReconstructionQtf_WaveCurrentInteraction::operator()(
    double time, const Eigen::Vector3d & posHeading,
    const Eigen::Vector2d & relativeVelocityLocal) const
{
    return (*this)(time, posHeading(0), posHeading(1), posHeading(2),
                   relativeVelocityLocal(0), relativeVelocityLocal(1)) ;
}

Eigen::ArrayXXd ReconstructionQtf_WaveCurrentInteraction::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
    const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings,
    const Eigen::ArrayXd & relativeVelocitiesLocalX,
    const Eigen::ArrayXd & relativeVelocitiesLocalY) const
{
    if ((times.size() != xs.size()) || (times.size() != ys.size())
        || (times.size() != headings.size()))
        throw BV::Tools::Exceptions::BVException(
            "Wrong sizes for times, xs, ys and headings") ;
    Eigen::ArrayXXd response(times.size(), data_.getNModes()) ;

#pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), xs(i), ys(i), headings(i),
                                  relativeVelocitiesLocalX(i),
                                  relativeVelocitiesLocalY(i)) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf_WaveCurrentInteraction::operator()(
    const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), data_.getNModes()) ;

#pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), 0., 0., 0., 0., 0.) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf_WaveCurrentInteraction::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings,
    const Eigen::ArrayX2d & relativeVelocitiesLocal) const
{
    return (*this)(times, posHeadings.col(0), posHeadings.col(1),
                   posHeadings.col(2), relativeVelocitiesLocal.col(0),
                   relativeVelocitiesLocal.col(1)) ;
}
