#pragma once

#include <memory>
#include <Eigen/Dense>
#include "TimeDomainExport.hpp"

namespace BV {
namespace TimeDomain {

    // Translation of M.Olagnon cptrf1 fortran routine
    //cptrf1  : execute le comptage rainflow suivant la recommandation
    //AFNOR A03 - 406
    //On fournit le tableau ixtrt des indices tels que
    //(extrema(i), extrema(ixtrt(i))) soit un demi - cycle
    //N.B.Quand i = ixtrt(ixtrt(i)), on a un cycle,
    //sinon, c'est un demi-cycle du residu.
    TIME_DOMAIN_API Eigen::ArrayXi cptrf1(Eigen::ArrayXd extrema);


    // Translation of M.Olagnon getExtrema fortran routine
    // Extract extrema from signal.
    TIME_DOMAIN_API Eigen::ArrayXd getExtrema(Eigen::ArrayXd signal);

}
}