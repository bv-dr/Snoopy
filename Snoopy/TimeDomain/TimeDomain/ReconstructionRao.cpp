#include "ReconstructionRao.hpp"
#include "Tools/BVException.hpp"
#include "Tools/EigenUtils.hpp"
#include "Math/Tools.hpp"


using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

ReconstructionRaoLocal::ReconstructionRaoLocal(
    std::shared_ptr<const Wif> p_wif, const Rao & data,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    Interpolators::ExtrapolationType extrapType, unsigned int numThreads, bool forceRun) :
    ReconstructionRaoABC(p_wif, data, interpScheme, interpStrategy, extrapType,
        numThreads, forceRun)
{   
if ( ( (data_.isReadyForInterpolation())||(forceRun) ) == false)
    {
        throw BV::Tools::Exceptions::BVException(
            "RAO is not ready for interpolation ! \n Make sure RAO is defined on 0-360 deg (included)\n If needed, use .getSymetrized() to symetrize RAO\n or .getSorted(duplicatesBounds=True) to duplicates its 0 degree value at 360");
    }

        // We can also directly interpolate the heading as wif is in local axis system
    BV::Spectral::Rao freqInterp(
        data_.getRaoAtFrequencies(p_wif_->getFrequencies(), interpScheme,
            interpStrategy, extrapType));
    dataInterpolated_ = freqInterp.getComplexAtHeadings(
        p_wif_->getIndependentHeadings(), p_wif_->getIndependentHeadingsIndices(),
        interpScheme_, interpStrategy_, extrapType_);


    encFreq_ = p_wif_->getEncounterFrequencies(data_.getForwardSpeed());

    auto nmodes = data_.getNModes();
    auto nwaves = p_wif_->getAmplitudes().size();
    std::complex<double> j(0.0, 1.0);
    fourierCoefs_ = Eigen::ArrayXXcd(nwaves, nmodes);
    for (int iMode = 0; iMode < data_.getNModes(); ++iMode)
    {
        fourierCoefs_.col(iMode) = dataInterpolated_.col(iMode) * p_wif_->getAmplitudes() * Eigen::exp(j * p_wif_->getPhases());
    }

    //Optimization for spreaded case, gather components with exact same encounter frequency.
    if ( p_wif_->getUniDirectional() == false)
    {
        Eigen::ArrayXd fourier_freqs(BV::Tools::Unique(encFreq_));
        Eigen::Index nFreqs(fourier_freqs.size());
        Eigen::ArrayXXcd cvalues(nFreqs, data_.getNModes());
        for (Eigen::Index iFreq = 0; iFreq < nFreqs; ++iFreq)
        {
            for (int iMode = 0; iMode < data_.getNModes(); ++iMode)
            {
                std::complex<double> sum(0.);
                for ( int iwe = 0; iwe < nwaves; ++iwe )
                {
                    if ( encFreq_[iwe] == fourier_freqs[iFreq] ) // Not tolerance, should be consistent with "Unique" used just above!
                    {
                        sum += fourierCoefs_.col(iMode)[iwe];
                    }
                }
                cvalues(iFreq, iMode) = sum;
            }
        }
        fourierCoefs_ = cvalues;
        encFreq_ = fourier_freqs;
    }
    fourierCoefsAbs_ = fourierCoefs_.abs();
    fourierCoefsArg_ = fourierCoefs_.arg();
}

Eigen::ArrayXd ReconstructionRaoLocal::operator()() const
{
    const typename Rao::IndexType nModes(data_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionRaoLocal::operator()(double time) const
{
    auto nModes(data_.getNModes());
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes));
    for (auto i = 0; i < nModes; ++i)
    {
        res(i) = (fourierCoefsAbs_.col(i) * Eigen::cos(encFreq_ * time + fourierCoefsArg_.col(i))).sum();
    }
    return res;
}

Eigen::ArrayXXd ReconstructionRaoLocal::operator()(const Eigen::ArrayXd& times) const
{
    Eigen::ArrayXXd response(times.size(), data_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i));
    }
    return response;
}


const Eigen::ArrayXXcd& ReconstructionRaoLocal::getFourierCoefficients() const
{
     return fourierCoefs_;
}

ReconstructionRao::ReconstructionRao(
    std::shared_ptr<const Wif> p_wif, const Rao & data,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    Interpolators::ExtrapolationType extrapType, unsigned int numThreads, bool forceRun) :
    ReconstructionRaoABC(p_wif, data, interpScheme, interpStrategy, extrapType,
                         numThreads, forceRun),
    dataInterpolated_(
        data.getRaoAtFrequencies(p_wif_->getFrequencies(), interpScheme,
                                 interpStrategy, extrapType))
{
if (((data_.isReadyForInterpolation()) || (forceRun)) == false)
    {
        throw BV::Tools::Exceptions::BVException(
            "RAO is not ready for interpolation ! \n Make sure RAO is defined on 0-360 deg (included)\n If needed, use .getSymetrized() to symetrize RAO\n or .getSorted(duplicatesBounds=True) to duplicates its 0 degree value at 360");
}
}

Eigen::ArrayXd ReconstructionRao::getArgument_(double time, double bodyXGlobal,
                                               double bodyYGlobal,
                                               double bodyHeading) const
{
    const Eigen::Vector2d & raoWaveReferencePoint(
        data_.getWaveReferencePoint()) ;
    const Eigen::Vector3d & raoApplicationPoint(
        data_.getReferencePoint()) ;
    // Compute RAO wave reference point in global:
    const double cosa(std::cos(bodyHeading)) ;
    const double sina(std::sin(bodyHeading)) ;
    // vessel appPt in global basis - vessel appPt local to global + wave refPt local to global
    const double xGlobal(
        bodyXGlobal + (raoWaveReferencePoint(0) - raoApplicationPoint(0)) * cosa
            - (raoWaveReferencePoint(1) - raoApplicationPoint(1)) * sina) ;
    const double yGlobal(
        bodyYGlobal + (raoWaveReferencePoint(0) - raoApplicationPoint(0)) * sina
            + (raoWaveReferencePoint(1) - raoApplicationPoint(1)) * cosa) ;
    return GetArgument(*(p_wif_.get()), time, xGlobal, yGlobal) ;
}

Eigen::ArrayXd ReconstructionRao::operator()(double bodyHeading) const
{
    const typename Rao::IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionRao::operator()(double time, double xGlobal,
                                             double yGlobal,
                                             double bodyHeading) const
{
    Eigen::ArrayXd relativeHeadings = p_wif_->getIndependentHeadings()
        - bodyHeading ;
    BV::Math::WrapAngle0_2PI(relativeHeadings) ;
    Eigen::ArrayXXcd complexData(
        dataInterpolated_.getComplexAtHeadings(
            relativeHeadings, p_wif_->getIndependentHeadingsIndices(),
            interpScheme_, interpStrategy_, extrapType_)) ; //size(nw, nModes)
    Eigen::ArrayXd argument(getArgument_(time, xGlobal, yGlobal, bodyHeading)) ;
    const Eigen::ArrayXd & waveAmps(p_wif_->getAmplitudes()) ;
    const typename Rao::IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes)) ;
    //FIXME to remove when conversion Array/Tensor is known

    // OPENMP actually slows things down significantly here
    //#pragma omp declare reduction (+: Eigen::ArrayXd: omp_out=omp_out+omp_in)  initializer(omp_priv=Eigen::ArrayXd::Zero(omp_orig.size()))
    //#pragma omp parallel for reduction(+:res)
    for (Eigen::ArrayXd::Index iw = 0; iw < waveAmps.size(); ++iw)
    {
        for (typename Rao::IndexType iMode = 0; iMode < nModes; ++iMode)
        {
            const std::complex<double> & complexValue(complexData(iw, iMode)) ;
            res(iMode) += waveAmps[iw] * std::abs(complexValue)
                * std::cos(argument(iw) + std::arg(complexValue)) ;
            //res(iMode) += waveAmps[iw] * std::real(complexValue)
            //    * std::cos(argument(iw))
            //    - waveAmps[iw] * std::imag(complexValue)
            //        * std::sin(argument(iw)) ;
        }
    }
    return res ;
}

Eigen::ArrayXd ReconstructionRao::operator()(
    double time, const Eigen::Vector3d & posHeading) const
{
    return (*this)(time, posHeading(0), posHeading(1), posHeading(2)) ;
}

Eigen::ArrayXXd ReconstructionRao::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
    const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings) const
{
    if ((times.size() != xs.size()) || (times.size() != ys.size())
        || (times.size() != headings.size()))
        throw BV::Tools::Exceptions::BVException(
            "Wrong sizes for times, xs, ys and headings") ;
    Eigen::ArrayXXd response(times.size(), dataInterpolated_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), xs(i), ys(i), headings(i)) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionRao::operator()(
//FIXME : Check that wif is local !
    const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), dataInterpolated_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), -data_.getWaveReferencePoint()[0],
                                  -data_.getWaveReferencePoint()[1], 0.) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionRao::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings) const
{
    return (*this)(times, posHeadings.col(0), posHeadings.col(1),
                   posHeadings.col(2)) ;
}
