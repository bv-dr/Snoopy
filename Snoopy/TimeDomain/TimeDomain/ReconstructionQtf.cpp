﻿#include <cmath>

#include "ReconstructionQtf.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"
#include <iomanip>

#include <chrono>
#include <ctime>

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

ReconstructionQtfLocal::ReconstructionQtfLocal(
    std::shared_ptr<const Wif> p_wif,
    const BV::Spectral::Qtf &data,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const FrequencyInterpolationStrategies & frequencyInterpStrategy,
    const Interpolators::ExtrapolationType & extrapType,
    double dwMax,
    unsigned int numThreads) :
    ReconstructionQtfABC(p_wif, data, interpScheme, interpStrategy,
                         frequencyInterpStrategy, extrapType, dwMax, numThreads),
    dataInterpolated_(data.getComplexData())        // We need this, because there is no default constructor: QtfTensor()
{
    Spectral::Qtf freqInterp(data.getQtfAtFrequencies(p_wif_->getFrequencies(),
                                                      WJmWI_, interpScheme,
                                                      interpStrategy,
                                                      frequencyInterpStrategy,
                                                      extrapType)) ;
    Eigen::ArrayXd headings(p_wif_->getIndependentHeadings()) ;
    double relativeHeading = WrapAngle0_2PI(headings(0)) ;
    dataInterpolated_ = freqInterp.getComplexAtHeading(relativeHeading,
                                                       interpScheme_,
                                                       interpStrategy_,
                                                       extrapType_) ;
}

Eigen::ArrayXd ReconstructionQtfLocal::operator()() const
{
    if (qtfMode_ > 0.0)
    {   // argument(0) != 0
        throw BV::Tools::Exceptions::BVException("Mean load not implemented yet for sum frequencies") ;
    }
    Eigen::ArrayXd::Index nws(WJmWI_.size()) ;

    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes_)) ;
    auto qtfMode(data_.getSumMode());
    for (Eigen::ArrayXd::Index iw=0; iw<nws; ++iw)
    {
        std::complex<double> val ;
        for (Qtf::IndexType iMode=0; iMode<nModes_; ++iMode)
        {
            val = dataInterpolated_.getFromiWjDW(0, iw, 0, iMode) ;
            meanLoad(iMode) += AJ_AI_[iw](0) * std::abs(val) ;
        }
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtfLocal::operator()(double time) const
{
    //Eigen::ArrayXd::Index nws(p_wif_->getFrequencies().size()) ;
    Eigen::ArrayXd::Index nws(WJmWI_.size()) ;

    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes_)) ;
    auto qtfMode(data_.getSumMode());
    for (Eigen::ArrayXd::Index iw=0; iw<nws; ++iw)
    {
        Eigen::ArrayXd argument(getArgument_(iw, time)) ;
        std::complex<double> val ;
        for (Qtf::IndexType iMode=0; iMode<nModes_; ++iMode)
        {
            val = dataInterpolated_.getFromiWjDW(0, iw, 0, iMode) ;
            double carg(std::cos(std::arg(val))) ;
            if (qtfMode > 0.)
            {
                carg = std::cos(argument(0) +std::arg(val)) ;
            }
            res(iMode) += AJ_AI_[iw](0) * std::abs(val) * carg ;
            Eigen::ArrayXd dws(WJ_WI_[iw]) ;
            Eigen::ArrayXd::Index ndws(dws.size()) ;
            for (Eigen::ArrayXd::Index idw=1; idw<ndws; ++idw)
            {
                if (WJmWI_[iw](idw) <= dwMax_)
                {
                    val = dataInterpolated_.getFromiWjDW(0, iw, idw, iMode) ;
                    res(iMode) += AJ_AI_[iw](idw)
                            * 2. * std::abs(val)
                            * std::cos(argument(idw) +qtfMode *std::arg(val)) ;
                            //* (val.real() * std::cos(argument(iw1, iw2))
                            //   + val.imag() * std::sin(argument(iw1, iw2))) ;
                }
            }
        }
    }
    return res ;
}

Eigen::ArrayXXd ReconstructionQtfLocal::operator()(const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), nModes_) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i)) ;
    }
    return response ;
}


ReconstructionQtf::ReconstructionQtf(
    std::shared_ptr<const Wif> p_wif,
    const Qtf & data,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const FrequencyInterpolationStrategies & frequencyInterpStrategy,
    const Interpolators::ExtrapolationType & extrapType,
    double dwMax,
    double headingApproxTol,
    unsigned int numThreads) :
    ReconstructionQtfABC(p_wif, data, interpScheme, interpStrategy,
                         frequencyInterpStrategy, extrapType, dwMax, numThreads),
    dataWaveReferencePoint_(data.getWaveReferencePoint()),
    dataApplicationPoint_(data.getReferencePoint()),
    oldRelativeHeading_(-1.e10),
    dataInterpolated_(Qtf(data.getQtfAtFrequencies(p_wif_->getFrequencies(),
                                               WJmWI_, interpScheme,
                                               interpStrategy,
                                               frequencyInterpStrategy,
                                               extrapType))),
    headingApproxTol_(headingApproxTol)
{
    headings_ = p_wif_->getIndependentHeadings();
    if (headings_.size() > 1)
    {
        throw BV::Tools::Exceptions::BVException("No spreading allowed for Qtf reconstruction.") ;
    }

    nbhead_ = dataInterpolated_.getHeadings().size();
    nbfreq_= dataInterpolated_.getComplexData().getData().dimension(1);
    // P0_ = 1.;
    P1_= nbhead_;
    P2_= P1_*nbfreq_;
    complexData_.resize(nbfreq_, nModes_);          // Allocate space
    coefCalc_();
}

void ReconstructionQtf::coefCalc_()
{
    const Eigen::Tensor<std::complex<double>, 3> data(dataInterpolated_.getComplexData().getData());
    const Eigen::ArrayXd x(dataInterpolated_.getHeadings());

    if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM ||
        interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP)
    {
        dataCoefs_ = GetLinearCoeffs(data, x, 0);
    }
    if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP ||
        interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::AMP_PHASE)
    {
        Eigen::Tensor<double, 3> amp(dataInterpolated_.getModules().getData());
        ampCoefs_ = GetLinearCoeffs(amp, x, 0);
    }
    if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::AMP_PHASE)
    {
        Eigen::Tensor<double, 3> phase(dataInterpolated_.getPhases().getData());
        phaseCoefs_ = GetLinearCoeffs(phase, x, 0);
    }
}

void ReconstructionQtf::getAtHeading_(const double & relativeHeading) const
{
    const Eigen::ArrayXd& qtfHeadings(dataInterpolated_.getHeadings());
    const Eigen::Tensor<std::complex<double>, 3>& data(dataInterpolated_.getComplexData().getData());                  // Reference to the Eigen::Tensor of dataInterpolated
    // getting interval: qtfHeadings[ih] <= relativeHeading < qtfHeadings[ih +1] or 2*pi
    Eigen::Index ih = 0;
    if (std::abs(oldRelativeHeading_ -relativeHeading) > headingApproxTol_)     // Linear Interpolation is done only if relativeHeading is different from that used previously.
    {                                                                           // which means, no need to recalculate qtf for the same heading
        for (ih = 0; ih < nbhead_ -1; ih ++)
        {
            if (relativeHeading < qtfHeadings[ih+1]-1.e-8) break;
        }
        // in a case relativeHeading > qtfHeadings[ih] for ih < nbhead_ -1, the loop will finish naturally, which means that ih == nbhead_ -1.
        // for qtfHeadings >= qtfHeadings[nbhead_ -1] (>= the lates heading) the linear extrapolation is used based on the latest interval
        // Similar for the case if qtfHeadings[0] > 0.0, the values for relativeHeading <= qtfHeadings[0] are based on the first interval
        if (ih == nbhead_ -1) ih--;                                                      // relativeHeadings >= the latest heading in the list

        bool atNode(false);
        if (std::abs(relativeHeading-qtfHeadings[ih]) < 1.e-8)                             // if relativeHeading is a heading in the list => no need to recalculate => use exact values
        {
            atNode = true;
        }
        else if (qtfHeadings[nbhead_-1] > 2*M_PI-1.e-8 && relativeHeading > 2*M_PI-1.e-8)   // relativeHeading == qtfHeadings[nbhead_ -1] == 2 pi (last heading)
        {
            atNode = true;
            ih = nbhead_ -1;
        }
        if (atNode)
        {
            for (Eigen::Index ifreq = 0; ifreq < nbfreq_; ifreq ++)
            {
                for (Eigen::Index imode = 0; imode < nModes_; imode ++)
                {
                    complexData_(ifreq, imode) = data(ih, ifreq, imode);
                }
            }
        }
        else
        {
            std::complex<double> r;
            double a;
            double p;
            for (Eigen::Index ifreq = 0; ifreq < nbfreq_; ifreq ++)
            {
                for (Eigen::Index imode = 0; imode < nModes_; imode ++)
                {
                    if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM ||
                        interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP)
                    {
                        r = dataCoefs_(0, ih, ifreq, imode) +dataCoefs_(1, ih, ifreq, imode) *relativeHeading;
                    }
                    if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::AMP_PHASE ||
                             interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP)
                    {
                        a = ampCoefs_(0, ih, ifreq, imode) +ampCoefs_(1, ih, ifreq, imode) *relativeHeading;
                    }
                    if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP)
                    {
                        double mod(std::abs(r));
                        if (mod > 0) r = r/mod *a;
                    }
                    else if (interpStrategy_ == BV::Spectral::ComplexInterpolationStrategies::AMP_PHASE)
                    {
                        p = phaseCoefs_(0, ih, ifreq, imode) +phaseCoefs_(1, ih, ifreq, imode) *relativeHeading;
                        r = std::complex<double>(a *std::cos(p), a *std::sin(p));
                    }
                    complexData_(ifreq, imode) = r;
                }
            }
        }
        oldRelativeHeading_ = relativeHeading;
    }
}

Eigen::ArrayXd ReconstructionQtf::operator()(double bodyHeading) const
{
    if (qtfMode_ > 0.0)
    {   // argument(0) != 0
        throw BV::Tools::Exceptions::BVException("Mean load not implemented yet for sum frequencies") ;
    }
    const Qtf::IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    double relativeHeading = WrapAngle0_2PI(headings_(0) - bodyHeading) ;
    getAtHeading_(relativeHeading) ;
    Eigen::Index nws(WJmWI_.size());
    const auto& dwsizes(dataInterpolated_.getComplexData().getDWSizes());
    for (Eigen::Index iw=0; iw<nws; ++iw)
    {
        std::complex<double> val ;
        double tmp ;
        for (Qtf::IndexType iMode=0; iMode<nModes_; ++iMode)
        {
            if (dwsizes[iw] != 0)   // if dwsizes[iw] == 0 => val = 0 => std::abs(val) = 0 => res(iMode) += 0
            {
                val = complexData_(dataInterpolated_.getComplexData().getIndiWjDW(iw, 0), iMode);
                meanLoad(iMode) += AJ_AI_[iw](0) *std::real(val); // argument(0) = 0
            }
        }
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtf::operator()(double time, double xGlobal,
                                       double yGlobal,
                                       double bodyHeading) const
{
    double relativeHeading = WrapAngle0_2PI(headings_(0) - bodyHeading) ;
    getAtHeading_(relativeHeading) ;

    //Eigen::Index nws(p_wif_->getFrequencies().size()) ;
    Eigen::Index nws(WJmWI_.size());
    const auto& dwsizes(dataInterpolated_.getComplexData().getDWSizes());

    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes_)) ;
    double projDistance(updateProjDistance_(xGlobal, yGlobal, bodyHeading));
    for (Eigen::Index iw=0; iw<nws; ++iw)
    {
        Eigen::ArrayXd argument(getArgument_(iw, time, projDistance)) ;
        Eigen::ArrayXd carg(argument.size());
        Eigen::ArrayXd sarg(argument.size());

        const Eigen::ArrayXd& dws(WJ_WI_[iw]) ;
        Eigen::Index ndws(dws.size()) ;

        for(Eigen::Index idw = 0; idw < argument.size();idw++)
        {
            // it may happen, that argument.size() is greater than used number of arguments
            if (WJmWI_[iw](idw) <= dwMax_)
            {
                if (dwsizes[iw] <= idw) break;
                carg(idw) = std::cos(argument[idw]);
                sarg(idw) = std::sin(argument[idw]);
            }
        }
        // cos(a +qtfMode_ arg(val)) = cos(a) *cos(arg(val)) -qtfMode_ *sin(a) *sin(arg(val)), because atfMode_ = +/- 1
        // |val| *cos(argument(idw) +qtfMode_ *arg(val)) = cos(argument(idw)) *|val| *cos(arg(val)) -qtfMode_ *sin(argument(idw)) *|val| *sin(arg(val))
        //                                               = cos(argument(idw)) *real(val) -qtfMode_ *sin(argument(idw)) *imag(val)

        std::complex<double> val ;
        double tmp ;

        for (Qtf::IndexType iMode=0; iMode<nModes_; ++iMode)
        {
            if (dwsizes[iw] != 0)   // if dwsizes[iw] == 0 => val = 0 => std::abs(val) = 0 => res(iMode) += 0
            {
                val = complexData_(dataInterpolated_.getComplexData().getIndiWjDW(iw, 0), iMode);
                if (qtfMode_ < 0.0)
                {
                    res(iMode) += AJ_AI_[iw](0) *std::real(val); // argument(0) = 0
                }
                else
                {   // argument(0) != 0
                    res(iMode) += AJ_AI_[iw](0) *(carg[0] *std::real(val) -sarg[0] *std::imag(val));
                }
            }
            for (Eigen::Index idw=1; idw<ndws; ++idw)
            {
                if (WJmWI_[iw](idw) <= dwMax_)
                {
                    if (dwsizes[iw] <= idw) break;  // idw is <= dwsizes[iw] => 1), val == 0, 2) next idw will be cirtanly greater than dwsizes[iw]
                    val = complexData_(dataInterpolated_.getComplexData().getIndiWjDW(iw, idw), iMode);
                    // explicitly evaluation of: res(iMode) += AJ_AI_[iw](idw) *2.*(std::cos(argument(idw)) *std::real(val) -qtfMode_ *std::sin(argument(idw)) *std::imag(val));
                    if (qtfMode_ < 0.)  // qtfMode_ == -1.
                        tmp = carg[idw] *std::real(val) +sarg[idw] *std::imag(val);
                        //tmp = std::abs(val) *std::cos(argument(idw) -std::arg(val));      // original, much slower than new
                    else                // qtfMode_ == +1.
                        tmp = carg[idw] *std::real(val) -sarg[idw] *std::imag(val);
                        //tmp = std::abs(val) *std::cos(argument(idw) +std::arg(val));
                    res(iMode) += AJ_AI_[iw](idw) *(tmp +tmp);
                }
            }
        }
    }
    return res ;
}

Eigen::ArrayXd ReconstructionQtf::operator()(
    double time, const Eigen::Vector3d & posHeading) const
{
    return (*this)(time, posHeading(0), posHeading(1), posHeading(2)) ;
}

Eigen::ArrayXXd ReconstructionQtf::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
    const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings) const
{
    if ((times.size() != xs.size()) || (times.size() != ys.size())
        || (times.size() != headings.size()))
        throw BV::Tools::Exceptions::BVException(
            "Wrong sizes for times, xs, ys and headings") ;
    Eigen::ArrayXXd response(times.size(), nModes_) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), xs(i), ys(i), headings(i)) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf::operator()(
    const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), nModes_) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), 0., 0., 0.) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings) const
{
    return (*this)(times, posHeadings.col(0), posHeadings.col(1),
                   posHeadings.col(2)) ;
}
