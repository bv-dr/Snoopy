#pragma once

#include <Eigen/Dense>

#include "TimeDomainExport.hpp"
#include "TimeDomain/Reconstruction.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/Rao.hpp"

namespace BV {
namespace TimeDomain {

class ReconstructionRaoABC : public ReconstructionABC
{
protected:
    const Spectral::Rao& data_ ;
    const BV::Math::Interpolators::InterpScheme interpScheme_ ;
    const BV::Spectral::ComplexInterpolationStrategies interpStrategy_ ;
    const BV::Math::Interpolators::ExtrapolationType extrapType_ ;
public:
    ReconstructionRaoABC(std::shared_ptr<const Spectral::Wif> p_wif,
                         const Spectral::Rao & data,
                         const Math::Interpolators::InterpScheme & interpScheme=
                             Math::Interpolators::InterpScheme::LINEAR,
                         const Spectral::ComplexInterpolationStrategies & interpStrategy=
                             Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
                         Math::Interpolators::ExtrapolationType extrapType=
                             Math::Interpolators::ExtrapolationType::BOUNDARY,
                         unsigned int numThreads=1,
                         bool forceRun=false) :
        ReconstructionABC(p_wif, numThreads),
        data_(data), interpScheme_(interpScheme),
        interpStrategy_(interpStrategy), extrapType_(extrapType)
    {
    }

    Eigen::Array<BV::Spectral::Modes, Eigen::Dynamic, 1> getModes() override
    {
        return data_.getModes();
    }

    Spectral::Rao getData(void) const
    {
        return data_ ;
    }
} ;

class TIME_DOMAIN_API ReconstructionRaoLocal : public ReconstructionRaoABC
{
private:
    Eigen::ArrayXXcd dataInterpolated_ ; // Frequencies and headings interpolation

    Eigen::ArrayXXcd fourierCoefs_; // Fourier coefficients (complex)
    Eigen::ArrayXXd fourierCoefsAbs_; // Fourrier coefficients (module)
    Eigen::ArrayXXd fourierCoefsArg_; // Fourrier coefficients (argument)

    Eigen::ArrayXd encFreq_; // Encounter frequency 

public:
    ReconstructionRaoLocal(std::shared_ptr<const Spectral::Wif> p_wif,
                           const Spectral::Rao & data,
                           const Math::Interpolators::InterpScheme & interpScheme=
                               Math::Interpolators::InterpScheme::LINEAR,
                           const Spectral::ComplexInterpolationStrategies & interpStrategy=
                               Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
                           Math::Interpolators::ExtrapolationType extrapType=
                               Math::Interpolators::ExtrapolationType::BOUNDARY,
                           unsigned int numThreads=1,
                           bool forceRunNotSym=false) ;

    Eigen::ArrayXd operator()() const ;
    Eigen::ArrayXd operator()(double time) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;

    const Eigen::ArrayXXcd& getFourierCoefficients() const;

} ;

class TIME_DOMAIN_API ReconstructionRao : public ReconstructionRaoABC
{
    Spectral::Rao dataInterpolated_ ; // Frequencies interpolation

    Eigen::ArrayXd getArgument_(double time, double bodyX, double bodyY,
                                double bodyHeading) const ;

public:
    ReconstructionRao(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Rao & data,
        const Math::Interpolators::InterpScheme & interpScheme =
            Math::Interpolators::InterpScheme::LINEAR,
        const Spectral::ComplexInterpolationStrategies & interpStrategy =
            Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
        Math::Interpolators::ExtrapolationType extrapType=
            Math::Interpolators::ExtrapolationType::BOUNDARY,
        unsigned int numThreads = 1,
        bool forceRun =false) ;

    Eigen::ArrayXd operator()(double heading) const ;
    Eigen::ArrayXd operator()(double time, double xGlobal, double yGlobal,
                              double heading) const ;
    Eigen::ArrayXd operator()(double time,
                              const Eigen::Vector3d & posHeading) const ;

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times,
                               const Eigen::ArrayXd & xs,
                               const Eigen::ArrayXd & ys,
                               const Eigen::ArrayXd & headings) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times,
                               const Eigen::ArrayX3d & posHeadings) const ;

} ;

}
}
