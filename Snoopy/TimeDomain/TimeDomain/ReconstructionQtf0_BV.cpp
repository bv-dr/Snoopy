#include "ReconstructionQtf0_BV.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

//////////////////// local reconstruction implementation ///////////////////////

ReconstructionQtf0_BVLocal::ReconstructionQtf0_BVLocal(
    std::shared_ptr<const Wif> p_wif, const Qtf0 & data,
    const Interpolators::InterpScheme & interpScheme,
    Interpolators::ExtrapolationType extrapType,
    unsigned int numThreads) :
    ReconstructionQtf0ABC(p_wif, data, interpScheme, extrapType, numThreads)
{
    // We can also directly interpolate the heading as wif is in local axis system
    BV::Spectral::Qtf0 freqInterp(data.getQtfAtFrequencies(p_wif_->getFrequencies(),
                                                           interpScheme,
                                                           extrapType)) ;
    dataInterpolated_ = freqInterp.getAtHeadings(
            p_wif_->getIndependentHeadings(), p_wif_->getIndependentHeadingsIndices(),
            interpScheme_, extrapType_) ;
}

Eigen::ArrayXd ReconstructionQtf0_BVLocal::getArgument_(double time) const
{
    return p_wif_->getEncounterFrequencies(data_.getForwardSpeed()) * time
             + p_wif_->getPhases() ;
}

Eigen::ArrayXd ReconstructionQtf0_BVLocal::operator()() const
{
    const Eigen::ArrayXd & wifFrequencies(p_wif_->getFrequencies()) ;
    const Eigen::ArrayXd & wifAmplitudesSquared(p_wif_->getAmplitudes().pow(2)) ;
    //Get heading just under relative heading and interpolate
    using IndexType = typename BV::Spectral::Qtf0::IndexType ;
    const IndexType nModes(data_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    // getting interval: qtfHeadings[ih] <= relativeHeading < qtfHeadings[ih +1] or 2*pi
    for (Eigen::Index iMode=0; iMode<nModes; ++iMode)
    {
        meanLoad[iMode] = (wifAmplitudesSquared * dataInterpolated_.col(iMode)).sum() ;
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtf0_BVLocal::operator()(double time) const
{
    const Eigen::ArrayXd & waveAmps(p_wif_->getAmplitudes()) ;
    Eigen::ArrayXd argument(getArgument_(time)) ;
    Eigen::ArrayXd acosarg((argument.cos()) * waveAmps) ;
    Eigen::ArrayXd asinarg((argument.sin()) * waveAmps) ;
    using IndexType = typename BV::Spectral::Qtf0::IndexType ;
    const IndexType nModes(data_.getNModes()) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes)) ;
    for (IndexType iMode = 0; iMode < nModes; ++iMode)
    {
        res(iMode) = (acosarg.sum()) * ((acosarg * dataInterpolated_.col(iMode)).sum())
            + (asinarg.sum()) * ((asinarg * dataInterpolated_.col(iMode)).sum()) ;
    }
    return res ;
}

Eigen::ArrayXXd ReconstructionQtf0_BVLocal::operator()(
    const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), data_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i)) ;
    }
    return response ;
}

//////////////////// global reconstruction implementation //////////////////////

ReconstructionQtf0_BV::ReconstructionQtf0_BV(
    std::shared_ptr<const Wif> p_wif, const Qtf0 & data,
    const Interpolators::InterpScheme & interpScheme,
    Interpolators::ExtrapolationType extrapType,
    unsigned int numThreads) :
    ReconstructionQtf0ABC(p_wif, data, interpScheme, extrapType, numThreads),
    dataInterpolated_(
        data.getQtfAtFrequencies(p_wif_->getFrequencies(), interpScheme, extrapType))
{
}

Eigen::ArrayXd ReconstructionQtf0_BV::getArgument_(double time,
                                                   double bodyXGlobal,
                                                   double bodyYGlobal,
                                                   double bodyHeading) const
{
    const Eigen::Vector2d & dataWaveReferencePoint(
        data_.getWaveReferencePoint()) ;
    const Eigen::Vector3d & dataApplicationPoint(
        data_.getReferencePoint()) ;
    // Compute Qtf wave reference point in global
    const double cosa(std::cos(bodyHeading)) ;
    const double sina(std::sin(bodyHeading)) ;
    const double xGlobal(
        bodyXGlobal + (dataWaveReferencePoint(0) - dataApplicationPoint(0))* cosa
            + (dataWaveReferencePoint(1) - dataApplicationPoint(1)) * sina) ;
    const double yGlobal(
        bodyYGlobal - (dataWaveReferencePoint(0) - dataApplicationPoint(0))* sina
            + (dataWaveReferencePoint(1) - dataApplicationPoint(1))* cosa) ;
    return GetArgument(*(p_wif_.get()), time, xGlobal, yGlobal) ;
}

Eigen::ArrayXd ReconstructionQtf0_BV::operator()(double heading) const
{
    using IndexType = typename BV::Spectral::Qtf0::IndexType ;
    const IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    const Eigen::ArrayXd & wifFrequencies(p_wif_->getFrequencies()) ;
    const Eigen::ArrayXd & wifAmplitudesSquared(p_wif_->getAmplitudes().pow(2)) ;
    Eigen::ArrayXd relativeHeadings(p_wif_->getHeadings() - heading) ;
    WrapAngle0_2PI(relativeHeadings) ;
    Eigen::ArrayXXd qtf0HeadInterp(dataInterpolated_.getAtHeadings(
        relativeHeadings, p_wif_->getIndependentHeadingsIndices(),
        interpScheme_, extrapType_)) ;
    for (Eigen::Index iMode=0; iMode<nModes; ++iMode)
    {
        for (Eigen::Index iw=0; iw < wifFrequencies.size(); ++iw)
        {
            meanLoad(iMode) += wifAmplitudesSquared(iw) * qtf0HeadInterp(iw, iMode) ;
        }
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtf0_BV::operator()(double time, double xGlobal,
                                                 double yGlobal,
                                                 double bodyHeading) const
{
    Eigen::ArrayXd relativeHeadings = p_wif_->getIndependentHeadings() - bodyHeading ;
    BV::Math::WrapAngle0_2PI(relativeHeadings) ;
    Eigen::ArrayXXd qtfInterp(
        dataInterpolated_.getAtHeadings(relativeHeadings,
                                        p_wif_->getIndependentHeadingsIndices(),
                                        interpScheme_, extrapType_)) ; //size(nw, nw, nModes)
    const Eigen::ArrayXd & waveAmps(p_wif_->getAmplitudes()) ;
    Eigen::ArrayXd argument(getArgument_(time, xGlobal, yGlobal, bodyHeading)) ;
    Eigen::ArrayXd acosarg((argument.cos()) * waveAmps) ;
    Eigen::ArrayXd asinarg((argument.sin()) * waveAmps) ;
    using IndexType = typename BV::Spectral::Qtf0::IndexType ;
    const IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes)) ;
    for (IndexType iMode = 0; iMode < nModes; ++iMode)
    {
        res(iMode) = (acosarg.sum()) * ((acosarg * qtfInterp.col(iMode)).sum())
            + (asinarg.sum()) * ((asinarg * qtfInterp.col(iMode)).sum()) ;
    }
    return res ;
}

Eigen::ArrayXd ReconstructionQtf0_BV::operator()(
    double time, const Eigen::Vector3d & posHeading) const
{
    return (*this)(time, posHeading(0), posHeading(1), posHeading(2)) ;
}

Eigen::ArrayXXd ReconstructionQtf0_BV::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
    const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings) const
{
    if ((times.size() != xs.size()) || (times.size() != ys.size())
        || (times.size() != headings.size()))
        throw BV::Tools::Exceptions::BVException(
            "Wrong sizes for times, xs, ys and headings") ;
    Eigen::ArrayXXd response(times.size(), dataInterpolated_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), xs(i), ys(i), headings(i)) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf0_BV::operator()(
    const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), dataInterpolated_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), 0., 0., 0.) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf0_BV::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings) const
{
    return (*this)(times, posHeadings.col(0), posHeadings.col(1),
                   posHeadings.col(2)) ;
}
