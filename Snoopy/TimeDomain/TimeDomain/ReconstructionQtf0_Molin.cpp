#include "ReconstructionQtf0_Molin.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

ReconstructionQtf0_MolinLocal::ReconstructionQtf0_MolinLocal(
    std::shared_ptr<const Wif> p_wif, const Qtf0 & data,
    const Interpolators::InterpScheme & interpScheme,
    Interpolators::ExtrapolationType extrapType, unsigned int numThreads) :
    ReconstructionQtf0_BVLocal(p_wif, data, interpScheme, extrapType, numThreads)
{
}

Eigen::ArrayXd ReconstructionQtf0_MolinLocal::operator()(double time) const
{
    const Eigen::ArrayXd & waveAmps(p_wif_->getAmplitudes()) ;
    Eigen::ArrayXd argument(getArgument_(time)) ;
    Eigen::ArrayXd acosarg((argument.cos()) * waveAmps) ;
    Eigen::ArrayXd asinarg((argument.sin()) * waveAmps) ;
    const typename Qtf0::IndexType nModes(data_.getNModes()) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes)) ;
    const Eigen::ArrayXXd qtfInterpAbs = dataInterpolated_.abs() ;
    const Eigen::ArrayXXd sqrtQtfInterp(qtfInterpAbs.sqrt()) ;
    const Eigen::ArrayXXd signs(dataInterpolated_.sign()) ;
    for (typename Qtf0::IndexType iMode = 0; iMode < nModes; ++iMode)
    {
        const Eigen::ArrayXd aSqrtQtfCos(acosarg * sqrtQtfInterp.col(iMode)) ;
        const Eigen::ArrayXd aSqrtQtfSin(asinarg * sqrtQtfInterp.col(iMode)) ;
        res(iMode) = aSqrtQtfCos.sum() * (aSqrtQtfCos * signs.col(iMode)).sum()
            + aSqrtQtfSin.sum() * (aSqrtQtfSin * signs.col(iMode)).sum() ;
    }
    return res ;
}

ReconstructionQtf0_Molin::ReconstructionQtf0_Molin(
    std::shared_ptr<const Wif> p_wif, const Qtf0 & data,
    const Interpolators::InterpScheme & interpScheme,
    Interpolators::ExtrapolationType extrapType, unsigned int numThreads) :
    ReconstructionQtf0_BV(p_wif, data, interpScheme, extrapType, numThreads)
{
}

Eigen::ArrayXd ReconstructionQtf0_Molin::operator()(double heading) const
{
    using IndexType = typename BV::Spectral::Qtf0::IndexType ;
    const IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    const Eigen::ArrayXd & wifFrequencies(p_wif_->getFrequencies()) ;
    const Eigen::ArrayXd & wifAmplitudesSquared(p_wif_->getAmplitudes().pow(2)) ;
    Eigen::ArrayXd relativeHeadings(p_wif_->getHeadings() - heading) ;
    WrapAngle0_2PI(relativeHeadings) ;
    Eigen::ArrayXXd qtf0HeadInterp(dataInterpolated_.getAtHeadings(
        relativeHeadings, p_wif_->getIndependentHeadingsIndices(),
        interpScheme_, extrapType_)) ;
    for (Eigen::Index iMode=0; iMode<nModes; ++iMode)
    {
        for (Eigen::Index iw=0; iw < wifFrequencies.size(); ++iw)
        {
            meanLoad(iMode) += wifAmplitudesSquared(iw) * qtf0HeadInterp(iw, iMode) ;
        }
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtf0_Molin::operator()(double time, double xGlobal,
                                                    double yGlobal,
                                                    double bodyHeading) const
{
    Eigen::ArrayXd relativeHeadings = p_wif_->getIndependentHeadings()
        - bodyHeading ;
    BV::Math::WrapAngle0_2PI(relativeHeadings) ;
    Eigen::ArrayXXd qtfInterp(
        dataInterpolated_.getAtHeadings(relativeHeadings,
                                        p_wif_->getIndependentHeadingsIndices(),
                                        interpScheme_, extrapType_)) ; //size(nw, nw, nModes)
    const Eigen::ArrayXd & waveAmps(p_wif_->getAmplitudes()) ;
    Eigen::ArrayXd argument(getArgument_(time, xGlobal, yGlobal, bodyHeading)) ;
    Eigen::ArrayXd acosarg((argument.cos()) * waveAmps) ;
    Eigen::ArrayXd asinarg((argument.sin()) * waveAmps) ;
    const typename Qtf0::IndexType nModes(dataInterpolated_.getNModes()) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes)) ;
    const Eigen::ArrayXXd qtfInterpAbs = qtfInterp.abs() ;
    const Eigen::ArrayXXd sqrtQtfInterp(qtfInterpAbs.sqrt()) ;
    const Eigen::ArrayXXd signs(qtfInterp.sign()) ;
    for (typename Qtf0::IndexType iMode = 0; iMode < nModes; ++iMode)
    {
        const Eigen::ArrayXd aSqrtQtfCos(acosarg * sqrtQtfInterp.col(iMode)) ;
        const Eigen::ArrayXd aSqrtQtfSin(asinarg * sqrtQtfInterp.col(iMode)) ;
        res(iMode) = aSqrtQtfCos.sum() * (aSqrtQtfCos * signs.col(iMode)).sum()
            + aSqrtQtfSin.sum() * (aSqrtQtfSin * signs.col(iMode)).sum() ;
    }
    return res ;
}
