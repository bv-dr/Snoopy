#pragma once

#include <memory>
#include <complex>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"

#include "Spectral/Wif.hpp"
#include "Spectral/Qtf.hpp"
#include "Spectral/MQtf.hpp"
#include "Spectral/QtfTensor.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Math/Tools.hpp"

#include <set>

#include "TimeDomain/Reconstruction.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API ReconstructionMQtfLocal: public ReconstructionABC
{
private:

    BV::Spectral::MQtf data_;
    int degree_;
    Eigen::ArrayXXd mqtfAmpPhase_;

    Eigen::ArrayXXd getArgument_(Eigen::ArrayXd times) const
    {
        // Wave convention ai.cos(wi.t+phii)
        Eigen::ArrayXXd res(times.size(), WJI_.size());
        for(Eigen::Index itime = 0; itime < times.size(); itime ++)
            res.row(itime) = WJI_ * times(itime) + PJI_ ;
        return res;
    }
    //BV::Math::Interpolators::InterpScheme interpScheme_ ;
    BV::Spectral::ComplexInterpolationStrategies interpStrategy_ ;
    //BV::Spectral::FrequencyInterpolationStrategies frequencyInterpStrategy_ ;
    //BV::Math::Interpolators::ExtrapolationType extrapType_ ;
    double dwMax_;
    double qtfMode_ ;
    Eigen::ArrayXd WI_ ;        // Wi
    Eigen::ArrayXd WJmI_ ;      // Wj - Wi (positive, zero or negative)
    Eigen::ArrayXd WJI_ ;       // Wj +qtfMode_ Wi ( => Wj +Wi for sum mode, Wj -Wi for the diff mode)
    Eigen::ArrayXd PJI_ ;       // Pj +qtfMode_ Pi
    Eigen::ArrayXd AJI_ ;       // Aj *Ai
    Eigen::ArrayXd KJI_ ;       // Kj +qtfMode_ Ki
    Eigen::ArrayXXd HJI_ ;      // HJI_(pt, 0) == hj, HJI_(pt, 1) == hi

    double qtfWMin_, qtfWMax_;

    inline bool isWif_Unidirectional() {
        auto hs(BV::Math::getUnique(p_wif_->getHeadings()));
        return hs.size() == 1;
    }
protected:

public:
    ReconstructionMQtfLocal(std::shared_ptr<const BV::Spectral::Wif> p_wif,
        const BV::Spectral::MQtf & data,
        const int & degree = 1,
        const BV::Spectral::ComplexInterpolationStrategies & interpStrategy =
            BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
        const double & dwMax = 1e6,
        unsigned int numThreads = 1);

    inline Eigen::Array<BV::Spectral::Modes, Eigen::Dynamic, 1> getModes() {
        return getQtf().getModes();
    }

    inline const BV::Spectral::MQtf & getQtf() const
    {
        return data_ ;
    }

    inline double getDwMax() const
    {
        return dwMax_;
    }

    Eigen::ArrayXd operator()(double time) const {
        Eigen::ArrayXd times(1);
        times(0) = time;
        auto res_tmp = (*this)(times);
        return res_tmp.row(0);
    }

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;
};

}   // TimeDomain
}   // BV
