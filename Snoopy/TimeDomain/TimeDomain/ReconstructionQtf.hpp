#pragma once

#include <memory>
#include <complex>
#include <set>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"

#include "Spectral/Wif.hpp"
#include "Spectral/Qtf.hpp"
#include "Spectral/MQtf.hpp"
#include "Spectral/QtfTensor.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Math/Interpolators/TensorLinear1D.hpp"

#include "TimeDomain/ReconstructionQtfABC.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API ReconstructionQtfLocal: public ReconstructionQtfABC<BV::Spectral::Qtf>
{
    // Frequencies and headings interpolation
    BV::Spectral::QtfTensor<std::complex<double>,
                            BV::Spectral::Details::ComplexSymmetry> dataInterpolated_ ;

    template <typename IndexType>
    Eigen::ArrayXd getArgument_(IndexType iFreq, double time) const
    {
        // Wave convention ai.cos(wi.t+phii)
        return WJ_WI_[iFreq] * time + PJ_PI_[iFreq] ;
    }

public:
    /**
     * @brief ReconstructionQtfLocal creates a ReconstructionQtfLocal object for the unidirectional Qtf
     * @param wif
     * @param data  : Qtf object
     * @param interpScheme
     * @param interpStrategy
     * @param frequencyInterpStrategy
     * @param extrapType
     * @param numThreads
     */
    ReconstructionQtfLocal(std::shared_ptr<const BV::Spectral::Wif> p_wif,
        const BV::Spectral::Qtf & data,
        const BV::Math::Interpolators::InterpScheme & interpScheme =
            BV::Math::Interpolators::InterpScheme::LINEAR,
        const BV::Spectral::ComplexInterpolationStrategies & interpStrategy =
            BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
        const BV::Spectral::FrequencyInterpolationStrategies & frequencyInterpStrategy =
            BV::Spectral::FrequencyInterpolationStrategies::W_DW,
        const BV::Math::Interpolators::ExtrapolationType & extrapType =
            BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
        double dwMax = 1e6,
        unsigned int numThreads=1) ;

    Eigen::ArrayXd operator()() const ;
    
    Eigen::ArrayXd operator()(double time) const ;

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;

} ;


class TIME_DOMAIN_API ReconstructionQtf: public ReconstructionQtfABC<BV::Spectral::Qtf>
{
    const Eigen::Vector2d dataWaveReferencePoint_ ;
    const Eigen::Vector3d dataApplicationPoint_ ;

    // Some variables, in order not to spend time for the recalculation or reassignment
    Eigen::Index nbhead_;
    Eigen::Index nbfreq_;
    Eigen::Index P1_;
    Eigen::Index P2_;
    mutable Eigen::Tensor<std::complex<double>, 2> complexData_;

    // for x in [x[i], x[i+1])
    Eigen::Tensor<std::complex<double>, 4> dataCoefs_;      // RE_IM approximation : data_(x, j, k) = dataCoefs_(i, j, k, 0) +dataCoefs_(i, j, k, 1) *x
    Eigen::Tensor<double, 4> ampCoefs_;                     // AMP approximation   : amp(x, j, k)   = ampCoefs_(i, j, k, 0) +ampCoefs_(i, j, k, 1) *x
    Eigen::Tensor<double, 4> phaseCoefs_;                   // phase approximation : phase(x, j, k) = phaseCoefs_(i, j, k, 0) +phaseCoefs_(i, j, k, 1) *x
    // RE_IM            : => data(x, j, k)
    // AMP_PHASE        : => amp(x, j, k) e^i{ phase(x, j, k) }
    // RE_IM_AMP        : => amp(x, j, k) e^i{ arg(data(x, j, k)) }

    // calculate the coefficients depending on the strategy
    void coefCalc_();

    mutable double oldRelativeHeading_;

protected:
    BV::Spectral::Qtf dataInterpolated_ ; // Frequencies interpolation
    double updateProjDistance_(double bodyXGlobal, double bodyYGlobal, double bodyHeading) const
    {
        // Compute RAO wave reference point in global
        const double cosa(std::cos(bodyHeading)) ;
        const double sina(std::sin(bodyHeading)) ;
        const double xGlobal(
            bodyXGlobal + (dataWaveReferencePoint_(0) - dataApplicationPoint_(0)) * cosa
                - (dataWaveReferencePoint_(1) - dataApplicationPoint_(1)) * sina) ;
        const double yGlobal(
            bodyYGlobal + (dataWaveReferencePoint_(0) - dataApplicationPoint_(0)) * sina
                + (dataWaveReferencePoint_(1) - dataApplicationPoint_(1)) * cosa) ;
        const double dx(xGlobal - p_wif_->getReferencePointX()) ;
        const double dy(yGlobal - p_wif_->getReferencePointY()) ;
        // Only 1 heading
        return dx * p_wif_->getCosHeadings()(0) + dy * p_wif_->getSinHeadings()(0) ;
    }
    template <typename IndexType>
    Eigen::ArrayXd getArgument_(IndexType iFreq, double time, double projDistance) const
    {
        // Wave convention ai.cos(wi.t+phii-ki.X)
        return WJ_WI_[iFreq] * time + PJ_PI_[iFreq] - KJ_KI_[iFreq] * projDistance ;
    }

    void getAtHeading_(const double & relativeHeading) const ;

    /**
     * p_wif_ independent headings.
     */
    Eigen::ArrayXd headings_;

    double headingApproxTol_;

    Eigen::ArrayXd meanLoad_ ;

public:
    ReconstructionQtf(
        std::shared_ptr<const BV::Spectral::Wif> p_wif, const BV::Spectral::Qtf & data,
        const BV::Math::Interpolators::InterpScheme & interpScheme =
            BV::Math::Interpolators::InterpScheme::LINEAR,
        const BV::Spectral::ComplexInterpolationStrategies & interpStrategy =
            BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
        const BV::Spectral::FrequencyInterpolationStrategies & frequencyInterpStrategy =
            BV::Spectral::FrequencyInterpolationStrategies::W_DW,
        const BV::Math::Interpolators::ExtrapolationType & extrapType =
            BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
        double dwMax = 1e6,
        double headingApproxTol = 1.e-8,
        unsigned int numThreads=1) ;

    Eigen::ArrayXd operator()(double heading) const ;

    Eigen::ArrayXd operator()(double time, double xGlobal, double yGlobal,
                              double heading) const ;
    Eigen::ArrayXd operator()(double time,
                              const Eigen::Vector3d & posHeading) const ;

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times,
                               const Eigen::ArrayXd & xs,
                               const Eigen::ArrayXd & ys,
                               const Eigen::ArrayXd & headings) const ;

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;

    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times,
                               const Eigen::ArrayX3d & posHeadings) const ;

} ;

}
}
