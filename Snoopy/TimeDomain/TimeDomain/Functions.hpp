#pragma once

#include "TimeDomain/Reconstruction.hpp"
#include "Math/Functions/ABC.hpp"

namespace BV {
namespace TimeDomain {

class ReconstructionFunction : public BV::Math::Functions::ABC<4, 3, double>
{
private:
    using Parent = BV::Math::Functions::ABC<4, 3, double> ;
    TimeDomain::ReconstructionWif rWif_ ;
public:
    ReconstructionFunction(std::shared_ptr<const Spectral::Wif> p_wif) :
        rWif_(TimeDomain::ReconstructionWif(p_wif))
    {
    }

    const Parent::OutputType & eval(const Parent::InputType & vals) const
    {
        const Eigen::Vector2d velocity(rWif_.evalProjected(vals(0), 0., 0.)) ;
        this->output_(0) = velocity(0) ;
        this->output_(1) = velocity(1) ;
        this->output_(2) = 0. ;
        return this->output_ ;
    }

} ;

} // End of namespace TimeDomain
} // End of namespace BV
