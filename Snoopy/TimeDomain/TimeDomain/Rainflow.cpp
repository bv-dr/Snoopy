#include "Rainflow.hpp"
#include <iostream>

Eigen::ArrayXi BV::TimeDomain::cptrf1(Eigen::ArrayXd extrema)
{
    //Translated from M.Olagnon Fortran routines

    int nxtr = static_cast<int> (extrema.size()) ;
    Eigen::ArrayXi ixtrt = Eigen::ArrayXi::Zero(nxtr);

    Eigen::ArrayXd xwrkt = Eigen::ArrayXd(nxtr);
    Eigen::ArrayXi iwrkt = Eigen::ArrayXi(nxtr);

    int itst = -1;

    for (auto ixtr = 0; ixtr < nxtr; ++ixtr)
        {

        //
        //...... lecture des points
        //
        itst += 1;
        xwrkt(itst) = extrema(ixtr);
        iwrkt(itst) = ixtr;

        //
        //...... test sur l'existence d'un cycle ferme
        //
        while (true)
            {
            if (itst < 3) goto exit;

            if (xwrkt(itst - 2) > xwrkt(itst - 3))
            {

                if ( (xwrkt(itst - 1) < xwrkt(itst - 3)) | (xwrkt(itst) < xwrkt(itst - 2)) )
                {
                    goto exit;
                }
            }
            else if (xwrkt(itst - 2) < xwrkt(itst - 3))
            {
                if ( (xwrkt(itst - 1) > xwrkt(itst - 3)) | (xwrkt(itst) > xwrkt(itst - 2)))
                {
                    goto exit;
                }
            }
            else
            {
                throw std::logic_error("Error in rainflow counting");
            }
            //
            //...... stockage du cycle trouve
            //
            auto iw1 = iwrkt(itst - 1);
            auto iw2 = iwrkt(itst - 2);

            ixtrt(iw2) = iw1;
            ixtrt(iw1) = iw2;
            xwrkt(itst - 2) = xwrkt(itst);
            iwrkt(itst - 2) = iwrkt(itst);
            itst = itst - 2;
            }
    exit: ;

    }
    //
    //....traitement du residu
    //
    for (auto ixtr = 0; ixtr < itst ; ++ixtr)
    {
        ixtrt(iwrkt(ixtr)) = iwrkt(ixtr + 1);
    }

    ixtrt(nxtr-1) = 0;

    return ixtrt;
}

TIME_DOMAIN_API Eigen::ArrayXd BV::TimeDomain::getExtrema(Eigen::ArrayXd signal)
{
    //!---------------------------------------------------------------------- -
    //!extpic  : extrait les extrema relatifs d'un signal
    //!
    //!auteur : Michel Olagnon
    //!mouture : 1.2
    //!date : 2 / 12 / 93

    // integer nsig, nxtr, kerr


    auto nsig = signal.size();


    Eigen::ArrayXd xxtrt(nsig);

    //
    //....initialisation : trouver le sens de variation
    //

    int kvar = 0;

    auto xprv = signal(0);

    int isigd;

    for (auto isig = 1; isig < nsig; ++isig)
    {
        if (signal(isig) > xprv)
        {
            kvar = +1;
            isigd = isig;
            goto exit;
        }
        else if (signal(isig) < xprv)
        {
            kvar = -1;
            isigd = isig;
            goto exit;
        }
    }
    exit:;

    //
    //....cas du signal constant
    //
    if (kvar == 0)
    {
        throw std::logic_error("Constant signal, no Rainflow possible");
    }

    //
    //....boucle sur le signal restant
    //
    int nxtr = 0;
    xxtrt(nxtr) = xprv;

    xprv = signal(isigd - 1);

    for (auto isig = isigd; isig < nsig; ++isig)  //do isig = isigd, nsig
    {
        if (kvar == 1) //mont�e
        {
            if (signal(isig) < xprv)
            {
                nxtr = nxtr + 1;
                xxtrt(nxtr) = xprv;
                kvar = -1;
            }
        }
        else if (kvar == -1) //descente
        {
            if (signal(isig) > xprv)
            {
                nxtr = nxtr + 1;
                xxtrt(nxtr) = xprv;
                kvar = +1;
            }
        }
        xprv = signal(isig);
    }
    nxtr = nxtr + 1;
    xxtrt(nxtr) = signal(nsig-1);
    xxtrt.conservativeResize(nxtr+1);
    return xxtrt;
}
