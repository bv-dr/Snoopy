#pragma once

#include "TimeDomainExport.hpp"
#include "TimeDomain/ReconstructionQtf0_BV.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API ReconstructionQtf0_MolinLocal: public ReconstructionQtf0_BVLocal
{
public:
    ReconstructionQtf0_MolinLocal(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Qtf0 & data,
        const Math::Interpolators::InterpScheme & interpScheme =
            Math::Interpolators::InterpScheme::LINEAR,
        Math::Interpolators::ExtrapolationType extrapType=
            Math::Interpolators::ExtrapolationType::BOUNDARY,
        unsigned int numThreads=1) ;

    Eigen::ArrayXd operator()(double time) const ;
} ;

class TIME_DOMAIN_API ReconstructionQtf0_Molin: public ReconstructionQtf0_BV
{
public:
    ReconstructionQtf0_Molin(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Qtf0 & data,
        const Math::Interpolators::InterpScheme & interpScheme =
            Math::Interpolators::InterpScheme::LINEAR,
        Math::Interpolators::ExtrapolationType extrapType=
            Math::Interpolators::ExtrapolationType::BOUNDARY,
        unsigned int numThreads=1) ;

    Eigen::ArrayXd operator()(double heading) const ;

    Eigen::ArrayXd operator()(double time, double xGlobal, double yGlobal,
                              double heading) const ;
} ;

}
}
