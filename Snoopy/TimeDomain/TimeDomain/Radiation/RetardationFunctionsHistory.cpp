#include "TimeDomain/Radiation/RetardationFunctionsHistory.hpp"
#include "TimeDomain/Radiation/RetardationFunction.hpp"
#include "TimeDomain/Radiation/Tools.hpp"

#include <iostream>

namespace BV {
namespace TimeDomain {

RetardationFunctionsHistory::RetardationFunctionsHistory(
                                const Eigen::Ref<const Eigen::ArrayXd> & freqs,
                                const Eigen::Tensor<double, 3> & damping,
                                const RetardationParameters & params
                                                        ) :
    frequencies_(freqs), damping_(damping), params_(params)
{
    // Compute the history
    RetardationFunction rf(params.getExtrapolationType(),
                           params.getInfiniteFrequency()) ;
    // Initialize temporary arrays according to cut frequency
    // FIXME here we assume the frequencies are sorted in ascending order.
    // Index of first frequency greater than 0.
    Eigen::Index nFreqs(freqs.size()) ;
    Eigen::Index iFreq0(0) ;
    Eigen::Index iFreqN(0) ;
    bool freq0Set(false) ;
    for (Eigen::Index iFreq=0; iFreq<nFreqs; ++iFreq)
    {
        const double & freq(freqs(iFreq)) ;
        if ((!(freq < 0.)) && (!freq0Set))
        {
            iFreq0 = iFreq ;
            freq0Set = true ;
        }
        if (freq > params.getCutFrequency())
        {
            iFreqN = iFreq ;
            break ;
        }
    }
    if (iFreqN == 0) // all frequencies are below wCut
    {
        iFreqN = freqs.size() ;
    }
    Eigen::Index nFreqsTmp(iFreqN - iFreq0) ;
    Eigen::ArrayXd freqsTmp(frequencies_.segment(iFreq0, nFreqsTmp)) ;
    Eigen::DSizes<Eigen::Index, 3> offset {iFreq0, 0, 0} ;
    Eigen::Index nModes1(damping.dimension(1)) ;
    Eigen::Index nModes2(damping.dimension(2)) ;
    Eigen::DSizes<Eigen::Index, 3> extent {nFreqsTmp, nModes1, nModes2} ;
    Eigen::Tensor<double, 3> dampTmp(damping.slice(offset, extent)) ;

    // Compute the retardation function history
    double timeStep(params.getRetardationTimeStep()) ;
    Eigen::Index nTimeInstants(static_cast<Eigen::Index>(
                                      params.getRetardationDuration() / timeStep
                                                        )+1) ;
    history_ = Eigen::Tensor<double, 3>(nTimeInstants, nModes1, nModes2) ;
    timeInstants_ = Eigen::ArrayXd(nTimeInstants) ;
    for (Eigen::Index iTime=0; iTime<nTimeInstants; ++iTime)
    {
        double time(iTime * timeStep) ;
        timeInstants_(iTime) = time ;
        Eigen::MatrixXd res(rf.get(time, freqsTmp, dampTmp)) ;
        history_.chip(iTime, 0) = Eigen::TensorMap<Eigen::Tensor<double, 2> >(
                                                    res.data(), nModes1, nModes2
                                                                             ) ;
    }
}

Eigen::Index RetardationFunctionsHistory::getHistorySize_(const double & time) const
{
    if (time >= params_.getRetardationDuration())
    {
        return getNTimeInstants() ;
    }
    return (std::round(time*1.e6)/std::round(params_.getRetardationTimeStep()*1.e6)) + 1 ;
}

Eigen::Tensor<double, 3> RetardationFunctionsHistory::getHistory(
                                                     const double & time
                                                                ) const
{
    Eigen::Index nInstants(getHistorySize_(time)) ;
    Eigen::Index nModes1(history_.dimension(1)) ;
    Eigen::Index nModes2(history_.dimension(2)) ;
    Eigen::DSizes<Eigen::Index, 3> offset {0, 0, 0} ;
    Eigen::DSizes<Eigen::Index, 3> extent {nInstants, nModes1, nModes2} ;
    return history_.slice(offset, extent) ;
}

Eigen::ArrayXd RetardationFunctionsHistory::getTimeInstants(const double & time) const
{
    Eigen::Index nInstants(getHistorySize_(time)) ;
    if (time > params_.getRetardationDuration())
    {
        return timeInstants_ + time - params_.getRetardationDuration() ;
    }
    return timeInstants_.head(nInstants) ;
}

Eigen::Index RetardationFunctionsHistory::getNModes1() const
{
    return history_.dimension(1) ;
}

Eigen::Index RetardationFunctionsHistory::getNModes2() const
{
    return history_.dimension(2) ;
}

Eigen::Index RetardationFunctionsHistory::getNTimeInstants() const
{
    return timeInstants_.size() ;
}

double RetardationFunctionsHistory::getRetardationDuration() const
{
    return params_.getRetardationDuration() ;
}

double RetardationFunctionsHistory::getRetardationTimeStep() const
{
    return params_.getRetardationTimeStep() ;
}

Eigen::Tensor<double, 3> RetardationFunctionsHistory::getDamping() const
{
    return damping_;
}

Eigen::ArrayXd RetardationFunctionsHistory::getFrequencies() const
{
    return frequencies_;
}


Eigen::Tensor<double, 3> RetardationFunctionsHistory::reComputeDamping(
                                const Eigen::Ref<const Eigen::ArrayXd> & freqs
                                                                      ) const
{
    Eigen::Index nFreqs(freqs.size()) ;
    Eigen::Index nModes1(history_.dimension(1)) ;
    Eigen::Index nModes2(history_.dimension(2)) ;
    Eigen::Tensor<double, 3> damping(nFreqs, nModes1, nModes2) ;
    auto cos = [](const double & val) -> double
    {
        return std::cos(val) ;
    } ;
    for (Eigen::Index iFreq=0; iFreq<nFreqs; ++iFreq)
    {
        const double & freq(freqs(iFreq)) ;
        auto intCos = [freq](const double & val) -> double
        {
            return std::sin(val) / freq ;
        } ;
        for (Eigen::Index iMode1=0; iMode1<nModes1; ++iMode1)
        {
            for (Eigen::Index iMode2=0; iMode2<nModes2; ++iMode2)
            {
                auto fB = [&](const Eigen::Index iFreq) -> double
                {
                    return history_(iFreq, iMode1, iMode2) ;
                } ;
                damping(iFreq, iMode1, iMode2) = Details::Integrate(
                                    fB, cos, intCos, timeInstants_, freq, 0, -1.
                                                                   ) ;
            }
        }
    }
    return damping ;
}

Eigen::Tensor<double, 3> RetardationFunctionsHistory::reComputeAddedMass(
                                const Eigen::Ref<const Eigen::ArrayXd> & freqs,
                                const Eigen::Ref<const Eigen::MatrixXd> & maInf
                                                                        ) const
{
    Eigen::Index nFreqs(freqs.size()) ;
    Eigen::Index nModes1(history_.dimension(1)) ;
    Eigen::Index nModes2(history_.dimension(2)) ;
    Eigen::Tensor<double, 3> addedMass(nFreqs, nModes1, nModes2) ;
    auto sin = [](const double & val) -> double
    {
        return std::sin(val) ;
    } ;
    for (Eigen::Index iFreq=0; iFreq<nFreqs; ++iFreq)
    {
        const double & freq(freqs(iFreq)) ;
        auto intSin = [freq](const double & val) -> double
        {
            return -std::cos(val) / freq ;
        } ;
        for (Eigen::Index iMode1=0; iMode1<nModes1; ++iMode1)
        {
            for (Eigen::Index iMode2=0; iMode2<nModes2; ++iMode2)
            {
                auto fB = [&](const Eigen::Index iFreq) -> double
                {
                    return history_(iFreq, iMode1, iMode2) ;
                } ;
                addedMass(iFreq, iMode1, iMode2) = maInf(iMode1, iMode2)
                        - 1./freq * Details::Integrate(
                                    fB, sin, intSin, timeInstants_, freq, 0, -1.
                                                      ) ;
            }
        }
    }
    return addedMass ;
}

} // End of namespace TimeDomain
} // End of namespace BV
