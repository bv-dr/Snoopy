#include "Tools/BVException.hpp"
#include "TimeDomain/Radiation/RetardationParameters.hpp"

namespace BV {
namespace TimeDomain {

RetardationParameters::RetardationParameters(const double & retardationDuration,
                                             const double & retardationTimeStep,
                                             const double & wCut, const double & wInf,
                                             const int & extrapolationType) :
    retardationDuration_(retardationDuration),
    retardationTimeStep_(retardationTimeStep),
    wCut_(wCut), wInf_(wInf),
    extrapolationType_(extrapolationType)
{
    if (retardationDuration_ < 1.e-8)
    {
        throw BV::Tools::Exceptions::BVException(
                      "Retardation duration cannot be null"
                                                ) ;
    }
}

const double & RetardationParameters::getRetardationDuration() const
{
    return retardationDuration_ ;
}

const double & RetardationParameters::getRetardationTimeStep() const
{
    return retardationTimeStep_ ;
}

const double & RetardationParameters::getCutFrequency() const
{
    return wCut_ ;
}

const double & RetardationParameters::getInfiniteFrequency() const
{
    return wInf_ ;
}

const int & RetardationParameters::getExtrapolationType() const
{
    return extrapolationType_ ;
}

} // End of namespace TimeDomain
} // End of namespace BV
