#pragma once

#include "TimeDomainExport.hpp"

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "TimeDomain/Radiation/RetardationParameters.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API RetardationFunctionsHistory
{
private:
    // FIXME I am not sure we have to save these parameters
    Eigen::ArrayXd frequencies_ ;
    Eigen::Tensor<double, 3> damping_ ;
    RetardationParameters params_ ;
    Eigen::Tensor<double, 3> history_ ;
    Eigen::ArrayXd timeInstants_ ;

    Eigen::Index getHistorySize_(const double & time) const ;

public:
    /*!
     * \brief Initialisation of the RetardationFunctionsHistory.
     * \param[in] freqs, array of shape (nFreqs,) containing the frequencies for
     *            which the damping values are defined.
     * \param[in] damping, array of shape (nFreqs, nModes1, nModes2) containing
     *            the damping values used for the calculation of the retardation
     *            functions.
     * \param[in] params, a RetardationParameters instance containing the
     *            main retardation calculation parameters.
     *
     * All expected retardation functions are computed in this constructor
     * as it takes too long to get them when required.
     *
     * Frequencies and thus damping are adapted to fit the cutting frequency
     * defined in the retardation parameters. Furthermore, no negative
     * frequencies will remain.
     */
    RetardationFunctionsHistory(const Eigen::Ref<const Eigen::ArrayXd> & freqs,
                                const Eigen::Tensor<double, 3> & damping,
                                const RetardationParameters & params) ;

    // FIXME try to use tensor ref to get references instead of copies !
    /*!
     * \brief Return the history of the retardation functions before provided
     *        time instant.
     * \param[in] time, the time instant for which the history should be
     *            returned. Depending on the time provided, the history will be
     *            either the total retardation duration or the truncated
     *            history if time is lower than retardation duration.
     * \return A tensor of shape
     *         (min(time, retardationDuration), nModes1, nModes2)
     *         containing the retardation functions history.
     */
    Eigen::Tensor<double, 3> getHistory(const double & time) const ;

    /*!
     * \brief Returns the time instants corresponding to the retardation
     *        functions history at provided time.
     * \param[in] time, the time instant for which the history instants should
     *            be returned. Depending on the time provided, the instants will
     *            be either the total retardation duration or the truncated
     *            instants history if time is lower than retardation duration.
     * \return An array of shape (min(time, retardationDuration),)
     *         containing the retardation functions history instants.
     */
    Eigen::ArrayXd getTimeInstants(const double & time) const ;

    /*!
     * \brief Recomputes the hydrodynamic damping coefficients based on the
     *        calculated retardation functions.
     * \param[in] freqs, the frequencies for which to recalculate the
     *            hydrodynamic coefficients.
     * \return The recalculated damping coefficients of shape
     *         (len(freqs), nModes1, nModes2 )
     *
     * The hydrodynamic coefficients are calculated according to following
     * formulation [Ogilvie, 1964]:
     * \n
     *      \f$ \displaystyle
     *          \textbf{A}(\omega) = \textbf{A}_{\infty}
     *              - \frac{1}{\omega} \int_{0}^{\infty}
     *                        \textbf{K}(t)sin(\omega t) dt
     *      \f$
     *
     *      \f$ \displaystyle
     *          \textbf{B}(\omega) = \textbf{B}_{\infty}
     *              + \int_{0}^{\infty} \textbf{K}(t) cos(\omega t) dt
     *      \f$
     *
     */
    Eigen::Tensor<double, 3> reComputeDamping(
                           const Eigen::Ref<const Eigen::ArrayXd> & freqs
                                             ) const ;
    Eigen::Tensor<double, 3> reComputeAddedMass(
                           const Eigen::Ref<const Eigen::ArrayXd> & freqs,
                           const Eigen::Ref<const Eigen::MatrixXd> & maInf
                                               ) const ;

    Eigen::Index getNModes1() const ;
    Eigen::Index getNModes2() const ;
    Eigen::Index getNTimeInstants() const ;
    double getRetardationDuration() const ;
    double getRetardationTimeStep() const ;

    Eigen::Tensor<double, 3> getDamping() const;
    Eigen::ArrayXd getFrequencies() const;

    // TODO checker on damping correct recalculation
} ;

} // End of namespace TimeDomain
} // End of namespace BV
