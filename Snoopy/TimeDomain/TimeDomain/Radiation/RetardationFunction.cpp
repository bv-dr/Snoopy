#include <cmath>
#include <iostream>

#include "Math/Tools.hpp"
#include "TimeDomain/Radiation/RetardationFunction.hpp"
#include "TimeDomain/Radiation/Tools.hpp"

namespace BV {
namespace TimeDomain {

RetardationFunction::RetardationFunction(const unsigned & extrapolationType,
                                         const double & wInf) :
    extrapolationType_(extrapolationType), wInf_(wInf)
{
}

Eigen::MatrixXd RetardationFunction::get(
              const double & time,
              const Eigen::Ref<const Eigen::ArrayXd> & frequencies,
              const Eigen::Tensor<double, 3> & damping
                                        ) const
{
    //Eigen::ArrayXd coswtDiff(Details::GetCosWtDifferences(time, frequencies)) ;
    Eigen::Index nModes1(damping.dimension(1)) ;
    Eigen::Index nModes2(damping.dimension(2)) ;
    Eigen::MatrixXd values(nModes1, nModes2) ;
    auto cos = [](const double & val) -> double
    {
        return std::cos(val) ;
    } ;
    auto intCos = [time](const double & val) -> double
    {
        if (BV::Math::IsClose(time, 0.))
        {
            return 0. ;
        }
        return std::sin(val) / time ;
    } ;
    // FIXME check the loop order for efficiency...
    for (Eigen::Index iMode1=0; iMode1<nModes1; ++iMode1)
    {
        for (Eigen::Index iMode2=0; iMode2<nModes2; ++iMode2)
        {
            // Integrate the damping
            auto fB = [damping, iMode1, iMode2](const Eigen::Index iFreq) -> double
            {
                return damping(iFreq, iMode1, iMode2) ;
            } ;
            values(iMode1, iMode2) = (2./M_PI)
                    * Details::Integrate(fB, cos, intCos,
                                         frequencies,
                                         time,
                                         extrapolationType_,
                                         wInf_) ;
        }
    }
    return values ;
}

} // End of namespace TimeDomain
} // End of namespace BV
