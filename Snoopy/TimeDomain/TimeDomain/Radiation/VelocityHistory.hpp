#pragma once

#include "TimeDomainExport.hpp"

#include <Eigen/Dense>

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API VelocityHistory
{
private:
    double retardationTimeStep_ ;
    double retardationDuration_ ;
    Eigen::Index nModes_ ;
    Eigen::Index windowSize_ ;
    Eigen::Index historySize_ ;
    Eigen::MatrixXd history_ ;
    double lastTime_ ;
    int currentIndex_ ;
    bool hasConstantTimeStep_ ;
    Eigen::VectorXd nonUniformTimeInstants_ ;
    int currentIndexRealStep_ ;

    Eigen::Index getWindowDimension_(const double & time) const ;

    void reset_() ;

public:

    VelocityHistory(const double & retardationTimeStep,
                    const double & retardationDuration,
                    const Eigen::Index & nModes,
                    Eigen::Index historySizeFactor=10) ;

    void add(const double & time,
             const Eigen::Ref<const Eigen::VectorXd> & values) ;

    Eigen::MatrixXd get(const double & time) const ;

    void checkStep(const double & time) ;
} ;

} // End of namespace TimeDomain
} // End of namespace BV
