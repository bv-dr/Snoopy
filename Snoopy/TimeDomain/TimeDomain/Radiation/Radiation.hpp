#pragma once

#include "TimeDomainExport.hpp"

#include <Eigen/Dense>

#include "TimeDomain/Radiation/RetardationParameters.hpp"
#include "TimeDomain/Radiation/RetardationFunctionsHistory.hpp"
#include "TimeDomain/Radiation/VelocityHistory.hpp"

namespace BV {
namespace TimeDomain {

// TODO RK specific convolution schemes ??

/*!
 * \brief Computes the convolution between velocity and retardation functions.
 *        This version of the convolution is for a fixed time step.
 * \param[in] rfHistory The retardation functions history tensor of shape
 *            (retardationDuration, nModes1, nModes2) with which to perform the
 *            convolution with velocity.
 * \param[in] velHistory The velocity history matrix of shape
 *            (retardationDuration, nModes2). Note that the history instants
 *            have to correspond to the same ones as the retardation functions.
 * \param[in] timeStep, the fixed time step of the retardation functions and
 *            velocity history.
 * \return The radiation memory loads of shape (nModes1,), convolution of the
 *         retardation functions with the velocity history.
 *
 * The convolution equation is of the following form:
 *
 *      \f$ \displaystyle
 *          \int_{-\infty}^{t} \textbf{K}(t-\tau) \cdot \dot{\vec{\xi}}(\tau) \ d\tau
 *      \f$
 */
TIME_DOMAIN_API Eigen::VectorXd Convolution(
                         const Eigen::Tensor<double, 3> & rfHistory,
                         const Eigen::Ref<const Eigen::MatrixXd> & velHistory,
                         const double & timeStep
                                           ) ;

/*!
 * \brief Computes the convolution between velocity and retardation functions.
 *        This version of the convolution is for a non-fixed time step.
 * \param[in] rfHistory The retardation functions history tensor of shape
 *            (retardationDuration, nModes1, nModes2) with which to perform the
 *            convolution with velocity.
 * \param[in] velHistory The velocity history matrix of shape
 *            (retardationDuration, nModes2). Note that the history instants
 *            have to correspond to the same ones as the retardation functions.
 * \param[in] timeInstants, the time instants of the retardation functions and
 *            velocity history.
 * \return The radiation memory loads of shape (nModes1,), convolution of the
 *         retardation functions with the velocity history.
 *
 * The convolution equation is of the following form:
 *
 *      \f$ \displaystyle
 *          \int_{-\infty}^{t} \textbf{K}(t-\tau) \cdot \dot{\vec{\xi}}(\tau) \ d\tau
 *      \f$
 */
TIME_DOMAIN_API Eigen::VectorXd Convolution(
            const Eigen::Tensor<double, 3> & rfHistory,
            const Eigen::Ref<const Eigen::MatrixXd> & velHistory,
            const Eigen::Ref<const Eigen::VectorXd> & timeInstants
                                           ) ;

class TIME_DOMAIN_API Radiation
{
private:
    RetardationFunctionsHistory rfHistory_ ;
    VelocityHistory velHistory_ ;
public:
    /*!
     * \brief Radiation memory loads helper class initialisation.
     *        This class aim is to help dealing with the common history of
     *        the velocity and the pre-calculated retardation functions.
     *        The retardation functions are pre-calculated on a given range of
     *        time instants, with a specific time step. Velocity history however
     *        depends on the time domain stepper, which has its own internal
     *        time steps and may even be adaptive. This class proposes to adapt
     *        the velocity history via interpolations in order to match the
     *        retardation functions time instants and perform the convolution.
     * \param[in] rfh, the retardation functions history object (instance of
     *            RetardationFunctionsHistory). This object contains the
     *            pre-computed retardation functions history.
     */
    Radiation(const RetardationFunctionsHistory & rfh) ;

    /*!
     * \brief Set the velocity values at time. These values are recorded and
     *        will be used in the interpolations to obtain a correct velocities
     *        history.
     * \param[in] time, the time instant corresponding to the velocity values.
     * \param[in] velocity, an array of shape (nModes2,) containing the
     *            velocity values at provided time instants.
     */
    void setVelocity(const double & time,
                     const Eigen::Ref<const Eigen::VectorXd> & velocity) ;

    void checkStep(const double & time) ;

    /*!
     * \brief Return the result of the convolution of the stored velocity
     *        values with the pre-computed retardation functions at a given
     *        time instant.
     * \param[in] time, the time instant for which to obtain the radiation
     *            memory loads.
     * \return The radiation memory loads array of shape (nModes1,).
     *         This is the result of the Convolution function.
     *         The velocity history may have been adapted (interpolated) in
     *         order to match the retardation functions history.
     */
    Eigen::VectorXd get(const double & time) ;

} ;

} // End of namespace TimeDomain
} // End of namespace BV
