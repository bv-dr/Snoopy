#include "TimeDomain/Radiation/Tools.hpp"
#include "Math/Tools.hpp"

namespace BV {
namespace TimeDomain {
namespace Details {

// TODO comment those functions !!
double Integrate(std::function<double(Eigen::Index)> B,
                 std::function<double(double)> g,
                 std::function<double(double)> G,
                 const Eigen::Ref<const Eigen::ArrayXd> & xs,
                 const double & alpha,
                 const unsigned & extrapType,
                 const double & xInf)
{
    // First compute the GX term
    Eigen::ArrayXd GX(GetGX(alpha, g, xs)) ;
    double res(0.) ;
    Eigen::Index nX(xs.size()) ;
    if (BV::Math::IsClose(alpha, 0.))
    {
        for (Eigen::Index iX=0; iX<nX-1; ++iX)
        {
            res += (xs(iX+1) - xs(iX)) * (B(iX+1) + B(iX)) ;
        }
        res *= 0.5 ;
    }
    else
    {
        for (Eigen::Index iX=0; iX<nX-1; ++iX)
        {
            double Bip1(B(iX+1)) ;
            double Bi(B(iX)) ;
            res += (Bip1 * G(xs(iX+1)*alpha) - Bi * G(xs(iX)*alpha))
                   + GX(iX+1) * (Bip1 - Bi) ;

        }
    }
    // FIXME the extrapolation is performed in the same way as in the fortran
    // code for the moment
    if (extrapType == 1)
    {
        // Linear extrapolation
        if (xInf > xs(nX-1))
        {
            if (BV::Math::IsClose(alpha, 0.))
            {
                res += 0.5 * B(nX-1) * (xInf - xs(nX-1)) ;
            }
            else
            {
                double a(-B(nX-1) / (xInf - xs(nX-1))) ;
                res += -B(nX-1) * G(alpha*xs(nX-1))
                        + (g(alpha*xInf) - GX(nX)) * a/std::pow(alpha, 2) ;
            }
        }
        else
        {
            if (BV::Math::IsClose(alpha, 0.))
            {
                res += 0.5 * B(nX-1) * 0.2 * xs(nX-1) ;
            }
            else
            {
                double a(-B(nX-1) / (0.2 * xs(nX-1))) ;
                res += -B(nX-1) * G(alpha*xs(nX-1))
                   + (g(alpha*1.2*xs(nX-1)) - GX(nX)) * a/std::pow(alpha, 2) ;
            }
        }
    }
    else if ((extrapType == 2) && (B(nX-1) > 0.))
    {
        // Exponential extrapolation
        double a((B(nX-1) - B(nX-2)) / (xs(nX-1) - xs(nX-2))) ;
        // If positive slope and fct is positive
        // or negative slope and negative fct
        if ((a * B(nX-1) > 0.) || (std::abs(a / B(nX-1)) < 0.1))
        {
            // linear extrap from t(n) to 1.2 * t(n)
            if (BV::Math::IsClose(alpha, 0.))
            {
                res += 0.5 * B(nX-1) * 0.2 * xs(nX-1) ;
            }
            else
            {
                double b(-B(nX-1) / (0.2 * xs(nX-1))) ;
                res += -B(nX-1) * G(alpha * xs(nX-1))
                    +(g(alpha*1.2*xs(nX-1)) - GX(nX)) * b/std::pow(alpha, 2) ;
            }
        }
        else
        {
            // Exponential extrapolation
            if (!BV::Math::IsClose(B(nX-1), 0.))
            {
                double beta(-a / B(nX-1)) ;
                double alpha2(std::pow(alpha, 2)) ;
                res += B(nX-1) * (beta * g(alpha * xs(nX-1))
                                  - alpha2 * G(alpha * xs(nX-1)))
                                 / (std::pow(beta, 2) + alpha2) ;
            }
        }

    }
    return res ;
}

Eigen::ArrayXd GetGX(const double & alpha,
                     std::function<double(double)> g,
                     const Eigen::Ref<const Eigen::ArrayXd> & xs)
{
    Eigen::Index nX(xs.size()) ;
    Eigen::ArrayXd GX(Eigen::ArrayXd::Zero(nX+1)) ;
    Eigen::ArrayXd GXTmp(Eigen::ArrayXd::Zero(nX+1)) ;

    // We don't want a division by 0.
    if (alpha < 1.e-8)
    {
        return GX ;
    }

    GXTmp.head(nX) = (xs * alpha).unaryExpr(g) ;
    GXTmp(nX) = GXTmp(nX-1) ;
    // FIXME we need to use this temporary table as Eigen has troubles
    // performing these inplace calculations...or maybe we have to tell it
    // someway or another...
    GX.segment(1, nX-1) = (GXTmp.segment(1, nX-1) - GXTmp.head(nX-1))
                          / (xs.tail(nX-1) - xs.head(nX-1))
                          / std::pow(alpha, 2) ;
    GX(0) = GX(1) ;
    GX(nX) = GXTmp(nX-1) ;

    return GX ;
}


} // End of namespace Details
} // End of namespace TimeDomain
} // End of namespace BV
