#pragma once

#include "TimeDomainExport.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API RetardationParameters
{
private:
    double retardationDuration_ ;
    double retardationTimeStep_ ;
    double wCut_ ;
    double wInf_ ;
    int extrapolationType_ ;
public:
    /*!
     * \brief Retardation parameters constructor.
     * \param[in] retardationDuration, duration of the retardation functions
     *            time history (s).
     * \param[in] retardationTimeStep, time step of the retardation functions
     *            time history (s).
     * \param[in] wCut, cutting frequency applied to the input frequencies (and
     *            hence the damping values). All frequencies lower than this
     *            value are kept for the retardation functions calculation.
     * \param[in] wInf, frequency value for which the input damping is null in
     *            case of a linear extrapolation of the retardation functions.
     * \param[in] extrapolationType, (0, 1 or 2) the type of extrapolation to
     *            to apply in the retardation functions calculation (above wCut).
     *            0: No extrapolation
     *            1: Linear extrapolation
     *            2: Exponential extrapolation
     */
    RetardationParameters(const double & retardationDuration,
                          const double & retardationTimeStep,
                          const double & wCut, const double & wInf,
                          const int & extrapolationType) ;

    const double & getRetardationDuration() const ;
    const double & getRetardationTimeStep() const ;
    const double & getCutFrequency() const ;
    const double & getInfiniteFrequency() const ;
    const int & getExtrapolationType() const ;

} ;

} // End of namespace TimeDomain
} // End of namespace BV
