#include "TimeDomain/Radiation/Radiation.hpp"

namespace BV {
namespace TimeDomain {

Eigen::VectorXd Convolution(
            const Eigen::Tensor<double, 3> & rfHistory,
            const Eigen::Ref<const Eigen::MatrixXd> & velHistory,
            const double & timeStep
                           )
{
    Eigen::Index nInstants(rfHistory.dimension(0)) ;
    Eigen::Index nModes1(rfHistory.dimension(1)) ;
    Eigen::Index nModes2(rfHistory.dimension(2)) ;
    Eigen::VectorXd radiation(Eigen::VectorXd::Zero(rfHistory.dimension(1))) ;
    for (Eigen::Index iMode1=0; iMode1<nModes1; ++iMode1)
    {
        for (Eigen::Index iMode2=0; iMode2<nModes2; ++iMode2)
        {
            radiation(iMode1) += 0.5 * rfHistory(nInstants-1, iMode1, iMode2)
                                     * velHistory(0, iMode2) ;
            for (Eigen::Index iInstant=1; iInstant<nInstants-1; ++iInstant)
            {
                radiation(iMode1) += rfHistory(nInstants-iInstant-1, iMode1, iMode2)
                                    * velHistory(iInstant, iMode2) ;
            }
            radiation(iMode1) += 0.5 * rfHistory(0, iMode1, iMode2)
                                     * velHistory(nInstants-1, iMode2) ;
        }
    }
    radiation *= -timeStep ;
    return radiation ;
}

Eigen::VectorXd Convolution(
            const Eigen::Tensor<double, 3> & rfHistory,
            const Eigen::Ref<const Eigen::MatrixXd> & velHistory,
            const Eigen::Ref<const Eigen::VectorXd> & timeInstants
                           )
{
    Eigen::Index nInstants(rfHistory.dimension(0)) ;
    Eigen::Index nModes1(rfHistory.dimension(1)) ;
    Eigen::Index nModes2(rfHistory.dimension(2)) ;
    Eigen::VectorXd radiation(Eigen::VectorXd::Zero(rfHistory.dimension(1))) ;
    for (Eigen::Index iMode1=0; iMode1<nModes1; ++iMode1)
    {
        for (Eigen::Index iMode2=0; iMode2<nModes2; ++iMode2)
        {
            for (Eigen::Index iInstant=1; iInstant<nInstants; ++iInstant)
            {
                // FIXME some terms can be factorised to be used at next time step
                radiation(iMode1) += 0.5 * (
                        rfHistory(nInstants-iInstant-1, iMode1, iMode2)
                        * velHistory(iInstant, iMode2)
                      + rfHistory(nInstants-iInstant, iMode1, iMode2)
                        * velHistory(iInstant-1, iMode2)
                                           )
                         * (timeInstants(iInstant) - timeInstants(iInstant-1)) ;
            }
        }
    }
    return -radiation ;
}

Radiation::Radiation(const RetardationFunctionsHistory & rfh) :
    rfHistory_(rfh),
    velHistory_(rfh.getRetardationTimeStep(),
                rfh.getRetardationDuration(),
                rfh.getNModes2())
{
}

void Radiation::setVelocity(const double & time,
                            const Eigen::Ref<const Eigen::VectorXd> & velocity)
{
    velHistory_.add(time, velocity) ;
}

Eigen::VectorXd Radiation::get(const double & time)
{
    return Convolution(rfHistory_.getHistory(time),
                       velHistory_.get(time),
                       rfHistory_.getRetardationTimeStep()) ;
}

void Radiation::checkStep(const double & time)
{
    velHistory_.checkStep(time) ;
}

} // End of namespace TimeDomain
} // End of namespace BV
