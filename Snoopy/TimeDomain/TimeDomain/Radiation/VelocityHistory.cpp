#include "TimeDomain/Radiation/VelocityHistory.hpp"
#include "Math/Interpolators/Linear.hpp"

namespace BV {
namespace TimeDomain {

VelocityHistory::VelocityHistory(const double & retardationTimeStep,
                                 const double & retardationDuration,
                                 const Eigen::Index & nModes,
                                 Eigen::Index historySizeFactor) :
    retardationTimeStep_(retardationTimeStep),
    retardationDuration_(retardationDuration),
    nModes_(nModes),
    windowSize_((std::round(retardationDuration*1.e6)/std::round(retardationTimeStep*1.e6))+1),
    historySize_(std::max(static_cast<Eigen::Index>(2), historySizeFactor)*windowSize_),
    history_(Eigen::MatrixXd::Zero(historySize_, nModes)),
    lastTime_(0.),
    currentIndex_(0),
    hasConstantTimeStep_(true),
    nonUniformTimeInstants_(historySize_),
    currentIndexRealStep_(0)
{
}

Eigen::Index VelocityHistory::getWindowDimension_(const double & time) const
{
    if (time > retardationDuration_)
    {
        return windowSize_ ;
    }
    //return static_cast<Eigen::Index>(time / retardationTimeStep_) + 1 ;
    return (std::round(time*1.e6) / std::round(retardationTimeStep_*1.e6)) + 1 ;
}

void VelocityHistory::reset_()
{
    history_.topRows(windowSize_+1) = history_.block(currentIndex_-windowSize_, 0,
                                                     windowSize_+1, nModes_) ;
    nonUniformTimeInstants_.head(windowSize_+1) = nonUniformTimeInstants_.segment(
                                          currentIndex_-windowSize_, windowSize_+1
                                                                                 ) ;
    currentIndex_ = windowSize_ ;
    currentIndexRealStep_ = currentIndex_ ;
}

void VelocityHistory::add(const double & time,
                          const Eigen::Ref<const Eigen::VectorXd> & values)
{
    double timeStep(time - lastTime_) ;
    // Check if we have a constant time step
    // FIXME I can't get the modulo work correctly with floating point precision...
    double modTime(std::fmod(time, retardationTimeStep_)) ;
    if (BV::Math::IsClose(time, lastTime_))
    {
        // FIXME I am not sure what to do in this case...
        // We can overload the values, but what if some values before were
        // interpolated according to previously saved values ?
        // TODO also save previous time and perform the interpolation again ?!
        history_.row(currentIndex_) = values ;
        nonUniformTimeInstants_(currentIndex_) = time ;
    }
    else if ((timeStep >= retardationTimeStep_)
            && (BV::Math::IsClose(modTime, 0., 1.e-8)
                || BV::Math::IsClose(modTime, retardationTimeStep_, 1.e-8))
            && hasConstantTimeStep_)
    {
        // Below -2 is because we only want the inner instants
        Eigen::Index nInnerSteps(getWindowDimension_(time-lastTime_)-2) ;
        // Make sure there is enough space in array
        if (currentIndex_ + nInnerSteps+1 >= historySize_)
        {
            reset_() ;
        }
        Eigen::VectorXd instantsToCompute(nInnerSteps) ;
        for (Eigen::Index iStep=0; iStep<nInnerSteps; ++iStep)
        {
            instantsToCompute(iStep) = lastTime_ + (iStep+1)*retardationTimeStep_ ;
        }
        Eigen::VectorXd axis(2) ;
        axis << lastTime_, time ;
        Eigen::MatrixXd data(2, nModes_) ;
        if (currentIndex_ < 0)
        {
            data.row(0) = Eigen::VectorXd::Zero(nModes_) ;
        }
        else
        {
            data.row(0) = history_.row(currentIndex_) ;
        }
        data.row(1) = values ;
        Eigen::MatrixXd newValues(nInnerSteps, nModes_) ;
        BV::Math::Interpolators::Linear1D::set(axis, data, instantsToCompute,
                                               newValues) ;
        history_.block(currentIndex_+1, 0, nInnerSteps, nModes_) = newValues ;
        nonUniformTimeInstants_.segment(currentIndex_+1, nInnerSteps) = instantsToCompute ;
        currentIndex_ += nInnerSteps+1 ;
        history_.row(currentIndex_) = values ;
        nonUniformTimeInstants_(currentIndex_) = time ;
    }
    else
    {
        // What should we do ?
        // in this case we have 2 solutions:
        //     - interpolate the velocities on the retardation steps
        //     - compute the retardation functions on the velocity steps. In this
        //        particuliar case, I don't know if we can check the correctness
        //        of the retardation function...and it may be very time consuming
        hasConstantTimeStep_ = false ;
        if (currentIndex_ + 1 >= historySize_)
        {
            reset_() ;
        }
        ++currentIndex_ ;
        history_.row(currentIndex_) = values ;
        nonUniformTimeInstants_(currentIndex_) = time ;
    }
    lastTime_ = time ;
}

void VelocityHistory::checkStep(const double & time)
{
    // Save the "real" time steps, not the ode stepper ones
    ++currentIndexRealStep_ ;
    history_.row(currentIndexRealStep_) = history_.row(currentIndex_) ;
    nonUniformTimeInstants_(currentIndexRealStep_) = time ;
    for (Eigen::Index i=currentIndexRealStep_+1; i<=currentIndex_; ++i)
    {
        nonUniformTimeInstants_(i) = 0. ;
        history_.row(i) = Eigen::VectorXd::Zero(history_.cols()) ;
    }
    currentIndex_ = currentIndexRealStep_ ;
}

Eigen::MatrixXd VelocityHistory::get(const double & time) const
{
    if (hasConstantTimeStep_)
    {
        if (time < retardationDuration_+1.e-8)
        {
            return history_.topRows(getWindowDimension_(time)) ;
        }
        return history_.block(currentIndex_-windowSize_+1, 0, windowSize_, nModes_) ;
    }
    // The time step is not uniform, we have to interpolate the velocity
    // history on the retardation functions instants.
    // First generate the time instants to get the velocities at.
    Eigen::Index windowDimension(getWindowDimension_(time)) ;
    double startTime(time - (windowDimension-1)*retardationTimeStep_) ;
    if (startTime < 0.-1.e-8)
    {
        // FIXME what should we do in this case ?
        startTime += retardationTimeStep_ ;
    }
    Eigen::VectorXd instants(windowDimension) ;
    for (Eigen::Index iTime=0; iTime<windowDimension; ++iTime)
    {
        instants(iTime) = startTime + iTime*retardationTimeStep_ ;
    }
    Eigen::MatrixXd velHistory(windowDimension, nModes_) ;
    // TODO enable more interpolation schemes
    // FIXME get rid of unecessary history (before retard duration)
    BV::Math::Interpolators::Linear1D::set(nonUniformTimeInstants_.head(currentIndex_+1),
                                           history_.topRows(currentIndex_+1),
                                           instants, velHistory) ;
    return velHistory ;
}

} // End of namespace TimeDomain
} // End of namespace BV
