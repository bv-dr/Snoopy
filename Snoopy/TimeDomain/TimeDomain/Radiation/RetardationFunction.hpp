#pragma once

#include "TimeDomainExport.hpp"

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

namespace BV {
namespace TimeDomain {

// TODO extrapolation during the integral calculation !!!

class TIME_DOMAIN_API RetardationFunction
{
private:
    unsigned extrapolationType_ ;
    double wInf_ ;

public:
    /*!
     * \brief Initialisation of the RetardationFunction type.
     * \param[in] extrapolationType An integer defining the type of
     *                extrapolation for the damping curves (0: no extrapolation,
     *                1: linear, 2: exponential).
     * \param[in] wInf Infinite frequency considered for linear extrapolation of
     *                the damping.
     */
    RetardationFunction(const unsigned & extrapolationType,
                        const double & wInf) ;

    /*!
     * \brief Computes the retardation function for a given time instant.
     * \param[in] time The time instant for which to calculate the retardation
     *                function.
     * \param[in] frequencies Array containing the encounter frequencies for
     *                which the damping values are defined.
     *                Note that these frequencies should have been pre-processed
     *                (no negative frequencies should be present).
     * \param[in] damping Array containing the damping values used in the
     *                calculation.
     *                Shape: ( nFrequencies, nModes1, nModes2 )
     * \return values Array used to output the result of the calculation.
     *                Shape: ( nModes1, nModes2 )
     *
     * The following integral is evaluated at provided time instant:\n
     *
     *   [<em>Johan Tuitman: "Hydroelastic response of ship structures
     *        to slamming induced whipping", eqs (4.50), (4.51) </em>]\n
     *
     *        \f$ \displaystyle
     *              \textbf{K}(t) = \frac{2}{\pi} \sum_{i=1}^{nFreq-1}
     *                     \int_{\omega_e(i)}^{\omega_e(i+1)}
     *                       (\textbf{a}_i \cdot \omega_e + \textbf{b}_i) \,
     *                       cos(\omega_e\,t)\ d\omega_e
     *        \f$
     * \n
     * Where
     * \n
     *        \f$
     *              \textbf{a}_i \cdot \omega_e(i) + \textbf{b}_i
     *                 = \textbf{B}(\omega_e(i)) - \textbf{B}(\infty) \newline
     *        \f$
     * \n
     * and
     * \n
     *        \f$ \textbf{a}_i \cdot \omega_e(i+1) + \textbf{b}_i
     *                 = \textbf{B}(\omega_e(i+1)) - \textbf{B}(\infty)
     *        \f$ .
     * \n
     *     FIXME add more details on the calculation assumptions
     *     (piecewise linear, ...)
     */
    Eigen::MatrixXd get(
              const double & time,
              const Eigen::Ref<const Eigen::ArrayXd> & frequencies,
              const Eigen::Tensor<double, 3> & damping
                       ) const ;
} ;

} // End of namespace TimeDomain
} // End of namespace BV
