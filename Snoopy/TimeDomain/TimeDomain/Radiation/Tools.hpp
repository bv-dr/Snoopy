#pragma once

#include "TimeDomainExport.hpp"

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <functional>

namespace BV {
namespace TimeDomain {
namespace Details {

// TODO documentation of this internal function ?
// The theory is provided in Opera theory manual
TIME_DOMAIN_API double Integrate(std::function<double(Eigen::Index)> B,
                                 std::function<double(double)> g,
                                 std::function<double(double)> G,
                                 const Eigen::Ref<const Eigen::ArrayXd> & xs,
                                 const double & alpha,
                                 const unsigned & extrapType,
                                 const double & xInf) ;

TIME_DOMAIN_API Eigen::ArrayXd GetGX(const double & alpha,
                                     std::function<double(double)> g,
                                     const Eigen::Ref<const Eigen::ArrayXd> & xs) ;

//TIME_DOMAIN_API double Integrate(
//            const Eigen::TensorRef<const Eigen::Tensor<double, 3> > & f,
//            const Eigen::Index & iMode1, const Eigen::Index & iMode2,
//            const Eigen::Ref<const Eigen::VectorXd> & ws,
//            const Eigen::Ref<const Eigen::VectorXd> & coswtDiff,
//            const double & time,
//            const unsigned & extrapType,
//            const double & wInf
//                                ) ;
//
///*!
// * \brief Computes the difference
// *          \f$ \displaystyle
// *                \frac{cos( \omega_{i+1} t ) - cos( \omega_{i} t )}
// *                     { (\omega_{i+1} - \omega_{i}) t }
// *          \f$
// *        for given time instant.
// * \param[in] time The time instant for which to calculate the cosinus
// *                difference.
// * \param[in] frequencies Array of encounter frequencies used in the
// *                calculation.
// * \return the coswt differences array.
// */
//TIME_DOMAIN_API Eigen::ArrayXd GetCosWtDifferences(
//                           const double & time,
//                           const Eigen::Ref<const Eigen::ArrayXd> & frequencies
//                                                  ) ;
//
//TIME_DOMAIN_API Eigen::MatrixXd GetFctWtDifferences(
//                           const Eigen::Ref<const Eigen::ArrayXd> & times,
//                           const Eigen::Ref<const Eigen::ArrayXd> & frequencies,
//                           std::function<double(double)> fct
//                                                   ) ;

} // End of namespace Details
} // End of namespace TimeDomain
} // End of namespace BV
