#include "Reconstruction.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;

//////////////////// local reconstruction implementation ///////////////////////

ReconstructionWifLocal::ReconstructionWifLocal(std::shared_ptr<const Wif> p_wif,
                                               double speed,
                                               unsigned int numThreads) :
    ReconstructionABC(p_wif, numThreads), speed_(speed)
{
    encFreq_ = p_wif_->getEncounterFrequencies(speed_);
}


double ReconstructionWifLocal::operator()(double time) const
{
    return (p_wif_->getAmplitudes() * (encFreq_ * time + p_wif_->getPhases() ).cos() ).sum() ;
}


double ReconstructionWifLocal::operator()(double time, double x, double y) const
{
    auto k = p_wif_->getWaveNumbers();
    auto cb = p_wif_->getCosHeadings();
    auto sb = p_wif_->getSinHeadings();
    return (p_wif_->getAmplitudes() * (encFreq_ * time 
                                       - ( k * x * cb + k * y * sb)  
                                       + p_wif_->getPhases()
                                      ).cos()).sum();
}


Eigen::Vector2d ReconstructionWifLocal::evalProjected(double time) const
{
    Eigen::Vector2d res;
    Eigen::ArrayXd A = p_wif_->getAmplitudes() * (p_wif_->getFrequencies() * time + p_wif_->getPhases().cos());
    res(0) = (A * p_wif_->getCosHeadings()).sum();
    res(1) = (A * p_wif_->getSinHeadings()).sum();
    return res;
}


Eigen::ArrayXd ReconstructionWifLocal::operator()(const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXd response(times.size()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response(i) = (*this)(times(i)) ;
    }
    return response ;
}

//////////////////// global reconstruction implementation //////////////////////

ReconstructionWif::ReconstructionWif(std::shared_ptr<const Wif> p_wif,
                                     unsigned int numThreads) :
    ReconstructionABC(p_wif, numThreads)
{
}

Eigen::ArrayXd ReconstructionWif::getA_(double time, double xGlobal,
                                        double yGlobal) const
{
    return p_wif_->getAmplitudes() * (GetArgument(*(p_wif_.get()), time, xGlobal, yGlobal).cos()) ;
}

Eigen::Vector2d ReconstructionWif::evalProjected(double time, double xGlobal,
                                                 double yGlobal) const
{
    Eigen::Vector2d res ;
    Eigen::ArrayXd A = getA_(time, xGlobal, yGlobal) ;
    res(0) = (A * p_wif_->getCosHeadings()).sum() ;
    res(1) = (A * p_wif_->getSinHeadings()).sum() ;
    return res ;
}

double ReconstructionWif::operator()(double time, double xGlobal,
                                     double yGlobal) const
{
    return getA_(time, xGlobal, yGlobal).sum() ;
}

double ReconstructionWif::operator()(double time,
                                     const Eigen::Vector2d & pos) const
{
    return (*this)(time, pos(0), pos(1)) ;
}

Eigen::ArrayXd ReconstructionWif::operator()(const Eigen::ArrayXd & times,
                                             const Eigen::ArrayXd & xs,
                                             const Eigen::ArrayXd & ys) const
{
    if ((times.size() != xs.size()) || (times.size() != ys.size()))
        throw BV::Tools::Exceptions::BVException(
            "Wrong sizes for times, xs and ys") ;
    Eigen::ArrayXd response(times.size()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response(i) = (*this)(times(i), xs(i), ys(i)) ;
    }
    return response ;
}

Eigen::ArrayXd ReconstructionWif::operator()(const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXd response(times.size()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response(i) = (*this)(times(i), 0., 0.) ;
    }
    return response ;
}

Eigen::ArrayXd ReconstructionWif::operator()(const Eigen::ArrayXd & times,
                                             const Eigen::ArrayX2d & pos) const
{
    return (*this)(times, pos.col(0), pos.col(1)) ;
}
