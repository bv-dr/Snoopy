#include "ReconstructionQtf0_WaveCurrentInteraction.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

ReconstructionQtf0_WaveCurrentInteraction::ReconstructionQtf0_WaveCurrentInteraction(
    std::shared_ptr<const Wif> p_wif, const Qtf0 & data,
    const Interpolators::InterpScheme & interpScheme,
    Interpolators::ExtrapolationType extrapType,
    unsigned int numThreads) :
    ReconstructionQtf0_BV(p_wif, data, interpScheme, extrapType, numThreads)
{
}

Eigen::ArrayXd ReconstructionQtf0_WaveCurrentInteraction::operator()(
    double bodyHeading,
    double relativeVelocityLocalX,
    double relativeVelocityLocalY) const
{
    const double gravity(9.81) ;
    const typename Qtf0::IndexType nModes(data_.getNModes()) ;
    Eigen::ArrayXd meanLoad(Eigen::ArrayXd::Zero(nModes)) ;
    const Eigen::ArrayXd & waveFrequencies(p_wif_->getFrequencies()) ;
    Eigen::ArrayXd waveAmplitudesSquared(p_wif_->getAmplitudes().pow(2)) ;
    Eigen::ArrayXd relativeWaveHeadings(p_wif_->getIndependentHeadings() - bodyHeading) ;
    Eigen::ArrayXd allRelativeWaveHeadings(p_wif_->getHeadings() - bodyHeading) ;
    WrapAngle0_2PI(relativeWaveHeadings) ;
    WrapAngle0_2PI(allRelativeWaveHeadings) ;
    // Get uL and uT
    const Eigen::ArrayXd longitudinalVelocities(
        getLongitudinalVelocities_(relativeVelocityLocalX,
                                   relativeVelocityLocalY,
                                   allRelativeWaveHeadings)) ;
    const Eigen::ArrayXd transversalVelocities(
        getTransversalVelocities_(relativeVelocityLocalX,
                                  relativeVelocityLocalY,
                                  allRelativeWaveHeadings)) ;
    // encounter frequency
    Eigen::ArrayXd encounterFrequencies(waveFrequencies -
        (waveFrequencies * waveFrequencies * longitudinalVelocities) / gravity) ;
    // encounter heading
    Eigen::ArrayXd encounterHeadings(allRelativeWaveHeadings -
        2. * waveFrequencies * transversalVelocities / gravity) ;
    WrapAngle0_2PI(encounterHeadings) ;
    const Eigen::ArrayXd coefficients(1. -
        4. * waveFrequencies * longitudinalVelocities / gravity) ;
    const Eigen::Tensor<double, 3> & qtfData(data_.getData()) ;
    // get modified QTF
    Eigen::ArrayXXd qtfMod(encounterFrequencies.size(), 2) ;
    Eigen::Tensor<double, 1> tensor(2) ;
    for (Eigen::ArrayXd::Index i = 0; i < encounterFrequencies.size(); ++i)
    {
        Math::Interpolators::Linear2D::set(data_.getHeadings(),
                                           data_.getFrequencies(), 0, 1,
                                           qtfData, encounterHeadings(i),
                                           encounterFrequencies(i), tensor) ;
        qtfMod(i, 0) = tensor(0) ;
        qtfMod(i, 1) = tensor(1) ;
    }
    // Get modified QTF, will only be used for surge and sway motions
    qtfMod.col(0) *= coefficients ;
    qtfMod.col(1) *= coefficients ;
    Eigen::ArrayXXd qtfInterp(
        dataInterpolated_.getAtHeadings(relativeWaveHeadings,
                                        p_wif_->getIndependentHeadingsIndices(),
                                        interpScheme_, extrapType_)) ;
    qtfInterp.col(0) = qtfMod.col(0) ;
    qtfInterp.col(1) = qtfMod.col(1) ;
    // following interpolation will only be used for z moment
    for (Eigen::Index iMode=0; iMode<nModes; ++iMode)
    {
        const Eigen::ArrayXd a2Qtf(waveAmplitudesSquared * qtfInterp.col(iMode)) ;
        meanLoad[iMode] += a2Qtf.sum() ;
    }
    return meanLoad ;
}

Eigen::ArrayXd ReconstructionQtf0_WaveCurrentInteraction::operator()(
    double time, double xGlobal, double yGlobal, double bodyHeading,
    double relativeVelocityLocalX, double relativeVelocityLocalY) const
{
    Eigen::ArrayXd relativeWaveHeadings = p_wif_->getIndependentHeadings()
        - bodyHeading ;
    Eigen::ArrayXd allRelativeWaveHeadings = p_wif_->getHeadings() - bodyHeading ;
    BV::Math::WrapAngle0_2PI(relativeWaveHeadings) ;
    BV::Math::WrapAngle0_2PI(allRelativeWaveHeadings) ;
    const Eigen::ArrayXd & waveAmps(p_wif_->getAmplitudes()) ;
    Eigen::ArrayXd argument(getArgument_(time, xGlobal, yGlobal, bodyHeading)) ;
    Eigen::ArrayXd acosarg((argument.cos()) * waveAmps) ;
    Eigen::ArrayXd asinarg((argument.sin()) * waveAmps) ;
    const typename Qtf0::IndexType nModes(data_.getNModes()) ;
    Eigen::ArrayXd res(Eigen::ArrayXd::Zero(nModes)) ;
    // Get uL and uT
    const Eigen::ArrayXd longitudinalVelocities(
        getLongitudinalVelocities_(relativeVelocityLocalX,
                                   relativeVelocityLocalY,
                                   allRelativeWaveHeadings)) ;
    const Eigen::ArrayXd transversalVelocities(
        getTransversalVelocities_(relativeVelocityLocalX,
                                  relativeVelocityLocalY,
                                  allRelativeWaveHeadings)) ;
    const double gravity(9.81) ; //FIXME set in parameters
    const Eigen::ArrayXd & waveFrequencies(p_wif_->getFrequencies()) ;
    // encounter frequency
    Eigen::ArrayXd encounterFrequencies(
        waveFrequencies
            - (waveFrequencies * waveFrequencies * longitudinalVelocities)
                / gravity) ;
    // encounter heading
    Eigen::ArrayXd encounterHeadings(
        allRelativeWaveHeadings
            - 2. * waveFrequencies * transversalVelocities / gravity) ;
    BV::Math::WrapAngle0_2PI(encounterHeadings) ;
    const Eigen::ArrayXd coefficients(
        1. - 4. * waveFrequencies * longitudinalVelocities / gravity) ;
    const Eigen::Tensor<double, 3> & qtfData(data_.getData()) ;
    Eigen::DSizes<Eigen::Tensor<double, 3>::Index, 3> offset { 0, 0, 0 } ;
    Eigen::DSizes<Eigen::Tensor<double, 3>::Index, 3> extent { data_.getNHeadings(),
        data_.getNFrequencies(), 2 } ;
    const Eigen::Tensor<double, 3> & qtfDataSurgeSway(
        qtfData.slice(offset, extent)) ;
    Eigen::ArrayXXd qtfMod(encounterFrequencies.size(), 2) ;
    Eigen::Tensor<double, 1> tensor(2) ;
    for (Eigen::ArrayXd::Index i = 0; i < encounterFrequencies.size(); ++i)
    {
        Math::Interpolators::Linear2D::set(data_.getHeadings(),
                                           data_.getFrequencies(), 0, 1,
                                           qtfData, encounterHeadings(i),
                                           encounterFrequencies(i), tensor) ;
        qtfMod(i, 0) = tensor(0) ;
        qtfMod(i, 1) = tensor(1) ;
    }
    // Get modified QTF, will only be used for surge and sway motions
    qtfMod.col(0) *= coefficients ;
    qtfMod.col(1) *= coefficients ;
    Eigen::ArrayXXd qtfInterp(
        dataInterpolated_.getAtHeadings(relativeWaveHeadings,
                                        p_wif_->getIndependentHeadingsIndices(),
                                        interpScheme_, extrapType_)) ;
    qtfInterp.col(0) = qtfMod.col(0) ;
    qtfInterp.col(1) = qtfMod.col(1) ;
    // Reconstruction
    const Eigen::ArrayXXd qtfInterpAbs = qtfInterp.abs() ;
    const Eigen::ArrayXXd qtfInterpSqrt(qtfInterpAbs.sqrt()) ;
    const Eigen::ArrayXXd signs(qtfInterp.sign()) ;
    for (typename Qtf0::IndexType iMode = 0; iMode < nModes; ++iMode)
    {
        const Eigen::ArrayXd aSqrtQtfCos(acosarg * qtfInterpSqrt.col(iMode)) ;
        const Eigen::ArrayXd aSqrtQtfSin(asinarg * qtfInterpSqrt.col(iMode)) ;
        res(iMode) = aSqrtQtfCos.sum() * (aSqrtQtfCos * signs.col(iMode)).sum()
            + aSqrtQtfSin.sum() * (aSqrtQtfSin * signs.col(iMode)).sum() ;
    }
    return res ;
}

Eigen::ArrayXd ReconstructionQtf0_WaveCurrentInteraction::operator()(
    double time, const Eigen::Vector3d & posHeading,
    const Eigen::Vector2d & relativeVelocityLocal) const
{
    return (*this)(time, posHeading(0), posHeading(1), posHeading(2),
                   relativeVelocityLocal(0), relativeVelocityLocal(1)) ;
}

Eigen::ArrayXXd ReconstructionQtf0_WaveCurrentInteraction::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
    const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings,
    const Eigen::ArrayXd & relativeVelocitiesLocalX,
    const Eigen::ArrayXd & relativeVelocitiesLocalY) const
{
    if ((times.size() != xs.size()) || (times.size() != ys.size())
        || (times.size() != headings.size()))
        throw BV::Tools::Exceptions::BVException(
            "Wrong sizes for times, xs, ys and headings") ;
    Eigen::ArrayXXd response(times.size(), dataInterpolated_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)
    for (Eigen::ArrayXXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), xs(i), ys(i), headings(i),
                                  relativeVelocitiesLocalX(i),
                                  relativeVelocitiesLocalY(i)) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf0_WaveCurrentInteraction::operator()(
    const Eigen::ArrayXd & times) const
{
    Eigen::ArrayXXd response(times.size(), dataInterpolated_.getNModes()) ;

    #pragma omp parallel for num_threads(numThreads_)   
    for (Eigen::ArrayXd::Index i = 0; i < times.size(); ++i)
    {
        response.row(i) = (*this)(times(i), 0., 0., 0., 0., 0.) ;
    }
    return response ;
}

Eigen::ArrayXXd ReconstructionQtf0_WaveCurrentInteraction::operator()(
    const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings,
    const Eigen::ArrayX2d & relativeVelocitiesLocal) const
{
    return (*this)(times, posHeadings.col(0), posHeadings.col(1),
                   posHeadings.col(2), relativeVelocitiesLocal.col(0),
                   relativeVelocitiesLocal.col(1)) ;
}
