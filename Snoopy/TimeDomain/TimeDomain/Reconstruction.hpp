#pragma once

#include <memory>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/TransferFunction.hpp"
#include "Math/Tools.hpp"
#include "Math/Tools.hpp"


namespace BV {
namespace TimeDomain {

class ReconstructionABC
{
protected:
    std::shared_ptr<const Spectral::Wif> p_wif_ ;

    /**
     * Number of threads for OpenMP reconstruction (when possible)
     */
    unsigned int numThreads_ ;

public:
    virtual ~ReconstructionABC() = default;

    ReconstructionABC(std::shared_ptr<const Spectral::Wif> p_wif,
                      unsigned int numThreads=1) :
        p_wif_(p_wif), numThreads_(numThreads)
    {
    }

    ReconstructionABC(const ReconstructionABC & other)
    {
        if (&other == this)
        {
            return ;
        }
        p_wif_ = std::make_shared<Spectral::Wif>(*(other.p_wif_.get())) ;
        numThreads_ = other.numThreads_ ;
    }

    inline std::shared_ptr<const Spectral::Wif> getWif() const
    {
        return p_wif_ ;
    }

    inline unsigned int getNumThreads() const
    {
        return numThreads_;
    }

    inline void setNumThreads(unsigned int numThreads)
    {
        numThreads_ = numThreads;
    }

    virtual Eigen::Array<BV::Spectral::Modes, Eigen::Dynamic, 1>  getModes()
    {
        throw std::logic_error("getModes not implemented for this Reconstructor");
    }
} ;

class TIME_DOMAIN_API ReconstructionWifLocal : public ReconstructionABC
{
private:


public:
    explicit ReconstructionWifLocal(std::shared_ptr<const Spectral::Wif> p_wif,
                                    double speed = 0.0, 
                                    unsigned int numThreads=1) ;

    double speed_;
    Eigen::Vector2d evalProjected(double time) const;
    Eigen::ArrayXd encFreq_; // Encounter frequency 
    double operator()(double time) const ;
    double operator()(double time, double x, double y) const;

Eigen::ArrayXd operator()(const Eigen::ArrayXd & times) const ;
} ;

inline Eigen::ArrayXd GetArgument(
    const BV::Spectral::Wif & wif, double time, double xGlobal, double yGlobal
                                 )
{
    if (BV::Math::IsNull(xGlobal) && BV::Math::IsNull(yGlobal))
        return wif.getFrequencies() * time + wif.getPhases() ;
    const double dx(xGlobal - wif.getReferencePointX()) ;
    const double dy(yGlobal - wif.getReferencePointY()) ;
    const Eigen::ArrayXd projDistance(
        dx * wif.getCosHeadings() + dy * wif.getSinHeadings()) ;
    // Wave convention ai.cos(wi.t+phii-ki.X)
    return wif.getFrequencies() * time + wif.getPhases()
        - wif.getWaveNumbers() * projDistance ;
}

class TIME_DOMAIN_API ReconstructionWif : public ReconstructionABC
{
protected:
    Eigen::ArrayXd getA_(double time, double xGlobal, double yGlobal) const ;

public:
    explicit ReconstructionWif(std::shared_ptr<const Spectral::Wif> p_wif,
                               unsigned int numThreads=1) ;

    Eigen::Vector2d evalProjected(double time, double x, double y) const;
    double operator()(double time, double x, double y) const;
    double operator()(double time, const Eigen::Vector2d & pos) const;

    Eigen::ArrayXd operator()(const Eigen::ArrayXd & times,
                              const Eigen::ArrayXd & xs,
                              const Eigen::ArrayXd & ys) const;

    Eigen::ArrayXd operator()(const Eigen::ArrayXd & times) const;

    Eigen::ArrayXd operator()(const Eigen::ArrayXd & times,
                              const Eigen::ArrayX2d & pos) const;
} ;

}
}
