#pragma once

#include <memory>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"
#include "Math/Functions/ABC.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/Qtf.hpp"
#include "TimeDomain/ReconstructionQtf_WaveCurrentInteraction.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API ReconstructionQtf_WaveCurrentInteractionKim: public ReconstructionQtf_WaveCurrentInteraction
{
public:
    using ReconstructionQtf_WaveCurrentInteraction::ReconstructionQtf_WaveCurrentInteraction ;

    Eigen::ArrayXd operator()(double time, double xGlobal, double yGlobal,
        double heading, double relativeVelocityLocalX,
        double relativeVelocityLocalY) const ;
    using ReconstructionQtf_WaveCurrentInteraction::operator() ;
} ;

}
}
