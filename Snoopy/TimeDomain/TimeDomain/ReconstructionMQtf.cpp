﻿#include <cmath>

#include "ReconstructionMQtf.hpp"
#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"
#include <iomanip>

using namespace BV::Spectral ;
using namespace BV::TimeDomain ;
using namespace BV::Math ;

ReconstructionMQtfLocal::ReconstructionMQtfLocal(
    std::shared_ptr<const Wif> p_wif,
    const MQtf & data,
    const int & degree,
    const ComplexInterpolationStrategies &interpStrategy,
    const double &dwMax,
    unsigned int numThreads) :
        ReconstructionABC(p_wif, numThreads),
        data_(data),
        degree_(degree),
        interpStrategy_(interpStrategy),
        dwMax_(std::min(dwMax, data.getDwMax())),
        qtfMode_(data.getSumMode())
{
    if (data_.isQtf() && !isWif_Unidirectional()) {
        throw std::logic_error("Using unidirectional Qtf for the multi-directional waves (Wif)");
    }
    const Eigen::ArrayXd & ws(p_wif_->getFrequencies()) ;
    const Eigen::ArrayXd & hs(p_wif->getHeadings());
    const Eigen::ArrayXd & phis(p_wif_->getPhases()) ;
    const Eigen::ArrayXd & as(p_wif_->getAmplitudes()) ;
    const Eigen::ArrayXd & ks(p_wif_->getWaveNumbers()) ;
    qtfWMin_ = data_.getFrequencies().minCoeff();
    qtfWMax_ = data_.getFrequencies().maxCoeff();
    Eigen::Index nws(ws.size()) ;
    /*
     * nws is a total number of waves in the wif file:
     *  W_1 = wave 1: w1, a1, p1, b1  (frequency, amplitude, phase, heading)
     *  W_2 = wave 2: w2, a2, p2, b2
     *  ...
     *  W_N = wave nws: ...
     *
     * Let I   = {i : i = 1,2,3, .., N}          => ∀i ∈ I   we have W_i
     *     I_i = {j : j = i, i+1, i+2, .., N}    => ∀j ∈ I_i we have W_j, j = i, i+1, ..., N
     *     J_i = {j : j ∈ I_i & | \omega_{W_j} -\omega_{W_i} | < dwMax_} => |J_i| <= |I_i|
     *     j_0 = position(j) in J_i, j ∈ J_i   => j_0 = 1, 2, ..., |J_i|
     * for j ∈ J_i, i ∈ I
     *  The deltas are defined as
     *      (WJmWI_[i])(j_0) = \omega_{W_j} - \omega_{W_i}
     *  Argument:
     *      (WJ_JI_[i])(j_0) = \omega_{W_j} + qtfMode_ *\omega_{W_i},   qtfMode_ = \pm 1
     *      (PJ_PI_[i])(j_0) = \phi_{W_j}   + qtfMode_ *\phi_{W_i}
     *      (KJ_KI_[i])(j_0) = k_{W_j}      + qtfMode_ *k_{W_i}
     *      (AJ_AI_[i])(j_0) = a_{W_j}      *           a_{W_i}
     * Creating all combinations of waves (taking into account symmetry and antisymmetry)
     */
    Eigen::Index nws_tmp((nws*nws +nws)/2);         // Assume that all diagonal and upper off-diagonal terms are used
    Eigen::ArrayXd wi(nws_tmp);
    Eigen::ArrayXd wjmi(nws_tmp);
    Eigen::ArrayXd wj_i(nws_tmp);
    Eigen::ArrayXd pj_i(nws_tmp);
    Eigen::ArrayXd kj_i(nws_tmp);
    Eigen::ArrayXd aj_i(nws_tmp);
    Eigen::ArrayXXd hji(nws_tmp, 2);
    Eigen::Index nwi(0);                            // counter of the accepted w_i waves
    for (Eigen::Index iw1=0; iw1<nws; ++iw1)        // starting from the wave 1 till the latest wave in the list
    {
        if (ws(iw1) < qtfWMin_ || ws(iw1) > qtfWMax_) continue;  // Ignore this wave, which is out of the qtf range

        for (Eigen::Index iw2=iw1; iw2<nws; ++iw2)  // starting from the wave iw1 till the latest wave in the list
        {
            double eps(2.0);
            if (iw1 == iw2) eps = 1.0;
            if (ws(iw2) < qtfWMin_ || ws(iw2) > qtfWMax_) continue; // Ignore this wave, since it is not in the qtf range
            double wjwi(ws(iw2) - ws(iw1));         // wj_wi == w_iw2 - w_iw1
            double wjwi_abs(abs(wjwi));
            if (wjwi_abs <= dwMax_)                 // only the waves with dw == w_iw2 -w_iw1 <= dwMax_ is taking into account
            {
                // wi(pt) = min(ws(iw1), ws(iw2))
                if (wjwi >= 0) { // ws(iw2) >= ws (iw1)
                    wi(nwi)      = ws(iw1);     // wi = ws(iw1), wj = ws(iw2) = ws(iw1) +|wjwi|
                    wjmi(nwi)    = wjwi;        // wjmi >= 0
                    wj_i(nwi)    = ws(iw2)     +qtfMode_ *ws(iw1);
                    pj_i(nwi)    = phis(iw2)   +qtfMode_ *phis(iw1);
                    aj_i(nwi)    = as(iw2)               *as(iw1)           *eps;
                    hji(nwi, 0)  = hs(iw2);
                    hji(nwi, 1)  = hs(iw1);
                } else {    // ws(iw2) < ws(iw1)
                    wi(nwi)      = ws(iw2);     // wi = ws(iw2), wj = ws(iw1) = ws(iw2) +|wjwi|
                    wjmi(nwi)    = -wjwi;       //wjmi > 0
                    wj_i(nwi)    = ws(iw1)     +qtfMode_ *ws(iw2);
                    pj_i(nwi)    = phis(iw1)   +qtfMode_ *phis(iw2);
                    aj_i(nwi)    = as(iw1)               *as(iw2)           *eps;
                    hji(nwi, 0)  = hs(iw1);
                    hji(nwi, 1)  = hs(iw2);
                }
                // k_{12}^\pm = sqrt( k_1^2 +k_2^2 \pm 2k_1 k_2 \cos(\beta_1 -\beta_2))
                //            = sqrt( k_1^2 +k_2^2 \pm 2k_1 k_2 \cos(\beta_2 -\beta_1))
                kj_i(nwi) = sqrt(abs(ks(iw1)*ks(iw1) +ks(iw2)*ks(iw2) +qtfMode_ * 2.0 *ks(iw1) *ks(iw2) *cos(hs(iw1) -hs(iw2))));
                nwi ++;
            }
        }
        WI_       = wi.head(nwi);       // min(ws(iw1), ws(iw2)) => max(ws(iw1), ws(iw2)) = WI_(pt) +WJmI_(pt)
        WJmI_     = wjmi.head(nwi);     // max(ws(iw1), ws(iw2)) -min(ws(iw1), ws(iw2)) >= 0
        WJI_      = wj_i.head(nwi);     // min(ws(iw1), ws(iw2)) \pm max(ws(iw1), ws(iw2))
        PJI_      = pj_i.head(nwi);
        KJI_      = kj_i.head(nwi);
        AJI_      = aj_i.head(nwi);
        HJI_      = hji.topRows(nwi);
    }
    // Computing number of points where the interpolation must be done
    Eigen::Index npts(WJI_.size());
    // Creating (h1, w1, dw, h2) points
    if (data_.isQtf()) {
        Eigen::ArrayXXd pts(npts, 3);
        for(Eigen::Index pt = 0; pt <npts; pt++) {
            pts(pt, 0) = HJI_(pt, 1);       // hi
            pts(pt, 1) = WI_(pt);           // wi
            pts(pt, 2) = WJmI_(pt);         // wj -wi   >= 0
        }
        // Getting the Qtf at these points
        mqtfAmpPhase_ = data_.getQtfAt(pts, degree_, interpStrategy_);
    } else if (data_.isMQtf()) {
        Eigen::ArrayXXd pts(npts, 4);
        for(Eigen::Index pt = 0; pt <npts; pt++) {
            pts(pt, 0) = HJI_(pt, 1);       // hi
            pts(pt, 1) = WI_(pt);           // wi
            pts(pt, 2) = WJmI_(pt);         // wj -wi   >= 0
            pts(pt, 3) = HJI_(pt, 0);       // hj
        }
        // Getting the Qtf at these points
        mqtfAmpPhase_ = data_.getQtfAt(pts, degree_, interpStrategy_);
    }
}

Eigen::ArrayXXd ReconstructionMQtfLocal::operator()(const Eigen::ArrayXd & times) const
{
    /*
     * pt = n(i,j)
     * WI_(pt)          : W_i: qtfWMin_ <= W_i <= qtfWMax_
     * WJmI_(pt)        : W_j -W_i >= 0, because W_i = min(ws(iw1), ws(iw2)), W_j = max(ws(iw1), ws(iw2))
     * WJI_(pt)         : W_j +qtfMode_ *W_i
     * PJI_(pt)         : P_j +qtfMode_ *P_i
     * KJI_(pt)         : K_j +qtfMode_ *K_i
     * AJI_(pt)         : A_j           *A_i        * eps, where eps = 1 for the iw1 = iw2, and 2 for iw1 != iw2
     */
    Eigen::ArrayXXd res(Eigen::ArrayXXd::Zero(times.size(), data_.getNModes())) ;
    //∀ ∃ ∈
    for(Eigen::Index pt = 0; pt < WJmI_.size(); pt ++) {
        double qtfAmp(mqtfAmpPhase_(pt, 0));
        double qtfPhi(mqtfAmpPhase_(pt, 1));
//        std::cout << "pt       = " << pt+1 << " / " << WJmI_.size() << "\n";
//        std::cout << "WI_(pt)  = " << WI_(pt) << "\n";
//        std::cout << "WJI_(pt) = " << WJI_(pt) << "\n";
//        std::cout << "PJI_(pt) = " << PJI_(pt) << "\n";
//        std::cout << "AJI_(pt) = " << AJI_(pt) << "\n";
//        std::cout << "qtfAmp   = " << qtfAmp << "\n";
//        std::cout << "qtfPhi   = " << qtfPhi << "\n";
        for(decltype(data_.getNModes()) iMode = 0; iMode < data_.getNModes(); iMode ++) {
            for(decltype(times.size()) itime = 0; itime < times.size(); itime++) {
                res(itime, iMode) += AJI_(pt) *qtfAmp
                        *std::cos(WJI_(pt)*times(itime) +PJI_(pt) +qtfMode_ *qtfPhi);
            }
        }
//        std::cout << "--\n";
    }
    return res;
}
