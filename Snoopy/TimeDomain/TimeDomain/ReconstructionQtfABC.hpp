#pragma once

#include <memory>
#include <complex>
#include <set>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"

#include "Spectral/Wif.hpp"
#include "Spectral/Qtf.hpp"
#include "Spectral/MQtf.hpp"
#include "Spectral/QtfTensor.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Math/Tools.hpp"

#include "TimeDomain/Reconstruction.hpp"

namespace BV {
namespace TimeDomain {


template <typename QtfType>
class ReconstructionQtfABC: public ReconstructionABC
{
protected:
    const QtfType & data_;

    typename BV::Spectral::Qtf::IndexType nModes_ ;     // Number of modes: surge, sway, heave, roll, pitch, yaw
    BV::Math::Interpolators::InterpScheme interpScheme_ ;
    BV::Spectral::ComplexInterpolationStrategies interpStrategy_ ;
    BV::Spectral::FrequencyInterpolationStrategies frequencyInterpStrategy_ ;
    BV::Math::Interpolators::ExtrapolationType extrapType_ ;
    double dwMax_;
    double qtfMode_ ;
    /*
     * WJmWI_: deltas of dwij = wj - wi for (i,j): dwij <= dwMax_
     *  ws = wif.getFrequencies()
     * [
     *   [ws(0) -ws(0), ws(1) -ws(0), ws(2) -ws(0), ..., ws(k0) -ws(0)],    // ws(k0+1) -ws(0) > dwMax or k0+1 == N
     *   [ws(1) -ws(1), ws(2) -ws(1), ws(3) -ws(1), ..., ws(k1) -ws(1)],    // ws(k1+1) -ws(1) > dwMax or k1+1 == N
     *   [ws(2) -ws(2), ws(3) -ws(2), ws(4) -ws(2), ..., ws(k2) -ws(2)],    // ws(k2+1) -ws(2) > dwMax or k2+1 == N
     *   ...
     *   [ws(N-1) -ws(N-1)]
     * ]
     */
    std::vector<Eigen::ArrayXd> WJmWI_ ;
    /*
     * |WJmWI_|
     */
    std::vector<Eigen::ArrayXd> WJmWI_abs_ ;
    /*
     * WJ_WI_: wj \pm wi for dwij = wj -wi in WJmWI_[wi]
     *  ws = wif.getFrequencies()
     * [
     *   [ws(0 :k0) \pm ws(0)],
     *   [ws(1 :k1) \pm ws(1)],
     *   [ws(2 :k2) \pm ws(2)],
     *   ...
     *   [ws(N-1) \pm ws(N-1)]
     * ]
     */
    std::vector<Eigen::ArrayXd> WJ_WI_ ;
    /*
     * WJref_: is mainly used for the mQtf case. It keeps the reference to the wj wave's index in the wif object.
     * Since not all wave pairs may be used for the reconstruction, idw and iw indices may not be enough
     */
    std::vector<Eigen::ArrayXi> WJref_;
    /*
     * reference of the wif waves which are <= qtfWMax_ and >= qtfWMin_
     */
    Eigen::ArrayXi WIref_;
    /*
     * AJ_AI_: aj *ai for dwij = wj -wi in WJmWI_[wi]
     *  as = wif.getAmplitudes()
     * [
     *   [as(0 :k0) *as(0)],
     *   [as(1 :k1) *as(1)],
     *   [as(2 :k2) *as(2)],
     *   ...
     *   [as(N-1) * as(N-1)]
     * ]
     */
    std::vector<Eigen::ArrayXd> AJ_AI_ ;
    /*
     * PJ_PI_: see WJ_WI_ but for the phases phis
     *  phis = wif.getPhases()
     */
    std::vector<Eigen::ArrayXd> PJ_PI_ ;
    /*
     * KJ_KI_: see WJ_WI_ but for the wavenumbers ks
     *  ks = wif.getWaveNumbers()
     */
    std::vector<Eigen::ArrayXd> KJ_KI_ ;

    double qtfWMin_, qtfWMax_;

    Eigen::ArrayXd indw_;
    Eigen::ArrayXi d2iw_;
    /**
     * @brief makeIndependentFrequencies: creates a list of unique ordinal set of frequencies
     * The result is written to Eigen::ArrayXd indw_ variable.
     * In order to link a frequency in wif with independent frequencies list,
     * Eigen::ArrayXd d2iw_ is used
     */
    void makeIndependentFrequencies_()
    {
        auto w(p_wif_->getFrequencies());
        auto nws(w.size());
        indw_ = BV::Math::getUnique(w);
        Eigen::Index vs(indw_.size());
        // Loop to find d2iFrequenciesIndices
        if (d2iw_.size() != nws)
            d2iw_.resize(nws);

        for (Eigen::Index iw = 0; iw < nws; ++iw)
            for (Eigen::Index iv = 0; iv < vs; ++iv)
            {
                if (BV::Math::IsClose(w(iw),indw_(iv)))
                {
                    d2iw_(iw) = static_cast<int>(iv);
                    break;
                }
            }
    }

    Eigen::ArrayXd indb_;
    Eigen::ArrayXi d2ib_;
    /**
     * @brief makeIndependentHeadings: creates a list of unique ordinal set of headings
     * The result is written to Eigen::ArrayXd indb_ bariable.
     * The headings are wrapped to 0, 2pi range
     * In order to link a heading from wif with independent frequencies list,
     * Eigen::ArrayXd d2ib_ is used
     */
    void makeIndependentHeadings_(bool wrap = true)
    {
        // Make a copy of the frequencies and not the reference to the original ones
        Eigen::ArrayXd b(p_wif_->getHeadings());
        // is wrap is true, then we modify the headings to be from 0 to 2pi
        if (wrap) BV::Math::WrapAngle0_2PI(b);
        auto nbs(b.size());
        indb_ = BV::Math::getUnique(b);
        Eigen::Index vs(indb_.size());
        // Loop to find d2iHeadingsIndices
        // Since b is a copy of the frequencies and b can be wrapped,
        // d2ib_ still will have correct link between the original frequency list and the list of unique frequencies
        if (d2ib_.size() != (unsigned)nbs)
            d2ib_.resize(nbs);

        for (Eigen::Index ib = 0; ib < nbs; ++ib)
            for (Eigen::Index iv = 0; iv < vs; ++iv)
            {
                if (BV::Math::IsClose(b(ib), indb_(iv)))
                {
                    d2ib_(ib) = static_cast<int>(iv);
                    break;
                }
            }
    }
public:
    ReconstructionQtfABC(
        std::shared_ptr<const BV::Spectral::Wif> p_wif,
        const QtfType & data,
        const BV::Math::Interpolators::InterpScheme & interpScheme =
            BV::Math::Interpolators::InterpScheme::LINEAR,
        const BV::Spectral::ComplexInterpolationStrategies & interpStrategy =
            BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
        const BV::Spectral::FrequencyInterpolationStrategies & frequencyInterpStrategy =
            BV::Spectral::FrequencyInterpolationStrategies::W_DW,
        const BV::Math::Interpolators::ExtrapolationType & extrapType =
            BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
        double dwMax = 1e6,
        unsigned int numThreads=1);

    inline Eigen::Array<BV::Spectral::Modes, Eigen::Dynamic, 1> getModes() {
        return getQtf().getModes();
    }

    inline const QtfType & getQtf() const
    {
        return data_ ;
    }

    inline double getDwMax() const
    {
        return dwMax_;
    }

    inline const Eigen::ArrayXd & getIndependentFrequencies() const
    {
        return indw_ ;
    }

    inline const Eigen::ArrayXi & getDep2IndFrequenciesIndices() const
    {
        return d2iw_ ;
    }

    inline const Eigen::ArrayXd & getIndependentHeadings() const
    {
        return indb_ ;
    }

    inline const Eigen::ArrayXi & getDep2IndHeadingsIndices() const
    {
        return d2ib_ ;
    }

} ; // class ReconstructionQtfABC

template<typename QtfType>
ReconstructionQtfABC<QtfType>::ReconstructionQtfABC(
    std::shared_ptr<const BV::Spectral::Wif> p_wif,
    const QtfType & data,
    const BV::Math::Interpolators::InterpScheme & interpScheme,
    const BV::Spectral::ComplexInterpolationStrategies & interpStrategy,
    const BV::Spectral::FrequencyInterpolationStrategies & frequencyInterpStrategy,
    const BV::Math::Interpolators::ExtrapolationType & extrapType,
    double dwMax,
    unsigned int numThreads) :
        ReconstructionABC(p_wif, numThreads),
        data_(data),
        nModes_(data.getNModes()),
        interpScheme_(interpScheme), interpStrategy_(interpStrategy),
        frequencyInterpStrategy_(frequencyInterpStrategy),
        extrapType_(extrapType),
        dwMax_(std::min(dwMax, data.getDwMax())),
        qtfMode_(data.getSumMode())
{
    const Eigen::ArrayXd & ws(p_wif_->getFrequencies()) ;
    const Eigen::ArrayXd & phis(p_wif_->getPhases()) ;
    const Eigen::ArrayXd & as(p_wif_->getAmplitudes()) ;
    const Eigen::ArrayXd & ks(p_wif_->getWaveNumbers()) ;
    qtfWMin_ = data_.getFrequencies().minCoeff();
    qtfWMax_ = data_.getFrequencies().maxCoeff();
    Eigen::Index nws(ws.size()) ;
    Eigen::ArrayXi wiref(nws);  // by default all waves from the wif is accepted
    Eigen::Index nwi(0);        // counter of the accepted w_i waves
    /*
     * nws is a total number of waves in the wif file:
     *  W_1 = wave 1: w1, a1, p1, b1  (frequency, amplitude, phase, heading)
     *  W_2 = wave 2: w2, a2, p2, b2
     *  ...
     *  W_N = wave nws: ...
     *
     * Let I   = {i : i = 1,2,3, .., N}          => ∀i ∈ I   we have W_i
     *     I_i = {j : j = i, i+1, i+2, .., N}    => ∀j ∈ I_i we have W_j, j = i, i+1, ..., N
     *     J_i = {j : j ∈ I_i & | \omega_{W_j} -\omega_{W_i} | < dwMax_} => |J_i| <= |I_i|
     *     j_0 = position(j) in J_i, j ∈ J_i   => j_0 = 1, 2, ..., |J_i|
     * for j ∈ J_i, i ∈ I
     *  The deltas are defined as
     *      (WJmWI_[i])(j_0) = \omega_{W_j} - \omega_{W_i}
     *  In order to define 'j' from WJmWI_
     *      (WJref_[i])(j_0) = j
     *  Argument:
     *      (WJ_JI_[i])(j_0) = \omega_{W_j} + qtfMode_ *\omega_{W_i},   qtfMode_ = \pm 1
     *      (PJ_PI_[i])(j_0) = \phi_{W_j}   + qtfMode_ *\phi_{W_i}
     *      (KJ_KI_[i])(j_0) = k_{W_j}      + qtfMode_ *k_{W_i}
     *      (AJ_AI_[i])(j_0) = a_{W_j}      *           a_{W_i}
     * Creating all combinations of waves (taking into account symmetry and antisymmetry)
     */
    for (Eigen::Index iw1=0; iw1<nws; ++iw1)   // starting from the wave 1 till the latest wave in the list
    {
        //if (ws(iw1) < qtfWMin_ || ws(iw1) > qtfWMax_) continue;  // Ignore this wave, which is out of the qtf range
        wiref(nwi ++) = iw1;
        std::vector<double> wj_wiTmp;
        std::vector<double> wj_wiAbs;
        std::vector<int> wjref;
        std::vector<double> wj_wi;
        std::vector<double> pj_pi;
        std::vector<double> kj_ki;
        std::vector<double> aj_ai;
        wj_wiTmp.reserve(nws);
        wj_wiTmp.reserve(nws);
        wjref.reserve(nws);
        wj_wi.reserve(nws);
        pj_pi.reserve(nws);
        kj_ki.reserve(nws);
        aj_ai.reserve(nws);
        for (Eigen::Index iw2=iw1; iw2<nws; ++iw2) // starting from the wave iw1 till the latest wave in the list
        {
            //if (ws(iw2) < qtfWMin_ || ws(iw2) > qtfWMax_) continue; // Ignore this wave, since it is not in the qtf range
            double wjwi(ws(iw2) - ws(iw1));        // wj_wi == w_iw2 - w_iw1
            double wjwi_abs(abs(wjwi));
            if (wjwi_abs <= dwMax_)               // only the waves with dw == w_iw2 -w_iw1 <= dwMax_ is taking into account
            {
                wj_wiTmp.push_back(wjwi);                   // if the condition is satisfied, then add this difference to the "vector"
                wj_wiAbs.push_back(wjwi_abs);
                wjref.push_back(static_cast<int>(iw2));     // and save the reference to the wj index
                wj_wi.push_back(ws(iw2)   +qtfMode_ *ws(iw1));
                pj_pi.push_back(phis(iw2) +qtfMode_ *phis(iw1));
                kj_ki.push_back(ks(iw2)   +qtfMode_ *ks(iw1));
                aj_ai.push_back(as(iw2)             *as(iw1));
            }
        }
        std::size_t nwj_wi(wj_wiTmp.size()) ;       // number of all accepted differences (w_iw2 -w_iw1 < dwMax_)
        WIref_ = wiref.head(nwi);                   // == segment(0, nwi)
        WJmWI_.push_back(Eigen::Map<Eigen::ArrayXd>(wj_wiTmp.data(), nwj_wi)) ; // each element of WjmWI_ is an Eigen::ArrayXd, which has the differences between all (accepted) frequencies starting from wave iw1 and the wave iw1
        WJmWI_abs_.push_back(Eigen::Map<Eigen::ArrayXd>(wj_wiAbs.data(), nwj_wi));
        WJref_.push_back(Eigen::Map<Eigen::ArrayXi>(wjref.data(), nwj_wi));
        WJ_WI_.push_back(Eigen::Map<Eigen::ArrayXd>(wj_wi.data(), nwj_wi));     // Eigen::ArrayXd of sums or difs frequencies depending on the qtf mode
        PJ_PI_.push_back(Eigen::Map<Eigen::ArrayXd>(pj_pi.data(), nwj_wi));     // Eigen::ArrayXd of sums or difs phases depending on the qtf mode
        KJ_KI_.push_back(Eigen::Map<Eigen::ArrayXd>(kj_ki.data(), nwj_wi));     // Eigen::ArrayXd of sums or difs wavenumbers depending on the qtf mode
        AJ_AI_.push_back(Eigen::Map<Eigen::ArrayXd>(aj_ai.data(), nwj_wi));     // Eigen::ArrayXd of products of amplitudes
    }
}

}   // TimeDomain
}   // BV
