#pragma once

#include <memory>
#include <Eigen/Dense>

#include "TimeDomainExport.hpp"
#include "Math/Functions/ABC.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/Qtf.hpp"
#include "TimeDomain/ReconstructionQtf.hpp"

namespace BV {
namespace TimeDomain {

class TIME_DOMAIN_API ReconstructionQtf_WaveCurrentInteraction: public ReconstructionQtf
{
protected:
    Eigen::VectorXd invPhaseVelocity_ ;
    Eigen::VectorXd invGroupVelocity_ ;
    Eigen::VectorXd dAlfaOmegaRatio_ ;

    /*!
     * \brief Vessel relative velocity / current projected along the wave spectrum directional vector.
     */
    inline double getLongitudinalVelocity_(
        double vx, double vy, double relativeHeading) const
    {
        return vx * std::cos(relativeHeading)
        + vy * std::sin(relativeHeading) ;
    }

    /*!
     * \brief Vessel relative velocity / current projected along the wave spectrum orthogonal vector.
     */
    inline double getTransversalVelocity_(
        double vx, double vy, double relativeHeading) const
    {
        return -vx * std::sin(relativeHeading)
        + vy * std::cos(relativeHeading) ;
    }

public:
    ReconstructionQtf_WaveCurrentInteraction(
        std::shared_ptr<const Spectral::Wif> p_wif, const Spectral::Qtf & data,
        const Math::Interpolators::InterpScheme & interpScheme =
        Math::Interpolators::InterpScheme::LINEAR,
        const BV::Spectral::ComplexInterpolationStrategies & interpStrategy =
        BV::Spectral::ComplexInterpolationStrategies::RE_IM_AMP,
        const BV::Spectral::FrequencyInterpolationStrategies & frequencyInterpStrategy =
        BV::Spectral::FrequencyInterpolationStrategies::W_DW,
        Math::Interpolators::ExtrapolationType extrapType=
        Math::Interpolators::ExtrapolationType::BOUNDARY,
        double dwMax = 1e6,
        unsigned int numThreads=1) ;

    virtual Eigen::ArrayXd operator()(double time, double xGlobal, double yGlobal,
        double heading, double relativeVelocityLocalX,
        double relativeVelocityLocalY) const ;

    Eigen::ArrayXd operator()(
        double bodyHeading,
        double relativeVelocityLocalX,
        double relativeVelocityLocalY) const ;

    Eigen::ArrayXd operator()(
        double time, const Eigen::Vector3d & posHeading,
        const Eigen::Vector2d & relativeVelocityLocal) const ;
    Eigen::ArrayXXd operator()(
        const Eigen::ArrayXd & times, const Eigen::ArrayXd & xs,
        const Eigen::ArrayXd & ys, const Eigen::ArrayXd & headings,
        const Eigen::ArrayXd & relativeVelocitiesLocalX,
        const Eigen::ArrayXd & relativeVelocitiesLocalY) const ;
    Eigen::ArrayXXd operator()(const Eigen::ArrayXd & times) const ;
    Eigen::ArrayXXd operator()(
        const Eigen::ArrayXd & times, const Eigen::ArrayX3d & posHeadings,
        const Eigen::ArrayX2d & relativeVelocitiesLocal) const ;
} ;

}
}
