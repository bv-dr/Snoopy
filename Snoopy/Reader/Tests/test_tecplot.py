from Snoopy import Reader as rd
from Snoopy import WaveKinematic as wk
import pytest

@pytest.mark.parametrize("filename", [
                                      ( f"{rd.TEST_DIR:}/probes.dat" ),
                                      ( f"{wk.TEST_DATA:}/cn_stream/st_0.05_kd_0.50/FreeSurface_CN_Stream.dat" ),
                                      ( f"{wk.TEST_DATA:}/cn_stream/st_0.12_kd_6.28/FreeSurface_CN_Stream.dat" ),
                                      ]
                         )
def test_readtecplot(filename):
    print(f"Reading file: {filename:}")
    rd.tecplot_HOS(filename)
    print(" : Done!")

if __name__ == "__main__":
    filenames = [
                 f"{rd.TEST_DIR:}/probes.dat",
                 f"{wk.TEST_DATA:}/cn_stream/st_0.05_kd_0.50/FreeSurface_CN_Stream.dat",
                 f"{wk.TEST_DATA:}/cn_stream/st_0.12_kd_6.28/FreeSurface_CN_Stream.dat",
    ]
    for f in filenames:
        test_readtecplot(filename = f)

