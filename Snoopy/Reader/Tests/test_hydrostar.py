import numpy as np
from matplotlib import pyplot as plt
from Snoopy import Reader as rd
from Snoopy import Mechanics as mcn

filename_mcn = f"{rd.TEST_DIR:}/hsmcn_B31.h5"
filename_prs = f"{rd.TEST_DIR:}/p_B31.h5"

def test_hsmcn(display = False):
    """Test reading of hydrostar hsmcn hdf => Snoopy.Rao
    
    For now, just check that it runs
    """
    raos = mcn.HydroCoef.read(filename_mcn).hydro.export_rao_6_dof(kind = "motion")
    assert(True)
    
    if display : 
        raos[0].plot(imode=2)


def test_hsprs(display = False):
    """Test reading of hydrostar hsprs hdf => Snoopy.Rao

    Check that the reconstructed total RAO from HydroStar are the same as HydroStar ones.

    TODO : add test with forward speed.
    """

    # Simply check that this read.
    prs_tot = rd.PressureData.read(filename_prs).prs.export_to_rao(component = "total")
    prs_inc = rd.PressureData.read(filename_prs).prs.export_to_rao(component = "inc") # Should be one
    prs_dif = rd.PressureData.read(filename_prs).prs.export_to_rao(component = "dif")     
    prs_rad2 = rd.PressureData.read(filename_prs).prs.export_to_rao(component = "rad2")
    
    #--- Read data and extract total RAOs calculated by HydroStar
    prs_data = rd.PressureData.read(filename_prs)
    prs_tot_rao_hydrostar = prs_data.prs.export_to_rao(component = "total")
    
    #--- Read motion from HydroStar, and let Snoopy recompose the total from HydroStar decomposed pressure
    motionRaos = mcn.HydroCoef.read(filename_mcn).hydro.export_rao_6_dof(kind = "motion")[0]
    # motionRaos_at_cob = motionRaos.getMotionRaoAtPoint( coords = prs_data.ref_point.values[0,:] , angleUnit = "rad") #Now handled within the prs.export_to_rao
    # prs_abs_rao = prs_data.prs.export_to_rao(component = "total_from_given_motion", motionRaos = [motionRaos_at_cob])
    prs_abs_rao = prs_data.prs.export_to_rao(component = "total_from_given_motion", motionRaos = [motionRaos])
        
    # Choose one point for the check
    point_pos = 10
    
    # Add relative motion component
    z_rao = motionRaos.getMotionRaoAtPoint( coords = prs_data.points[point_pos].values , angleUnit = "rad").getRaoAtMode(2)
    prs_tot_rao_snoopy = prs_abs_rao.getRaoAtMode(point_pos) - z_rao
    
    assert( np.isclose( prs_tot_rao_snoopy.cvalues , prs_tot_rao_hydrostar.cvalues[:,:,point_pos:point_pos+1] ).all() )
    
    if display : 
        # Note i = 37  (point_id = 38 in hstar) corresponds to a point at midship, on starboard
        prs_tot.plot(imode=37).set(title = "Total")
        prs_inc.plot(imode=37).set(title = "INC")
        prs_dif.plot( imode = 37 ).set(title = "DIF")
        prs_rad2.plot( imode = 37 ).set(title = "rad2")
        
        
        fig, ax = plt.subplots()
        prs_tot_rao_snoopy.plot(headingsDeg = [ 180. ], ax=ax, label_prefix = "Snoopy")
        prs_tot_rao_hydrostar.plot(headingsDeg = [ 180. ], ax=ax, ls = "--", label_prefix = "Ref", imode = point_pos)
        ax.legend()
        plt.title("Total pressure from HydroStar vs recomposed pressure from Snoopy")


def test_change_pressures_ref_point(display = False):
    import xarray as xr
    from Snoopy.Reader.hydrostar import PressureData

    motionRaos = mcn.HydroCoef.read(filename_mcn).hydro.export_rao_6_dof(kind = "motion")[0]
    
    old_prsXA = PressureData.read( filename_prs)

    new_ref_point = [ -20.0, -10.0, 300.0]
    new_prsXA = old_prsXA.prs.get_at_ref_point( ref_point = new_ref_point )

    rao_old_CoB = old_prsXA.prs.export_to_rao( motionRaos = [motionRaos], component = "total_from_given_motion")
    rao_new_alt_ref_point = new_prsXA.prs.export_to_rao( motionRaos = [motionRaos], component = "total_from_given_motion")
    
    if display:
        fig , ax = plt.subplots()
        rao_old_CoB.plot(ax=ax, headingsDeg = [135.])
        rao_new_alt_ref_point.plot(ax=ax, headingsDeg = [135.])
        
    assert( np.isclose(  new_prsXA.ref_point.values[:] , new_ref_point ).all() )
    assert( not np.isclose(  old_prsXA.pressure_rad.values, new_prsXA.pressure_rad.values, rtol = 0.4 ).all() ) #we check that radiation pressures are indeed deplaced (ie modified)
    assert( np.isclose(  rao_old_CoB.cvalues , rao_new_alt_ref_point.cvalues, rtol = 1e-6 ).all() ) #we check that all pressures rao (after combined with motion for rad. terms) are quasi equal.


if __name__ == "__main__" :
    test_hsprs(display = True)
    test_hsmcn(display = True)
    test_change_pressures_ref_point(display = True)
