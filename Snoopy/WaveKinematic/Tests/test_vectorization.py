# -*- coding: utf-8 -*-


import numpy as np
from Snoopy.WaveKinematic import VariableBathymetry
from Snoopy import Spectral as sp


"""Obsolete ?
"""

if __name__ == "__main__":

    x0 = 0.0
    xmax = 100.0
    h0 = 30

    slope = 20./100.
    depthFunc = lambda x : -slope*(x-x0) + h0

    wmin = 0.1
    wmax = 2.00
    wstep = 0.1
    wrps = np.arange(wmin,wmax+wstep,wstep)

    wdiff  = np.array([0.05,0.1,0.2])
    heading = np.array([0.0,30.0])

    test = VariableBathymetry(x0=x0, xmax=xmax,
                                    depthFunc = depthFunc,
                                    wrps = wrps, wdiff= wdiff,
                                    heading=heading)

    # check vectorization
    nw = len(wrps)
    ndw = len(wdiff)
    nbhead = len(heading)

    head = np.array(heading) * np.pi/180
    w1  = wrps
    w2  = np.zeros((nw,ndw))

    h = test._depth
    npt = len(h)

    k2   = np.empty((nw,ndw,npt))
    k0   = np.empty((nw,ndw))
    cg0  = np.empty((nw,ndw))
    nu2  = np.empty((nw,ndw,nbhead,npt))
    cg2  = np.empty((nw,ndw,npt))
    amp2 = np.empty((nw,ndw,nbhead,npt))

    kmx = np.empty((nw,ndw,nbhead))
    kmy = np.empty((nw,ndw,nbhead))

    nu2sum  = np.zeros(nu2.shape)

    nusum  = np.zeros(test._nu.shape)



    k = sp.w2k(w1,h0)
    kd = np.empty((ndw,npt))

    mu = np.empty((nw,ndw,nbhead,npt),dtype=complex)

    for iw in range(nw):
        for idw in range(ndw):
            w2[iw,idw]  = w1[iw] - wdiff[idw]
            k0[iw,idw]  = sp.w2k(w2[iw,idw],h0)
            cg0[iw,idw] = sp.w2Cg(w2[iw,idw],h0)
            for ib in range(nbhead):
                print('heading=',heading[ib])
                S=0.
                kmx[iw,idw,ib] =  (k[iw] - k0[iw,idw]) * np.cos(head[ib])
                kmy[iw,idw,ib] =  (k[iw] - k0[iw,idw]) * np.sin(head[ib])

                for ipt in range(npt):
                    k2[iw,idw,ipt]  = sp.w2k(w2[iw,idw],h[ipt])
                    kd[idw,ipt]     = sp.w2k(wdiff[idw],h[ipt])
                    nu2[iw,idw,ib,ipt] = np.sqrt(k2[iw,idw,ipt]**2 - k0[iw,idw]**2 * np.sin(head[ib])**2)
                    cg2[iw,idw,ipt] = sp.w2Cg(w2[iw:iw+1,idw],h[ipt])
                    if k2[iw,idw,ipt]*h[ipt] < 1e-4:
                        r = (h[ipt] / h0)
                    else:
                        r = np.tanh(k2[iw,idw,ipt]*h[ipt]) / np.tanh(k0[iw,idw]*h0)
                    konu = np.sqrt(1.- r**2 * np.sin(head[ib])**2)

                    amp2[iw,idw,ib,ipt]  = np.sqrt( cg0[iw,idw]/cg2[iw,idw,ipt] * np.cos(head[ib]) / konu )

                    mu[iw,idw,ib,ipt] = (kd[idw,ipt]**2 - kmy[iw,idw,ib]**2).astype('complex')**0.5



    for iw in range(nw):
        for idw in range(ndw):
            for ib in range(nbhead):
                for ipt in range(npt):
                    for jpt in range(1,ipt+1):
                        nu2sum[iw,idw,ib,ipt] += nu2[iw,idw,ib,jpt] * test._dx

    for iw in range(nw):
        for ib in range(nbhead):
            for ipt in range(npt):
                for jpt in range(1,ipt+1):
                    nusum[iw,ib,ipt] += test._nu[iw,ib,jpt] * test._dx

    print('')
    disp_error = lambda x,y : print('error=', np.max(np.abs(x-y)))

    disp_error(w2,test._w2)
    disp_error(k2,test._k2)
    disp_error(cg2,test._cg2)
    disp_error(amp2,test._amp2)
    disp_error(kmx,test._kmx)
    disp_error(kmy,test._kmy)
    disp_error(kd,test._kd)
    disp_error(nu2,test._nu2)
    disp_error(mu,test._mu)

    disp_error(nu2sum,test._nu2sum)
    disp_error(nusum,test._nusum)









