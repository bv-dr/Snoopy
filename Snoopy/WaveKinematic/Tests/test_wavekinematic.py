import numpy as np
from Snoopy import Spectral as sp
from Snoopy import WaveKinematic as wk
from Snoopy import TimeDomain as td
from matplotlib import pyplot as plt
from Snoopy import logger

spec = sp.Jonswap( 9.0 , 12.0 , 1. , np.deg2rad(180.))
seaState = sp.SeaState(spec)
sp_wif = sp.Wif( seaState, nbSeed = 50, wmax = 1.8 , seed = 15 )
sp_wif_h100 = sp.Wif( seaState, nbSeed = 50, wmax = 1.8 , seed = 15, depth = 100 )

def not_ok_yet_openmp():

    kin = wk.waveKinematic.FirstOrderKinematic( sp_wif, numThreads = 1 )
    kin_mp = wk.waveKinematic.FirstOrderKinematic( sp_wif, numThreads = 20 )

    time = np.arange( 0, 1000, 0.5 )
    eta = kin.getElevation( time , np.full( time.shape  , 0.0), np.full( time.shape  , 0.0) , speed = 0.0 )
    eta_mp = kin_mp.getElevation( time , np.full( time.shape  , 0.0), np.full( time.shape  , 0.0) , speed = 0.0 )


    err = (eta - eta_mp).max()

    if err >= 1e-5 :
        fig, ax = plt.subplots()
        ax.plot( time ,eta )
        ax.plot( time ,eta_mp )
        plt.show()

    print (err)
    assert(err < 1e-5)


def test_getElevation():
    """Check that 1st order elevation by wk.FirstOrderKinematic is same as from simple reconstruction

    """

    kin = wk.waveKinematic.FirstOrderKinematic( sp_wif, numThreads = 1 )

    time = np.arange( 0, 1000, 0.5 )
    eta_kin = kin.getElevation( time , np.full( time.shape  , 0.0), np.full( time.shape  , 0.0) , speed = 0.0 )

    rec = td.ReconstructionWif( sp_wif )

    eta_wif = rec(time)

    assert(np.isclose(eta_kin, eta_wif).all() )

    print (" 1st order elevation ok")




def test_delta():
    """Check that Delta-Stretching with Delta = 0 yield same results as Wheeler
    """

    kin_delta = wk.DeltaStretching( sp_wif_h100, delta = 0.0, hDelta = 100 )
    kin_wheeler = wk.Wheeler1st( sp_wif_h100 )

    time = np.arange( 0, 1000, 0.5 )
    eta = kin_delta.getElevation( time , np.full( time.shape  , 0.0), np.full( time.shape  , 0.0) , speed = 0.0 )

    t_eta_max = time[eta.argmax()]
    eta_max = np.max(eta)

    v_delta = kin_delta.getVelocity( t_eta_max , 0.0 , 0.0 , eta_max * 0.95)

    v_wheeler = kin_wheeler.getVelocity( t_eta_max , 0.0 , 0.0 , eta_max * 0.95)

    assert(np.isclose(v_delta, v_wheeler).all() )


def test_perf(display = False):
    """Test efficiency of wavekinematic module

    Results are not tested yet
    """
    n_time = 1000
    time = np.linspace( 0, 1000, n_time )

    n_el = 50
    position = np.zeros( (n_el,3) , dtype = float )
    position[:,2] = np.linspace(-15 , 0.0, n_el)

    caseList = [  (wk.Wheeler1st( sp_wif, numThreads = 1 ), "Wheeler"),
                  (wk.FirstOrderKinematic( sp_wif, numThreads = 1 ), "Linear"),
                  (wk.SecondOrderKinematic( sp_wif, numThreads = 1 ), "2nd" ),
                  (wk.SecondOrderKinematic21( sp_wif, numThreads = 1 ) ,"2nd21" ) ]

    res = np.empty( (n_time , n_el, 3 , len(caseList)) )

    for i_kin, case in enumerate( caseList ) :
        kin, label = case
        logger.info("START test" + label)
        for it, t in enumerate(time) :
            for i , xyz in enumerate(position) :
                res[ it , i , : ,i_kin ] = kin.getVelocity( t , *xyz )
        logger.info("STOP test")

    if display :
        fig , ax = plt.subplots()
        ax.plot( time , res[:,0,0] )
        fig , ax = plt.subplots()
        for i_kin, case in enumerate( caseList ) :
            kin, label = case
            ax.plot( position[:,2] , res[0,:,0, i_kin] , label ="Ux " + label )
            ax.plot( position[:,2] , res[0,:,1, i_kin] , label ="Uy "+label)
            ax.plot( position[:,2] , res[0,:,2, i_kin] , label ="Uz "+label)
        ax.legend()
    assert(True)


def plot_wave_kinematic():
    """Plot wave kinematic, (qualitative check)
    """

    T = 10.0
    a = 5.0
    wif = sp.Wif.Airy(a = a , period = T , heading = 0.0)

    l = sp.t2l(T)

    x = np.arange(-l/2, l/2, 5.0)
    z = np.arange(-l/8, 10., 1.0)

    X, Z = np.meshgrid( x, z )

    for name , model in [ ("1st", wk.FirstOrderKinematic),("Wheeler", wk.Wheeler1st) ,("delta-stretching", wk.DeltaStretching), ('2nd order', wk.SecondOrderKinematic)]:

        u = np.full( X.shape , np.nan)
        v = np.full( X.shape , np.nan)

        if name == "delta-stretching":
            kin = model(wif, delta = 0.5)
        else:
            kin = model(wif)

        for ix in range(len(x)) :
            for iz in range(len(z)) :
                if Z[iz, ix] < kin.getElevation( 0.0, X[iz,ix] , 0.0 ) :
                    u[iz,ix], _ , v[iz,ix] = kin.getVelocity( 0.0 , X[iz,ix] , 0.0 , Z[iz, ix] )

        eta = kin.getElevation( 0.0, x , 0.0 )

        va = (u**2+v**2)**0.5

        fig, ax = plt.subplots()
        ax.quiver(X/l, Z,u,v ,va, cmap = "cividis")
        ax.plot(x/l, eta)
        ax.set(title = name, xlabel = r"$x/\lambda$", ylabel = "z")


def plot_delta_stretching(stretchWheelerInTrough = True):
    """Plot Delta-stretching wave kinematics for different parameters
    """

    T = 10.0
    a = 5.0
    wif = sp.Wif.Airy(a = a , period = T , heading = 0.0)

    l = sp.t2l(T)

    x = np.arange(-l/2, l/2, l/2-1.)
    z = np.arange(-l/8, 10., 1.0)

    X, Z = np.meshgrid( x, z )

    fig,ax1 = plt.subplots()
    ax1.set(title = "Horizontal. Trough", xlabel = "z", ylabel = "U")
    fig,ax2 = plt.subplots()
    ax2.set(title = "Vertical. Trough", xlabel = "z", ylabel = "W")
    fig,ax3 = plt.subplots()
    ax3.set(title = "Horizontal. Crest", xlabel = "z", ylabel = "U")
    fig,ax4 = plt.subplots()
    ax4.set(title = "Vertical. Crest", xlabel = "z", ylabel = "W")
    for name , model, delta, h_delta, symbol in [ ("1st", wk.FirstOrderKinematic, None, None, "*"),
                                                  ("Wheeler", wk.Wheeler1st, None, None, "d") ,
                                                  ("$\\Delta$-stretching", wk.DeltaStretching, 0.0, 10., "None"),     # Delta = 0.0, h_delta = 10
                                                  ("$\\Delta$-stretching", wk.DeltaStretching, 0.0, 100., "None"),    # Delta = 0.0, h_delta = 100
                                                  ("$\\Delta$-stretching", wk.DeltaStretching, 0.0, -1., "None"),     # Delta = 0.0, h_delta = -1 (inf)
                                                  ("$\\Delta$-stretching", wk.DeltaStretching, 1.0, 10., "None"),     # Delta = 1.0, h_delta = 10
                                                  ("$\\Delta$-stretching", wk.DeltaStretching, 0.5, 10., "None"),     # Delta = 0.5, h_delta = 10
                                                 ]:

        u = np.full( X.shape , np.nan)
        v = np.full( X.shape , np.nan)

        if delta is None:
            kin = model(wif)
            if name == "Wheeler" and not stretchWheelerInTrough:
                kin.inCrestOnly()
        else:
            kin = model(wif, delta = delta, hDelta = h_delta, inCrestOnly = not stretchWheelerInTrough)

        for ix in range(len(x)) :
            for iz in range(len(z)) :
                if Z[iz, ix] < kin.getElevation( 0.0, X[iz,ix] , 0.0 ) :
                    u[iz,ix], _ , v[iz,ix] = kin.getVelocity( 0.0 , X[iz,ix] , 0.0 , Z[iz, ix] )

        if name != "$\\Delta$-stretching":
            ax1.plot(u[:,0], z, label = name, marker = symbol)
            ax2.plot(v[:,0], z, label = name, marker = symbol)
            ax3.plot(u[:,1], z, label = name, marker = symbol)
            ax4.plot(v[:,1], z, label = name, marker = symbol)
        else:
            ax1.plot(u[:,0], z, label = name + f" ($\\Delta={kin.getDelta():}$, $h_\\Delta={kin.getDepthDelta():}$)")
            ax2.plot(v[:,0], z, label = name + f" ($\\Delta={kin.getDelta():}$, $h_\\Delta={kin.getDepthDelta():}$)")
            ax3.plot(u[:,1], z, label = name + f" ($\\Delta={kin.getDelta():}$, $h_\\Delta={kin.getDepthDelta():}$)")
            ax4.plot(v[:,1], z, label = name + f" ($\\Delta={kin.getDelta():}$, $h_\\Delta={kin.getDepthDelta():}$)")
    ax1.legend()
    ax1.grid()
    ax2.legend()
    ax2.grid()
    ax3.legend()
    ax3.grid()
    ax4.legend()
    ax4.grid()


if __name__ == "__main__" :
    test_perf( display = True )
    test_getElevation()
    plot_wave_kinematic()
    test_delta()
    plot_delta_stretching(stretchWheelerInTrough = True)
