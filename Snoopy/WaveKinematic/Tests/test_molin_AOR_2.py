# -*- coding: utf-8 -*-

import numpy as np
from Snoopy.WaveKinematic import VariableBathymetry
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

def func(x,l,h0,hmax):
    if x<3*l:
        return h0
    elif (x>=3*l and x<6*l):
        slope = (hmax-h0)/(3*l)
        return slope*(x-3*l) + h0
    else:
        return hmax

if __name__ == '__main__':

    plt.close('all')


    Tr = 10.0
    wr = 2.*np.pi/Tr

    wmin = 2.
    wmax = 9.
    nw   = 40

    '''
    old with w2>w1
    w2 = np.linspace(wmin,wmax,nw)
    w1 = w2 + wr
    '''
    
    w1 = np.linspace(wmin,wmax,nw)    
    
    wdiff = np.array([wr])
    heading = np.array([0])

    xL = 0
    hL = 1.05
    slope = 5./100.
    hR = 0.15
    xR = (hL-hR)/slope + xL

    depthFunc = lambda x: -slope*(x-xL) + hL

    test = VariableBathymetry(x0=xL, xmax=xR, depthFunc = depthFunc,
                              wrps=w1, wdiff=wdiff, heading=heading,
                              nstep = 200)

    ax = test.plot_bathymetry()

    test.solve_first_order_problem()

    test.solve_second_order_problem()


    # get the x locations
    depths = np.array([21.,29.,40.]) / 100

    x_locations = [ fsolve( func = lambda x: depthFunc(x) - h, x0=0 )[0]   for h in depths ]

    ax.scatter(x_locations, - depths, marker='.', color='red')

    points = np.array( [  [x,0,0]  for x in x_locations ] )


    R, alpha = test.get_correction_factor(points)

    npt = points.shape[0]

    '''
    Plots
    '''

    # plot R
    fig,ax = plt.subplots()
    for ipt in range(npt):
        ax.plot(test._w1,R[:,:,:,ipt].reshape(-1),label=f'local_depth={depths[ipt]:.2f} [m]')

    ax.set_xlabel(r'$\omega$ [rad/s]')
    ax.set_ylabel(r'$R$')
    ax.set_xlim(wmin,wmax)
    ax.set_ylim(0,1)
    ax.grid(True)
    ax.legend()

    # plot alpha
    fig,ax = plt.subplots()
    for ipt in range(npt):
        ax.plot(test._w1,alpha[:,:,:,ipt].reshape(-1),label=f'local_depth={depths[ipt]:.2f} [m]')

    ax.set_ylabel(r'$alpha$ [degree]')
    ax.set_ylim(0,80)
    ax.grid(True)
    ax.set_xlabel(r'$\omega$ [rad/s]')
    ax.set_xlim(wmin,wmax)
    ax.legend()
