# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
from Snoopy.WaveKinematic import VariableBathymetry

'''
The wave frequencies are f1 = 0.6470Hz and f2 = 0.5005Hz,
and the associated amplitudes are A1 = 0.06m and A2 = 0.012m.
'''

if __name__ == '__main__':

    plt.close('all')

    T1 = 1./0.6470
    T2 = 1./0.5005

    w1 = 2*np.pi/T1
    w2 = 2*np.pi/T2

    wdiff = w1 - w2

    x0 = 0
    xmax = 30.
    xs = 8.5
    xe = 25.0
    hs = 0.7
    he = 0.23

    slope = (hs - he)/(xs-xe)
    depthFunc = lambda x : +slope*(np.clip(x,xs,xe)-xs) + hs

    test = VariableBathymetry(x0=x0, xmax=xmax,
                              depthFunc = depthFunc,
                              wrps = np.array([w1]), wdiff= np.array([wdiff]),
                              heading=np.array([0]),
                              nstep=300)

    ax = test.plot_bathymetry()

    test.solve_first_order_problem()
    test.solve_second_order_problem()

    p = test.get_second_order_potential_mesh().reshape(-1)

    # figure 4.6 pp 79
    fig,ax = plt.subplots()
    iw  = 0
    idw = 0
    ib  = 0
    ax.plot(test._x, p.real,label='real')
    ax.plot(test._x, p.imag,label='imag')
    ax.plot(test._x, np.abs(p),label='abs')
    ax.legend()
    ax.grid(True)
    ax.set_title('second order potential')
    ax.set_xlabel('x [m]')
    ax.set_ylabel(r'$\phi^{(2)}$')