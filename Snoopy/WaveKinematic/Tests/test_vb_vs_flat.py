from Snoopy.WaveKinematic import check_second_order_flat

def test_Incident2_vs_VarBath(w_min = 0.5, w_max = 15, depth = 3., wdif = 0.1, refWave = [0., 0.], plot = False):
    assert(check_second_order_flat(tol = 1.2e-2, x0 = -1.0, depth = depth, w_min = w_min, w_max = w_max, wdif = wdif, refWave = refWave, plot = plot))


if __name__ == "__main__":
    to_Plot = True
    # l' = L l => h' = L h, w' = w /\sqrt{L}
    test_Incident2_vs_VarBath(w_min = 0.5, w_max = 15., depth = 3., wdif = 0.1, plot = to_Plot)
    #test_Incident2_vs_VarBath(w_min = 0.05, w_max = 1.5, depth = 300., wdif = 0.1, plot = to_Plot)
