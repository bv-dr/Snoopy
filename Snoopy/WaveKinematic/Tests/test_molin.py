# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from Snoopy.WaveKinematic import VariableBathymetry


if __name__ == "__main__":

    plt.close("all")

    # input ------------------------------------------------------------------

    x0 = 0.0
    xmax = 100.0
    h0 = 30

    slope = 20/100
    depthFunc = lambda x : -slope*(x-x0) + h0

    T = 12.
    wmin = 2.*np.pi/T
    wmax = wmin
    wstep = wmin/10
    wrps = np.arange(wmin,wmax+wstep,wstep)

    wdiff  = np.array([0.1])
    heading = [0.0,30.]

    # ------------------------------------------------------------------------


    bathymetry = VariableBathymetry(x0=x0, xmax=xmax,
                                    depthFunc = depthFunc,
                                    wrps = wrps, wdiff= wdiff,
                                    heading=heading)

    x  = np.linspace(-300,300,200)
    ax = bathymetry.plot_bathymetry(x = x)

    kinematics = bathymetry.first_order_kinematics(x=x)

    fig,ax = plt.subplots()
    ax.plot(x,kinematics['amplitude'][0,0,:],label=r'$\beta=0$')
    ax.plot(x,kinematics['amplitude'][0,1,:],label=r'$\beta=30$')
    ax.set(
           xlabel='x[m]',
           ylabel='Amplitude')
    ax.grid(True)
    ax.legend()
    ax.set_title('first order wave amplitude')


