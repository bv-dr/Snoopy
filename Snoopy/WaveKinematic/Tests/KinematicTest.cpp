#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <ctime>
#include <cstdio>

#include "WaveKinematic/FirstOrderKinematic.hpp"
#include "WaveKinematic/Wheeler1st.hpp"
#include "WaveKinematic/Wheeler2nd.hpp"
#include "WaveKinematic/SecondOrderKinematic.hpp"
#include "WaveKinematic/DeltaStretcher.hpp"
#include "Spectral/Jonswap.hpp"
#include "Spectral/SeaState.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/MQtf.hpp"
#include "Spectral/Qtf.hpp"
#include "Math/Interpolators/Interpolators.hpp"

#include <bspline.h>
#include <bsplinebuilder.h>
#include <datatable.h>

#include <spdlog/spdlog.h>
#include "spdlog/cfg/env.h"  // support for loading levels from the environment variable
#include "spdlog/fmt/ostr.h" // support for user defined types
                             //
#include <iostream>
#include "WaveKinematic/StreamFunction.hpp"
#include "WaveKinematic/foamStar/foamStarType.H"
#include "WaveKinematic/foamStar/waveModel.H"
#include "WaveKinematic/foamStar/streamFunctionWaveCoeffs.H"


using namespace BV::Spectral;
using namespace BV::WaveKinematic;

std::shared_ptr<Wif> getWif(double wmin, double wmax, int nbwaves, double amplitude_max, double heading = 0.0, double depth = -1)
{
    // wave frequencies: nbwaves from wmin to wmax
    Eigen::VectorXd w = Eigen::VectorXd::LinSpaced(nbwaves, wmin, wmax) ;
    // amplitudes are random values from 0.0 to amplitude_max;
    //Eigen::VectorXd a; a.setRandom(nbwaves); a += Eigen::VectorXd::Ones(nbwaves); a /= 2.0 *amplitude_max ;
    // amplitudes are 0.1 for all waves
    Eigen::VectorXd a = Eigen::VectorXd::Ones(nbwaves) *amplitude_max ;
    // phases: 0
    Eigen::VectorXd phi = Eigen::VectorXd::Zero(nbwaves); //= Eigen::VectorXd::LinSpaced(nbwaves, -M_PI, M_PI) ;
    // heading: 0 in radians
    Eigen::VectorXd head = Eigen::VectorXd::Ones(nbwaves) *heading;
    std::shared_ptr<Wif> p_wif(std::make_shared<Wif>(w, a, phi, head, depth)) ;
    return p_wif;
}
std::shared_ptr<Wif> getWifRandom(double wmin, double wmax, int nbwaves, double amplitude_max, double heading_min = 0.0, double heading_max = 0.0, double depth = -1)
{
    // wave frequencies: nbwaves from wmin to wmax
    Eigen::VectorXd w = Eigen::VectorXd::LinSpaced(nbwaves, wmin, wmax) ;
    // amplitudes are random values from 0.0 to amplitude_max;
    Eigen::VectorXd a(nbwaves); a.setRandom(); a += Eigen::VectorXd::Ones(nbwaves); a /= 2.0 *amplitude_max ;
    // phases: random from -pi to pi
    Eigen::VectorXd phi; phi.setRandom(nbwaves); phi *= M_PI;
    // heading: random in radians from heading_min to heading_max
    Eigen::VectorXd head(nbwaves); head.setRandom(); head += Eigen::VectorXd::Ones(nbwaves); head /= 2.0 /(heading_max -heading_min); head += Eigen::VectorXd::Ones(nbwaves)*heading_min;
    std::shared_ptr<Wif> p_wif(std::make_shared<Wif>(w, a, phi, head, depth)) ;
    return p_wif;
}

void outputWifInfo(std::shared_ptr<const Wif> p_wif, std::fstream &f)
{
    auto nbwaves = p_wif->getAmplitudes().size() ;
    f << "# nbwaves = " << nbwaves << std::endl ;
    for(auto i = 0; i < nbwaves; ++i) {
        f << "# " << i
          << ", a = " << p_wif->getAmplitudes()(i)
          << ", w = " << p_wif->getFrequencies()(i)
          << ", phi = " << p_wif->getPhases()(i)
          << ", head = " << p_wif->getHeadings()(i) << "°\n" ;
    }
}

void pressure_Test(std::shared_ptr<Wif> p_wif)
{
    std::cout << "pressure_Test: " << __LINE__ << " / " __FILE__ << "\n";
    FirstOrderKinematic k1(p_wif) ;
    SecondOrderKinematic k2(p_wif) ;

    double tp_t = 0.0;
    double tp_x = 0.0;
    double tp_y = 0.0;
    double tp_z = 0.0;
    double xmin = -150.0 ;
    double xmax =  150.0 ;
    int ixmax = 2000 ;
    Eigen::VectorXd x = Eigen::VectorXd::LinSpaced(ixmax, xmin, xmax) ;
    std::fstream f ;
    f.setf(std::ios::fixed, std::ios::floatfield) ;
    f.precision(11) ;
    f.open("pressure.dat", std::ios::out) ;
    outputWifInfo(p_wif, f);
    for(auto i = 0; i < ixmax; ++i) {
        tp_x = x(i) ;
        double p1  = k1.getPressure(tp_t, tp_x, tp_y, tp_z) ;
        double p2  = k2.getPressure(tp_t, tp_x, tp_y, tp_z) ;
        double p12 = k2.getPressure1() ; // first order pressure, was calculated in k2.getPresssure(...) method
        double p21 = k2.getPressure21() ;
        double p22 = k2.getPressure22() ;
        f << tp_t << "\t" << tp_x << "\t"  << tp_y << "\t" << tp_z << "\t" << p1 << "\t"  << p2 << "\t" << p12 << "\t" << p21 << "\t" << p22 << std::endl ;
    }
    f.close() ;
}

void velocity_Test(std::shared_ptr<Wif> p_wif)
{
    std::cout << "velocity_Test: " << __LINE__ << " / " __FILE__ << "\n";
    FirstOrderKinematic k1(p_wif) ;
    SecondOrderKinematic k2(p_wif) ;

    double tp_t(0.0);
    double tp_x(0.0);
    double tp_y(0.0);
    double tp_z(0.0);
    double xmin = -150.0 ;
    double xmax =  150.0 ;
    int ixmax = 2000 ;
    Eigen::VectorXd x = Eigen::VectorXd::LinSpaced(ixmax, xmin, xmax) ;
    std::fstream f ;
    f.setf(std::ios::fixed, std::ios::floatfield) ;
    f.precision(11) ;
    f.open("velocity.dat", std::ios::out) ;
    outputWifInfo(p_wif, f);
    for(auto i = 0; i < ixmax; ++i) {
        tp_x = x(i) ;
        Eigen::Vector3d v1 = k1.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        Eigen::Vector3d v2 = k2.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        f << tp_t << "\t" << tp_x << "\t"  << tp_y << "\t" << tp_z << "\t"
          << v1(0) << "\t" << v1(1) << "\t" << v1(2) << "\t"
          << v2(0) << "\t" << v2(1) << "\t" << v2(2)
          << std::endl ;
    }
    f.close() ;
}

double *getDerivatives(const double *f, const double *t, const int &len)
{
    double *res = new double[len];
    *res = 0.0;
    for (int i = 1; i < len; ++i) {
        *(res +i) = (*(f+i) -*(f+i-1))/(*(t+i) -*(t+i-1));
    }
    return res;
}

double *getIntegrals(const double *f, const double *t, const int &len, const double &F0)
{
    double *res = new double[len];
    *res = F0;
    for(int i = 1; i < len; ++i) {
        *(res +i) = *(res +i-1) +*(f+i) * (*(t+i)-*(t+i-1));
    }
    return res;
}

void acceleration_Test(std::shared_ptr<Wif> p_wif, int z)
{
    std::cout << "acceleration_Test: " << __LINE__ << " / " __FILE__ << "\n";
//    FirstOrderKinematic    k(p_wif) ;
//    SecondOrderKinematic21 k(p_wif) ;
    SecondOrderKinematic   k(p_wif) ;

    double tp_t(0.0);
    double tp_x(0.0);
    double tp_y(0.0);
    double tp_z(z);

    double timeMin = 0.0;
    double timeMax = 30.0;
    int iTimeMax = 1000;
    double timeStep = (timeMax -timeMin)/(double)iTimeMax;
    std::cout << "time min : " << timeMin << "\n";
    std::cout << "     max : " << timeMax << "\n";
    std::cout << "     stp : " << timeStep << "\n";
    std::cout << "     int : " << iTimeMax << "\n";

    double *times = new double [iTimeMax];
    double *vel_x = new double [iTimeMax];
    double *vel_y = new double [iTimeMax];
    double *vel_z = new double [iTimeMax];
    double *acc_x = new double [iTimeMax];
    double *acc_y = new double [iTimeMax];
    double *acc_z = new double [iTimeMax];
    double *fs    = new double [iTimeMax];
    double *acc_vel_x = nullptr;
    double *acc_vel_y = nullptr;
    double *acc_vel_z = nullptr;
    double *vel_acc_x = nullptr;
    double *vel_acc_y = nullptr;
    double *vel_acc_z = nullptr;

    // filling time, velocities and accelerations
    for(auto i = 0; i < iTimeMax; ++i) {
        *(times +i) = timeMin +(double)i *timeStep;
        tp_t = *(times +i);
        auto v = k.getVelocity(tp_t, tp_x, tp_y, tp_z);
        auto a = k.getAcceleration(tp_t, tp_x, tp_y, tp_z);
        *(fs +i) = k.getElevation(tp_t, tp_x, tp_y);
        *(vel_x +i) = v(0);
        *(vel_y +i) = v(1);
        *(vel_z +i) = v(2);
        *(acc_x +i) = a(0);
        *(acc_y +i) = a(1);
        *(acc_z +i) = a(2);
    }

    // calculating the accelerations from the velocities
    acc_vel_x = getDerivatives(vel_x, times, iTimeMax);
    acc_vel_y = getDerivatives(vel_y, times, iTimeMax);
    acc_vel_z = getDerivatives(vel_z, times, iTimeMax);

    // calculating the velocities from the accelerations
    vel_acc_x = getIntegrals(acc_x, times, iTimeMax, *vel_x);
    vel_acc_y = getIntegrals(acc_y, times, iTimeMax, *vel_y);
    vel_acc_z = getIntegrals(acc_z, times, iTimeMax, *vel_z);


    // output
    std::fstream f("kinematic.dat", std::fstream::out);
    f.setf(std::ios::fixed, std::ios::floatfield);
    f.precision(8);
    for(auto i=0; i < iTimeMax; ++i) {
        f << *(times +i) << "\t"
          << *(vel_x +i) << "\t" << *(vel_acc_x +i) << "\t"
          << *(vel_y +i) << "\t" << *(vel_acc_y +i) << "\t"
          << *(vel_z +i) << "\t" << *(vel_acc_z +i) << "\t"
          << *(acc_x +i) << "\t" << *(acc_vel_x +i) << "\t"
          << *(acc_y +i) << "\t" << *(acc_vel_y +i) << "\t"
          << *(acc_z +i) << "\t" << *(acc_vel_z +i) << "\t"
          << *(fs +i) << "\t" << z << "\n";
    }
    f.close();

    // cleaning
    delete [] times;
    delete [] vel_x;
    delete [] vel_y;
    delete [] vel_z;
    delete [] acc_x;
    delete [] acc_y;
    delete [] acc_z;
    delete [] acc_vel_x;
    delete [] acc_vel_y;
    delete [] acc_vel_z;
    delete [] vel_acc_x;
    delete [] vel_acc_y;
    delete [] vel_acc_z;
    delete [] fs;

    return;
}

void stretching_Test(std::shared_ptr<Wif> p_wif, double amplitude_max = 0.1 )
{
    std::cout << "stretching_Test: " << __LINE__ << " / " __FILE__ << "\n";
    FirstOrderKinematic k1(p_wif) ;
    SecondOrderKinematic k2(p_wif) ;
    Wheeler1st w1(p_wif) ;
    Wheeler2nd w2(p_wif) ;

    double tp_t(0.0);
    double tp_x(1.0);
    double tp_y(0.0);
    double tp_z(0.0);
    double zmin = -amplitude_max *12 ;
    double zmax =  amplitude_max *12 ;
    int izmax = 200 ;
    Eigen::VectorXd z = Eigen::VectorXd::LinSpaced(izmax, zmin, zmax) ;
    std::fstream f ;
    f.setf(std::ios::fixed, std::ios::floatfield) ;
    f.precision(11) ;
    f.open("stretching.dat", std::ios::out) ;
    outputWifInfo(p_wif, f);
    for(auto i = 0; i < izmax; ++i) {
        tp_z = z(i) ;
        Eigen::Vector3d v1 = k1.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        Eigen::Vector3d v2 = k2.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        Eigen::Vector3d vw1 = w1.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        Eigen::Vector3d vw2 = w2.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        f << tp_t << "\t" << tp_x << "\t" << tp_y << "\t" << tp_z << "\t"
          << v1(0) << "\t" << v1(1) << "\t" << v1(2) << "\t"
          << v2(0) << "\t" << v2(1) << "\t" << v2(2) << "\t"
          << vw1(0) << "\t" << vw1(1) << "\t" << vw1(2) << "\t"
          << vw2(0) << "\t" << vw2(1) << "\t" << vw2(2) << "\t"
          << std::endl ;
    }
    f.close() ;
}

void timeTrace(std::shared_ptr<Wif> p_wif, double amplitude_max, double dt, double tmax)
{
    SecondOrderKinematic k2(p_wif) ;
    double tp_t(0.0);
    double tp_x(0.0);
    double tp_y(0.0);
    double tp_z(0.0);
    double tmin = 0.0 ;
    int itmax = static_cast<int>((tmax -tmin)/dt +0.5);
    Eigen::VectorXd t = Eigen::VectorXd::LinSpaced(itmax, tmin, tmax) ;
    std::fstream f;
    f.open("time.dat", std::ios::out);
    f.precision(11);
    std::chrono::time_point<std::chrono::system_clock> startTime = std::chrono::system_clock::now() ;
    std::time_t st = std::chrono::system_clock::to_time_t(startTime) ;
    std::cout << "chrono start at " << std::ctime(&st) << std::endl;
    for(auto i = 0; i < itmax; i++)
    {
        tp_t = t(i);
        double p2 = k2.getPressure(tp_t, tp_x, tp_y, tp_z);
        Eigen::Vector3d v = k2.getVelocity(tp_t, tp_x, tp_y, tp_z) ;
        double eta2 = k2.getElevation(tp_t, tp_x, tp_y);
        f << tp_t << "\t" << tp_x << "\t" << tp_y << "\t" << tp_z << "\t"
          << p2    << "\t" << eta2  << "\t" << v(0)  << "\t" << v(1)  << "\t" << v(2) << std::endl;
    }
    std::chrono::time_point<std::chrono::system_clock> endTime = std::chrono::system_clock::now() ;
    st = std::chrono::system_clock::to_time_t(endTime);
    std::cout << "chrono end at " << std::ctime(&st) << std::endl;
    std::cout << "      duration " << std::chrono::duration_cast<std::chrono::seconds>(endTime -startTime).count() << std::endl;
    f.close();
}

void velocitySO_Test(std::shared_ptr<Wif> p_wif, double z_min, double z_max, double dz)
{
    SecondOrderKinematic k2(p_wif);
    SecondOrderKinematic21 k21(p_wif);
    FirstOrderKinematic k1(p_wif);
    auto eta_1  = k1.getElevation(0.0, 0.0, 0.0);
    auto eta_21 = k21.getElevation(0.0, 0.0, 0.0);
    auto eta_2  = k2.getElevation(0.0, 0.0, 0.0);
    std::cout << "eta_1 = " << eta_1  << std::endl;
    std::cout << "eta_21= " << eta_21 << std::endl;
    std::cout << "eta_22= " << eta_2  << std::endl;
    std::fstream f;
    f.open("vel_SO.dat", std::ios::out);
    f.precision(11);
    int iz_max = (int)((z_max -z_min)/dz +0.5);
    for(auto iz = 0; iz < iz_max; ++iz) {
        auto z = z_min +iz *dz;
        Eigen::Vector3d v1 = k1.getVelocity(0.0, 0.0, 0.0, z);
        Eigen::Vector3d v21 = k21.getVelocity(0.0, 0.0, 0.0, z);
        Eigen::Vector3d v2 = k2.getVelocity(0.0, 0.0, 0.0, z);
        f << z << "\t"
          << v1(0)  << "\t" << v1(1)  << "\t" << v1(2)  << "\t" << eta_1  << "\t"
          << v21(0) << "\t" << v21(1) << "\t" << v21(2) << "\t" << eta_21 << "\t"
          << v2(0)  << "\t" << v2(1)  << "\t" << v2(2)  << "\t" << eta_2
          << "\n";
    }
    f.close();
}

void test1()
{
    Jonswap swell(5.0, 14.0, 3.3);      //   swell = sp.Jonswap( hs  = 5.0 , tp = 14.0 , gamma = 3.3 , heading = 0.0 )
    Jonswap windsea(5.0, 8.0, 1.0);     //   windsea = sp.Jonswap( hs  = 5.0 , tp = 8.0 , gamma = 1.0 , heading = 0.0 )

    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    std::shared_ptr<ParametricSpectrum> windsea_ptr = std::make_shared<Jonswap>(windsea);
    spectrums.push_back(windsea_ptr);
    spectrums.push_back(std::make_shared<Jonswap>(swell));
    SeaState ss(spectrums);             //   ss = sp.SeaState( [ windsea  ] )

    std::shared_ptr<Wif> p_wif(std::make_shared<Wif>(ss, 0.2, 2.0, 5));           //   wif = sp.Wif( ss, wmin = 0.2 , wmax = 2.0 , nbSeed = 200, seed = 5 )

    //a = wif.amp
    FirstOrderKinematic kin(p_wif);       //   kin = sp.FirstOrderKinematic( wif )

    std::fstream f("test1.dat", std::ios::out);
    for(auto i = 0; i < 25000; i ++)
    {
        auto t = 0.2 *(double)i;
        auto eta = kin.getElevation(t, 0.0, 0.0);       // eta = kin.getElevation_SE( np.arange( 0. , 5000 , 0.2) , [ 0. , 0. ] )
        auto vel = kin.getVelocity(t, 0.0, 0.0, 0.0);   // vel = kin.getVelocity_DF( np.arange( 0. , 5000 , 0.2) , [ 0. , 0. , 0. ] )
        f << t << "\t" << eta << "\t" << vel(0) << "\t" << vel(1) << "\t" << vel(2) << "\n";
    }
    f.close();
}


void printWifInfo(std::shared_ptr<const BV::Spectral::Wif> p_wif, const std::string & wifName = "Wif") {
    auto w(p_wif->getFrequencies());
    auto b(p_wif->getHeadings());
    auto a(p_wif->getAmplitudes());
    auto phi(p_wif->getPhases());
    auto n(w.size());
    std::cout << "Wif : " << wifName << "\n";
    for(auto i = 0; i < n; ++i)
        std::cout << "[" << std::fixed << std::setw(2) << std::setprecision(0) << i << "] : w = " << std::setw(6) << std::setprecision(3) << w(i)
                  << " / a = " << a(i)
                  << " / phi = " << phi(i)
                  << " / b = " << b(i) << "\n" ;
}

void old_Test() {
    std::cout << "The specturm is Jonswap(8.0, 10.0, 1.0, 180.0)\n";
    auto spec = Jonswap(8.0, 10.0, 1.0, 180.0);
    //    Wif wif = getWif(0.5, 1.0, 300, 0.1, M_PI/8.0);
    auto p_wif = std::make_shared<Wif>(spec, 0.05, 2.5, 300, 1, 9);
    //Wif wif("/media/igor/Linux_Partition/dr_git/Snoopy/tester/test.wif");

//    pressure_Test(wif) ;
//    velocity_Test(wif) ;
//    stretching_Test(wif) ;
//    timeTrace(wif, 0.1, 0.5, 10800);
//    velocitySO_Test(wif, -10, 20, 0.01);
//    test1();
    //    int z = 1;     // the point is above the mean free surface
    //    int z = 0;     // the point is on the mean free surface
    int z = -1;    // the point is below the mean free surface
    acceleration_Test(p_wif, z);
}

void test_freeSurface()
{
    std::cout << "test_freeSurface: " << __LINE__ << " / " __FILE__ << "\n";
    std::shared_ptr<Wif> p_wif = getWif(0.5, 1.0, 1, 1, 0, 10);

    FirstOrderKinematic k1(p_wif) ;
    SecondOrderKinematic k2(p_wif) ;

    double tp_t(0.0);
    double tp_x(0.0);
    double tp_y(0.0);
    //double tp_z(0.0);
    double xmin = -150.0 ;
    double xmax =  150.0 ;
    int ixmax = 2000 ;
    Eigen::VectorXd x = Eigen::VectorXd::LinSpaced(ixmax, xmin, xmax) ;
    std::fstream f ;
    f.setf(std::ios::fixed, std::ios::floatfield) ;
    f.precision(11) ;
    std::cout << "The results are written to freeSurface.dat\n";
    f.open("freeSurface.dat", std::ios::out) ;
    outputWifInfo(p_wif, f);
    for(auto i = 0; i < ixmax; ++i) {
        tp_x = x(i) ;
        double z1 = k1.getElevation(tp_t, tp_x, tp_y) ; // starting from 0, 3 elements
        double z2 = k2.getElevation(tp_t, tp_x, tp_y) ;
        double z12 = k2.getElevation1() ; // first order free surface elevation, was calculated in k2.getElevation(...) method
        double z21 = k2.getElevation21() ;
        double z22 = k2.getElevation22() ;
        f << tp_t << "\t" << tp_x << "\t"  << tp_y << "\t" << z1 << "\t"  << z2 << "\t" << z12 << "\t" << z21 << "\t" << z22 << std::endl ;
    }
    f.close() ;
}

void incidentWavesTest()
{
    /*
     * To verify the sum mode of the wave kinematic model.
     * The results are compared with the HydroStars results (hs2nd, incident part)
     * The HydroStar gives as qtf+ p22/g or eta22 values (at z = 0 they are the same).
     *
     * Notice that p21/g and eta21 at z = 0 are different, because eta21 has additional term:
     * \eta^{(1)} \phi^{(1)}_{zt}
     */
    std::cout << "incidentWaveTest: " << __LINE__ << " / " << __FILE__ << "\n";
    std::cout << "the results are written to p22.dat file\n";
    double w_min = 0.05;
    //double w_max = 1.50;
    double dw = 0.05;
    std::cout << std::fixed;
    std::cout << std::showpos;
    std::cout << "Sum\n";
    std::cout << "Single wave\n";
    std::ofstream f("p22.dat");
    f << std::fixed;
    f << "# Sum\n";
    f << "# Single wave\n";
	double tp_t = 0.0;		// 1.5; 15.0
	double tp_x = 21.0;
	double tp_y = 0.0;
	double tp_z = 0.0;
    for (int w = 0; w < 30; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(0.01, w_min +dw *w, 1, 1., 0.0, 20.0);  // single wave of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);        // no need to set sum mode, because for the one wave case the diff results are zero (if the set down is not used!)
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequency: " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ": eta2 = " << std::setw(11) << std::setprecision(11) << eta -eta1
                  << ": p2 = " << std::setw(11) << std::setprecision(11) << p -p1
                  << ": p21 = " << std::setw(11) << std::setprecision(11) << p21
                  << ": p22 = " << std::setw(11) << std::setprecision(11) << p22<< "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "two waves\n";
    f << "# two waves\n";
    for (int w = 0; w < 29; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+1), 2, 1., 0.0, 20.0);  // two waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(+1);                      // set only the sum mode, for diff mode this values must be -1, and for the both: 0
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double eta22 = k.getElevation22();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequencies: w1 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ", w2 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\n";
        std::cout << "    eta1 = " << std::setw(5) << std::setprecision(11) << eta1
                  << " eta21 = " << std::setw(5) << std::setprecision(11) << eta21
                  << " eta22 = " << std::setw(5) << std::setprecision(11) << eta22
                  << " eta = " << std::setw(5) << std::setprecision(11) << eta
                  << "\n";
        std::cout << "    p1   = " << std::setw(5) << std::setprecision(11) << p1
                  << " p21   = " << std::setw(5) << std::setprecision(11) << p21
                  << " p22   = " << std::setw(5) << std::setprecision(11) << p22
                  << " p   = " << std::setw(5) << std::setprecision(11) << p
                  << "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "Three waves\n";
    f << "# Three waves\n";
    for (int w = 0; w < 28; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+2), 3, 1., 0.0, 20.0);  // three waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(+1);
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double eta22 = k.getElevation22();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequencies: w1 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ", w2 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1)
                  << ", w3 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2)
                  << "\n";
        std::cout << "    eta1 = " << std::setw(5) << std::setprecision(11) << eta1
                  << " eta21 = " << std::setw(5) << std::setprecision(11) << eta21
                  << " eta22 = " << std::setw(5) << std::setprecision(11) << eta22
                  << " eta = " << std::setw(5) << std::setprecision(11) << eta
                  << "\n";
        std::cout << "    p1   = " << std::setw(5) << std::setprecision(11) << p1
                  << " p21   = " << std::setw(5) << std::setprecision(11) << p21
                  << " p22   = " << std::setw(5) << std::setprecision(11) << p22
                  << " p   = " << std::setw(5) << std::setprecision(11) << p
                  << "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "Seven waves\n";
    f << "# Seven waves\n";
    for (int w = 0; w < 24; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+6), 7, 1., 0.0, 20.0);  // three waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(+1);
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double eta22 = k.getElevation22();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequencies: w1 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ", w2 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1)
                  << ", w3 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2)
                  << ", w4 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(3)
                  << ", w5 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(4)
                  << ", w6 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(5)
                  << ", w7 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(6)
                  << "\n";
        std::cout << "    eta1 = " << std::setw(5) << std::setprecision(11) << eta1
                  << " eta21 = " << std::setw(5) << std::setprecision(11) << eta21
                  << " eta22 = " << std::setw(5) << std::setprecision(11) << eta22
                  << " eta = " << std::setw(5) << std::setprecision(11) << eta
                  << "\n";
        std::cout << "    p1   = " << std::setw(5) << std::setprecision(11) << p1
                  << " p21   = " << std::setw(5) << std::setprecision(11) << p21
                  << " p22   = " << std::setw(5) << std::setprecision(11) << p22
                  << " p   = " << std::setw(5) << std::setprecision(11) << p
                  << "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(3) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(4) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(5) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(6) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "Dif\n";
    std::cout << "Single wave\n";
    f << "# Dif\n";
    f << "# Single wave\n";
    for (int w = 0; w < 30; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(0.01, w_min +dw *w, 1, 1., 0.0, 20.0);  // single wave of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(-1);
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequency: " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ": eta2 = " << std::setw(11) << std::setprecision(11) << eta -eta1
                  << ": p2 = " << std::setw(11) << std::setprecision(11) << p -p1
                  << ": p21 = " << std::setw(11) << std::setprecision(11) << p21
                  << ": p22 = " << std::setw(11) << std::setprecision(11) << p22<< "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "two waves\n";
    f << "# two waves\n";
    for (int w = 0; w < 29; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+1), 2, 1., 0.0, 20.0);  // two waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(-1);                      // set only the sum mode, for diff mode this values must be -1, and for the both: 0
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double eta22 = k.getElevation22();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequencies: w1 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ", w2 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\n";
        std::cout << "    eta1 = " << std::setw(5) << std::setprecision(11) << eta1
                  << " eta21 = " << std::setw(5) << std::setprecision(11) << eta21
                  << " eta22 = " << std::setw(5) << std::setprecision(11) << eta22
                  << " eta = " << std::setw(5) << std::setprecision(11) << eta
                  << "\n";
        std::cout << "    p1   = " << std::setw(5) << std::setprecision(11) << p1
                  << " p21   = " << std::setw(5) << std::setprecision(11) << p21
                  << " p22   = " << std::setw(5) << std::setprecision(11) << p22
                  << " p   = " << std::setw(5) << std::setprecision(11) << p
                  << "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "Three waves\n";
    f << "# Three waves\n";
    for (int w = 0; w < 28; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+2), 3, 1., 0.0, 20.0);  // three waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(-1);
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double eta22 = k.getElevation22();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequencies: w1 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ", w2 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1)
                  << ", w3 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2)
                  << "\n";
        std::cout << "    eta1 = " << std::setw(5) << std::setprecision(11) << eta1
                  << " eta21 = " << std::setw(5) << std::setprecision(11) << eta21
                  << " eta22 = " << std::setw(5) << std::setprecision(11) << eta22
                  << " eta = " << std::setw(5) << std::setprecision(11) << eta
                  << "\n";
        std::cout << "    p1   = " << std::setw(5) << std::setprecision(11) << p1
                  << " p21   = " << std::setw(5) << std::setprecision(11) << p21
                  << " p22   = " << std::setw(5) << std::setprecision(11) << p22
                  << " p   = " << std::setw(5) << std::setprecision(11) << p
                  << "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    std::cout << "Seven waves\n";
    f << "# Seven waves\n";
    for (int w = 0; w < 24; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+6), 7, 1., 0.0, 20.0);  // three waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(-1);
        double eta = k.getElevation(tp_t, tp_x, tp_y) ;
        double eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double eta22 = k.getElevation22();
        double p   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        double p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        double p22 = k.getPressure22() /9.81;
        std::cout << "Wave frequencies: w1 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0)
                  << ", w2 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1)
                  << ", w3 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2)
                  << ", w4 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(3)
                  << ", w5 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(4)
                  << ", w6 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(5)
                  << ", w7 = " << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(6)
                  << "\n";
        std::cout << "    eta1 = " << std::setw(5) << std::setprecision(11) << eta1
                  << " eta21 = " << std::setw(5) << std::setprecision(11) << eta21
                  << " eta22 = " << std::setw(5) << std::setprecision(11) << eta22
                  << " eta = " << std::setw(5) << std::setprecision(11) << eta
                  << "\n";
        std::cout << "    p1   = " << std::setw(5) << std::setprecision(11) << p1
                  << " p21   = " << std::setw(5) << std::setprecision(11) << p21
                  << " p22   = " << std::setw(5) << std::setprecision(11) << p22
                  << " p   = " << std::setw(5) << std::setprecision(11) << p
                  << "\n";
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(1) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(2) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(3) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(4) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(5) << "\t"
          << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(6) << "\t"
          << std::setw(14) << std::setprecision(11) << p22
          << "\n";
    }
    f << "# eta21\n";
    f << "# Single wave\n";
    for (int w = 0; w < 30; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(0.01, w_min +dw *w, 1, 1., 0.0, 20.0);  // single wave of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        double eta1 = k.getElevation(tp_t, tp_x, tp_y) ;
        eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double p1   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(14) << std::setprecision(11) << eta1 << "\t"
          << std::setw(14) << std::setprecision(11) << eta21 << "\t"
          << std::setw(14) << std::setprecision(11) << p1 << "\t"
          << std::setw(14) << std::setprecision(11) << p21 << "\t"
          << "\n";
    }
    f << "# two waves\n";
    for (int w = 0; w < 29; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+1), 2, 1., 0.0, 20.0);  // two waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        double eta1 = k.getElevation(tp_t, tp_x, tp_y) ;
        eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double p1   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(14) << std::setprecision(11) << eta1 << "\t"
          << std::setw(14) << std::setprecision(11) << eta21 << "\t"
          << std::setw(14) << std::setprecision(11) << p1 << "\t"
          << std::setw(14) << std::setprecision(11) << p21 << "\t"
          << "\n";
    }
    f << "# Three waves\n";
    for (int w = 0; w < 28; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+2), 3, 1., 0.0, 20.0);  // three waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        double eta1 = k.getElevation(tp_t, tp_x, tp_y) ;
        eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double p1   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(14) << std::setprecision(11) << eta1 << "\t"
          << std::setw(14) << std::setprecision(11) << eta21 << "\t"
          << std::setw(14) << std::setprecision(11) << p1 << "\t"
          << std::setw(14) << std::setprecision(11) << p21 << "\t"
          << "\n";
    }
    std::cout << "Seven waves\n";
    f << "# Seven waves\n";
    for (int w = 0; w < 24; ++ w) {  //
        std::shared_ptr<Wif> p_wif = getWif(w_min +dw *w, w_min +dw *(w+6), 7, 1., 0.0, 20.0);  // three waves of amplitude 1, and heading 0.0. Fluid depth = 20.0 m.
        SecondOrderKinematic k(p_wif);
        k.setMode(-1);
        double eta1 = k.getElevation(tp_t, tp_x, tp_y) ;
        eta1 = k.getElevation1();
        double eta21 = k.getElevation21();
        double p1   = k.getPressure(tp_t, tp_x, tp_y, tp_z) /9.81;       // pressure/\rho to [m]
        p1  = k.getPressure1() /9.81;
        double p21 = k.getPressure21() /9.81;
        f << std::setw(5) << std::setprecision(2) << p_wif->getFrequencies()(0) << "\t"
          << std::setw(14) << std::setprecision(11) << eta1 << "\t"
          << std::setw(14) << std::setprecision(11) << eta21 << "\t"
          << std::setw(14) << std::setprecision(11) << p1 << "\t"
          << std::setw(14) << std::setprecision(11) << p21 << "\t"
          << "\n";
    }
    f.close();
}

void dudz0_test()
{
    /*
     * Before compiling this test, make get1stVelocityDzAt0 in WaveKinematicABC.hpp to be declared as public
     * and uncomment the line:
     * dudz0 = kin.get1stVelocityDzAt0(t, x, y);
     *
     * this test is to chech that the 
     * (u(x, y, z) -u(x, y, 0.0))/z = dudz0(x, y)
     */
    std::cout << "dudz0_test: " << __LINE__ << " / " << __FILE__ << "\n";
    double t(1.0);
    double x(1.0);
    double y(2.1);
    double z(-1.0e-5);  // to be sure that the velocity will be calculated
    int n(30);          // n waves
    double wmin(0.3);
    double wmax(1.2);
    double amp_max(1.0);
    double heading_min(-M_PI);
    double heading_max( M_PI);
    double depth(2.0);
    std::cout << "Number of waves: " << n << "\n";
    std::cout << "Min frequency  : " << wmin << "\n";
    std::cout << "Max frequency  : " << wmax << "\n";
    std::cout << "Max amplitude  : " << amp_max << "\n";
    std::cout << "Min Heading    : " << heading_min << "\n";
    std::cout << "Max Heading    : " << heading_max << "\n";
    std::cout << "Depth          : " << depth << "\n";
    std::cout << "x              : " << x << "\n";
    std::cout << "y              : " << y << "\n";
    std::cout << "time           : " << t << "\n";
    srand((unsigned int) time(0));
    std::shared_ptr<Wif> p_wif(getWifRandom(wmin, wmax, n, amp_max, heading_min, heading_max, depth));
    std::cout << "Generated wif file:\n";
    std::cout << "\t#N  : Freq    Amp           Phase         Heading\n";
    {
        auto w(p_wif->getFrequencies());
        auto a(p_wif->getAmplitudes());
        auto p(p_wif->getPhases());
        auto h(p_wif->getHeadings());
        std::cout << std::fixed;
        for(int iw = 0; iw < n; ++iw)
            std::cout << "\t" << std::setw(3) << iw +1 << " : "
                << std::setw(5) << std::setprecision(2) << w(iw) << "   "
                << std::setw(11) << std::setprecision(8) << a(iw) << "   "
                << std::setw(11) << std::setprecision(8) << p(iw) << "   "
                << std::setw(6) << std::setprecision(2) << h(iw)*180./M_PI
                << "\n";
    }
    FirstOrderKinematic kin(p_wif);
    Eigen::Vector3d dudz0(3);
    dudz0(0) = 0.;
    dudz0(1) = 0.;
    dudz0(2) = 0.;
    //dudz0 = kin.get1stVelocityDzAt0(t, x, y);
    auto u0(kin.getVelocity(t, x, y, z));
    int idz = 10;
    double dz(1.0);
    std::cout << "du/dz(x,y,0) = " << dudz0(0) << ", " << dudz0(1) << ", " << dudz0(2) << "\n";
    std::cout << "                 Relative errors\n";
    std::cout << "dz         dux         duy         duz\n";
    std::cout << std::scientific;
    for(int iz = 0; iz < idz; ++iz)
    {
        auto u1(kin.getVelocity(t, x, y, z +dz));
        auto dif = (u1-u0)/dz -dudz0;
        std::cout << std::setw(9) << std::setprecision(3) << dz << "  "
            << std::setw(10) << std::setprecision(3) <<dif(0) << "  "
            << std::setw(10) << std::setprecision(3) <<dif(1) << "  "
            << std::setw(10) << std::setprecision(3) <<dif(2)
            << "\n";
        dz = dz/10.;
    }
}

void deltaStretching_test(double depth, double eta, bool inCrestOnly = true)
{
    /*
     * tests:
     *  1) depending on d_delta (small or large) 2 different implementations of the formula are used to stretch
     *  2) stretching for different deltas with d_delta == depth (2.0 for infinite depth case)
     *  3) stretching for different deltas with d_delta == 0.5 depth (1.0 for infinte depthcase)
     *
     * parameters: depth
     */
    std::cout << "deltaStretching_test: " << __LINE__ << " / " << __FILE__ << "\n";
    double t(1.0);
    double x(1.0);
    double y(2.1);
    int n(10);          // n waves
    double wmin(0.3);
    double wmax(1.2);
    double amp_max(1.0);
    double heading_min(-M_PI);
    double heading_max( M_PI);
    std::cout << "Number of waves: " << n << "\n";
    std::cout << "Min frequency  : " << wmin << "\n";
    std::cout << "Max frequency  : " << wmax << "\n";
    std::cout << "Max amplitude  : " << amp_max << "\n";
    std::cout << "Min Heading    : " << heading_min << "\n";
    std::cout << "Max Heading    : " << heading_max << "\n";
    std::cout << "Depth          : " << depth << "\n";
    std::cout << "x              : " << x << "\n";
    std::cout << "y              : " << y << "\n";
    std::cout << "time           : " << t << "\n";
    //srand((unsigned int) time(0));
    std::shared_ptr<Wif> p_wif(getWifRandom(wmin, wmax, n, amp_max, heading_min, heading_max, depth));
    std::cout << "Generated wif file:\n";
    std::cout << "\t#N  : Freq    Amp           Phase         Heading\n";
    {
        auto w(p_wif->getFrequencies());
        auto a(p_wif->getAmplitudes());
        auto p(p_wif->getPhases());
        auto h(p_wif->getHeadings());
        std::cout << std::fixed;
        for(int iw = 0; iw < n; ++iw)
            std::cout << "\t" << std::setw(3) << iw +1 << " : "
                << std::setw(5) << std::setprecision(2) << w(iw) << "   "
                << std::setw(11) << std::setprecision(8) << a(iw) << "   "
                << std::setw(11) << std::setprecision(8) << p(iw) << "   "
                << std::setw(6) << std::setprecision(2) << h(iw)*180./M_PI
                << "\n";
    }
    {
        double dd_tmp(-1.);
        if (depth < 0.0)
            dd_tmp = 2.0;
        DeltaStretching kin(p_wif, 0.5, dd_tmp, inCrestOnly);
        std::cout << "DeltaStretching object\n";
        std::cout << "\tdelta = " << kin.getDelta() << "\n";
        std::cout << "\td_delta = " << kin.getDepthDelta() << "\n";
        int ndz(11);
        double z_start(depth);
        if (depth < 0.0)
            z_start = 2.0;
        double dz((eta+z_start)/float(ndz-1));
        std::cout << std::fixed;
        std::cout << "zs1 : formula for stretching for large d_delta\n";
        std::cout << "zs2 : formula for stretching for small d_delta\n";
        std::cout << "  z        zs1      zs2\n";
        for (int iz = 0; iz < ndz; ++iz) {
            double z(-z_start +dz *iz);
            /*
             * if d_delta == depth > 0 => dd_tmp == -1 => d_delta == depth => 0.9depth < depth < 1.1depth => large and small d_delta formulae will be used
             * if depth == -1 (inf) and d_delta == -1 => formula for inf d_delta will be used => 0.9*z_start and 1.1*z_start will be ignored
             * if depth == -1 (inf) => d_delta == dd_tmp == 2.0 > 0 => see the first (finite depth case) explanation
             */
            double zs1(kin.stretcher(z, eta, 0.9*dd_tmp));
            double zs2(kin.stretcher(z, eta, 1.1*dd_tmp));
            std::cout 
                << std::setw(7) << std::setprecision(4) << z << "  "
                << std::setw(7) << std::setprecision(4) << zs1 << "  "
                << std::setw(7) << std::setprecision(4) << zs2
                << "\n";
        }
    }
    {
        std::cout << "Testing delta == 0.00, 0.25, 0.50, 0.75 and 1.0 for d_delta == depth\n";
        std::cout << "\tzs1 : delta = 0.00 (linear stretching, aka Wheeler)\n";
        std::cout << "\tzs2 : delta = 0.25\n";
        std::cout << "\tzs3 : delta = 0.50\n";
        std::cout << "\tzs4 : delta = 0.75\n";
        std::cout << "\tzs5 : delta = 1.00\n";
        double dd_tmp(-1.);
        if (depth < 0.0)
            dd_tmp = 2.0;
        DeltaStretching kin1(p_wif, 0.00, dd_tmp, inCrestOnly);
        DeltaStretching kin2(p_wif, 0.25, dd_tmp, inCrestOnly);
        DeltaStretching kin3(p_wif, 0.50, dd_tmp, inCrestOnly);
        DeltaStretching kin4(p_wif, 0.75, dd_tmp, inCrestOnly);
        DeltaStretching kin5(p_wif, 1.00, dd_tmp, inCrestOnly);
        int ndz(11);
        double z_start(depth);
        if (depth < 0.0)
            z_start = 2.0;
        double dz((eta+z_start)/float(ndz-1));
        std::cout << std::fixed;
        std::cout << "           0.00     0.25     0.50     0.75     1.00\n";
        std::cout << "  z        zs1      zs2      zs3      zs4      zs5\n";
        for (int iz = 0; iz < ndz; ++iz) {
            double z(-z_start +dz *iz);
            double zs1(kin1.stretcher(z, eta));
            double zs2(kin2.stretcher(z, eta));
            double zs3(kin3.stretcher(z, eta));
            double zs4(kin4.stretcher(z, eta));
            double zs5(kin5.stretcher(z, eta));
            std::cout 
                << std::setw(7) << std::setprecision(4) << z << "  "
                << std::setw(7) << std::setprecision(4) << zs1 << "  "
                << std::setw(7) << std::setprecision(4) << zs2 << "  "
                << std::setw(7) << std::setprecision(4) << zs3 << "  "
                << std::setw(7) << std::setprecision(4) << zs4 << "  "
                << std::setw(7) << std::setprecision(4) << zs5
                << "\n";
        }
    }
    {
        std::cout << "Testing delta == 0.00, 0.25, 0.50, 0.75 and 1.0 for d_delta == 0.5 depth\n";
        std::cout << "\tzs1 : delta = 0.00 (linear stretching, aka Wheeler) starting from d_delta = 0.5*depth (finite), 1.0 (infinite)\n";
        std::cout << "\tzs2 : delta = 0.25\n";
        std::cout << "\tzs3 : delta = 0.50\n";
        std::cout << "\tzs4 : delta = 0.75\n";
        std::cout << "\tzs5 : delta = 1.00\n";
        std::cout << "\td_delta = " << 0.5*depth << "\n";
        double dd_tmp(depth);
        if (depth < 0.0)
            dd_tmp = 2.0;
        DeltaStretching kin1(p_wif, 0.00, 0.5*dd_tmp, inCrestOnly);
        DeltaStretching kin2(p_wif, 0.25, 0.5*dd_tmp, inCrestOnly);
        DeltaStretching kin3(p_wif, 0.50, 0.5*dd_tmp, inCrestOnly);
        DeltaStretching kin4(p_wif, 0.75, 0.5*dd_tmp, inCrestOnly);
        DeltaStretching kin5(p_wif, 1.00, 0.5*dd_tmp, inCrestOnly);
        int ndz(11);
        double z_start(depth);
        if (depth < 0.0)
            z_start = 2.0;
        double dz((eta+z_start)/float(ndz-1));
        std::cout << std::fixed;
        std::cout << "           0.00     0.25     0.50     0.75     1.00\n";
        std::cout << "  z        zs1      zs2      zs3      zs4      zs5\n";
        for (int iz = 0; iz < ndz; ++iz) {
            double z(-z_start +dz *iz);
            double zs1(kin1.stretcher(z, eta));
            double zs2(kin2.stretcher(z, eta));
            double zs3(kin3.stretcher(z, eta));
            double zs4(kin4.stretcher(z, eta));
            double zs5(kin5.stretcher(z, eta));
            std::cout 
                << std::setw(7) << std::setprecision(4) << z << "  "
                << std::setw(7) << std::setprecision(4) << zs1 << "  "
                << std::setw(7) << std::setprecision(4) << zs2 << "  "
                << std::setw(7) << std::setprecision(4) << zs3 << "  "
                << std::setw(7) << std::setprecision(4) << zs4 << "  "
                << std::setw(7) << std::setprecision(4) << zs5
                << "\n";
        }
    }
    // Velocity is checked in the python

}

void StreamFunction_test()
{
    std::cout << "Stream function test\n";

    double time(1.);
    double x(2.);
    double z(-1.);
    //                depth H    omega
    StreamFunction sf(5.,   0.1, 1.);
    Eigen::Vector3d U(sf.getVelocity(time, x, z));

    std::cout << "omega = " << sf.getFrequency() << "\n";
    std::cout << "k     = " << sf.getWaveNumber() << "\n";
    std::cout << "eta   = " << sf.getElevation(time, x) << "\n";
    std::cout << "u     = " << U(0) << "\n";
    std::cout << "v     = " << U(1) << "\n";
    std::cout << "w     = " << U(2) << "\n";

}

int main()
{
    spdlog::info("The Kinematic test\n");
    //spdlog::warn("The Kinematic test\n");
    //std::cout << "The Kinematic test\n" ;

    //dudz0_test();     // READ comments to this function
    //deltaStretching_test(2.0, 1.0);     // finite depth case, eta = 1.0 > 0
    //deltaStretching_test(-1.0, 1.0);     // infinte depth case, eta = 1.0 > 0
    //deltaStretching_test(2.0, -1.0, true);     // finite depth case, eta = -1.0 < 0 => no stretching!
    //deltaStretching_test(2.0, -1.0, false);     // finite depth case, eta = -1.0 < 0 => no stretching!

    //old_Test();
    //test_freeSurface();
    //incidentWavesTest();
    //
    StreamFunction_test();

#if 0
    // chrono test
    /*
     * time:
     *      std::chrono::system_clock::now() returns std::chrono::system_clock::time_point
     *      std::chrono::system_clock::time_point::time_since_epoch() returns std::chrono::duration from "epoch" in periods
     *      period = num/denom, where num   = std::chrono::system_clock::period::num,
     *                                denom = std::chrono::system_clock::period::den.
     * Thus, to get time in seconds we need multiply duration by num and divide by denom
     */
    auto t1 = std::chrono::system_clock::now() ;
    unsigned long t1p = t1.time_since_epoch().count();
    auto t1s = t1p *std::chrono::system_clock::period::num /std::chrono::system_clock::period::den;
    auto t1m = t1s /60;
    auto t1h = t1m /60;
    auto t1d = t1h /24;
    std::cout << "t1 (in periods) = " << t1p << "\n";
    std::cout << "t1 (in seconds) = " << t1s << "\n";
    std::cout << "t1 (in minutes) = " << t1m << "\n";
    std::cout << "t1 (in hours)   = " << t1h << "\n";
    std::cout << "t1 (in days)    = " << t1d << "\n";
    t1 = std::chrono::system_clock::now();
    t1p = t1.time_since_epoch().count();
    std::cout << "Value t1 (unsigned long) = " << t1p << "\n";
    std::cout << "Value t1 (unsigned int)  = " << (unsigned int) t1p << "\n";
    t1 = std::chrono::system_clock::now();
    t1p = t1.time_since_epoch().count();
    std::cout << "Value t1 (unsigned long) = " << t1p << "\n";
    std::cout << "Value t1 (unsigned int)  = " << (unsigned int) t1p << "\n";
    t1 = std::chrono::system_clock::now();
    t1p = t1.time_since_epoch().count();
    std::cout << "Value t1 (unsigned long) = " << t1p << "\n";
    std::cout << "Value t1 (unsigned int)  = " << (unsigned int) t1p << "\n";
    std::cout << std::fixed ;
    std::cout << std::defaultfloat ;
#endif
    return 0;
}
