# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from Snoopy.WaveKinematic import VariableBathymetry

def depthFunc0(x):
    if x<0.:
        return +0.7
    elif (x>=0 and x<17.5):
        slope = 1./35.
        return -slope*x + 0.7
    elif (x>=17.5 and x<22.):
        slope = 1./45.
        return -slope*(x-17.5) + 0.2
    elif (x>=22. and x<34.):
        slope = 1./60.
        return +slope*(x-22.0) + 0.1
    else:
        return +0.3

'''
The wave periods are T1 = 1.546s and T2 = 2s,
and the associated amplitudes are A1 = 0.061m and A2 = 0.008m
'''

if __name__ == '__main__':

    plt.close('all')
    
    
    T1 = 2.0
    T2 = 1.546
    A1 = 0.008
    A2 = 0.061
    
    
    '''
    # old defition with w1>w2
    T1 = 1.546
    T2 = 2.0
    A1 = 0.061
    A2 = 0.008
    '''
    
    w1 = 2*np.pi/T1
    w2 = 2*np.pi/T2

    wrps = np.array([w1])
    wdiff = np.array([w2 - w1])
    heading = np.array([0])

    x0   = 0.0
    xmax = 34.0

    #depthFunc  = lambda x: np.array(list(map(depthFunc0,x)))
    depthFunc  = lambda x: np.vectorize(depthFunc0)(x)

    test = VariableBathymetry(x0=x0, xmax=xmax, depthFunc = depthFunc,
                              wrps=wrps, wdiff=wdiff, heading=heading,
                              nstep = 500)
    ax = test.plot_bathymetry()

    test.solve_first_order_problem()
    test.solve_second_order_problem()

    p = test.get_second_order_potential_mesh().reshape(-1)

    elv = A1*A2*p / test._grav

    # check pressure integration
    npt = 20
    zs  = 0
    points = np.zeros( (npt,3) )
    points[:,0] = np.linspace(x0,xmax,npt)
    points[:,2] = zs
    p1 = test.get_second_order_potential(points).reshape(-1)

    # fig 5 pp 7
    fig,ax = plt.subplots()
    ax.plot(test._x, elv.real, label='real')
    ax.plot(test._x, elv.imag,label='imag')
    ax.plot(test._x, np.abs(elv),label='abs')
    ax.legend()
    ax.grid(True)
    ax.set_xlabel('x[m]')
    ax.set_ylabel(r'$\eta^{(2)}$')
    ax.set_title('second order wave elevation')

    # comparaison
    fig,ax = plt.subplots()
    ax.plot(test._x, np.abs(p), label='on mesh')
    ax.plot(points[:,0], np.abs(p1), marker='.', linestyle='', label='on points')
    ax.legend()
    ax.grid(True)
    ax.set_xlabel('x[m]')
    ax.set_ylabel(r'$\phi^{(2)}$')
    ax.set_title('pressure computation check')
    
        