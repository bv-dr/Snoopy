# -*- coding: utf-8 -*-


import numpy as np
from scipy.integrate import quad
from Snoopy.WaveKinematic.bathymetry_utils import integrate_fz, qm

def f(z,k,h):
    return ( np.exp(k*z) + np.exp(-k*(z+2.*h)) ) / (1. + np.exp(-2*k*h))

def test_analytic():

    k1 = 1.5
    k2 = 2.0
    h1  = 10
    h2  = 10
    d   = 8

    grav = 9.81

    w1 = np.sqrt(grav*k1*np.tanh(k1*h1))
    w2 = np.sqrt(grav*k2*np.tanh(k2*h1))

    g  = lambda z: f(z,k1,h1) * f(z,k2,h2)

    I_exact,_ = quad(g,-d,0)

    I = integrate_fz(k1,k2,h1,h2,d)

    print('I=', I )
    print('I_exact=', I_exact )
    print('error=',abs(I-I_exact))
    assert( np.isclose( I , I_exact ) )


if __name__ == "__main__":

    test_analytic()

    k1 = 1.5
    k2 = 2.0
    h1  = 10
    h2  = 10
    d   = 8
    grav = 9.81
    w1 = np.sqrt(grav*k1*np.tanh(k1*h1))
    w2 = np.sqrt(grav*k2*np.tanh(k2*h1))

    import timeit
    t = timeit.Timer(lambda:integrate_fz(k1,k2,h1,h2,d))
    print(t.timeit(100000))

    t1 = timeit.Timer(lambda: qm(w2,w1,k2*h1,k1*h1,0.))
    print(t1.timeit(100000))

