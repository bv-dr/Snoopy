# -*- coding: utf-8 -*-
"""
Created on Mon May  3 08:45:17 2021

@author: couledhousseine
"""

import numpy as np
import matplotlib.pyplot as plt
from Snoopy.WaveKinematic import VariableBathymetry


if __name__ == '__main__':

    plt.close('all')

    T1    = 15.0
    Tbeat = 100.0
    T2 = Tbeat*T1/(Tbeat-T1)

    w1 = 2*np.pi/T1
    w2 = 2*np.pi/T2

    wrps = np.array([w1])
    wdiff = np.array([w1-w2])
    heading = np.array([0])


    '''
    figure 2 pp 4 : change T1 and Tbeat to compare
    '''

    slopes = [0.5, 1., 2., 4.]

    fig, ax  = plt.subplots()
    fig ,ax1 = plt.subplots()

    for slope in slopes:

        m0   = slope/100
        h0   = 100.
        hmax = 5.
        x0   = 0.
        xmax = -(hmax-h0) / m0 + x0


        depthFunc = lambda x : -m0*(x-x0) + h0

        test = VariableBathymetry(x0=x0, xmax=xmax, depthFunc = depthFunc,
                                  wrps=wrps, wdiff=wdiff, heading=heading,
                                  nstep = 2000)

        #ax = test.plot_bathymetry()

        test.solve_first_order_problem()
        test.solve_second_order_problem()

        depth = test.get_depth()

        R, alpha = test.get_correction_factor()

        label_txt = f'slope = {slope:2.2f}%'


        ax.plot(depth,R.reshape(-1),label=label_txt)
        ax.set_xlim(0,40)
        ax.set_ylim(0,1.2)
        ax.grid(True)
        ax.set_xlabel('h [m]')
        ax.set_ylabel(r'$R$')
        ax.legend()
        ax.set_title(rf'$T_1$={T1:.2f} [s] - $T_2$={T2:.2f} [s]')


        ax1.plot(depth,alpha.reshape(-1),label=label_txt)
        ax1.set_xlim(0,40)
        ax1.set_ylim(0,90)
        ax1.grid(True)
        ax1.set_xlabel('h [m]')
        ax1.set_ylabel(r'$\alpha$')
        ax1.legend()
        ax1.set_title(rf'$T_1$={T1:.2f} [s] - $T_2$={T2:.2f} [s]')