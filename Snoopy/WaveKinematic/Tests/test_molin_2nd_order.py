# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from Snoopy.WaveKinematic import VariableBathymetry

if __name__ == '__main__':

    plt.close('all')

    w1 = 0.132641157943925
    w2 = 7.391979999999999E-002

    h0 = 65.
    slope = 0.05
    hmax = 29.

    x0 = 0.0
    xmax = 720.0

    depthFunc = lambda x : -slope*(x-x0) + h0

    test = VariableBathymetry(x0=x0, xmax=xmax,
                              depthFunc = depthFunc,
                              wrps = np.array([w1]), wdiff= np.array([w1-w2]),
                              heading=np.array([0]),
                              nstep=100)

    test.plot_bathymetry()

    test.solve_first_order_problem()
    test.solve_second_order_problem()

    # compare integration constants
    a = test.a.reshape(-1)
    b = test.b.reshape(-1)
    c = test.c.reshape(-1)
    d = test.d.reshape(-1)

    path = r"C:\Users\couledhousseine\Documents\hstar_projects\QTF_variable_depth\code\variable_depth"

    fx = lambda x : np.loadtxt(path+'/'+x)[:,0] + 1j*np.loadtxt(path+'/'+x)[:,1]

    am = fx('fort.2021')
    bm = fx('fort.2022')
    cm = fx('fort.2023')
    dm = fx('fort.2024')

    fig,ax = plt.subplots()
    ax.plot(dm.real,label='Molin - real',marker='.')
    ax.plot(d.real,label='Present - real')
    ax.plot(dm.imag,label='Molin - imag',marker='.')
    ax.plot(d.imag,label='Present - imag')
    ax.legend()

    p = test.get_second_order_potential_mesh().reshape(-1)

    fig,ax = plt.subplots()
    ax.plot(test._x, p.real, label='real')
    ax.plot(test._x, p.imag,label='imag')
    ax.plot(test._x, np.abs(p),label='abs')
    ax.legend()

    npt = 10
    points  = np.zeros((npt,3))
    xpoints = np.linspace(713.16,719.64,npt)
    points[:,0] = xpoints

    R,alpha = test.get_correction_factor(points)

    R = R.reshape(-1)
    alpha = alpha.reshape(-1)










