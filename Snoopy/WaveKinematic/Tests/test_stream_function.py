import numpy as np
from matplotlib import pyplot as plt
import pytest
from Snoopy import WaveKinematic as wk
from Snoopy import Spectral as sp
from Snoopy.WaveKinematic.streamFunction import steepness_limit, t_to_l, l_to_t


@pytest.mark.parametrize("T, H, depth", [( 8., 10., 200),
                                         ( 8., 10., -1)
                                        ])
def test_basics(T , H , depth ):
    # Check that input height and period are correctly retrieved.
    sf = wk.StreamFunction( H = H , T = T , depth = depth)

    assert( sf.check() )
    assert( np.isclose( sf.getPeriod() , T) )

    sf_l = wk.StreamFunction( H = H , wavelength = sf.getWaveLength() , depth = depth)
    h_l = sf.getElevation( t = 0. , x = 0. ) - sf.getElevation( t = T/2. , x = 0. )

    assert( np.isclose( sf_l.getPeriod() , T) )
    assert( np.isclose( sf_l.getWaveLength() , sf.getWaveLength() ) )
    assert( np.isclose( sf_l.getElevation( t = T , x = 0. ) , sf_l.getElevation( t = T , x = 0. ) ) )
    assert( np.isclose( sf_l.getElevation( t = 0 , x = sf.getWaveLength() ) , sf_l.getElevation( t = 0 , x = 0. ) ) )
    assert( np.isclose(h_l , H) )
    


def test_froude( ):
    # Check that results does not depends on Froude scaling.
    st = 0.11
    depth_over_l = 1.
    eta = []
    ref_lengths = [ 1., 100, 500, 1000, ]
    for ref_length in ref_lengths: # Does not work for 10000.
        x = np.linspace(0.0 , ref_length , 200)
        depth = ref_length * depth_over_l
        sf = wk.StreamFunction( H = st * ref_length ,  wavelength = ref_length , depth = depth)
        eta.append( np.array([sf.getElevation( t = 0. , x = x_ ) / ref_length for x_ in x]) )
        
    for i in range(len(ref_lengths)-1) : 
        np.isclose( eta[i] , eta[-1] , atol = 1e-5, rtol = 1e-5).all()



# Dispersion
def test_non_linear_dispersion(display = False) : 
    # Check back and forth
    for kd in [ 0.1, 0.2 , 0.5, 1.5, 2.0]:
        st_lim = steepness_limit( kd )*1.1
        ref_length = 500.
        st_list = np.linspace(0. , st_lim , 10 )
        depth = kd * ref_length
        for st in st_list:
            h= st * ref_length
            t = l_to_t( ref_length , h , depth )
            back_calc = t_to_l( t,  h, depth ) 
            # print (kd , st, back_calc , t)
            assert( np.isclose(back_calc, ref_length, rtol = 0.001 ) )



def test_cn_stream( display = False ):
    """Compare Snoopy to cn-stream results (stored).
    """
    
    from Snoopy import Reader as rd
    N = 30
    for kd, st in ( [ np.pi*2 , 0.12 ] , ( 0.5 , 0.05 ) )[1:2] :
        file = f"{wk.TEST_DATA:}/cn_stream/st_{st:.2f}_kd_{kd:.2f}/FreeSurface_CN_Stream.dat"
        ref_data = rd.tecplot_HOS(file)
        length = 500.
        depth = kd / (2*np.pi / length)
        sf = wk.StreamFunction( H = st * length ,  wavelength = length , depth = depth, N = N)
        eta = sf.getElevation( t = 0.0 , x = ref_data.index.values )
        assert( np.isclose(eta , ref_data.eta.values, atol = st * length / 5000).all())
        
        if display : 
            fig, ax = plt.subplots()
            ax.plot( ref_data.index.values , ref_data.eta.values , label = "CN-STREAM")
            ax.plot( ref_data.index.values , eta , marker = "*", linestyle ="" , label = "Snoopy")
            ax.legend()
       
        


def compare_N( st=0.4, kd = 1. ):
    from Snoopy import PyplotTools as dplt
    fig, ax= plt.subplots()
    cmapa = dplt.getColorMappable(10, 30, cmap = "cividis")
    for N in [ 10, 20, 30] :
        length = 500.0
        depth = kd / (2*np.pi / length)
        x = np.linspace(-0.5*length , 0.5*length , 200)
        sf = wk.StreamFunction( H = st * length ,  wavelength = length , depth = depth, N = N)
        eta_t0 = np.array([sf.getElevation( t = 0. , x = x_ ) for x_ in x])
        ax.plot( x , eta_t0 / (st*length ) , color = cmapa.to_rgba(N) , label = f"N = {N:}")
    ax.legend()



def compare_raschii( kd = 1.0):
    # Compare Snoopy to Raschii
    import raschii
    from Snoopy import PyplotTools as dplt

    fig, ax= plt.subplots()
    fig_vx, ax_vx = plt.subplots()
    fig_vz, ax_vz = plt.subplots()
    StreamRaschii , _ = raschii.get_wave_model("Fenton")
    
    
    stList = np.linspace( 0.001, wk.streamFunction.steepness_limit( kd, variant ="numerical" ) , 5)

    cmapa = dplt.getColorMappable(0.00, stList[-1]*1.001, cmap = "cividis")
    
    N = 30

    z = np.linspace( -2 , 1.1, 100)
    z0 = np.linspace( -2 , 0.0, 100)
    
    for st in stList :
        length = 500.0
        depth = kd * length

        height = st * length
        
        x = np.linspace(-length / 2.0 , length/2. , 200)
        x2 = np.linspace(-length / 2.0 , length/2. , 50)

        # Infintie depth crest linear particle velocity
        vlin = 0.5 * height * 2*np.pi / sp.l2t(length)
        try : 
            sf = wk.StreamFunction( H = height , wavelength = length, useLength = True, depth = depth,  N = N, damp = 0.2, maxIter = 5000, tol = 1e-14)
            eta_t0 = np.array([sf.getElevation( t = 0. , x = x_ ) for x_ in x])
            ax.plot( x , eta_t0 / (st*length ) , color = cmapa.to_rgba(st) , label = f"Steepness = {st:}")

            # Compare velocity profile
            a = sf.getElevation(0.,0.)
            
            vx = np.array([sf.getVelocity( x = 0. , z = z_ * a, t = 0.  )[0] for z_ in z])
            ax_vx.plot(vx / vlin , z ,  color = cmapa.to_rgba(st) , label = f"Steepness = {st:}")
            
            vz = np.array([sf.getVelocity( x = 0.0 , z = z_ * a, t = sf.getPeriod() / 4  )[2] for z_ in z0])
            ax_vz.plot(vz / vlin , z0 ,  color = cmapa.to_rgba(st) , label = f"Steepness = {st:}")
        except RuntimeError: 
            print(f"Snoopy StreamFunction not converging for {st:}")

        try : 
            # Compare velocity profile
            a = sf.getElevation(0.,0.)

            sf_rach = StreamRaschii(height = height, depth = depth , length = length , N = N)
            eta_t0_rash = sf_rach.surface_elevation(x2) - depth
            ax.plot( x2 , eta_t0_rash / (st*length ) , color = cmapa.to_rgba(st) , linestyle = "" , marker = "*")

            vx_rasch = np.array([ sf_rach.velocity( 0.0 , z_*a + depth , 0.0)[0][0] for z_ in z] )
            ax_vx.plot(vx_rasch / vlin , z  , color = cmapa.to_rgba(st) , linestyle = "" , marker = "*")
            
            vz_rasch = np.array([ sf_rach.velocity( 0.0 , z_*a + depth , -sf_rach.T / 4.)[0][1] for z_ in z0] )
            ax_vz.plot(vz_rasch / vlin , z0  , color = cmapa.to_rgba(st) , linestyle = "" , marker = "*")
        except RuntimeError: 
            print(f"Raschii not converging for {st:}")
            
    ax.legend()
    ax.set(xlabel = r"$x/\lambda$" , ylabel = r"$\eta/H$")
    ax_vx.legend()
    ax_vx.set(xlabel = "$Vx / Vx_{lin}^{inf}$" , ylabel = "z / H" )
    
    ax_vz.legend()
    ax_vz.set(xlabel = "$Vz / Vz_{lin}^{inf}$" , ylabel = "z / H" )


def find_numerical_limits():
    ref_length = 500.
    kd_list = np.arange(0.1, 2.0 , 0.05)
    st_lim = []
    for kd in  kd_list:
        depth = kd / (2*np.pi / ref_length)
        st_list = np.linspace( 0.0001, steepness_limit(kd = kd , variant = "Miche") , 200 )
        for ist, st in enumerate(st_list) :
            height = st * ref_length
            try : 
                sf = wk.StreamFunction( H = height, wavelength=ref_length , depth = depth, maxIter = 5000 )
            except: 
                st_lim.append( st_list[ist-1] )
                print ("crash")
                break
            
            if not sf.check():
                ax = sf.plot_x()
                st_lim.append( st_list[ist-1] )
                break
    print (kd_list)
    print (st_lim)
    fig, ax = plt.subplots()
    ax.plot(kd_list , st_lim, "+", label = "Snoopy limits")
    ax.plot(kd_list , [ wk.streamFunction.steepness_limit(kd_) for kd_ in kd_list], "-", label = "Miche")
    
            

if __name__ == "__main__":
    print ("Run")
    test_basics( T = 8.0 , H = 12. , depth = 200 )
    test_basics( T = 8.0 , H = 10. , depth = -1 )
    test_froude()
    test_non_linear_dispersion()
    test_cn_stream(True)
    
    # find_numerical_limits()
    # compare_raschii(kd = 0.02)
    # compare_N( st = 0.04 , kd = 0.4 )
