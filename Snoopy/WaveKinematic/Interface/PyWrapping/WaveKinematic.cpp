#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <pybind11/functional.h>

#include "Spectral/Wif.hpp"
#include "Spectral/Rao.hpp"
#include "Spectral/Qtf0.hpp"
#include "Spectral/MQtf.hpp"

// WaveKinematic related headers
#include "WaveKinematic/WaveKinematicABC.hpp"
#include "WaveKinematic/FirstOrderKinematic.hpp"
#include "WaveKinematic/SecondOrderKinematic21.hpp"
#include "WaveKinematic/SecondOrderKinematic.hpp"
#include "WaveKinematic/Wheeler1st.hpp"
#include "WaveKinematic/DeltaStretcher.hpp"
#include "WaveKinematic/Wheeler2nd.hpp"
#include "WaveKinematic/StreamFunction.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;
using namespace BV::WaveKinematic;

class PyWaveKinematicABC : public WaveKinematicABC
{
public:
    using WaveKinematicABC::WaveKinematicABC;

    Eigen::Vector3d getVelocity(double time, double x, double y, double z) override {
        PYBIND11_OVERLOAD_PURE(
            Eigen::Vector3d,        /* Return type */
            WaveKinematicABC,       /* Parent class */
            getVelocity,            /* Name of function in C++ (must match Python name) */
            time, x, y, z           /* Argument(s) */
        );
    }

    Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override {
        PYBIND11_OVERLOAD_PURE(
            Eigen::Vector3d,        /* Return type */
            WaveKinematicABC,       /* Parent class */
            getAcceleration,        /* Name of function in C++ (must match Python name) */
            time, x, y, z           /* Argument(s) */
        );
    }

    double getPressure(double time, double x, double y, double z) override {
        PYBIND11_OVERLOAD(
            double,
            WaveKinematicABC,
            getPressure,
            time, x, y, z
        );
    }

    double getElevation(double time, double x, double y, double speed = 0., bool update = true) override {
        PYBIND11_OVERLOAD(
            double,
            WaveKinematicABC,
            getElevation,
            time, x, y, speed, update
        );
    }
};

PYBIND11_MODULE(_WaveKinematic, m)
{
    m.doc() = "Wave Kinematic module";

    py::module::import("_Spectral") ; // For enums to be accessible

	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    py::class_<WaveKinematicABC, std::shared_ptr<WaveKinematicABC>, PyWaveKinematicABC> waveKinematicABC(m, "WaveKinematicABC");
    waveKinematicABC
        .def(py::init<std::shared_ptr<Wif>, unsigned int>(), py::keep_alive<1, 2>(), "wif"_a, "numThreads"_a = 1)
        .def(py::init<std::shared_ptr<Wifm>, unsigned int>(), py::keep_alive<1, 2>(), "wifm"_a, "numThreads"_a = 1)

        .def("getElevation", py::vectorize(&WaveKinematicABC::getElevation), "time"_a, "x"_a, "y"_a, "speed"_a = 0., "update"_a = true,
            R"rst(
            Return wave velocity at a fixed position, as pandas DataFrame (time, vx, vy, vz).

            Parameters
            ----------
            time : float or array like
                Time
            x : float
                x coordinates of the point.
            y : float or array like
                y coordinates of the point.
            z : float or array like
                z coordinates of the point.
            speed : float, optional
                Speed.The default is 0.0.

            Returns
            -------
            float or array like
                Wave kinematic

            Example
            -------
            vel = kin.getElevation( time = np.arange(0., 10800, 0.5), 0.0 , 0.0 , 0.0)  

            )rst")
        
        .def("getVelocity", &WaveKinematicABC::getVelocity, "time"_a, "x"_a, "y"_a,  "z"_a)
        .def("getAcceleration", &WaveKinematicABC::getAcceleration, "time"_a, "x"_a, "y"_a, "z"_a)
        .def("getPressure", py::vectorize(&WaveKinematicABC::getPressure), "time"_a, "x"_a, "y"_a, "z"_a)
        .def("getPressure1", &WaveKinematicABC::getPressure1, "getPressure1 returns the latest calculated first order pressure")
        .def("getElevation_2D", &WaveKinematicABC::getElevation_2D, "Return elevation along time and x,y position", "timeVect"_a, "xVect"_a, "yVect"_a, "speed"_a = 0., pybind11::call_guard<pybind11::gil_scoped_release>())

        //.def("getElevation_omp", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, double>(&WaveKinematicABC::getElevation_omp), "Return elevation along time and x,y position", "timeVect"_a, "xVect"_a, "yVect"_a, "speed"_a = 0., pybind11::call_guard<pybind11::gil_scoped_release>())

        .def("getElevation1", py::overload_cast<>(&WaveKinematicABC::getElevation1), "getElevation1 returns the latest calculated first order free surface elevation")
        .def("getDepth", &WaveKinematicABC::getDepth, "Return water depth considered")
        .def("setIgnoreFreeSurface", &WaveKinematicABC::setIgnoreFreeSurface, "If value is true (default), the potential, velocity, acceleration and pressure are calculated before z <= 0, otherwise they are calculated for z <= \\eta_1");

    py::class_<FirstOrderKinematic, std::shared_ptr<FirstOrderKinematic> >(m, "FirstOrderKinematic", waveKinematicABC, "Calculates the first order velocities, pressure and free surface elevation")
        .def(py::init<std::shared_ptr<Wif>, unsigned int>(),
             py::keep_alive<1, 2>(), "wif"_a, "numThreads"_a = 1)
        .def(py::init<std::shared_ptr<Wifm>, unsigned int>(),
             py::keep_alive<1, 2>(), "wifm"_a, "numThreads"_a = 1);
    py::class_<Wheeler1st, std::shared_ptr<Wheeler1st>>(m, "Wheeler1st", waveKinematicABC,
            R"rst(
            Calculates the first order velocities with Wheeler's stretching rule

            Parameters
            ----------
            wif or wifm : Snoopy.Spectral.Wif or list(Snoopy.Spectral.Wif)
                waves information data object
            inCrestOnly : boolean (default False)
                if inCrestOnly == True then the stretching is performed ONLY when the free surface elevation
                    at this point larger than the mean free surface elevation (eta = 0)
                if inCrestOnly == False then the stretching is done for any free surface elevation
            numThreads : int (default 1)
                number of threads used for parallel calculations

            Example
            -------
            kin1 = Snoopy.WaveKinematic.Wheeler1st(wif)         # inCrestOnly == False
            kin2 = Snoopy.WaveKinematic.Wheeler1st(wif, True)   # inCrestOnly == True

            )rst")
        .def(py::init<std::shared_ptr<Wif>, bool, unsigned int>(),
             py::keep_alive<1, 2>(), "wif"_a, "inCrestOnly"_a = false, "numThreads"_a = 1)
        .def(py::init<std::shared_ptr<Wifm>, bool, unsigned int>(),
             py::keep_alive<1, 2>(), "wifm"_a, "inCrestOnly"_a = false, "numThreads"_a = 1)
        .def("inCrestOnly", &Wheeler1st::inCrestOnly,
            R"rst(
            Defines whether the wheeler stretching is done for the crests only or for the both crests and troughs

            Parameters
            ----------
            isInCrestOnly : boolean (default = True)
                if it is True, then the stretching is performed under the crest only,
                otherwise everywhere

                This parameter is equivalent to that for the constructor

            Example
            -------
            kin.inCrestOnly()       # == kin.inCrestOnly(True)
            kin.inCrestOnly(True)
            kin.inCrestOnly(False)
            )rst",
            "isInCrestOnly"_a = true)
        .def("isInCrestOnly", &Wheeler1st::isInCrestOnly,
            R"rst(
            Returns True if the stretching is done only in the crest, and False if it is done for any case

            Returns
            -------
            boolean
                the value of inCrestOnly

            Example
            -------
            print(kin.isInCrestOnly())
            )rst");

    py::class_<DeltaStretching, std::shared_ptr<DeltaStretching>>(m, "DeltaStretching", waveKinematicABC, 
            R"rst(
            Calculates the first order velocities with Delta-stretching rule

            Parameters
            ----------
            wif or wifm : Snoopy.Spectral.Wif or list(Snoopy.Spectral.Wif)
                waves information data object
            delta : double
                delta value to calculate the stretched z coordinate
            hDelta : double (default -1.)
                h_delta value which indicates the starting depth from which the stretching is applied
                if hDelta == -1. then hDelta = depth.
            inCrestOnly : boolean (default False)
                if inCrestOnly == True then the stretching is performed ONLY when the free surface elevation
                    at this point larger than the mean free surface elevation (eta = 0)
                if inCrestOnly == False then the stretching is done for any free surface elevation
            numThreads : int (default 1)
                number of threads used for parallel calculations

            Example
            -------
            kin1 = Snoopy.WaveKinematic.DeltaStretching(wif, delta = 0.0)                       # equal to Wheeler1st(wif)
            kin2 = Snoopy.WaveKinematic.DeltaStretching(wif, delta = 0.0, inCrestOnly = True)   # equal to Wheeler1st(wif, inCrestOnly = True)

            )rst")
        .def(py::init<std::shared_ptr<Wif>, double, double, bool, unsigned int>(),
            py::keep_alive<1, 2>(), "wif"_a, "delta"_a, "hDelta"_a = -1, "inCrestOnly"_a = false, "numThreads"_a = 1)
        .def(py::init<std::shared_ptr<Wifm>, double, double, bool, unsigned int>(),
            py::keep_alive<1, 2>(), "wifm"_a, "delta"_a, "hDelta"_a = -1, "inCrestOnly"_a = false, "numThreads"_a = 1)
        .def("getDepthDelta", &DeltaStretching::getDepthDelta,
            R"rst(
            Returns the h_delta value

            Returns
            -------
            double
                h_delta value

            Examples:
            hd = kin.getDepthDelta()
            )rst")
        .def("setDepthDelta", &DeltaStretching::setDepthDelta,
            R"rst(
            Set h_delta value

            Parameters
            ----------
            hDelta : double
                if depth > 0 (finite depth case), then hDelta must be non-negative value
                if depth <= 0 (infinite depth case), then hDetla = -1 corresponds to the infinite h_delta (same as depth)
                                                          or it can be non-negative value

            Examples:
            kin.setDepthDelta(hDelta = 10.)
            )rst",
            "hDelta"_a)
        .def("getDelta", &DeltaStretching::getDelta,
            R"rst(
            Returns the delta value

            Returns
            -------
            double
                delta value

            Examples:
            delta = kin.getDelta()
            )rst")
        .def("setDelta", &DeltaStretching::setDelta,
            R"rst(
            Set delta value

            Parameters
            ----------
            delta : double
                The coefficient from 0 to 1.
                The interval of z values [-h_delta,eta] is mapped (stretched) to [-h_delta, delta *eta]

            Examples:
            kin.setDelta(delta = 0.0)   # Wheeler1st model if h_Delta == depth
            kin.setDelta(delta = 1.0)   # pure linear extrapolation if h_Delta == depth
            )rst",
            "delta"_a)
        .def("isInCrestOnly", &DeltaStretching::isInCrestOnly,
            R"rst(
            Returns True if the stretching is done only in the crest, and False if it is done for any case

            Returns
            -------
            boolean
                the value of inCrestOnly

            Example
            -------
            print(kin.isInCrestOnly())
            )rst")
        .def("inCrestOnly", &DeltaStretching::inCrestOnly,
            R"rst(
            Defines whether the wheeler stretching is done for the crests only or for the both crests and troughs

            Parameters
            ----------
            isInCrestOnly : boolean (default = True)
                if it is True, then the stretching is performed under the crest only,
                otherwise everywhere

                This parameter is equivalent to that for the constructor

            Example
            -------
            kin.inCrestOnly()       # == kin.inCrestOnly(True)
            kin.inCrestOnly(True)
            kin.inCrestOnly(False)
            )rst",
            "isInCrestOnly"_a = true);

    py::class_<SecondOrderKinematic21, std::shared_ptr<SecondOrderKinematic21>, WaveKinematicABC>(m, "SecondOrderKinematic21", "Calculates the second order velocities, pressure and free surface elevation, based on the first order quanitites")
        .def(py::init<std::shared_ptr<Wif>, unsigned int>(),
            py::keep_alive<1, 2>(), "wif"_a, "numThreads"_a = 1)
        .def(py::init<std::shared_ptr<Wifm>, unsigned int>(),
             py::keep_alive<1, 2>(), "wifm"_a, "numThreads"_a = 1)
        .def("getElevation21", py::overload_cast<>(&SecondOrderKinematic21::getElevation21),
                "getElevation returns the total free surface elevation (the sum of the both first and second order elevations.  To get only the quadratic part of the second order component call this method")
        .def("getPressure21", py::overload_cast<>(&SecondOrderKinematic21::getPressure21),
                "getPressure returns the sum of the both first and second order pressures.  The quadratic part of the second order pressure, thus is calculated. In order to not recalculate it, use this method");

	py::class_<SecondOrderKinematic, std::shared_ptr<SecondOrderKinematic>, SecondOrderKinematic21>(m, "SecondOrderKinematic", "Calculates the second order velocities, pressure and free surface elevation")
		.def(py::init<std::shared_ptr<Wif>, unsigned int>(),
			py::keep_alive<1, 2>(), "wif"_a, "numThreads"_a = 1)
		.def(py::init<std::shared_ptr<Wifm>, unsigned int>(),
			py::keep_alive<1, 2>(), "wifm"_a, "numThreads"_a = 1)
		.def("getElevation22", py::overload_cast<>(&SecondOrderKinematic::getElevation22), "Returns eta_22 percalculated by the getElevation method")
		.def("getPressure22", py::overload_cast<>(&SecondOrderKinematic::getPressure22), "Returns p_22 precalculated by the getPressure method")
		.def("getQi", &SecondOrderKinematic::getQi, "Returns Q_i/(gk^- tanh(k^-h) -(w_1 \\mp w_2)^2)")
		.def("getWi", &SecondOrderKinematic::getWi, "Returns w_1 \\mp w_2")
		.def("getKi", &SecondOrderKinematic::getKi, "Returns k_1 \\mp k_2")
		.def("getKix", &SecondOrderKinematic::getKix, "Returns k_1 \\cos\beta_1 \\mp k_2 \\cos\\beta_2")
		.def("getKiy", &SecondOrderKinematic::getKiy, "Returns k_1 \\sin\\beta_1 \\mp k_2 \\sin\\beta_2");

    py::class_<Wheeler2nd, std::shared_ptr<Wheeler2nd>, SecondOrderKinematic>(m, "Wheeler2nd", "Similar to Wheeler1st, but uses the second order free surface elevation for the stretching")
        .def(py::init<std::shared_ptr<Wif>, unsigned int>(),
             py::keep_alive<1, 2>(), "wif"_a, "numThreads"_a = 1)
        .def(py::init<std::shared_ptr<Wifm>, unsigned int>(),
             py::keep_alive<1, 2>(), "wifm"_a, "numThreads"_a = 1);

    py::class_<StreamFunction>(m, "StreamFunction")
        .def(py::init<double, double, double, double, bool, double, bool, int, int, double, double, double>(),
            "depth"_a, "H"_a, "T"_a = 1., "wavelength"_a = 0., "useLength"_a = false, "cEcS"_a = 0., "useEulerCurrent"_a = true, "N"_a = 30,
            "maxIter"_a = 1000, "tol"_a = 1.e-14, "damp"_a = 0.3, "g"_a = 9.81,
        R"rst(
        Stream Function implementation. Based on the implementation of the Stream Function in FoamStar 2

        Parameters
        ----------
        depth : double
            depth of the fluid. Negative or zero values correspond to the infinite depth case.

        H : double
            wave height.

        T : double, optional
            wave period.
            Either T or wavelength must be provided depending on useLength. See useLength

        wavelength : double, optional
            wave length.
            Either T or wavelength must be provided depending on useLength. See useLength

        useLength : bool, False
            by default useLength is False, which means that the calculation will be done by using wave period T.
            If it is True, then the wave length is used to start the calculation. In this case the wavelength parameter must be given.

        cEcS : double
            depending on useEulerCurrent value it is the mean value of Euler or Stokes currents

        useEulerCurrent : bool, True
            if useEulerCurrent is True, then cEcS corresponds to the mean value of Euler current. If it is False, then cEcS is that for Stokes.

        N : int, 30
            order of the wave

        maxIter : int, 1000
            number of maximum iteration to be used to resolve the problem. Note, that this value cannot be less than 10.

        tol : double
            tolerance value.

        damp : double, 0.3
            Relaxation  for numerical solver.

        g : double, 9.81
            gravity acceleration
            )rst")


            .def("getElevation", py::vectorize(&StreamFunction::getElevation), "t"_a, "x"_a, R"rst(
        get the Free surface elevation

        Parameters
        ----------
        t : double
            time instance

        x : double
            x coordinate

        return : double
            the free surface elevation at time t at position x
        )rst")

    .def("getVelocity", &StreamFunction::getVelocity, "t"_a, "x"_a, "z"_a, R"rst(
        get the velocities

        Parameters
        ----------
        t : double
            time instance

        x : double
            x coordinate

        z : double
            z coordinate (vertical)

        return : numpy array of size 3
            the velocity vector at time t and position (x, z)
        )rst")

    .def("getFrequency", &StreamFunction::getFrequency, R"rst(
        get the wave frequency
        )rst")

    .def("getPeriod", &StreamFunction::getPeriod, R"rst(
        get the wave period
        )rst")

    .def("getWaveNumber", &StreamFunction::getWaveNumber, R"rst(
        get the wavenumber
        )rst")

    .def("getWaveLength", &StreamFunction::getWaveLength, R"rst(
        get the wavelength
        )rst")

    .def("getWaveHeight", &StreamFunction::getWaveHeight, R"rst(
        get the WaveHeight
        )rst")

    .def("getDepth", &StreamFunction::getDepth, R"rst(
        get the depth
        )rst");
}
