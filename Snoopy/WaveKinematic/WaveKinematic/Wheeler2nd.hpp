#pragma once

#include "WaveKinematicExport.hpp"
#include "Spectral/Wif.hpp"
#include "WaveKinematic/Wheeler.hpp"
#include "WaveKinematic/SecondOrderKinematic.hpp"

namespace BV
{
namespace WaveKinematic
{

class WAVE_KINEMATIC_API Wheeler2nd : public SecondOrderKinematic
{
private:
    /*!
     * \brief wheeler_ : describes the stretching rule. Because it is common for the 1st and 2nd order, it is put in separate class
     */
    Wheeler wheeler_ ;
public:
    Wheeler2nd(std::shared_ptr<Spectral::Wif> p_wif, unsigned int numThreads = 1)  :
        SecondOrderKinematic(p_wif, numThreads)
    {
        /*
         * In SecondOrderKinematic, the phase is calculated at (x = 0, y = 0, t = 0)
         *                          the amplitudes for az are calculated in z = 0.
         */
        isWheeler_ = true ;
        getElevation(time_, x_, y_);    // calculates the elevation and saves it to eta1Saved_, eta21Saved_ and eta22Saved_
        wheeler_.setDepth(depth_) ;
    }

    Wheeler2nd(std::shared_ptr<Spectral::Wifm> p_wifm, unsigned int numThreads = 1)  :
        SecondOrderKinematic(p_wifm, numThreads)
    {
        /*
         * In SecondOrderKinematic, the phase is calculated at (x = 0, y = 0, t = 0)
         *                          the amplitudes for az are calculated in z = 0.
         */
        isWheeler_ = true ;
        getElevation(time_, x_, y_);    // calculates the elevation and saves it to eta1Saved_, eta21Saved_ and eta22Saved_
        wheeler_.setDepth(depth_) ;
    }

    ~Wheeler2nd()
    {

    }

    using SecondOrderKinematic::getVelocity ;
    using SecondOrderKinematic::getPressure ;
    using SecondOrderKinematic::getAcceleration;

    Eigen::Vector3d getVelocity(double time, double x, double y, double z) override;
    Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override;
};

}
}
