#pragma once

#include "WaveKinematicExport.hpp"

namespace BV
{
namespace WaveKinematic
{

/*!
 * \brief The Wheeler class is used for definition of the stretching function
 */
class WAVE_KINEMATIC_API Wheeler
{
private:
    /*!
     * \brief depth_ : for the finite case depth_ > 0.0
     */
    double depth_ ;
    /*!
     * \brief inCrestOnly_ : stretch only if eta > 0
     */
    bool inCrestOnly_;
public:
    Wheeler() :
        depth_(0.0), inCrestOnly_(false)
    {

    }
    Wheeler(double depth) :
        depth_(depth), inCrestOnly_(false)
    {

    }
    ~Wheeler()
    {

    }
    /*!
     * \brief replace the z by z' according to the Wheeler's stretching rule
     * \param[in] z : z coordinate
     * \param[in] eta : free surface elevation
     * \return (z-\eta)/(1 +\eta/depth) for the finite depth case and z-depth for the infinite one
     */
    double stretcher(double z, double eta);

    void setDepth(double depth)
    {
        depth_ = depth ;
    }
    void setInCrestOnly(bool inCrestOnly)
    {
        inCrestOnly_ = inCrestOnly;
    }
    bool isInCrestOnly() const {return inCrestOnly_;}
};

}
}
