#include "WaveKinematic/StreamFunction.hpp"

using namespace BV::WaveKinematic;

void StreamFunction::updatePhase(double t, double x, bool force)
{
    if (force || isPointDifferent(t, x))
    {
        time_ = t;
        x_    = x;
        phase_ = getPhase(time_, x_);
    }
}

Eigen::Vector3d StreamFunction::getVelocity(double time, double x, double z)
{
    // if we calculate the velocities only under the free surface => we need to calculate the free surface elevation
    if (!ignoreFreeSurface_)
    {
        getElevation(time, x);
        // z > eta ?
        if ( z-eta_ >= 1.e-8)
        {
            return Eigen::Vector3d(0.0, 0.0, 0.0);
        }
    }

    // if ignore the free surface elevation => calculate velocity
    // otherwise check, that z is not larger than the free surface with a numerical error: z < eta +1.e-8
    // calculate the velocity
    double Uh(jB_[0]);
    double Uv(0.);
    double expkZ(exp(k_ *z));

    updatePhase(time, x);

    if (depth_ < 1.e-10)    // deep water
    {
        // temporary
        double expjkZ(1);
        for (int j=1; j<jB_.size(); j++)
        {
            expjkZ *= expkZ;
            Uh += jB_[j]*expjkZ*cos(j*phase_);
            Uv += jB_[j]*expjkZ*sin(j*phase_);
        }
    }
    else
    {
        const double expkZh = exp(-2*k_*z) * expm2kh_;
        // temporary
        double expjkZ(1);
        double expjkh(1);
        double expjkZh(1);
        for (int j=1; j<jB_.size(); j++)
        {
            expjkZ *= expkZ;
            expjkh *= expm2kh_;
            expjkZh *= expkZh;
            const double exp0 = expjkZ/(1+expjkh);
            const double coshTerm(exp0*(1+expjkZh));
            const double sinhTerm(exp0*(1-expjkZh));
            Uh += jB_[j]*coshTerm*cos(j*phase_);
            Uv += jB_[j]*sinhTerm*sin(j*phase_);
        }
    }
    return Eigen::Vector3d(Uh, 0., Uv);
}

double StreamFunction::getElevation(double time, double x)
{
    updatePhase(time, x);
    eta_ = 0.0;
    for (int j = 1; j < A_.size(); j++) { eta_ += A_(j)*cos(j*phase_); }

    return eta_;
}

