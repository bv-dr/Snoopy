#pragma once

#include "WaveKinematicExport.hpp"

#include <Eigen/Dense>
#include <iostream>

#include "WaveKinematic/foamStar/streamFunctionWaveCoeffs.H"

namespace BV
{
namespace WaveKinematic
{

/**
 * @class StreamFunction
 *
 * @brief calculate the free surface elevation and velocity field based on the stream function theory
 * Class is based on the foamStar stream function implementation
 */

class WAVE_KINEMATIC_API StreamFunction
{
    protected:
        double T_;
        double omega_;
        double wavelength_;
        double k_;
        double depth_;
        double wave_height_;
        bool useL_;
        double cEcS_;
        bool useCE_;
        int N_;
        int maxIter_;
        double tol_;
        double damp_;
        double gMag_;

        // A_ : Amplitudes of stream function harmonics
        Eigen::VectorXd A_;
        // B_ : Amplitudes for (u,v,w)
        Eigen::VectorXd jB_;

        bool ignoreFreeSurface_;

        // cached value
        double eta_;
        double expm2kh_;
        double time_;
        double x_;
        double phase_;

        virtual void updatePhase(double t, double x, bool force = false);
        virtual double getPhase(double t, double x) { return t *omega_ -k_ *x; }
        /**
         * Return true if the time (t) is different from the previous calculation
         * @param t : time instant t
         */
        virtual bool isTimeDifferent(double t, double tol = 1.e-10) const { return std::abs(time_-t) > tol; }
        /**
         * Return true if either time (t) or horizontal position (x,y) is different from the previous calculation
         * @param t : time instant t
         * @param x : x coordinate
         */
        virtual bool isPointDifferent(double t, double x, double tol = 1.e-10) const { return std::abs(time_-t) > tol || std::abs(x_-x) > tol; }

    private:
    public:
        StreamFunction(
            const double depth,             // infinite depth <= 0
            const double H,                 // wave height
            const double T,                 // wave period
            const double wavelength = 1.,   // wave length
            //const double omega,           // wave radian freq.
            //const double k = 0,           // wave number
            const bool useL = false,        // use L or T?
            const double cEcS = 0,          // value of mean current or drift
            const bool useCE = true,        // use Euler Current or not?
            const int N = 30,               // order
            const int maxIter = 1000,       // mininum 10
            const double tol = 1.e-14,
            const double damp = 0.3,
            const double gMag = 9.81) :
            T_(T), omega_(T < 1.e-10 ? 1. : 2.*M_PI/T), wavelength_(wavelength), k_(wavelength < 1.e-10 ? 1. : 2.*M_PI/wavelength), depth_(depth), wave_height_(H),
            useL_(useL), cEcS_(cEcS), useCE_(useCE), N_(N), maxIter_(maxIter),
            tol_(tol), damp_(damp), gMag_(gMag),
            ignoreFreeSurface_(true),
            eta_(0.0), expm2kh_(0.0), time_(-1000.), x_(0.), phase_(0.)
        {
            bv::waves::streamFunctionWaveCoeffs sfwc(
                    depth_,             // infinite depth <= 0
                    wave_height_,       // wave height
                    omega_,             // wave frequency in radians
                    k_,                 // wave number (used as input value if useL is true)
                    useL_,              // if true then the wave length is used as input otherwise the period is used
                    cEcS_,              // value of mean current or drift
                    useCE_,             // if true, cE (Euler Current) is used, otherwise, cS (Stoks) is used
                    N_,                 // order
                    maxIter_,           // number of iterations. Minimum 10
                    tol_,
                    damp_,
                    gMag_,              // acceleration due to the gravity
                    bv::dSMALL*1e3
                    );
            if (useL_)                  // wave length (wavenumber k) was used => frequency was calculated
            {
                omega_ = sfwc.omega_;
                if (omega_ < 1.e-10) T_ = 1.e90; else T_ = 2.*M_PI/omega_;
            }
            else                        // period (frequency omega) was used => wavenumber was calculated
            {
                k_ = sfwc.k_;
                if (k_ < 1.e-10) wavelength_ = 1.e90; else wavelength_ = 2.*M_PI/k_;
            }
            expm2kh_ = exp(-2. *k_ *depth_);
            sfwc.getA(A_);
            sfwc.getB(jB_);
            for (int j=1; j < jB_.size(); j++) { jB_[j] *= j; }
        }

        StreamFunction(const StreamFunction & other)
        {
            omega_          = other.omega_;
            T_              = other.T_;
            k_              = other.k_;
            wavelength_     = other.wavelength_;
            depth_          = other.depth_;
            wave_height_    = other.wave_height_;
            useL_           = other.useL_;
            cEcS_           = other.cEcS_;
            useCE_          = other.useCE_;
            N_              = other.N_;
            maxIter_        = other.maxIter_;
            tol_            = other.tol_;
            damp_           = other.damp_;
            gMag_           = other.gMag_;
            A_              = other.A_;
            jB_             = other.jB_;
            ignoreFreeSurface_ = other.ignoreFreeSurface_;
            eta_            = other.eta_;
            expm2kh_        = other.expm2kh_;
            time_           = other.time_;
            x_              = other.x_;
            phase_          = other.phase_;
        }

        virtual ~StreamFunction() {}

        virtual bool setIgnoreFreeSurface(bool ignoreFreeSurface) { bool oldValue = ignoreFreeSurface_; ignoreFreeSurface_ = ignoreFreeSurface; return oldValue; }

        virtual double getPhase() const { return phase_; }
        virtual double getWaveNumber() const { return k_; }
        virtual double getFrequency() const { return omega_; }
        virtual double getPeriod() const { return T_; }
        virtual double getWaveLength() const { return wavelength_; }
        virtual double getWaveHeight() const { return wave_height_; }
        virtual double getDepth() const { return depth_; }

        virtual Eigen::Vector3d getVelocity(double time, double x, double z);
        virtual double getElevation(double time, double x);
};

}
}
