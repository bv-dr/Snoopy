#include "WaveKinematic/SecondOrderKinematic21.hpp"

using namespace BV::WaveKinematic;

Eigen::Vector3d SecondOrderKinematic21::getVelocity_dz_at0(double time, double x, double y)
{
    /*
     * for finite depth:
     * \phi_{xz}(0) = \sum_n a_n k_n \tanh (k_n h) \frac{k_n g\cos\beta_n}{\omega_n} \cos\phase_n
     *              = \sum_n a_n \omega_n k_n\cos\beta_n \cos\phase_n,
     *      because g k_n \tanh k_n h = \omega_n^2
     * for infinit depth:
     * \phi_{xz}(0) = \sum_n a_n k_n               \frac{k_n g\cos\beta_n}{\omega_n} \cos\phase_n
     *              = \sum_n a_n \omega_n k_n\cos\beta_n \cos\phase_n
     *      because g k_n             = \omega_n^2
     */
    updatePhase(time, x, y) ;
    auto nbwaves = p_wif_->getAmplitudes().size();
    auto a = p_wif_->getAmplitudes();
    auto w = p_wif_->getFrequencies();
    double uxz = 0.0;
    double uyz = 0.0;
    for(auto i = 0; i < nbwaves; ++i) {
        auto factor = a(i) *w(i) *cphase_(i);
        uxz += factor *kchead_(i) ;
        uyz += factor *kshead_(i) ;
    }
    return Eigen::Vector3d(uxz, uyz, 0.0);
}
Eigen::Vector3d SecondOrderKinematic21::getAcceleration_dz_at0(double time, double x, double y)
{
    /*
     * for finite depth:
     * \phi_{xz}(0) = \sum_n a_n k_n \tanh (k_n h) \frac{k_n g\cos\beta_n}{\omega_n} \cos\phase_n
     *              = \sum_n a_n \omega_n k_n\cos\beta_n \cos\phase_n,
     *      because g k_n \tanh k_n h = \omega_n^2
     * for infinit depth:
     * \phi_{xz}(0) = \sum_n a_n k_n               \frac{k_n g\cos\beta_n}{\omega_n} \cos\phase_n
     *              = \sum_n a_n \omega_n k_n\cos\beta_n \cos\phase_n
     *      because g k_n             = \omega_n^2
     */
    updatePhase(time, x, y) ;
    auto nbwaves = p_wif_->getAmplitudes().size();
    auto a = p_wif_->getAmplitudes();
    auto w = p_wif_->getFrequencies();
    double uxz = 0.0;
    double uyz = 0.0;
    for(auto i = 0; i < nbwaves; ++i) {
        auto factor = -a(i) *w(i) *w(i) *sphase_(i);
        uxz += factor *kchead_(i) ;
        uyz += factor *kshead_(i) ;
    }
    return Eigen::Vector3d(uxz, uyz, 0.0);
}

double SecondOrderKinematic21::get2ndPressure(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return 0.0;
    }
    // p^{(21)}. eq. (27) in "Incident_Second_Order_eqs.pdf"
    Eigen::Vector3d vel = get1stVelocity(time, x, y, z, 0.0, false) ;  // velocity vector: \nabla \Phi^{(1)}
    p21Saved_ = (-0.5 /g_ * vel.transpose()) *vel;		// pressure is defined as P/rho/g
    return p21Saved_;
}

double SecondOrderKinematic21::get2ndElevation(double time, double x, double y)
{
    if (!isEPointDifferent(time, x, y)) return eta21Saved_;
    // \eta^{(21)}. eq. (33) in "Incident_Second_Order_eqs.pdf"
    eta1Saved_ = get1stElevation(time, x, y) ;
    auto phi_zt = get1stPhiztAt0(time, x, y) ;
    auto vel = get1stVelocity(time, x, y, 0.0) ;    // useConstrain == false, because we need \nabla\phi at z = 0
    eta21Saved_ = (-eta1Saved_ *phi_zt -((0.5*vel.transpose()) *vel))/g_;
    saveEPoint(time, x, y);
    return eta21Saved_;
}

Eigen::Vector3d SecondOrderKinematic21::get21Velocity(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)   // above the free surface, the velocity is zero
            return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
    if (z <= 0.0)   // below the mean free surface, the velocity is of the first order
        return get1stVelocity(time, x, y, z, eta, useConstrain) ;
    // 0 < z <= eta => stretching
    return get1stVelocity(time, x, y, 0.0, 0.0, false) +getVelocity_dz_at0(time, x, y) *z;

}
Eigen::Vector3d SecondOrderKinematic21::get21Acceleration(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)   // above the free surface, the velocity is zero
            return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
    if (z <= 0.0)   // below the mean free surface, the velocity is of the first order
        return get1stAcceleration(time, x, y, z, eta, useConstrain) ;
    // 0 < z <= eta => stretching
    return get1stAcceleration(time, x, y, 0.0, 0.0, false) +getAcceleration_dz_at0(time, x, y) *z;
}
