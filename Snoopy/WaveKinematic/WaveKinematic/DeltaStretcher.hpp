#pragma once

#include "WaveKinematicExport.hpp"
#include "Spectral/Wif.hpp"
#include "WaveKinematic/WaveKinematicABC.hpp"

namespace BV
{
namespace WaveKinematic
{

/*!
 * \brief The DeltaStretching class is used for definition of the delta-stretching function
 * delta-stretching is working only for eta > 0
 */
class WAVE_KINEMATIC_API DeltaStretching : public WaveKinematicABC
{
private:
    /*!
     * \brief delta_ : \f$\Delta\f$ parameter from 0 to 1. If \f$\Delta=0\f$ and \f$d_\Delta = d\f$ => method is equivalent to Wheeler model, if \f$\Delta = 1\f$ and \f$d_\Delta = d\f$ => linear extrapolation must be used
     */
    double delta_;
    /*!
     * \brief hDelta_ : \f$d_\Delta\f$
     */
    double hDelta_;
    /*!
     * \brief inCrestOnly_ : if it is true, then the calculation is done only in the crest case (Rudenbusch & Forristall)
     *  otherwise for all case (book of Molin)
     */
    bool inCrestOnly_;
public:
    DeltaStretching(std::shared_ptr<Spectral::Wif> p_wif, double delta, double hDelta = -1, bool inCrestOnly = false, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wif, numThreads), delta_(delta), hDelta_(hDelta), inCrestOnly_(inCrestOnly)
    {
        /*
         * In WaveKinematicABC, the phase is calculated at (x = 0, y = 0, t = 0)
         *                      the amplitudes for az are calculated in z = 0.
         */
        isWheeler_ = false;
        get1stElevation(time_, x_, y_); // result is saved in eta1Saved_
        // In the case of the finite depth if hDelta <= 0 => hDelta = depth
        if (depth_ > 0.0 && (hDelta < 0.0 || hDelta > depth_))
            hDelta_ = depth_;
    }

    DeltaStretching(std::shared_ptr<Spectral::Wifm> p_wifm, double delta, double hDelta = -1, bool inCrestOnly = false, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wifm, numThreads), delta_(delta), hDelta_(hDelta), inCrestOnly_(inCrestOnly)
    {
        /*
         * In WaveKinematicABC, the phase is calculated at (x = 0, y = 0, t = 0)
         *                      the amplitudes for az are calculated in z = 0.
         */
        isWheeler_ = false ;
        get1stElevation(time_, x_, y_); // result is saved in eta1Saved_
        // In the case of the finite depth if hDelta < 0 => hDelta = depth
        if (depth_ > 0.0 && (hDelta < 0.0 || hDelta > depth_))
            hDelta_ = depth_;
    }

    ~DeltaStretching()
    {

    }
    /*!
     * \brief replace the z by z' according to the Wheeler's stretching rule
     * \param[in] z : z coordinate
     * \param[in] eta : free surface elevation
     * \return (z-\eta)/(1 +\eta/depth) for the finite depth case and z-depth for the infinite one
     */
    double stretcher(double z, double eta, double smallDDelta = 0.5);

    void setDelta(double delta) {delta_ = delta;}
    void setDepthDelta(double hDelta) {hDelta_ = hDelta;}
    void inCrestOnly(bool isInCrestOnly = true) {inCrestOnly_ = isInCrestOnly;}
    bool isInCrestOnly() const {return inCrestOnly_;}
    double getDelta() const {return delta_;}
    double getDepthDelta() const{return hDelta_;}

    using WaveKinematicABC::getVelocity ;
    using WaveKinematicABC::getAcceleration ;

    Eigen::Vector3d getVelocity(double time, double x, double y, double z) override;
    Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override;
};

}   // namespace WaveKinematic
}   // namespace BV
