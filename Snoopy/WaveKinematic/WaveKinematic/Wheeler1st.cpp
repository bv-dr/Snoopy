#include "WaveKinematic/Wheeler1st.hpp"

using namespace BV::WaveKinematic;

Eigen::Vector3d Wheeler1st::getVelocity(double time, double x, double y, double z)
{
    get1stElevation(time, x, y);    // evaluate the free surface elevation. It automatically checks whether there is a need to recalculate it
    // zs - stretched z. Normally, it should be from -d to 0
    double zs = wheeler_.stretcher(z, eta1Saved_);
    // in a case zs is outside of this range return 0
    if (zs > 0.0)
        return Eigen::Vector3d(0., 0., 0.);
    // for the finite depth case zs have to be above -depth_
    if (depth_ > 0.0 && (zs < -depth_))
        return Eigen::Vector3d(0., 0., 0.);
    return get1stVelocity(time, x, y, zs, eta1Saved_, false);
}

Eigen::Vector3d Wheeler1st::getAcceleration(double time, double x, double y, double z)
{
    get1stElevation(time, x, y);
    double zs = wheeler_.stretcher(z, eta1Saved_);
    if (zs > 0.0)
        return Eigen::Vector3d(0., 0., 0.);
    if (depth_ > 0.0 && (zs < -depth_))
        return Eigen::Vector3d(0., 0., 0.);
    return get1stAcceleration(time, x, y, zs, eta1Saved_, false);
}
