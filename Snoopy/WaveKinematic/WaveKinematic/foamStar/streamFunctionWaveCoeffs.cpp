/*
 * The file is a part of Snoopy and FoamStar.
 *
 * If there is a modification done in this file in Snoopy or FoamStar, the other library must be updated
 */

#include "streamFunctionWaveCoeffs.H"
#include "waveModel.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * Static Member Functions * * * * * * * * * * * * * //

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

bool bv::waves::streamFunctionWaveCoeffs::solve()
{
    z_=z0_; int nz(z_.size());
    Eigen::MatrixXd A(Eigen::MatrixXd::Zero(nz,nz));
    Eigen::VectorXd f(nz);
    Eigen::VectorXd x;

    bool converged(false);
    for (iter_=0; iter_<= maxIter_; iter_++)
    {
        //Info << "DEBUG: " << iter_ << Foam::endl; forAll(z_,i) Info << " " << z_(i); Info << Foam::endl;
        // Precalculation some variables
        double sqrtz2 = sqrt(z_(1));
        Eigen::ArrayXXd Sjm(N_, N_+1);
        Eigen::ArrayXXd Cjm(N_, N_+1);

        for (int j=0; j<N_; j++)
        {
            for (int m=0; m<N_+1; m++)
            {
                //Info << "Z(m+9): " << j << " " << m << " " << z_(m+9) << " " << ((j+1)*z_(m+9)) << Foam::endl;
                Sjm(j, m) = exp((j+1)*z_(m+9));
            }
        }

        Cjm = Sjm;
        if (!infDepth_)
        {
            for (int j=0; j<N_; j++)
            {
                // e2 = 1./(1. +e^{-2jz_1})
                double e2( 1./( 1.+exp(-2.*(j+1)*z_(0)) ) );
                for (int m=0; m<N_+1; m++)
                {
                    // e1 = e^{-2j(z_1 +z_{m+10})}
                    double e1(exp(-2.*(j+1)*(z_(0) +z_(m+9))));
                    double fs((1.-e1)*e2);
                    double fc((1.+e1)*e2);
                    Sjm(j, m) *= fs;
                    Cjm(j, m) *= fc;
                }
            }
        }

        // f_1 & df_1/dz
        if (infDepth_)
        {
            f(0) = z_(0) + 1.;  //dummy eq. z_(0) = -1;
            A(0,0) = 1.;
        }
        else
        {
            f(0) = z_(1) - z_(0)*H_/h_;
            A(0,0) = -H_/h_;
            A(0,1) = 1.;
        }

        // f_2 & df_2/dz
        if (useL_)
        {
            f(1) = z_(1) - k_*H_;
        }
        else
        {
            f(1) = z_(1) -Ht_*z_(2)*z_(2);
            A(1,2) = -2.*Ht_ *z_(2);
        }
        A(1,1) = 1.;

        // f_3 & df_3/dz
        f(2) = z_(3) *z_(2) -2.*M_PI;
        A(2,2) = z_(3);
        A(2,3) = z_(2);

        // f_4 & df_4/dz
        f(3) = z_(4) +z_(6) -z_(3);
        A(3,3) = -1.;
        A(3,4) =  1.;
        A(3,6) =  1.;

        // f_5 & df_5/dz
        f(4) = z_(5) +z_(6) -z_(3);
        A(4,3) = -1.;
        A(4,5) =  1.;
        A(4,6) =  1.;
        if (!infDepth_)
        {
            double _1z1(1./z_(0));
            f(4) -= z_(7) *_1z1;
            A(4,0) = z_(7) *_1z1 *_1z1;
            A(4,7) = -_1z1;
        }

        // f_6 & df_6/dz
        f(5) = z_(useCE_ ? 4 : 5) -cEcS_ *sqrtz2;
        A(5,1) = 0.5*cEcS_/sqrtz2;
        A(5,useCE_ ? 4 : 5) = 1.;

        // f_7 & df_7/dz
        f(6) = z_(9) +z_(N_+9);
        A(6,9) = 1.;
        A(6,N_+9) = 1.;
        for (int m=1; m<N_; m++)
        {
            f(6) += z_(m+9) +z_(m+9);
            A(6, 9+m) = 2.;
        }

        // f_8 & df_8/dz
        f(7) = z_(9) -z_(N_+9) -z_(1);
        A(7,1) = -1.;
        A(7,9) =  1.;
        A(7,N_+9) = -1.;

        // f_m+9, df_m+9/dz,       for m = 0,1,2,...,N
        // f_N+10+m, df_N+10+m/dz, for m = 0,1,2,...,N
        for (int m=0; m<N_+1; m++)
        {
            double psi(0.);
            double dpsi1(0.);
            double dpsim10(0.);
            double u(-z_(6));
            double du1(0.);
            double w(0.);
            double dw1(0.);
            double dum10(0.);
            double dwm10(0.);
            for (int j=0; j<N_; j++)
            {
                double Sc(Sjm(j,m) *cjm_(j,m));
                double Cc(Cjm(j,m) *cjm_(j,m));
                double Ss(Sjm(j,m) *sjm_(j,m));
                double Cs(Cjm(j,m) *sjm_(j,m));
                psi +=        z_(N_+10 +j) *Sc;
                u   += (j+1) *z_(N_+10 +j) *Cc;
                w   += (j+1) *z_(N_+10 +j) *Ss;
                int j1(j+1);
                int j2((j+1)*(j+1));
                if (!infDepth_)
                {
                    double tanhjz1(tanh((j+1)*z_(0)));
                    dpsi1 += j1 *z_(N_+10+j) *(Cc -tanhjz1 *Sc);
                    du1   += j2 *z_(N_+10+j) *(Sc -tanhjz1 *Cc);
                    dw1   += j2 *z_(N_+10+j) *(Cs -tanhjz1 *Ss);
                }
                dpsim10 += j1 *z_(N_+10+j) *Cc;
                dum10   += j2 *z_(N_+10+j) *Sc;
                dwm10   += j2 *z_(N_+10+j) *Cs;

                A(8+m,    N_+10+j) = Sc;
            }
            f(8+m) = -z_(7) -z_(m+9)*z_(6) +psi;
            f(N_+9+m) = 0.5*u*u +0.5*w*w +z_(m+9) -z_(8);
            A(8+m,0) = dpsi1;
            A(8+m,6) = -z_(9+m);
            A(8+m,7) = -1.;

            A(N_+9+m,0) = u*du1 +w*dw1;
            A(N_+9+m,6) = -u;
            A(N_+9+m,8) = -1.;

            A(8+m,   9+m) = -z_(6) +dpsim10;
            A(N_+9+m,9+m) = u*dum10 +w*dwm10 +1.;
            for (int j=0; j<N_; j++)
            {
                A(N_+9+m, N_+10+j) = (j+1)*(u *Cjm(j,m) *cjm_(j,m) +w *Sjm(j,m) *sjm_(j,m));
            }
        }

        // https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
        x = A.fullPivLu().solve(-f);
        z_ += damp_*x;
        if (!infDepth_) { z_[0] = std::abs(z_[0]); }
        z_.segment(1, 8) = z_.segment(1,8).cwiseAbs();
        error_ = x.norm();
        if (bv::LessOrEqual(error_, tol_)) { converged=true; break; }
        //std::cout << "DEBUG: iter_: " << iter_ << ", error_: " << error_ << std::endl;
    }

    // remove high-order component(s)
    Eigen::VectorXd Barray; this->getB(Barray);
    int limN(N_);
    for (auto Bi = Barray.end()-1; Bi!=Barray.begin(); Bi--)
    {
        if (std::abs(*Bi) > cutOffTol_) { break; }
        limN--;
    }
    limN = std::max(1,limN);
    if (limN != N_)
    {
        N_=limN;
        this->initialize();
        return this->solve();
    }

    if (!converged)
    {
        if (iter_ > maxIter_)
        {
            if (damp_>0.2)
            {
                damp_ -= 0.1;
                BVWARN("Wave solution did not converged within maxIter:", maxIter_,
                    ".\nTarget tol.:", tol_, ", L2 norm Err.:", error_,
                    ".\nRe-try with damp_:", damp_);
                nRetry_++;
                return this->solve();
            }
            else
            {
                BVSTOP("Wave solution did not converged in maxIter:", maxIter_,
                    ".\nTarget tol.:", tol_, ", L2 norm err.:", error_,
                    ".\nPlease re-try with some adjustments to maxIter and/or damping parameters.");
                return false;
            }
        }
    }

    if (nRetry_>0)
    {
        BVINFO("Wave solution converged at iter.:", iter_,
            ".\nTarget tol.:", tol_, ", L2 norm err.:", error_);
    }

    return true;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void bv::waves::streamFunctionWaveCoeffs::initialize()
{
    // Precalculate
    //      cjm_(j,m) = cos(jm\pi/N) for j = 1,2,...,N, m = 0,1,2,...,N
    //      sjm_(j,m) = sin(jm\pi/N) for j = 1,2,...,N, m = 0,1,2,...,N
    cjm_.resize(N_, N_+1);
    sjm_.resize(N_, N_+1);
    double piN(M_PI/N_);
    double darg(0.);
    for (int j=0; j<N_; j++)
    {
        darg += piN;        // darg = (j+1) *pi/N
        double arg (0.);
        for (int m=0; m<N_+1; m++)
        {
            // arg = (j+1) *m *M_PI/N_
            cjm_(j,m) = cos(arg);
            sjm_(j,m) = sin(arg);
            arg += darg;
        }
    }

    z_.resize(2*N_+10);
    double tanhkd(1.);
    if (infDepth_)
    {
        z_(0) = -1;
        z_(1) = k_ *H_;
        z_(2) = 2.*M_PI;
        z_(3) = 1.;
    }
    else
    {
        z_(0) = k_ *h_; tanhkd = tanh(z_(0));
        z_(1) = 4.*M_PI*M_PI *Ht_;
        z_(2) = 2.*M_PI/sqrt(tanhkd);
        z_(3) = sqrt(tanhkd);
    }
    if (useCE_)
    {
        z_(4) = sqrt(z_(1)) *cEcS_;
        z_(5) = 0.;
    }
    else
    {
        z_(4) = 0.;
        z_(5) = sqrt(z_(1)) *cEcS_;
    }
    z_(6) = z_(3);
    z_(7) = 0.;
    z_(8) = 0.5 *tanhkd;
    z_(9) = 0.5 *z_(1);
    for (int m=0; m<N_; m++)
    {
        z_(m+10) = z_(9) * cjm_(m, 1);   //cos((m+1)*M_PI/N_);
    }
    z_(N_+10) = z_(9) /z_(3);
    for (int m=0; m<N_-1; m++)
    {
        z_(N_+11 +m) = 0.;
    }
    z0_ = z_;
}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

bv::waves::streamFunctionWaveCoeffs::streamFunctionWaveCoeffs
(
    const double depth,
    const double H,
    const double w,
    const double k,
    const bool useL,
    const double cEcS,
    const bool useCE,
    const int N,
    const int maxIter,
    const double tol,
    const double damp,
    const double gMag,
    const double cutOffTol
)
:
    N_(std::max(N,int(1))),
    h_(depth), H_(H),
    omega_(w), k_(k), useL_(useL),
    cEcS_(cEcS), useCE_(useCE),
    maxIter_(std::max(10,maxIter)),
    tol_(std::max(1e-15,tol)),
    cutOffTol_(std::max(1e-15, cutOffTol)),
    damp_(std::min(1.0,std::max(0.01,damp))),
    nRetry_(0),
    gMag_(gMag)
{
    infDepth_ = waveModel::isDeepWater(h_);
    cEcS_ /= sqrt(gMag_ *H_);

    double T(0.);

    if (!useL_) // pre-estimate k_
    {
        // Note: omega_ is exact if useL_ false;
        // otherwise, omega_ is an estimate (likewise for T and Ht_)
        T = 2.0*M_PI/w;
        if (infDepth_)
        {
            k_ = w*w/gMag_;
        }
        else
        {
            double a(w*w/gMag*h_);
            double b(a*sqrt(1./tanh(a)));
            double t(tanh(b));
            double bc(b*(1.-t*t));
            k_ = (a+b*bc)/(t +bc)/h_;
        }
    }
    else
    {   // estimate omega_, T
        if (infDepth_)
        {
            omega_ = sqrt(k_ *gMag);
        }
        else
        {
            omega_ = sqrt(abs(k_ *gMag *tanh(k_ *h_)));
        }
        T = 2.0*M_PI/omega_;
    }
    Ht_ = H_/(gMag_*T *T);

    // initialize and solve for z_
    this->initialize();
    this->solve();

    // update dependent parameters
    if (infDepth_) { k_ = z_(1)/H_; } else { k_ = z_(0)/h_; }
    double fac1(sqrt(k_/gMag_));
    T = z_(2)/sqrt(k_*gMag_);
    Ht_ = H_/(gMag_*T *T);
    omega_ = 2.0*M_PI/T;
    c_ = z_(3)/fac1;                    // c sqrt(k/g) /sqrt(k/g)
    cE_ = z_(4)/fac1;
    cS_ = z_(5)/fac1;
    u_ = z_(6)/fac1;
    q_ = z_(7)/k_/fac1;
    r_ = z_(8)*gMag_/k_;
    Y_.resize(N_);
    for (int j=0; j<N_; j++)
    {
        Y_(j) = z_(9) +z_(N_+9)*cjm_(j,N_);
        for (int m=1; m<N_; m++) { Y_(j) += 2.*z_(9+m)*cjm_(j,m); }
        Y_(j) /= N_;
    }
};


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void bv::waves::streamFunctionWaveCoeffs::getA(Eigen::VectorXd& A) const
{
    A.resize(N_+1);
    A(0) = 0;
    for (int j=0; j<N_; j++) { A(j+1) = Y_(j)/k_; }
    A(N_) *= 0.5;
}

void bv::waves::streamFunctionWaveCoeffs::getB(Eigen::VectorXd& B) const
{
    const double fac1(sqrt(k_/gMag_));
    B.resize(N_+1);
    B(0) = (z_(3) - z_(6))/fac1; // c - uBar
    for (int j=0; j<N_; j++) { B(j+1) = z_(N_+10+j)/fac1; }
}

// ************************************************************************* //
