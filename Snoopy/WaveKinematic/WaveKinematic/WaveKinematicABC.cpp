#include "WaveKinematic/WaveKinematicABC.hpp"

using namespace BV::WaveKinematic;

void WaveKinematicABC::initialize_(bool withUpdatePhases)
{
    depth_ = p_wif_->getDepth();
    g_       = 9.81;                // FIXME: Gravity acceleration may be already given somewhere else
    // calculation of k *cos(heading) and k *sin(heading)
    const Eigen::ArrayXd& k = p_wif_->getWaveNumbers();
    kchead_ = p_wif_->getCosHeadings() * k;
    kshead_ = p_wif_->getSinHeadings() * k;
    if (depth_ > 0.0)
        tanhkh_ = (k *depth_).tanh() ;
    else
        tanhkh_ = Eigen::ArrayXd::Ones(k.size()) ;

    if (withUpdatePhases)
    {
        time_ = -1.0 ;  // time is greater than or equal to 0.0, thus -1.0 signify that the phase was never evaluated
        x_ = 0.0 ;
        y_ = 0.0 ;
        e_time_ = -1.0; // free surface elevation will be recalculated since time t is allways positive
        e_x_ = 0.0 ;
        e_y_ = 0.0 ;
    }
    // initialization of the variables for the Linear Kinematics
    const Eigen::ArrayXd& w = p_wif_->getFrequencies();
    gw_.resize(w.size());
    ampAtZ_.resize(w.size());
    Z_.resize(w.size()) ;
    Zz_.resize(w.size()) ;
    z_ = 1.0 ;                  // initial value of z_ is not 0.0 in order
    evaluateAmpAtZ0();          // evaluates ampAtZ0_, ampAtZ0z_ and ampAtZ0zz_, which are used for linear extrapolation if needed
    evaluateAmpAtZ(0.0);        // to be sure the amplitude is evaluated at z = 0.0
    for(auto i = 0; i < gw_.size(); ++i)
    {
        if (w(i) < 1.e-12)
        {
            gw_(i) = 1.e12 ;
        }
        else
        {
            gw_(i) = g_ / w(i);
        }
    }
}

void WaveKinematicABC::updatePhase(double t, double x, double y, bool force)
{
    if (isTimeDifferent(t))
    {
        if (p_wifm_)
        {
            std::size_t index(p_wifm_->getWifIndex(t)) ;
            if (index != wifIndex_)
            {
                wifIndex_ = index ;
                p_wif_ = p_wifm_->getWifAtIndex(index) ;
                initialize_(false) ;
            }
        }
        // Note, in order to execute the followin if in a case x_==x, y_==y but time_!=t
        // time_ is not set to t here.
    }
    if (force || isPointDifferent(t, x, y))
    {
        time_ = t;
        x_ = x;
        y_ = y;
        phase_ = getPhase(t, x, y);
        // evaluation of the cos(phase)
        cphase_ = phase_.cos();
        // evaluation of the sin(phase)
        sphase_ = phase_.sin();
//        cos_phase_updated = 0;    // this is for the second order case
//        sin_phase_updated = 0;    // this is for the second order case
    }
}

void WaveKinematicABC::evaluateZ(double z, int force)
{
    /*
     * eq. (5) in "Incident_Second_Order_eqs.pdf".
     *
     * Infinite depth:
     *      Z_  :     e^{k_i z}
     *      Zz_ : k_i e^{k_i z}
     * Finite depth:
     *      Z_  :     \cosh(k_i(z+h)) /\cosh(k_i h)
     *      Zz_ : k_i \sinh(k_i(z+h)) /\cosh(k_i h)
     */
    if ( (force) || (z_ != z))    // Z_ and Zz_ were not calculated yet
    {
        z_ = z ;
        const Eigen::ArrayXd& k = p_wif_->getWaveNumbers();
        if (z_ > 0.0)       // above the mean free surface
        {
            Z_  = 0.0 ;
            Zz_ = 0.0 ;
            return ;
        }
        // z_ <= 0.0
        if (depth_ > 0.0)   // finite depth case
        {
            if (z_ == 0.0)  // particular case
            {
                Z_  = 1.0 ;
                Zz_ = k *tanhkh_ ;    // k \tanh kh
                return ;
            }
            // z_ < 0.0
            Eigen::ArrayXd expkz  = (k *z_).exp() ;                     // e^{kz}
            Eigen::ArrayXd expkzh = (k*(-z_-depth_-depth_)).exp() ;     // e^{-k(z+2h)}
            Eigen::ArrayXd expkh  = (-k*(depth_+depth_)).exp()  ;       // e^{-2kh}
            Z_  =    (expkz +expkzh)/(1.0 +expkh) ;                     //   cosh k(z+h) /cosh kh
            Zz_ = k *(expkz -expkzh)/(1.0 +expkh) ;                     // k sinh k(z+h) /cosh kh
            return ;
        } else  // infinite depth case
        {
            if (z_ == 0.0)  // particular case
            {
                Z_  = 1.0 ;
                Zz_ = k ;
                return ;
            }
            // z_ < 0.0
            Eigen::ArrayXd expkz = (k *z_).exp() ;                      //   e^{kz}
            Z_  = expkz ;                                               //   e^{kz}
            Zz_ = k *expkz ;                                            // k e^{kz}
            return ;
        }
    }
}

void WaveKinematicABC::evaluateAmpAtZ(double z)
{
    /*
     * Infinite depth:
     *      ampAtZ_  :     a_i e^{k_i z}
     *      ampAtZz_ : k_i a_i e^{k_i z}
     * Finite depth:
     *      ampAtZ_  :     a_i \cosh(k_i(z+h)) /\cosh(k_i h)
     *      ampAtZz_ : k_i a_i \sinh(k_i(z+h)) /\cosh(k_i h)
     */
    if (z_ != z) {  // amplitude and its derivative were not calculated yet
        evaluateZ(z) ;
        const Eigen::ArrayXd& a = p_wif_->getAmplitudes() ;
        ampAtZ_  = a *Z_ ;
        ampAtZz_ = a *Zz_ ;
    }
}

void WaveKinematicABC::evaluateAmpAtZ0()
{
    /*
     * Infinite depth:
     *      ampAtZ0_   :        a_i
     *      ampAtZ0z_  :    k_i a_i
     *      ampAtZ0zz_ :  k_i^2 a_i
     * Finite depth:
     *      ampAtZ0_   :        a_i
     *      ampAtZ0z_  :    k_i a_i \tanh(k_ih)
     *      ampAtZ0zz_ :  k_i^2 a_i
     */
    const Eigen::ArrayXd& a(p_wif_->getAmplitudes());
    const Eigen::ArrayXd& k(p_wif_->getWaveNumbers());
    ampAtZ0_   = a;
    ampAtZ0z_  = ampAtZ0_.array()  *k;      // ak
    ampAtZ0zz_ = ampAtZ0z_.array() *k;      // ak^2
    if (depth_ > 0.0)       // Finite depth
    {
        ampAtZ0z_  = ampAtZ0z_.array() *tanhkh_;    // ak tanh(kh)
    }
}

// The implementation of the main functions:
double WaveKinematicABC::get1stPotential(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return 0.0;
    }
    /*
     * eq. (4) in "Incident_Second_Order_eqs.pdf".
     *
     * phi = -\sum_n a_n(z) g/w_n \sin(phase_n(x,y))
     * a_n(z) = A_n exp(k_nz)                 for infinite depth case
     * a_n(z) = A_n \cosh k_n(z+h) /\cosh k_nh  for finite depth case
     */
    updatePhase(time, x, y) ;
    evaluateZ(z) ;
    // gw_ := g / w
    return -static_cast<Eigen::VectorXd>(p_wif_->getAmplitudes()).transpose()
           *static_cast<Eigen::VectorXd>(gw_ *Z_ *sphase_) ;
}

Eigen::Vector3d WaveKinematicABC::get1stVelocity(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
    /*
     * u_x = \partial \phi /\partial x =  \sum a_n(z) g/w_n k_n \cos\beta_n \cos(phase_n(x,y)). a_n (z) -> ampAtZ_
     * u_y = \partial \phi /\partial y =  \sum a_n(z) g/w_n k_n \sin\beta_n \cos(phase_n(x,y)). a_n (z) -> ampAtZ_
     * u_z = \partial \phi /\partial z = -\sum a_n'(z) g/w_n \sin(phase_n(x,y)).                a_n'(z) -> ampAtZz_
     */
    updatePhase(time, x, y) ;
    Eigen::ArrayXd uxy_tmp = gw_ *cphase_.array() ;
    Eigen::VectorXd ux_tmp = uxy_tmp *kchead_.array() ;     // g/w_n *k_n\cos\beta_n *\cos\phase_n
    Eigen::VectorXd uy_tmp = uxy_tmp *kshead_.array() ;     // g/w_n *k_n\sin\beta_n *\cos\phase_n
    Eigen::VectorXd uz_tmp = gw_ *sphase_.array() ;         // g/w_n                 *\sin\phase_n

    evaluateAmpAtZ(z);

    return Eigen::Vector3d( ampAtZ_.transpose()  *ux_tmp,
                            ampAtZ_.transpose()  *uy_tmp,
                           -ampAtZz_.transpose() *uz_tmp);
}

Eigen::Vector3d WaveKinematicABC::get1stVelocityDzAt0(double time, double x, double y)
{
    /*
     * \partial u_x(x, y, 0)/\partial z = \partial^2 \phi(x, y, 0) /\partial x\partial z =  \sum a_n'(z=0) g/w_n k_n \cos\beta_n \cos(phase_n(x,y)). a_n (z) -> ampAtZ_
     * \partial u_y(x, y, 0)/\partial z = \partial^2 \phi(x, y, 0) /\partial y\partial z =  \sum a_n'(z=0) g/w_n k_n \sin\beta_n \cos(phase_n(x,y)). a_n (z) -> ampAtZ_
     * \partial u_z(x, y, 0)/\partial z = \partial^2 \phi(x, y, 0) /\partial z\partial z = -\sum a_n''(z=0) g/w_n \sin(phase_n(x,y)).                a_n'(z) -> ampAtZz_
     */
    updatePhase(time, x, y) ;
    Eigen::ArrayXd uxy_tmp = gw_ *cphase_.array() ;
    Eigen::VectorXd ux_tmp = uxy_tmp *kchead_.array() ;     // g/w_n *k_n\cos\beta_n *\cos\phase_n
    Eigen::VectorXd uy_tmp = uxy_tmp *kshead_.array() ;     // g/w_n *k_n\sin\beta_n *\cos\phase_n
    Eigen::VectorXd uz_tmp = gw_ *sphase_.array() ;         // g/w_n                 *\sin\phase_n

    return Eigen::Vector3d( ampAtZ0z_.transpose()  *ux_tmp,
                            ampAtZ0z_.transpose()  *uy_tmp,
                           -ampAtZ0zz_.transpose() *uz_tmp);
}

Eigen::Vector3d WaveKinematicABC::get1stAcceleration(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
    /*
     * a_x = \partial \phi /\partial x = -\sum a_n(z) g k_n \cos\beta_n \sin(phase_n(x,y)). a_n (z) -> ampAtZ_
     * u_y = \partial \phi /\partial y = -\sum a_n(z) g k_n \sin\beta_n \sin(phase_n(x,y)). a_n (z) -> ampAtZ_
     * u_z = \partial \phi /\partial z = -\sum a_n'(z) g \cos(phase_n(x,y)).                a_n'(z) -> ampAtZz_
     */
    updatePhase(time, x, y) ;
    Eigen::ArrayXd uxy_tmp = g_ *sphase_.array() ;
    Eigen::VectorXd ux_tmp = uxy_tmp *kchead_.array() ;     // g *k_n\cos\beta_n *\sin\phase_n
    Eigen::VectorXd uy_tmp = uxy_tmp *kshead_.array() ;     // g *k_n\sin\beta_n *\sin\phase_n
    Eigen::VectorXd uz_tmp = g_ *cphase_.array() ;          // g                 *\cos\phase_n

    evaluateAmpAtZ(z);

    return Eigen::Vector3d(-ampAtZ_.transpose()  *ux_tmp,
                           -ampAtZ_.transpose()  *uy_tmp,
                           -ampAtZz_.transpose() *uz_tmp);
}

double WaveKinematicABC::get1stPhiztAt0(double time, double x, double y)
{
    /*
     * Phi_zt(x,y,0,t) = \sum_{n=1}^N -a_n \omega_n^2 \cos(phase_n(x,y))
     */
    updatePhase(time, x, y) ;
    return -static_cast<Eigen::VectorXd>(p_wif_->getAmplitudes()).transpose()
           *static_cast<Eigen::VectorXd>(p_wif_->getFrequencies().pow(2) *cphase_) ;
}

double WaveKinematicABC::get1stPressure(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return 0.0;
    }
    /*
     * p = \sum_n a_n(z) g \cos(phase_n(x, y))
     */
    updatePhase(time, x, y) ;
    Eigen::VectorXd tmp = cphase_;			// pressure is defined as P/rho/g

    evaluateAmpAtZ(z);
    p1Saved_ = ampAtZ_.transpose() *tmp;
    return  p1Saved_;
}

double WaveKinematicABC::get1stElevation(double time, double x, double y, bool update)
{
    /*
     * \eta = \sum_n a_n \cos(phase_n(x, y))
     */
    /*
     * I do not understand for what purpose this variable (forgot if it was written by me, I.Ten)
     * It seems, that if update is true, we use updatePhase method, which not only calculates the phases
     *  but saves the instants of time, x and y in order do not recalculate the phase
     */
    if (update)
    {
        // In a case time, x and y are as same as for the previous calculation, no need to recalculate the elevation.
        if (!isEPointDifferent(time, x, y)) return eta1Saved_;
        // elevation
        updatePhase(time, x, y);
        eta1Saved_ = (static_cast<Eigen::VectorXd>(p_wif_->getAmplitudes())).transpose()* static_cast<Eigen::VectorXd>(cphase_);
        saveEPoint(time, x, y);     // save the point at which free surface elevation was calculated
        return eta1Saved_;
    }

    Eigen::ArrayXd cphase = getPhase(time, x, y).cos();
    return (static_cast<Eigen::VectorXd>(p_wif_->getAmplitudes())).transpose()* static_cast<Eigen::VectorXd>(cphase);
}




Eigen::ArrayXXd WaveKinematicABC::getElevation_2D(const Eigen::Ref<const Eigen::ArrayXd>& time, const Eigen::Ref<const Eigen::ArrayXd>& x, const Eigen::Ref<const Eigen::ArrayXd>& y, double speed)
{
    Eigen::ArrayXXd res(time.size(), x.size());

    #pragma omp parallel for num_threads(numThreads_)
    for (auto i = 0; i < x.size(); ++i)
    {
        res.col(i) = _getElevation_timeVect(time, x(i), y(i), speed );
    }

    return res;
}

// Not sure it works, binding python binding is not.
Eigen::ArrayXd WaveKinematicABC::getElevation_omp(const Eigen::Ref<const Eigen::ArrayXd>& time, const Eigen::Ref<const Eigen::ArrayXd>& x, const Eigen::Ref<const Eigen::ArrayXd>& y, double speed)
{
    Eigen::ArrayXd res(time.size());

    if ((time.size() != x.size()) || (time.size() != y.size()))
    {
        throw std::invalid_argument("getElevation : x, y and time should have same length");
    }

    #pragma omp parallel for num_threads(numThreads_)
    for (auto i = 0; i < x.size(); ++i)
    {
        res(i) = getElevation(time(i), x(i), y(i), speed, true );
    }

    return res;
}

/*
Eigen::ArrayXXd WaveKinematicABC::getElevation(const Eigen::Ref<const Eigen::ArrayXd>& time, const Eigen::Ref<const Eigen::ArrayX2d>& xy, double speed)
{
    auto numValues = xy.col(0).size();
    Eigen::ArrayXXd res(time.size(), numValues);

    #pragma omp parallel for num_threads(numThreads_)
    for (auto i = 0; i < numValues; ++i)
    {
        res.col(i) = getElevation(time, xy.col(0)(i), xy.col(1)(i), speed);
    }

    return res;
}

void WaveKinematicABC::getElevation(const Eigen::Ref<const Eigen::ArrayXd>& time, const Eigen::Ref<const Eigen::ArrayX2d>& xy, Eigen::Ref<Eigen::ArrayXXd> eta, double speed)
{
    auto numValues = xy.col(0).size();
    if (eta.size() != time.size() * numValues)
    {
        throw std::invalid_argument("getElevation: incorrect elevation array dimensions");
    }

    #pragma omp parallel for num_threads(numThreads_)
    for (auto i = 0; i < numValues; ++i)
    {
        eta.col(i) = getElevation(time, xy.col(0)(i), xy.col(1)(i), speed);
    }
}
*/
