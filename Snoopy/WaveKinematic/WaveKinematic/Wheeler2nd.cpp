#include "WaveKinematic/Wheeler2nd.hpp"

using namespace BV::WaveKinematic ;

Eigen::Vector3d Wheeler2nd::getVelocity(double time, double x, double y, double z)
{
    auto eta(getElevation(time, x, y));     // Calculates the 2nd order free surface elevation
    if (z > eta)
        return Eigen::Vector3d(0.0, 0.0, 0.0);
    return SecondOrderKinematic::getVelocity(time, x, y, wheeler_.stretcher(z, eta));
}

Eigen::Vector3d Wheeler2nd::getAcceleration(double time, double x, double y, double z)
{
    auto eta(getElevation(time, x, y));     // Calculates the 2nd order free surface elevation
    if (z > eta)
        return Eigen::Vector3d(0.0, 0.0, 0.0);
    return SecondOrderKinematic::getAcceleration(time, x, y, wheeler_.stretcher(z, eta));
}
