#include "WaveKinematic/SecondOrderKinematic.hpp"

#include <iostream>
#include <cmath>
#include <iomanip>

using namespace BV::WaveKinematic;

void SecondOrderKinematic::initialize_()
{
    // definition of the sum and difference quantities
    auto k = p_wif_->getWaveNumbers() ;
    auto omega = p_wif_->getFrequencies() ;
    auto a = p_wif_->getAmplitudes() ;
    nbwaves_ = static_cast<int>(k.size()) ;
    setMode();          // by default use sum and dif modes. Note: there is no any optimization if we use only one mode case

    indexOffset_ = Eigen::VectorXi(nbwaves_+1);
    indexOffset_(0) = 0;
    for(auto n = 1; n < nbwaves_ ; ++n)
        indexOffset_(n) = indexOffset_(n-1) +nbwaves_ -n;
    indexOffset_(nbwaves_) = indexOffset_(nbwaves_ -1) +nbwaves_;  // total number of elements

    kiP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    kiM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    wiP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    wiM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    qiP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    qiM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    QiP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    QiM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    Eigen::ArrayXd cosBetai(indexOffset_(nbwaves_));
    sphaseP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    cphaseP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    sphaseM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    cphaseM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    ai_      = Eigen::ArrayXd(indexOffset_(nbwaves_));
    tanhkhP_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    tanhkhM_ = Eigen::ArrayXd(indexOffset_(nbwaves_));
    expkhP_  = Eigen::ArrayXd(indexOffset_(nbwaves_));
    expkhM_  = Eigen::ArrayXd(indexOffset_(nbwaves_));
    zP_      = Eigen::ArrayXd(indexOffset_(nbwaves_));
    zM_      = Eigen::ArrayXd(indexOffset_(nbwaves_));
    zzP_     = Eigen::ArrayXd(indexOffset_(nbwaves_));
    zzM_     = Eigen::ArrayXd(indexOffset_(nbwaves_));
    const auto chead = p_wif_->getCosHeadings();
    const auto shead = p_wif_->getSinHeadings();
    auto h2 = -depth_ -depth_;
    for(auto n = 0; n < nbwaves_; ++n)
    {
        for(auto m = n; m < nbwaves_; ++m)
        {
            auto i = indexOffset_(n) +m;
            cosBetai(i) = chead(n)*chead(m) +shead(n)*shead(m);
            kiP_(i) = std::sqrt(k(n) *k(n) +k(m) *k(m) +2.0*k(n)*k(m)*cosBetai(i));
            kiM_(i) = std::sqrt( std::abs( k(n) *k(n) +k(m) *k(m) -2.0*k(n)*k(m)*cosBetai(i) ) );
            wiP_(i) = omega(n) +omega(m) ;
            wiM_(i) = omega(n) -omega(m) ;
            ai_(i) = a(n) *a(m) ;
            if (depth_ > 0.0)   // finite depth
            {
                tanhkhP_(i) = std::tanh(kiP_(i) *depth_);
                tanhkhM_(i) = std::tanh(kiM_(i) *depth_);
                expkhP_(i) = 1.0/(1.0 +std::exp(kiP_(i) *h2));
                expkhM_(i) = 1.0/(1.0 +std::exp(kiM_(i) *h2));
            } else
            {
                tanhkhP_(i) = 1.0;
                tanhkhM_(i) = 1.0;
                expkhP_(i)  = 1.0;
                expkhM_(i)  = 1.0;
            }
        }
    }

    if (depth_ > 0.0)   // h < \infty
    {
        Eigen::ArrayXd w3Sur4Sinh2(nbwaves_);
        for(auto n = 0; n < nbwaves_; ++n)
        {
            double em2kh = std::exp(k(n)*(-depth_ -depth_));   // e^{-2kh}
            w3Sur4Sinh2(n) = omega(n) *omega(n) *omega(n);   // \omega^3
            w3Sur4Sinh2(n) *= em2kh /(1.0 -em2kh) /(1.0 -em2kh);    // \omega^3 / (4 sinh^2kh)
        }
        for(auto n = 0; n < nbwaves_; ++n)
        {
            auto i = indexOffset_(n) +n;
            double TiM = cosBetai(i) /tanhkh_(n) /tanhkh_(n) ;
            double TiP =  TiM -1.0;
            TiM = -TiM -1.0;
            double wnmSur2 = 0.5 *omega(n) *omega(n) ;
            qiP_(i) = w3Sur4Sinh2(n) +w3Sur4Sinh2(n) +wnmSur2*wiP_(i)*TiP ;
            qiM_(i) = 0.0 ;
            QiP_(i) = qiP_(i) / (g_ *kiP_(i) *std::tanh(kiP_(i)*depth_) -wiP_(i)*wiP_(i));
            QiM_(i) = 0.0 ;
            for(auto m = n+1; m < nbwaves_; ++m)
            {
                i = indexOffset_(n) +m;
                TiM = cosBetai(i) /tanhkh_(n) /tanhkh_(m) ;
                TiP =  TiM -1.0;
                TiM = -TiM -1.0;
                wnmSur2 = 0.5 *omega(n) *omega(m) ;
                qiP_(i) = 2.0 *(w3Sur4Sinh2(n) +w3Sur4Sinh2(m) +wnmSur2*wiP_(i)*TiP) ;
                qiM_(i) = 2.0 *(w3Sur4Sinh2(n) -w3Sur4Sinh2(m) -wnmSur2*wiM_(i)*TiM) ;
                auto diP = g_ *kiP_(i) *std::tanh(kiP_(i) *depth_) -wiP_(i)*wiP_(i) ;    // gk^+\tanh (k^+h) -(\omega^+)^2
                auto diM = g_ *kiM_(i) *std::tanh(kiM_(i) *depth_) -wiM_(i)*wiM_(i) ;    // gk^-\tanh (k^-h) -(\omega^-)^2
                QiP_(i) = qiP_(i) /diP;
                if (std::abs(diM) < 1.0e-10)
                    QiM_(i) = 0.0;
                else
                    QiM_(i) = qiM_(i) /diM;
            }
        }
    } else
    {
        for(auto n = 0; n < nbwaves_; ++n)
        {
            auto i = indexOffset_(n) +n;
            double wnmSur2 = 0.5 *omega(n) *omega(n) ;
            qiP_(i) =  wnmSur2*wiP_(i)*( cosBetai(i) -1.0) ;
            qiM_(i) = 0.0 ;
            QiP_(i) = qiP_(i) / (g_ *kiP_(i) -wiP_(i) *wiP_(i));
            QiM_(i) = 0.0;
            for(auto m = n+1; m < nbwaves_; ++m)
            {
                i = indexOffset_(n) +m;
                wnmSur2 = 0.5 *omega(n) *omega(m) ;
                qiP_(i) =  2.0 *wnmSur2*wiP_(i)*( cosBetai(i) -1.0) ;
                qiM_(i) = -2.0 *wnmSur2*wiM_(i)*(-cosBetai(i) -1.0) ;
                auto diP = g_ *kiP_(i) -wiP_(i) *wiP_(i) ;      // gk^+ -(\omega^+)^2
                auto diM = g_ *kiM_(i) -wiM_(i) *wiM_(i) ;      // gk^- -(\omega^-)^2
                QiP_(i) = qiP_(i) /diP;
                if (std::abs(diM) < 1.0e-10)
                    QiM_(i) = 0.0;
                else
                    QiM_(i) = qiM_(i) /diM;
            }
        }
    }
    Ct_ = 0.0;
/*
 * For the time being it is recommended not to use this constant, otherwise there are the disagrements
 * in the comparison with other codes
 */
/*
    // Set-down
    if (depth_ > 0.0)
    {
        Ct_ = 0.0;
        for(auto n = 0; n < nbwaves_; ++n)
        {
            auto exp2kh = std::exp(k(n) *h2);                                   // e^{-2kh}
            auto exp4kh = exp2kh *exp2kh;                                       // e^{-4kh}
            auto s = (1.0 -exp4kh) /(1.0 +4.0 *depth_ *k(n) *exp2kh -exp4kh) ;  // s = \sinh 2kh /(2kh +sinh 2kh)
            auto S = -0.5 *g_ *k(n) *(4.0*s +1.0 -tanhkh_(n) *tanhkh_(n))
                    /(4.0 *s *s *k(n) *depth_ -tanhkh_(n));
            Ct_ -= a(n) *a(n) *S;                                               // eq. (25) in "Incident_Second_Order_eqs.pdf"
        }
        Ct_ = 0.0;
    } else
    {
        Ct_ = 0.0 ;                                                         // For the infinite depth case the set-down is zero.
    }
*/
    time_ = -1.0 ;   // because the time is non-negative, it will force to recalculate the phases
    z2_ = +1.0 ;
    evaluateZ2(0.0) ;
}

void SecondOrderKinematic::updatePhase(double t, double x, double y, bool force)
{
    if (force || time_ != t || x_ != x || y_ != y)
    {
        WaveKinematicABC::updatePhase(t, x, y, force) ;
        for(auto n = 0; n < nbwaves_; ++n)
        {
            double sn = sphase_(n);
            double cn = cphase_(n);
            for (auto m = n; m < nbwaves_; ++m)
            {
                auto i = indexOffset_(n) +m;
                double sm = sphase_(m);
                double cm = cphase_(m);
                double snsm = sn *sm;
                double cncm = cn *cm;
                double sncm = sn *cm;
                double cnsm = cn *sm;
                sphaseP_(i) = sncm +cnsm;
                sphaseM_(i) = sncm -cnsm;
                cphaseP_(i) = cncm -snsm;
                cphaseM_(i) = cncm +snsm;
            }
        }
    }
}

void SecondOrderKinematic::evaluateZ2(double z, int force)
{
    /*
     * eq. (5) in "Incident_Second_Order_eqs.pdf" applied for k^+ and k^-
     *
     * Infinite depth:
     *   k^+
     *      zP_   :       e^{k_i^+ z}
     *      zzP_  : k_i^+ e^{k_i^+ z}
     *   k^-
     *      zM_   :       e^{k_i^- z}
     *      zzM_  : k_i^- e^{k_i^- z}
     * Finite depth:
     *   k^+
     *      zP_   :       \cosh(k_i^+(z+h)) /\cosh(k_i^+ h)
     *      zzP_  : k_i^+ \sinh(k_i^+(z+h)) /\cosh(k_i^+ h)
     *   k^-
     *      zM_   :       \cosh(k_i^-(z+h)) /\cosh(k_i^- h)
     *      zzM_  : k_i^- \sinh(k_i^-(z+h)) /\cosh(k_i^- h)
     */
    if ( (force) || (z2_ != z))    // Z+/-_ and Zz+/-_ were not calculated yet
    {
        z2_ = z ;
        if (z2_ > 0.0)       // above the mean free surface
        {
            for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
            {
                zP_(i)  = 0.0;
                zM_(i)  = 0.0;
                zzP_(i) = 0.0;
                zzM_(i) = 0.0;
            }
            return ;
        }
        // z2_ <= 0.0
        if (depth_ > 0.0)   // finite depth case
        {
            if (z2_ == 0.0)  // particular case
            {
                for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
                {
                    zP_(i) = 1.0;
                    zM_(i) = 1.0;
                    zzP_(i) = kiP_(i) *tanhkhP_(i) ;    // k^+ \tanh k^+h
                    zzM_(i) = kiM_(i) *tanhkhM_(i) ;    // k^- \tanh k^-h
                }
                return ;
            }
            // z2_ < 0.0
            auto h2 = -depth_ -depth_;                                          //    -2h
            auto z2h = -z2_ +h2;                                                // -z -2h
            for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
            {
                auto expkzP  = std::exp(kiP_(i) *z2_);                      // e^{ k^+z}
                auto expkzM  = std::exp(kiM_(i) *z2_);                      // e^{ k^-z}
                auto expkzhP = std::exp(kiP_(i) *z2h);                      // e^{-k^+(z+2h)}
                auto expkzhM = std::exp(kiM_(i) *z2h);                      // e^{-k^-(z+2h)}
                zP_(i)  =          (expkzP +expkzhP) *expkhP_(i);           //     \cosh k^+(z+h) /\cosh k^+h
                zM_(i)  =          (expkzM +expkzhM) *expkhM_(i);           //     \cosh k^-(z+h) /\cosh k^-h
                zzP_(i) = kiP_(i) *(expkzP -expkzhP) *expkhP_(i);           // k^+ \sinh k^+(z+h) /\cosh k^+h
                zzM_(i) = kiM_(i) *(expkzM -expkzhM) *expkhM_(i);           // k^- \sinh k^+(z+h) /\cosh k^+h
            }
            return ;
        } else  // infinite depth case
        {
            if (z2_ == 0.0)  // particular case
            {
                for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
                {
                    zP_(i)  = 1.0;
                    zM_(i)  = 1.0;
                    zzP_(i) = kiP_(i);
                    zzM_(i) = kiM_(i);
                }
                return ;
            }
            // z2_ < 0.0
            for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
            {
                auto expkzP = std::exp(kiP_(i) *z2_);                       //     e^{k^+z}
                auto expkzM = std::exp(kiM_(i) *z2_);                       //     e^{k^-z}
                zP_(i)  =          expkzP;                                  //     e^{k^+z}
                zM_(i)  =          expkzM;                                  //     e^{k^-z}
                zzP_(i) = kiP_(i) *expkzP;                                  // k^+ e^{k^+z}
                zzM_(i) = kiM_(i) *expkzM;                                  // k^- e^{k^-z}
            }
            return ;
        }
    }
}

double SecondOrderKinematic::getQ(double time, double x, double y)
{
    updatePhase(time, x, y) ;
    double q = 0.0 ;
    for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
    {
        q += ai_(i) *(qiP_(i) *sphaseP_(i) +qiM_(i) *sphaseM_(i));
    }
    return q ;
}

double SecondOrderKinematic::get2ndPotential(double time, double x, double y, double z)
{
    // eq. (23) in "Incident_Second_Order_eqs.pdf"
    updatePhase(time, x, y) ;
    evaluateZ2(z) ;
    auto phi = 0.0 ;
    for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
    {
        phi += ai_(i) *(QiP_(i) *zP_(i) *sphaseP_(i) +QiM_(i) *zM_(i) *sphaseM_(i));
    }
    phi -= Ct_ *time;
    return phi ;
}

double SecondOrderKinematic::get2ndPressure(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return 0.0;
    }
    // p^{(22)}. eq. (29)-(31) in "Incident_Second_Order_eqs.pdf"
    auto p = 0.0 ;
    updatePhase(time, x, y) ;
    evaluateZ2(z) ;
    if (mode_ < 1) {    // dif mode or dif + sum mode
        // std::cout << "pressure: dif mode\n";
        for(auto i = 0; i<indexOffset_(nbwaves_); ++i)
        {
            p += -ai_(i) * wiM_(i) *QiM_(i) *zM_(i) *cphaseM_(i);  // (31)
        }
    }
    if (mode_ > -1) {  // sum mode or dif + sum mode
        // std::cout << "pressure: sum mode\n";
        for(auto i = 0; i<indexOffset_(nbwaves_); ++i)
        {
            p += -ai_(i) *wiP_(i) *QiP_(i) *zP_(i) *cphaseP_(i);    // (30)
        }
    }
    p += Ct_ ;
    p22Saved_ = p/g_;	// pressure is defined as P/rho/g
    return p ;
}

double SecondOrderKinematic::get2ndElevation(double time, double x, double y)
{
    // \eta^{(22)}. eq. (34) in "Incident_Second_Order_eqs.pdf"
    auto p = 0.0 ;
    updatePhase(time, x, y) ;
    if (mode_ < 1) {    // dif mode or dif + sum mode
        // std::cout << "elevation: dif mode\n";
        for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
        {
            p += -ai_(i) *wiM_(i) *QiM_(i) *cphaseM_(i);
        }
    }
    if (mode_ > -1) {   // sum mode or dif + sum mode
        // std::cout << "elevation: sum mode\n";
        for(auto i = 0; i < indexOffset_(nbwaves_); ++i)
        {
            p += -ai_(i) *wiP_(i) *QiP_(i) *cphaseP_(i);
        }
    }
    p += Ct_ ;
    eta22Saved_ = p/g_;
    return eta22Saved_;
}

Eigen::Vector3d SecondOrderKinematic::get2ndVelocity(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
    // eqs. (35), (37)-(39) in "Incident_Second_Order_eqs.pdf"
    updatePhase(time, x, y) ;
    evaluateZ2(z) ;
    Eigen::Vector3d vel(0.0, 0.0, 0.0) ;
    for(auto n = 0; n < nbwaves_; ++n)
    {
        for(auto m = n; m < nbwaves_; ++m)
        {
            auto i = indexOffset_(n) +m;
            vel(0) += ai_(i) *(-QiP_(i) *zP_(i) *cphaseP_(i) *(kchead_(n) +kchead_(m))
                               -QiM_(i) *zM_(i) *cphaseM_(i) *(kchead_(n) -kchead_(m)));        // (37)
            vel(1) += ai_(i) *(-QiP_(i) *zP_(i) *cphaseP_(i) *(kshead_(n) +kshead_(m))
                               -QiM_(i) *zM_(i) *cphaseM_(i) *(kshead_(n) -kshead_(m)));        // (38)
            vel(2) += ai_(i) *( QiP_(i) *zzP_(i) *sphaseP_(i)
                               +QiM_(i) *zzM_(i) *sphaseM_(i));                                 // (39)
        }
    }
    return vel ;
}

Eigen::Vector3d SecondOrderKinematic::get2ndAcceleration(double time, double x, double y, double z, double eta, bool useConstrain)
{
    if (useConstrain)
    {
        if (z > eta)
            return Eigen::Vector3d(0.0, 0.0, 0.0);
    }
    // eqs. (35), (37)-(39) in "Incident_Second_Order_eqs.pdf"
    updatePhase(time, x, y) ;
    evaluateZ2(z) ;
    Eigen::Vector3d acc(0.0, 0.0, 0.0);
    for(auto n = 0; n < nbwaves_; ++n)
    {
        for(auto m = n; m < nbwaves_; ++m)
        {
            auto i = indexOffset_(n) +m;
            acc(0) -= ai_(i) *(-QiP_(i) *zP_(i) *sphaseP_(i) *wiP_(i) *(kchead_(n) +kchead_(m))
                               -QiM_(i) *zM_(i) *sphaseM_(i) *wiM_(i) *(kchead_(n) -kchead_(m)));        // (37)
            acc(1) -= ai_(i) *(-QiP_(i) *zP_(i) *sphaseP_(i) *wiP_(i) *(kshead_(n) +kshead_(m))
                               -QiM_(i) *zM_(i) *sphaseM_(i) *wiM_(i) *(kshead_(n) -kshead_(m)));        // (38)
            acc(2) += ai_(i) *( QiP_(i) *zzP_(i) *cphaseP_(i) *wiP_(i)
                               +QiM_(i) *zzM_(i) *cphaseM_(i) *wiM_(i));                                 // (39)
        }
    }
    return acc;
}

// functions for the debug and verification purposes
double SecondOrderKinematic::getQ_(double t, double x, double y)
{
    // Q = -2\nabla \Phi \nabla \Phi_t +(\Phi_zz +\Phi_zzt/g)\Phi_t
    // taking into account, that
    //    \nabla \Phi = (u, v, w) and
    //    \Phi_t = -p we have
    // Q = -2 (u,v,w) \nabla \Phi_t -(\Phi_zz +\Phi_zzt/g) p
    Eigen::Vector3d u = get1stVelocity(t, x, y, 0.0) ;
    const Eigen::RowVectorXd amp = p_wif_->getAmplitudes().transpose() ;
    double p = -get1stPressure(t, x, y, 0.0) ;
    double phi_xt  = -amp *static_cast<Eigen::VectorXd>(g_ *kchead_ *sphase_) ;
    double phi_yt  = -amp *static_cast<Eigen::VectorXd>(g_ *kshead_ *sphase_) ;
    double phi_zt  = get1stPhiztAt0(t, x, y) ;
    double phi_zz  = -amp *static_cast<Eigen::VectorXd>(g_ *p_wif_->getWaveNumbers().pow(2) /p_wif_->getFrequencies() *sphase_) ;
    double phi_zzt = amp *static_cast<Eigen::VectorXd>(p_wif_->getFrequencies().pow(3) *sphase_) ;
    return -2.0 *(u(0) *phi_xt +u(1) *phi_yt +u(2) *phi_zt) +(phi_zz +phi_zzt/g_) *p ;
}

void SecondOrderKinematic::printInfo(int n, int m, int inm, int imin, int imax) const
{
    int i;
    if (inm == -1)
        i = indexOffset_(n) +m;
    else
        i = inm;
    if (imin == -1)
        imin = 0;
    if (imax == -1)
        imax = indexOffset_(nbwaves_);
    if ((imin <= i) && (i <= imax)) {
        std::cout << "mode_=" << mode_;
        std::cout << "\ti = " << i;
        std::cout << "\tn = " << n;
        std::cout << "\tm = " << m;
        std::cout << "\tai_:" << ai_(i);
        std::cout << "\twiP_:" << wiP_(i);
        std::cout << "\twiM_:" << wiM_(i);
        std::cout << "\tkiP_:" << kiP_(i);
        std::cout << "\tkiM_:" << kiM_(i);
        std::cout << "\tQiP_:" << QiP_(i);
        std::cout << "\tQiM_:" << QiM_(i);
        std::cout << "\tzP_:" << zP_(i);
        std::cout << "\tzM_:" << zM_(i);
        std::cout << "\tzzP_:" << zzP_(i);
        std::cout << "\tzzM_:" << zzM_(i);
        std::cout << "\tcphaseP_:" << cphaseP_(i);
        std::cout << "\tcphaseM_:" << cphaseM_(i);
        std::cout << "\tsphaseP_:" << sphaseP_(i);
        std::cout << "\tsphaseM_:" << sphaseM_(i);
        std::cout << "\tkchead_n:" << kchead_(n);
        std::cout << "\tkchead_m:" << kchead_(m);
        std::cout << "\tkshead_n:" << kshead_(n);
        std::cout << "\tkshead_m:" << kshead_(m);
        std::cout << "\twif.k_n:" << p_wif_->getWaveNumbers()(n);
        std::cout << "\twif.k_m:" << p_wif_->getWaveNumbers()(m);
        std::cout << "\twif.w_n:" << p_wif_->getFrequencies()(n);
        std::cout << "\twif.w_m:" << p_wif_->getFrequencies()(m);
        std::cout << "\n";
    }
}

Eigen::ArrayXXd SecondOrderKinematic::getQi(int mode) const
{
    Eigen::ArrayXXd Qi = Eigen::ArrayXXd::Zero(nbwaves_, nbwaves_);
    if (mode < 0)
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Qi(n, m - n) = QiM_(indexOffset_(n) +m);
    }
    else
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Qi(n, m - n) = QiP_(indexOffset_(n) +m);
    }
    return Qi;
}

Eigen::ArrayXXd SecondOrderKinematic::getWi(int mode) const
{
    Eigen::ArrayXXd Wi = Eigen::ArrayXXd::Zero(nbwaves_, nbwaves_);
    if (mode < 0)
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Wi(n, m - n) = wiM_(indexOffset_(n) + m);
    }
    else
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Wi(n, m - n) = wiP_(indexOffset_(n) + m);
    }
    return Wi;
}

Eigen::ArrayXXd SecondOrderKinematic::getKi(int mode) const
{
    Eigen::ArrayXXd Ki = Eigen::ArrayXXd::Zero(nbwaves_, nbwaves_);
    if (mode < 0)
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Ki(n, m - n) = kiM_(indexOffset_(n) + m);
    }
    else
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Ki(n, m - n) = kiP_(indexOffset_(n) + m);
    }
    return Ki;
}

Eigen::ArrayXXd SecondOrderKinematic::getKix(int mode) const
{
    Eigen::ArrayXXd Kix = Eigen::ArrayXXd::Zero(nbwaves_, nbwaves_);
    const auto k = p_wif_->getWaveNumbers();
    const auto chead = p_wif_->getCosHeadings();
    if (mode < 0)
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Kix(n, m - n) = k(n) *chead(n) - k(m) *chead(m);
    }
    else
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = 0; m < nbwaves_; ++m)
                Kix(n, m - n) = k(n) *chead(n) + k(m) *chead(m);
    }
    return Kix;
}

Eigen::ArrayXXd SecondOrderKinematic::getKiy(int mode) const
{
    Eigen::ArrayXXd Kiy = Eigen::ArrayXXd::Zero(nbwaves_, nbwaves_);
    const auto k = p_wif_->getWaveNumbers();
    const auto shead = p_wif_->getSinHeadings();
    if (mode < 0)
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = n; m < nbwaves_; ++m)
                Kiy(n, m - n) = k(n) *shead(n) - k(m) *shead(m);
    }
    else
    {
        for (auto n = 0; n < nbwaves_; ++n)
            for (auto m = 0; m < nbwaves_; ++m)
                Kiy(n, m - n) = k(n) *shead(n) + k(m) *shead(m);
    }
    return Kiy;
}
