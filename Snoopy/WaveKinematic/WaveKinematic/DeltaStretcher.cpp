#include "WaveKinematic/DeltaStretcher.hpp"

using namespace BV::WaveKinematic;

double DeltaStretching::stretcher(double z, double eta, double smallDDelta)
{
    // smallDDelta: for hDelta_ less than this value, it is assumed that hDelta_ is small
    //              otherwise hDelta_ is assumed to be large
    // z_Delta = (z+d_Delta)\frac{d_Delta +\Delta \eta}{d_Delta +\eta} -d_\Delta for z > -d_\Delta and \eta > 0
    // z_Delta = z otherwise
    if (inCrestOnly_ && eta <= 0.0)
        return z;
    // inCrestOnly_ = false or inCrestOnly_ = true && eta > 0.0
    if (hDelta_ > 0.0)  // Finite case
    {
        if (z <= -hDelta_)
            return z;
        // z > -hDelta
        if (hDelta_ < smallDDelta)
            return (z +hDelta_)*(hDelta_ +delta_*eta)/(hDelta_ +eta) -hDelta_;
        else
            return ((z - eta) +(1.0 +z / hDelta_)*delta_*eta ) /(1.0 +eta/hDelta_);
    }

    return z -eta +eta *delta_;
}

Eigen::Vector3d DeltaStretching::getVelocity(double time, double x, double y, double z)
{
    get1stElevation(time, x, y);    // evaluate the free surface elevation. It automatically checks whether there is a need to recalculate it
    if (z > eta1Saved_)
        return Eigen::Vector3d(0., 0., 0.); // No velocity above the free surface
    // eta > 0 => Crest
    double zs = stretcher(z, eta1Saved_);
    if (zs > 0.0)   // 0.0 < zs < eta => u(x,y,z;t) = u(x,y,0;t) +z du/dz(x,y,0;t)
        return get1stVelocity(time, x, y, -1.e-10, eta1Saved_, false) +zs*get1stVelocityDzAt0(time, x, y);
    // zs < 0.0 => return velocity at this point
    return get1stVelocity(time, x, y, zs, eta1Saved_, false);   // no need to check the constrains
}

Eigen::Vector3d DeltaStretching::getAcceleration(double time, double x, double y, double z)
{
    return Eigen::Vector3d(0., 0., 0.);
}
