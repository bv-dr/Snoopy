#pragma once

#include <math.h>

#include "WaveKinematicExport.hpp"
#include "WaveKinematic/SecondOrderKinematic21.hpp"
#include "Spectral/Wif.hpp"

namespace BV
{
namespace WaveKinematic
{

class WAVE_KINEMATIC_API SecondOrderKinematic : public SecondOrderKinematic21
{
private:
    /*!
     * Call in the constructor to initialize the class attributes
     */
    void initialize_();
    /*!
     * \brief nbwaves_ : size of the waves. Even it can be evaluated from the size of the array from wif_,
     * it is convenient to have it as separate variable
     */
    int nbwaves_;
    /*!
     * indexOffset_ corresponds to the offset for (n,*) row of indices:
     * (n,m) <=> indexOffset_(n) +m, n:0,N-1; m:n,N-1
     *                       0      1      2      3         m     m+1            N-2        N-1
     * indexOffset_(0)   : (0,0)  (0,1)  (0,2)  (0,3) ... (0,m)  (0,m+1) ... (  0,N-2)  (  0,N-1)   : N-0 elements
     *                              N     N+1    N+2       N+m    N+m+1         2N-2       2N-2
     * indexOffset_(1)   :        (1,1)  (1,2)  (1,3) ... (1,m)  (1,m+1) ... (  1,N-2)  (  1,N-1)   : N-1 elements
     *                                    2N-1   2N      2N-3+m  2N-2+m         3N-5       3N-4
     * indexOffset_(2)   :               (2,2)  (2,3) ... (2,m)  (2,m+1) ... (  2,N-2)  (  2,N-1)   : N-2 elements
     * ...
     * indexOffset_(m)   :                                (m,m)  (m,m+1) ... (  m,N-2)  (  m,N-1)   : N-m elements
     * ...
     *                                                                    (N^2-N)/2+N-3  (N^2-N)/2+N-2
     * indexOffset_(N-2) :                                                   (N-2,N-2)  (N-2,N-1)   :   2 elements
     *                                                                               (N^2-N)/2+N-1
     * indexOffset_(N-1) :                                                              (N-1,N-1)   :   1 element
     * indexOffset_(0) = 0
     * indexOffset_(n) = indexOffset_(n-1) +N -n
     *                 = n*(N-n) +n(n-1)/2
     *                 = nN      -n(n+1)/2
     *
     * indexOffset_(N) = Total number of elements
     */
    Eigen::ArrayXi indexOffset_;

    /*!
     * \brief ai_ : array of \f$a_{n(i)} *a_{m(i)}\f$
     */
    Eigen::ArrayXd ai_ ;
    /*!
     * \brief wiP_ : \f$\omega_{i(n,m)}^+ = \omega_n +\omega_m\f$
     *
     * eq. (22) in "Incident_Second_Order_eqs.pdf"
     */
    Eigen::ArrayXd wiP_ ;
    /*!
     * \brief wiM_ : \f$\omega_{i(n,m)}^- = \omega_n -\omega_m\f$
     *
     * eq. (22) in "Incident_Second_Order_eqs.pdf"
     */
    Eigen::ArrayXd wiM_ ;
    /*!
     * \brief kiP_ : \f$k_i^+(n,m)\f$
     *
     * eq. (22) in "Incident_Second_Order_eqs.pdf". Defined for n=1,N; m=n,N
     */
    Eigen::ArrayXd kiP_ ;
    /*!
     * \brief kiM_ : \f$k_i^-(n,m)\f$
     *
     * eq. (22) in "Incident_Second_Order_eqs.pdf". Defined for n=1,N; m=n,N
     */
    Eigen::ArrayXd kiM_ ;
    /*!
     * \brief tanhkhP_ : \f$\tanh k^+h\f$
     */
    Eigen::ArrayXd tanhkhP_ ;
    /*!
     * \brief tanhkhM_ : \f$\tanh k^-h\f$
     */
    Eigen::ArrayXd tanhkhM_ ;
    /*!
     * \brief expkhP_ : \f$\frac{1}{1+e^{-2k^+h}}\f$. Precalculated value which is used for evaluation
     * of the functions Z
     */
    Eigen::ArrayXd expkhP_;
    /*!
     * \brief expkhM_ : \f$\frac{1}{1+e^{-2k^-h}}\f$. Precalculated value which is used for evaluation
     * of the functions Z
     */
    Eigen::ArrayXd expkhM_;
    /*!
     * \brief Ct_ : set-down
     *
     * eq. (26) in "Incident_Second_Order_eqs.pdf"
     */
    double Ct_ ;
    /*!
     * \brief qiP_ : \f$q_{i(n,m)}^+\f$
     *
     * eq. (19) in "Incident_Second_Order_eqs.pdf"
     *
     * It is only used for the calculation of \f$Q\f$
     */
    Eigen::ArrayXXd qiP_ ;
    /*!
     * \brief qiM_ : \f$q_{i(n,m)}^-\f$
     *
     * eq. (19) in "Incident_Second_Order_eqs.pdf"
     *
     * It is only used for the calculation of \f$Q\f$
     */
    Eigen::ArrayXXd qiM_ ;
    /*!
     * \brief QiP_ : \f$a_{i(n,m)}Q_{i(n,m)}^+\f$ for \f$n=0,1,2,...,N-1\f$ and \f$m=n,n+1,...,N-1\f$, where \f$N\f$ is number of waves
     *
     * eq. (24) in "Incident_Second_Order_eqs.pdf". Defined for n=1,N; m=n,N. eq. ()
     */
    Eigen::ArrayXd QiP_;
    /*!
     * \brief QiM_ : \f$a_{i(n,m)}Q_{i(n,m)}^-\f$ for \f$n=0,1,2,...,N-1\f$ and \f$m=n,n+1,...,N-1\f$, where \f$N\f$ is number of waves
     *
     * eq. (24) in "Incident_Second_Order_eqs.pdf". Defined for n=1,N; m=n,N. eq. ()
     */
    Eigen::ArrayXd QiM_;
    /*!
     * \brief sphaseP_ : \f$\sin(\phi_{n(i)} +\phi_{m(i)})\f$ at (t, x, y)
     */
    Eigen::ArrayXd sphaseP_ ;
    /*!
     * \brief sphaseM_: \f$\sin(\phi_{n(i)} -\phi_{m(i)})\f$ at (t, x, y)
     */
    Eigen::ArrayXd sphaseM_ ;
    /*!
     * \brief sphasePlus_ : \f$\sin(\phi_n +\phi_m)\f$ at (t, x, y)
     */
    /*!
     * \brief cphaseP_ : \f$\cos(\phi_{n(i)} +\phi_{m(i)})\f$ at (t, x, y)
     */
    Eigen::ArrayXd cphaseP_ ;
    /*!
     * \brief cphaseM_: \f$\cos(\phi_{n(i)} -\phi_{m(i)})\f$ at (t, x, y)
     */
    Eigen::ArrayXd cphaseM_ ;
    /*!
     * \brief evaluateZ2(z, force) : similar to WaveKinematicABC::evaluateZ(z, force), but for the \f$k^+\f$ and \f$k^-\f$
     * \param[in] z : z coordinate
     * \param[in] force : if force > 0, then the values recalculated even z2_ == z
     */
    void evaluateZ2(double z, int force = 0) ;
    /*!
     * \brief z2_ : last z value at which Z2(z) was evaluated
     */
    double z2_ ;
    /*!
     * \brief zP_ : results of Z2(z) for \f$k^+\f$
     */
    Eigen::ArrayXd zP_ ;
    /*!
     * \brief zM_ : results of Z2(z) for \f$k^-\f$
     */
    Eigen::ArrayXd zM_ ;
    /*!
     * \brief zzP_ : results of Z2(z) for \f$k^+\f$, derivatives
     */
    Eigen::ArrayXd zzP_ ;
    /*!
     * \brief zzM_ : results of Z2(z) for \f$k^-\f$, derivatives
     */
    Eigen::ArrayXd zzM_ ;
    /*!
     * \brief eta22Saved_ : the second order free surface \f$\eta^{(22)}\f$
     * This value is saved in this variable. use getElevation22() method to retrieve it.
     */
    double eta22Saved_ ;
    /*!
     * \brief p22Saved_ : the second order pressure \f$\p^{(22)}\f$
     * This value is saved in thie variable. use getPressure22() method to get it.
     */
    double p22Saved_ ;
    /*!
     * \brief mode_ : -1 => dif mode, +1 => sum mode, 0 => dif + sum mode
     * This value is set by setMode(mode = 0)
     */
    int mode_;
    // some functions for debug and verification purpose
    double getQ_(double t, double x, double y) ;
    /*!
     * \brief printInfo: For debug purpose, it prints out the variables' values
     * \param[in] n
     * \param[in] m
     * \param[in] inm = -1. If inm != -1, then n and m are ignored, else, i = indexOffset_(n) +m
     * \param[in] imin <= i. if imin == -1 => imin == 0
     * \param[in] imax >= i. if imax == -1 => imax == max(i)
     */
    void printInfo(int n, int m, int inm = -1, int imin = -1, int imax = -1) const;
protected:
    /*!
     * \brief updates the phases at time instant t and point (x, y)
     * \param[in] t : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \param[in] force = 0 : if force == 1, then phase is updated by ignoring the comparison with the previous time and position
     *
     * This is overriden function. It calls the original one to calculate the phases and
     * the sines and cosines of the sum and difference phases
     */
    void updatePhase(double t, double x, double y, bool force = 0) override ;
    /*!
     * \brief Getting Q using double sum
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \return Q
     */
    double getQ(double time, double x, double y) ;
    /*!
     * \brief get2ndPotential
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \param[in] z : z coordinate
     * \return \f$\Phi^{(2)}\f$
     *
     * eq. (23) in "Incident_Second_Order_eqs.pdf"
     */
    double get2ndPotential(double time, double x, double y, double z) ;
    /*!
     * \brief get2ndPressure : calculates the second order pressure due to the second order quantities
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \param[in] z : z coordinate
     * \param[in] eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * \param[in] useConstrain : by default false. If this parameter is true, the constrain z > eta is used.
     *       otherwise pressure is calculated at any point
     * \returns \f$p^{(2)} = -\Phi^{(2)}_t\f$
     *
     * eqs. (29)-(31) in "Incident_Second_Order_eqs.pdf"
     */
    double get2ndPressure(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = true) override;
    /*!
     * \brief get2ndElevation : calculates the free surface elevation of the second order due to the second order quantities
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \return \f$\displaystyle -\frac{1}{g}\Phi_{t}^{(2)}\f$ at \f$z = 0\f$
     *
     * eq. (34) in "Incident_Second_Order_eqs.pdf"
     */
    double get2ndElevation(double time, double x, double y) override;
    /*!
     * \brief Returns a vector of the first order velocities at (x, y, z) at time instant t
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \param[in] z : z coordinate
     * \return Eigen::Vector3d(u_x^{(2)}, u_y^{(2)}, u_z^{(2)})
     *
     * eqs. (35), (37)-(39) in "Incident_Second_Order_eqs.pdf"
     */
    Eigen::Vector3d get2ndVelocity(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = true) ;

    Eigen::Vector3d get2ndAcceleration(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = true);

    using SecondOrderKinematic21::get21Velocity ;
public:
    SecondOrderKinematic(std::shared_ptr<Spectral::Wif> p_wif, unsigned int numThreads = 1) :
        SecondOrderKinematic21(p_wif, numThreads)
    {
        isWheeler_ = false ;
        initialize_();
    }

    SecondOrderKinematic(std::shared_ptr<Spectral::Wifm> p_wifm, unsigned int numThreads = 1) :
        SecondOrderKinematic21(p_wifm, numThreads)
    {
        isWheeler_ = false ;
        initialize_();
    }

    ~SecondOrderKinematic()
    {

    }

    using SecondOrderKinematic21::getVelocity ;
    using SecondOrderKinematic21::getAcceleration;
    using SecondOrderKinematic21::getPressure ;
    using SecondOrderKinematic21::getElevation ;

    void setMode(int mode = 0) {
        mode_ = mode;
    }

    Eigen::Vector3d getVelocity(double time, double x, double y, double z) override
    {
        double eta(getElevation(time, x, y));
        if (z > eta) return Eigen::Vector3d(0.0, 0.0, 0.0);

        if (z <= 0.0)
            return get1stVelocity(time, x, y, z, 0.0, false) +get2ndVelocity(time, x, y, z, 0.0, false) ;
        // 0 < z <= eta_1_ =>
        /*!
         * from (5) of Kinematics Under Extreme Waves:
         * \f$u^{(tot)} = u_0 +\frac{\partial u^{(1)}}{\partial z}\Bigr|_{z=0} +u^{(2)}(0),\mbox{ for } z>0\f$
         */
        auto vel21 = SecondOrderKinematic21::get21Velocity(time, x, y, z, 0.0, false);    // no need to check whether z > eta, already done
        return vel21 +get2ndVelocity(time, x, y, 0.0, 0.0, false);
    }

    Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override
    {
        double eta(getElevation(time, x, y));
        if (z > eta) return Eigen::Vector3d(0.0, 0.0, 0.0);

        if (z <= 0.0)
            return get1stAcceleration(time, x, y, z, 0.0, false) +get2ndAcceleration(time, x, y, z, 0.0, false) ;
        // 0 < z <= eta_1_ =>
        auto acc21 = SecondOrderKinematic21::get21Acceleration(time, x, y, z, 0.0, false);    // no need to check whether z > eta, already done
        return acc21 +get2ndAcceleration(time, x, y, 0.0, 0.0, false);
    }

    /*!
     * \brief getPressure: calculates the total pressure: \f$p^{(1)} +p^{(21)} +p^{(22)}\f$
     * \param[in] time
     * \param[in] x
     * \param[in] y
     * \param[in] z
     * \return the total pressure up to the second order
     */
    double getPressure(double time, double x, double y, double z) override
    {
        double eta(getElevation(time, x, y));
        if (z > eta)
        {
            p1Saved_ = 0.0;
            p21Saved_ = 0.0;
            p22Saved_ = 0.0;
            return 0.0;    // above the free surface, pressure is zero
        }

        // no need to check if z > eta anymore
        get1stPressure(time, x, y, z, 0.0, false);                              // p1, saves automatically to p1Saved_
        SecondOrderKinematic21::get2ndPressure(time, x, y, z, 0.0, false) ;     // p21, saves automatically to p21Saved_
        // the call SecondOrderKinematic21::getPressure(time, x, y, z) ; is not good, because
        // the get2ndPressure in this method will be replaced by SecondOrderKinematic::get2ndPressure(..)
        get2ndPressure(time, x, y, z, 0.0, false) ;                             // saves automatically the p22 to p22Saved_
        return p1Saved_ +p21Saved_ +p22Saved_;
    }

    /*!
     * \brief getElevation: calculates the total free surface elevation: \f$\eta^{(1)} +\eta^{(21)} +\eta^{(22)}\f$
     * \param[in] time
     * \param[in] x
     * \param[in] y
     * \return the total free surface elevation up to the second order
     */
    double getElevation(double time, double x, double y, double /*speed*/ = 0., bool /*update*/ = true) override
    {
        if (isEPointDifferent(time, x, y)) {
            SecondOrderKinematic21::get2ndElevation(time, x, y) ;   // it calculates eta21 (saved to the eta21Saved_) and eta1 (saved to the eta1Saved_)
            get2ndElevation(time, x, y) ;                           // it calculates eta22 (saved to the eta22Saved_)
            saveEPoint(time, x, y);
        }
        return  eta1Saved_ +eta21Saved_ +eta22Saved_;
    }

    /*!
     * \brief getElevation22
     * \return second order free surface elevation \f$\eta^{(22)}\f$
     */
    double getElevation22()
    {
        return eta22Saved_ ;
    }
    double getPressure22()
    {
        return p22Saved_ ;
    }
    Eigen::ArrayXXd getQi(int mode = -1) const;

    Eigen::ArrayXXd getWi(int mode = -1) const;

    Eigen::ArrayXXd getKi(int mode = -1) const;

    Eigen::ArrayXXd getKix(int mode = -1) const;

    Eigen::ArrayXXd getKiy(int mode = -1) const;

};
}
}
