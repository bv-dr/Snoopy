#include "WaveKinematic/Wheeler.hpp"

using namespace BV::WaveKinematic;

double Wheeler::stretcher(double z, double eta)
{
    if (inCrestOnly_&& eta <= 0.0)
        return z;
    // inCrestOnly == false or (inCrestOnly_== true and eta > 0)
    if (depth_ > 0.0)
        return (z - eta) / (1.0 + eta / depth_);

    return z - eta;
}
