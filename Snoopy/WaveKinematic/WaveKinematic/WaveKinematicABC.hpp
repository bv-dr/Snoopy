#pragma once

#include "WaveKinematicExport.hpp"

#include "Spectral/Wif.hpp"

#include <Eigen/Dense>
#include <iostream>


namespace BV
{
namespace WaveKinematic
{

/**
 * @class WaveKinematicABC
 *
 * @brief Base class for FirstOrderKinematic and SecondKinematic.
 *
 * Phase convention used in this module:
 * \f[
 *  \eta = -\frac {1}{g}\phi_t(z=0) = Re\{ \sum a_n e^{i(\omega_n t -k_n x +\phi_{n0})} \}
                                    = \sum a_n \cos(\omega_n t -k_n x +\phi_{n0}),
 *  where \phi_{n0} are initial phases
 * \f]
 * \f[
 *  \phi = Re\{ \sum a_n i \frac{ g }{ \omega_n } \frac{\cosh[k_n(z+h)]}{\cosh k_n h} e^{i(\omega_n t -k_n x +\phi_{n0})} \}
         = - \sum a_n \frac{g}{\omega_n} \frac{\cosh[k_n(z+h)]}{\cosh k_n h} \sin(\omega_n t -k_n x +\phi_{n0})
 * \f]
 * \f[
 *  u_x = \phi_x = Re\{ \sum a_n \frac{ gk_n\cos\beta_n} {\omega_n} \frac{\cosh[k_n(z+h)]}{\cosh k_n h} e^{i(\omega_n t -k_n x +\phi_{n0})} \}
                 = \sum a_n \frac{ gk_n\cos\beta_n }{\omega_n} \frac{\cosh[k_n(z+h)]}{\cosh k_n h} \cos(\omega_n t -k_n x +\phi_{n0})
 * \f]
 * \f[
 *  u_y = \phi_y = Re\{ \sum a_n \frac{ gk_n\sin\beta_n} {\omega_n} \frac{\cosh[k_n(z+h)]}{\cosh k_n h} e^{i(\omega_n t -k_n x +\phi_{n0})} \}
                 = \sum a_n \frac{ gk_n\sin\beta_n }{\omega_n} \frac{\cosh[k_n(z+h)]}{\cosh k_n h} \cos(\omega_n t -k_n x +\phi_{n0})
 * \f]
 * \f[
 *  u_z = \phi_z = Re\{ \sum a_n i \frac{ gk_n } {\omega_n}\frac{\sinh[k_n(z+h)]}{\cosh k_n h} e^{i(\omega_n t -k_n x +\phi_{n0})} \}
                 = -\sum a_n \frac{ gk_n }{\omega_n} \frac{\sinh[k_n(z+h)]}{\cosh k_n h} \sin(\omega_n t -k_n x +\phi_{n0})
 * \f]
 * \f[
 *  p = -\phi_t = Re\{ \sum a_n g \frac{\cosh[k_n(z+h)]}{\cosh k_n h} e^{i(\omega_n t -k_n x +\phi_{n0})} \}
 *              = \sun a_n g \frac{\cosh[k_n(z+h)]}{\cosh k_n h} \cos(\omega_n t -k_n x +\phi_{n0})
 * \f]
 */
class WAVE_KINEMATIC_API WaveKinematicABC
{
protected:
    std::shared_ptr<Spectral::Wif> p_wif_;
    std::shared_ptr<Spectral::Wifm> p_wifm_;
    std::size_t wifIndex_;
    bool isWheeler_;
    double depth_;
    double g_; // gravity. Probably it is defined already somewhere else. //FIXME

    /**
     * k *cos(heading)
     */
    Eigen::ArrayXd kchead_;

    /**
     * k *sin(heading)
     */
    Eigen::ArrayXd kshead_;

    /**
     * phase of the wave, which depends on the time and the position
     */
    Eigen::ArrayXd phase_;

    // Phases coordinates
    /**
     * time at which the latest phase update was performed
     */
    double time_;

    /**
     * x coordinate at which the latest phase update was performed
     */
    double x_;

    /**
     * coordinate at which the latest phase update was performed
     */
    double y_;

    // Elevation coordinates
    /**
     * time at which the latest free surface elevation was performed
     */
    double e_time_;
    /**
     * x coordinate at which the latest free surface elevation was performed
     */
    double e_x_;
    /**
     * y coordinate at which the latest free surface elevation was performed
     */
    double e_y_;
    /**
     * simple function which stores the time and (x,y) coordinates of the previous free surface calculation
     */
    void saveEPoint(double time, double x, double y) {e_time_ = time; e_x_ = x; e_y_ = y;}
    /**
     * @brief free surface elevation at e_x_, e_y_, e_time_
     */
    double eta1Saved_ ;
    /**
     * first order pressure
     */
    double p1Saved_;

    /**
     * cos (phase)
     */
    Eigen::ArrayXd cphase_;

    /**
     * sin (phase)
     */
    Eigen::ArrayXd sphase_;

    // The following variables are used for the Linear Kinematics
    /**
     * gw_ : g/w for each frequency
     */
    Eigen::ArrayXd gw_;

    /**
     * z_ : z coordinate
     */
    double z_;

    /**
     * amplitude with taken into account the z_
     * \f$ = e^{kz} \f$ for the infinite depth case and
     * \f$ = \cosh[k(z+h)]/\cosh kh \f$ for the finite depth case
     */
    Eigen::VectorXd ampAtZ_;

    /**
     * derivative of amptAtZ_ with respect to z_
     * \f$ = k e^{kz} \f$ for the infinite depth case and
     * \f$ = k \sinh[k(z+h)]/\cosh kh \f$ for the finite depth case
     */
    Eigen::VectorXd ampAtZz_;

    /**
     * EvaluateAmpAtZ
     * @param z
     */
    void evaluateAmpAtZ(double z);

    /**
     * amplitude and Z(z) product at z = 0
     */
    Eigen::VectorXd ampAtZ0_;
    /**
     * amplitude and Z'(z) product at z = 0
     */
    Eigen::VectorXd ampAtZ0z_;
    /**
     * amplitude and Z''(z) product at z = 0
     */
    Eigen::VectorXd ampAtZ0zz_;
    /**
     * Evaluate amptAtZ0_, ampAtZ0z_ and ampAtZ0zz_;
     */
    void evaluateAmpAtZ0();

    /**
     * \f$\tanh kh\f$
     */
    Eigen::ArrayXd tanhkh_;

    /**
     * Z(z, force) : calculates \f$\displaystyle\frac{\cosh[k(z+h)]}{\cosh kh}\f$ for the finite depth case of \f$e^{-kz}\f$ for the deep case,
     * and their derivatives. The values are stored in Z_ and Zz_, respectively
     * @param z z coordinate
     * @param force if force != 0, then the Z_ and Zz_ vectors are recalculated
     *
     * Eq. (5) in "Incident_Second_Order_eqs.pdf".
     */
    void evaluateZ(double z, int force = 0);

    /**
     * Z_ : Z(z) function's result
     */
    Eigen::ArrayXd Z_;

    /**
     * Zz_ : derivative of Z(z) function's result
     */
    Eigen::ArrayXd Zz_;

    /**
     * Return true if either time (t) or horizontal position (x,y) is different from the previous calculation
     * @param t : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     */
    bool isPointDifferent(double t, double x, double y, double tol = 1.e-10) const { return std::abs(time_-t) > tol || std::abs(x_-x) > tol || std::abs(y_-y) > tol; }
    /**
     * Return true if either time (t) or horizontal position (x,y) is different from the previous free surface elevation calculation
     * @param t : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     */
    bool isEPointDifferent(double t, double x, double y, double tol = 1.e-10) const { return std::abs(e_time_-t) > tol || std::abs(e_x_-x) > tol || std::abs(e_y_-y) > tol; }

    /**
     * Return true if the time (t) is different from the previous calculation
     * @param t : time instant t
     */
    bool isTimeDifferent(double t, double tol = 1.e-10) const { return std::abs(time_-t) > tol; }

    /**
     * if ignoreFreeSurface_ is true, then velocity, acceleration, potential and pressure are caclulated for z <=0
     * otherwise, for z <= \\eta_1
     */
    bool ignoreFreeSurface_;

    /**
     * Update the phase at a given time and position
     * @param t The time instant
     * @param x The position X coordinate
     * @param y The position Y coordinate
     * @param force If true, update the phase even if time and position have not changed
     */
    virtual void updatePhase(double t, double x, double y, bool force = false);

    // The following functions are used to evaluate the first order quantities
    /**
     * Returns the first order velocity potential at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @param eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * @param useConstrain : by default false. If this parameter is true, the constrain z > eta is used.
     *       otherwise potential is calculated at any point
     * @return \phi^{(1)}(x,y,z,t)
     *
     * Eq. (4) in "Incident_Second_Order_eqs.pdf".
     */
    double get1stPotential(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = false);

    /**
     * Returns a vector of the first order velocities at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @param eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * @param useConstrain : by default false. If this parameter is true, the constrain z > eta is used.
     *       otherwise velocity is calculated at any point
     * @return Eigen::Vector3d(u_x^{(1)}, u_y^{(1)}, u_z^{(1)})
     */
    Eigen::Vector3d get1stVelocity(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = false);

    /**
     * Returns a vector of \partial u(x,y,0)/\partial z at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @return Eigen::Vector3d(dudz_x^{(1)}(x, y, 0), dudz_y^{(1)}(x, y, 0), dudz_z^{(1)})
     */
    Eigen::Vector3d get1stVelocityDzAt0(double time, double x, double y);

    /**
     * Returns a vector of the first order acceleration at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @param eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * @param useConstrain : by default false. If this parameter is true, the constrain z > eta is used.
     *       otherwise acceleration is calculated at any point
     */
    Eigen::Vector3d get1stAcceleration(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = false);

    /**
     * Calculates the derivative of the velocity potential with respect to time and z at mean free surface
     * @param time
     * @param x
     * @param y
     * @return \f$\displaystyle \Phi_{zt}(x,y,0,t)\f$
     */
    double get1stPhiztAt0(double time, double x, double y);

    /**
     * Returns the first order pressure at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @param eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * @param useConstrain : by default false. If this parameter is true, the constrain z > eta is used.
     *       otherwise pressure is calculated at any point
     * @return p^{(1)}(x,y,z,t)
     */
    virtual double get1stPressure(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = false);

    /**
     * Get the first order elevation a given time and position
     * @param time The time instant
     * @param x The position X coordinate
     * @param y The position Y coordinate
     * @param update If true, update the internal phase
     * @return \eta^{(1)}(x,y,t)
     */
    virtual double get1stElevation(double time, double x, double y, bool update = true);

private:
    /**
     * Number of threads for OpenMP reconstruction (when possible)
     */
    unsigned int numThreads_;

    /**
     * Call in the constructor to initialize the class attributes.
     */
    void initialize_(bool withUpdatePhases=true);

    /**
     * Get the phase at a given time an position.
     * @param t time instant
     * @param x x coordinate
     * @param y y coordinate
     */
    inline Eigen::ArrayXd getPhase(double t, double x, double y) const;

public:
    /**
     * Abstract class for evaluating a velocity vector
     * @param wif : Spectral::Wif object, defined in Spectral/Wif.h
     */
    WaveKinematicABC(std::shared_ptr<Spectral::Wif> p_wif, unsigned int numThreads = 1) :
        p_wif_(p_wif), p_wifm_(nullptr), wifIndex_(-1),
        time_(-1.0), x_(0.0), y_(0.0), e_time_(-1.0), e_x_(0.0), e_y_(0.0),
        eta1Saved_(0.0),
        ignoreFreeSurface_(true),
        numThreads_(numThreads)
    {
        initialize_();
    }

    /**
     * Abstract class for evaluating a velocity vector
     * @param wif : Spectral::Wif object, defined in Spectral/Wif.h
     */
    WaveKinematicABC(std::shared_ptr<Spectral::Wifm> p_wifm, unsigned int numThreads = 1) :
        p_wif_(p_wifm->getWif(0.)), p_wifm_(p_wifm), wifIndex_(-1),
        time_(-1.0), x_(0.0), y_(0.0), e_time_(-1.0), e_x_(0.0), e_y_(0.0),
        eta1Saved_(0.0),
        ignoreFreeSurface_(true),
        numThreads_(numThreads)
    {
        initialize_();
    }

    WaveKinematicABC(const WaveKinematicABC & other)
    {
        if (&other == this)
        {
            return ;
        }
        if (other.p_wifm_ != nullptr)
        {
            p_wifm_ = std::make_shared<Spectral::Wifm>(*(other.p_wifm_.get())) ;
            p_wif_ = p_wifm_->getWif(0.) ;
        }
        else
        {
            p_wif_ = std::make_shared<Spectral::Wif>(*(other.p_wif_.get())) ;
            p_wifm_ = nullptr ;
        }
        wifIndex_ = other.wifIndex_ ;
        isWheeler_ = other.isWheeler_ ;
        depth_ = other.depth_ ;
        g_ = other.g_ ;
        kchead_ = other.kchead_ ;
        kshead_ = other.kshead_ ;
        phase_ = other.phase_ ;
        time_ = other.time_ ;
        x_ = other.x_ ;
        y_ = other.y_ ;
        e_time_ = other.e_time_;
        e_x_ = other.e_x_;
        e_y_ = other.e_y_;
        eta1Saved_ = other.eta1Saved_;
        cphase_ = other.cphase_ ;
        sphase_ = other.sphase_ ;
        gw_ = other.gw_ ;
        z_ = other.z_ ;
        ampAtZ_ = other.ampAtZ_ ;
        ampAtZz_ = other.ampAtZz_ ;
        tanhkh_ = other.tanhkh_ ;
        Z_ = other.Z_ ;
        Zz_ = other.Zz_ ;
        ignoreFreeSurface_ = other.ignoreFreeSurface_;
        numThreads_ = other.numThreads_ ;
    }

    virtual ~WaveKinematicABC()
    {
    }

    inline virtual bool isWheeler(void) const;

    /**
     * set ignoreFreeSurface_ to true (calculate first order quantities below z <= 0) or false
     * (calculate first order quantities below \\eta_1
     */
    bool setIgnoreFreeSurface(bool ignoreFreeSurface) { bool oldValue = ignoreFreeSurface_; ignoreFreeSurface_ = ignoreFreeSurface; return oldValue; }

    /**
     * Evaluates a velocity vector at (x,y,z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordiante
     * @return Eigen::Vector3d(u_x, u_y, u_z)
     *
     * This function have to be implemented in the child classes!
     */
    virtual Eigen::Vector3d getVelocity(double time, double x, double y, double z) = 0;

    /**
     * Evaluates a velocity vector at (x,y,z) at time instant t
     * @param time : time instant
     * @param position : Eigen::Vector3d(x, y, z)
     * @return Eigen::Vector3d(u_x, u_y, u_z)
     *
     * No need to re-implement this function, but add 'using WaveKinematicABC::getVelocity' in the class definition of the child
     
    inline virtual Eigen::Vector3d getVelocity(double time, const Eigen::Ref<const Eigen::Vector3d>& position);
    */

    /**
     * Evaluates a velocity vector at (x,y,z) at time instant t
     * @param timePosition : Eigen::Vector4d(t, x, y, z)
     * @return Eigen::Vector3d(u_x, u_y, u_z)
     *
     * No need to re-implement this function, but add 'using WaveKinematicABC::getVelocity' in the class definition of the child
     
    inline virtual Eigen::Vector3d getVelocity(const Eigen::Ref<const Eigen::Vector4d>& timePosition);
    */

    /**
     * @brief evaluates an acceleration vector at (x, y, z) a time instant t
     * @param time : time instant
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @return Eigen::Vector3d(a_x, a_y, a_z)
     */
    virtual Eigen::Vector3d getAcceleration(double time, double x, double y, double z) = 0;

    //inline virtual Eigen::Vector3d getAcceleration(double time, const Eigen::Ref<const Eigen::Vector3d>& position);

    //inline virtual Eigen::Vector3d getAcceleration(const Eigen::Ref<const Eigen::Vector4d>& timePosition);

    // Pressure
    /**
     * Returns the pressure at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @return p(x,y,z,t)
     */
    inline virtual double getPressure(double time, double x, double y, double z);

    /**
     * Returns the pressure at (x,y,z) at time instant t
     * @param time : time instant
     * @param position : Eigen::Vector3d(x, y, z)
     * @return Eigen::Vector3d(u_x, u_y, u_z)
     *
     * No need to re-implement this function, but add 'using WaveKinematicABC::getPressure' in the class definition of the child
     
    inline virtual double getPressure(double time, const Eigen::Ref<const Eigen::Vector3d>& position);
    */

    /**
     * Return the pressure at (x,y,z) at time instant t
     * @param timePosition : Eigen::Vector4d(t, x, y, z)
     * @return Eigen::Vector3d(u_x, u_y, u_z)
     *
     * No need to re-implement this function, but add 'using WaveKinematicABC::getPressure' in the class definition of the child
    inline virtual double getPressure(const Eigen::Ref<const Eigen::Vector4d>& timePosition);
    */

    /**
     * return the latest calculated first order pressure
     */
    double getPressure1() {return p1Saved_;}

    /**
     * Get the elevation at a given time and position.
     *
     * @param time The time instant
     * @param x The position X coordinate
     * @param y The position Y coordinate
     * @param speed The speed
     * @param update If true, update the internal phase
     */
    inline virtual double getElevation(double time, double x, double y, double speed = 0., bool update = true);

    /**
     * return the latest calculated first order free surface elevation
     */
    double getElevation1() {return eta1Saved_;}


    inline Eigen::ArrayXd _getElevation_timeVect(const Eigen::Ref<const Eigen::ArrayXd>& time, double x, double y, double speed = 0.);

    inline Eigen::ArrayXd getElevation_omp(const Eigen::Ref<const Eigen::ArrayXd>& time, const Eigen::Ref<const Eigen::ArrayXd>& x, const Eigen::Ref<const Eigen::ArrayXd>& y, double speed = 0.);

    Eigen::ArrayXXd getElevation_2D(const Eigen::Ref<const Eigen::ArrayXd>& time, const Eigen::Ref<const Eigen::ArrayXd>& x, const Eigen::Ref<const Eigen::ArrayXd>& y, double speed = 0.);

    inline double getDepth();

};

Eigen::ArrayXd WaveKinematicABC::getPhase(double t, double x, double y) const
{
    return p_wif_->getFrequencies() * t - kchead_ * x - kshead_ * y + p_wif_->getPhases();
}

bool WaveKinematicABC::isWheeler(void) const
{
    return isWheeler_;
}


double  WaveKinematicABC::getPressure(double time, double x, double y, double z)
{
    double eta(0.0);
    double eta1(get1stElevation(time, x, y)); // 1st order elevation should be calculated to fill in eta1Saved_ variable
    if (!ignoreFreeSurface_)
        eta = eta1;
    return get1stPressure(time, x, y, z, eta, true);
}



double  WaveKinematicABC::getElevation(double time, double x, double y, double speed, bool update)
{
    return get1stElevation(time, x + speed * time, y, update);
}



Eigen::ArrayXd  WaveKinematicABC::_getElevation_timeVect(const Eigen::Ref<const Eigen::ArrayXd>& time, double x, double y, double speed)
{
    return time.unaryExpr([this, x, y, speed](double t) { return getElevation(t, x, y, speed, false); });
}


double WaveKinematicABC::getDepth()
{
    return depth_;
}

}
}
