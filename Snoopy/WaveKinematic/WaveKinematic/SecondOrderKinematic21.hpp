#pragma once

#include "WaveKinematicExport.hpp"

#include "WaveKinematic/WaveKinematicABC.hpp"
#include "Spectral/Wif.hpp"

namespace BV
{
namespace WaveKinematic
{

class WAVE_KINEMATIC_API SecondOrderKinematic21 : public WaveKinematicABC
{
private:
protected:
    inline double getTotal21Elevation_(double time, double x, double y) ;
    /*!
     * \brief get2ndPressure : calculates the second order pressure due to the first order quantities
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \param[in] z : z coordinate
     * \param[in] eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * \param[in] useConstrain : by default true. If this parameter is true, the constrain z > eta is used.
     *       otherwise pressure is calculated at any point
     * \return \f$\displaystyle -\frac{1}{2}\left(\nabla\Phi^{(1)}\right)^2\f$
     *
     * eq. (27) in "Incident_Second_Order_eqs.pdf"
     */
    virtual double get2ndPressure(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = true) ;
    /*!
     * \brief get2ndElevation : calculates the free surface elevation of the second order due to the first order quantities
     * \param[in] time : time instant t
     * \param[in] x : x coordinate
     * \param[in] y : y coordinate
     * \return \f$\displaystyle -\frac{1}{g}\eta^{(1)}\Phi_{zt}^{(1)} -\frac{1}{2g}\left(\nabla\Phi^{(1)}\right)^2 \f$
     *
     * eq. (33) in "Incident_Second_Order_eqs.pdf"
     */
    virtual double get2ndElevation(double time, double x, double y) ;
    /*!
     * \brief eta21Saved_ : This value is saved in this variable. use getElevation21() method to retrieve it.
     */
    double eta21Saved_ ;
    /*!
     * \brief p21Saved_ : Quadratic part of the second order pressure. use getPressure21() to get this value
     */
    double p21Saved_ ;
    /*!
     * \brief getVelocity_dz_at0: evaluates \f$\frac{\partial u^{(1)}}{\partial z}\Bigl|_{z=0}\f$ for horizontal velocities
     * \param[in] time
     * \param[in] x
     * \param[in] y
     * \return \f$(ux_z, uy_z, 0)\f$
     */
    Eigen::Vector3d getVelocity_dz_at0(double time, double x, double y);

    Eigen::Vector3d getAcceleration_dz_at0(double time, double x, double y);
    /*!
     * \brief eta_: This is temporal variable for the first order free surface elevation which is passed to the SecondOrderKinematic
     */
    double eta_;
    /**
     * Returns the quadratic part of the second order velocity at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @param eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * @param useConstrain : by default true. If this parameter is true, the constrain z > eta is used.
     *       otherwise potential is calculated at any point
     * @return Eigen::Vector3d(u_x^{(21)}, u_y^{(21)}, u_z^{(21)})
     */
    virtual Eigen::Vector3d get21Velocity(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = true);
    /**
     * Returns the quadratic part of the second order acceleration at (x, y, z) at time instant t
     * @param time : time instant t
     * @param x : x coordinate
     * @param y : y coordinate
     * @param z : z coordinate
     * @param eta : constrains, for z > eta, result is 0.0. default value eta = 0.0
     * @param useConstrain : by default true. If this parameter is true, the constrain z > eta is used.
     *       otherwise potential is calculated at any point
     * @return Eigen::Vector3d(u_x^{(21)}, u_y^{(21)}, u_z^{(21)})
     */
    virtual Eigen::Vector3d get21Acceleration(double time, double x, double y, double z, double eta = 0.0, bool useConstrain = true);
public:
    SecondOrderKinematic21(std::shared_ptr<Spectral::Wif> p_wif, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wif, numThreads)
    {
        isWheeler_ = false ;
    }

    SecondOrderKinematic21(std::shared_ptr<Spectral::Wifm> p_wifm, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wifm, numThreads)
    {
        isWheeler_ = false ;
    }

    ~SecondOrderKinematic21()
    {

    }

    using WaveKinematicABC::getVelocity ;
    using WaveKinematicABC::getAcceleration;
    using WaveKinematicABC::getPressure ;
    using WaveKinematicABC::getElevation ;

#define ETA2_FREESURFACE
    Eigen::Vector3d getVelocity(double time, double x, double y, double z) override
    {
    // above the free surface the velocity must be zero!
#ifdef ETA2_FREESURFACE
        eta_ = getElevation (time, x, y);     // calculates the second order free surface elevation (using the first order quantities)
#else
        eta_ = get1stElevation(time, x, y);  // first order free surface elevation
#endif
        return get21Velocity(time, x, y, z, eta_, true);
    }

    Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override
    {
    // above the free surface the velocity must be zero!
#ifdef ETA2_FREESURFACE
        eta_ = getElevation(time, x, y);     // calculates the second order free surface elevation (using the first order quantities)
#else
        eta_ = get1stElevation(time, x, y);  // first order free surface elevation
#endif
        return get21Acceleration(time, x, y, z, eta_, true);
    }

    double getPressure(double time, double x, double y, double z) override
    {
    // above the free surface the velocity must be zero!
#ifdef ETA2_FREESURFACE
        eta_ = getElevation(time, x, y);     // calculates the second order free surface elevation (using the first order quantities)
#else
        eta_ = get1stElevation(time, x, y);  // first order free surface elevation
#endif
        if (z > eta_)
        {
            p1Saved_ = 0.0;
            p21Saved_ = 0.0;
            return 0.0;
        }

        get1stPressure(time, x, y, z, 0.0, false) ;                     // saves value to p1Saved_
        get2ndPressure(time, x, y, z, 0.0, false) ;                     // saves result to p21Saved_
        return p1Saved_ +p21Saved_;
    }

    double getElevation(double time, double x, double y, double /*speed*/ = 0., bool /*update*/ = true) override
    {
        return getTotal21Elevation_(time, x, y);
    }

    /*!
     * \brief get the quadratic part of the second order elevation (saved after getElevation())
     */
    double getElevation21() {return eta21Saved_;}

    double getPressure21() {return p21Saved_;}
};
inline double SecondOrderKinematic21::getTotal21Elevation_(double time, double x, double y)
{
    get2ndElevation(time, x, y);    // it calculates eta1Saved_ and eta21Saved_
    return eta1Saved_ +eta21Saved_;
}
}
}

