#pragma once

#include "WaveKinematicExport.hpp"

#include "WaveKinematic/WaveKinematicABC.hpp"

namespace BV
{
namespace WaveKinematic
{

/**
 * Derived from the abstract class WaveKinematicABC. This class uses only
 * getLinearVelocity implemented in WaveKinematicABC.
 */
class WAVE_KINEMATIC_API FirstOrderKinematic : public WaveKinematicABC
{
public:

    /**
     * Constructor.
     * @param wif The Spectral::Wif object
     */
    FirstOrderKinematic(std::shared_ptr<Spectral::Wif> p_wif, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wif, numThreads)
    {
        isWheeler_ = false ;
    }

    FirstOrderKinematic(std::shared_ptr<Spectral::Wifm> p_wifm, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wifm, numThreads)
    {
        isWheeler_ = false ;
    }

    ~FirstOrderKinematic()
    {
    }

    using WaveKinematicABC::getVelocity;
    using WaveKinematicABC::getPressure;
    using WaveKinematicABC::getAcceleration;

    /**
     * Return the first order velocity vector at (x, y, z) at time instant t
     * @param time time instant t
     * @param x The x coordinate
     * @param y The y coordinate
     * @param z The z coordinate
     * @return Eigen::Vector3d(u_x^{(1)}, u_y^{(1)}, u_z^{(1)})
     */
    inline Eigen::Vector3d getVelocity(double time, double x, double y, double z) override;

    /**
     * Return the first order acceleration vector at (x, y, z) at time instant t
     * @param time time instant t
     * @param x The x coordinate
     * @param y The y coordinate
     * @param z The z coordinate
     * @return Eigen::Vector3d(a_x^{(1)}, a_y^{(1)}, a_z^{(1)})
     */
    inline Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override;

};


inline Eigen::Vector3d FirstOrderKinematic::getVelocity(double time, double x, double y, double z)
{
    double eta(0.0);
    double eta1(get1stElevation(time, x, y)); // 1st order elevation should be calculated to fill in eta1Saved_ variable
    if (!ignoreFreeSurface_)
        eta = eta1;
    return get1stVelocity(time, x, y, z, eta, true);
}

inline Eigen::Vector3d FirstOrderKinematic::getAcceleration(double time, double x, double y, double z)
{
    double eta(0.0);
    double eta1(get1stElevation(time, x, y)); // 1st order elevation should be calculated to fill in eta1Saved_ variable
    if (!ignoreFreeSurface_)
        eta = eta1;
    return get1stAcceleration(time, x, y, z, eta, true);
}

}
}
