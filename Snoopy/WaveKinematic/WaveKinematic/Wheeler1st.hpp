#pragma once

#include "WaveKinematicExport.hpp"
#include "Spectral/Wif.hpp"
#include "WaveKinematic/Wheeler.hpp"
#include "WaveKinematic/WaveKinematicABC.hpp"

namespace BV
{
namespace WaveKinematic
{

class WAVE_KINEMATIC_API Wheeler1st : public WaveKinematicABC
{
private:
    /*!
     * \brief wheeler_ : describes the stretching rule. Because it is common for the 1st and 2nd order, it is put in separate class
     */
    Wheeler wheeler_ ;
public:
    Wheeler1st(std::shared_ptr<Spectral::Wif> p_wif, bool inCrestOnly = false, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wif, numThreads)
    {
        /*
         * In WaveKinematicABC, the phase is calculated at (x = 0, y = 0, t = 0)
         *                      the amplitudes for az are calculated in z = 0.
         */
        isWheeler_ = true ;
        get1stElevation(time_, x_, y_); // result is saved in eta1Saved_
        wheeler_.setDepth(depth_) ;
        wheeler_.setInCrestOnly(inCrestOnly);
    }

    Wheeler1st(std::shared_ptr<Spectral::Wifm> p_wifm, bool inCrestOnly = false, unsigned int numThreads = 1) :
        WaveKinematicABC(p_wifm, numThreads)
    {
        /*
         * In WaveKinematicABC, the phase is calculated at (x = 0, y = 0, t = 0)
         *                      the amplitudes for az are calculated in z = 0.
         */
        isWheeler_ = true ;
        get1stElevation(time_, x_, y_); // result is saved in eta1Saved_
        wheeler_.setDepth(depth_) ;
        wheeler_.setInCrestOnly(inCrestOnly);
    }

    ~Wheeler1st()
    {

    }

    using WaveKinematicABC::getVelocity ;
    using WaveKinematicABC::getAcceleration;

    Eigen::Vector3d getVelocity(double time, double x, double y, double z) override;
    Eigen::Vector3d getAcceleration(double time, double x, double y, double z) override;

    void inCrestOnly(bool isInCrestOnly = true) {wheeler_.setInCrestOnly(isInCrestOnly);}
    bool isInCrestOnly() const {return wheeler_.isInCrestOnly();}
};

}
}
