#pragma once

#include "Spectral/API.hpp"
#include "Spectral/Davenport.hpp"
#include "Spectral/Harris.hpp"
#include "Spectral/Hino.hpp"
#include "Spectral/Kaimal.hpp"
#include "Spectral/Kareem.hpp"
#include "Spectral/ModifiedHarris.hpp"
#include "Spectral/NPD.hpp"
#include "Spectral/OchiShin.hpp"
#include "Spectral/Queffeulou.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;

class PySpectrum : public Spectrum
{
public:
    using Spectrum::Spectrum;
    using Spectrum::name_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override {
        PYBIND11_OVERLOAD_PURE(
            Eigen::ArrayXd,     /* Return type */
            Spectrum,           /* Parent class */
            compute,            /* Name of function in C++ (must match Python name) */
            w                   /* Argument(s) */
        );
    }
};

void InitWindSpectra(py::module & m)
{

    py::class_<Spectrum, std::shared_ptr<Spectrum>, PySpectrum> spectrum(m, "Spectrum");
    spectrum
        .def(py::init<std::string, double>())
        .def("compute", py::overload_cast<double>(&Spectrum::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&>(&Spectrum::compute, py::const_))
        .def("energyRange", &Spectrum::energyRange, "ratio"_a , "wmin"_a = 0.1, "wmax"_a = 2.5 , "dw"_a = 0.001,  "Return range that contains x % of the energy")
        .def("getDiscreteFrequencyEdges", &Spectrum::getDiscreteFrequencyEdges)
        .def("getDiscreteFrequencies", &Spectrum::getDiscreteFrequencies, "wmin"_a = 0.1, "wmax"_a = 1.8, "nbSeed"_a = 100, "seed"_a = 0, "Return edges and center frequencies, refining more the energetic part")
        .def_readonly("name", &PySpectrum::name_)
        .def_readwrite("heading", &Spectrum::heading)
        .def_readwrite("meanVelocity", &Spectrum::meanValue);

    py::class_<API, std::shared_ptr<API> >(m, "API", spectrum,
    		R"rst(
            The American Petroleum Institute proposes the following formula
            from analysis of available wind data and spectral formulations:

            .. math::

               S_{API}(f)=s_v^2/f_p.\left(1+1.5.\left( \frac{f}{f_p} \right) \right)^{\left(-\frac{5}{3}\right)}


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`f_p` is the average factor (by default: :math:`0.0025V_{W10}`);
            - :math:`S_{NPD}` is the power spectral density :math:`(m^2/s)`;
            - :math:`V_{W10}` is the mean hourly wind speed :math:`(m/s)` measured 10m above the sea surface.
            - :math:`s_v` is the turbulence intensity, :math:`s_V=0.164V_{W10}` (at reference height 10m).

            )rst")
        .def(py::init<double, double, double>(),
            "meanVelocity"_a, "fp"_a, "heading"_a=0.,
    		R"rst(
            Define a API wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float fp: the average factor;
            :param float heading: the wind heading in global;
            )rst")

        .def_readwrite("fp", &API::fp)
        .def_static("getNParams", &API::getNParams)
        .def_static("getCoefs_0", &API::getCoefs_0)
        .def_static("getCoefs_min", &API::getCoefs_min)
        .def_static("getCoefs_max", &API::getCoefs_max)
        .def_static("getCoefs_name", &API::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "fp", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &API::getCoefs)
        ;

    py::class_<Davenport, std::shared_ptr<Davenport> >(m, "Davenport", spectrum,
    		R"rst(
            The power spectral density function of the Davenport spectrum
            is defined as follows:

            .. math::

               \frac{fS_D(f)}{u_{*}^{2}} = 4.0\frac{x^2}{(1+x^2)^{4/3}}


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_{D}` is the power spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`x` is the normalized frequency given as follows: :math:`x= \frac{fL}{V_{W10}}`;
            - :math:`L` is a representative length scale :math:`(m)`;
            - :math:`V_{W10}` is the mean wind velocity at 10m height :math:`(m/s)`
                which corresponds to the spectrum amplitude velocity;
            - :math:`u_{*}` is the friction velocity given by: :math:`u_{*} = \frac{kV_{W10}}{\ln(z/z_0)}`;
                with :math:`k` the Von Karman's constant (0.4) and :math:`z_0`, the roughness parameter, here taken at 0.025.

            Finally, the developped Davenport spectrum formulation is:

            .. math::

               S_D(f)= 4L^{-\frac{2}{3}}C_{10}V_{W10}^{\frac{8}{3}}\frac{f}{\left(f^2+\frac{V_{W10}^2}{L^2}\right)^\frac{4}{3}}


            )rst")
        .def(py::init<double, double, double, double>(),
            "meanVelocity"_a, "k"_a, "L"_a, "heading"_a=0.,
    		R"rst(
            Define a  Davenport wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float k: the surface drag coefficient;
            :param float L: the representative length scale;
            :param float heading: the wind heading in global;
            )rst")
        .def_readwrite("k", &Davenport::C10)
        .def_readwrite("L", &Davenport::L)
        .def_static("getNParams", &Davenport::getNParams)
        .def_static("getCoefs_0", &Davenport::getCoefs_0)
        .def_static("getCoefs_min", &Davenport::getCoefs_min)
        .def_static("getCoefs_max", &Davenport::getCoefs_max)
        .def_static("getCoefs_name", &Davenport::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "k", "L", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &Davenport::getCoefs)
        ;

    py::class_<Harris, std::shared_ptr<Harris> >(m, "Harris", spectrum,
    		R"rst(
            Below is the Harris-Det norske Veritas wind spectral formulation,
            reproduced in Feikema and Wichers (1991).

            .. math::

                \frac{fS_{H-DNV}(f)}{u_{*}^{2}}=4F_g


            .. math::

                F_g= \frac{x}{(2+x^2)^\frac{5}{6}}


            with :math:`x`, the normalized frequency given by: :math:`x = \frac{1800f}{V_{W10}}`.
            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_{H-DNV}` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`F_g` is is the gust factor;
            - :math:`u_{*}^{2}` is the friction velocity equal to :math:`u_{*}^{2}=CV_{W10}^2`;
            - :math:`C` is the turbulence or surface drag coefficient may be chosen to equal 0.002 for rough seas and 0.0015 for moderate seas;
            - :math:`L` is the length scale dimension (m), which was chosen to be 1800 m;
            - :math:`V_{W10}` is the hourly mean wind speed (m/s) at 10m height;

            )rst")
        .def(py::init<double, double, double, double>(),
            "meanVelocity"_a, "C"_a, "L"_a, "heading"_a=0.,
    		R"rst(
            Define a Harris wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C: the surface drag coefficient;
            :param float L: the representative length scale;
            :param float heading: the wind heading in global;
            )rst")
        .def_readwrite("C", &Harris::C)
        .def_readwrite("L", &Harris::L)
        .def_static("getNParams", &Harris::getNParams)
        .def_static("getCoefs_0", &Harris::getCoefs_0)
        .def_static("getCoefs_min", &Harris::getCoefs_min)
        .def_static("getCoefs_max", &Harris::getCoefs_max)
        .def_static("getCoefs_name", &Harris::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C", "L", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &Harris::getCoefs)
        ;

    py::class_<Hino, std::shared_ptr<Hino> >(m, "Hino", spectrum,
    		R"rst(
            The original form of the Hino spectrum is given by Ochi and Shin (1988),

            .. math::

                \frac{fS_H(f)}{\sigma^2}=\frac{0.475x}{\left(1+x^2\right)^{\frac{5}{6}}}


            with :math:`x`, given by:

            .. math::

                x=\frac{10\sigma^3\left(\frac{z}{10}\right)^{1-4\alpha}}{0.0275V_{W10}(\alpha V_{W10})^3}f


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_H` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`\alpha` is the exponent of the power law governing the profile of the mean wind speed;
            - :math:`\sigma^2` is the variance of the wind speed defined by Koulousek et al. (1984)
                as :math:`\sigma^2=5.6C(V_W(z))^2\left(\frac{10}{z}\right)^{2\alpha}`.
            - :math:`C` is the turbulence or surface drag coefficient;
            - :math:`V_{W10}` is the hourly mean wind speed (m/s) at 10m height;

            In a study by Ochi and Shin (1988), :math:`\alpha` was chosen to be 0.16, value which has been imposed within Opera.

            Kolousek et al. (1984) suggests that the Hino spectral formulation may be applied over an ocean environment.
            )rst")
        .def(py::init<double, double, double, double, double, double>(),
            "meanVelocity"_a, "C"_a, "z"_a, "vwz"_a, "alpha"_a, "heading"_a=0.,
    		R"rst(
            Define a Hino wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C: the surface drag coefficient;
            :param float z: the height above sea surface;
            :param float vwz
            :param float alpha
            :param float heading: the wind heading in global;
            )rst")
        .def_readonly("C", &Hino::C)
        .def_readonly("z", &Hino::z)
        .def_readonly("vwz", &Hino::vwz)
        .def_static("getNParams", &Hino::getNParams)
        .def_static("getCoefs_0", &Hino::getCoefs_0)
        .def_static("getCoefs_min", &Hino::getCoefs_min)
        .def_static("getCoefs_max", &Hino::getCoefs_max)
        .def_static("getCoefs_name", &Hino::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C", "z", "vwz", "alpha", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &Hino::getCoefs)
        ;

    py::class_<Kaimal, std::shared_ptr<Kaimal> >(m, "Kaimal", spectrum,
    		R"rst(
            The power spectral density function of the Kaimal spectrum is defined as follows:

            .. math::

                \frac{fS_K(f)}{u_{*}^{2}} = \frac{200x}{\left(1+50x\right)^{\frac{5}{3}}}


            with :math:`x` is the normalized frequency given as follows:

            .. math::

                x = \frac{f.z}{V_{W10}}


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`z` is the elevation (m) taken at 10m;
            - :math:`S_K` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`u_{*}` is the friction velocity given by: :math:`u_{*} = \frac{kV_{W10}}{\ln(z/z_0)}`.
            - :math:`V_{W10}` is the hourly mean wind speed (m/s) at 10m height;

            with :math:`k` the Von Karman's constant (0.4) and :math:`z_0`,
            the roughness parameter, here taken at 0.025.

            So, the final formulation is:

            .. math::

                S_K(f)=\frac{200zC_{10}V_{W10}}{\left(1+\frac{50zf}{V_{W10}}\right)^{\frac{5}{3}}}


            where :math:`C_{10}` is the sea surface drag coefficient
            approximatively equal to 0.0044 and given by:

            .. math::

                C_{10}=\left(\frac{k}{\ln(z/z_0)}\right)^2

            )rst")
        .def(py::init<double, double, double, double>(),
            "meanVelocity"_a, "C10"_a, "z"_a, "heading"_a=0.,
    		R"rst(
            Define a Kaimal wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C10: the surface drag coefficient;
            :param float z: the height above sea surface;
            :param float heading: the wind heading in global;
            )rst")
        .def_readwrite("C10", &Kaimal::C10)
        .def_readwrite("z", &Kaimal::z)
        .def_static("getNParams", &Kaimal::getNParams)
        .def_static("getCoefs_0", &Kaimal::getCoefs_0)
        .def_static("getCoefs_min", &Kaimal::getCoefs_min)
        .def_static("getCoefs_max", &Kaimal::getCoefs_max)
        .def_static("getCoefs_name", &Kaimal::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C10", "z", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &Kaimal::getCoefs)
        ;

    py::class_<Kareem, std::shared_ptr<Kareem> >(m, "Kareem", spectrum,
    		R"rst(
            Kareem proposes the following formula from analysis of available wind data
            and spectral formulations, Ochi and Shin (1988).

            .. math::

                S(f_*)=\frac{335f_*}{(1+71f_*)^{\frac{5}{3}}}


            with :math:`f_*` is the normalized frequency given as follows:

            .. math::

                f_*=f.z/V_W(z)


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`z` is the elevation (m) taken at 10m;
            - :math:`S_{Ka}` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`V_W(z)` is the mean wind speed (m/s) at a height :math:`z` (m),
                defined as: :math:`V_W(z)=V_{W10}+2.5u_*ln(z/10)`.
            - :math:`u_{*}` is the shear velocity given by: :math:`u_*=\sqrt{C_{10}}V_{W10}`.
            - :math:`C_{10}` is the sea surface drag coefficient  ;
            - :math:`V_{W10}` is the hourly mean wind speed (m/s) at 10m height;

            In the derivation of the spectral formulation, Kareem specifically
            considers the spectral density obtained at low frequencies.
            )rst")
        .def(py::init<double, double, double, double>(),
            "meanVelocity"_a, "C10"_a, "z"_a, "heading"_a=0.,
    		R"rst(
            Define a Kareem wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C10: the surface drag coefficient;
            :param float z: the height above sea surface;
            :param float heading: the wind heading in global;
            )rst")
        .def_readonly("C10", &Kareem::C10)
        .def_readonly("z", &Kareem::z)
        .def_static("getNParams", &Kareem::getNParams)
        .def_static("getCoefs_0", &Kareem::getCoefs_0)
        .def_static("getCoefs_min", &Kareem::getCoefs_min)
        .def_static("getCoefs_max", &Kareem::getCoefs_max)
        .def_static("getCoefs_name", &Kareem::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C10", "z", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &Kareem::getCoefs)
        ;

    py::class_<ModifiedHarris, std::shared_ptr<ModifiedHarris> >(m, "ModifiedHarris", spectrum,
    		R"rst(
            Below is the spectral formulation suggested by Wills cited by Feikema and Wichers (1991).

            .. math::

                \frac{fS_{H_DNV}(f)}{u_{*}^{2}} = 4 F_g


            .. math::

                F_g=\frac{xA(x)}{\left(2+x^2\right)^\frac{5}{6}}


            with :math:`x` is the normalized frequency given as follows:

            .. math::

                x=\frac{Lf}{V_{W10}}


            and:

            .. math::

                A(x)=0.51\left(\frac{\left(2+x^2\right)^\frac{5}{6}}{\left(x^{0.15}+\frac{9}{8}x\right)^\frac{5}{3}}\right)


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_W` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`u_{*}^{2}` is the friction velocity equal to :math:`u_{*}^{2}=CV_{W10}^2`;
            - :math:`F_g` is the gust factor;
            - :math:`L` is the length scale dimension (m);
            - :math:`C` is the sea surface drag coefficient.
            - :math:`V_{W10}` is the hourly mean wind speed (m/s) at 10m height;

            Feikema and Wichers (1991) take a velocity independent sea surface drag coefficient :math:`C` of 0.003,
            however because it's origin is unclear it may therefore be considered dubious.
            To compare spectral density functions, the sea surface drag formulation stipulated by
            Bureau Veritas would be more useful.

            The sea surface drag coefficient at 10m above the sea level :math:`C_{10}`:

            .. math::

                C_{10}=0.00104+\frac{0.0015}{1+exp\left(-\frac{V_{W10}-12.5}{1.564}\right)}


            )rst")
        .def(py::init<double, double, double, double>(),
            "meanVelocity"_a, "C"_a, "L"_a, "heading"_a=0.,
    		R"rst(
            Define a Modified Harris wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C: the surface drag coefficient;
            :param float L: the representative length scale;
            :param float heading: the wind heading in global;
            )rst")
        //.def_readwrite("C", &ModifiedHarris::C)
        .def_readonly("C", &ModifiedHarris::C)
        .def_readonly("L", &ModifiedHarris::L)
        .def_static("getNParams", &ModifiedHarris::getNParams)
        .def_static("getCoefs_0", &ModifiedHarris::getCoefs_0)
        .def_static("getCoefs_min", &ModifiedHarris::getCoefs_min)
        .def_static("getCoefs_max", &ModifiedHarris::getCoefs_max)
        .def_static("getCoefs_name", &ModifiedHarris::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C", "L", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &ModifiedHarris::getCoefs)
        ;

    py::class_<NPD, std::shared_ptr<NPD> >(m, "NPD", spectrum,
    		R"rst(
            The Norwegian Petroleum Directorate proposes the following formula
            from analysis of available wind data and spectral formulations:

            .. math::

                S_{NPD}(f)=\frac{320.\left(\frac{V_{W10}}{10}\right)^2}{\left(1+f'^n\right)^\frac{5}{3n}}


            .. math::

                f'=172.f.\left(\frac{V_{W10}}{10}\right)^{-0.75}


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_{NPD}` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`V_{W10}` is the hourly mean wind speed (m/s) at 10m height;

            )rst")
        .def(py::init<double, double>(),
            "meanVelocity"_a, "heading"_a=0.,
    		R"rst(
            Define a NPD wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float heading: the wind heading in global;
            )rst")
        .def("compute", py::overload_cast<double>(&Spectrum::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&>(&Spectrum::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, double>(&NPD::compute, py::const_))
        .def_static("getNParams", &NPD::getNParams)
        .def_static("getCoefs_0", &NPD::getCoefs_0)
        .def_static("getCoefs_min", &NPD::getCoefs_min)
        .def_static("getCoefs_max", &NPD::getCoefs_max)
        .def_static("getCoefs_name", &NPD::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &NPD::getCoefs)
        ;

    py::class_<OchiShin, std::shared_ptr<OchiShin> >(m, "OchiShin", spectrum,
    		R"rst(
            Ochi-Shin (1988) suggest the following spectral formulation for wind over a seaway.

            .. math::

               F_g=
               \left\lbrace
               \begin{array}{ccc}
               583x  & \mbox{if} & 0<x<0.003 \vspace{0.1cm}\\
               \frac{420x^{0.7}}{\left(1+x^{0.35}\right)^{11.5}} & \mbox{if} & 0.003<x<0.1 \vspace{0.1cm}\\
               \frac{838x}{\left(1+x^{0.35}\right)^{11.5}} & \mbox{if} & x>0.1
               \end{array}\right.


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_{O-S}` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`x=zf/V_W(z)` also referred to as the normalized frequency;
            - :math:`V_W(z)` is the hourly mean wind velocity at a height :math:`z` (m/s);
            - :math:`z` is the height above sea level (m).

            Fiekema and Wichers (1991) suggest the following formulation for
            the drag coefficient to be used in conjunction with the Ochi and
            Shin spectral formulation:

            .. math::

               C=(750+69V_W(z))*10^{-6}


            )rst")
        .def(py::init<double, double, double, double>(),
            "meanVelocity"_a, "C"_a, "z"_a, "heading"_a=0.,
    		R"rst(
            Define a Ochi-Shin wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C: the surface drag coefficient;
            :param float z: the height above sea surface;
            :param float heading: the wind heading in global;
            )rst")
        .def_readwrite("C", &OchiShin::C)
        .def_readwrite("z", &OchiShin::z)
        .def_static("getNParams", &OchiShin::getNParams)
        .def_static("getCoefs_0", &OchiShin::getCoefs_0)
        .def_static("getCoefs_min", &OchiShin::getCoefs_min)
        .def_static("getCoefs_max", &OchiShin::getCoefs_max)
        .def_static("getCoefs_name", &OchiShin::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C", "z", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &OchiShin::getCoefs)
        ;

    py::class_<Queffeulou, std::shared_ptr<Queffeulou> >(m, "Queffeulou", spectrum,
    		R"rst(
            The power spectral density function of the Queffeulou spectrum
            is defined as follows:

            .. math::

               S_Q(f)=2.05C_{10}V_{W10}Ri^{-1}. z . \frac{1}{ \left(1 + \frac{0.5206675z^{ \frac{5}{3} }. f^{ \frac{5}{3}} }{(V_{W10} Ri)^{ \frac{5}{3} } } \right) }


            where:

            - :math:`f` is the frequency :math:`(Hz)`;
            - :math:`S_Q` is the wind spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`Ri` is the Richardson constant :math:`(0.05<Ri<0.2)`;
            - :math:`V_{W10}` is the hourly mean wind velocity at a 10m height (m/s);
            - :math:`C_{10}` is the sea surface drag coefficient.
            - :math:`z` is the height above sea level (m).

            )rst")
        .def(py::init<double, double, double, double, double>(),
            "meanVelocity"_a, "C10"_a, "z"_a, "Ri"_a, "heading"_a=0.,
    		R"rst(
            Define a Queffeulou wind spectrum.

            :param float meanVelocity: the wind mean velocity;
            :param float C10: the surface drag coefficient;
            :param float z: the height above sea surface;
            :param float Ri: the Richardson constant;
            :param float heading: the wind heading in global;
            )rst")
        .def_readwrite("C10", &Queffeulou::C10)
        .def_readwrite("z", &Queffeulou::z)
        .def_readwrite("Ri", &Queffeulou::Ri)
        .def_static("getNParams", &Queffeulou::getNParams)
        .def_static("getCoefs_0", &Queffeulou::getCoefs_0)
        .def_static("getCoefs_min", &Queffeulou::getCoefs_min)
        .def_static("getCoefs_max", &Queffeulou::getCoefs_max)
        .def_static("getCoefs_name", &Queffeulou::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"meanVelocity", "C10", "z", "Ri", "heading"} ;
                                        return attrs ;
                                     })
        .def("getCoefs", &Queffeulou::getCoefs)
        ;
}
