#pragma once

#include "Spectral/Rao.hpp"
#include "Spectral/Qtf0.hpp"
#include "Spectral/Qtf.hpp"
#include "Spectral/MQtf.hpp"

#include "Tools/TensorPythonCaster.hpp"
#include "Math/Interpolators/Interpolators.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;

// Intermediate class to be able to expose protected attribute to Python
class PyQtf0 : public Qtf0
{
public:
    using Qtf0::refPoint_;
    using Qtf0::waveRefPoint_;
};

// Intermediate class to be able to expose protected attribute to Python
class PyRao : public Rao
{
public:
    using Rao::refPoint_;
    using Rao::waveRefPoint_;
};

// Intermediate class to be able to expose protected attribute to Python

namespace WrappingDetails {

    template <int Rank, typename Derived,
              typename ComplexStorageType, typename ModuleStorageType,
              typename PhasisStorageType, typename RealStorageType,
              typename ImagStorageType, typename ModuleType>
    void declareAllTensorsStorage(ModuleType & m)
    {
        using LocalType = AllTensorsStorage<Rank, Derived, ComplexStorageType,
                                            ModuleStorageType, PhasisStorageType,
                                            RealStorageType, ImagStorageType> ;
        std::string sName("AllTensorsStorage"+std::to_string(Rank)+typeid(Derived).name()
                          +typeid(ComplexStorageType).name()
                          +typeid(ModuleStorageType).name()
                          +typeid(PhasisStorageType).name()
                          +typeid(RealStorageType).name()
                          +typeid(ImagStorageType).name()) ;
        py::class_<LocalType>(m, sName.c_str())
            .def("getComplexData", py::overload_cast<typename LocalType::IndexType,
                                                    const Eigen::ArrayXd &,
                                                    const BV::Math::Interpolators::InterpScheme &,
                                                    const ComplexInterpolationStrategies &,
                                                    const BV::Math::Interpolators::ExtrapolationType &>(
                                                        &LocalType::getComplexData,
                                                        py::const_),
                 "axisIndex"_a, "values"_a, "interpScheme"_a, "interpStrategy"_a, "extrapType"_a,
                 R"rst(
                 Get interpolated complex data along given index axis on given values.

                 :param axisIndex.
                 :param values: a :py:class:`numpy.ndarray` containing
                     the values at which data has to be interpolated.
                 :param interpScheme: interpolation scheme to choose within enum values.
                     Linear interpolation scheme set by default.
                 :param interpStrategy: interpolation strategy to choose within enum values.
                     Interpolation done by default on real, imaginary and amplitude terms.
                 :param extrapType: extrapolation type to choose within enum values.
                     Boundary extrapolation type set by default.

                 :return: the interpolated complex data.
                 )rst")
            .def("getModules", py::overload_cast<typename LocalType::IndexType,
                                                 const Eigen::ArrayXd &,
                                                 const BV::Math::Interpolators::InterpScheme &,
                                                 const BV::Math::Interpolators::ExtrapolationType &>(
                                                        &LocalType::getModules,
                                                        py::const_),
                 "axisIndex"_a, "values"_a, "interpScheme"_a, "extrapType"_a,
                 R"rst(
                 Get interpolated modules data along given index axis on given values.

                 :param int axisIndex: the interpolated axis index. 
                 :param values: a :py:class:`numpy.ndarray` containing
                     the values at which data has to be interpolated.
                 :param interpScheme: interpolation scheme to choose within enum values.
                     Linear interpolation scheme set by default.
                 :param extrapType: extrapolation type to choose within enum values.
                     Boundary extrapolation type set by default.

                 :return: the interpolated modules data.
                 )rst")
            .def("getPhases", py::overload_cast<typename LocalType::IndexType,
                                                const Eigen::ArrayXd &,
                                                const BV::Math::Interpolators::InterpScheme &,
                                                const BV::Math::Interpolators::ExtrapolationType &>(
                                                       &LocalType::getPhases,
                                                       py::const_),
                 "axisIndex"_a, "values"_a, "interpScheme"_a, "extrapType"_a,
                 R"rst(
                 Get interpolate phases data along given index axis on given values.
                 
                 :param int axisIndex: the interpolated axis index.   
                 :param values: a :py:class:`numpy.ndarray` containing
                     the values at which data has to be interpolated.
                 :param interpScheme: interpolation scheme to choose within enum values.
                     Linear interpolation scheme set by default.
                 :param extrapType: extrapolation type to choose within enum values.
                     Boundary extrapolation type set by default.

                 :return: the interpolated phases data.
                 )rst")
            .def("getComplexData", py::overload_cast<>(&LocalType::getComplexData,
                                                       py::const_),
                 R"rst(
                 Get tensor complex data.

                 :return: the tensor complex data.
                 )rst")
            .def("getModules", py::overload_cast<>(&LocalType::getModules,
                                                   py::const_),
                 R"rst(
                 Get tensor module components.

                 :return: the tensor module components.
                 )rst")
            .def("getPhases", py::overload_cast<>(&LocalType::getPhases,
                                                  py::const_),
                 R"rst(
                 Get tensor phase components.

                 :return: the tensor phase components.
                 )rst")
            .def("getReal", &LocalType::getReal,
                 R"rst(
                 Get tensor real components.

                 :return: the tensor real components.
                 )rst")
            .def("getImag", &LocalType::getImag,
                 R"rst(
                 Get tensor imaginary components.

                 :return: the tensor imaginary components.
                 )rst")

            //Operator overloading
            .def("__add__", [](const Derived &a, const Derived & b) { return a + b;}, py::is_operator(),
                    R"rst(
                    Objects at both sides of addition operator are summed term by term.
                    :return: a copy of current object summed right.
                    )rst")
            .def("__iadd__", [](Derived &a, const Derived & b) { a += b;}, py::is_operator(),
                    R"rst(
                    Add object at right side of addition operator to left side object term by term.
                    :return: current object summed right.
                    )rst")
            .def("__sub__", [](const Derived &a, const Derived & b) { return a - b;}, py::is_operator(),
                    R"rst(
                    Objects at both sides of subtraction operator are subtracted term by term.
                    :return: a copy of current object subtracted right.
                    )rst")
            .def("__isub__", [](Derived &a, const Derived & b) { a -= b;}, py::is_operator(),
                    R"rst(
                    Subtract object at right side of subtraction operator to left side object term by term.
                    :return: current object subtracted right.
                    )rst")
            .def("__mul__", [](const Derived &a, const double & b) { return a * b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by scalar.
                    :return: a copy of current object multiplied.
                    )rst")
            .def("__imul__", [](Derived &a, const double & b) { a *= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by scalar.
                    :return: current object multiplied.
                    )rst")
            .def("__truediv__", [](const Derived &a, const double & b) { return a / b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by scalar.
                    :return: a copy of current object divided.
                    )rst")
            .def("__itruediv__", [](Derived &a, const double & b) { a /= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by scalar.
                    :return: current object divided.
                    )rst")
            .def("__mul__", [](const Derived &a, const std::complex<double> & b) { return a * b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by complex.
                    :return: a copy of current object multiplied.
                    )rst")
            .def("__imul__", [](Derived &a, const std::complex<double> & b) { a *= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by complex.
                    :return: current object multiplied.
                    )rst")
            .def("__truediv__", [](const Derived &a, const std::complex<double> & b) { return a / b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are dividec by complex.
                    :return: a copy of current object divided.
                    )rst")
            .def("__itruediv__", [](Derived &a, const std::complex<double> & b) { a /= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by complex.
                    :return: current object divided.
                    )rst")
        ;
    }

    template <int Rank, typename Derived, typename ModuleType>
    void declareRealTensorStorage(ModuleType & m)
    {
        std::string sName("RealTensorStorage"+std::to_string(Rank)+typeid(Derived).name()) ;
        py::class_<RealTensorStorage<Rank, Derived> >(m, sName.c_str())
            .def("getData", py::overload_cast<typename RealTensorStorage<Rank, Derived>::IndexType,
                                              const Eigen::ArrayXd &,
                                              const BV::Math::Interpolators::InterpScheme &,
                                              BV::Math::Interpolators::ExtrapolationType>(
                                                        &RealTensorStorage<Rank, Derived>::getData,
                                                        py::const_),
                 "axisIndex"_a, "values"_a, "interpScheme"_a, "extrapType"_a,
                 R"rst(
                 Get interpolated data along given index axis on given values.

                 :param int axisIndex: the interpolated axis index.
                 :param values: a :py:class:`numpy.ndarray` containing
                     the values at which data has to be interpolated.
                 :param interpScheme: interpolation scheme to choose within enum values.
                     Linear interpolation scheme set by default.
                 :param extrapType: extrapolation type to choose within enum values.
                     Boundary extrapolation type set by default.

                 :return: the interpolated data.
                 )rst")
            .def("getData", py::overload_cast<>(&RealTensorStorage<Rank, Derived>::getData,
                                                py::const_),
                 R"rst(
                 Get data.
                 )rst")
            //Operator overloading
            .def("__add__", [](const Derived &a, const Derived & b) { return a + b;}, py::is_operator(),
                    R"rst(
                    Objects at both sides of addition operator are summed term by term.
                    :return: a copy of current object summed right.
                    )rst")
            .def("__iadd__", [](Derived &a, const Derived & b) { a += b;}, py::is_operator(),
                    R"rst(
                    Add object at right side of addition operator to left side object term by term.
                    :return: current object summed right.
                    )rst")
            .def("__sub__", [](const Derived &a, const Derived & b) { return a - b;}, py::is_operator(),
                    R"rst(
                    Objects at both sides of subtraction operator are subtracted term by term.
                    :return: a copy of current object subtracted right.
                    )rst")
            .def("__isub__", [](Derived &a, const Derived & b) { a -= b;}, py::is_operator(),
                    R"rst(
                    Subtract object at right side of subtraction operator to left side object term by term.
                    :return: current object subtracted right.
                    )rst")

            .def("__mul__", [](const Derived &a, const double & b) { return a * b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by scalar.
                    :return: a copy of current object multiplied.
                    )rst")
            .def("__imul__", [](Derived &a, const double & b) { a *= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by scalar.
                    :return: current object multiplied.
                    )rst")
            .def("__truediv__", [](const Derived &a, const double & b) { return a / b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by scalar.
                    :return: a copy of current object divided.
                    )rst")
            .def("__itruediv__", [](Derived &a, const double & b) { a /= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by scalar.
                    :return: current object divided.
                    )rst")

        ;
    }

    template <int Rank, typename Derived, typename ModuleType>
    void declareRaoStorage(ModuleType & m)
    {
        std::string sName("RaoStorage"+std::to_string(Rank)+typeid(Derived).name()) ;
        using ComplexStorageType = Eigen::Tensor<std::complex<double>, 3> ;
        using ModuleStorageType = Eigen::Tensor<double, 3> ;
        using PhasisStorageType = Eigen::Tensor<double, 3> ;
        using RealStorageType = Eigen::Tensor<double, 3> ;
        using ImagStorageType = Eigen::Tensor<double, 3> ;
        using AllTStorageType = AllTensorsStorage<Rank, Derived, ComplexStorageType,
                                                  ModuleStorageType, PhasisStorageType,
                                                  RealStorageType, ImagStorageType> ;
        declareAllTensorsStorage<Rank, Derived, ComplexStorageType,
                                 ModuleStorageType, PhasisStorageType,
                                 RealStorageType, ImagStorageType>(m) ;
        py::class_<RaoStorage<Rank, Derived>, AllTStorageType>(m, sName.c_str())
            .def("__imul__", [](Derived &a, const double & b) { a *= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by scalar.
                    :return: current object multiplied.
                    )rst")
            .def("__mul__", [](const Derived &a, const double & b) { return a * b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by scalar.
                    :return: a copy of current object multiplied.
                    )rst")
            .def("__itruediv__", [](Derived &a, const double & b) { a /= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by scalar.
                    :return: current object divided.
                    )rst")
            .def("__truediv__", [](const Derived &a, const double & b) { return a / b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by scalar.
                    :return: a copy of current object divided.
                    )rst")

            .def("__imul__", [](Derived &a, const std::complex<double> & b) { a *= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by complex.
                    :return: current object multiplied.
                    )rst")
            .def("__mul__", [](const Derived &a, const std::complex<double> & b) { return a * b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are multiplied by complex.
                    :return: a copy of current object multiplied.
                    )rst")
            .def("__itruediv__", [](Derived &a, const std::complex<double> & b) { a /= b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by complex.
                    :return: current object divided.
                    )rst")
            .def("__truediv__", [](const Derived &a, const std::complex<double> & b) { return a / b;}, py::is_operator(),
                    R"rst(
                    All terms of current object are divided by complex.
                    :return: a copy of current object divided.
                    )rst")
            .def("__imul__",[](Derived &a, const Derived & b) { a *= b;}, py::is_operator(),
                    R"rst(
                    Multiply object at right side of multiplication operator to left side object term by term.
                    :return: current object multiplied right.
                    )rst")
            .def("__mul__",  [](const Derived &a, const Derived & b) { return a * b;}, py::is_operator(),
                    R"rst(
                    Objects at both sides of multiplication operator are multiplied term by term.
                    :return: a copy of current object multiplied right.
                    )rst")
            .def("__itruediv__",[](Derived &a, const Derived & b) { a /= b;}, py::is_operator(),
                    R"rst(
                    Objects at both sides of division operator are divided term by term.
                    :return: a copy of current object divided right.
                    )rst")
            .def("__truediv__",  [](const Derived &a, const Derived & b) { return a / b;}, py::is_operator(),
                    R"rst(
                    Multiply object at right side of multiplication operator to left side object term by term.
                    :return: current object multiplied right.
                    )rst")
        ;
    }

    template <int Rank, typename Derived, typename ModuleType>
    void declareQtfStorage(ModuleType & m)
    {
        using AllTStorageType = AllTensorsStorage<Rank, Derived, QtfTensorComplex,
                                                  QtfTensorModule, QtfTensorPhasis,
                                                  QtfTensorReal, QtfTensorImag> ;
        declareAllTensorsStorage<Rank, Derived, QtfTensorComplex,
                                 QtfTensorModule, QtfTensorPhasis,
                                 QtfTensorReal, QtfTensorImag>(m) ;
        std::string sName("QtfStorage"+std::to_string(Rank)+typeid(Derived).name()) ;
        py::class_<QtfStorage<Rank, Derived>, AllTStorageType>(m, sName.c_str())
            .def("getNDeltaFrequencies", &QtfStorage<Rank, Derived>::getNDeltaFrequencies,
                 R"rst(
                 Get number of delta frequencies Ndf.

                 :return int: the number of delta frequencies.
                 )rst")
            .def("getDeltaFrequencies", &QtfStorage<Rank, Derived>::getDeltaFrequencies,
                 R"rst(
                 Get delta frequencies relative to Qtf data.

                 :return: the Ndf :py:class:`numpy.ndarray` delta frequencies.
                 )rst")
            .def("getComplexData", [](QtfStorage<Rank, Derived> *s){return s->getComplexData().toWDWTensor() ;},
                 R"rst(
                 Get Qtf complex data.

                 :return: the Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray` Qtf complex data.
                 )rst")
            .def("getModules", [](QtfStorage<Rank, Derived> *s){return s->getModules().toWDWTensor() ;},
                 R"rst(
                 Get Qtf module components.

                 :return: the Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray` Qtf module components.
                 )rst")
            .def("getPhases", [](QtfStorage<Rank, Derived> *s){return s->getPhases().toWDWTensor() ;},
                 R"rst(
                 Get Qtf phase components.

                 :return: the Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray` Qtf phase components.
                 )rst")
            .def("getReal", [](QtfStorage<Rank, Derived> *s){return s->getReal().toWDWTensor() ;},
                 R"rst(
                 Get Qtf real components.

                 :return: the Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray` Qtf real components.
                 )rst")
            .def("getImag", [](QtfStorage<Rank, Derived> *s){return s->getImag().toWDWTensor() ;},
                 R"rst(
                 Get Qtf imag components.

                 :return: the Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray` Qtf imag components.
                 )rst")
        ;
    }

    template <int Rank, typename Derived, typename ModuleType>
    void declareMQtfStorage(ModuleType & m)
    {
        std::string sName("MQtfStorage"+std::to_string(Rank)+typeid(Derived).name()) ;
        using ComplexStorageType = Eigen::Tensor<std::complex<double>, 5> ;
        using ModuleStorageType = Eigen::Tensor<double, 5> ;
        using PhasisStorageType = Eigen::Tensor<double, 5> ;
        using RealStorageType = Eigen::Tensor<double, 5> ;
        using ImagStorageType = Eigen::Tensor<double, 5> ;
        declareAllTensorsStorage<Rank, Derived, ComplexStorageType,
                                 ModuleStorageType, PhasisStorageType,
                                 RealStorageType, ImagStorageType>(m) ;
    }

    template <int Rank, typename Derived, typename StorageType, typename ModuleType>
    void declareHydroTF(ModuleType & m)
    {
        std::string tfName("_TransferFunction"+std::to_string(Rank)+typeid(Derived).name()) ;
        std::string htfName("_HydroTransferFunction"+std::to_string(Rank)+typeid(Derived).name()) ;
        py::class_<TransferFunction<StorageType>, StorageType>(m, tfName.c_str())
            .def("getReferencePoint", &TransferFunction<StorageType>::getReferencePoint)
            .def("setReferencePoint", &TransferFunction<StorageType>::setReferencePoint)
            .def("getWaveReferencePoint", &TransferFunction<StorageType>::getWaveReferencePoint)
            .def("setWaveReferencePoint", &TransferFunction<StorageType>::setWaveReferencePoint)
            .def("getAxis", &TransferFunction<StorageType>::getAxis)
        ;

        py::class_<HydroTransferFunction<StorageType>, TransferFunction<StorageType> >(m, htfName.c_str())
            .def("getNHeadings", &HydroTransferFunction<StorageType>::getNHeadings,
                 R"rst(
                 Get number of headings Nb.

                 :return int: the number of headings.
                 )rst")
            .def("getNFrequencies", &HydroTransferFunction<StorageType>::getNFrequencies,
                 R"rst(
                 Get number of frequencies Nf.

                 :return int: the number of frequencies.
                 )rst")
            .def("getClosestHeadingIndex", &HydroTransferFunction<StorageType>::getClosestHeadingIndex,
                 R"rst(
                 Get the index of the closest heading from provided value in heading axis
                 
                 :return: the index of the heading closest to provided value.
                 )rst")
            .def("getHeadings", &HydroTransferFunction<StorageType>::getHeadings,
                 R"rst(
                 Get headings relative to transfer function data.

                 :return: the Nh :py:class:`numpy.ndarray` headings.
                 )rst")
            .def("getFrequencies", &HydroTransferFunction<StorageType>::getFrequencies,
                 R"rst(
                 Get frequencies relative to transfer function data.

                 :return: the Nf :py:class:`numpy.ndarray` frequencies.
                 )rst")
            .def("getForwardSpeed", &HydroTransferFunction<StorageType>::getForwardSpeed,
                 R"rst(
                 Get forward speed relative to transfer function data.

                 :return float: the forward speed.
                 )rst")
            .def("getDepth", &HydroTransferFunction<StorageType>::getDepth,
                 R"rst(
                 Get depth relative to transfer function data.

                 :return float: the forward speed.
                 )rst")
            .def("getModes", &HydroTransferFunction<StorageType>::getModes,
                 R"rst(
                 Get modes relative to transfer function data.

                 :return: the list of Nm labels (enum values) corresponding to each of the modes.
                 )rst")
            .def("getNModes", &HydroTransferFunction<StorageType>::getNModes,
                 R"rst(
                 Get number of modes Nm.

                 :return int: the number of modes.
                 )rst")
            .def("getModeCoefficients", &HydroTransferFunction<StorageType>::getModeCoefficients,
                 R"rst(
                 Get mode coefficients.
                 )rst")
        ;
    }
} // End of namespace WrappingDetails

void InitTransferFunctions(py::module & m)
{
    py::enum_<ComplexInterpolationStrategies>(m, "ComplexInterpolationStrategies")
        .value("RE_IM", ComplexInterpolationStrategies::RE_IM)
        .value("RE_IM_AMP", ComplexInterpolationStrategies::RE_IM_AMP)
        .value("AMP_PHASE", ComplexInterpolationStrategies::AMP_PHASE)
    ;

    py::enum_<Modes>(m, "Modes")
        .value("NONE", Modes::NONE)
        .value("SURGE", Modes::SURGE)
        .value("SWAY", Modes::SWAY)
        .value("HEAVE", Modes::HEAVE)
        .value("ROLL", Modes::ROLL)
        .value("PITCH", Modes::PITCH)
        .value("YAW", Modes::YAW)
        .value("FX", Modes::FX)
        .value("FY", Modes::FY)
        .value("FZ", Modes::FZ)
        .value("MX", Modes::MX)
        .value("MY", Modes::MY)
        .value("MZ", Modes::MZ)
        .value("WAVE", Modes::WAVE)
        .value("RWE", Modes::RWE)
        .value("SECTFX", Modes::SECTFX)
        .value("SECTFY", Modes::SECTFY)
        .value("SECTFZ", Modes::SECTFZ)
        .value("SECTMX", Modes::SECTMX)
        .value("SECTMY", Modes::SECTMY)
        .value("SECTMZ", Modes::SECTMZ)
        .value("WATERVELOCITY_X", Modes::WATERVELOCITY_X)
        .value("WATERVELOCITY_Y", Modes::WATERVELOCITY_Y)
        .value("WATERVELOCITY_Z", Modes::WATERVELOCITY_Z)
        .value("PRESSURE", Modes::PRESSURE)
        .value("VSURGE", Modes::VSURGE)
        .value("VSWAY",  Modes::VSWAY)
        .value("VHEAVE", Modes::VHEAVE)
        .value("VROLL",  Modes::VROLL)
        .value("VPITCH", Modes::VPITCH)
        .value("VYAW",   Modes::VYAW)
        .value("ASURGE", Modes::ASURGE)
        .value("ASWAY",  Modes::ASWAY)
        .value("AHEAVE", Modes::AHEAVE)
        .value("AROLL",  Modes::AROLL)
        .value("APITCH", Modes::APITCH)
        .value("AYAW",   Modes::AYAW)        
        .value("CA_11",   Modes::CA_11)                
        .value("CA_12",   Modes::CA_12)                
        .value("CA_13",   Modes::CA_13)                
        .value("CA_14",   Modes::CA_14)                
        .value("CA_15",   Modes::CA_15)                
        .value("CA_16",   Modes::CA_16)                
        .value("CA_21",   Modes::CA_21)                
        .value("CA_22",   Modes::CA_22)                
        .value("CA_23",   Modes::CA_23)
        .value("CA_24",   Modes::CA_24)                
        .value("CA_25",   Modes::CA_25)                
        .value("CA_26",   Modes::CA_26)
        .value("CA_31",   Modes::CA_31)                
        .value("CA_32",   Modes::CA_32)                
        .value("CA_33",   Modes::CA_33)
        .value("CA_34",   Modes::CA_34)                
        .value("CA_35",   Modes::CA_35)                
        .value("CA_36",   Modes::CA_36)
        .value("CA_41",   Modes::CA_41)                
        .value("CA_42",   Modes::CA_42)                
        .value("CA_43",   Modes::CA_43)
        .value("CA_44",   Modes::CA_44)                
        .value("CA_45",   Modes::CA_45)                
        .value("CA_46",   Modes::CA_46)        
        .value("CA_51",   Modes::CA_51)                
        .value("CA_52",   Modes::CA_52)                
        .value("CA_53",   Modes::CA_53)
        .value("CA_54",   Modes::CA_54)                
        .value("CA_55",   Modes::CA_55)                
        .value("CA_56",   Modes::CA_56)        
        .value("CA_61",   Modes::CA_61)                
        .value("CA_62",   Modes::CA_62)                
        .value("CA_63",   Modes::CA_63)
        .value("CA_64",   Modes::CA_64)                
        .value("CA_65",   Modes::CA_65)                
        .value("CA_66",   Modes::CA_66)                        
        .value("CM_11",   Modes::CM_11)                
        .value("CM_12",   Modes::CM_12)                
        .value("CM_13",   Modes::CM_13)                
        .value("CM_14",   Modes::CM_14)                
        .value("CM_15",   Modes::CM_15)                
        .value("CM_16",   Modes::CM_16)                
        .value("CM_21",   Modes::CM_21)                
        .value("CM_22",   Modes::CM_22)                
        .value("CM_23",   Modes::CM_23)
        .value("CM_24",   Modes::CM_24)                
        .value("CM_25",   Modes::CM_25)                
        .value("CM_26",   Modes::CM_26)
        .value("CM_31",   Modes::CM_31)                
        .value("CM_32",   Modes::CM_32)                
        .value("CM_33",   Modes::CM_33)
        .value("CM_34",   Modes::CM_34)                
        .value("CM_35",   Modes::CM_35)                
        .value("CM_36",   Modes::CM_36)
        .value("CM_41",   Modes::CM_41)                
        .value("CM_42",   Modes::CM_42)                
        .value("CM_43",   Modes::CM_43)
        .value("CM_44",   Modes::CM_44)                
        .value("CM_45",   Modes::CM_45)                
        .value("CM_46",   Modes::CM_46)        
        .value("CM_51",   Modes::CM_51)                
        .value("CM_52",   Modes::CM_52)                
        .value("CM_53",   Modes::CM_53)
        .value("CM_54",   Modes::CM_54)                
        .value("CM_55",   Modes::CM_55)                
        .value("CM_56",   Modes::CM_56)        
        .value("CM_61",   Modes::CM_61)                
        .value("CM_62",   Modes::CM_62)                
        .value("CM_63",   Modes::CM_63)
        .value("CM_64",   Modes::CM_64)                
        .value("CM_65",   Modes::CM_65)                
        .value("CM_66",   Modes::CM_66)  

         // Stresses
        .value("AXIAL_LINESTRESS",       Modes::AXIAL_LINESTRESS)
        .value("PT1_LINESTRESS",         Modes::PT1_LINESTRESS)
        .value("PT2_LINESTRESS",         Modes::PT2_LINESTRESS)
        .value("PT3_LINESTRESS",         Modes::PT3_LINESTRESS)
        .value("PT4_LINESTRESS",         Modes::PT4_LINESTRESS)
        .value("BOTTOM_X_STRESS",        Modes::BOTTOM_X_STRESS)
        .value("BOTTOM_Y_STRESS",        Modes::BOTTOM_Y_STRESS)
        .value("BOTTOM_XY_STRESS",       Modes::BOTTOM_XY_STRESS)
        .value("TOP_X_STRESS",           Modes::TOP_X_STRESS)
        .value("TOP_Y_STRESS",           Modes::TOP_Y_STRESS)
        .value("TOP_XY_STRESS",          Modes::TOP_XY_STRESS)
        .value("BOTTOM_ENVELOPE_STRESS", Modes::BOTTOM_ENVELOPE_STRESS)
        .value("TOP_ENVELOPE_STRESS",    Modes::TOP_ENVELOPE_STRESS)
    ;

    py::enum_<QtfType>(m, "QtfType",
            R"rst(
            QtfType.QTF  : the object is uni-directional Qtf
            QtfType.MQTF : the object is multi-directional Qtf
            )rst")
        .value("QTF", QtfType::QTF)
        .value("MQTF", QtfType::MQTF)
    ;

    using ModesType = Eigen::Array<Modes, Eigen::Dynamic, 1> ;
    py::module::import("_Math") ; // For InterpScheme to be accessible

    WrappingDetails::declareRaoStorage<3, Rao>(m) ;
    WrappingDetails::declareHydroTF<3, Rao, RaoStorage<3, Rao> >(m) ;
    py::class_<Rao, HydroTransferFunction<RaoStorage<3, Rao> > >(m, "Rao",
            R"rst(
            Response Amplitude Operator data definition, called Rao here.

            Rao terms are obtained from a first order diffraction-radiation analysis.
            They correspond to the mean loads applied to the vessel
            when subjected to an Airy wave.
            They are calculated for given vessel motion coordinates called here modes.
            The Rao are then available for Nm modes, but also for
            a limited number Nb of incidences relative to the vessel heading,
            and of wave frequencies Nf.
            Rao is then a Nb x Nf x Nm matrix defined in the rigid body reference frame.
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      double, double, const Eigen::ArrayXd &>(),
                      "b"_a, "w"_a, "modesCoefficients"_a, "modes"_a,
                      "module"_a, "phase"_a, "refPoint"_a ,
            "waveRefPoint"_a, "forwardSpeed"_a = 0., "depth"_a = -1., "meanValues"_a = Eigen::ArrayXd::Zero(0), 
            R"rst(
            Initialisation of the Response Amplitude Operator data from
            separate amplitude and phase data arrays.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Rao data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Rao data (in rad/s).
            :param modeCoefficients: a coefficient on each of the last axis values.
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Rao axis
            :param module: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Rao amplitude terms.
            :param phase: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Rao phase terms.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Rao
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Rao data.
            :param float depth: the depth for which the Rao data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<std::complex<double>, 3> &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      double, double, Eigen::ArrayXd>(),
                      "b"_a, "w"_a, "modesCoefficients"_a, "modes"_a,
                      "cvalue"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "forwardSpeed"_a=0., "depth"_a=-1., "meanValues"_a = Eigen::ArrayXd::Zero(0),
            R"rst(
            Initialisation of the Response Amplitude Operator data from
            a complex data array.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Rao data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Rao data (in rad/s).
            :param modeCoefficients: a coefficient on each of the last axis values.
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Rao axis
            :param cvalue: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the complex Rao terms.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Rao
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Rao data.
            :param float depth: the depth for which the Rao data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      double, double, Eigen::ArrayXd>(),
                      "b"_a, "w"_a, "module"_a, "phase"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "forwardSpeed"_a=0., "depth"_a=-1., "meanValues"_a = Eigen::ArrayXd::Zero(0),
            R"rst(
            Initialisation of the Response Amplitude Operator data from
            separate amplitude and phase data arrays.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Rao data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Rao data (in rad/s).
            :param module: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Rao amplitude terms.
            :param phase: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Rao phase terms.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Rao
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Rao data.
            :param float depth: the depth for which the Rao data was computed
            )rst")
            .def(py::init<const Eigen::ArrayXd&,
                const Eigen::ArrayXd&,
                const ModesType&,
                const Eigen::Tensor<double, 3>&,
                const Eigen::Tensor<double, 3>&,
                const Eigen::Vector3d&,
                const Eigen::Vector2d&,
                double, double, Eigen::ArrayXd>(),
                "b"_a, "w"_a, "modes"_a, "module"_a, "phase"_a,
                "refPoint"_a, "waveRefPoint"_a, "forwardSpeed"_a = 0.,
                "depth"_a = -1., "meanValues"_a = Eigen::ArrayXd::Zero(0),
                R"rst(
            Initialisation of the Response Amplitude Operator data from
            separate amplitude and phase data arrays.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Rao data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Rao data (in rad/s).
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Rao axis
            :param module: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Rao amplitude terms.
            :param phase: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Rao phase terms.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Rao
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Rao data.
            :param float depth: the depth for which the Rao data was computed
            )rst")

    .def(py::init<const Eigen::ArrayXd&,
        const Eigen::ArrayXd&,
        const Eigen::Tensor<std::complex<double>, 3>&,
        const Eigen::Vector3d&,
        const Eigen::Vector2d&,
        double, double, Eigen::ArrayXd>(),
        "b"_a, "w"_a, "cvalue"_a, "refPoint"_a,
        "waveRefPoint"_a, "forwardSpeed"_a = 0., "depth"_a = -1., "meanValues"_a = Eigen::ArrayXd::Zero(0),
        R"rst(
            Initialisation of the Response Amplitude Operator data from
            a complex data array.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Rao data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Rao data (in rad/s).
            :param cvalue: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the complex Rao terms.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Rao
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Rao data.
            :param float depth: the depth for which the Rao data was computed
            )rst")
    .def(py::init<const Eigen::ArrayXd&,
        const Eigen::ArrayXd&,
        const ModesType&,
        const Eigen::Tensor<std::complex<double>, 3>&,
        const Eigen::Vector3d&,
        const Eigen::Vector2d&,
        double, double, Eigen::ArrayXd>(),
        "b"_a, "w"_a, "modes"_a, "cvalue"_a, "refPoint"_a,
        "waveRefPoint"_a, "forwardSpeed"_a = 0., "depth"_a = -1., "meanValues"_a = Eigen::ArrayXd::Zero(0),
        R"rst(
            Initialisation of the Response Amplitude Operator data from
            a complex data array.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Rao data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Rao data (in rad/s).
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Rao axis
            :param cvalue: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the complex Rao terms.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Rao
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Rao data.
            :param float depth: the depth for which the Rao data was computed
            )rst")

    .def(py::init<const std::vector<Rao>&, bool>(), "rao"_a, "checkReferencePoint"_a = true)

        .def("__getstate__", [](const Rao &p) {  return py::make_tuple( p.getHeadings(), p.getFrequencies(), p.getComplexData(), p.getReferencePoint(), p.getWaveReferencePoint(), p.getForwardSpeed() ); })
        .def("__setstate__", [](Rao &p, py::tuple t) {  new (&p) Rao( t[0].cast<Eigen::ArrayXd>(),
                                                                      t[1].cast<Eigen::ArrayXd>(),
                                                                      t[2].cast<Eigen::Tensor<std::complex<double>, 3>>(),
                                                                      t[3].cast<Eigen::Vector3d>(),
                                                                      t[4].cast<Eigen::Vector2d>(), 
                                                                      t[5].cast<double>()) ; })

        .def(py::init<Rao const &>(),
            R"rst(
            Initialisation of the Response Amplitude Operator data
            from a Response Amplitude Operator data.

            :param Rao.
            )rst")


        //getters
        .def("getComplexAtHeadings", &Rao::getComplexAtHeadings,
             "headings"_a,
             "indices"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=ComplexInterpolationStrategies::RE_IM_AMP,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Rao data at given headings.

             :param headings: a :py:class:`numpy.ndarray` containing
                 the headings (in rad) at which Rao data has to be interpolated.
             :param indices: the list of the independent headings indices in :py:attr:`values` array.
             :param interpScheme: interpolation scheme to choose within enum values.
                 Linear interpolation scheme set by default.
             :param interpStrategy: interpolation strategy to choose within enum values.
                 Interpolation done by default on real, imaginary and amplitude terms.
             :param extrapType: extrapolation type to choose within enum values.
                 Boundary extrapolation type set by default.

             :return: the :py:class:`numpy.ndarray` containing interpolated Rao complex terms
                 at given headings.
             )rst")
        .def("getRaoAtFrequencies", &Rao::getRaoAtFrequencies,
             "values"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=ComplexInterpolationStrategies::RE_IM_AMP,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Rao data at given frequencies.

             :param values: a :py:class:`numpy.ndarray` containing
                 the frequencies (in rad/s) at which Rao data has to be interpolated.
             :param interpScheme: interpolation scheme to choose within enum values.
                 Linear interpolation scheme set by default.
             :param interpStrategy: interpolation strategy to choose within enum values.
                 Interpolation done by default on real, imaginary and amplitude terms.
             :param extrapType: extrapolation type to choose within enum values.
                 Boundary extrapolation type set by default.

             :return: the :py:class:`~Spectral.Rao` interpolated at given frequencies.
             )rst")
        .def("getRaoAtHeadings", &Rao::getRaoAtHeadings,
             "values"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=ComplexInterpolationStrategies::RE_IM_AMP,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Rao data at given headings.

             :param values: a :py:class:`numpy.ndarray` containing
                the headings (in radians) at which Rao data has to be interpolated.
             :param interpScheme: interpolation scheme to choose within enum values.
                Linear interpolation scheme set by default.
             :param interpStrategy: interpolation strategy to choose within enum values.
                Interpolation done by default on real, imaginary and amplitude terms.
             :param extrapType: extrapolation type to choose within enum values.
                Boundary extrapolation type set by default.

             :return: the :py:class:`~Spectral.Rao` interpolated at given headings.
             )rst")
        .def("getRaoAtModeCoefficients", &Rao::getRaoAtModeCoefficients,
             "values"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=ComplexInterpolationStrategies::RE_IM_AMP,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Rao data at given mode coefficients.

             :param values: a :py:class:`numpy.ndarray` containing
                the coefficients at which Rao data has to be interpolated.
             :param interpScheme: interpolation scheme to choose within enum values.
                Linear interpolation scheme set by default.
             :param interpStrategy: interpolation strategy to choose within enum values.
                Interpolation done by default on real, imaginary and amplitude terms.
             :param extrapType: extrapolation type to choose within enum values.
                Boundary extrapolation type set by default.

             :return: the :py:class:`~Spectral.Rao` interpolated at given coefficients.
             )rst")
        .def("getEncFrequencies", &Rao::getEncFrequencies,
             R"rst(
             Get encounter frequencies for all Rao terms according to forward speed.

             :return: the Nb x Nf :py:class:`numpy.ndarray` containing the encounter frequencies.
             )rst")
        .def("getRaoIn2piRange", &Rao::getRaoIn2piRange,
             R"rst(Adjust the headings in $[-2\pi,0]$ or $[-\pi,\pi]$ ranges to $[0, 2\pi]$ range.
             In addition, it closes the heading set.

             :return: new constructed Rao class with the headings in the range $[0, 2\pi]$
             )rst")

        .def("getMeanValues", &Rao::getMeanValues, "return mean values" )

        .def("isReadyForSpectral", &Rao::isReadyForSpectral, "Check if RAO is ready for spectral calculation")

        .def("isReadyForInterpolation", &Rao::isReadyForInterpolation, "Check if RAO is ready for interpolation")

        .def("getdw", &Rao::getdw, "Return frequency step")

        .def("getdb", &Rao::getdb, "Return heading step")

        .def("setFrequencies", &Rao::setFrequencies, "Set frequencies")

        .def("setModes", &Rao::setModes, "Set Modes" , "modes"_a)

        .def("is360", &Rao::is360, "Check if RAO is available on 360degrees")

        //Expose attribute to python class
//        .def_readonly("freq", &PyRao::w_)
//        .def_readonly("fenc", &PyRao::we_)
//        .def_readonly("head", &PyRao::b_)
//        .def_readonly("module", &PyRao::amp_)
//        .def_readonly("phase", &PyRao::phi_)
//        .def_readonly("speed", &PyRao::speed_)
//        .def_readonly("component", &PyRao::component_)
//        .def_readonly("refPoint", &PyRao::refPoint_)
//        .def_readonly("waveRefPoint", &PyRao::waveRefPoint_)

        .def("getDerivate", &Rao::getDerivate , "n"_a = 1 , "Derivate the RAO, in place")
        .def("derivate", &Rao::derivate)
        ;

    WrappingDetails::declareRealTensorStorage<3, Qtf0>(m) ;
    WrappingDetails::declareHydroTF<3, Qtf0, RealTensorStorage<3, Qtf0> >(m) ;
    py::class_<Qtf0, HydroTransferFunction<RealTensorStorage<3, Qtf0> > >(m, "Qtf0",
            R"rst(
            Quadratic Transfer Function data definition, called Qtf0 here.

            Qtf0 terms are obtained from a second order diffraction-radiation analysis.
            They correspond to the mean loads induced to the vessel when subjected
            to an Airy wave.
            They are calculated for given vessel motion coordinates called here modes.
            The Qtf0 are then available for Nm modes, but also for
            a limited number Nb of incidences relative to the vessel heading
            and Nf wave frequencies.
            Qtf0 is then a Nb x Nf x Nm matrix defined in the rigid body reference frame.
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      double, double>(),
                      "b"_a, "w"_a, "modeCoefficients"_a, "modes"_a,
                      "values"_a, "refPoint"_a , "waveRefPoint"_a,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Quadratic Transfer Function data (diagonal terms only).

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf0 data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf0 data (in rad/s).
            :param modeCoefficients: a coefficient on each of the last axis values.
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Qtf0 axis
            :param values: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf0 values.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf0
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      double, double>(),
                      "b"_a, "w"_a, "values"_a, "refPoint"_a , "waveRefPoint"_a,
                      "forwardSpeed"_a=0, "depth"_a=-1.,
            R"rst(
            Initialisation of the Quadratic Transfer Function data (diagonal terms only).

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf0 data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf0 data (in rad/s).
            :param values: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf0 values.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf0
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<double, 3> &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      double, double>(),
                      "b"_a, "w"_a, "modes"_a,
                      "values"_a, "refPoint"_a , "waveRefPoint"_a,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Quadratic Transfer Function data (diagonal terms only).

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf0 data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf0 data (in rad/s).
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Qtf0 axis
            :param values: a Nb x Nf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf0 values.
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf0
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")

        .def(py::init<Qtf0 const &>(),
            R"rst(
            Initialisation of the Quadratic Transfer Function data (diagonal terms only)
            from a Quadratic Transfer Function data.

            :param Qtf0.
            )rst")

        //getters
        .def("getQtfAtFrequencies", &Qtf0::getQtfAtFrequencies,
             "values"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Qtf0 data at given frequencies.

             :param values: a :py:class:`numpy.ndarray` containing
                 the frequencies (in rad/s) at which Qtf0 data has to be interpolated.
             :param interpScheme: interpolation scheme to choose within enum values.
                 Linear interpolation scheme set by default.
             :param extrapType: extrapolation type to choose within enum values.
                 Boundary extrapolation type set by default.

             :return: the :py:class:`~Spectral.Qtf0` interpolated at given frequencies.
             )rst")
        .def("getAtHeadings", &Qtf0::getAtHeadings,
             "values"_a,
             "indices"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Qtf0 data at given headings.

             :param values: a :py:class:`numpy.ndarray` containing
                 the headings (in rad) at which Qtf0 data has to be interpolated.
             :param indices: the list of the independent headings indices in :py:attr:`values` array.
             :param interpScheme: interpolation scheme to choose within enum values.
                 Linear interpolation scheme set by default.
             :param extrapType: extrapolation type to choose within enum values.
                 Boundary extrapolation type set by default.

             :return: the :py:class:`numpy.ndarray` containing interpolated Qtf0 terms
                 at given headings.
             )rst")
        ;

    py::enum_<QtfMode>(m, "QtfMode",
            R"rst(
            This class proposes Qtf mode options.

            You may inform whether Qtf terms are calculated for
            summed or subtracted frequencies.
            )rst")
        .value("SUM", QtfMode::SUM)
        .value("DIFF", QtfMode::DIFF)
    ;

    py::enum_<QtfStorageType>(m, "QtfStorageType",
            R"rst(
            This class proposes different Qtf storage types.

            You may inform whether Qtf terms are given
            for a couple of frequencies or
            for a combination of frequencies and delta frequencies.
            )rst")
        .value("W_W", QtfStorageType::W_W)
        .value("W_DW", QtfStorageType::W_DW)
    ;

    py::enum_<FrequencyInterpolationStrategies>(m, "FrequencyInterpolationStrategies",
            R"rst(
            This class proposes different Qtf frequency interpolation strategies.

            You may inform whether Qtf terms have to be interpolated
            within couples of frequencies or
            within combinations of frequencies and delta frequencies.
            )rst")
        .value("W_W", FrequencyInterpolationStrategies::W_W)
        .value("W_DW", FrequencyInterpolationStrategies::W_DW)
    ;

    WrappingDetails::declareQtfStorage<4, Qtf>(m) ;
    WrappingDetails::declareHydroTF<4, Qtf, QtfStorage<4, Qtf> >(m) ;
    py::class_<Qtf, HydroTransferFunction<QtfStorage<4, Qtf> > >(m, "Qtf",
            R"rst(
            Full Quadratic Transfer Function data definition, called Qtf here.

            Qtf terms are obtained from a second order diffraction-radiation analysis.
            They correspond to the mean loads applied to the vessel
            when subjected to the action of a bichromatic wave of unitary amplitude.
            They are calculated for given vessel motion coordinates called here modes.
            The Qtf are then available for Nm modes, but also for
            a limited number Nb of incidences relative to the vessel heading,
            and couples of wave frequencies Nf x Ndf.
            Qtf is then a Nb x Nf x Ndf x Nm matrix defined in the rigid body reference frame.
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<double, 4> &,
                      const Eigen::Tensor<double, 4> &,
                      const QtfStorageType &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      const QtfMode &,
                      double, double>(),
                      "b"_a, "w"_a, "dw"_a, "modeCoefficients"_a, "modes"_a,
                      "amplitudes"_a, "phases"_a, "qtfStorageType"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "qtfMode"_a=QtfMode::DIFF,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from
            separate amplitude and phase data arrays.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf data (in rad/s).
            :param dw: a Ndf-sized :py:class:`numpy.ndarray` containing
                Nf frequencies or Ndf delta frequencies corresponding to provided Qtf data (in rad/s).
            :param modeCoefficients: a coefficient on each of the last axis values.
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Qtf axis
            :param amplitudes: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf amplitude terms.
            :param phases: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf phase terms.
            :param qtfStorageType: an enum giving the type of qtf provided (w, w) or (w, dw).
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param qtfMode: an enum giving whether frequencies are summed or subtracted.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<std::complex<double>, 4> &,
                      const QtfStorageType &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      const QtfMode &,
                      double, double>(),
                      "b"_a, "w"_a, "dw"_a, "modeCoefficients"_a, "modes"_a,
                      "reIm"_a, "qtfStorageType"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "qtfMode"_a=QtfMode::DIFF,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from
            a complex data array.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf data (in rad/s).
            :param dw: a Ndf-sized :py:class:`numpy.ndarray` containing
                Nf frequencies or Ndf delta frequencies corresponding to provided Qtf data (in rad/s).
            :param modeCoefficients: a coefficient on each of the last axis values.
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Qtf axis
            :param reIm: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the complex Qtf terms.
            :param qtfStorageType: an enum giving the type of qtf provided (w, w) or (w, dw).
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param qtfMode: an enum giving whether frequencies are summed or subtracted.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<double, 4> &,
                      const Eigen::Tensor<double, 4> &,
                      const QtfStorageType &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      const QtfMode &,
                      double, double>(),
                      "b"_a, "w"_a, "dw"_a, "modes"_a,
                      "amplitudes"_a, "phases"_a, "qtfStorageType"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "qtfMode"_a=QtfMode::DIFF,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from
            separate amplitude and phase data arrays.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf data (in rad/s).
            :param dw: a Ndf-sized :py:class:`numpy.ndarray` containing
                Nf frequencies or Ndf delta frequencies corresponding to provided Qtf data (in rad/s).
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Qtf axis
            :param amplitudes: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf amplitude terms.
            :param phases: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf phase terms.
            :param qtfStorageType: an enum giving the type of qtf provided (w, w) or (w, dw).
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param qtfMode: an enum giving whether frequencies are summed or subtracted.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const ModesType &,
                      const Eigen::Tensor<std::complex<double>, 4> &,
                      const QtfStorageType &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      const QtfMode &,
                      double, double>(),
                      "b"_a, "w"_a, "dw"_a, "modes"_a,
                      "reIm"_a, "qtfStorageType"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "qtfMode"_a=QtfMode::DIFF,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from
            a complex data array.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf data (in rad/s).
            :param dw: a Ndf-sized :py:class:`numpy.ndarray` containing
                Nf frequencies or Ndf delta frequencies corresponding to provided Qtf data (in rad/s).
            :param modes: the Nm labels (enum values) corresponding to each of the
                modes of the last Qtf axis
            :param reIm: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the complex Qtf terms.
            :param qtfStorageType: an enum giving the type of qtf provided (w, w) or (w, dw).
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param qtfMode: an enum giving whether frequencies are summed or subtracted.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::Tensor<double, 4> &,
                      const Eigen::Tensor<double, 4> &,
                      const QtfStorageType &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      const QtfMode &,
                      double, double>(),
                      "b"_a, "w"_a, "dw"_a,
                      "amplitudes"_a, "phases"_a, "qtfStorageType"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "qtfMode"_a=QtfMode::DIFF,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from
            separate amplitude and phase data arrays.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf data (in rad/s).
            :param dw: a Ndf-sized :py:class:`numpy.ndarray` containing
                Nf frequencies or Ndf delta frequencies corresponding to provided Qtf data (in rad/s).
            :param amplitudes: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf amplitude terms.
            :param phases: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the Qtf phase terms.
            :param qtfStorageType: an enum giving the type of qtf provided (w, w) or (w, dw).
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param qtfMode: an enum giving whether frequencies are summed or subtracted.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")
        .def(py::init<const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::ArrayXd &,
                      const Eigen::Tensor<std::complex<double>, 4> &,
                      const QtfStorageType &,
                      const Eigen::Vector3d &,
                      const Eigen::Vector2d &,
                      const QtfMode &,
                      double, double>(),
                      "b"_a, "w"_a, "dw"_a,
                      "reIm"_a, "qtfStorageType"_a, "refPoint"_a ,
                      "waveRefPoint"_a, "qtfMode"_a=QtfMode::DIFF,
                      "forwardSpeed"_a=0., "depth"_a=-1.,
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from
            a complex data array.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing
                the relative headings corresponding to provided Qtf data (in rad).
            :param w: a Nf-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Qtf data (in rad/s).
            :param dw: a Ndf-sized :py:class:`numpy.ndarray` containing
                Nf frequencies or Ndf delta frequencies corresponding to provided Qtf data (in rad/s).
            :param reIm: a Nb x Nf x Ndf x Nm :py:class:`numpy.ndarray`,
                containing the complex Qtf terms.
            :param qtfStorageType: an enum giving the type of qtf provided (w, w) or (w, dw).
            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the Qtf
                application point values in the rigid body reference frame.
            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated.
            :param qtfMode: an enum giving whether frequencies are summed or subtracted.
            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.
            :param float depth: the depth for which the Qtf data was computed
            )rst")

        .def(py::init<Qtf const &>(),
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data
            from a Full Quadratic Transfer Function data.

            :param Qtf.
            )rst")

        .def(py::init<MQtf const &>(),
            R"rst(
            Initialisation of the Full Quadratic Transfer Function data from a Full
            Multidirectional Quadratic Function data. (Qtf(ω,ω+dω,β) = MQtf(ω,β1=β,ω+dω,β2=β))

            :param MQtf.
            )rst")

        .def("__getstate__", [](const Qtf &p) {  return py::make_tuple(
                    p.getHeadings(),                // b                                                    t[ 0]
                    p.getFrequencies(),             // w                                                    t[ 1]
                    p.getDeltaFrequencies(),        // dw                                                   t[ 2]
                    p.getModeCoefficients(),        // modeCoefficients                                     t[ 3]
                    p.getModes(),                   // modes                                                t[ 4]
                    p.getComplexData().toWDWTensor(),             // dataReIm                                             t[ 5]
                    p.getReferencePoint(),          // refPoint                                             t[ 6]
                    p.getWaveReferencePoint(),      // waveRefPoint                                         t[ 7]
                    p.getSumMode(),                 // mode (QtfMode::DIFF or SUM)  DIFF == -1, SUM == +1   t[ 8]
                    p.getForwardSpeed(),            // forwardSpeed                                         t[ 9]
                    p.getDepth()                    // depth                                                t[10]
                    ); })
        .def("__setstate__", [](Qtf &p, py::tuple t) {  new (&p) Qtf( t[ 0].cast<Eigen::ArrayXd>(),                         // b
                                                                      t[ 1].cast<Eigen::ArrayXd>(),                         // w
                                                                      t[ 2].cast<Eigen::ArrayXd>(),                         // dw
                                                                      t[ 3].cast<Eigen::ArrayXd>(),                         // modeCoefficients
                                                                      t[ 4].cast<Eigen::Array<Modes, Eigen::Dynamic, 1>>(), // modes
                                                                      t[ 5].cast<Eigen::Tensor<std::complex<double>, 4>>(), // dataReIm
                                                                      QtfStorageType::W_DW,                                 // qtfStorageType
                                                                      t[ 6].cast<Eigen::Vector3d>(),                        // refPoint
                                                                      t[ 7].cast<Eigen::Vector2d>(),                        // waveRefPoint
                                                                      t[ 8].cast<double>() > 0 ? QtfMode::SUM : QtfMode::DIFF,             // mode
                                                                      t[ 9].cast<double>(),                                 // forwardSpeed
                                                                      t[10].cast<double>()                                  // depth
                                                                      ) ; })

        .def("getSumMode", &Qtf::getSumMode,
                R"rst(
                Return a float of value 1. if frequencies are summed,
                or a float of value -1. if frequencies are subtracted.

                :return float: 1. if sum mode, -1. if difference mode.
                )rst")
        //.def("getNDeltaFrequencies", &Qtf::getNDeltaFrequencies)
        //.def("getDeltaFrequencies", &Qtf::getDeltaFrequencies)
        //getters
        .def("getComplexAtHeading", &Qtf::getComplexAtHeading,
             "values"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=ComplexInterpolationStrategies::RE_IM_AMP,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Qtf data at given headings.

             :param values: a :py:class:`numpy.ndarray` containing
                 the headings (in rad) at which Qtf data has to be interpolated.
             :param interpScheme: interpolation scheme to choose within enum values.
                 Linear interpolation scheme set by default.
             :param interpStrategy: interpolation strategy to choose within enum values.
                 Interpolation done by default on real, imaginary and amplitude terms.
             :param extrapType: extrapolation type to choose within enum values.
                 Boundary extrapolation type set by default.

             :return: the :py:class:`~Spectral.QtfTensorComplex` complex data interpolated at given headings.
             )rst")
        .def("getQtfAtFrequencies", &Qtf::getQtfAtFrequencies,
             "values"_a,
             "dfrequencies"_a,
             "interpScheme"_a=BV::Math::Interpolators::InterpScheme::LINEAR,
             "interpStrategy"_a=ComplexInterpolationStrategies::RE_IM_AMP,
             "frequencyInterpStrategy"_a=FrequencyInterpolationStrategies::W_DW,
             "extrapType"_a=BV::Math::Interpolators::ExtrapolationType::BOUNDARY,
             R"rst(
             Interpolate Qtf data at given frequencies and delta frequencies.

             :param values: a :py:class:`numpy.ndarray` containing
                 the frequencies (in rad/s) at which Qtf0 data has to be interpolated.
             :param dfrequencies: a :py:class:`numpy.ndarray` containing
                 the delta frequencies (in rad/s) at which Qtf0 data has to be interpolated.
             :param interpScheme: interpolation scheme to choose within enum values.
                 Linear interpolation scheme set by default.
             :param interpStrategy: interpolation strategy to choose within enum values.
                 Interpolation done by default on real, imaginary and amplitude terms.
             :param frequencyInterpStrategy: frequency interpolation strategy to choose within enum values.
                 Interpolation done by default on combinations of frequencies and delta frequencies.
             :param extrapType: extrapolation type to choose within enum values.
                 Boundary extrapolation type set by default.

             :return: the :py:class:`~Spectral.Qtf` interpolated at given frequencies.
             )rst")
        ;

    py::class_<MQtf>(m, "MQtf",
            R"rst(
            Full Multidirectional or Uni-directional Quadratic Transfer Function data definition, called MQtf here.

            Qtf terms are obtained from a second order diffraction-radiation analysis.
            They correspond to the mean loads applied to the vessel
            when subjected to the action of a bichromatic wave of unitary amplitude. Each wave
            component propagates in its own direction.
            They are calculated for given vessel motion coordinates called here modes.
            The MQtf is only available for 1 mode, but also for
            a limited number Nb of incidences relative to the vessel heading,
            and couples of wave frequencies Nf x Ndf.
            MQtf is then a Nb x Nf x Ndf x Nb matrix defined in the rigid body reference frame for the MultiDirectional case
            and Nb x Nf x Ndf matrix for the Uni-directional case.
            )rst")
        .def(py::init(),
            R"rst(
            Initialisation of the empty object
            )rst")
        .def(py::init<const Eigen::ArrayXd &,                               // b
                      const Eigen::ArrayXd &,                               // w
                      const Eigen::ArrayXd &,                               // dw
                      const Eigen::Tensor<std::complex<double>,3> &,        // Qtf tensor
                      const Eigen::Vector2d &,                              // waveRefPoint  = Eigen::Vector2d::Zero()
                      const Eigen::Vector3d &,                              // refPoint      = Eigen::Vector3d::Zero()
                      const QtfMode &,                                      // qtfMode       = QtfMode::DIFF
                      const double &,                                       // depth         = -1.0
                      const double &                                        // forwardSpeed  = 0.0
                      >(),
                      "b"_a, "w"_a, "dw"_a, "qtf"_a, "waveRefPoint"_a = Eigen::Vector2d::Zero(),
                      "refPoint"_a = Eigen::Vector3d::Zero(), "qtfMode"_a = QtfMode::DIFF,
                      "depth"_a = -1.0, "forwardSpeed"_a = 0.0,
            R"rst(
            Initialisation of the Full Unidirectional Quadratic Transfer Function based on the
            Splinter interpolation tool.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing the relative headings

            :param w: a Nw-sized :py:class:`numpy.ndarray` containing the frequencies

            :param dw: a Ndw-sized :py:class:`numpy.ndarray` containing the delta (difference)) frequencies

            :param qtf: a Nb x Nw x Ndw-sized :py:class:`numpy.ndarray` containing the complex qtf values

            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated. (by default [0.0, 0.0])

            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the MQtf
                application point values in the rigid body reference frame. (by default [0.0, 0.0, 0.0])

            :param qtfMode: an enum giving whether frequencies are summed (QtfMode.SUM) or subtracted (QtfMode.DIFF).

            :param float depth: the depth for which the Qtf data was computed

            :param float forwardSpeed: the forward speed corresponding to provided Qtf0 data.

             )rst")
        .def(py::init<const Eigen::ArrayXd &,                               // b
                      const Eigen::ArrayXd &,                               // w
                      const Eigen::ArrayXd &,                               // dw
                      const Eigen::Tensor<std::complex<double>,4> &,        // Qtf tensor
                      const Eigen::Vector2d &,                              // waveRefPoint  = Eigen::Vector2d::Zero()
                      const Eigen::Vector3d &,                              // refPoint      = Eigen::Vector3d::Zero()
                      const QtfMode &,                                      // qtfMode       = QtfMode::DIFF
                      const double &,                                       // depth         = -1.0
                      const double &                                        // forwardSpeed  = 0.0
                      >(),
                      "b"_a, "w"_a, "dw"_a, "mqtf"_a, "waveRefPoint"_a = Eigen::Vector2d::Zero(),
                      "refPoint"_a = Eigen::Vector3d::Zero(), "qtfMode"_a = QtfMode::DIFF,
                      "depth"_a = -1., "forwardSpeed"_a = 0.,
            R"rst(
            Initialisation of the Full Multi-directional Quadratic Transfer Function based on the
            Splinter interpolation tool.

            :param b: a Nb-sized :py:class:`numpy.ndarray` containing the relative headings

            :param w: a Nw-sized :py:class:`numpy.ndarray` containing the frequencies

            :param dw: a Ndw-sized :py:class:`numpy.ndarray` containing the delta (difference)) frequencies

            :param mqtf: a Nb x Nw x Ndw x Nb-sized :py:class:`numpy.ndarray` containing the complex qtf values

            :param waveRefPoint: the 2-sized :py:class:`numpy.ndarray`,
                containing the point coordinates in sea surface plane at which
                the values phases were calculated. (by default [0.0, 0.0])

            :param refPoint: a 3-sized :py:class:`numpy.ndarray`, the MQtf
                application point values in the rigid body reference frame. (by default [0.0, 0.0, 0.0])

            :param qtfMode: an enum giving whether frequencies are summed (QtfMode.SUM) or subtracted (QtfMode.DIFF).

            :param depth: the depth for which the Qtf data was computed

            :param forwardSpeed: the forward speed corresponding to provided Qtf0 data.

             )rst")
        .def(py::init<MQtf const &>(),
            R"rst(
            Initialization of the Full Multidirectional Quadratic Transfer Function data
            from another Full Multidirectional Quadratic Transfer Function data.

            :param MQtf.
            )rst")
        .def(py::init<Qtf const &>(),
            R"rst(
            Initialization of the Full Multidirectional Quadratic Transfer Function data
            from a Unidirectional Quadratic Transfer Function data.

            :param Qtf.
            )rst")
        .def("getForwardSpeed", &MQtf::getForwardSpeed,
             R"rst(
             Return the value of the Forward Speed
             )rst")
        .def("getWaveRefPoint", &MQtf::getWaveRefPoint,
             R"rst(
             Return the wave reference point
             )rst")
        .def("getRefPoint", &MQtf::getRefPoint,
             R"rst(
             Return the reference point
             )rst")
        .def("getHeadings", &MQtf::getHeadings,
             R"rst(
             Return the headings
             )rst")
        .def("getFrequencies", &MQtf::getFrequencies,
             R"rst(
             Return the frequencies
             )rst")
        .def("getMQtfTensor", &MQtf::getMQtfTensor,
             R"rst(
             Return the tensor of size Nb x Nw x Ndw x Nb2,
             where Nb2 = 1 if the qtfType == QtfType.QTF
                   Nb2 = Nb if the qtfType == QtfType.MQTF
             )rst")
        .def("getRho", &MQtf::getRho,
             R"rst(
             Return the fluid density
             )rst")
        .def("getGrav", &MQtf::getGrav,
             R"rst(
             Return the gravity acceleration
             )rst")
        .def("getDepth", &MQtf::getDepth,
             R"rst(
             Return the fluid depth
             )rst")
        .def("getRefLength", &MQtf::getRefLength,
             R"rst(
             Return the reference length
             )rst")
        .def("isQtf", &MQtf::isQtf,
             R"rst(
             Return True if the qtfType == QtfType.QTF
                    False otherwise
             )rst")
        .def("isMQtf", &MQtf::isMQtf,
             R"rst(
             Return True if the qtfType == QtfType.MQTF
                    False otherwise
             )rst")
        .def("isModeSum", &MQtf::isModeSum,
             R"rst(
             Return True if the qtfMode == QtfMode.SUM
                    False otherwise
             )rst")
        .def("isModeDiff", &MQtf::isModeDiff,
             R"rst(
             Return True if the qtfMode == QtfMode.DIFF
                    False otherwise
             )rst")
        .def("getDeltaFrequencies", &MQtf::getDeltaFrequencies,
             R"rst(
             Get delta frequencies relative to Qtf data.

             :return: the Ndf :py:class:`numpy.ndarray` delta frequencies.
             )rst")
        .def("getQtfAtFrequencies", &MQtf::getQtfAtFrequencies,
             "ws"_a,
             "degree"_a = 1,
             "interpStrategy"_a = ComplexInterpolationStrategies::RE_IM_AMP,
             R"rst(
             Returns new MQtf object for a given frequencies.

             :param ws: :py:class:`numpy.ndarray` a list of frequencies for which
                    the qtf values must be calculated. The frequencies differencices are
                    calculated using ws.

             :param degree: integer value, by default = 1. Degree of the bspline interpolator

             :param interpStrategy:
                - ComplexInterpolationStrategies.RE_IM_AMP: phase is calculated
                    using Re & Im values interpolation, the amplitude is interpolated using the amplitudes
                - ComplexInterpolationStrategies.RE_IM: phase and amplitude is calculated using
                    interpolation of the real and imaginary values
                - ComplexInterpolationStrategies.AMP_PHASE: interpolator uses the amplitudes and the phases for the
                    interpolation
             )rst")
        .def("getQtfAtHeadings", &MQtf::getQtfAtHeadings,
             "hs"_a,
             "degree"_a = 1,
             "interpStrategy"_a = ComplexInterpolationStrategies::RE_IM_AMP,
             R"rst(
             Returns new MQtf object for a given headings.

             :param hs: :py:class:`numpy.ndarray` a list of headings for which
                    the qtf values must be calculated

             :param degree: integer value, by default = 1. Degree of the bspline interpolator

             :param interpStrategy:
                - ComplexInterpolationStrategies.RE_IM_AMP: phase is calculated
                    using Re & Im values interpolation, the amplitude is interpolated using the amplitudes
                - ComplexInterpolationStrategies.RE_IM: phase and amplitude is calculated using
                    interpolation of the real and imaginary values
                - ComplexInterpolationStrategies.AMP_PHASE: interpolator uses the amplitudes and the phases for the
                    interpolation
             )rst")
        .def("getQtfAt", py::overload_cast<const Eigen::Vector3d &,
                                           const int &,
                                           const ComplexInterpolationStrategies &>(&MQtf::getQtfAt, py::const_),
             "pt"_a,
             "degree"_a = 1,
             "interpStrategy"_a = ComplexInterpolationStrategies::RE_IM_AMP,
             R"rst(
             )rst")
        .def("getQtfAt", py::overload_cast<const Eigen::Vector4d &,
                                           const int &,
                                           const ComplexInterpolationStrategies &>(&MQtf::getQtfAt, py::const_),
             "pt"_a,
             "degree"_a = 1,
             "interpStrategy"_a = ComplexInterpolationStrategies::RE_IM_AMP,
             R"rst(
             )rst")
        .def("getQtfAt", py::overload_cast<const Eigen::ArrayXXd &,
                                           const int &,
                                           const ComplexInterpolationStrategies &>(&MQtf::getQtfAt, py::const_),
             "pts"_a,
             "degree"_a = 1,
             "interpStrategy"_a = ComplexInterpolationStrategies::RE_IM_AMP,
             R"rst(
             )rst")
        .def("getQtfAt", py::overload_cast<const Eigen::ArrayXd &,
                                           const Eigen::ArrayXd &,
                                           const Eigen::ArrayXd &,
                                           const int &,
                                           const ComplexInterpolationStrategies &>(&MQtf::getQtfAt, py::const_),
             "hs"_a,
             "ws"_a,
             "dws"_a,
             "degree"_a = 1,
             "interpStrategy"_a = ComplexInterpolationStrategies::RE_IM_AMP,
             R"rst(
             Returns new MQtf object for a given headings, frequencies and differences of the frequencies.
             This method is the general. getQtfQtFrequencies and getQtfAtHeadings are uses this method.

             :param hs: :py:class:`numpy.ndarray` a list of headings for which
                    the qtf values must be calculated

             :param ws: :py:class:`numpy.ndarray` a list of frequencies for which
                    the qtf values must be calculated.

             :param dws: :py:class:`numpy.ndarray` a list of the delta frequencies.

             :param degree: integer value, by default = 1. Degree of the bspline interpolator

             :param interpStrategy:
                - ComplexInterpolationStrategies.RE_IM_AMP: phase is calculated
                    using Re & Im values interpolation, the amplitude is interpolated using the amplitudes
                - ComplexInterpolationStrategies.RE_IM: phase and amplitude is calculated using
                    interpolation of the real and imaginary values
                - ComplexInterpolationStrategies.AMP_PHASE: interpolator uses the amplitudes and the phases for the
                    interpolation
             )rst")
        .def("writeHHTensor", &MQtf::writeHeadHeadTensor,
             "filename"_a)
        .def("writeWdWTensor", &MQtf::writeWdWTensor,
             "filename"_a)
        .def("readQtfFromFile", &MQtf::readQtfFromFile,
             "filename"_a)
        .def("writeQtfToFile", &MQtf::writeQtfToFile,
             "filename"_a)
        .def("getSumMode", &MQtf::getSumMode)
        .def("getMode", &MQtf::getMode)
        .def("getNModes", &MQtf::getNModes)
        .def("getModes", &MQtf::getModes)
        .def("getDwMax", &MQtf::getDwMax)
        ;
}
