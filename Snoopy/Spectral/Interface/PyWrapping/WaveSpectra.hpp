#pragma once

#include "Spectral/Wallop.hpp"
#include "Spectral/Jonswap.hpp"
#include "Spectral/PiersonMoskowitz.hpp"
#include "Spectral/OchiHubble.hpp"
#include "Spectral/SimpleOchiHubble.hpp"
#include "Spectral/Gauss.hpp"
#include "Spectral/Gamma.hpp"
#include "Spectral/Torsethaugen.hpp"
#include "Spectral/SimpleTorsethaugen.hpp"
#include "Spectral/WhiteNoise.hpp"
#include "Spectral/LogNormal.hpp"
#include "Spectral/WaveTabulatedSpectrum.hpp"
#include "Spectral/Spreading.hpp"
#include "Interface/PyWrapping/WindSpectra.hpp"



namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;

class PySpreading : public Spreading
{
public:
    using Spreading::Spreading;
    using Spreading::value_;
    using Spreading::meanAngle_;

    double compute(double angle) const override {
        PYBIND11_OVERLOAD_PURE(double, Spreading, compute, angle); }

    double getStd() const override {
        PYBIND11_OVERLOAD_PURE( double , Spreading, getStd, ); }
};

class PyWaveSpectrum : public WaveSpectrum
{
public:
    using WaveSpectrum::WaveSpectrum;
    using WaveSpectrum::spreading;
    using WaveSpectrum::tailOrder_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override {
        PYBIND11_OVERLOAD_PURE(
            Eigen::ArrayXd,    /* Return type */
            WaveSpectrum,      /* Parent class */
            compute,           /* Name of function in C++ (must match Python name) */
            w                  /* Argument(s) */
        );
    }

    Eigen::ArrayXXd integrate_partial_moments(const Eigen::Ref<const Eigen::ArrayXd>& w, const int& maxMoment, const double& dw = 0.1) const override {
        PYBIND11_OVERLOAD_PURE(
            Eigen::ArrayXXd,    /* Return type */
            WaveSpectrum,      /* Parent class */
            integrate_partial_moments,           /* Name of function in C++ (must match Python name) */
            w, maxMoment, dw         /* Argument(s) */
        );
    };
        

    double getTz() const override { PYBIND11_OVERLOAD( double , WaveSpectrum, getTz, );   }
    double getTm() const override { PYBIND11_OVERLOAD( double , WaveSpectrum, getTm, );   }
    double getHs() const override { PYBIND11_OVERLOAD( double , WaveSpectrum, getHs, );   }
    double getTp() const override { PYBIND11_OVERLOAD(double, WaveSpectrum, getTp, );  }

    double getMoment(int i) const override {
    PYBIND11_OVERLOAD( double , WaveSpectrum, getMoment , i );   }
};

class PyParametricSpectrum : public ParametricSpectrum
{
public:
    using ParametricSpectrum::ParametricSpectrum;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override {
        PYBIND11_OVERLOAD_PURE( Eigen::ArrayXd, ParametricSpectrum, compute, w ); }

    std::vector<double> getCoefs() const override {
        PYBIND11_OVERLOAD_PURE( std::vector<double> , ParametricSpectrum, getCoefs, ); }


    double getTz() const override { PYBIND11_OVERLOAD(double, ParametricSpectrum, getTz, ); }
    double getTm() const override { PYBIND11_OVERLOAD(double, ParametricSpectrum, getTm, ); }
    double getHs() const override { PYBIND11_OVERLOAD(double, ParametricSpectrum, getHs, ); }
    double getTp() const override { PYBIND11_OVERLOAD(double, ParametricSpectrum, getTp, ); }

    std::string print() const override {
        PYBIND11_OVERLOAD_PURE( std::string, ParametricSpectrum, print, );
    }
};



class PyWaveTabulatedSpectrum : public WaveTabulatedSpectrum
{
public:
    using WaveTabulatedSpectrum::WaveTabulatedSpectrum;
    double getTz() const override { PYBIND11_OVERLOAD(double, WaveTabulatedSpectrum, getTz, ); }
    double getTm() const override { PYBIND11_OVERLOAD(double, WaveTabulatedSpectrum, getTm, ); }
    double getHs() const override { PYBIND11_OVERLOAD(double, WaveTabulatedSpectrum, getHs, ); }
    double getTp() const override { PYBIND11_OVERLOAD(double, WaveTabulatedSpectrum, getTp, ); }
};

void InitWaveSpectra(py::module & m)
{
    py::enum_<SpreadingType>(m, "SpreadingType")
        .value("No", SpreadingType::No)
        .value("Cosn", SpreadingType::Cosn)
        .value("Cos2s", SpreadingType::Cos2s)
        .value("Wnormal", SpreadingType::Wnormal)
       ;

    py::class_<Spreading, std::shared_ptr<Spreading>, PySpreading> spreading(m, "Spreading");
    spreading
        .def("compute", py::overload_cast<double>(&Spreading::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::ArrayXd&>(&Spreading::compute, py::const_))
        .def("getStd", &Spreading::getStd)
        .def("getSwanDeg", &Spreading::getSwanDeg, "n"_a = 200, "Number of integration point")
        .def_readwrite("value", &PySpreading::value_)
        .def_readwrite("meanAngle", &PySpreading::meanAngle_)
        ;

    py::class_<Cosn, std::shared_ptr<Cosn> > (m, "Cosn", spreading)
        .def(py::init<double, double>(), "n"_a, "meanAngle"_a, "Cosn spreading, angle in radians")
        .def_static("coef_to_std", &Cosn::coef_to_std, "n"_a, "Approximate standard deviation from n")
        .def_static("std_to_coef", &Cosn::std_to_coef, "std"_a, "Approximate n from standard deviation")
        .def_static("getMaxCoef", &Cosn::getMaxCoef )
        .def_static("getMinCoef", &Cosn::getMinCoef )
        .def_static("getType", &Cosn::getType)
        ;

    py::class_<Cos2s, std::shared_ptr<Cos2s>>(m, "Cos2s", spreading)
        .def(py::init<double, double>(), "s"_a, "meanAngle"_a, "Cos2s spreading, angle in radians")
        .def_static("coef_to_std", &Cos2s::coef_to_std, "s"_a, "Approximate standard deviation from s")
        .def_static("std_to_coef", &Cos2s::std_to_coef, "std"_a, "Approximate s from standard deviation")
        .def_static("getMaxCoef", &Cos2s::getMaxCoef )
        .def_static("getMinCoef", &Cos2s::getMinCoef )
        .def_static("getType", &Cos2s::getType)
        ;

    py::class_<Wnormal, std::shared_ptr<Wnormal>>(m, "Wnormal", spreading)
        .def(py::init<double, double, int >(), "sigma"_a, "meanAngle"_a, "k_max"_a = 2,  "Wnormal spreading, angle in radians")
        .def_static("coef_to_std", &Wnormal::coef_to_std, "std"_a, "1 to 1")
        .def_static("std_to_coef", &Wnormal::std_to_coef, "std"_a, "1 to 1")
        .def_static("getMaxCoef", &Wnormal::getMaxCoef )
        .def_static("getMinCoef", &Wnormal::getMinCoef )
        .def_static("getType", &Wnormal::getType)
        ;

    py::class_<NoSpread, std::shared_ptr<NoSpread>>(m, "NoSpread", spreading)
        .def(py::init<double>(),"meanAngle"_a, "No spreading")
        .def_static("getMaxCoef", &NoSpread::getMaxCoef)
        .def_static("getMinCoef", &NoSpread::getMinCoef)
        .def_static("getType", &NoSpread::getType)
        ;

    py::class_<WaveSpectrum, std::shared_ptr<WaveSpectrum>, PyWaveSpectrum, Spectrum> waveSpectrum(m, "WaveSpectrum");
    waveSpectrum
        .def(py::init<std::string, double, SpreadingType, double>())
        .def(py::init<const PyWaveSpectrum&>())
        .def("getSpreadingFunctionName", &WaveSpectrum::getSpreadingFunctionName)
        .def("integrate_partial_moments", &WaveSpectrum::integrate_partial_moments)

#if defined(_MSC_VER)
        .def("compute", [](const WaveSpectrum & spectrum, double val){return spectrum.compute(val) ;})
        .def("compute", [](const WaveSpectrum & spectrum, const Eigen::Ref<const Eigen::ArrayXd>& ar){return spectrum.compute(ar) ;})
        .def("compute", [](const WaveSpectrum & spectrum, const Eigen::Ref<const Eigen::ArrayXd>& ar, double val){return spectrum.compute(ar, val) ;})
        .def("compute", [](const WaveSpectrum & spectrum, const Eigen::Ref<const Eigen::ArrayXd>& ar,
                           const Eigen::Ref<const Eigen::ArrayXd>& ar2){return spectrum.compute(ar, ar2) ;})
#else
        .def("compute", py::overload_cast<double>(&WaveSpectrum::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&>(&WaveSpectrum::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, double>(&WaveSpectrum::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&>(&WaveSpectrum::compute, py::const_))
#endif
        .def("computeSpreading", py::overload_cast<double>(&WaveSpectrum::computeSpreading, py::const_), "headings"_a, "Return density integrated in frequency")
        .def("getSt", &WaveSpectrum::getSt,  "Return Steepness")
        .def("getHs", &WaveSpectrum::getHs,  "Return significant wave height")
        .def("getTp", &WaveSpectrum::getTp,  "Return peak period")
        .def("getTz", &WaveSpectrum::getTz,  "Return mean up-crossing period")
        .def("getTm", &WaveSpectrum::getTm,  "Return mean period")
        .def("getSt_Tm", &WaveSpectrum::getSt_Tm, "Return steepness based on mean period")
        .def("getUr", &WaveSpectrum::getUr, "Return Ursell number (related to water-depth)")
        .def("getForristall_alpha_beta", &WaveSpectrum::getForristall_alpha_beta, "depth"_a = 0., "Return Forristall alpha and beta coefficient")
        .def("getMoment", &WaveSpectrum::getMoment,  "Return spectral moment (analytical for Wallop, Ochi-Hubble and Pierson-Moskowitz. Regression for Jonswap)")
        .def("getSpreadingValue", &WaveSpectrum::getSpreadingValue)
        .def("getSpreadingType", &WaveSpectrum::getSpreadingType)
        .def_readonly("spreading", &PyWaveSpectrum::spreading)
        .def_readonly("tailOrder", &PyWaveSpectrum::tailOrder_)

        

        .def("get_wrange", &WaveSpectrum::get_wrange, "ratio"_a = 0.99 , R"rst(
            Return range to get the main part of the wave spectrum (approximate).
            More precise range can be obtained with.energyRange()

            Parameters
            ----------
            energyRatio : float, optional
                Amount of energy to be preserved. The default is 0.99.

            Returns
            --------
            tuple
                Frequency range
            )rst")

        //Inherited from Spectrum:
        //.def_readwrite("heading", &WaveSpectrum::heading)
        //.def("energyRange", &WaveSpectrum::energyRange, "ratio"_a , "wmin"_a = 0.1, "wmax"_a = 2.5 , "dw"_a = 0.001,  "Return range that contains x % of the energy")
        ;

    py::class_<ParametricSpectrum, std::shared_ptr<ParametricSpectrum>, PyParametricSpectrum> parametricSpectrum(m, "ParametricSpectrum", waveSpectrum);
    parametricSpectrum
        .def(py::init<const char*, double, SpreadingType, double>())
        .def_static("getNParams", &ParametricSpectrum::getNParams)
        .def_static("getCoefs_0", &ParametricSpectrum::getCoefs_0)
        .def_static("getCoefs_min", &ParametricSpectrum::getCoefs_min)
        .def_static("getCoefs_max", &ParametricSpectrum::getCoefs_max)
        .def_static("getCoefs_name", &ParametricSpectrum::getCoefs_name)
        .def("__str__", [](const ParametricSpectrum &p) { return p.print(); })
        .def("getCoefs", &ParametricSpectrum::getCoefs)
        .def("__getstate__", [](const ParametricSpectrum &p) {  return py::make_tuple( p.getCoefs(), p.heading, p.getSpreadingType(), p.getSpreadingValue() );})
        ;

    py::class_<Wallop, std::shared_ptr<Wallop>>wallop(m, "Wallop", parametricSpectrum,
            R"rst(
            The expression of the wave energy form of a Wallop spectrum can be given as follows:

            .. math::

               S_{\omega}(\omega)=A\frac{H_S^2}{\omega_P}\left(\frac{\omega}{\omega_P}\right)^{-m} exp\left[-B\left(\frac{\omega}{\omega_P}\right)^{-q}\right]

            with 

            .. math:: B=\frac{m}{q}
            .. math:: A=\frac{q}{16}\left(\frac{m}{q}\right)^{\frac{m-1}{q}}/\Gamma\left(\frac{m-1}{q}\right)


            For example, the Pierson-Moskowitz spectrum is a Wallop spectrum
            with :math:`m=5` and :math:`q=4`. Others as ISSC and ITTC are derived from this formulation.
            )rst");
        wallop
        .def(py::init<double, double, double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "m"_a, "q"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Wallop wave spectrum.

            :param float hs: the significant wave height;
            :param float tp: the peak period;
            :param float m: the first shape parameter;
            :param float q: the second shape parameter;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs", &Wallop::hs_)
        .def_readwrite("tp", &Wallop::tp_)
        .def_readwrite("m", &Wallop::m_)
        .def_readwrite("q", &Wallop::q_)
        .def_readonly_static("name", &Wallop::name)
        .def_static("getNParams", &Wallop::getNParams)
        .def_static("getCoefs_0", &Wallop::getCoefs_0)
        .def_static("getCoefs_min", &Wallop::getCoefs_min)
        .def_static("getCoefs_max", &Wallop::getCoefs_max)
        .def_static("getCoefs_name", &Wallop::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                     {
                                        std::vector<std::string> attrs {"hs", "tp", "m", "q", "heading"} ;
                                        return attrs ;
                                     })
        ;

        
        py::class_<LogNormal, std::shared_ptr<LogNormal>>lognormal(m, "LogNormal", parametricSpectrum,
            R"rst(
            Log-Normal spectrum
.. math:: S(\omega) = \frac{ \omega_p m_0 } { \omega \sigma \sqrt{ 2\pi } } \exp^{ -\frac{ \log(\omega) - \mu } { 2 \sigma^2 }}
.. math:: \mu = \log(\omega_p) + \sigma^2
            )rst");
        lognormal
            .def(py::init<double, double, double,  double, SpreadingType, double>(),
                "hs"_a, "tp"_a, "sigma"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
                R"rst(
            Define a log-normal wave spectrum.

            Parameters
            ----------
            hs : float
                Significant wave height
            tp : float
                Peak period
            sigma : float
                spectral bandwith : std of log(Sw)
            heading : float, optional
                Heading (in rad). The default is 0.0.
            spreading_type : SpreadingType
                Spreading formulation (Cosn, Cos2s, Wnormal)
            spreading_value : float
                Spreading value
            )rst")

            .def("getStd", &LogNormal::getStd)
            .def_readonly("mu", &LogNormal::mu_)
            .def_readonly("sigma", &LogNormal::sigma_)
            .def_static("sigma_from_goda", &LogNormal::sigma_from_goda)
            .def_static("goda_from_sigma", &LogNormal::goda_from_sigma)
            .def_readonly_static("name", &LogNormal::name)
            .def_static("getNParams", &LogNormal::getNParams)
            .def_static("getCoefs_0", &LogNormal::getCoefs_0)
            .def_static("getCoefs_min", &LogNormal::getCoefs_min)
            .def_static("getCoefs_max", &LogNormal::getCoefs_max)
            .def_static("getCoefs_name", &LogNormal::getCoefs_name)
            .def_static("getCoefs_attr", []()
                                        {
                                            std::vector<std::string> attrs {"hs", "tp", "sigma", "heading"} ;
                                            return attrs ;
                                        })
            ;

    py::class_<Jonswap, std::shared_ptr<Jonswap>>(m, "Jonswap", parametricSpectrum,
            R"rst(Jonswap spectrum

.. math::

 S_w(\omega)=A * \frac{5}{16} \ H_s^2 \ \omega_p^{4} \ \omega^{-5} exp\left[ -\frac{5}{4}\left(\frac{\omega}{\omega_P}\right) ^{-4}\right]\gamma^{\left[exp\left(\frac{-(\omega-\omega_P)^2}{2\sigma^2\omega^2_P}\right)\right]}
         )rst")

        .def(py::init<double, double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "gamma"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(Define a Jonswap wave spectrum.
    
            Parameters
            ----------
            hs : float
                Significant wave height
            tp : float
                Peak period
            gamma : float
                Peak enhancment factor
            heading : float, optional
                Heading (in rad). The default is 0.0.
            spreading_type : SpreadingType, optional
                Spreading formulation (Cosn, Cos2s, Wnormal, or No). The default is sp.SpreadingType.No
            spreading_value : float, optional
                Spreading value

            )rst")
        .def_readwrite("hs", &Jonswap::hs_)
        .def_readwrite("tp", &Jonswap::tp_)
        .def_readwrite("gamma", &Jonswap::gamma_)
        .def_readwrite("sigma_a", &Jonswap::sigmaA_)
        .def_readwrite("sigma_b", &Jonswap::sigmaB_)
        .def_readonly_static("name", &Jonswap::name)
        .def_static("getNParams", &Jonswap::getNParams)
        .def_static("getCoefs_0", &Jonswap::getCoefs_0)
        .def_static("getCoefs_min", &Jonswap::getCoefs_min)
        .def_static("getCoefs_max", &Jonswap::getCoefs_max)
        .def_static("getCoefs_name", &Jonswap::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "gamma", "heading"} ;
                                        return attrs ;
                                    })

        .def_static("tp2tz", py::vectorize(&Jonswap::tp2tz) , "tp"_a , "gamma"_a, "Convert from Tp to Tz")
        .def_static("tz2tp", py::vectorize(&Jonswap::tz2tp) , "tz"_a , "gamma"_a, "Convert from Tz to Tp")
        .def_static("tp2tm", py::vectorize(&Jonswap::tp2tm) , "tp"_a , "gamma"_a, "Convert from Tp to Tm")
        .def_static("tm2tp", py::vectorize(&Jonswap::tm2tp) , "tz"_a , "gamma"_a, "Convert from Tm to Tp")
        .def_static("tp2t0m1", py::vectorize(&Jonswap::tp2t0m1), "tp"_a, "gamma"_a, "Convert from Tp to T0m1")
        .def_static("t0m12tp", py::vectorize(&Jonswap::t0m12tp), "t0m1"_a, "gamma"_a, "Convert from T0m1 to Tp")

        .def_static("tz2t0m1", py::vectorize(&Jonswap::tz2t0m1), "tz"_a, "gamma"_a, "Convert from Tz to T0m1")
        .def_static("t0m12tz", py::vectorize(&Jonswap::t0m12tz), "t0m1"_a, "gamma"_a, "Convert from T0m1 to Tz")

        .def_static("tmOverTp", py::vectorize(&Jonswap::tmOverTp) , "gamma"_a, "Return Tm/Tp, using regression tm/tp = 0.6687 + 0.1182 * gamma**0.5 - 0.01489 * gamma")
        .def_static("tzOverTp", py::vectorize(&Jonswap::tzOverTp) , "gamma"_a, "Return Tz/Tp, using regression tz/tp = 0.6063 + 0.1164 * gamma**0.5 - 0.01224 * gamma")
        .def_static("tzOverTm", py::vectorize(&Jonswap::tzOverTm) , "gamma"_a, "Return Tz/Tm, using regression.")
        .def_static("t0m1OverTp", py::vectorize(&Jonswap::t0m1OverTp), "gamma"_a, "Return T0m1/Tp, using regression t0m1/tp = 0.849 + 0.0099 / gamma + 0.0477 * log(gamma) - 0.00170 * gamma")

        .def_static("tmOverTp2gamma", py::vectorize(&Jonswap::tmOverTp2gamma) , "gamma"_a, "Return gamma")
        .def_static("tzOverTp2gamma", py::vectorize(&Jonswap::tzOverTp2gamma) , "gamma"_a, "Return gamma")
        .def_static("tzOverTm2gamma", py::vectorize(&Jonswap::tzOverTm2gamma) , "gamma"_a, "Return gamma")
        
        .def("__setstate__", [](Jonswap &p, py::tuple t) {  auto coefs = t[0].cast<Eigen::ArrayXd>();  new (&p) Jonswap(coefs[0], coefs[1], coefs[2], t[1].cast<double>(), t[2].cast<SpreadingType>(), t[3].cast<double>()); })
        ;

    py::class_<PiersonMoskowitz, std::shared_ptr<PiersonMoskowitz>>(m, "PiersonMoskowitz", parametricSpectrum,
            R"rst(
            The energy density function of the Pierson-Moskowitz spectrum is defined as follows:

            .. math::

               S_{PM}(\omega)=\frac{4\pi^3H_S^2}{T_Z^4}\frac{1}{\omega^5}
               exp(-\frac{16\pi^3}{T_Z^4}\frac{1}{\omega^4})


            where:

            - :math:`\omega` is the circular frequency :math:`(rad/s)`;
            - :math:`S_{PM}` is the wave spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`H_S` is the significant wave height (m);
            - :math:`T_Z` is the zero-up crossing period.

           )rst")
        .def(py::init<double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Pierson-Moskowitz wave spectrum.

            :param float hs: the significant wave height;
            :param float tp: the peak period;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs", &PiersonMoskowitz::hs_)
        .def_readwrite("tp", &PiersonMoskowitz::tp_)
        .def_readonly_static("name", &PiersonMoskowitz::name)
        .def_static("getNParams", &PiersonMoskowitz::getNParams)
        .def_static("getCoefs_0", &PiersonMoskowitz::getCoefs_0)
        .def_static("getCoefs_min", &PiersonMoskowitz::getCoefs_min)
        .def_static("getCoefs_max", &PiersonMoskowitz::getCoefs_max)
        .def_static("getCoefs_name", &PiersonMoskowitz::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "heading"} ;
                                        return attrs ;
                                    })
        .def_static("tp2tz", &PiersonMoskowitz::tp2tz , "tp"_a , "Convert from Tp to Tz")
        .def_static("tz2tp", &PiersonMoskowitz::tz2tp , "tz"_a , "Convert from Tz to Tp")
        .def("__setstate__", [](PiersonMoskowitz &p, py::tuple t) {  auto coefs = t[0].cast<Eigen::ArrayXd>();  new (&p) PiersonMoskowitz(coefs[0], coefs[1], t[1].cast<double>(), t[2].cast<SpreadingType>(), t[3].cast<double>()); })
        ;

    py::class_<OchiHubble, std::shared_ptr<OchiHubble>>(m, "OchiHubble", parametricSpectrum,
            R"rst(
            The energy density function of the Ochi-Hubble spectrum is defined as follows:

            .. math::

               S_{O-H}(\omega) = \frac{1}{4}\sum_{i=1}^{2}\left( \left(\lambda_i+\frac{1}{4}\right)\omega_{p,i}^4 \right)^{\lambda_i}
               \frac{H_{S,i}^2}{\Gamma(\lambda_i)\omega^{4\lambda_i+1}}\exp\left( -\left(\lambda_i+\frac{1}{4}\right)
               \left( \frac{\omega_{p,i}}{\omega} \right)^4\right)

            where:

            - :math:`\omega` is the circular frequency :math:`(rad/s)`;
            - :math:`S_{O-H}` is the wave spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`H_S` is the significant wave height (m);
            - :math:`\omega_p` is the peak wave circular frequency (:math:`rad/s`) and is equal to: :math:`\omega_p = \frac{2\pi}{T_p}`;
            - :math:`T_p` is the peak wave period (s);
            - :math:`\lambda` is the spectral shape parameter (peakedness parameter).

            Each part of the Ochi-Hubble spectrum is a Wallop spectrum with :math:`m=4\lambda+1`
            and :math:`q=4`.

            Ochi and Hubble formulated double peak spectral model where
            the resulting spectrum was modeled as a superposition of two
            modified Pierson-Moskowitz spectra. As we can see, this spectrum
            has three parameters for each wave system, significant height,
            spectral peak period and the shape factor :math:`\lambda`.

            This formulation can be extended to three wave components.

            )rst")
        .def(py::init<double, double, double, double, double, double, double, SpreadingType, double>(),
            "hs1"_a, "tp1"_a, "lambda1"_a, "hs2"_a, "tp2"_a, "lambda2"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Ochi-Hubble wave spectrum.

            :param float hs1: the first significant wave height;
            :param float tp1: the first peak period;
            :param float lambda1: the first shape parameter;
            :param float hs2: the second significant wave height;
            :param float tp2: the second peak period;
            :param float lambda2: the second shape parameter;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs1", &OchiHubble::hs1_)
        .def_readwrite("tp1", &OchiHubble::tp1_)
        .def_readwrite("lambda1", &OchiHubble::lambda1_)
        .def_readwrite("hs2", &OchiHubble::hs2_)
        .def_readwrite("tp2", &OchiHubble::tp2_)
        .def_readwrite("lambda2", &OchiHubble::lambda2_)
        .def_readonly_static("name", &OchiHubble::name)
        .def_static("getNParams", &OchiHubble::getNParams)
        .def_static("getCoefs_0", &OchiHubble::getCoefs_0)
        .def_static("getCoefs_min", &OchiHubble::getCoefs_min)
        .def_static("getCoefs_max", &OchiHubble::getCoefs_max)
        .def_static("getCoefs_name", &OchiHubble::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs1", "tp1", "lambda1", "hs2", "tp2", "lambda2", "heading"} ;
                                        return attrs ;
                                    })
        ;

    py::class_<SimpleOchiHubble, std::shared_ptr<SimpleOchiHubble>>(m, "SimpleOchiHubble", wallop,
            R"rst(
            The energy density function of the Ochi-Hubble spectrum is defined as follows:

            .. math::

               S_{O-H}(\omega) = \frac{1}{4}\sum_{i=1}^{2}\left( \left(\lambda_i+\frac{1}{4}\right)\omega_{p,i}^4 \right)^{\lambda_i}
               \frac{H_{S,i}^2}{\Gamma(\lambda_i)\omega^{4\lambda_i+1}}\exp\left( -\left(\lambda_i+\frac{1}{4}\right)
               \left( \frac{\omega_{p,i}}{\omega} \right)^4\right)


            where:

            - :math:`\omega` is the circular frequency :math:`(rad/s)`;
            - :math:`S_{O-H}` is the wave spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`H_S` is the significant wave height (m);
            - :math:`\omega_p` is the peak wave circular frequency (rad/s) and is equal to: :math:`\omega_p = \frac{2\pi}{T_p}`;
            - :math:`T_p` is the peak wave period (s);
            - :math:`\lambda` is the spectral shape parameter (peakedness parameter).

            Each part of the Ochi-Hubble spectrum is a Wallop spectrum with :math:`m=4\lambda+1` and :math:`q=4`.
            Ochi and Hubble formulated double peak spectral model where
            the resulting spectrum was modeled as a superposition of two
            modified Pierson-Moskowitz spectra. As we can see, this spectrum
            has three parameters for each wave system, significant height,
            spectral peak period and the shape factor :math:`\lambda`.

            This formulation can be extended to three wave components.
            )rst")
        .def(py::init<double, double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "lambda"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Simple Ochi-Hubble wave spectrum.

            :param float hs: the significant wave height;
            :param float tp: the peak period;
            :param float lambda: the shape parameter;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs", &SimpleOchiHubble::hs_)
        .def_readwrite("tp", &SimpleOchiHubble::tp_)
        .def_readwrite("lambda_", &SimpleOchiHubble::lambda_)
        .def_readonly_static("name", &SimpleOchiHubble::name)
        .def_static("getNParams", &SimpleOchiHubble::getNParams)
        .def_static("getCoefs_0", &SimpleOchiHubble::getCoefs_0)
        .def_static("getCoefs_min", &SimpleOchiHubble::getCoefs_min)
        .def_static("getCoefs_max", &SimpleOchiHubble::getCoefs_max)
        .def_static("getCoefs_name", &SimpleOchiHubble::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "lambda", "heading"} ;
                                        return attrs ;
                                    })
        .def("__setstate__", [](SimpleOchiHubble &p, py::tuple t) {  auto coefs = t[0].cast<Eigen::ArrayXd>();  new (&p) SimpleOchiHubble(coefs[0], coefs[1], coefs[2], t[1].cast<double>(), t[2].cast<SpreadingType>(), t[3].cast<double>()); })
        ;

    py::class_<Gauss, std::shared_ptr<Gauss>>(m, "Gauss", parametricSpectrum,
            R"rst(
            The energy density function of the Gaussian spectrum is defined as follows:

            .. math::

               S_{G}(\omega) = \frac{H_S^{2}}{16\sigma_{\omega}}\phi\left( \frac{\omega-\omega_p}{\sigma_{\omega}}\right)


            where:

            - :math:`\omega` is the circular frequency :math:`(rad/s)`;
            - :math:`S_{G}` is the wave spectral density function of the spectrum :math:`(m^2/s)`;
            - :math:`H_S` is the significant wave height (m);
            - :math:`\omega_p` is the peak wave circular frequency (rad/s) and is equal to: :math:`\omega_p = \frac{2\pi}{T_p}`;
            - :math:`T_p` is the peak wave period (s);
            - :math:`\phi` is the standard normal distribution given by: :math:`\phi(x) = \frac{1}{\sqrt{2.\pi}}e^{-\frac{1}{2}x^2}`

            )rst")
        .def(py::init<double, double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "sigma"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Gauss wave spectrum.

            :param float hs: the significant wave height;
            :param float tp: the peak period;
            :param float sigma: the standard deviation;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs", &Gauss::hs_)
        .def_readwrite("tp", &Gauss::tp_)
        .def_readwrite("sigma", &Gauss::sigma_)
        .def_readonly_static("name", &Gauss::name)
        .def_static("getNParams", &Gauss::getNParams)
        .def_static("getCoefs_0", &Gauss::getCoefs_0)
        .def_static("getCoefs_min", &Gauss::getCoefs_min)
        .def_static("getCoefs_max", &Gauss::getCoefs_max)
        .def_static("getCoefs_name", &Gauss::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "sigma", "heading"} ;
                                        return attrs ;
                                    })
        ;

    py::class_<Gamma, std::shared_ptr<Gamma>>(m, "Gamma", parametricSpectrum)
        .def(py::init<double, double, double, double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "gamma"_a, "m"_a, "n"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.)
        .def_readwrite("hs", &Gamma::hs_)
        .def_readwrite("tp", &Gamma::tp_)
        .def_readwrite("gamma", &Gamma::gamma_)
        .def_readwrite("m", &Gamma::m_)
        .def_readwrite("n", &Gamma::n_)
        .def_readonly_static("name", &Gamma::name)
        .def_static("getNParams", &Gamma::getNParams)
        .def_static("getCoefs_0", &Gamma::getCoefs_0)
        .def_static("getCoefs_min", &Gamma::getCoefs_min)
        .def_static("getCoefs_max", &Gamma::getCoefs_max)
        .def_static("getCoefs_name", &Gamma::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "gamma", "m", "n", "heading"} ;
                                        return attrs ;
                                    })
        ;


    py::class_<Torsethaugen, std::shared_ptr<Torsethaugen>>(m, "Torsethaugen", parametricSpectrum,
            R"rst(
            The energy density function of the Torsethaugen spectrum is the sum of two
            JONSWAP spectra relative to wind wave and swell wave systems.
            The characteristics of each JONSWAP spectrum are deduced from two parameters: the Torsethaugen spectrum significant height :math:`H_{m0}` and the Torsethaugen spectrum natural period :math:`T_P`.
            The distinction between wind dominated and swell dominated sea states is defined by the fully developed sea where:


            .. math::

               T_P = T_f = 6.6 H_{m0}^{1/3}


            If :math:`T_P \le T_f`, the local wind sea dominates the spectral peak, otherwise it is dominated by the swell.
            The energy density function is defined as follows:

            .. math::

               S_T(\omega)=\sum_{i=1}^{2}E_i.S_i(\omega)


            where :math:`i = 1` primary sea system, :math:`i = 2` secondary sea system.

            .. math::

               E_1 = \frac{1}{16}.H_{m0, 1}^2.T_{P, 1} \text{\hspace{1cm} and \hspace{1cm}} E_2 = \frac{1}{16}.H_{m0, 2}^2.T_{P, 2}


            .. math::

               S_{1}(\omega) & = G_0.A_{\gamma}.\frac{\omega_{P 1}^{N_1}}{\omega^{N_1}}.exp(-\frac{N_1}{M_1} (\frac{\omega_{P 1}}{\omega})^{M_1}).\gamma^{exp(\frac{-(\omega/\omega_{P 1}-1)^2}{2 \sigma^2})} \\
               S_{2}(\omega) & = G_0.A_{\gamma}.\frac{\omega_{P 2}^{N_2}}{\omega^{N_2}}.exp(-\frac{N_2}{M_2} (\frac{\omega_{P 2}}{\omega})^{M_2})


            where:

            - :math:`G_0` is the normalizing factor related to the Pierson-Moskovitz wave spectrum;
            - :math:`A_{\gamma}` is the normalizing factor related to regression analyses;
            - :math:`M_i` and :math:`N_i` are spectral shape parameters;
                The spectral shape parameter :math:`M_i` is always 4 except for :math:`T_P \le T_f` and :math:`i=2`.
                In this case :math:`M_2=4(1-0.7\text{exp}\left( \frac{-H_{m0}}{3}\right)`. \\
                The spectral shape parameter :math:`N_i` is always :math:`0.5 \sqrt{H_{m0}} + 3.2`
            - :math:`\omega_{P i}` is the circular frequency corresponding to the
                peak period of the spectrum :math:`T_P` :math:`(\omega_P=2\pi/T_P` and :math:`T_P=\sqrt[4]{1.25\pi}T_Z)`;
            - :math:`\gamma_i` is the peakedness factor;
            - :math:`\sigma_i` is the peakedness scale, typically taken in the North Sea
                as :math:`\sigma_1=0.07` for :math:`\omega\le\omega_P` and as :math:`\sigma_2=0.09`
                for :math:`\omega\ge\omega_P` (the mean values of JONSWAP measurements).

            First :math:`G_0` is obtained from the relation:

            .. math::

               G_0 = \left[ \frac{1}{M} (\frac{N}{M})^{-\frac{N-1}{M}} \Gamma(\frac{N-1}{M}) \right]^{-1}


            Then :math:`A_{\gamma}` is calculated as follows:

            .. math::

               \begin{array}{lll}
               A_{\gamma} & = & \left( 1. + 4.1 . ( N - 2. M^{0.28} + 5.3 ) ^{0.96 - 1.45.M^{0.1}} . \ln(\gamma)^{f_2} \right) / \gamma \\
               f_2 & = & (2.2.M^{-3.3} + 0.57). N^{0.53 - 0.58.M^{0.37}} + 0.94 - 1.04.M^{-1.9}
               \end{array}


            A non-dimensional period scale is introduced by:

            .. math::

               \epsilon_{l u} = \frac{T_f - T_P}{T_f - T_{l u}}


            where :math:`T_{l u}=2\sqrt{H_{m0}}` if :math:`T_P \le T_f` and :math:`T_{l u}=25` if :math:`T_P > T_f`,
            defines the lower or upper value for :math:`T_P`.
            The significant wave height for each peak is given as :math:`H_{m0,1} = R_{pp} H_{m0}` and :math:`H_{m0,2}=\sqrt{1-R_{pp}^2} H_{m0}`. :math:`R_{pp}` is defined according to the coefficients :math:`A_1` and :math:`A_{10}` as follows:

            .. math::

               R_{pp}= (1 - A_{10}) \text{exp} \left( - \left( \frac{\epsilon_{l u}}{A_1}\right)^2 \right) + A_{10}


            .. math::

               \begin{array}{cc}
               A_1 = \left\{
               \begin{array}{lll}
                 0.5 & \text{if} & T_P \geq T_f \\
                 0.3 & \text{if} & T_P > T_f
               \end{array}
               \right.&
               A_{10} = \left\{
               \begin{array}{lll}
                 0.7 & \text{if} & T_P \le T_f \\
                 0.6 & \text{if} & T_P > T_f
               \end{array}
               \right. \\
               \end{array}

            The primary and secondary peakperiods are defined as:

            .. math::

               T_{p,1}= T_p


            .. math::

               T_{p,2}= \left\{
               \begin{array}{lll}
                 T_f + 2 & \text{if} & T_P \geq T_f \\
                 \text{max} \left( 2.5 \text{; } (\frac{16.s_4.0.4^N}{G_0.H_{m0, 2}})^{-1/(N-1)}   \right) & \text{if} & T_P > T_f \\
               \end{array}
               \right. \\


            With :math:`s_4` defined as:

            .. math::

               s_4 = \text{max} \left( 0.01 \text{; } 0.08.(1 - \text{exp} (- \frac{1}{3} H_{m0}))  \right)


            The peakedness parameters are defined as:

            .. math::

               \gamma_1 &= 35(1+3.5 \text{exp} (-H_{m0})) \gamma_T \\
               \gamma_2 &= 1


            where:

            .. math::

               \gamma_T = \left\{
               \begin{array}{lll}
                 \left( \frac{2 \pi H_{m0,1}}{g T_p^2} \right)^{0.857} & \text{if} & T_P \geq T_f \\
                 (1+6 \epsilon_{lu}) \left( \frac{2 \pi H_{m0}}{g T_f^2} \right)^{0.857} & \text{if} & T_P > T_f
               \end{array}
               \right. \\

            )rst")
        .def(py::init<double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Torsethaugen wave spectrum.

            :param float hs: the significant wave height;
            :param float tp: the peak period;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs", &Torsethaugen::hs_)
        .def_readwrite("tp", &Torsethaugen::tp_)
        .def_readonly_static("name", &Torsethaugen::name)
        .def_static("getNParams", &Torsethaugen::getNParams)
        .def_static("getCoefs_0", &Torsethaugen::getCoefs_0)
        .def_static("getCoefs_min", &Torsethaugen::getCoefs_min)
        .def_static("getCoefs_max", &Torsethaugen::getCoefs_max)
        .def_static("getCoefs_name", &Torsethaugen::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "heading"} ;
                                        return attrs ;
                                    })
        ;

    py::class_<SimpleTorsethaugen, std::shared_ptr<SimpleTorsethaugen>>(m, "SimpleTorsethaugen", parametricSpectrum,
            R"rst(
            The energy density function of the Torsethaugen Simplified spectrum is the sum of two
            JONSWAP spectra relative to wind wave and swell wave systems.
            The characteristics of each JONSWAP spectrum are deduced from two parameters:
            the Torsethaugen spectrum significant height :math:`H_{m0}` and the Torsethaugen spectrum natural period :math:`T_P`.
            The distinction between wind dominated and swell dominated sea states is defined by the fully developed sea where:


            .. math::

               T_P = T_f = 6.6 H_{m0}^{1/3}

            If :math:`T_P \le T_f`, the local wind sea dominates the spectral peak, otherwise it is dominated by the swell.
            The energy density function is defined as follows:

            .. math::

               S_T(\omega)=\sum_{i=1}^{2}E_i.S_{i}(\omega)


            where :math:`i = 1` primary sea system, :math:`i = 2` secondary sea system.

            .. math::

               E_1 = \frac{1}{16}.H_{m0, 1}^2.T_{P, 1} \text{\hspace{1cm} and \hspace{1cm}} E_2 = \frac{1}{16}.H_{m0, 2}^2.T_{P, 2}


            .. math::

               S_{1}(\omega) & = G_0.A_{\gamma}.\frac{\omega_{P 1}^4}{\omega^4}.exp(-(\frac{\omega_{P 1}}{\omega})^4).\gamma^{exp(\frac{-(\omega/\omega_{P 1}-1)^2}{2 \sigma^2})} \\
               S_{2}(\omega) & = G_0.A_{\gamma}.\frac{\omega_{P 2}^4}{\omega^4}.exp(-(\frac{\omega_{P 1}}{\omega})^4)


            where:

            - :math:`G_0 = 3.26`;
            - :math:`A_{\gamma} = (1+1.1.(\ln \gamma)^{1.19})/\gamma`;
            - :math:`\omega_{P i}` is the circular frequency corresponding to the
                peak period of the spectrum :math:`T_P` :math:`(\omega_P=2\pi/T_P` and :math:`T_P=\sqrt[4]{1.25\pi}T_Z)`;
            - :math:`\gamma_i` is the peakedness factor;
            - :math:`\sigma_i` is the peadedness scale, typically taken in the North Sea
                as :math:`\sigma_1=0.07` for :math:`\omega\le\omega_P` and as :math:`\sigma_2=0.09`
                for :math:`\omega\ge\omega_P` (the mean values of JONSWAP measurements);

            A non-dimensional period scale is introduced by:

            .. math::

               \epsilon_{l u} = \left\{
               \begin{array}{lllll}
               1. & \text{if} & T_p \le T_f & \text{and} & T_f \le T_{l u} \\
               \frac{T_f - T_P}{T_f - T_{l u}} & \text{if} & T_p \le T_f & \text{and} & T_f > T_{l u} \\
               \frac{T_p - T_f}{T_f - T_{l u}} & \text{if} & T_p > T_f & \text{and} & T_f \le T_{l u} \\
               1. & \text{if} & T_p > T_f & \text{and} & T_f > T_{l u} \\
               \end{array}
               \right.


            where :math:`T_{l u}=2\sqrt{H_{m0}}` if :math:`T_P \le T_f` and :math:`T_{l u}=25`
            if :math:`T_P > T_f`, defines the lower or upper value for :math:`T_P`.
            The significant wave height for each peak is given
            as :math:`H_{m0,1} = R_{pp} H_{m0}` and :math:`H_{m0,2}=\sqrt{1-R_{pp}^2} H_{m0}`. :math:`R_{pp}` is defined according to the coefficients :math:`A_1` and :math:`A_{10}` as follows:

            .. math::

               R_{pp}= (1 - A_{10}) \text{exp} \left( - \left( \frac{\epsilon_{l u}}{A_1}\right)^2 \right) + A_{10}


            .. math::

               \begin{array}{cc}
               A_1 = \left\{
               \begin{array}{lll}
                 0.5 & \text{if} & T_P \geq T_f \\
                 0.3 & \text{if} & T_P > T_f
               \end{array}
               \right.&
               A_{10} = \left\{
               \begin{array}{lll}
                 0.7 & \text{if} & T_P \le T_f \\
                 0.6 & \text{if} & T_P > T_f
               \end{array}
               \right. \\
               \end{array}


            The primary and secondary peakperiods are defined as:

            .. math::

               T_{p,1}= T_p


            .. math::

               T_{p,2}= \left\{
               \begin{array}{lll}
                 T_f + 2 & \text{if} & T_P \geq T_f \\
                 T_f \text{if} & T_P > T_f
               \end{array}
               \right. \\


            Finally the peakedness parameters are defined as:

            .. math::

               \gamma_1 &= 35.\gamma_T \\
               \gamma_2 &= 1


            where:

            .. math::

               \gamma_T = \left\{
               \begin{array}{lll}
                 \left( \frac{2 \pi H_{m0,1}}{g T_p^2} \right)^{0.857} & \text{if} & T_P \geq T_f \\
                 (1+6 \epsilon_{lu}) \left( \frac{2 \pi H_{m0}}{g T_f^2} \right)^{0.857} & \text{if} & T_P > T_f
               \end{array}
               \right. \\


            )rst")
        .def(py::init<double, double, double, SpreadingType, double>(),
            "hs"_a, "tp"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(
            Define a Simple Torsethaugen wave spectrum.

            :param float hs: the significant wave height;
            :param float tp: the peak period;
            :param float heading: the wave heading in global basis;
            :param spreading type: an enum value defining the spreading type;
            :param float spreading value: the speading value;
            )rst")
        .def_readwrite("hs", &SimpleTorsethaugen::hs_)
        .def_readwrite("tp", &SimpleTorsethaugen::tp_)
        .def_readonly_static("name", &SimpleTorsethaugen::name)
        .def_static("getNParams", &SimpleTorsethaugen::getNParams)
        .def_static("getCoefs_0", &SimpleTorsethaugen::getCoefs_0)
        .def_static("getCoefs_min", &SimpleTorsethaugen::getCoefs_min)
        .def_static("getCoefs_max", &SimpleTorsethaugen::getCoefs_max)
        .def_static("getCoefs_name", &SimpleTorsethaugen::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "tp", "heading"} ;
                                        return attrs ;
                                    })
        ;

    py::class_<WhiteNoise, std::shared_ptr<WhiteNoise>>(m, "WhiteNoise", parametricSpectrum)
        .def(py::init<double, double, double, double, double, double, SpreadingType, double>(),
            "hs"_a, "w1"_a, "w2"_a, "w3"_a, "w4"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(

            )rst")
        .def_readwrite("w1", &WhiteNoise::w1_)
        .def_readwrite("w2", &WhiteNoise::w2_)
        .def_readwrite("w3", &WhiteNoise::w3_)
        .def_readwrite("w4", &WhiteNoise::w4_)
        .def_readonly_static("name", &WhiteNoise::name)
        .def_static("getNParams", &WhiteNoise::getNParams)
        .def_static("getCoefs_0", &WhiteNoise::getCoefs_0)
        .def_static("getCoefs_min", &WhiteNoise::getCoefs_min)
        .def_static("getCoefs_max", &WhiteNoise::getCoefs_max)
        .def_static("getCoefs_name", &WhiteNoise::getCoefs_name)
        .def_static("getCoefs_attr", []()
                                    {
                                        std::vector<std::string> attrs {"hs", "w1", "w2", "w3", "w4", "heading"} ;
                                        return attrs ;
                                    })
        ;

    py::class_<WaveTabulatedSpectrum, std::shared_ptr<WaveTabulatedSpectrum>, PyWaveTabulatedSpectrum>(m, "WaveTabulatedSpectrum", waveSpectrum)
        .def(py::init<const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      double, SpreadingType, double>(),
            "w"_a, "sw"_a, "heading"_a = 0., "spreading_type"_a = SpreadingType::No, "spreading_value"_a = 0.,
            R"rst(

            )rst")
        .def_readwrite("w", &WaveTabulatedSpectrum::w)
        .def_readwrite("sw", &WaveTabulatedSpectrum::sw)
        .def("__getstate__", [](const WaveTabulatedSpectrum &p) {  return py::make_tuple( p.w, p.sw, p.heading, p.getSpreadingType(), p.getSpreadingValue() );})
        .def("__setstate__", [](WaveTabulatedSpectrum &p, py::tuple t) {  new (&p) WaveTabulatedSpectrum(t[0].cast<Eigen::ArrayXd>() , t[1].cast<Eigen::ArrayXd>()  );})
        ;

}
