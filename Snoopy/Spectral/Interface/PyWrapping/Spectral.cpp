#include "Tools/TensorPythonCaster.hpp"
#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <pybind11/functional.h>

#include "Spectral/SpectralTools.hpp"
#include "Spectral/ResponseSpectrum.hpp"
#include "Spectral/SpectralMoments.hpp"
#include "Spectral/ResponseSpectrum2nd.hpp"
#include "Spectral/ResponseSpectrum2ndMQtf.hpp"
#include "Spectral/Dispersion.hpp"
#include "Spectral/spectralStats.hpp"
#include "Interface/PyWrapping/WindSpectra.hpp"
#include "Interface/PyWrapping/WaveSpectra.hpp"
#include "Interface/PyWrapping/Wif.hpp"
#include "Interface/PyWrapping/TransferFunctions.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;


// Intermediate class to be able to expose protected attribute to Python
class PyResponseSpectrum : public ResponseSpectrum
{
public:
    using ResponseSpectrum::seaState_;
    using ResponseSpectrum::rao_;
    using ResponseSpectrum::isComputed_;
};

// Intermediate class to be able to expose protected attribute to Python
class PySpectralMoments : public SpectralMoments
{
public:
    using SpectralMoments::seaStates_;
    using SpectralMoments::rao_;
    using SpectralMoments::isComputed_;
    using SpectralMoments::isM4_;
    using SpectralMoments::all_headings_;
};

// Intermediate class to be able to expose protected attribute to Python
class PyResponseSpectrum2nd : public ResponseSpectrum2nd
{
public:
    using ResponseSpectrum2nd::seaState_;
    using ResponseSpectrum2nd::qtf_;
    using ResponseSpectrum2nd::isComputed_;
};

PYBIND11_MODULE(_Spectral, m)
{
    m.doc() = "Spectral module";

    InitWindSpectra(m) ;
    InitWaveSpectra(m) ;
    InitWif(m) ;
    InitTransferFunctions(m) ;

    //TODO generalize
    #ifdef __INTEL_COMPILER
        m.def("getCompilerVersion", [] { std::stringstream ss; ss << "Intel C++ Compiler" << __INTEL_COMPILER; return ss.str(); });
    #elif _MSC_VER
        m.def("getCompilerVersion", [] { std::stringstream ss; ss << "MSVC " << _MSC_VER; return ss.str(); });
    #else
        m.def("getCompilerVersion", [] { return "Unknown compiler"; });
    #endif

	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    // Dispersion relation ship
    m.def("w2k", py::overload_cast<const Eigen::ArrayXd& , double >(&w2k) ,
    		"w"_a , "depth"_a=0.,
    	    R"rst(
    		Compute wave number k from circular frequency w. Solve dispersion relation ship :math:`w*w = g*k*\tanh(kh)`.

            :param w: the n-sized :py:class:`numpy.ndarray` circular frequencies (in rad/s).
            :param float depth: the water depth.

            :return: the calculated n-sized :py:class:`numpy.ndarray` wave numbers.
            )rst");
    m.def("w2k", py::overload_cast<double , double >(&w2k) , "w"_a , "depth"_a = 0. ,
    	    R"rst(
    		Compute wave number k from circular frequency w. Solve dispersion relation ship :math:`w*w = g*k*\tanh(kh)`.

            :param float w: the circular frequency (in rad/s).
            :param float depth: the water depth.

            :return float: the calculated wave number.
            )rst");

    m.def("k2w", py::overload_cast<const Eigen::ArrayXd& , double >(&k2w) ,
    		"k"_a , "depth"_a=0.,
    	    R"rst(
    		Compute circular frequency from wave number :math:`k`.

            :param k: the n-sized :py:class:`numpy.ndarray` wave numbers.
            :param float depth: the water depth.

            :return: the calculated n-sized :py:class:`numpy.ndarray` circular frequencies.
            )rst");
    m.def("k2w", py::overload_cast<double, double >(&k2w), "k"_a, "depth"_a = 0.,
    	    R"rst(
    		Compute circular frequency from wave number :math:`k`.

            :param float k: the wave number.
            :param float depth: the water depth.

            :return float: the calculated circular frequency.
            )rst");

    //Encounter frequency stuff
    m.def("w2we", py::overload_cast<const Eigen::ArrayXd& , const Eigen::ArrayXd& , double, double >(&w2we) ,
    		"w"_a , "b"_a, "speed"_a,  "depth"_a = -1.,
    	    R"rst(
    		Compute encounter frequency.

            Parameters
            ----------
            w : float or array
                Frequency (rad/s)
            b : float or array
                Heading (radians)
            speed : float or array
                speed (m/s)
            depth : float, optional
                water depth

            Returns
            -------
            float or array
                Encounter frequency (rad/s)
            )rst");

    m.def("w2we", py::overload_cast<double, const Eigen::ArrayXd&, const Eigen::ArrayXd&, double>(&w2we),
        "w"_a, "b"_a, "speed"_a, "depth"_a = -1.,
        R"rst(
    		Compute encounter frequency.

            Parameters
            ----------
            w : float or array
                Frequency (rad/s)
            b : float or array
                Heading (radians)
            speed : float or array
                speed (m/s)
            depth : float, optional
                water depth

            Returns
            -------
            float or array
                Encounter frequency (rad/s)
            )rst");

    m.def("w2we", py::overload_cast<const Eigen::ArrayXd& , double , double, double >(&w2we) , "w"_a , "b"_a, "speed"_a,  "depth"_a = -1.,
    	    R"rst(
    		Compute encounter frequency.

            Parameters
            ----------
            w : float or array
                Frequency (rad/s)
            b : float or array
                Heading (radians)
            speed : float or array
                speed (m/s)
            depth : float, optional
                water depth

            Returns
            -------
            float or array
                Encounter frequency (rad/s)
            )rst");
    m.def("w2we", py::overload_cast<double, double, double, double >(&w2we) , "w"_a , "b"_a, "speed"_a,  "depth"_a = -1.,
    	    R"rst(
    		Compute encounter frequency.

            Parameters
            ----------
            w : float or array
                Frequency (rad/s)
            b : float or array
                Heading (radians)
            speed : float or array
                speed (m/s)
            depth : float, optional
                water depth

            Returns
            -------
            float or array
                Encounter frequency (rad/s)
            )rst");


    // Short term statistics
    m.def("ampFromRisk", py::vectorize(&ampFromRisk), "risk"_a, "duration"_a, "m0"_a, "m2"_a,
        R"rst(
            Amplitude associated to a given risk over a given duration.

            Parameters
            ----------
            risk : float or array
                Risk (between 0. and 1.)
            duration : float
                duration, in seconds
            m0 : float
                Moment of order 0
            m2 : float
                Moment of order 2

            Returns
            -------
            float
                Amplitude
                  )rst");

    m.def("RsRtzFromM0M2", &RsRtzFromM0M2, "m0"_a, "m2"_a,
        R"rst(
    		Compute Rs (range) and Rtz from m0 and m2.

            Parameters
            ----------
            m0 : float
                Moment of order 0
            m2 : float
                Moment of order 2

            Returns
            -------
            tuple
                (Rs, Rtz)
            )rst");

    m.def("RtzFromM0M2", py::vectorize(&RtzFromM0M2), "m0"_a, "m2"_a,
        R"rst(
    		Compute Rs (range) and Rtz from m0 and m2.

            Parameters
            ----------

            m0 : float
                Moment of order 0
            m2 : float
                Moment of order 2

            Returns
            -------
            float
                Rtz
            )rst");

    m.def("m0m2FromRsRtz", &m0m2FromRsRtz, "Rs"_a, "Rtz"_a,
        R"rst(
    		Compute  m0 and m2 from Rs and Rtz.

            Parameters
            ----------
            Rs : float
                Significant response
            Rtz : float
                Mean upcrossing period

            Returns
            -------
            tuple
                (m0, m2)
            )rst");


    py::class_<ResponseSpectrum>(m, "ResponseSpectrum")
        .def(py::init< const SeaStateABC& , const Rao&, int, bool, bool>(), py::keep_alive<1, 2>(), py::keep_alive<1, 3>(), "seaState"_a, "rao"_a, "numThreads"_a = 1, "checkRao"_a = true, "computeM4"_a = false,
            R"rst(
    Construct ResponseSpectrum object
        
    Parameters
    ----------
    ss : SeaState
        The seastate

    rao : Rao
        The transfer function, should be evenly discretized, without duplicates

    Example 
    -------
    >>> responseSpectrum = sp.ResponseSpectrum( ss ,rao )
    >>> m0, m2, responseSpectrum.getM0M2()
    [ 0.5456, 0.9845 ]

        )rst")

		.def("compute", &ResponseSpectrum::get, "Compute response spectrum and moments")
        .def("get", &ResponseSpectrum::get, "Return response spectrum")
        .def("getM0", &ResponseSpectrum::getM0, "Return m0" , "imode"_a = -1 )
        .def("getM2", &ResponseSpectrum::getM2, "Return m2", "imode"_a = -1)
        .def("getM4", &ResponseSpectrum::getM4, "Return m4", "imode"_a = -1)
        .def("getM0M2", &ResponseSpectrum::getM0M2, "Return (m0,m2)", "imode"_a = -1)
        .def("getSeaState", &ResponseSpectrum::getSeaState)
        .def("getRao", &ResponseSpectrum::getRao)
        .def("getFrequencies", &ResponseSpectrum::getFrequencies)
        .def("getM0s", &ResponseSpectrum::getM0s)
        .def("getM2s", &ResponseSpectrum::getM2s)
        .def("getM4s", &ResponseSpectrum::getM4s)
        .def("getModes", &ResponseSpectrum::getModes)
        .def("compute2D", &ResponseSpectrum::compute2D)
        .def_readonly("isComputed", &PyResponseSpectrum::isComputed_)
        //.def_readonly("rao", &PyResponseSpectrum::rao_)
        //.def_readonly("seaState", &PyResponseSpectrum::seaState_)
        ;

    py::class_<SpectralMoments>(m, "SpectralMoments" )
        .def(py::init<const std::vector<std::shared_ptr<SeaState>>&,
                      const Rao&, const bool&, const double&, const double&, const double&, const double&, const int&, const double&>(),
             py::keep_alive<1, 2>(), py::keep_alive<1, 3>(),
             "seaStates"_a, "rao"_a, "computeM4"_a = false, "dw"_a = 0.005, "db"_a = 5, "w_min"_a = -1., "w_max"_a = -1., "num_threads"_a = 1, "g"_a = 9.81, R"rst(
        Spectral moment calculator.

        Parameters
        ----------
        seaStates : ([SeaState])
            list of SeaStates.

        rao : Rao
            rao.

        computeM4 : bool
            compute (True), or not (False) the 4th moment. By default it is False.

        dw  : float, optional
            Frequencies intervals used for the spectrum integration. By default this value is 0.005

        db  : float
            Tolerance value in degrees, with which two headings are considered as same heading, for example if headTol == 5 degrees,
            130 degrees and 134 degrees are considered as same value. This will allow to optimize the calculation for large list of sea states,
            with a lot spectrum with differente headings. Default value is 5 degrees

        w_min : float, optional
            Minimum frequency from which the integration must be started.
            if w_min < 0 => w_min == RAO.w_min
            if w_min < RAO.w_min => RAO(w) = RAO(RAO.w_min) for w_min <= w <= RAO.w_min

        w_max : float, optional 
            Maximum frequency up-to which the integration must be done.
            if w_max < 0 => w_max == RAO.w_max
            if w_max > RAO.w_max => RAO(w) = RAO(RAO.w_max) for RAO.w_min >= w >= w_max

        num_threads : int, optional
            Number of cores the calculation (dispatch the different sea-states on several cores).

        Example
        -------
        >>> rao = sp.Rao("heave.rao")
        >>> ss = sp.SeaState( sp.Jonswap(hs=1.0, tp = 10 , gamma = 1.5 , b = np.pi) )
        >>> sm = sp.SpectralMoments( [ss] , rao )
        >>> sm.getM0s()
        [[10.05]]

        )rst")
        .def("getM0s", &SpectralMoments::getM0s, "get the 0-th spectrum moment as 2-D array: [seaStates, modes]. The moments are computed at the first call")
        .def("getM2s", &SpectralMoments::getM2s, "get the 2-th spectrum moment as 2-D array: [seaStates, modes]. The moments are computed at the first call")
        .def("getM4s", &SpectralMoments::getM4s, "if isM4 == True, then return the 4-th spectrum moment as 2-D array: [seaStates, modes]. The moments are computed at the first call")
        .def_readonly("isComputed", &PySpectralMoments::isComputed_, "if True, the moments were computed")
        .def_readonly("isM4", &PySpectralMoments::isM4_, "returns value of computeM4. If True, M4 is (was) computed, otherwise M4 is zero")
        .def_readonly("getSpectralHeadings", &PySpectralMoments::all_headings_, "returns unique sorted list of headings from all sea states")
        ;

    py::class_<ResponseSpectrum2nd>(m, "ResponseSpectrum2nd")
        .def(py::init< const SeaState& , const Qtf&  >(), py::keep_alive<1, 2>(), py::keep_alive<1, 3>(), "seaState"_a, "qtf"_a )
        .def("get", &ResponseSpectrum2nd::get)
        .def("getFrequencies", &ResponseSpectrum2nd::getFrequencies)
        .def("getM0", &ResponseSpectrum2nd::getM0)
        .def("getM2", &ResponseSpectrum2nd::getM2)
        .def("getM0M2", &ResponseSpectrum2nd::getM0M2)
        .def("getSeaState", &ResponseSpectrum2nd::getSeaState)
        .def("getQtf", &ResponseSpectrum2nd::getQtf)
        .def("getMean", &ResponseSpectrum2nd::getMean)
        .def_readonly("isComputed", &PyResponseSpectrum2nd::isComputed_)
        .def("getFrequencies", &ResponseSpectrum2nd::getFrequencies)
        //.def_readonly("qtf", &PyResponseSpectrum2nd::qtf_)
        //.def_readonly("seaState", &PyResponseSpectrum2nd::seaState_)

        .def("getNewman", py::overload_cast<const int>(&ResponseSpectrum2nd::getNewman), "meanType"_a = 0,
            R"rst(Returns Newman approximated 2nd order Response Spectrum.

            :param int meanType, 0 by default, is geometric mean, 1 - arithmetic mean
            )rst");

    py::class_<ResponseSpectrum2ndMQtf>(m, "ResponseSpectrum2ndMQtf")
        .def(py::init<const SeaState&, const MQtf &> (), py::keep_alive<1, 2>(), py::keep_alive<1, 3>(), "seaState"_a, "qtf"_a)
        .def("get", &ResponseSpectrum2ndMQtf::get)
        .def("getFrequencies", &ResponseSpectrum2ndMQtf::getFrequencies)
        .def("getM0", &ResponseSpectrum2ndMQtf::getM0)
        .def("getM2", &ResponseSpectrum2ndMQtf::getM2)
        .def("getM0M2", &ResponseSpectrum2ndMQtf::getM0M2)
        .def("getSeaState", &ResponseSpectrum2ndMQtf::getSeaState)
        .def("getQtf", &ResponseSpectrum2ndMQtf::getQtf)
        .def("isComputed", &ResponseSpectrum2ndMQtf::isComputed);
}
