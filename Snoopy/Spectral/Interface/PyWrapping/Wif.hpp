#pragma once

#include "Spectral/SeaState.hpp"
#include "Spectral/Wif.hpp"
#include "Spectral/ParametricSpectrum.hpp"
#include "Spectral/WaveTabulatedSpectrum.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Spectral;

class PySeaStateABC : public SeaStateABC
{
public:
    using SeaStateABC::SeaStateABC;
    using SeaStateABC::probability_;

    double compute(double w) const override { PYBIND11_OVERLOAD(   double,  SeaStateABC, compute, w ); }

    double getTailOrder() const override { PYBIND11_OVERLOAD_PURE(double, SeaStateABC, getTailOrder, ); }

    bool isUnidirectional() const override { PYBIND11_OVERLOAD_PURE(bool, SeaStateABC, isUnidirectional, ); }

    bool isSpreaded() const override { PYBIND11_OVERLOAD_PURE(bool, SeaStateABC, isSpreaded, ); }

    double compute(double w, double heading) const override { PYBIND11_OVERLOAD(   double,  SeaStateABC, compute, w , heading); }
    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override { PYBIND11_OVERLOAD_PURE(   Eigen::ArrayXd,  SeaStateABC, compute, w ); }
    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double b) const override { PYBIND11_OVERLOAD_PURE(   Eigen::ArrayXd,  SeaStateABC, compute, w, b  ); }
    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& b) const override { PYBIND11_OVERLOAD_PURE(   Eigen::ArrayXd,  SeaStateABC, compute, w, b  ); }

    SeaStateType getType() const override{ PYBIND11_OVERLOAD( SeaStateType,  SeaStateABC, getType, ); }
};


// Intermediate class to be able to expose protected attribute to Python
class PySeaState2D : public SeaState2D
{
public:
    using SeaState2D::w_;
    using SeaState2D::b_;
    using SeaState2D::sw_;
};

// Intermediate class to be able to expose protected attribute to Python
class PySeaState2D_Fourier : public SeaState2D_Fourier
{
public:
    using SeaState2D_Fourier::w_;
    using SeaState2D_Fourier::a0_;
    using SeaState2D_Fourier::a1_;
    using SeaState2D_Fourier::b1_;
    using SeaState2D_Fourier::a2_;
    using SeaState2D_Fourier::b2_;
};



// Intermediate class to be able to expose protected attribute to Python
class PyWif : public Wif
{
public:
    using Wif::w_;
    using Wif::amp_;
    using Wif::phi_;
    using Wif::b_;
    using Wif::depth_;
    using Wif::isWidth_;
    using Wif::unidirectional_;
};

void InitWif(py::module & m)
{
    py::enum_<SeaStateType>(m, "SeaStateType", py::arithmetic())
        .value("Parametric", SeaStateType::Parametric)
        .value("SemiParametric", SeaStateType::SemiParametric)
        .value("Full", SeaStateType::Full)
        .export_values();

    py::class_<SeaStateABC, PySeaStateABC> seaStateABC(m, "SeaStateABC" );
    seaStateABC
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&>(&SeaStateABC::compute, py::const_))
        .def("compute", py::overload_cast<double>(&SeaStateABC::compute, py::const_), "w"_a)
        .def("compute", py::overload_cast<double, double>(&SeaStateABC::compute, py::const_), "w"_a, "heading"_a)
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, double>(&SeaStateABC::compute, py::const_))
        .def("compute", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&>(&SeaStateABC::compute, py::const_))
        .def("computeSpreading", py::overload_cast<const Eigen::ArrayXd&>(&SeaStateABC::computeSpreading, py::const_), "headings"_a, "Return density integrated in frequency")
        .def("getType", &SeaStateABC::getType)
        .def("getTailOrder", &SeaStateABC::getTailOrder)
        .def("isUnidirectional", &SeaStateABC::isUnidirectional)
        .def("isSpreaded", &SeaStateABC::isSpreaded)
        .def_readonly("probability", &PySeaStateABC::probability_)
        ;


    py::class_<SeaState, std::shared_ptr<SeaState>>(m, "SeaState", seaStateABC)
        .def(py::init<std::shared_ptr<ParametricSpectrum>, double>(), "spectrum"_a, "probability"_a = 1.)
        .def(py::init<std::vector<std::shared_ptr<ParametricSpectrum>>, double>(), "spectrums"_a, "probability"_a = 1.)
        .def(py::init<std::vector<std::shared_ptr<WaveTabulatedSpectrum>>, double>(), "spectrums"_a, "probability"_a = 1.)
        .def(py::init<std::shared_ptr<WaveTabulatedSpectrum>, double>(), "spectrum"_a, "probability"_a = 1.)
        .def("getSpectrumCount", &SeaState::getSpectrumCount)
        .def("getSpectrum", &SeaState::getSpectrum, py::return_value_policy::reference)
        .def("getHs", &SeaState::getHs, "Return total Hs (from spectra parameters)")
        ;



    py::class_<SeaState2D>(m, "SeaState2D", seaStateABC)
        .def(py::init< const Eigen::ArrayXd&  ,const Eigen::ArrayXd& ,const Eigen::ArrayXXd&  , double  >(), "w"_a , "b"_a, "sw"_a, "probability"_a = 1.0 )
        .def(py::init< const SeaStateABC& , double, double, int, int >(), "SeaState"_a , "wmin"_a = 0.1, "wmax"_a = 3.0, "nbFreq"_a = 150, "nbHead"_a = 72 ,"Tabulate the sea-state" )
        .def_readonly("w", &PySeaState2D::w_)
        .def_readonly("b", &PySeaState2D::b_)
        .def_readonly("sw", &PySeaState2D::sw_)
        .def("__getstate__", [](const SeaState2D &p) {  return py::make_tuple( p.getFrequencies(), p.getHeadings(), p.getDensity(), p.getProbability() );})
        .def("__setstate__", [](SeaState2D &p, py::tuple t) {  new (&p) SeaState2D( t[0].cast<Eigen::ArrayXd>() , t[1].cast<Eigen::ArrayXd>() , t[2].cast<Eigen::ArrayXXd>(), t[3].cast<double>() );})
        ;


    py::class_<SeaState2D_Fourier>(m, "SeaState2D_Fourier", seaStateABC)
        .def(py::init< const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, const Eigen::Ref<const Eigen::ArrayXd>&, double>(), 
            R"rst(2D spectrum described by Fourrier coefficient
            Args:
        w(array) : Frequency(rad / s).
        a0(array) : a0.
        a1(array) : a1.
        b1(array) : b1.
        a2(array) : a2.
        b2(array) : b2.
        probability(float, optional) : probability.Defaults to 1.0.

        Returns :
        None.
        )rst", "w"_a, "a0"_a, "a1"_a, "b1"_a , "a2"_a, "b2"_a, "probability"_a = 1.0 )
        .def_readonly("w", &PySeaState2D_Fourier::w_)
        .def_readonly("a0", &PySeaState2D_Fourier::a0_)
        .def_readonly("a1", &PySeaState2D_Fourier::a1_)
        .def_readonly("b1", &PySeaState2D_Fourier::b1_)
        .def_readonly("a2", &PySeaState2D_Fourier::a2_)
        .def_readonly("b2", &PySeaState2D_Fourier::b2_)

        .def("__getstate__", [](const SeaState2D_Fourier& p) {  return py::make_tuple(p.getFrequencies(), p.get_a0(), p.get_a1(), p.get_b1(), p.get_a2(), p.get_b2(), p.getProbability()); })
        .def("__setstate__", [](SeaState2D_Fourier& p, py::tuple t) {  new (&p) SeaState2D_Fourier(t[0].cast<Eigen::ArrayXd>(),
                                                                                                   t[1].cast<Eigen::ArrayXd>(),
                                                                                                   t[2].cast<Eigen::ArrayXd>(),
                                                                                                   t[3].cast<Eigen::ArrayXd>(),
                                                                                                   t[4].cast<Eigen::ArrayXd>(),
                                                                                                   t[5].cast<Eigen::ArrayXd>(),
                                                                                                   t[6].cast<double>()    ); })
        ;

    py::class_<Wif, std::shared_ptr<Wif> >(m, "Wif",
            R"rst(
            Discretized wave / wind spectrum, called Wif here.
            )rst")
        .def(py::init<Wif const &>(),
            R"rst(
            Initialisation of the discretized wave / wind spectrum Wif
            from a discretized wave / wind spectrum Wif.

            :param Wif.
            )rst")
        .def(py::init<const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>& ,
                      double>(),
                      "w"_a, "a"_a, "phi"_a, "b"_a, "depth"_a = -1.,
            R"rst(
            Construct a Wif using its parameters directly.

            :param w: a n-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Wif data (in rad/s).
            :param a: a n-sized :py:class:`numpy.ndarray` containing
                the amplitudes corresponding to provided Wif data
                (in m for wave spectrum, in m/s for wind spectrum).
            :param phi: a n-sized :py:class:`numpy.ndarray` containing
                the phases corresponding to provided Wif data (in rad).
            :param b: a n-sized :py:class:`numpy.ndarray` containing
                the headings corresponding to provided Wif data (in rad).
            :param float depth: the water depth.
            )rst")

        .def(py::init<const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      double,
                      double>(),
            "w"_a, "cos_sin"_a, "b"_a, "depth"_a = -1.,
            R"rst(
            Construct a Wif using its parameters directly.

            :param w: a n-sized :py:class:`numpy.ndarray` containing
                the frequencies corresponding to provided Wif data (in rad/s).
            :param a: a 2n-sized :py:class:`numpy.ndarray` containing cos and sin coefficients
            :param b: heading (scalar)
            :param float depth: the water depth.
            )rst")

        .def(py::init<const WaveSpectrum&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      unsigned int, double>(),
                      "spectrum"_a, "w"_a, "b"_a = Eigen::ArrayXd(0),
                      "seed"_a = 0, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a spectrum and fixed frequencies and headings.

            :param spectrum: a :py:class:`~Spectral.WaveSpectrum` with analytical formulation.
            :param w: a n-sized :py:class:`numpy.ndarray` containing
                the frequencies at which :py:class:`~Spectral.WaveSpectrum` is discretized.
            :param b: a n-sized :py:class:`numpy.ndarray` containing
                the headings for all spectrum's discretized frequencies (in rad).
            :param int seed: The seed to use for generating random numbers for phasis, 0 to use a random seed.
            :param float depth: the water depth.
            )rst")
        .def(py::init<const Spectrum&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      unsigned int, double>(),
                      "spectrum"_a, "w"_a, "seed"_a = 0, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a spectrum and fixed frequencies (without spreading).

            :param spectrum: a :py:class:`~Spectral.Spectrum` with analytical formulation.
            :param w: a n-sized :py:class:`numpy.ndarray` containing
                the frequencies at which :py:class:`~Spectral.Spectrum` is discretized.
            :param int seed: The seed to use for generating random numbers for phasis, 0 to use a random seed.
            :param float depth: the water depth.
            )rst")
        .def(py::init<const SeaState&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      const Eigen::Ref<const Eigen::ArrayXd>&,
                      unsigned int, double>(),
                      "seastate"_a, "w"_a, "b"_a = Eigen::ArrayXd(0),
                      "seed"_a = 0, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a sea state and fixed frequencies and headings.

            :param seastate: a :py:class:`~Spectral.SeaState` defining the sea state.
            :param w: a n-sized :py:class:`numpy.ndarray` containing
                the frequencies at which :py:class:`~Spectral.WaveSpectrum` is discretized.
            :param b: a n-sized :py:class:`numpy.ndarray` containing
                the headings for all spectrum's discretized frequencies (in rad).
            :param int seed: The seed to use for generating random numbers for phasis, 0 to use a random seed.
            :param float depth: the water depth.
            )rst")
        // FIXME we had to change the order of the 2 following constructors
        // for pybind11 to see the derived type first...otherwise a segfault
        // occurs...
        .def(py::init<const WaveSpectrum& ,
                double , double , unsigned int , unsigned int , unsigned int, double>(),
                "spectrum"_a, "wmin"_a = 0.1, "wmax"_a = 2.0 , "nbSeed"_a = 200, "spreadNbheading"_a = 72,
                "seed"_a = 0, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a spectrum. With random shift to avoid repetition period.

            :param spectrum: a :py:class:`~Spectral.WaveSpectrum` with analytical formulation.
            :param float wmin: The minimum frequency
            :param float wmax: The minimum frequency
            :param int nbSeed: The number of seeds to use.
            :param int spreadNbheading: The number of headings to use for spreading.
            :param int seed: The seed to use for generating random numbers for phasis, 0 to use a random seed.
            :param float depth: the water depth.
            )rst")
        .def(py::init<const Spectrum&,
                double , double , unsigned int , unsigned int, double>(),
                "spectrum"_a, "wmin"_a = 0.1, "wmax"_a = 2.0 , "nbSeed"_a = 200,
                "seed"_a = 0, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a spectrum. With random shift to avoid repetition period.

            :param spectrum: a :py:class:`~Spectral.Spectrum` with analytical formulation.
            :param float wmin: The minimum frequency
            :param float wmax: The minimum frequency
            :param int nbSeed: The number of seeds to use.
            :param int seed: The seed to use for generating random numbers for phasis, 0 to use a random seed.
            :param float depth: the water depth.
            )rst")
        .def(py::init<const SeaState& ,
                double , double , unsigned int , unsigned int , unsigned int, double>(),
                "seaState"_a, "wmin"_a = 0.1, "wmax"_a = 2.0 , "nbSeed"_a = 200, "spreadNbheading"_a = 72,
                "seed"_a = 0, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a sea state. With random shift to avoid repetition period.

            :param seaState: a :py:class:`~Spectral.SeaState` defining the sea state.
            :param float wmin: The minimum frequency
            :param float wmax: The minimum frequency
            :param int nbSeed: The number of seeds to use.
            :param int spreadNbheading: The number of headings to use for spreading.
            :param int seed: The seed to use for generating random numbers for phasis, 0 to use a random seed.
            :param float depth: the water depth.
            )rst")
        .def(py::init<const std::string&, double>(), "filename"_a, "depth"_a = -1.,
            R"rst(
            Construct a Wif from a file. It does not check the validity of it.

            :param string filename.
            :param float depth: the water depth.
            )rst")
        .def("setSpectrum", py::overload_cast<const WaveSpectrum&>(&Wif::setSpectrum),
            "spectrum"_a,
              R"rst(
            Modify amplitudes (only!) to target a given wave spectrum
            )rst")
        .def("setSpectrum", py::overload_cast<const Spectrum&>(&Wif::setSpectrum),
            "spectrum"_a,
            R"rst(
            Modify amplitudes (only!) to target a given spectrum
            )rst")
        .def(py::self += py::self)

        .def("__imul__", [](Wif &a, const double & b) { a *= b; }, py::is_operator())

        .def(py::self + py::self)

        .def("removeZeroFrequency", &Wif::removeZeroFrequency,
        R"rst(
            Remove zero frequency if any
            )rst")

        .def("removeZero", &Wif::removeZero,
            "threshold"_a = 1.e-12,
            R"rst(
            Remove frequency with zero amplitude
            )rst")

        .def("removeFreq", &Wif::removeFreq,
             "ifreq"_a, "toResize"_a = true,
             R"rst(
             Remove frequency with index ifreq. If toResize is true (default), then all corresponding arrays are resized
             to nbfreq -1)rst")

        .def("offset", &Wif::offset, R"rst(Offset the wif, in place (change phases)

        Args:
            dt (float, optional): Shift in time. Defaults to 0.
            dx (float, optional): Shift in x direction. Defaults to 0.
            dy (float, optional): Shift in y direction. Defaults to 0.

        Returns:
            None.
            )rst", "dt"_a = 0., "dx"_a = 0., "dy"_a = 0.)

        .def("getFrequencies", &Wif::getFrequencies,
            R"rst(
            Get discretized spectrum list of frequencies.

            :return: the n-sized :py:class:`numpy.ndarray` frequencies.
            )rst")
        .def("getAmplitudes", &Wif::getAmplitudes,
            R"rst(
            Get discretized spectrum list of amplitudes.

            :return: the n-sized :py:class:`numpy.ndarray` amplitudes.
            )rst")
        .def("setAmplitudes", &Wif::setAmplitudes,
            R"rst(
            Set discretized spectrum list of amplitudes.

            :param amp: the n-sized :py:class:`numpy.ndarray` amplitudes.
            )rst")
        .def("getPhases", &Wif::getPhases,
            R"rst(
            Get discretized spectrum list of phases.

            :return: the n-sized :py:class:`numpy.ndarray` phases.
            )rst")
        .def("setPhases", &Wif::setPhases,
            R"rst(
            Set discretized spectrum list of phases.

            :param phi: the n-sized :py:class:`numpy.ndarray` phases.
            )rst")
        .def("setDepth", &Wif::setDepth , "depth"_a,
            R"rst(
            Set discretized spectrum list of phases.

            :param float depth: the water depth.
            )rst")
        .def("getDepth", &Wif::getDepth,
            R"rst(
            Get water depth.

            :return float: the water depth.
            )rst")
        .def("setHeadings", py::overload_cast<const Eigen::ArrayXd&>(&Wif::setHeadings),
            R"rst(
            Set discretized spectrum list of headings.

            :param b: the n-sized :py:class:`numpy.ndarray` headings.
            )rst")
        .def("setHeadings", py::overload_cast<double>(&Wif::setHeadings),
            R"rst(
            Set discretized spectrum list of headings.

            :param b: the n-sized :py:class:`numpy.ndarray` headings.
            )rst")
        .def("getComplexData", &Wif::getComplexData,
            R"rst(
            Get discretized spectrum complex data.

            :return: the complex n-sized :py:class:`numpy.ndarray` spectrum data.
            )rst")
        .def("getHeadings", &Wif::getHeadings,
            R"rst(
            Get discretized spectrum list of headings.

            :return: the n-sized :py:class:`numpy.ndarray` headings (in rad).
            )rst")
        .def("getCosHeadings", &Wif::getCosHeadings,
            R"rst(
            Get cosine values of discretized spectrum list of headings.

            :return: the n-sized :py:class:`numpy.ndarray` cosine headings.
            )rst")
        .def("getSinHeadings", &Wif::getSinHeadings,
            R"rst(
            Get sine values of discretized spectrum list of headings.

            :return: the n-sized :py:class:`numpy.ndarray` sine headings.
            )rst")
        .def("getBWidth", &Wif::getBWidth,
            R"rst(
            Get heading width.

            :return: the n-sized :py:class:`numpy.ndarray` containing heading width.
            )rst")
        .def("getWWidth", &Wif::getWWidth,
            R"rst(
            Get frequency width.

            :return float: the n-sized :py:class:`numpy.ndarray` containing frequency width.
            )rst")
        .def("getDensity", &Wif::getDensity,
            R"rst(
            Get discretized spectrum list of densities.

            :return: the n-sized :py:class:`numpy.ndarray` densities.
            )rst")
        .def("getWaveNumbers", &Wif::getWaveNumbers,
            R"rst(
            Get discretized spectrum list of wave numbers.

            :return: the n-sized :py:class:`numpy.ndarray` wave numbers.
            )rst")
        .def("getEncounterFrequencies", &Wif::getEncounterFrequencies, "speed"_a,
            R"rst(
            Get encounter frequencies for discretized spectrum according to forward speed.

            :param float speed: the forward speed.

            :return: the n-sized :py:class:`numpy.ndarray` encounter frequencies.
            )rst")

        .def("getIndependentHeadings", &Wif::getIndependentHeadings , "Return list of unique heading")

        //Expose attribute to python class (read only)
        .def_readonly("freq", &PyWif::w_)
        .def_readonly("amp", &PyWif::amp_)
        .def_readonly("phi", &PyWif::phi_)
        .def_readonly("head", &PyWif::b_)
        .def_readonly("depth", &PyWif::depth_)
        .def_readonly("isWidth", &PyWif::isWidth_)
        .def_readonly("unidirectional", &PyWif::unidirectional_)
        .def(py::pickle(
            [](const Wif& w) // __getstate__
                {
                    return py::make_tuple(w.getFrequencies(), w.getAmplitudes(), w.getPhases(), w.getHeadings(), w.getDepth());
                },
            [](py::tuple t) // __setstate__
                {
                    if (t.size() != 5)
                        throw std::runtime_error("Invalid state!");

                    Wif w(t[0].cast<Eigen::Ref<const Eigen::ArrayXd>>(), // w
                          t[1].cast<Eigen::Ref<const Eigen::ArrayXd>>(), // a
                          t[2].cast<Eigen::Ref<const Eigen::ArrayXd>>(), // phi
                          t[3].cast<Eigen::Ref<const Eigen::ArrayXd>>(), // b
                          t[4].cast<double>());                          // depth

                    return w;
                }))
        .def("isMultiWif", [](const Wif& w){return false ;})
        ;

    py::class_<Wifm, std::shared_ptr<Wifm> >(m, "Wifm")
        .def(py::init<Wifm const &>())
        .def(py::init<const std::vector<std::shared_ptr<Wif> > &, const Eigen::MatrixX2d &>(),
             py::keep_alive<1, 2>())
        .def("getWifIndex", &Wifm::getWifIndex)
        .def("getWif", &Wifm::getWif, py::return_value_policy::reference)
        .def("getNWifs", &Wifm::getNWifs)
        .def("getWifAtIndex", &Wifm::getWifAtIndex, py::return_value_policy::reference)
        .def("getBounds", &Wifm::getBounds)
        .def("setHeadings", &Wifm::setHeadings)
        .def("isMultiWif", [](const Wifm& w){return true ;})
        ;
}

