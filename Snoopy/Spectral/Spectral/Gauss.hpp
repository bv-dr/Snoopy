#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class Gauss
 *
 * @brief Define a Gauss spectrum.
 */
class SPECTRAL_API Gauss : public ParametricSpectrum
{
public:

    Gauss(double hs, double tp, double sigma, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs_(hs), tp_(tp), sigma_(sigma)
    {
        tailOrder_ = +1; // Not constant ==> > 0
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    // Standard deviation.
    double sigma_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    static int getNParams() { return 3; }
    static std::vector<const char*> getCoefs_name() { return{ "hs", "tp",  "sigma" }; }
    std::vector<double> getCoefs() const override   { return{  hs_,  tp_ ,  sigma_ }; }
    static std::vector<double> getCoefs_0()         { return{  1. ,  10. ,  0.5    }; }
    static std::vector<double> getCoefs_min()       { return{  0. ,  0.01,  0.01   }; }
    static std::vector<double> getCoefs_max()       { return{  30.,  60. ,  30.    }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<Gauss>();
    }

};

}
}
