#include "Rao.hpp"
#include "Dispersion.hpp"
#include "Tools/Sort.hpp"
#include "Tools/EigenUtils.hpp"
#include "Math/Tools.hpp"
#include <iostream>
#include <fstream>

using namespace BV::Spectral ;
using namespace BV::Math ;

void Rao::init_()
{
    //Compute encounter wave frequency
    using IndexType = Eigen::ArrayXd::Index ;
    const Eigen::ArrayXd & b = axes_[0] ;
    const Eigen::ArrayXd & w = axes_[1] ;
    k_ = w2k(w, depth_);
    const IndexType nbHeadings(b.size()) ;
    for (IndexType ihead = 0; ihead < nbHeadings; ++ihead)
    {
        we_.row(ihead) = w - k_ * forwardSpeed_ * std::cos(b(ihead)) ;
    }

    //Handle optional inputs
    if (meanValues_.size() == 0)
    {
        meanValues_ = Eigen::ArrayXd::Zero(getNModes());
    }
    else if (meanValues_.size() != getNModes()) 
    {
        throw std::invalid_argument("Wrong size for meanValues vector");
    }
}


//Derivate, in place
Rao& Rao::derivate(int n)
{
    const Eigen::ArrayXd & w = axes_[1] ;
    std::array<Eigen::Index, 3> offset = { 0, 0, 0 } ;
    std::array<Eigen::Index, 3> extent = { 1, 1, axes_[2].size() } ;

    auto we = getEncFrequencies();
    auto zi = std::complex<double>(0, 1);

    for (Eigen::Index ib = 0; ib < getHeadings().size(); ++ib)
    {
        offset[0] = ib;
        for (Eigen::Index iw = 0; iw < w.size(); ++iw)
        {
            offset[1] = iw ;
            complexData_.slice(offset, extent) = complexData_.slice(offset, extent)  * pow( zi*we(ib,iw), n) ;
        }
    }
    this->refresh_();
    return *this ;
}

//First main constructor, from Amplitude and Phase
Rao::Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
         const ModesType& modes,
         const Eigen::Tensor<double, 3>& dataAmp,
         const Eigen::Tensor<double, 3>& dataPhi,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         double forwardSpeed,
         double depth,
         Eigen::ArrayXd meanValues) :
    HydroTransferFunction<Storage>(
                         Storage({b, w, modeCoefficients}, dataAmp, dataPhi),
                         modes, refPoint, waveRefPoint, forwardSpeed, depth
                                     ),
    we_(b.size(), w.size()), meanValues_(meanValues)
{
    init_() ;
}

//First main constructor, from real and imaginary part
Rao::Rao(
    const Eigen::Ref<const Eigen::ArrayXd>& b,
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
    const ModesType& modes,
    const Eigen::Tensor<std::complex<double>, 3>& dataReIm,
    const Eigen::Ref<const Eigen::Vector3d>& refPoint,
    const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
    double forwardSpeed,
    double depth,
    Eigen::ArrayXd meanValues) :
    HydroTransferFunction<Storage>(
                         Storage({b, w, modeCoefficients}, dataReIm),
                         modes, refPoint, waveRefPoint, forwardSpeed, depth
                                     ),
    we_(b.size(), w.size()), meanValues_(meanValues)
{
    init_() ;
}


//With default "ModeType" and "ModeCoefficients"
Rao::Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Tensor<double, 3>& dataAmp,
         const Eigen::Tensor<double, 3>& dataPhi,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         double forwardSpeed,
         double depth,
         Eigen::ArrayXd meanValues) :
    Rao(b, w, Eigen::ArrayXd::Zero(dataAmp.dimension(2)),
        ModesType::Constant(dataAmp.dimension(2), Modes::NONE), dataAmp,
        dataPhi, refPoint, waveRefPoint, forwardSpeed, depth, meanValues)
{
    init_();
}

//With default "ModeCoefficients"
Rao::Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const ModesType& modes,
         const Eigen::Tensor<double, 3>& dataAmp,
         const Eigen::Tensor<double, 3>& dataPhi,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         double forwardSpeed,
         double depth,
         Eigen::ArrayXd meanValues) :
    Rao(b, w, Eigen::ArrayXd::Zero(dataAmp.dimension(2)), modes, dataAmp,
        dataPhi, refPoint, waveRefPoint, forwardSpeed, depth, meanValues)
{
    init_();
}

//With default "ModeType" and "ModeCoefficients", from real and imaginary
Rao::Rao(
    const Eigen::Ref<const Eigen::ArrayXd>& b,
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const Eigen::Tensor<std::complex<double>, 3>& dataReIm,
    const Eigen::Ref<const Eigen::Vector3d>& refPoint,
    const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
    double forwardSpeed,
    double depth,
    Eigen::ArrayXd meanValues) :
    Rao(b, w, Eigen::ArrayXd::Zero(dataReIm.dimension(2)),
        ModesType::Constant(dataReIm.dimension(2), Modes::NONE), dataReIm,
        refPoint, waveRefPoint, forwardSpeed, depth, meanValues)
{
    init_();
}

//With default "ModeCoefficients", from real and imaginary
Rao::Rao(
    const Eigen::Ref<const Eigen::ArrayXd>& b,
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const ModesType& modes,
    const Eigen::Tensor<std::complex<double>, 3>& dataReIm,
    const Eigen::Ref<const Eigen::Vector3d>& refPoint,
    const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
    double forwardSpeed,
    double depth,
    Eigen::ArrayXd meanValues) :
    Rao(b, w, Eigen::ArrayXd::Zero(dataReIm.dimension(2)), modes, dataReIm,
        refPoint, waveRefPoint, forwardSpeed, depth, meanValues)
{
    init_();
}

namespace BV {
namespace Spectral {
namespace Details {

Eigen::Index GetNModes_(const std::vector<Rao> & raos)
{
    // We check the modes number in the individual RAOs
    Eigen::Index nModes(0) ;
    for (const Rao & rao : raos)
    {
        nModes += rao.getNModes() ;
    }
    return nModes ;
}

void Check_(const std::vector<Rao> & raos, bool checkReferencePoint)
{
    Eigen::Index nB(raos[0].getNHeadings()) ;
    Eigen::Index nF(raos[0].getNFrequencies()) ;
    Eigen::ArrayXd bs(raos[0].getHeadings()) ;
    Eigen::ArrayXd fs(raos[0].getFrequencies()) ;
    Eigen::Vector3d refPoint(raos[0].getReferencePoint()) ;
    Eigen::Vector2d waveRefPoint(raos[0].getWaveReferencePoint()) ;
    double speed(raos[0].getForwardSpeed()) ;
    double depth(raos[0].getDepth()) ;
    std::size_t nRaos(raos.size()) ;
    for (std::size_t iRao=1; iRao<nRaos; ++iRao)
    {
        const Rao & rao(raos[iRao]) ;
        if ((rao.getNHeadings() != nB) || (rao.getNFrequencies() != nF))
        {
            throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different axes") ;
        }
        if (!((rao.getHeadings() - bs).isZero()))
        {
            throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different headings") ;
        }
        if (!((rao.getFrequencies() - fs).isZero()))
        {
            throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different frequencies") ;
        }

        if (checkReferencePoint) 
        {
            if (!((rao.getReferencePoint() - refPoint).isZero()))
            {
                throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different reference point") ;
            }
            if (!((rao.getWaveReferencePoint() - waveRefPoint).isZero()))
            {
                throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different wave reference point") ;
            }
        }

        if (!(std::abs(rao.getForwardSpeed() - speed) < 1.e-8))
        {
            throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different forward speed") ;
        }
        if (!(std::abs(rao.getDepth() - depth) < 1.e-8))
        {
            throw BV::Tools::Exceptions::BVException("Initialisation of Rao with a list of Rao with different depth") ;
        }
    }
}

Eigen::Tensor<std::complex<double>, 3> GetCompleteTensor_(const std::vector<Rao> & raos, bool checkReferencePoint)
{
    Check_(raos, checkReferencePoint) ;
    Eigen::Index nB(raos[0].getNHeadings()) ;
    Eigen::Index nF(raos[0].getNFrequencies()) ;
    std::size_t nRaos(raos.size()) ;
    Eigen::Index nModes(GetNModes_(raos)) ;
    Eigen::Tensor<std::complex<double>, 3> reIm(nB, nF, nModes) ;
    Eigen::Index currentModeIndex(0) ;
    for (std::size_t iRao=0; iRao<nRaos; ++iRao)
    {
        const Rao & rao(raos[iRao]) ;
        Eigen::Index nModesRao(rao.getNModes()) ;
        Eigen::Tensor<std::complex<double>, 3> reImRao(rao.getComplexData()) ;
        for (Eigen::Index iMode=0; iMode<nModesRao; ++iMode)
        {
            std::array<Eigen::Index, 3> offsetComplete = {0, 0, currentModeIndex+iMode} ;
            std::array<Eigen::Index, 3> offsetRao = {0, 0, iMode} ;
            std::array<Eigen::Index, 3> extent = {nB, nF, 1} ;
            reIm.slice(offsetComplete, extent) = reImRao.slice(offsetRao, extent) ;
        }
        currentModeIndex += nModesRao ;
    }
    return reIm ;
}

Eigen::Array<Modes, Eigen::Dynamic, 1> GetCompleteModes_(const std::vector<Rao> & raos)
{
    using ModesType = Eigen::Array<Modes, Eigen::Dynamic, 1> ;
    Eigen::Index nModes(GetNModes_(raos)) ;
    ModesType modes(nModes) ;
    Eigen::Index currentModeIndex(0) ;
    for (std::size_t iRao=0; iRao<raos.size(); ++iRao)
    {
        const Rao & rao(raos[iRao]) ;
        Eigen::Index nModesRao(rao.getNModes()) ;
        ModesType modesRao(rao.getModes()) ;
        for (Eigen::Index iMode=0; iMode<nModesRao; ++iMode)
        {
            modes(currentModeIndex+iMode) = modesRao(iMode) ;
        }
        currentModeIndex += nModesRao ;
    }
    return modes ;
}

Eigen::ArrayXd GetCompleteModesCoefs_(const std::vector<Rao> & raos)
{
    Eigen::Index nModes(GetNModes_(raos)) ;
    Eigen::ArrayXd modesCoefs(nModes) ;
    Eigen::Index currentModeIndex(0) ;
    for (std::size_t iRao=0; iRao<raos.size(); ++iRao)
    {
        const Rao & rao(raos[iRao]) ;
        Eigen::Index nModesRao(rao.getNModes()) ;
        Eigen::ArrayXd modesCoefsRao(rao.getModeCoefficients()) ;
        for (Eigen::Index iMode=0; iMode<nModesRao; ++iMode)
        {
            modesCoefs(currentModeIndex+iMode) = modesCoefsRao(iMode) ;
        }
        currentModeIndex += nModesRao ;
    }
    return modesCoefs ;
}

Eigen::ArrayXd GetCompleteMeanValues_(const std::vector<Rao> & raos)
{
    Eigen::Index nModes(GetNModes_(raos)) ;
    Eigen::ArrayXd meanValues(nModes) ;
    Eigen::Index currentModeIndex(0) ;
    for (std::size_t iRao=0; iRao<raos.size(); ++iRao)
    {
        const Rao & rao(raos[iRao]) ;
        Eigen::Index nModesRao(rao.getNModes()) ;
        Eigen::ArrayXd meanValuesRao(rao.getMeanValues()) ;
        for (Eigen::Index iMode=0; iMode<nModesRao; ++iMode)
        {
            meanValues(currentModeIndex+iMode) = meanValuesRao(iMode) ;
        }
        currentModeIndex += nModesRao ;
    }
    return meanValues ;
}

} // End of namespace Details
} // End of namespace Spectral
} // End of namespace BV

// Construction via a list of RAOs. Each RAO should contain a different mode.
Rao::Rao(const std::vector<Rao> & raos, bool checkReferencePoint) :
    Rao(raos[0].getHeadings(), raos[0].getFrequencies(),
        Details::GetCompleteModesCoefs_(raos),
        Details::GetCompleteModes_(raos),
        Details::GetCompleteTensor_(raos, checkReferencePoint),
        raos[0].getReferencePoint(), raos[0].getWaveReferencePoint(),
        raos[0].getForwardSpeed(), raos[0].getDepth(),
        Details::GetCompleteMeanValues_(raos))
{
}


void Rao::setModes(const ModesType& modes)
{
    modes_ = modes;
}


void Rao::setFrequencies(const Eigen::Ref<const Eigen::ArrayXd>& w)
{
    axes_[1] = w;

    // Recompute wave number and encounter frequencies
    init_();  
}

const Eigen::ArrayXXd & Rao::getEncFrequencies() const
{
    return we_ ;
}

const Eigen::ArrayXd & Rao::getWaveNumbers() const
{
    return k_;
}

const Eigen::ArrayXd & BV::Spectral::Rao::getMeanValues() const
{
    return meanValues_;
}

void BV::Spectral::Rao::setMeanValues(const Eigen::ArrayXd & vals)
{
    if (vals.size() != meanValues_.size())
    {
        throw std::invalid_argument("Wrong mean values size") ;
    }
    meanValues_ = vals ;
}

Eigen::ArrayXXcd Rao::getComplexAtHeadings(
    const Eigen::ArrayXd & relativeHeadings,
    const std::vector<unsigned> & indices,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    BV::Math::Interpolators::ExtrapolationType extrapType) const
{
    using IndexType = Eigen::Tensor<double, 3>::Index ;
    IndexType iHeading(0) ;
    std::vector<unsigned>::const_iterator nextIndex = indices.begin() ;
    IndexType nModes(getNModes()) ;
    IndexType nFreqs(getNFrequencies()) ;
    Eigen::ArrayXXcd result(nFreqs, nModes) ;
    Eigen::DSizes<IndexType, 3> offsets { 0, 0, 0 } ;
    Eigen::DSizes<IndexType, 3> extents { getNHeadings(), 1, nModes } ;
    Eigen::Tensor<std::complex<double>, 3> tmp ;
    Eigen::ArrayXd relativeHeading(1) ;
    IndexType iFreq(0) ;
    bool isEnd(nextIndex == indices.end()) ;
    if (relativeHeadings.size() == 1)
    {
        // Easy optimization when there is only one heading
        relativeHeading(0) = relativeHeadings(0) ;
        tmp = this->getComplexData(0, relativeHeading, interpScheme,
                                   interpStrategy, extrapType) ;
        isEnd = true ;
    }
    for (IndexType iComponent = 0; iComponent < nFreqs; ++iComponent)
    {
        if ((*nextIndex) == iComponent)
        {
            isEnd = ((*nextIndex) == indices.back()) ;
            relativeHeading(0) = relativeHeadings(iHeading) ;
            iFreq = 0 ;
            offsets[1] = iComponent ;
            if (!isEnd)
            {
                ++nextIndex ;
                extents[1] = static_cast<IndexType>(*nextIndex) - iComponent ;
            }
            else
            {
                extents[1] = nFreqs - iComponent ;
            }
            tmp = this->getComplexData(0, relativeHeading, offsets, extents,
                    interpScheme, interpStrategy, extrapType) ;
            ++iHeading ;
        }
        for (IndexType iMode = 0; iMode < nModes; ++iMode)
        {
            result(iComponent, iMode) = tmp(0, iFreq, iMode) ;
        }
        ++iFreq ;
    }
    return result ;
}

Rao Rao::getRaoAtFrequencies(
    const Eigen::ArrayXd & ws, const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    BV::Math::Interpolators::ExtrapolationType extrapType) const
{
    Eigen::Tensor<std::complex<double>, 3> dataInterpReIm(
        getComplexData(1, ws, interpScheme, interpStrategy, extrapType)) ;
    return Rao(axes_[0], ws, axes_[2], modes_, dataInterpReIm, refPoint_,
               waveRefPoint_, forwardSpeed_, depth_) ;
}

Rao Rao::getRaoAtHeadings(
    const Eigen::ArrayXd & bs, const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    BV::Math::Interpolators::ExtrapolationType extrapType) const
{
    //Check that Rao is ready for interpolation
    if (isReadyForInterpolation() == false )
    {
        throw std::invalid_argument("Heading should be sorted, and closed on 0-360 to be able to interpolate");
    }

    Eigen::Tensor<std::complex<double>, 3> dataInterpReIm(
        getComplexData(0, bs, interpScheme, interpStrategy, extrapType)) ;
    return Rao(bs, axes_[1], axes_[2], modes_, dataInterpReIm, refPoint_,
               waveRefPoint_, forwardSpeed_, depth_) ;
}

Rao Rao::getRaoAtModeCoefficients(
        const Eigen::ArrayXd & coefs,
        const BV::Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        BV::Math::Interpolators::ExtrapolationType extrapType) const
{
    if (axes_[2].size() == 1)
    {
        std::cout << "WARNING only one mode coefficient in RAO, no interpolation performed in getRaoAtModeCoefficients!" << std::endl ;
        return *this ;
    }
    Modes mode0(modes_(0)) ;
    for (Eigen::Index iMode=0; iMode<modes_.size(); ++iMode)
    {
        if (modes_(iMode) != mode0)
        {
            throw BV::Tools::Exceptions::BVException(
                      "Mode coefficients interpolation on different modes types"
                                                    ) ;
        }
    }
    Eigen::Tensor<std::complex<double>, 3> dataInterpReIm(
        getComplexData(2, coefs, interpScheme, interpStrategy, extrapType)) ;
    return Rao(axes_[0], axes_[1], coefs, modes_.head(coefs.size()),
               dataInterpReIm, refPoint_, waveRefPoint_, forwardSpeed_, depth_) ;
}

Rao Rao::getFreqExtRao(const double& wmin, const double& wmax) const
{
    const Eigen::ArrayXd freqs(axes_[1]);
    const Eigen::Index nbfreq(freqs.size());
    const double w_min(wmin < 0. ? freqs[0] : wmin);
    const double w_max(wmax < 0. ? freqs[nbfreq-1] : wmax);
    const Eigen::Index n0(w_min < freqs[0]-1.e-8 ? 1 : 0);
    const Eigen::Index nN(w_max > freqs[nbfreq-1]+1.e-8 ? 1 : 0);
    if (!(n0 + nN))     // w_min >= freqs[0] and w_max <= freqs[nbfreq -1]
    {
        return *this;  // return copy of this Rao
    }
    // either w_min < freqs[0] and/or w_max > freqs[nbfreq -1]
    const Eigen::Index nbhead(axes_[0].size());
    const Eigen::Index nbmode(axes_[2].size());
    Eigen::Tensor<std::complex<double>, 3> data(nbhead, nbfreq +n0 +nN, nbmode);
    Eigen::ArrayXd ws(nbfreq +n0 +nN);
    // Generating new frequency array
    if (n0)
    {
        ws(0) = w_min;
    }
    for (Eigen::Index iw = 0; iw < nbfreq; iw++)
    {
        ws(n0 +iw) = freqs(iw);
    }
    if (nN)
    {
        ws(nbfreq +n0) = w_max;
    }
    // Filling in data tensor
    for (Eigen::Index ib = 0; ib < nbhead; ib++)
    {
        if (n0)
        {
            for (Eigen::Index im = 0; im < nbmode; im++)
            {
                data(ib, 0, im) = complexData_(ib, 0, im);      // Rao(w_min) = Rao(Rao.w_min)
            }
        }
        for (Eigen::Index iw = 0; iw < nbfreq; iw++)
        {
            for (Eigen::Index im = 0; im < nbmode; im++)
            {
                data(ib, n0 +iw, im) = complexData_(ib, iw, im);
            }
        }
        if (nN)
        {
            for (Eigen::Index im = 0; im < nbmode; im++)
            {
                data(ib, n0 +nbfreq, im) = complexData_(ib, nbfreq -1, im);     // rao(w_max) = Rao(Rao.w_max)
            }
        }
    }

    return Rao(axes_[0], ws, axes_[2], modes_, data, refPoint_, waveRefPoint_, forwardSpeed_, depth_);
}

bool Rao::isReadyForSpectral() const
{
    // Check that that there is no duplicated headings or frequencies
    Eigen::ArrayXd head = getHeadings();
    auto isUniqueHead = BV::Tools::Unique(BV::Math::getWrapAngle0_2PI( head )).size() == getHeadings().size();
    auto isUniqueFreq = BV::Tools::Unique( getFrequencies()).size() == getFrequencies().size();
    return (isUniqueFreq && isUniqueHead && getdb() > 0.0 && getdw() > 0.0 && is360() ) ;
}

bool Rao::isReadyForInterpolation() const
{
    //Heading should be sorted
    auto head = getHeadings();
    for (auto iHead = 0; iHead < head.size()- 1; ++iHead)
    {
        if (head(iHead + 1) <= head(iHead))
            return false;
    }

    //Data beyond 0-360 should be there
    if (  head(0) > 0.0  ||  head( head.size()-1 ) < 2*M_PI )
    { 
        return false;
    }

    return true;
}

bool Rao::is360() const
{
    // Check that there is data on 360 degree
    // Rough, just check that heading are available in the 4 quarters

    double minSpacing = M_PI / 2.;
    Eigen::ArrayXd head = getHeadings();  // TODO sort
    Eigen::ArrayXd tmp;
    tmp = head - M_PI * 1. / 4;
    bool a1 = BV::Math::getWrapAngle0_2PI(tmp).minCoeff() < minSpacing;
    tmp = head - M_PI * 3. / 4;
    bool a2 = BV::Math::getWrapAngle0_2PI(tmp).minCoeff() < minSpacing;
    tmp = head - M_PI * 5. / 4;
    bool a3 = BV::Math::getWrapAngle0_2PI(tmp).minCoeff() < minSpacing;
    tmp = head - M_PI * 7. / 4;
    bool a4 = BV::Math::getWrapAngle0_2PI(tmp).minCoeff() < minSpacing;
    return (a1 && a2 && a3 && a4);

}

double Rao::getdw() const
{
    auto freq = getFrequencies();
    auto nbfreq = freq.size();
    double dw = (freq(freq.size() - 1) - freq(0)) / (nbfreq - 1);
    for (auto iFreq = 0; iFreq < nbfreq-1; ++iFreq)
        {
        if ( abs(( freq(iFreq + 1) - freq(iFreq)) / dw - 1) > 0.03 )
            {
            return -1.0;
            }
        }
    return dw;
}

double Rao::getdb() const
{
    auto head = getHeadings();
    auto nbhead = head.size();
    double db = (head(head.size() - 1) - head(0)) / (nbhead - 1);
    for (auto iHead = 0; iHead < nbhead-1; ++iHead)
    {
        if ( abs( (head(iHead + 1) - head(iHead) ) / db - 1) > 0.03)
        {
            return -1.0;
        }
    }
    return db;
}


Rao Rao::getRaoIn2piRange() const {
    auto b = getHeadingIn2PiRange();
    auto complexData(complexData_);
    auto ind = BV::Tools::sortArray(b);
    BV::Tools::sortTensorByCol(complexData, 0, ind);
    BV::Tools::boundTensor(0, b, complexData, 0.0, 2*M_PI, true);
    return Rao(b, axes_[1], axes_[2], modes_, complexData, refPoint_, waveRefPoint_,
               forwardSpeed_, depth_);
}

//Derivate, return new Rao
Rao Rao::getDerivate(int n) const
{
    Rao rao(*this) ;
    rao.derivate(n) ;
    return rao ;
}

Eigen::Tensor<double, 4> Rao::getRao2LinearCoefficientsForOmega() const
{
    const Eigen::ArrayXd w(getFrequencies());
    const Eigen::Index nbFreq(w.size());
    const Eigen::Index nbHead(getHeadings().size());
    const Eigen::Index nbMode(getModes().size());
    const Eigen::Tensor<double, 3> modules(getModules());
    Eigen::Tensor<double, 4> coeffs(nbHead, nbFreq-1, 2, nbMode);
    for (Eigen::Index head = 0; head < nbHead; ++head)
    {
        for (Eigen::Index mode = 0; mode < nbMode; ++mode)
        {
            auto r2_1 = modules(head, 0, mode);
            r2_1 *= r2_1;
            auto w_1 = w[0];
            for (Eigen::Index freq = 1; freq < nbFreq; ++freq)
            {
                auto r2_2 = modules(head, freq, mode);
                r2_2 *= r2_2;
                auto w_2 = w[freq];
                // coeffs
                assert((w_2 - w_1) > 1.e-8);
                double denom = 1.0 / (w_2 - w_1);
                // a_0
                coeffs(head, freq -1, 1, mode) = (r2_2 - r2_1) * denom;
                // a_1
                coeffs(head, freq -1, 0, mode) = (r2_1 * w_2 - r2_2 * w_1) * denom;
                // next interval
                r2_1 = r2_2;
                w_1 = w_2;
            }
        }
    }
    return coeffs;
}

Eigen::Tensor<double, 4> Rao::getRao2LinearCoefficientsForOmegaAtBeta(const Eigen::Ref<const Eigen::ArrayXd>& b) const
{
    const Eigen::ArrayXd w(getFrequencies());
    const Eigen::Index nbFreq(w.size());
    const Eigen::ArrayXd beta(getHeadingIn2PiRange());
    const Eigen::Index nbHead(beta.size());
    const Eigen::Index nbMode(getModes().size());
    const Eigen::Tensor<double, 3> modules(getModules());
    const Eigen::Index nbHead_(b.size());
    Eigen::Tensor<double, 4> coeffs(nbHead_, nbFreq - 1, 2, nbMode);
    // Assumption: b is sorted in the range of [0;2\pi) and has unique values (with some tolerance, of course)
    Eigen::ArrayXi betaIntervals(-2*Eigen::ArrayXi::Ones(nbHead_));  // interval to which belongs b[i]
    // The very first beta: b[0]
    if (b[0] < beta[0]) // below the lowest => lowest is not 0.0 => b[0] in [0, beta[0])
    {
        betaIntervals[0] = -1;
    }
    else
    {
        for (Eigen::Index j = 1; j < nbHead; ++j)
        {
            if (b[0] < beta[j])
            {
                betaIntervals[0] = j - 1;   // beta[j-1] <= b[0] < beta[j]
                break;
            }
        }
        if (betaIntervals[0] == -2)         // beta[nbHead -1] <= b[0] < 2\pi
        {
            betaIntervals[0] = nbHead - 1;
        }
    }
    for (Eigen::Index i = 1; i < nbHead_; ++i)
    {
        if (betaIntervals[i - 1] < 0)       // b[i-1] < beta[0]
        {
            if (b[i] < beta[0])
            {
                betaIntervals[i] = -1;      // b[i] in [0, beta[0])
            }
            else
            {
                for (Eigen::Index j = 0; j < nbHead; ++j)
                {
                    if (b[i] < beta[j])
                    {
                        betaIntervals[i] = j - 1;   // beta[j-1] <= b[i] < beta[j]
                        break;
                    }
                }
                if (betaIntervals[i] == -2)         // beta[nbHead-1] <= b[i] < 2\pi
                {
                    betaIntervals[i] = nbHead - 1;
                }
            }
        }
        else                                // beta[k] <= b[i-1]
        {
            for (Eigen::Index j = betaIntervals[i - 1]; j < nbHead; ++j)
            {
                if (b[i] < beta[j])                 // beta[j-1] <= b[i] < beta[j]
                {
                    betaIntervals[i] = j - 1;
                    break;
                }
            }
            if (betaIntervals[i] == -2)             // beta[nbHead-1] <= b[i] < 2\pi
            {
                betaIntervals[i] = nbHead - 1;
            }
        }
    }
    Eigen::Tensor<double, 3> modules_(nbHead_, nbFreq, nbMode);
    // The cycle condition is used to get mudules2_ at b points
    for (Eigen::Index i = 0; i < nbHead_; ++i)
    {
        Eigen::Index ind1(0);
        Eigen::Index ind2(0);
        double b1(0.);
        double b2(0.);
        if (betaIntervals[i] < 0)                   // 0 <= b[i] < beta[0]
        {
            ind1 = nbHead - 1;
            ind2 = 0;
            b1 = beta[ind1] - 2 * M_PI;
            b2 = beta[ind2];
        }
        else if (betaIntervals[i] < nbHead - 1)     // beta[j] <= b[i] < beta[j+1]
        {
            ind1 = betaIntervals[i];
            ind2 = betaIntervals[i] + 1;
            b1 = beta[ind1];
            b2 = beta[ind2];
        }
        else                                        // beta[nbHead-1] <= b[i] < 2\pi
        {
            ind1 = nbHead-1;
            ind2 = 0;
            b1 = beta[ind1];
            b2 = beta[ind2] + 2 * M_PI;
        }
        double db(b2 - b1);
        assert(db > 0.0);
        double c1((b2 - b[i])/db);
        double c2((b1 - b[i])/db);
        for (Eigen::Index freq = 0; freq < nbFreq; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                double m1(modules(ind1, freq, mode));
                double m2(modules(ind2, freq, mode));
                modules_(i, freq, mode) = c1 * m1 - c2 * m2;
            }
        }
    }
    // Evaluation of the coefficients
    for (Eigen::Index head = 0; head < nbHead_; ++head)
    {
        for (Eigen::Index mode = 0; mode < nbMode; ++mode)
        {
            auto r2_1 = modules_(head, 0, mode);
            auto w_1 = w[0];
            for (Eigen::Index freq = 1; freq < nbFreq; ++freq)
            {
                auto r2_2 = modules_(head, freq, mode);
                auto w_2 = w[freq];
                // coeffs
                assert((w_2 - w_1) > 1.e-8);
                double denom = 1.0 / (w_2 - w_1);
                // a_0
                coeffs(head, freq -1, 1, mode) = (r2_2 - r2_1) * denom;
                // a_1
                coeffs(head, freq -1, 0, mode) = (r2_1 * w_2 - r2_2 * w_1) * denom;
                // next interval
                r2_1 = r2_2;
                w_1 = w_2;
            }
        }
    }
    Eigen::Tensor<double, 4> coeffs2(nbHead_, nbFreq - 1, 3, nbMode);
    for (Eigen::Index head = 0; head < nbHead_; ++head)
    {
        for (Eigen::Index freq = 0; freq < nbFreq - 1; ++freq)
        {
            for (Eigen::Index mode = 0; mode < nbMode; ++mode)
            {
                // \tilde{a_0} = a_0^2
                coeffs2(head, freq, 0, mode) = coeffs(head, freq, 0, mode) * coeffs(head, freq, 0, mode);
                // \tilde{a_1} = 2 a_0 a_1
                coeffs2(head, freq, 1, mode) = 2. * coeffs(head, freq, 0, mode) * coeffs(head, freq, 1, mode);
                // \tilde{a_2] = a_1^2
                coeffs2(head, freq, 2, mode) = coeffs(head, freq, 1, mode) * coeffs(head, freq, 1, mode);
            }
        }
    }
    return coeffs2;
}

Eigen::Tensor<double, 5> Rao::getRao2LinearCoefficients(int* ptrNZero, int* ptrNN1, const int& headExtrapolation) const
{
    const Eigen::ArrayXd w(getFrequencies());
    const Eigen::Index nbFreq(w.size());
    const Eigen::ArrayXd beta(getHeadingIn2PiRange());
    const Eigen::Index nbHead(beta.size());
    const Eigen::Index nbMode(getModes().size());
    const Eigen::Tensor<double, 3> modules(getModules());
    Eigen::ArrayXXd modules0(nbFreq, nbMode);   // modules at bounds
    bool isSym(false);
    int nZero(0);
    int nN1(0);
    if (1.e-8 < beta(0))    // 0 ... b_0 ... b_1 ... ... b_N-1
    {
        nZero = 1;
    }
    if (beta(nbHead - 1) < M_PI + 1.e-8)        // b_0 ... b_1 ... ... b_N-1 ... pi ... 2pi
    {
        nN1 = 1;
        isSym = headExtrapolation == 2;
    }
    if (!nN1 && (beta(nbHead - 1) < 2 * M_PI -1.e-8))    // b_0 ... b_1 ... ... pi ... ... b_N-1 ... 2pi
    {
        nN1 = 1;
    }
    if (isSym)
    {
        std::cout << "Symmetric spectrum\n";
        return getRao2LinearCoefficientsSymmetric(ptrNZero, ptrNN1);
    }
    // headExtrapolation != 2
    if (nZero && nN1)   // 0 < b_0 < b_N-1 < 2pi
    {
        if (!headExtrapolation)     // headExtrapolation == 0
        {
            modules0 = 0.0;
        }
        else                        // headExtrapolation > 0
        {
            double dbeta1(1./(beta(0) + 2 * M_PI - beta(nbHead - 1)));  // 1/(betaN -betaN-1), where betaN = beta0 +2pi
            for (Eigen::Index freq = 0; freq < nbFreq; freq++)
            {
                for (Eigen::Index mode = 0; mode < nbMode; mode++)
                {
                    modules0(freq, mode) = (modules(0, freq, mode) *(beta(0) +2*M_PI) - modules(0, freq, mode) *beta(nbHead-1)) *dbeta1;
                }
            }
        }
    }
    else if (nZero)     // 0 < b_0 < b_N-1 == 2pi
    {
        for (Eigen::Index freq = 0; freq < nbFreq; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                modules0(freq, mode) = modules(nbHead - 1, freq, mode);
            }
        }
    }
    else                // 0 == b_0 < b_N-1 < 2pi
    {
        for (Eigen::Index freq = 0; freq < nbFreq; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                modules0(freq, mode) = modules(0, freq, mode);
            }
        }
    }
    // Precalculation of dw
    Eigen::ArrayXd dw(nbFreq - 1);
    for (Eigen::Index freq = 0; freq < nbFreq-1; freq++)
    {
        dw(freq) = w(freq + 1) - w(freq);
        assert(std::abs(dw(freq)) > 1.e-8);
        dw(freq) = 1. / dw(freq);
    }

    Eigen::Tensor<double, 5> coeffs(nbHead +nZero +nN1, nbFreq-1, 2, 2, nbMode);
    if (nZero)          // 0 < b_0
    {
        double db(beta(0));     // db = b_0 -0 = b_0 > 1.e-8 because nZero == 1
        db = 1. / db;
        for (Eigen::Index freq = 0; freq < nbFreq - 1; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                coeffs(0, freq, 0, 0, mode) = (modules0(freq, mode) * beta(0) * w(freq + 1)
                                             - modules0(freq + 1, mode) * beta(0) * w(freq)) * dw(freq) * db;
                coeffs(0, freq, 0, 1, mode) = ((modules(0, freq, mode) - modules0(freq, mode)) * w(freq + 1)
                                             - (modules(0, freq + 1, mode) - modules0(freq + 1, mode)) * w(freq)) * dw(freq) * db;
                coeffs(0, freq, 1, 0, mode) = ((modules0(freq + 1, mode) - modules0(freq, mode)) * beta(0)) * dw(freq) * db;
                coeffs(0, freq, 1, 1, mode) = (modules(0, freq + 1, mode) - modules0(freq + 1, mode)
                                             - modules(0, freq, mode) + modules0(freq, mode)) * dw(freq) * db;
            }
        }
    }
    for (Eigen::Index head = 0; head < nbHead - 1; head++)
    {
        double db(beta(head + 1) - beta(head));
        assert(std::abs(db) > 1.e-8);
        db = 1. / db;
        for (Eigen::Index freq = 0; freq < nbFreq - 1; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                coeffs(head +nZero, freq, 0, 0, mode) = (modules(head, freq, mode) * beta(head + 1) * w(freq + 1)
                                                       - modules(head + 1, freq, mode) * beta(head) * w(freq + 1)
                                                       - modules(head, freq + 1, mode) * beta(head + 1) * w(freq)
                                                       + modules(head + 1, freq + 1, mode) * beta(head) * w(freq)) * dw(freq) * db;
                coeffs(head +nZero, freq, 0, 1, mode) = ((modules(head + 1, freq, mode) - modules(head, freq, mode)) * w(freq + 1)
                                                - (modules(head + 1, freq + 1, mode) - modules(head, freq + 1, mode)) * w(freq)) * dw(freq) * db;
                coeffs(head +nZero, freq, 1, 0, mode) = ((modules(head, freq + 1, mode) - modules(head, freq, mode)) * beta(head + 1)
                                                       - (modules(head + 1, freq + 1, mode) - modules(head + 1, freq, mode)) * beta(head)) * dw(freq) * db;
                coeffs(head +nZero, freq, 1, 1, mode) = (modules(head + 1, freq + 1, mode) - modules(head, freq + 1, mode)
                                                       - modules(head + 1, freq, mode) + modules(head, freq, mode)) * dw(freq) * db;
            }
        }
    }
    if (nN1)            // b_N-1 < 2pi
    {
        double db(2*M_PI- beta(nbHead-1));  // db = 2pi -b_N-1 > 1.e-8 because nN1 == 1
        db = 1. / db;
        for (Eigen::Index freq = 0; freq < nbFreq - 1; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                coeffs(nbHead-2 +nZero +nN1, freq, 0, 0, mode) = (modules(nbHead-1, freq, mode) * 2*M_PI* w(freq + 1)
                                                                - modules0(freq, mode) * beta(nbHead -1) * w(freq + 1)
                                                                - modules(nbHead-1, freq + 1, mode) * 2*M_PI* w(freq)
                                                                + modules0(freq + 1, mode) * beta(nbHead-1) * w(freq)) * dw(freq) * db;
                coeffs(nbHead-2 +nZero +nN1, freq, 0, 1, mode) = ((modules0(freq, mode) - modules(nbHead -1, freq, mode)) * w(freq + 1)
                                                                - (modules0(freq + 1, mode) - modules(nbHead -1, freq + 1, mode)) * w(freq)) * dw(freq) * db;
                coeffs(nbHead-2 +nZero +nN1, freq, 1, 0, mode) = ((modules(nbHead-1, freq + 1, mode) - modules(nbHead-1, freq, mode)) * 2*M_PI
                                                                - (modules0(freq + 1, mode) - modules0(freq, mode)) * beta(nbHead-1)) * dw(freq) * db;
                coeffs(nbHead-2 +nZero +nN1, freq, 1, 1, mode) = (modules0(freq + 1, mode) - modules(nbHead-1, freq + 1, mode)
                                                                - modules0(freq, mode) + modules(nbHead-1, freq, mode)) * dw(freq) * db;
            }
        }
    }
    *ptrNZero = nZero;
    *ptrNN1 = nN1;
    int nbHead_ = nbHead-1 + nZero + nN1;
    Eigen::Tensor<double, 5> coeffs2(nbHead_, nbFreq - 1, 3, 3, nbMode);
    for (Eigen::Index head = 0; head < nbHead_; ++head)
    {
        for (Eigen::Index freq = 0; freq < nbFreq - 1; ++freq)
        {
            for (Eigen::Index mode = 0; mode < nbMode; ++mode)
            {
                // \tilde{a_{00}} = a_{00}^2
                coeffs2(head, freq, 0, 0, mode) = coeffs(head, freq, 0, 0, mode) * coeffs(head, freq, 0, 0, mode);
                // \tilde{a_{10}} = 2 a_{10} a_{00}
                coeffs2(head, freq, 1, 0, mode) = 2. * coeffs(head, freq, 1, 0, mode) * coeffs(head, freq, 0, 0, mode);
                // \tilde{a_{01}} = 2 a_{01} a_{00}
                coeffs2(head, freq, 0, 1, mode) = 2. * coeffs(head, freq, 0, 1, mode) * coeffs(head, freq, 0, 0, mode);
                // \tilde{a_{11}} = 2 a_{11} a_{00} +2 a_{10} a_{01}
                coeffs2(head, freq, 1, 1, mode) = 2. * coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 0, 0, mode)
                                                + 2. * coeffs(head, freq, 1, 0, mode) * coeffs(head, freq, 0, 1, mode);
                // \tilde{a_{20}} = a_{10}^2
                coeffs2(head, freq, 2, 0, mode) = coeffs(head, freq, 1, 0, mode) * coeffs(head, freq, 1, 0, mode);
                // \tilde{a_{02}} = a_{01}^2
                coeffs2(head, freq, 0, 2, mode) = coeffs(head, freq, 0, 1, mode) * coeffs(head, freq, 0, 1, mode);
                // \tilde{a_{21}} = 2 a_{11} a_{10}
                coeffs2(head, freq, 2, 1, mode) = 2. * coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 1, 0, mode);
                // \tilde{a_{12}} = 2 a_{11} a_{01}
                coeffs2(head, freq, 1, 2, mode) = 2. * coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 0, 1, mode);
                // \tilde{a_{22}} = a_{11}^2
                coeffs2(head, freq, 2, 2, mode) = coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 1, 1, mode);
            }
        }
    }
    return coeffs2;
}

Eigen::Tensor<double, 5> Rao::getRao2LinearCoefficientsSymmetric(int* ptrNZero, int* ptrNN1) const
{
    const Eigen::ArrayXd w(getFrequencies());
    const Eigen::Index nbFreq(w.size());
    const Eigen::ArrayXd beta(getHeadingIn2PiRange());      // 0 <= beta[0] < beta[1] < ... < beta[beta.size() -1] <= PI   !!!
    const Eigen::Index nbHead(beta.size());
    const Eigen::Index nbMode(getModes().size());
    const Eigen::Tensor<double, 3> modules(getModules());
    int nZero(0);
    int nN1(0);
    if (1.e-8 < beta(0))    // 0 ... b_0 ... b_1 ... ... b_N-1
    {
        nZero = 1;
    }
    if (beta(nbHead - 1) < M_PI - 1.e-8)        // b_0 ... b_1 ... ... b_N-1 ... pi
    {
        nN1 = 1;
    }
    // Precalculation of dw
    Eigen::ArrayXd dw(nbFreq - 1);
    for (Eigen::Index freq = 0; freq < nbFreq-1; freq++)
    {
        dw(freq) = w(freq + 1) - w(freq);
        assert(std::abs(dw(freq)) > 1.e-8);
        dw(freq) = 1. / dw(freq);
    }

    Eigen::Tensor<double, 5> coeffs(nbHead +nZero +nN1, nbFreq-1, 2, 2, nbMode);
    if (nZero)          // 0 < b_0
    {
        for (Eigen::Index freq = 0; freq < nbFreq - 1; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                coeffs(0, freq, 0, 0, mode) = (modules(0, freq, mode) * w(freq + 1) - modules(0, freq + 1, mode) * w(freq)) *dw(freq);
                coeffs(0, freq, 0, 1, mode) = 0.0;
                coeffs(0, freq, 1, 0, mode) = (modules(0, freq + 1, mode) - modules(0, freq, mode)) * dw(freq);
                coeffs(0, freq, 1, 1, mode) = 0.0;
            }
        }
    }
    for (Eigen::Index head = 0; head < nbHead - 1; head++)
    {
        double db(beta(head + 1) - beta(head));
        assert(std::abs(db) > 1.e-8);
        db = 1. / db;
        for (Eigen::Index freq = 0; freq < nbFreq - 1; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                coeffs(head +nZero, freq, 0, 0, mode) = (modules(head, freq, mode) * beta(head + 1) * w(freq + 1)
                                                       - modules(head + 1, freq, mode) * beta(head) * w(freq + 1)
                                                       - modules(head, freq + 1, mode) * beta(head + 1) * w(freq)
                                                       + modules(head + 1, freq + 1, mode) * beta(head) * w(freq)) * dw(freq) * db;
                coeffs(head +nZero, freq, 0, 1, mode) = ((modules(head + 1, freq, mode) - modules(head, freq, mode)) * w(freq + 1)
                                                - (modules(head + 1, freq + 1, mode) - modules(head, freq + 1, mode)) * w(freq)) * dw(freq) * db;
                coeffs(head +nZero, freq, 1, 0, mode) = ((modules(head, freq + 1, mode) - modules(head, freq, mode)) * beta(head + 1)
                                                       - (modules(head + 1, freq + 1, mode) - modules(head + 1, freq, mode)) * beta(head)) * dw(freq) * db;
                coeffs(head +nZero, freq, 1, 1, mode) = (modules(head + 1, freq + 1, mode) - modules(head, freq + 1, mode)
                                                       - modules(head + 1, freq, mode) + modules(head, freq, mode)) * dw(freq) * db;
            }
        }
    }
    if (nN1)            // b_N-1 < 2pi
    {
        for (Eigen::Index freq = 0; freq < nbFreq - 1; freq++)
        {
            for (Eigen::Index mode = 0; mode < nbMode; mode++)
            {
                coeffs(nbHead-2 +nZero +nN1, freq, 0, 0, mode) = (modules(nbHead-1, freq, mode) *w(freq +1) -modules(nbHead - 1, freq + 1, mode) *w(freq)) *dw(freq);
                coeffs(nbHead-2 +nZero +nN1, freq, 0, 1, mode) = 0.0;
                coeffs(nbHead-2 +nZero +nN1, freq, 1, 0, mode) = (modules(nbHead-1, freq + 1, mode) -modules(nbHead-1, freq, mode)) * dw(freq);
                coeffs(nbHead-2 +nZero +nN1, freq, 1, 1, mode) = 0.0;
            }
        }
    }
    *ptrNZero = nZero;
    *ptrNN1 = nN1;
    int nbHead_ = nbHead-1 + nZero + nN1;
    Eigen::Tensor<double, 5> coeffs2(nbHead_, nbFreq - 1, 3, 3, nbMode);
    for (Eigen::Index head = 0; head < nbHead_; ++head)
    {
        for (Eigen::Index freq = 0; freq < nbFreq - 1; ++freq)
        {
            for (Eigen::Index mode = 0; mode < nbMode; ++mode)
            {
                // \tilde{a_{00}} = a_{00}^2
                coeffs2(head, freq, 0, 0, mode) = coeffs(head, freq, 0, 0, mode) * coeffs(head, freq, 0, 0, mode);
                // \tilde{a_{10}} = 2 a_{10} a_{00}
                coeffs2(head, freq, 1, 0, mode) = 2. * coeffs(head, freq, 1, 0, mode) * coeffs(head, freq, 0, 0, mode);
                // \tilde{a_{01}} = 2 a_{01} a_{00}
                coeffs2(head, freq, 0, 1, mode) = 2. * coeffs(head, freq, 0, 1, mode) * coeffs(head, freq, 0, 0, mode);
                // \tilde{a_{11}} = 2 a_{11} a_{00} +2 a_{10} a_{01}
                coeffs2(head, freq, 1, 1, mode) = 2. * coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 0, 0, mode)
                                                + 2. * coeffs(head, freq, 1, 0, mode) * coeffs(head, freq, 0, 1, mode);
                // \tilde{a_{20}} = a_{10}^2
                coeffs2(head, freq, 2, 0, mode) = coeffs(head, freq, 1, 0, mode) * coeffs(head, freq, 1, 0, mode);
                // \tilde{a_{02}} = a_{01}^2
                coeffs2(head, freq, 0, 2, mode) = coeffs(head, freq, 0, 1, mode) * coeffs(head, freq, 0, 1, mode);
                // \tilde{a_{21}} = 2 a_{11} a_{10}
                coeffs2(head, freq, 2, 1, mode) = 2. * coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 1, 0, mode);
                // \tilde{a_{12}} = 2 a_{11} a_{01}
                coeffs2(head, freq, 1, 2, mode) = 2. * coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 0, 1, mode);
                // \tilde{a_{22}} = a_{11}^2
                coeffs2(head, freq, 2, 2, mode) = coeffs(head, freq, 1, 1, mode) * coeffs(head, freq, 1, 1, mode);
            }
        }
    }
    return coeffs2;
}

void Rao::Rao2ToFile(const std::string& filename, const int& mode) const
{
    std::ofstream out(filename);
    const auto b(getHeadingIn2PiRange());
    const auto w(getFrequencies());
    const Eigen::Index nbHead(b.size());
    const Eigen::Index nbFreq(w.size());
    const auto modules(getModules());
    for (Eigen::Index head = 0; head < nbHead; head++)
    {
        for (Eigen::Index freq = 0; freq < nbFreq; freq++)
        {
//            out << beta(head) << "\t" << w(freq) << "\t" << modules(head, freq, 0) << "\n";
            out << b(head) << "\t" << w(freq) << "\t" << std::pow(modules(head, freq, mode), 2) << "\n";
        }
        out << "\n";
    }
    out.close();

}
void Rao::Rao2ToFile(const std::string& filename,
                     const Eigen::Tensor<double, 5>& coeffs2,
                     const int& nZero,
                     const int& nN1,
                     const int& nB,
                     const int& nF,
                     const int& mode,
                     const int& bDegree,
                     const int& wDegree,
                     const bool& isSym) const
{
    const auto b(getHeadingIn2PiRange());
    const auto w(getFrequencies());
    std::ofstream out(filename);
    Eigen::ArrayXd b_ext(b.size() +nZero +nN1);
    bool isSymmetry(b[b.size()-1] <= M_PI +1.e-8 && isSym);     // Symmetry condition is used if b[N-1] <= pi && isSym == true
    if (nZero)
    {
        b_ext(0) = 0.0;
        std::cout << "0 < b[0] = " << b[0] << "\n";
    }
    for (Eigen::Index ib = 0; ib < coeffs2.dimension(0) +1 - nZero - nN1; ib++)
    {
        b_ext(nZero +ib) = b[ib];
    }
    if (nN1)
    {
        if (isSymmetry)
        {
            b_ext(nZero +coeffs2.dimension(0) ) = M_PI;
        }
        else
        {
            b_ext(nZero +coeffs2.dimension(0) ) = 2*M_PI;
        }
        std::cout << "b[N-1] = " << b[coeffs2.dimension(0) -nZero -nN1] << " < 2pi\n";
    }
    std::cout << "original headings list b has " << b.size() << " headings:\n" << b << "\n";
    std::cout << "extended headings list b_ext has " << b_ext.size() << " headings:\n" << b_ext << "\n";
    if (isSymmetry) std::cout << "Rao is symmetric\n";
    for(Eigen::Index ib = 0; ib < coeffs2.dimension(0); ib ++)
    {
        for (Eigen::Index ibb = 0; ibb < nB +(ib == coeffs2.dimension(0) -1 ? 1 : 0); ibb++)
        {
            double bb(b_ext[ib]+ ibb*(b_ext[ib+1]-b_ext[ib])/nB);
            for(Eigen::Index iw = 0; iw < coeffs2.dimension(1); iw++)
            {
                for (Eigen::Index iww = 0; iww < nF +(iw == coeffs2.dimension(1) -1 ? 1 : 0); iww++)
                {
                    double ww(w[iw] +iww*(w[iw+1]-w[iw])/nF);
                    double rao2(0.0);
                    for (Eigen::Index pw = 0; pw < wDegree + wDegree + 1; pw++)
                    {
                        for (Eigen::Index pb = 0; pb < bDegree + bDegree + 1; pb++)
                        {
                            rao2 += coeffs2(ib, iw, pw, pb, 0) *std::pow(ww, pw) *std::pow(bb, pb);
                        }
                    }
                    out << bb << "\t" << ww << "\t" << rao2 << "\n";
                }
            }
            out << "\n";
        }
    }
    if (!isSymmetry)
    {
        out.close();
        return;
    }
    // Symmetric part
    std::cout << "Headings in reverse order:\n";
    for(Eigen::Index ib = coeffs2.dimension(0); ib > -1; ib --)
        std::cout << b_ext[ib] << "\n";
    for(Eigen::Index ib = coeffs2.dimension(0) -1; ib > -1; ib --)
    {
        for (Eigen::Index ibb = 0; ibb < nB +(ib == 0 ? 1 : 0); ibb++)
        {
            double bb(b_ext[ib+1]- ibb*(b_ext[ib+1]-b_ext[ib])/nB);
            std::cout << "bb = " << bb << " 2pi -bb = " << 2*M_PI -bb << "\n";
            for(Eigen::Index iw = 0; iw < coeffs2.dimension(1); iw++)
            {
                for (Eigen::Index iww = 0; iww < nF +(iw == coeffs2.dimension(1) -1 ? 1 : 0); iww++)
                {
                    double ww(w[iw] +iww*(w[iw+1]-w[iw])/nF);
                    double rao2(0.0);
                    for (Eigen::Index pw = 0; pw < wDegree + wDegree + 1; pw++)
                    {
                        for (Eigen::Index pb = 0; pb < bDegree + bDegree + 1; pb++)
                        {
                            rao2 += coeffs2(ib, iw, pw, pb, 0) *std::pow(ww, pw) *std::pow(bb, pb);
                        }
                    }
                    out << 2*M_PI -bb << "\t" << ww << "\t" << rao2 << "\n";
                }
            }
            out << "\n";
        }
    }
    out.close();
}
