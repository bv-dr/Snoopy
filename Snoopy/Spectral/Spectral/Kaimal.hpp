#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class Kaimal
 *
 * @brief Define a Kaimal wind spectrum.
 */
class SPECTRAL_API Kaimal: public Spectrum
{
public:

    Kaimal(double meanVelocity, double C10, double z, double heading=0.) :
        Spectrum("Kaimal", heading, meanVelocity), C10(C10), z(z)
    {
    }

    // Surface drag coefficient
    double C10 ;

    // Elevation
    double z ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 3 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "height"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C10, z} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.0044, 10.} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1e3} ;}
} ;

}
}
