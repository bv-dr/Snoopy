#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"


/*
        https://doi.org/10.1017/S0022112081000360
*/

namespace BV
{
namespace Spectral
{

class SPECTRAL_API Wallop : public ParametricSpectrum
{
public:

    Wallop(double hs, double tp, double m, double q, double heading = 0.,
           SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs_(hs), tp_(tp), m_(m), q_(q)
    {
        tailOrder_ = -m;
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    // First shape parameter.
    double m_;

    // Second shape parameter.
    double q_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    static int getNParams() { return 4; };
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp", "m" , "q"  }; }
    std::vector<double> getCoefs() const override   { return {  hs_,  tp_,  m_ ,  q_  }; }
    static std::vector<double> getCoefs_0()         { return {  1. ,  10.,  5. ,  4.  }; }
    static std::vector<double> getCoefs_min()       { return {  0. ,  1. ,  1. ,  1.  }; }
    static std::vector<double> getCoefs_max()       { return {  30.,  20.,  20.,  20. }; }

    double getMoment(int i) const override;

    inline std::string print() const override
    {
        return ParametricSpectrum::print<Wallop>();
    }

};

}
}
