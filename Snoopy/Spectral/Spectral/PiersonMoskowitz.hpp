#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"


namespace BV
{
namespace Spectral
{

/**
 * @class PiersonMoskowitz
 *
 * @brief Define a PiersonMoskowitz spectrum.
 *
 *
 *         Same as Jonswap with gamma = 1
 */
class SPECTRAL_API PiersonMoskowitz : public ParametricSpectrum
{
public:

    PiersonMoskowitz(double hs, double tp, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs_(hs), tp_(tp)
    {
        tailOrder_ = -5;
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    // Use pre-fitted regression to convert between Tp and Tz
    static double tp2tz(double tp)
    {
        return tp / pow((5./4)*M_PI, 0.25);
    }

    static double tz2tp(double tz)
    {
        return tz * pow((5./4)*M_PI, 0.25);
    }

    double getTz() const override
    {
        return tp2tz(tp_);
    }

    static int getNParams() { return 2; }
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp"   }; }
    std::vector<double> getCoefs() const override   { return {  hs_,  tp_   }; }
    static std::vector<double> getCoefs_0()         { return {  1. ,  10.   }; }
    static std::vector<double> getCoefs_min()       { return {  0. ,  0.001 }; }
    static std::vector<double> getCoefs_max()       { return {  30.,  60.   }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<PiersonMoskowitz>();
    }

};

}
}
