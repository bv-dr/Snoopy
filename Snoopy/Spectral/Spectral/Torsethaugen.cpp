#include "Torsethaugen.hpp"
#include "Gamma.hpp"

using namespace BV::Spectral;

const char* Torsethaugen::name = "Torsethaugen";

Eigen::ArrayXd Torsethaugen::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    double tf = 6.6 * cbrt(hs_);
    double n = 0.5 * sqrt(hs_) + 3.2;

    double hs1, gamma1, hs2, tp2, m2;
    double tp1 = tp_;
    double m1 = 4.;
    double gamma2 = 1.;

    if (tp_ <= tf)
    {
        // Wind dominated
        double rpw = 0.7 + 0.3 * exp(-pow(2 * (tf - tp_) / (tf - 2 * sqrt(hs_)), 2));
        hs1 = rpw * hs_;
        gamma1 = 35. * (1. + 3.5 * exp(-hs_)) * pow(2. * M_PI * hs1 / (9.81 * tp_ * tp_), 0.857);

        hs2 = sqrt(1 - rpw * rpw) * hs_;
        tp2 = tf + 2.0;
        m2 = 4.;
    }
    else
    {
        double rps = 0.6 + 0.4 * exp(-pow((tp_ - tf) / (0.3 * (25. - tf)), 2));
        hs1 = rps * hs_;
        gamma1 = 35. * (1. + 3.5 * exp(-hs_)) * pow(2. * M_PI * hs_ / (9.81 * tf * tf), 0.857) * (1. + 6. * (tp_ - tf) / (25. - tf));

        hs2 = sqrt(1 - rps * rps) * hs_;
        m2 = 4. * (1. - 0.7 * exp(-hs_ / 3));
        double s4 = std::max(0.01, 0.08 * (1. - exp(-hs_ / 3.)));
        double g0 = pow(1. / m2 * pow(n / m2, (n - 1.) / m2) * exp(lgamma((n - 1.) / m2)), -1.);
        tp2 = std::max(2.5, pow(16 * s4 * pow(0.4, n) / (g0 * hs2 * hs2), -1 / (n - 1)));
    }
    Eigen::VectorXd sw1 = Gamma(hs1, tp1, gamma1, m1, n, heading).compute(w);
    Eigen::VectorXd sw2 = Gamma(hs2, tp2, gamma2, m2, n, heading).compute(w);
    return sw1 + sw2;
}
