#include "PiersonMoskowitz.hpp"

using namespace BV::Spectral;

const char* PiersonMoskowitz::name = "Pierson-Moskowitz";

Eigen::ArrayXd PiersonMoskowitz::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if (tp_ <= 0.  || hs_ <= 0.)
        return Eigen::ArrayXd::Zero(w.size());

    Eigen::ArrayXd sw(w.size());

    double tp4 = pow(1. / tp_, 4.);
    double hs2 = hs_ * hs_;
    double wp = (M_PI + M_PI) / tp_;

    // Normalizing factor (ISO - 19901)
    double a = 487.045 ; // 5. * pow(M_PI, 4)

    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i];
        if (wi == 0)
            continue;

        double wpsw4 = wp / wi;
        wpsw4 = wpsw4 * wpsw4;
        wpsw4 = -1.25 * wpsw4 * wpsw4;
        double wswp2 = (wi - wp) / wp;
        wswp2 = wswp2 * wswp2;

        double w5 = pow(wi, 5.);
        sw[i] = a * hs2 * tp4 * exp(wpsw4) / w5;
    }

    return sw;
}
