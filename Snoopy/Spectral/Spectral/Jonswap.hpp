#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"

#include <set>

namespace BV
{
namespace Spectral
{

/**
 * @class Jonswap
 *
 * @brief Define a JONSWAP spectrum.
 */
class SPECTRAL_API Jonswap : public ParametricSpectrum
{
public:

    Jonswap(double hs, double tp, double gamma, double sigmaA, double sigmaB, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs_(hs), tp_(tp), gamma_(gamma), sigmaA_(sigmaA), sigmaB_(sigmaB)
    {
        tailOrder_ = -5;
    }

    Jonswap(double hs, double tp, double gamma, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : Jonswap(hs, tp, gamma, 0.07, 0.09, heading, spreadingType, spreadingValue)
    {
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    // Peak parameter.
    double gamma_;

    // First shape parameter.
    double sigmaA_;

    // Second shape parameter.
    double sigmaB_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    //Use pre-fitted regression to convert betwee Tp and Tz
    static double tzOverTp(double gamma)
    {
        return (0.6063 + 0.1164 * pow(gamma, 0.5) - 0.01224 * gamma);
    };

    static double tp2tz(double tp, double gamma)
    {
        return tp * tzOverTp(gamma);
    };

    static double tz2tp(double tz, double gamma)
    {
        return tz / tzOverTp(gamma);
    };

    // Exact inversion of tzOverTp regression
    static double tzOverTp2gamma(double r)
    {
        return -81.6993464052288*r - 0.186466743560169*std::sqrt(-212500.0*r + 187645.0) + 94.7524990388312;
    }

    static double tmOverTp(double gamma)
    {
        return (0.6687 + 0.1182 * pow(gamma, 0.5) - 0.01489 * gamma);
    };


    static double tm2tp(double tm, double gamma)
    {
        return tm / tmOverTp(gamma);
    };

    static double tp2tm(double tp, double gamma)
    {
        return tp * tmOverTp(gamma);
    };

    // Exact inversion of tmOverTp regression
    static double tmOverTp2gamma(double r)
    {
        return -67.1591672263264*r - 0.0053312381236748*std::sqrt(-148900000.0*r + 134497530.0) + 76.4169524351625;
    }

    static double tzOverTm(double gamma)
    {
        return tzOverTp(gamma) / tmOverTp(gamma);
    }

    static double tzOverTm2gamma(double r)
    {
        return (169425630.0*r*r + 11820.0*r*std::sqrt(134497530.0*r*r - 240919350.0*r + 108083520.0) - 309711750.0*r - 11640.0*std::sqrt(134497530.0*r*r - 240919350.0*r + 108083520.0) + 141955920.0)/(2217121.0*r*r - 3645072.0*r + 1498176.0);
    }


    static double t0m1OverTp(double gamma)
    {
        return 0.849 + 0.0099 / gamma + 0.0477 * log(gamma) - 0.00170 * gamma;
        // return (0.7844 + 0.08316 * pow(gamma, 0.5) - 0.01033 * gamma);
    };

    static double t0m12tp(double t0m1, double gamma)
    {
        return t0m1 / t0m1OverTp(gamma);
    };

    static double tp2t0m1(double tp, double gamma)
    {
        return tp * t0m1OverTp(gamma);
    };

    static double tz2t0m1(double tz, double gamma)
    {
        return tz * t0m1OverTp(gamma) / tzOverTp(gamma);
    };

    static double t0m12tz(double t0m1, double gamma)
    {
        return t0m1 * tzOverTp(gamma) / t0m1OverTp(gamma);
    };

    double getTz() const override
    {
        return tp2tz( tp_ , gamma_);
    }

    double getTm() const override
    {
        return tp2tm( tp_ , gamma_);
    }

    double getMoment(int i) const override
    {
        double m0 = pow(getHs(),2) / 16.;
        if (i==0) { return m0;    }
        if (i==1) {return 2*M_PI * m0 / getTm() ;}
        if (i==2) {return pow(2*M_PI/getTz(), 2) * m0 ;}
        return 0;
    }

    static int getNParams() { return 3; }
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp"  , "gamma" }; }
    std::vector<double> getCoefs() const override   { return {  hs_,  tp_  ,  gamma_ }; }
    static std::vector<double> getCoefs_0()         { return {  1. ,  10.  ,  1.     }; }
    static std::vector<double> getCoefs_min()       { return {  0. ,  0.001,  1.     }; }
    static std::vector<double> getCoefs_max()       { return {  30.,  40.  ,  20.    }; }

    std::tuple<double, double> get_wrange(double energyRatio = 0.99) const override;

    inline std::string print() const override
    {
        return ParametricSpectrum::print<Jonswap>();
    }

};

}
}
