#include "Dispersion.hpp"

double BV::Spectral::w2k(double w, double depth)
{
    const double g = 9.81 ;
    const double threshold = 1.e-12 ;

    double k = 0. ;

    if (w < 1e-40) 
    {
        return 0.0;
    }
    else if (depth < threshold || depth > 1. / threshold)
    {
        k = w * w / g ;
    }
    else
    {
        double x = w * w * depth / g ;
        double xk0 ;
        double x2 = 0. ;
        if (x < 2.)
        {
            xk0 = ((0.0305 * x + 0.1701) * x + 0.9994) * std::pow(x, 0.5) ;
        }
        else
        {
            x2 = exp(-x - x) * x ;
            xk0 = (-6. * x2 + 2.) * x2 + x ;
        }
        if (x < 2. || x2 > threshold)
        {
            double ti = 1. ;
            while (abs(ti) > threshold)
            {
                double dxkn0 = log((x + xk0) / (xk0 - x)) * 0.5 - xk0 ;
                double dxkn1 = x / (x * x - xk0 * xk0) - 1. ;
                ti = dxkn0 / dxkn1 ;
                xk0 = xk0 - ti ;
            }
        }
        k = xk0 / depth ;
    }

    return k ;
}

Eigen::ArrayXd BV::Spectral::w2k(const Eigen::ArrayXd& w, double depth)
{
    Eigen::ArrayXd k(w.size()) ;

    for (Eigen::Index iw = 0; iw < w.size(); ++iw)
    {
        k(iw) = w2k(w(iw), depth) ;
    }
    return k ;
}

double BV::Spectral::k2w(double k, double depth)
{
    const double g = 9.81 ; // FIXME opera has a Gravity model...
    const double threshold = 1.e-12 ;
    if (depth < threshold)
    {
        return sqrt(g * k) ;
    }
    else
    {
        return sqrt(g * k * tanh(k * depth)) ;
    }
}

Eigen::ArrayXd BV::Spectral::k2w(const Eigen::ArrayXd& k, double depth)
{
    const double g = 9.81 ;
    const double threshold = 1.e-12 ;
    if (depth < threshold)
    {
        return Eigen::sqrt(g * k) ;
    }
    else
    {
        return Eigen::sqrt(g * k * Eigen::tanh(k * depth)) ;
    }
}

double BV::Spectral::w2we(double w, double heading, double speed, double depth)
{
    double k = w2k(w, depth) ;
    return w - k * speed * std::cos(heading) ;
}

Eigen::ArrayXd BV::Spectral::w2we(const Eigen::ArrayXd& w,
                                  const Eigen::ArrayXd& heading, double speed,
                                  double depth)
{
    auto k = w2k(w, depth) ;
    return w - k * speed * Eigen::cos(heading) ;
}


Eigen::ArrayXd BV::Spectral::w2we(double w,
    const Eigen::ArrayXd& heading, const Eigen::ArrayXd& speed,
    double depth)
{
    float k = w2k( w , depth);
    return w - k * speed * Eigen::cos(heading);
}


Eigen::ArrayXd BV::Spectral::w2we(const Eigen::ArrayXd& w, double heading,
                                  double speed, double depth)
{
    auto k = w2k(w, depth) ;
    return w - k * speed * std::cos(heading) ;
}

Eigen::ArrayXd BV::Spectral::w2we(const Eigen::ArrayXd& w,
                                  const Eigen::ArrayXd& heading, double speed,
                                  const Eigen::ArrayXd& k, double depth)
{
    return w - k * speed * Eigen::cos(heading) ;
}

double BV::Spectral::k2Cp(double k, double depth, double gravity)
{
    if (depth < 1e-4)
    {
        return sqrt((gravity / k)) ;
    }
    else
    {
        return sqrt((gravity / k) * tanh(k * depth)) ;
    }
}

double BV::Spectral::w2Cg(double w, double depth, double gravity)
{
    if (depth < 1e-4)
    {
        return 0.5 * gravity / w ;
    }
    else
    {
        double k(BV::Spectral::w2k(w, depth)) ;
        double Cp(BV::Spectral::k2Cp(k, depth, gravity)) ;
        return (0.5 + (k * depth / (2 * sinh(k * depth)))) * Cp ;
    }
}

double BV::Spectral::k2Cg(double k, double depth, double gravity)
{
    if (depth < 1e-4)
    {
        return 0.5 * gravity / BV::Spectral::k2w(k, depth) ; // FIXME add gravity once available
    }
    else
    {
        double Cp(BV::Spectral::k2Cp(k, depth, gravity)) ;
        return (0.5 + (k * depth / (2 * sinh(k * depth)))) * Cp ;
    }
}

