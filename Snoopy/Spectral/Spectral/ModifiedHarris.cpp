#include "ModifiedHarris.hpp"

using namespace BV::Spectral;

Eigen::ArrayXd ModifiedHarris::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{

    Eigen::ArrayXd sw(Eigen::ArrayXd::Zero(w.size())) ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi(w[i]/doublePi) ;
        double x(L*fi/meanValue) ;
        double x2(x*x) ;
        double Ax(0.51*std::pow(2.+x2,5./6.)/std::pow(std::pow(x,0.15)+9./8.*x,5./3.)) ;
        double Fg(x*Ax/std::pow(2.+x2,5./6.)) ;
        sw[i] = 4.*Fg*ustar2/fi ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
