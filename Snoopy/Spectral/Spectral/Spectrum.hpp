#pragma once

#include "SpectralExport.hpp"
#include "Math/Tools.hpp"
#include <Eigen/Dense>

namespace BV {
namespace Spectral {

/**
 * @class Spectrum
 *
 * @brief Define a spectrum
 */
class SPECTRAL_API Spectrum
{
public:

    Spectrum(std::string name, double heading, double meanValue=0.) :
        heading( BV::Math::WrapAngle0_2PI(heading) ), meanValue(meanValue), name_(std::move(name)), tailOrder_(+1)
    {
        if (heading > 10) { throw std::logic_error("Heading should by in radians!"); }
    }

    virtual ~Spectrum()
    {
    }

    /**
     * Compute the spectral density.
     *
     * @param w The wave frequencies.
     * @return sw The wave density spectrum S(w).
     */
    virtual Eigen::ArrayXd compute(
        const Eigen::Ref<const Eigen::ArrayXd>& w) const = 0 ;

    /**
     *Compute the energy range containing more than x % of the total energy
     *
     */
    std::tuple<double, double> energyRange(double energyRatio,
                                           double wmin = 0.1, double wmax = 2.0,
                                           double dw = 0.001) const ;


    /**
     * Compute the spectral density. (same as above, for only one double)
     *
     * @param w The wave frequencies.
     * @return sw The wave density spectrum S(w).
     */
    double compute(double w) const
    {
        Eigen::ArrayXd x = Eigen::ArrayXd::Constant(1, w) ;
        Eigen::ArrayXd sw = compute(x) ;
        return sw(0) ;
    }

    /**
     * Return the unit spectrum moments up to maxMoment for each frequency
     * interval [w_i, w_{i+1}) for i = 0, N-1
     * \f$I(i,n) = /int_{\omega_i}^{\omega_{i+1}} \omega^n S_w(\omega) d\omega\f$
     *
     * @param w The wave frequencies from 0 to N
     * @param maxMoment The maximum moment to be calculated
     * @return I(i,n) The unit moments of the spectrum
     */
    virtual Eigen::ArrayXXd integrate_partial_moments(const Eigen::Ref<const Eigen::ArrayXd>& w,
                                                      const int& maxMoment,
                                                      const double& dw = 0.1) const;

    /**
     * Returns the mean value
     * @return meanValue The mean value.
     */
    double getMeanValue() const
    {
        return meanValue ;
    }

    /**
     * Returns the heading
     * @return heading The heading.
     */
    double getHeading() const
    {
        return heading ;
    }

    const std::string getName() const
    {
        return name_ ;
    }

    double getTailOrder() const
    {
        return tailOrder_;
    }

    virtual double getM0() const {throw std::logic_error("getM0 : not implemented for this wave spectrum"); };

    double heading ;
    double meanValue ;
    
    
    // Discretize the frequency range, with higher density where the energy is. If seed != -1 => random offset
    Eigen::ArrayXd getDiscreteFrequencyEdges(double wmin, double wmax, unsigned int nbSeed, int seed) const;

    // Discretize the frequency range, with higher density where the energy is. If seed != -1 => random offset
    Eigen::ArrayXd getDiscreteFrequencies(double wmin, double wmax, unsigned int nbSeed, int seed) const;
    

protected:
    /**
     * The spectrum name which has to be set in each derived class.
     */
    std::string name_ ;

    double tailOrder_;

} ;

}
}
