#include "API.hpp"

using namespace BV::Spectral;

Eigen::ArrayXd API::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if (fp <= 0.)
        return Eigen::ArrayXd::Zero(w.size()) ;

    Eigen::ArrayXd sw(w.size()) ;
    const double wp (2. * M_PI * fp) ;
    const double sv = 0.164 * meanValue ;
    const double sv2 = sv * sv ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i] ;
        sw[i] = sv2 / wp * std::pow(1. + 1.5 * wi / wp, -5./3.) ;
    }
    return sw ;
}
