#include "Spectrum.hpp"
#include <iostream>
#include <cmath>
#include <array>
#include <cassert>
#include "Tools/SpdLogger.hpp"

#ifdef _MSC_VER
#include <intrin.h>
#pragma intrinsic(__rdtsc)
#endif

#ifndef _MSC_VER
#include <x86intrin.h>
#endif

using namespace BV::Spectral ;

std::tuple<double, double> Spectrum::energyRange(double energyRatio,
                                                 double wmin, double wmax,
                                                 double dw) const
{
    int nbfreq = static_cast<int>((wmax - wmin) / dw) ;
    Eigen::ArrayXd freq(nbfreq) ;
    for (int ifreq = 0; ifreq < nbfreq; ++ifreq)
    {
        freq(ifreq) = wmin + ifreq * dw ;
    }

    //Compute wave spectrum on the whole frequency range
    Eigen::ArrayXd spec = compute(freq) ;

    bool checkSpectrum(true) ;
    double energy ;
    try
    {
        energy = getM0() * energyRatio / dw;
    }
    catch(std::logic_error const &e)
    {
        // pass getM0 if not available, no check can be done.
        checkSpectrum = false ;
        energy = spec.sum() * energyRatio;
    }
    if (checkSpectrum && ( spec.sum() < energy ))
    {
        spdlog::error(  "Energy in range [{}, {}] is {}, which is lower than the target energy {}",wmin, wmax, spec.sum() * dw, getM0() * energyRatio);
        throw std::logic_error("Not enough energy in the starting range");
    }

    //Get spectrum maximum
    Eigen::ArrayXd::Index itp ;
    spec.maxCoeff(&itp) ;

    int iwmin = 0 ;
    int iwmax = 0 ;

    while (spec.segment(itp - iwmin, iwmax + iwmin).sum() < energy)
    {
        // lower bound has been reached, extend upper bound
        if (itp - iwmin == 0)
        {
            iwmax = iwmax + 1 ;
        }
        // upper bound has been reached, extend lower bound
        else if (itp + iwmax == nbfreq-1)
        {
            iwmin = iwmin + 1 ;
        }
        // no bound was reached, extend lower bound if it gives more or same amount of energy than upper bound
        else if (spec(itp - iwmin - 1) >= spec(itp + iwmax + 1))
        {
            iwmin = iwmin + 1 ;
        }
        // no bound was reached, extend upper bound if it gives more energy than lower bound
        else
        {
            iwmax = iwmax + 1 ;
        }
    }
    return std::make_tuple(freq(itp - iwmin), freq(itp + iwmax)) ;
}

Eigen::ArrayXXd Spectrum::integrate_partial_moments(const Eigen::Ref<const Eigen::ArrayXd>& w,
                                                    const int& maxMoment,
                                                    const double& dw) const
{
    assert(w.size() > 1);
    assert(maxMoment > 0);
    assert(dw > 1.0e-8);

    Eigen::ArrayXXd unitMoments(w.size()-1, maxMoment+1);
    Eigen::ArrayXd dw_i(w.size() - 1);  // dw_i on the interval [w_i, w_{i+1})
    Eigen::ArrayXi d_i(w.size() - 1);   // Number of point per each interval
    Eigen::Index N_(0);
    for (Eigen::Index i = 0; i < w.size() - 1; ++i)
    {
        d_i(i) = int((w(i + 1) - w(i)) / dw) +1;
        dw_i(i) = (w(i + 1) - w(i)) / d_i(i);
        N_ += d_i(i);
    }
    N_++;
    Eigen::ArrayXd w_int(N_);
    Eigen::Index ind(0);
    for (Eigen::Index i = 0; i < w.size() - 1; ++i)
    {
        for (Eigen::Index j = 0; j < d_i(i); ++j)
        {
            w_int(ind++) = w[i] + j * dw_i(i);
        }
    }
    w_int(ind) = w(w.size() - 1);
    Eigen::ArrayXd sw(compute(w_int));
    // Integration, on each [w_i, w_{i+1}] interval we use trapezoid method
    // first node
    Eigen::Index ind1(0);
    Eigen::ArrayXd f1(maxMoment + 1);
    f1[0] = sw[ind1];                       // w^0 sw[ind1]
    for (Eigen::Index k = 1; k < maxMoment + 1; ++k)
    {
        f1[k] = f1[k - 1] * w_int[ind1];    // w^k sw[ind1]
    }
    // integrals
    for (Eigen::Index i = 0; i < w.size() - 1; ++i)
    {
        Eigen::ArrayXd sum(Eigen::ArrayXd::Zero(maxMoment+1));
        for (Eigen::Index j = 0; j < d_i(i); j++)
        {
            // second node
            Eigen::Index ind2(ind1 + 1);
            Eigen::ArrayXd f2(maxMoment + 1);
            f2[0] = sw[ind2];                       // w^0 sw[ind2]
            for (Eigen::Index k = 1; k < maxMoment + 1; ++k)
            {
                f2[k] = f2[k - 1] * w_int[ind2];    // w^k sw[ind2]
            }
            sum += dw_i(i) * (f1 + f2) * 0.5;
            // for the next iteration, the first node is the second node of this iteration
            ind1 = ind2;    // point index
            f1 = f2;        // function's value at this point
        }
        unitMoments.row(i) = sum;
    }
    return unitMoments;
}

Eigen::ArrayXd Spectrum::getDiscreteFrequencyEdges(double wmin, double wmax, unsigned int nbSeed, int seed) const
{
    double eMin = 0.0, eMax = 0.0;
    std::tie(eMin, eMax) = energyRange(0.85, wmin, wmax, 0.001);

    int nbSeed_nose = int(nbSeed * 0.5 * (eMin - wmin) / (wmax - wmin - (eMax - eMin)));
    int nbSeed_tail = std::max(1, int(nbSeed * 0.5 * (wmax - eMax) / (wmax - wmin - (eMax - eMin))));
    int nbSeed_peak = nbSeed - nbSeed_nose - nbSeed_tail;

    double meanDw_tail = (wmax - eMax) / nbSeed_tail;
    double meanDw_peak = (eMax - eMin) / nbSeed_peak;

    //Random vector for frequency shift
    Eigen::ArrayXd randomShift;
    Eigen::ArrayXd list_freq = Eigen::ArrayXd::Zero(nbSeed + 1);
    Eigen::ArrayXd center = Eigen::ArrayXd::Zero(nbSeed);

    if (seed == -1 ) 
    {
        randomShift = Eigen::ArrayXd::Zero(nbSeed);
    }
    else
    {
        if (seed == 0)
        {
            seed = static_cast<unsigned int>(__rdtsc()); // see comments for rdtsc few lines above
        }
        std::srand(seed);
        randomShift = Eigen::ArrayXd::Random(nbSeed);  //Between -1 and 1 !
        randomShift *= 0.475;
    }

    //-------- Nose
    list_freq(0) = wmin;
    if (nbSeed_nose > 0)
    {
        double meanDw_nose = (eMin - wmin) / nbSeed_nose;

        for (Eigen::Index i = 1; i < nbSeed_nose; ++i)
        {
            list_freq(i) = wmin + i * meanDw_nose
                + randomShift(i - 1) * meanDw_nose;
        }
    }

    //-------- Peak
    list_freq(nbSeed_nose) = eMin;
    for (Eigen::Index i = 1; i < nbSeed_peak; ++i)
    {
        list_freq(i + nbSeed_nose) = eMin + i * meanDw_peak
            + randomShift(i + nbSeed_nose - 1) * meanDw_peak;
    }

    //-------- Tail
    list_freq(nbSeed_nose + nbSeed_peak) = eMax;
    for (Eigen::Index i = 1; i < nbSeed_tail; ++i)
    {
        list_freq(i + nbSeed_nose + nbSeed_peak) = eMax + i * meanDw_tail
            + randomShift(i + nbSeed_nose + nbSeed_peak - 1) * meanDw_tail;
    }
    list_freq(nbSeed) = wmax;

    return list_freq;
}

Eigen::ArrayXd Spectrum::getDiscreteFrequencies(double wmin, double wmax, unsigned int nbSeed, int seed) const
{
    auto list_freq = getDiscreteFrequencyEdges(wmin, wmax, nbSeed, seed);
    Eigen::ArrayXd w_ = Eigen::ArrayXd::Zero( nbSeed );
    for (Eigen::Index i = 0; i < nbSeed; ++i)
    {
        w_(i) = 0.5*(list_freq(i + 1) + list_freq(i));
    }
    return w_;
}
