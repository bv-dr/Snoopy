#pragma once

#include "SpectralExport.hpp"

namespace BV {
namespace Spectral {

enum struct FrequencyInterpolationStrategies
{
    W_W, W_DW
} ;

enum struct QtfStorageType {W_W, W_DW} ;

namespace Details {

struct ComplexSymmetry
{
    static std::complex<double> get(const std::complex<double> & val)
    {
        return std::conj(val) ;
    }
} ;

struct PhasisSymmetry
{
    static double get(const double & val)
    {
        return -val ;
    }
} ;

struct ModuleSymmetry
{
    static double get(const double & val)
    {
        return val ;
    }
} ;

struct RealSymmetry
{
    static double get(const double & val)
    {
        return val ;
    }
} ;

struct ImagSymmetry
{
    static double get(const double & val)
    {
        return -val ;
    }
} ;

struct ComplexSymmetrySlice
{
    template <typename SliceType>
    static auto get(const SliceType & val)
    {
        return val.conjugate() ;
    }
} ;

struct PhasisSymmetrySlice
{
    template <typename SliceType>
    static auto get(const SliceType & val)
    {
        return -val ;
    }
} ;

struct ModuleSymmetrySlice
{
    template <typename SliceType>
    static auto get(const SliceType & val)
    {
        return val ;
    }
} ;

struct RealSymmetrySlice
{
    template <typename SliceType>
    static auto get(const SliceType & val)
    {
        return val ;
    }
} ;

struct ImagSymmetrySlice
{
    template <typename SliceType>
    static auto get(const SliceType & val)
    {
        return -val ;
    }
} ;

template <typename T>
struct SymmetryFuncGetter ;

template <>
struct SymmetryFuncGetter<ComplexSymmetry>
{
    using Type = ComplexSymmetrySlice ;
} ;

template <>
struct SymmetryFuncGetter<ModuleSymmetry>
{
    using Type = ModuleSymmetrySlice ;
} ;

template <>
struct SymmetryFuncGetter<PhasisSymmetry>
{
    using Type = PhasisSymmetrySlice ;
} ;

template <>
struct SymmetryFuncGetter<RealSymmetry>
{
    using Type = RealSymmetrySlice ;
} ;

template <>
struct SymmetryFuncGetter<ImagSymmetry>
{
    using Type = ImagSymmetrySlice ;
} ;

// Objects needed by the interpolation scheme

template <typename Derived, typename SymmetryFunction>
class QtfTensorFrequencyView
{
public:
    using IndexType = typename Derived::Index ;
private:
    IndexType iw_ ;
    IndexType idw_ ;
    Derived & master_ ;
    bool useSym_ ;
    using SliceType = Eigen::TensorSlicingOp<
                           const Eigen::DSizes<IndexType, 3>,
                           const Eigen::DSizes<IndexType, 3>,
                           Eigen::Tensor<double, 3>
                                            > ;
    using ConstSliceType = Eigen::TensorSlicingOp<
                           const Eigen::DSizes<IndexType, 3>,
                           const Eigen::DSizes<IndexType, 3>,
                           const Eigen::Tensor<double, 3>
                                                 > ;
    SliceType slice_ ;
    //Eigen::TensorRef<typename Derived::InnerType> slice_ ;
public:
    using DerivedType = Derived ;
    QtfTensorFrequencyView(const IndexType & iw, const IndexType & idw,
                           Derived & master) :
        iw_(iw), idw_(idw), master_(master),
        useSym_((master.getFrequencyInterpStrategy() == FrequencyInterpolationStrategies::W_W) ? idw < iw : false),
        // FIXME this is a bit hard to understand but it is necessary
        // as in (w, w) we may need values not present in the tensor
        // (symmetric values) when interpolating the diagonal...
        slice_(master.getData(),
               Eigen::DSizes<IndexType, 3> {0,
                       (master.getFrequencyInterpStrategy() == FrequencyInterpolationStrategies::W_DW) ?
                           master.getIndiWjDW(iw, idw) :
                           (useSym_ ?
                               master.getIndiWjW(idw, iw) :
                               master.getIndiWjW(iw, idw)), // idw is iw2
                                                          0},
               Eigen::DSizes<IndexType, 3> {master.dimension(0), 1, master.dimension(2)})
    {
    }

    Eigen::DSizes<IndexType, 3> dimensions() const
    {
        Eigen::DSizes<IndexType, 3> dims {master_.dimension(0), 1, master_.dimension(2)} ;
        return dims ;
    }

    const typename Derived::ArrayXu & getDWSizes() const
    {
        return master_.getDWSizes() ;
    }

    const typename Derived::ArrayXu & getDWSizesSum() const
    {
        return master_.getDWSizesSum() ;
    }

    typename Derived::Type operator+(const QtfTensorFrequencyView<Derived, SymmetryFunction> & other) const
    {
        Eigen::Array<IndexType, Eigen::Dynamic, 1> dWSizes(1) ;
        dWSizes << 1 ;
        Eigen::Array<IndexType, Eigen::Dynamic, 1> dWSizesSum(1) ;
        dWSizesSum << 0 ;
        if (useSym_)
        {
            if (other.useSym_)
            {
                return typename Derived::Type((SymmetryFunction::get(slice_)
                                               +SymmetryFunction::get(other.slice_)).eval(),
                                              dWSizes, dWSizesSum) ;
            }
            else
            {
                return typename Derived::Type((SymmetryFunction::get(slice_)
                                               +other.slice_).eval(),
                                              dWSizes, dWSizesSum) ;
            }
        }
        else if (other.useSym_)
        {
            return typename Derived::Type((slice_
                                           +SymmetryFunction::get(other.slice_)).eval(),
                                          dWSizes, dWSizesSum) ;
        }
        return typename Derived::Type((slice_+other.slice_).eval(), dWSizes, dWSizesSum) ;
    }

    typename Derived::Type operator*(const double & scale) const
    {
        Eigen::Array<IndexType, Eigen::Dynamic, 1> dWSizes(1) ;
        dWSizes << 1 ;
        Eigen::Array<IndexType, Eigen::Dynamic, 1> dWSizesSum(1) ;
        dWSizesSum << 0 ;
        if (useSym_)
        {
            return typename Derived::Type((SymmetryFunction::get(slice_) *scale).eval(),
                                          dWSizes, dWSizesSum) ;
        }
        return typename Derived::Type((slice_*scale).eval(), dWSizes, dWSizesSum) ;
    }

    void operator=(const typename Derived::Type & other)
    {
        slice_ = other.getData() ;
    }

} ;

template <typename T, typename S>
T operator*(const double & val, const QtfTensorFrequencyView<T, S> & t)
{
    return t * val ;
}

template <typename Derived>
class QtfTensorHeadingView
{
public:
    using IndexType = typename Derived::Index ;
private:
    Eigen::Array<IndexType, Eigen::Dynamic, 1> dWSizes_ ;
    Eigen::Array<IndexType, Eigen::Dynamic, 1> dWSizesSum_ ;
    using SliceType = Eigen::TensorSlicingOp<
                           const Eigen::DSizes<typename Derived::Index, 3>,
                           const Eigen::DSizes<typename Derived::Index, 3>,
                           Eigen::Tensor<double, 3>
                                            > ;
    SliceType slice_ ;
    Derived & master_ ;
    //Eigen::TensorRef<typename Derived::InnerType> slice_ ;
public:
    using DerivedType = Derived ;
    QtfTensorHeadingView(const IndexType & ih, Derived & master) :
        dWSizes_(master.getDWSizes()), dWSizesSum_(master.getDWSizesSum()),
        slice_(master.getData(),
               Eigen::DSizes<typename Derived::Index, 3> {ih, 0, 0},
               Eigen::DSizes<typename Derived::Index, 3> {1, master.dimension(1), master.dimension(2)}),
        master_(master)
    {
    }

    SliceType getData()
    {
        return slice_ ;
    }

    const SliceType getData() const
    {
        return slice_ ;
    }

    Eigen::DSizes<typename Derived::Index, 3> dimensions() const
    {
        Eigen::DSizes<typename Derived::Index, 3> dims {1, master_.dimension(1), master_.dimension(2)} ;
        return dims ;
    }

    const typename Derived::ArrayXu & getDWSizes() const
    {
        return master_.getDWSizes() ;
    }

    const typename Derived::ArrayXu & getDWSizesSum() const
    {
        return master_.getDWSizesSum() ;
    }

    typename Derived::Type operator+(const QtfTensorHeadingView<Derived> & other) const
    {
        return typename Derived::Type((slice_+other.slice_).eval(), dWSizes_, dWSizesSum_) ;
    }

    typename Derived::Type operator-(const QtfTensorHeadingView<const Derived> & other) const
    {
        return typename Derived::Type((slice_-other.slice_).eval(), dWSizes_, dWSizesSum_) ;
    }

    typename Derived::Type operator*(const double & scale) const
    {
       return typename Derived::Type((slice_*scale).eval(), dWSizes_, dWSizesSum_) ;
    }

    void operator=(const typename Derived::Type & other)
    {
        slice_ = other.getData() ;
    }

    void operator=(const QtfTensorHeadingView<const Derived> & other)
    {
        slice_ = other.getData() ;
    }
} ;

template <typename T>
T operator*(const double & val, const QtfTensorHeadingView<T> & t)
{
    return t * val ;
}

} // End of namespace Details

template <typename T, typename SymmetryFunction_>
class QtfTensor
{
public:
    using IndexType = Eigen::Index ;
    using ArrayXu = Eigen::Array<IndexType, Eigen::Dynamic, 1> ;

private:
    Eigen::Tensor<T, 3> data_ ;
    ArrayXu dWSizes_ ;
    ArrayXu dWSizesSum_ ;
    mutable FrequencyInterpolationStrategies interpStrategy_ ;
    // Below are temporary variables to store the real and imag values
    mutable bool realIsSet_ ;
    mutable bool imagIsSet_ ;
    mutable Eigen::Tensor<double, 3> real_ ;
    mutable Eigen::Tensor<double, 3> imag_ ;

    void initializeWDW_(const Eigen::Tensor<T, 4> & data)
    {
        IndexType nH(data.dimension(0)) ;
        IndexType nw(data.dimension(1)) ;
        IndexType ndw(std::min(data.dimension(2), nw+1)) ;
        IndexType nm(data.dimension(3)) ;
        IndexType innerSize(ndw*nw - static_cast<IndexType>(ndw*(ndw-1)/2.)) ;
        data_ = Eigen::Tensor<T, 3>(nH, innerSize, nm) ;
        dWSizes_ = ArrayXu::Constant(nw, ndw) ;
        dWSizesSum_ = ArrayXu::Constant(nw, 0) ;
        // Index should be upper triangle of the matrix, up to nDw+1 (band)
        // x x x x
        //   x x x x
        //     x x x x
        //       x x x x
        //         x x x
        //           x x
        //             x
        // In addition we don't want to store the zeros contained in the
        // original tensor
        IndexType ind(0) ;
        IndexType crit(nw - ndw) ;
        for (IndexType iH=0; iH<nH; ++iH)
        {
            for (IndexType iw=0; iw<nw; ++iw)
            {
                for (IndexType idw=0; idw<ndw; ++idw)
                {
                    ind = iw*ndw+idw ;
                    if (iw > crit)
                    {
                        if (idw > nw - iw - 1)
                        {
                            dWSizes_(iw) = idw ;
                            break ; // We don't want the zeros
                        }
                        else
                        {
                            ind -= static_cast<IndexType>((iw-crit)*(iw-crit-1)/2.) ;
                        }
                    }
                    for (IndexType im=0; im<nm; ++im)
                    {
                        //this->operator()(iH, ind, im) = data(iH, iw, idw, im) ;
                        data_(iH, ind, im) = data(iH, iw, idw, im) ;
                    }
                }
                if (iw > 0)
                {
                    dWSizesSum_[iw] = dWSizes_[iw-1] + dWSizesSum_[iw-1] ;
                }
            }
        }
    }

    void initializeWW_(const Eigen::Tensor<T, 4> & data)
    {
        IndexType nH(data.dimension(0)) ;
        IndexType nw(data.dimension(1)) ;
        IndexType nm(data.dimension(3)) ;
        IndexType innerSize(nw*nw - static_cast<IndexType>(nw*(nw-1)/2.)) ;
        data_ = Eigen::Tensor<T, 3>(nH, innerSize, nm) ;
        dWSizes_ = ArrayXu::Constant(nw, nw) ;
        dWSizesSum_ = ArrayXu::Constant(nw, 0) ;
        // Index should be upper triangle of the matrix
        // x x x x x x x
        //   x x x x x x
        //     x x x x x
        //       x x x x
        //         x x x
        //           x x
        //             x
        // In addition we don't want to store the zeros contained in the
        // original tensor FIXME get dwMax ?
        for (IndexType iH=0; iH<nH; ++iH)
        {
            IndexType ind(0) ;
            for (IndexType iw=0; iw<nw; ++iw)
            {
                dWSizes_[iw] = nw - iw ;
                for (IndexType iw2=iw; iw2<nw; ++iw2)
                {
                    for (IndexType im=0; im<nm; ++im)
                    {
                        //this->operator()(iH, ind, im) = data(iH, iw, idw, im) ;
                        data_(iH, ind, im) = data(iH, iw, iw2, im) ;
                    }
                    ++ind ;
                }
                if (iw > 0)
                {
                    dWSizesSum_[iw] = dWSizes_[iw-1] + dWSizesSum_[iw-1] ;
                }
            }
        }

    }

public:
    using SymmetryFunction = SymmetryFunction_ ;
    using Scalar = T ;
    using Type = QtfTensor<T, SymmetryFunction> ;
    using InnerType = Eigen::Tensor<T, 3> ;
    using Dimensions = typename Eigen::Tensor<T, 3>::Dimensions ;
    using Index = typename Eigen::Tensor<T, 3>::Index ;

    QtfTensor():
        interpStrategy_(FrequencyInterpolationStrategies::W_DW),
        realIsSet_(false), imagIsSet_(false)
    {
    }

    // Constructor with values from an input file (all square)
    QtfTensor(const Eigen::Tensor<T, 4> & data,
              const QtfStorageType & qtfStorageType=QtfStorageType::W_DW) :
        realIsSet_(false), imagIsSet_(false)
    {
        if (qtfStorageType == QtfStorageType::W_W)
        {
            initializeWW_(data) ;
        }
        else
        {
            initializeWDW_(data) ;
        }
        interpStrategy_ = FrequencyInterpolationStrategies::W_DW ;
    }

    QtfTensor(const Eigen::TensorRef<const Eigen::Tensor<T, 3> > & data,
              const ArrayXu & dWSizes,
              const ArrayXu & dWSizesSum) :
        data_(data), dWSizes_(dWSizes), dWSizesSum_(dWSizesSum),
        interpStrategy_(FrequencyInterpolationStrategies::W_DW),
        realIsSet_(false), imagIsSet_(false)
    {
    }

    QtfTensor(const Dimensions & dims, const ArrayXu & dWSizes,
              const ArrayXu & dWSizesSum) :
        data_(dims), dWSizes_(dWSizes), dWSizesSum_(dWSizesSum),
        interpStrategy_(FrequencyInterpolationStrategies::W_DW),
        realIsSet_(false), imagIsSet_(false)
    {
        data_.setZero() ;
    }

    void setfrequencyInterpStrategy(const FrequencyInterpolationStrategies & strategy) const
    {
        interpStrategy_ = strategy ;
    }
    const FrequencyInterpolationStrategies & getFrequencyInterpStrategy() const
    {
        return interpStrategy_ ;
    }

    const InnerType & getData() const
    {
        return data_ ;
    }

    InnerType & getData()
    {
        return data_ ;
    }

    const ArrayXu & getDWSizes() const
    {
        return dWSizes_ ;
    }

    const ArrayXu & getDWSizesSum() const
    {
        return dWSizesSum_ ;
    }

    Eigen::Index size() const
    {
        return data_.size() ;
    }

    Dimensions dimensions() const
    {
        return data_.dimensions() ;
    }

    Index dimension(const IndexType & ind) const
    {
        return data_.dimension(ind) ;
    }

    // FIXME use enable_if to make sure that T is of complex type and use T::value_type
    QtfTensor<double, Details::ModuleSymmetry> abs() const
    {
        QtfTensor<double, Details::ModuleSymmetry> tmp(data_.abs().eval(),
                                                       dWSizes_, dWSizesSum_) ;
        tmp.setfrequencyInterpStrategy(interpStrategy_) ;
        return tmp ;
    }

    QtfTensor<double, Details::RealSymmetry> real() const
    {
        if (!realIsSet_)
        {
            real_ = data_.real().eval() ;
            realIsSet_ = true ;
        }
        QtfTensor<double, Details::RealSymmetry> tmp(
                                            real_, dWSizes_, dWSizesSum_
                                                    ) ;
        tmp.setfrequencyInterpStrategy(interpStrategy_) ;
        return tmp ;
    }

    QtfTensor<double, Details::ImagSymmetry> imag() const
    {
        if (!imagIsSet_)
        {
            imag_ = data_.imag().eval() ;
            imagIsSet_ = true ;
        }
        QtfTensor<double, Details::ImagSymmetry> tmp(
                                            imag_, dWSizes_, dWSizesSum_
                                                    ) ;
        tmp.setfrequencyInterpStrategy(interpStrategy_) ;
        return tmp ;
    }

    void setFrequency(const IndexType & iw, const Type & other)
    {
        Dimensions shape(dimensions()) ;
        Eigen::DSizes<Index, 3> offsets{0, getIndiWjDW(iw, 0), 0} ;
        Eigen::DSizes<Index, 3> extents{shape[0], other.dimension(1), shape[2]} ;
        data_.slice(offsets, extents) = other.getData() ;
    }

    IndexType getIndiWjDW(IndexType iW, IndexType idW) const
    {
        if (dWSizes_[iW] <= idW)
        {
            //throw "Wrong index in QtfTensor, out of dw range" ;
            //std::cout << "FIXME: Wrong index in QtfTensor, out of dw range " << idW << " >= " << dWSizes_[iW] << std::endl ;
            idW = dWSizes_[iW] - 1 ;
        }
        return dWSizesSum_[iW] + idW ;
    }

    IndexType getIndiWjW(IndexType iW1, IndexType iW2) const
    {
        if (iW2 < iW1)
        {
            throw "Unreachable indices in QtfTensor" ;
        }
        return getIndiWjDW(iW1, iW2-iW1) ;
    }

    T getFromiWjW(IndexType iH, IndexType iW1, IndexType iW2, IndexType iMode) const
    {
        if (iW1 <= iW2)
        {
            if (iW2 > (iW1 + dWSizes_[iW1] - 1))
            {
                return T(0.) ;
            }
            return getFromiWjDW(iH, iW1, iW2-iW1, iMode) ;
        }
        else
        {
            if (iW1 > (iW2 + dWSizes_[iW2] - 1))
            {
                return T(0.) ;
            }
            return SymmetryFunction::get(getFromiWjDW(iH, iW2, iW1-iW2, iMode)) ;
        }
    }

    T getFromiWjDW(IndexType iH, IndexType iW, IndexType idW, IndexType iMode) const
    {
        if (dWSizes_[iW] <= idW)
        {
            return T(0.) ;
        }
        //return this->operator()(iH, getIndiWjDW(iW, idW), iMode) ;
        return data_(iH, getIndiWjDW(iW, idW), iMode) ;
    }

    Eigen::Tensor<T, 1> getFromiWjDW(IndexType iH, IndexType iW, IndexType idW) const
    {
        IndexType nModes(dimension(2)) ;
        Eigen::Tensor<T, 1> zero(nModes) ;
        zero.setZero() ;
        if (dWSizes_[iW] <= idW)
        {
            return zero ;
        }
        for (IndexType iMode=0; iMode<nModes; ++iMode)
        {
            zero(iMode) = data_(iH, getIndiWjDW(iW, idW), iMode) ;
        }
        return zero ;
    }

    Type & operator+=(const Type & other)
    {
        data_ += other.data_ ;
        return *this ;
    }

    Type operator+(const Type & other) const
    {
        Type tmp(*this) ;
        tmp += other ;
        return tmp ;
    }

    Type operator+(const Details::QtfTensorHeadingView<const Type> & other) const
    {
        return Type((data_+other.getData()).eval(), dWSizes_, dWSizesSum_) ;
    }

    Type & operator-=(const Type & other)
    {
        data_ -= other.data_ ;
        return *this ;
    }

    Type operator-(const Type & other) const
    {
        Type tmp(*this) ;
        tmp -= other ;
        return tmp ;
    }

    Type operator*(const Scalar & scale) const
    {
        return Type((data_*scale).eval(), dWSizes_, dWSizesSum_) ;
    }

    Eigen::Tensor<T, 4> toWDWTensor() const
    {
        Eigen::DSizes<typename Eigen::Tensor<T, 4>::Index, 3> dims(data_.dimensions()) ;
        IndexType nh(dims[0]) ;
        IndexType nws(dWSizes_.size()) ;
        IndexType ndws(dWSizes_[0]) ;
        IndexType nm(dims[2]) ;
        Eigen::DSizes<typename Eigen::Tensor<T, 4>::Index, 4> newDims(nh, nws, ndws, nm) ;

        Eigen::Tensor<T, 4> res(newDims) ;
        for (IndexType ih=0; ih<nh; ++ih)
        {
            for (IndexType iw=0; iw<nws; ++iw)
            {
                for (IndexType idw=0; idw<ndws; ++idw)
                {
                    for (IndexType im=0; im<nm; ++im)
                    {
                        res(ih, iw, idw, im) = getFromiWjDW(ih, iw, idw, im) ;
                    }
                }
            }
        }
        return res ;
    }
} ;

template <typename T, typename SymmetryFunction>
QtfTensor<T, SymmetryFunction> operator*(const T & scale,
                                         const QtfTensor<T, SymmetryFunction> & other)
{
    return other * scale ;
}

using QtfTensorComplex = QtfTensor<std::complex<double>, Details::ComplexSymmetry> ;
using QtfTensorModule = QtfTensor<double, Details::ModuleSymmetry> ;
using QtfTensorPhasis = QtfTensor<double, Details::PhasisSymmetry> ;
using QtfTensorReal = QtfTensor<double, Details::RealSymmetry> ;
using QtfTensorImag = QtfTensor<double, Details::ImagSymmetry> ;

} // End of namespace Spectral
} // End of namespace BV

namespace BV {
namespace Math {
namespace Interpolators {
namespace Details {

template <typename T, typename SymmetryFunction>
struct Data<2, BV::Spectral::QtfTensor<T, SymmetryFunction>, void>
{
    typedef typename BV::Spectral::QtfTensor<T, SymmetryFunction>::IndexType IndexType ;
    typedef T ScalarType ;
    enum {
        Rank = 3
    } ;
    typedef BV::Spectral::QtfTensor<T, SymmetryFunction> Type ;
    typedef BV::Spectral::QtfTensor<T, SymmetryFunction> CopyType ;
    typedef BV::Spectral::Details::QtfTensorFrequencyView<Type, typename BV::Spectral::Details::SymmetryFuncGetter<SymmetryFunction>::Type> InnerType ;
    typedef BV::Spectral::Details::QtfTensorFrequencyView<Type, typename BV::Spectral::Details::SymmetryFuncGetter<SymmetryFunction>::Type> InnerTypeRef ;
    typedef BV::Spectral::Details::QtfTensorFrequencyView<const Type, typename BV::Spectral::Details::SymmetryFuncGetter<SymmetryFunction>::Type> InnerTypeConstRef ;
    //typedef BV::Spectral::QtfTensor<T, SymmetryFunction> InnerType ;
    //typedef Eigen::TensorRef<Eigen::Tensor<ScalarType, Rank-2> > InnerTypeRef ;
    //typedef Eigen::TensorRef<const Eigen::Tensor<ScalarType, Rank-2> > InnerTypeConstRef ;

    static CopyType getResult(const Eigen::ArrayXd & axis1,
                              const Eigen::ArrayXd & axis2,
                              const IndexType & axisIndex1,
                              const IndexType & axisIndex2,
                              const Type & data,
                              bool setZero=false)
    {
        // FIXME for frequency interpolation (which is why we are doing this),
        // axis indices are 1 and 2.
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[1] = axis2.size() ; // Only for 1 frequency...1 w*n dw
        using Au = typename Type::ArrayXu ;
        CopyType res(shape, Au::Constant(1, axis2.size()), Au::Zero(1)) ;
        res.setfrequencyInterpStrategy(data.getFrequencyInterpStrategy()) ; // FIXME
        return res ;
    }

    static CopyType getResult(const double & axis1,
                              const double & axis2,
                              const IndexType & axisIndex1,
                              const IndexType & axisIndex2,
                              const Type & data,
                              bool setZero = false)
    {
        // FIXME for frequency interpolation (which is why we are doing this),
        // axis indices are 1 and 2.
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[1] = 1 ; // Only for 1 frequency...1 w*n dw
        using Au = typename Type::ArrayXu ;
        CopyType res(shape, Au::Constant(1, 1), Au::Zero(1)) ;
        res.setfrequencyInterpStrategy(data.getFrequencyInterpStrategy()) ; // FIXME
        return res ;
    }

    template <typename AxisIndex>
    static auto get(const Type & data, const IndexType & i,
                    const IndexType & j,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2)
    {
        // FIXME for frequency interpolation (which is why we are doing this),
        // axis indices are 1 and 2.
        return InnerTypeConstRef(i, j, data) ;
    }

    template <typename ResultType, typename AxisIndex>
    static auto get(ResultType & result, const IndexType & i,
                    const IndexType & j,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2)
    {
        // FIXME for frequency interpolation (which is why we are doing this),
        // axis indices are 1 and 2.
        return InnerTypeRef(i, j, result) ;
    }

} ;

template <typename T, typename SymmetryFunction>
struct Data<1, BV::Spectral::QtfTensor<T, SymmetryFunction>, void>
{
    typedef typename BV::Spectral::QtfTensor<T, SymmetryFunction>::IndexType IndexType ;
    typedef T ScalarType ;
    enum {
        Rank = 3
    } ;
    typedef BV::Spectral::QtfTensor<T, SymmetryFunction> Type ;
    typedef BV::Spectral::QtfTensor<T, SymmetryFunction> CopyType ;
    typedef BV::Spectral::Details::QtfTensorHeadingView<Type> InnerType ;
    typedef BV::Spectral::Details::QtfTensorHeadingView<Type> InnerTypeRef ;
    typedef BV::Spectral::Details::QtfTensorHeadingView<const Type> InnerTypeConstRef ;
    //typedef Eigen::TensorRef<Eigen::Tensor<ScalarType, Rank-2> > InnerTypeRef ;
    //typedef Eigen::TensorRef<const Eigen::Tensor<ScalarType, Rank-2> > InnerTypeConstRef ;

    template <typename AxisType>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        typename CopyType::Dimensions shape(data.dimensions()) ;
        shape[0] = Axis<AxisType>::size(axis) ;
        return CopyType(shape, data.getDWSizes(), data.getDWSizesSum()) ;
    }

    template <typename AxisType, typename AxisIndex>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex,
                              bool setZero = false)
    {
        typename CopyType::Dimensions shape(data.dimensions()) ;
        shape[axisIndex] = Axis<AxisType>::size(axis) ;
        return CopyType(shape, data.getDWSizes(), data.getDWSizesSum()) ;
    }

    template <typename AxisIndex>
    static auto get(const Type & data, const IndexType & ind,
                    const AxisIndex & axisIndex)
    {
        return InnerTypeConstRef(ind, data) ;
    }

    template <typename ResultType, typename AxisIndex>
    static auto get(ResultType & result, const IndexType & ind,
                            const AxisIndex & axisIndex)
    {
        return InnerTypeRef(ind, result) ;
    }

} ;

template <typename Derived>
struct Data<1, BV::Spectral::Details::QtfTensorHeadingView<Derived>, void>
{
    typedef typename BV::Spectral::Details::QtfTensorHeadingView<Derived>::IndexType IndexType ;
    enum {
        Rank = 3
    } ;
    typedef Derived CopyType ;

} ;

template <typename Derived, typename SymmetryFunction>
struct Data<2, BV::Spectral::Details::QtfTensorFrequencyView<Derived, SymmetryFunction>, void>
{
    typedef typename BV::Spectral::Details::QtfTensorFrequencyView<Derived, SymmetryFunction>::IndexType IndexType ;
    enum {
        Rank = 3
    } ;
    typedef Derived CopyType ;

} ;

template <typename T, typename SymmetryFunction>
struct Data<3, BV::Spectral::QtfTensor<T, SymmetryFunction>, void>
{
    typedef typename BV::Spectral::QtfTensor<T, SymmetryFunction>::IndexType IndexType ;
    typedef T ScalarType ;
    enum {
        Rank = 3
    } ;
    typedef BV::Spectral::QtfTensor<T, SymmetryFunction> Type ;
    typedef BV::Spectral::QtfTensor<T, SymmetryFunction> CopyType ;
    typedef Eigen::Tensor<T, 1> InnerType ;
    //typedef Eigen::TensorRef<Eigen::Tensor<ScalarType, Rank-2> > InnerType ;
    //typedef Eigen::TensorRef<const Eigen::Tensor<ScalarType, Rank-3> > InnerTypeRef ;
    //typedef BV::Spectral::Details::QtfTensorHeadingView<const Type> InnerTypeConstRef ;

    template <typename AxisType1, typename AxisType2, typename AxisType3,
        typename std::enable_if<!std::is_scalar<AxisType1>::value
        && !std::is_scalar<AxisType2>::value
        && !std::is_scalar<AxisType3>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const AxisType3 & axis3, const Type & data,
                              bool setZero = false)
    {
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[0] = Axis<AxisType1>::size(axis1) ;
        shape[1] = Axis<AxisType2>::size(axis2) ;
        shape[2] = Axis<AxisType3>::size(axis3) ;
        return CopyType(shape, data.getDWSizes(), data.getDWSizesSum()) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
        typename std::enable_if<
            std::is_scalar<AxisType1>::value && std::is_scalar<AxisType2>::value
                && std::is_scalar<AxisType3>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const AxisType3 & axis3, const Type & data,
                               bool setZero = false)
    {
        InnerType res(data.dimension(2)) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex,
        typename std::enable_if<!std::is_scalar<AxisType1>::value
        && !std::is_scalar<AxisType2>::value
        && !std::is_scalar<AxisType3>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const AxisType3 & axis3,
                              const AxisIndex & axisIndex1,
                              const AxisIndex & axisIndex2,
                              const AxisIndex & axisIndex3,
                              const Type & data,
                              bool setZero = false)
    {
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[axisIndex1] = Axis<AxisType1>::size(axis1) ;
        shape[axisIndex2] = Axis<AxisType2>::size(axis2) ;
        shape[axisIndex3] = Axis<AxisType3>::size(axis3) ;
        return CopyType(shape, data.getDWSizes(), data.getDWSizesSum()) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
        typename AxisIndex,
        typename std::enable_if<std::is_scalar<AxisType1>::value
        && std::is_scalar<AxisType2>::value
        && std::is_scalar<AxisType3>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const AxisType3 & axis3,
                               const AxisIndex & axisIndex1,
                               const AxisIndex & axisIndex2,
                               const AxisIndex & axisIndex3, const Type & data,
                               bool setZero = false)
    {
        assert(axisIndex1 == 0) ;
        assert(axisIndex2 == 1) ;
        assert(axisIndex3 == 2) ;
        InnerType res(data.dimension(2)) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename DataType, typename IndexType_>
    static auto get(const DataType & data, const IndexType_ & i,
                    const IndexType_ & j, const IndexType_ & k)
    {
        return data.getFromiWjDW(i, j, k) ;
    }

} ;

} // End of namespace Details
} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV
