#pragma once

#include "Math/Interpolators/Details.hpp"
#include "QtfTensor.hpp"

/**
 * Helper function to compare floating point values for equality.
 * Taken from http://en.cppreference.com/w/cpp/types/numeric_limits/epsilon.
 */
template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type almost_equal(
    T x, T y, int ulp = 2)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::abs(x - y) < std::numeric_limits<T>::epsilon() * std::abs(x + y) * ulp
    // unless the result is subnormal
        || std::abs(x - y) < std::numeric_limits<T>::min() ;
}

namespace EigenFuture {

inline BV::Spectral::QtfTensorPhasis arg(
    const BV::Spectral::QtfTensorComplex & tensor
                                                                          )
{
    Eigen::Tensor<std::complex<double>, 3> tmp ;
    const Eigen::Tensor<std::complex<double>, 3> & data(tensor.getData()) ;
    tmp = data.unaryExpr([](std::complex<double> x)
    {   return std::complex<double>(std::arg(x), 0.) ;}) ;
    Eigen::Tensor<double, 3> tmp2(tmp.real()) ;
    return BV::Spectral::QtfTensorPhasis(
        tmp2, tensor.getDWSizes(), tensor.getDWSizesSum()) ;
}

inline BV::Spectral::QtfTensorComplex
ComplexFromReIm(
    const BV::Spectral::QtfTensorReal & re,
    const BV::Spectral::QtfTensorImag & im)
{

    return BV::Spectral::QtfTensorComplex(
        re.getData().template cast<std::complex<double> >()
        + im.getData().template cast<std::complex<double> >()
            * std::complex<double>(0., 1.),
        re.getDWSizes(), re.getDWSizesSum()) ;
}

inline BV::Spectral::QtfTensorComplex
ComplexFromAmpPhi(
    const BV::Spectral::QtfTensorModule & amp,
    const BV::Spectral::QtfTensorPhasis & phi)
{
    return BV::Spectral::QtfTensorComplex(
        amp.getData().template cast<std::complex<double> >()
        * (phi.getData().template cast<std::complex<double> >()
            * std::complex<double>(0., 1.)).exp(),
        amp.getDWSizes(), amp.getDWSizesSum());
}
//} // end of namespace EigenFuture
//
//namespace EigenFuture {

template <int Rank>
inline Eigen::Tensor<double, Rank> arg(const Eigen::Tensor<std::complex<double>, Rank> & tensor)
{
	Eigen::Tensor<std::complex<double>, Rank> tmp ;
    tmp = tensor.unaryExpr([](std::complex<double> x)
                           {
                                return std::complex<double>(std::arg(x), 0.) ;
                           }) ;
    return tmp.real() ;
}

template <int Rank>
inline Eigen::Tensor<std::complex<double>, Rank> ComplexFromReIm(
    const Eigen::Tensor<double, Rank> & re,
    const Eigen::Tensor<double, Rank> & im)
{
    return re.template cast<std::complex<double> >()
        + im.template cast<std::complex<double> >()
            * std::complex<double>(0., 1.) ;
}

template <int Rank>
inline Eigen::Tensor<std::complex<double>, Rank> ComplexFromAmpPhi(
    const Eigen::Tensor<double, Rank> & amp,
    const Eigen::Tensor<double, Rank> & phi)
{
    return amp.template cast<std::complex<double> >()
        * (phi.template cast<std::complex<double> >()
            * std::complex<double>(0., 1.)).exp() ;
}
} // end of namespace EigenFuture
