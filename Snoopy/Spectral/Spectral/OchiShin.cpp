#include "OchiShin.hpp"
#include "SpectralTools.hpp"

using namespace BV::Spectral ;

Eigen::ArrayXd OchiShin::compute(
    const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if ((almost_equal(z, 0.)) || (almost_equal(C, 0.)))
        return Eigen::ArrayXd::Zero(w.size()) ;

    Eigen::ArrayXd sw(w.size()) ;
    const double a = C * meanValue * meanValue ;
    const double b = z / meanValue ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi = w(i) / doublePi ;
        double x = b * fi ;
        double Fg ;
        if (x < 0.003)
        {
            Fg = 583. * x ;
        }
        else if (x < 0.1)
        {
            Fg = 420. * std::pow(x, 0.7)
                / std::pow(1. + std::pow(x, 0.35), 11.5) ;
        }
        else
        {
            Fg = 838. * x / std::pow(1. + std::pow(x, 0.35), 11.5) ;
        }
        sw[i] = a * Fg / fi ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
