#pragma once

#include "SpectralExport.hpp"
#include "TransferFunction.hpp"

namespace BV {
namespace Spectral {

template <int Rank_, typename Derived>
class RaoStorage : public AllTensorsStorage<Rank_, Derived>
{
public:
    using IndexType = typename AllTensorsStorage<Rank_, Derived>::IndexType ;
    using ComplexStorageType = typename AllTensorsStorage<Rank_, Derived>::ComplexStorageType ;
    using RealStorageType = typename AllTensorsStorage<Rank_, Derived>::RealStorageType ;
    using ImagStorageType = typename AllTensorsStorage<Rank_, Derived>::ImagStorageType ;

    using AllTensorsStorage<Rank_, Derived>::AllTensorsStorage ;

    using AllTensorsStorage<Rank_, Derived>::getComplexData ;

    ComplexStorageType getComplexData(
        IndexType axisIndex, const Eigen::ArrayXd & values,
        const Eigen::DSizes<IndexType, 3> & offsets,
        const Eigen::DSizes<IndexType, 3> & extents,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const Math::Interpolators::ExtrapolationType & extrapType) const
    {
        Eigen::Tensor<double, 3> amp(this->ampData_.slice(offsets, extents)) ;
        Eigen::Tensor<double, 3> phi(this->phiData_.slice(offsets, extents)) ;
        if (this->optimisation_ == Optimisation::CPU)
        {
            if (this->reData_.size() == 0)
            {
                this->getReal() ;
                this->getImag() ;
            }
            return this->getComplexData_(axisIndex, amp, phi,
                                         this->reData_.slice(offsets, extents),
                                         this->imData_.slice(offsets, extents),
                                         values, interpScheme,
                                         interpStrategy, extrapType) ;
        }
        else
        {
            Eigen::Tensor<std::complex<double>, 3> reIm(this->complexData_.slice(offsets, extents)) ;
            return this->getComplexData_(axisIndex, amp, phi,
                                         static_cast<RealStorageType>(reIm.real()),
                                         static_cast<ImagStorageType>(reIm.imag()),
                                         values, interpScheme,
                                         interpStrategy, extrapType) ;
        }
    }

    using AllTensorsStorage<Rank_, Derived>::operator* ;
    using AllTensorsStorage<Rank_, Derived>::operator*= ;
    using AllTensorsStorage<Rank_, Derived>::operator/ ;
    using AllTensorsStorage<Rank_, Derived>::operator/= ;

    // Overload parent operators to deal with operations on mean values

    Derived& operator+=(const Derived & rhs)
    {
        Derived & rao(AllTensorsStorage<Rank_, Derived>::operator+=(rhs)) ;
        rao.setMeanValues(rao.getMeanValues() + rhs.getMeanValues()) ;
        return rao ;
    }

    Derived & operator-=(const Derived & rhs)
    {
        Derived & rao(AllTensorsStorage<Rank_, Derived>::operator-=(rhs)) ;
        rao.setMeanValues(rao.getMeanValues() - rhs.getMeanValues()) ;
        return rao ;
    }

    Derived & operator*=(const double & rhs)
    {
        Derived & rao(AllTensorsStorage<Rank_, Derived>::operator*=(rhs)) ;
        rao.setMeanValues(rao.getMeanValues() * rhs) ;
        return rao ;
    }

    Derived & operator/=(const double & rhs)
    {
        Derived & rao(AllTensorsStorage<Rank_, Derived>::operator/=(rhs)) ;
        rao.setMeanValues(rao.getMeanValues() / rhs) ;
        return rao ;
    }

    Derived & operator*=(const Derived & rhs)
    {
        this->complexData_ *= rhs.getComplexData() ;
        this->refresh_();
        Derived & rao(static_cast<Derived&>(*this)) ;
        rao.setMeanValues(rao.getMeanValues() * rhs.getMeanValues()) ;
        return rao ;
    }

    Derived operator*(const Derived & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy *= rhs ;
        return copy ;
    }

    Derived & operator/=(const Derived & rhs)
    {
        this->complexData_ /= rhs.getComplexData() ;
        this->refresh_();
        Derived & rao(static_cast<Derived&>(*this)) ;
        rao.setMeanValues(rao.getMeanValues() / rhs.getMeanValues()) ;
        return rao ;
    }

    Derived operator/(const Derived & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy /= rhs ;
        return copy ;
    }

} ;

/**
 * @class Rao
 *
 * @brief Define a transfer function
 */
class SPECTRAL_API Rao: public HydroTransferFunction<RaoStorage<3, Rao> >
{
private:
    // Encounter frequency
    Eigen::ArrayXXd we_ ;

    // Wave number
    Eigen::ArrayXd k_;

    // Compute k and we
    void init_() ;

    // Mean value
    Eigen::ArrayXd meanValues_;


public:

    using Storage = RaoStorage<3, Rao> ;
    using IndexType = typename Storage::IndexType ;

    ~Rao()
    {
    }

    /**
     * Construct a Rao using its parameters directly.
     *
     * @param b The heading list.
     * @param w The frequency list.
     * @param modules The data module value
     * @param phases The data phase value (in rad)
     * @param speed
     * TODO add metadata* @param metadata
     */
    Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
        const ModesType& modes,
        const Eigen::Tensor<double, 3>& dataAmp,
        const Eigen::Tensor<double, 3>& dataPhi,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        double forwardSpeed = 0.,
        double depth=-1., 
        Eigen::ArrayXd meanValues = Eigen::ArrayXd::Zero(0)) ;

    Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
        const ModesType& modes,
        const Eigen::Tensor<std::complex<double>, 3>& dataReIm,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        double forwardSpeed = 0.,
        double depth=-1., 
        Eigen::ArrayXd meanValues = Eigen::ArrayXd::Zero(0)) ;

    Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Tensor<double, 3>& dataAmp,
        const Eigen::Tensor<double, 3>& dataPhi,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        double forwardSpeed = 0.,
        double depth=-1., 
        Eigen::ArrayXd meanValues = Eigen::ArrayXd::Zero(0)) ;

    Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const ModesType & modes,
        const Eigen::Tensor<double, 3>& dataAmp,
        const Eigen::Tensor<double, 3>& dataPhi,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        double forwardSpeed = 0.,
        double depth=-1., 
        Eigen::ArrayXd meanValues = Eigen::ArrayXd::Zero(0)) ;

    /**
     *  Construct from complex values
     */
    Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Tensor<std::complex<double>, 3>& dataReIm,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        double forwardSpeed = 0.,
        double depth=-1., 
        Eigen::ArrayXd meanValues = Eigen::ArrayXd::Zero(0)) ;

    Rao(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const ModesType& modes,
        const Eigen::Tensor<std::complex<double>, 3>& dataReIm,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        double forwardSpeed = 0.,
        double depth=-1., 
        Eigen::ArrayXd meanValues = Eigen::ArrayXd::Zero(0)) ;

    Rao(const std::vector<Rao> & raos, bool checkReferencePoint = false) ;

    /**
     * Initialize some Rao attributes (like encounter frequency) after it has been constructed.
     */
    const Eigen::ArrayXXd & getEncFrequencies() const ;

    const Eigen::ArrayXd & getWaveNumbers() const;

    const Eigen::ArrayXd & getMeanValues() const;

    void setMeanValues(const Eigen::ArrayXd & vals) ;

    using HydroTransferFunction<Storage>::getComplexData ;

    Eigen::ArrayXXcd getComplexAtHeadings(
        const Eigen::ArrayXd & relativeHeadings,
        const std::vector<unsigned> & indices,
        const BV::Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        BV::Math::Interpolators::ExtrapolationType extrapType) const ;

    Rao getRaoAtFrequencies(
        const Eigen::ArrayXd & ws,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        BV::Math::Interpolators::ExtrapolationType extrapType) const ;

    Rao getRaoAtHeadings(
        const Eigen::ArrayXd & bs,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        BV::Math::Interpolators::ExtrapolationType extrapType) const ;

    Rao getRaoAtModeCoefficients(
        const Eigen::ArrayXd & coefs,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        BV::Math::Interpolators::ExtrapolationType extrapType) const ;


    // Check if RAO is ready for spectral calculation
    bool isReadyForSpectral() const;

    // Check if RAO is ready for interpolation
    bool isReadyForInterpolation() const;


    // Check if RAO has constant frequency spacing, return -1 if not
    double getdw() const;


    // Check if RAO has constant heading spacing, return -1 if not
    double getdb() const;


    // Check if RAO is 0-360
    bool is360() const;

    /**
     * Adjust rao headings to be in the range from 0.0 (including) to 2pi (including)
     *
     * Assumption: heading is sorted: \beta_0 < \beta_1 < \beta_2 < ... < \beta_n < \beta_N
     * here N = n+1
     */
    Rao getRaoIn2piRange() const ;

    //return the derivate transfer function (*(i*we)**n)
    //FIXME : Handle encounter frequency !
    Rao getDerivate(int n) const ;

    void setFrequencies(const Eigen::Ref<const Eigen::ArrayXd>& w);

    void setModes(const ModesType& modes);

    // Derivate the RAO
    Rao& derivate(int n);

    /**
     * Return the linear interpolation coefficients of \f$|RAO|^2\f$ as function of frequencies for fixed headings
     * @return coefficients, which is array of the shape (nbHead x nbFreq-1 x 2 x nbMode), where
     * the before last index correponds to a pair \f$(a_0,a_1)\f$: \f$(x) = a_0 +a_1 x\f$
     */
    Eigen::Tensor<double, 4> getRao2LinearCoefficientsForOmega() const;
    /**
     * Get the linear interpolation coefficients of \f$|RAO|^2\f$ as function of frequencies for given headings list
     * @param b List of headings, for which the coefficients must be found. It is assumed that b is sorted in the range \f$[0, 2\pi)\f$ and has unique values
     * This is to speed up the calculation
     * @return coefficients, which is array of the shape (b.size() x nbFreq-1 x 3 x nbMode), where
     * the before last index correponds to \f$(a_0,a_1,a_2)\f$: \f$|RAO(x;b)|^2 = a_0 +a_1 x +a_2 x^2\f$
     */
    Eigen::Tensor<double, 4> getRao2LinearCoefficientsForOmegaAtBeta(const Eigen::Ref<const Eigen::ArrayXd>& b) const;

    /**
     * Similar to getRao2LinearCoefficientsForOmega, but returns the coefficients for 2D interpolation matrix (frequency and heading)
     * @param[out] nZero    After the execution equals 1 if 0 < b[0], otherwise 0
     * @param[out] nN1      After the execution equals 1 if b[N-1] < 2pi, otherwise 0 (if headExtrapolation == 2, then it equals 1 if b[N-1] < pi)
     * @param headExtrapolation (default 0).
     *    If 0, then coeffs(f, beta) = 0 for 0 <= beta < min(headings) or max(headings) < beta <= 2 pi  ( 0 < min < max < 2pi)
     *    If 1, then the cyclic extrapolation is used for 0 <= beta < min(headings) or max(headings) < beta <= 2 pi ( 0 < min < max < 2pi)
     *    If 2, symmetric interpolation is used (0 <= min < max <= pi)
     *    Ignored if 0 <= min and/or max <= 2pi
     * @return coefficients of the shape (nbHead-1+nZero+nN1, nbFreq-1, 2, 2, nbMode)
     * The third and fourth indices forms a matrix a(2x2): f(w, b) = a_00 + a10 *w + a01 *b + a_11 *w*b
     */
    Eigen::Tensor<double, 5> getRao2LinearCoefficients(int* nZero, int* nN1, const int& headExtrapolation = 0) const;

    /**
     * getRao2LinearCoefficients with Symmetry condition.
     * This method can be used as stand along method or through getRao2LinearCoefficients with headExtrapolation == 2.
     * In order to use this method, the headings MUST satisfy the following condition 0 <= b[0] < b[1] < .. < b[N-1] <= pi.
     * @param[out] nZero    After the execution equals 1 if 0 < b[0], otherwise 0
     * @param[out] nN1      After the execution equals 1 if b[N-1] < pi, otherwise 0
     * @return coefficients of the shape (nbHead-1 +nZero +nN1, nbFreq-1, 2, 2, nbMode) for heading in [0,pi]
     */
    Eigen::Tensor<double, 5> getRao2LinearCoefficientsSymmetric(int* nZero, int* nN1) const;

    /**
     * This is for the test and debug purposes.
     *
     * Write the |RAO|^2 to a file.
     * @param[in] filename A filename where |RAO|^2 is output. File format is beta x w
     * @param[in] mode     Mode index to be output (0-based index) (by default 0)
     */
    void Rao2ToFile(const std::string& filename,
                    const int& mode = 0) const;

    /**
     * This is for the test and debug purposes.
     *
     * Write the interpolated |RAO|^2 to a file
     * @param[in] filename A filename where |RAO|^2 is output. File format is beta x w x mode
     * @param[in] coeffs2  The coefficients obtained by getRao2LinearCoefficients or other interplator
     * @param[in] nZero    if beta[0] > 0 => nZero = 1, otherwise 0 (output of getRao2LinearCoefficients)
     * @param[in] nN1      if beta[N-1] < 2\pi => nN1 = 1, otherwise 0 (output of getRao2LinearCoefficients)
     * @param[in] nB       Number of points to add for the heading interval b_i, b_i+1 (by default 3)
     * @param[in] nF       Number of points to add for the frequency interval w_j, w_j+1 (by default 3)
     * @param[in] mode     Mode index to be output (0-based index) (by default 0)
     * @param[in] bDegree  Headings interpolation order (default 1)
     * @param[in] wDegree  Frequencies interpolation order (default 1)
     * @param[in] isSym    if b[N-1] <= pi  && isSym  => spectrum is assumed to be symmetric
     */
    void Rao2ToFile(const std::string& filename,
                    const Eigen::Tensor<double, 5>& coeffs2,
                    const int& nZero,
                    const int& nN1,
                    const int& nB = 3,
                    const int& nF = 3,
                    const int& mode = 0,
                    const int& bDegree = 1,
					const int& wDegree = 1,
                    const bool& isSym = true) const;

    /**
     * Return Rao with frequencies ranges [min(Rao.w_min, wmin), max(Rao.w_max, wmax)]
     * 
     * if wmin < Rao.w_min, when Rao(wmin) == Rao(Rao.w_min)    (constant interpolation)
     * if wmax > rao.w_max, when Rao(wmax) == Rao(Rao.w_max)	(constant interpolation)
     * @param[in] wmin (by default -1.0). If wmin < 0. then wmin = Rao.w_min
     * @param[in] wmax (by default -1.0). If wmax < 0. then wmax = Rao.w_max
     *
     * @return a new Rao
     */
    Rao getFreqExtRao(const double& wmin = -1., const double& wmax = -1.) const;
} ;

}
}
