#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class Gamma
 *
 * @brief Define a general Gamma spectrum, from which Torsethaugen can be derived.
 */
class SPECTRAL_API Gamma : public ParametricSpectrum
{
public:

    Gamma(double hs, double tp, double gamma, double m, double n, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs_(hs), tp_(tp), gamma_(gamma), m_(m), n_(n)
    {
        tailOrder_ = -n;
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    // Peak parameter.
    double gamma_;

    double m_;

    double n_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    static int getNParams() { return 5; }
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp" , "gamma", "m" , "n"  }; }
    std::vector<double> getCoefs() const override   { return {  hs_ , tp_ ,  gamma_,  m_ ,  n_  }; }
    static std::vector<double> getCoefs_0()         { return {  1.,   10. ,  1.    ,  5. ,  4.  }; }
    static std::vector<double> getCoefs_min()       { return {  0.,   0.1 ,  1.    ,  1. ,  1.  }; }
    static std::vector<double> getCoefs_max()       { return {  30.,  60. ,  30.   ,  15.,  15. }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<Gamma>();
    }

};

}
}
