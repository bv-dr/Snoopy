#include "Harris.hpp"
#include "SpectralTools.hpp"

using namespace BV::Spectral ;

Eigen::ArrayXd Harris::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if ((L <= 0.) || (almost_equal(C, 0.)))
        return Eigen::ArrayXd::Zero(w.size()) ;

    Eigen::ArrayXd sw(w.size()) ;
    const double a = 4. / std::pow(L, 2. / 3.) * C
        * std::pow(meanValue, 8. / 3.) ;
    const double b = (meanValue / L) * (meanValue / L) ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi = w(i) / doublePi ;
        sw[i] = a / std::pow((fi * fi + 2. * b), 5./6.) ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
