#pragma once
#include <stdexcept>
#include <Eigen/Dense>
#include "SpectralExport.hpp"
#include "SeaState.hpp"
#include "Qtf.hpp"
#include "MQtf.hpp"
#include "Math/Integration/simpson.hpp"
#include "Spectral/ResponseSpectrum2ndABC.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class ResponseSpectrum2ndMQtf
 *
 */
class SPECTRAL_API ResponseSpectrum2ndMQtf : public ResponseSpectrum2ndABC<MQtf>
{

protected:
    // Compute the response spectrum at a given wave frequency. Set isComputed to True
    void compute() ;
public:
    ResponseSpectrum2ndMQtf(const SeaState & seaState, const MQtf & qtf) :
        ResponseSpectrum2ndABC(seaState, qtf) {}
} ;

}
}
