#include "LogNormal.hpp"

using namespace BV::Spectral;

const char* LogNormal::name = "LogNormal";

Eigen::ArrayXd LogNormal::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if (tp_ <= 0. || sigma_ <= 0.0 || hs_ <= 0.)
        throw std::logic_error("LogNormal : Incorrect parameters, Hs and Tp should be positive and sigma > 0. \nGot  " 
            + std::to_string(hs_) + " " + std::to_string(tp_) + std::to_string(sigma_) ) ;

    Eigen::ArrayXd sw(w.size());

    double m0 = hs_ * hs_ / 16.;
    double c =  m0 / ( sqrt(2 * M_PI) * sigma_);

    // #pragma omp parallel for  // Speed-up a bit, but significant overhead with n=1, so that it is not worth it...
    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i] ;
        if (wi == 0)
            continue;

        double expo = pow( ( log(wi) - mu_  ), 2) / ( 2 * pow(sigma_, 2 )) ;
        sw[i] = exp( -expo ) * c / wi;
    }
    return sw ;
}
