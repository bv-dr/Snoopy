#include "WaveTabulatedSpectrum.hpp"
#include "SeaState.hpp"
#include "ParametricSpectrum.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Math/Integration/simpson.hpp"

using namespace BV::Spectral;
using namespace BV::Math::Integration;


double SeaStateABC::compute(double w) const
{
    Eigen::ArrayXd x = Eigen::ArrayXd::Constant(1, w) ;
    Eigen::ArrayXd sw = compute(x) ;
    return sw(0) ;
}

double SeaStateABC::compute(double w, double heading) const
{
    Eigen::ArrayXd x = Eigen::ArrayXd::Constant(1, w) ;
    Eigen::ArrayXd sw = compute(x, heading) ;
    return sw(0) ;
}


Eigen::ArrayXd SeaStateABC::compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double heading) const
{
    Eigen::ArrayXd headingV = Eigen::ArrayXd::Constant(w.size(), heading);
    return compute(w, headingV);
}

SeaState::SeaState(std::vector<std::shared_ptr<WaveTabulatedSpectrum>> spectrums, double probability)
{
	probability_ = probability;
	type_ = SeaStateType::SemiParametric;
	for (auto& spectrum : spectrums)
	{
		spectrums_.push_back(std::move(spectrum));
	}
}

SeaState::SeaState(std::vector<std::shared_ptr<ParametricSpectrum>> spectrums, double probability)
{
    probability_ = probability;
    type_ = SeaStateType::Parametric;
    for (auto& spectrum : spectrums)
    {
        spectrums_.push_back(std::move(spectrum));
    }
}

SeaState::SeaState(std::shared_ptr<ParametricSpectrum> spectrum, double probability)
{
    probability_ = probability;
    type_  = SeaStateType::Parametric;
    spectrums_.push_back(std::move(spectrum));
}

SeaState::SeaState(std::shared_ptr<WaveTabulatedSpectrum> spectrum, double probability)
{
    probability_ = probability;
    type_  = SeaStateType::SemiParametric;
    spectrums_.push_back(std::move(spectrum));
}

SeaState::~SeaState()
{
}

Eigen::ArrayXd SeaState::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    Eigen::ArrayXd sw = Eigen::ArrayXd::Zero(w.size());
    for (const auto& spectrum : spectrums_)
    {
        sw += spectrum->compute(w);
    }
    return sw;
}

Eigen::ArrayXd SeaState::computeSpreading(const Eigen::ArrayXd& b) const
{
    Eigen::ArrayXd sw = Eigen::ArrayXd::Zero(b.size());
    for (const auto& spectrum : spectrums_)
    {
        // For now, simply assume that 1st parameter is Hs, TODO replace by get_moment()
        sw += spectrum->computeSpreading(b) * pow(spectrum->getHs() , 2.) / 16. ;
    }
    return sw;
}


Eigen::ArrayXd SeaState::compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double heading) const
{
    Eigen::ArrayXd sw = Eigen::ArrayXd::Zero(w.size());
    for (const auto& spectrum : spectrums_)
    {
        sw += spectrum->compute(w, heading);
    }

    return sw;
}

double SeaState::getHs() const
{
    double hs2=0;
    for (const auto& spectrum : spectrums_)
    {
        hs2 += std::pow( spectrum->getHs(),2);
    }
    return std::pow(hs2,0.5);
}


double SeaState::getTailOrder() const
{
	double tailOrder = -10;
	for (const auto& spectrum : spectrums_)
	{
		if (spectrum->getTailOrder() > tailOrder) { tailOrder = spectrum->getTailOrder(); }
	}
	return tailOrder;
}




Eigen::ArrayXd SeaState::compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const
{
    Eigen::ArrayXd sw = Eigen::ArrayXd::Zero(w.size());
    for (const auto& spectrum : spectrums_)
    {
        sw += spectrum->compute(w, headings);
    }

    return sw;
}


SeaState2D::SeaState2D( const SeaStateABC& ss , double wmin, double wmax , int nbFreq , int nbHead )
{
    probability_ = ss.getProbability();
    type_ = SeaStateType::Full;

    w_ = Eigen::ArrayXd::LinSpaced(nbFreq, wmin, wmax);
    b_ = Eigen::ArrayXd::LinSpaced(nbHead + 1, 0.0, 2.0*M_PI);
    sw_ = Eigen::ArrayXXd( nbFreq, nbHead + 1 );

    for (unsigned ib = 0; ib < b_.size(); ++ib)
    {
        sw_.col(ib) = ss.compute(  w_, b_[ib] );
    }
}


Eigen::ArrayXd SeaState2D::compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const
{
    if ( w.size() != headings.size())  throw std::invalid_argument("Frequency and Headings vector should have same size") ;


    //Ensure heading in [0, 2*M_PI[
    auto _headings = headings.unaryExpr([&](const double x) { return std::fmod(x, 2*M_PI); });

    //Allocate
    Eigen::ArrayXd sw = Eigen::ArrayXd::Constant(w.size(),-1);


    //Interpolate
    for (unsigned i = 0; i < w.size(); ++i)
    {
        sw[i] = Math::Interpolators::Linear2D::get( w_, b_, sw_, w[i], _headings[i] ) ; // TO ADD once available, BV::Math::Interpolators::ExtrapolationType::ZERO) ;
    }
    return sw;
}


Eigen::ArrayXd SeaState2D::compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double heading) const
{
    //2D Interpolation

    Eigen::ArrayXd h_ = Eigen::ArrayXd::Constant(1, std::fmod(heading, 2*M_PI) );
    Eigen::ArrayXd sw = Math::Interpolators::Linear2D::get( w_, b_, sw_, w, h_ ) ; // TO ADD once available, BV::Math::Interpolators::ExtrapolationType::ZERO) ;
    return sw;
}

Eigen::ArrayXd SeaState2D::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    Eigen::ArrayXd sw = Eigen::ArrayXd::Zero(w.size());

    // Integrate in direction, assume iso heading. Be careful, 0/360 is duplicated
    Eigen::ArrayXd sw1 = (sw_.rowwise().sum() - sw_.col( b_.size()-1 )  ) * 2. * M_PI / ( b_.size()-1 );

    return Math::Interpolators::Linear1D::get(w_, sw1, w,
                                              BV::Math::Interpolators::ExtrapolationType::ZERO) ;
}


Eigen::ArrayXd SeaState2D::computeSpreading(const Eigen::ArrayXd& b) const
{
    Eigen::ArrayXd sw_head = Eigen::ArrayXd::Zero(b_.size());

	// Integrate each at each original heading
	for (unsigned ihead = 0; ihead < b_.size(); ++ihead)
	{
		sw_head[ihead] = trapz(sw_.col(ihead), w_);
	}

	// Interpolate at the desired heading
	return Math::Interpolators::Linear1D::get(b_, sw_head, b, BV::Math::Interpolators::ExtrapolationType::ZERO);
}


Eigen::ArrayXd SeaState2D_Fourier::compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const
{
    if (w.size() != headings.size())  throw std::invalid_argument("Frequency and Headings vector should have same size");

    // Interpolate coefficients
    Eigen::ArrayXd a0 = Math::Interpolators::Linear1D::get(w_, a0_, w, BV::Math::Interpolators::ExtrapolationType::ZERO);
    Eigen::ArrayXd a1 = Math::Interpolators::Linear1D::get(w_, a1_, w, BV::Math::Interpolators::ExtrapolationType::ZERO);
    Eigen::ArrayXd a2 = Math::Interpolators::Linear1D::get(w_, a2_, w, BV::Math::Interpolators::ExtrapolationType::ZERO);
    Eigen::ArrayXd b1 = Math::Interpolators::Linear1D::get(w_, b1_, w, BV::Math::Interpolators::ExtrapolationType::ZERO);
    Eigen::ArrayXd b2 = Math::Interpolators::Linear1D::get(w_, b2_, w, BV::Math::Interpolators::ExtrapolationType::ZERO);

    // Compute spectral density
    Eigen::ArrayXd sw = Eigen::ArrayXd::Constant(w.size(), -1);
    sw = (a0 / (2 * M_PI)) * (1 + 2 * (a1 * Eigen::cos(1. * headings) + b1 * Eigen::sin(1. * headings)
                                     + a2 * Eigen::cos(2. * headings) + b2 * Eigen::sin(2. * headings)));
    return sw;
}

Eigen::ArrayXd SeaState2D_Fourier::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    return Math::Interpolators::Linear1D::get(w_, a0_, w, BV::Math::Interpolators::ExtrapolationType::ZERO);
}

Eigen::ArrayXd SeaState2D_Fourier::computeSpreading(const Eigen::ArrayXd& b) const
{
    // Compute spectral density
    Eigen::ArrayXd sw_head = Eigen::ArrayXd::Zero(b.size());

    for (unsigned ihead = 0; ihead < b.size(); ++ihead)
    {
        Eigen::ArrayXd s_ = (a0_ / (2 * M_PI)) * (1 + 2 * (a1_ * cos(1. * b[ihead]) + b1_ * sin(1. * b[ihead])
                                                         + a2_ * cos(2. * b[ihead]) + b2_ * sin(2. * b[ihead])));

        sw_head[ihead] = trapz(s_ , w_);
    }
        
    return sw_head;
}
