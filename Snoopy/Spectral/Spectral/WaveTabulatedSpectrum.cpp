#include "WaveTabulatedSpectrum.hpp"
#include "Math/Interpolators/Interpolators.hpp"

using namespace BV::Spectral;


Eigen::ArrayXd WaveTabulatedSpectrum::compute(const Eigen::Ref<const Eigen::ArrayXd>& ws) const
{
    return Math::Interpolators::Linear1D::get(w, sw, ws,
                                              BV::Math::Interpolators::ExtrapolationType::ZERO) ;
    //return SplineFunction(w, sw)(w);
}

double BV::Spectral::WaveTabulatedSpectrum::getTp() const
{
    Eigen::ArrayXd::Index itp;
    sw.maxCoeff(&itp);
    return  2*M_PI / w( itp ) ;
}


std::tuple<double, double> WaveTabulatedSpectrum::get_wrange(double energyRatio) const
{
    return std::tuple<double, double>( w.minCoeff(), w.maxCoeff());
}