#include "spectralStats.hpp"


double BV::Spectral::ampFromRisk(  double risk, double duration,  double m0, double m2   )
{
    int n = duration /  ( 2 * M_PI * sqrt( m0 / m2)) ;
    return sqrt(2. * m0 * log(-1. / (  pow(1 - risk, 1. / n ) - 1)));
}    



std::tuple<double, double> BV::Spectral::m0m2FromRsRtz(double Rs, double Rtz)
{
    return std::tuple<double, double>(pow(Rs / 4.004, 2), pow(2 * M_PI*Rs / (Rtz*4.004), 2));
}

std::tuple<double, double> BV::Spectral::RsRtzFromM0M2(double m0, double m2)
{
    return std::tuple<double, double>(4.004*pow(m0, 0.5), 2 * M_PI* pow(m0 / m2, 0.5));
}


float BV::Spectral::RtzFromM0M2(double m0, double m2)
{
    return 2 * M_PI* pow(m0 / m2, 0.5);
}
