#pragma once
#include "SpectralExport.hpp"
#include <stdexcept>
#include <Eigen/Dense>
#include "SeaState.hpp"
#include "Rao.hpp"
#include "WaveSpectrum.hpp"

#include "spdlog/spdlog.h"

namespace BV
{
namespace Spectral
{
/**
 * @class SpectrumMoments
 *
 */
class SPECTRAL_API SpectralMoments
{
protected:
    std::vector<std::shared_ptr<SeaStateABC>> seaStates_;
    // Since rao_ is modified by adding (or not) 2 additional frequencies: w_min and w_max
    // it cannot any more to be reference to the object
    const Rao rao_;
    int num_threads_ = 1;

    bool isRaoShort_;

    bool isComputed_ = false;

    /**
     * approximate frequency interval
     */
    double dw_;

    /**
     * Moment 0, which depends on seaState and mode
     */
    Eigen::ArrayXXd m0_;
    /**
     * Moment 2, which depends on seaState and mode
     */
    Eigen::ArrayXXd m2_;
    /**
     * Moment 4, which depends on seaState and mode
     */
    Eigen::ArrayXXd m4_;

    /**
     * If isM4_ is true, the 4th moment is going to be calculated, if not, only m0 and m2
     */
    bool isM4_;
    /**
     * if isM4_ == true, maxMoment_ = 4, otherwise 2.
     */
    Eigen::Index maxMoment_;
    /**
     * maxMoment_ /2
     */
    Eigen::Index maxMoment2_;

    void compute_();

    /**
     * Tolerance with which headings are considered to be equal (in degrees)
     */
    double db_;
    
    double g_;

    /**
     * For the check purpose. It consists all unique sorted headings with given head_tol_
     */
    Eigen::ArrayXd all_headings_;

public:
    SpectralMoments(const std::vector<std::shared_ptr<SeaState>>& seaStates,
                    const Rao& rao,
                    const bool& isM4 = false,
                    const double& dw = 0.005,
                    const double& db = 5,
                    const double& w_min = -1.,
                    const double& w_max = -1.,
                    const int& num_threads = 1,
                    const double& g = 9.81);

    const Eigen::ArrayXXd& getM0s()
    {
        if (!isComputed_) compute_();
        return m0_;
    }

    const Eigen::ArrayXXd& getM2s()
    {
        if (!isComputed_) compute_();
        return m2_;
    }

    const Eigen::ArrayXXd& getM4s()
    {
        if (!isComputed_) compute_();
        return m4_;
    }
};
}
}
