#include "WaveSpectrum.hpp"
#include "SpectralTools.hpp"
#include "Dispersion.hpp"
#include <iostream>
#include <cmath>
#include <array>

using namespace BV::Spectral ;

namespace {

constexpr size_t noSpreadingTypes = static_cast<size_t>(SpreadingType::Wnormal)
    + 1 ;

const std::array<std::string, noSpreadingTypes> kSpreadingNames {
    "NO", "COSN", "COS2S", "WNORMAL" } ;

}



WaveSpectrum::WaveSpectrum(std::string name, double heading, SpreadingType spreadingType, double spreadingValue)
    : Spectrum(std::move(name), heading)
    {
        switch (spreadingType)
        {
        case SpreadingType::Cosn:
            spreading = std::make_shared<Cosn>(spreadingValue, heading); break;
        case SpreadingType::Cos2s:
            spreading = std::make_shared<Cos2s>(spreadingValue, heading); break;
        case SpreadingType::Wnormal:
            spreading = std::make_shared<Wnormal>(spreadingValue, heading) ; break;
        case SpreadingType::No:
            spreading = std::make_shared<NoSpread>(heading) ; break;
        default:
            throw std::logic_error("Unknown spreading type") ;
        }
    }

std::string WaveSpectrum::getSpreadingFunctionName() const
{
    return kSpreadingNames[static_cast<size_t>(getSpreadingType())] ;
}

Eigen::ArrayXd WaveSpectrum::compute(const Eigen::Ref<const Eigen::ArrayXd>& w,
                                     double heading) const
{
    return compute(w) * computeSpreading(heading) ;
}

Eigen::ArrayXd WaveSpectrum::compute(
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const Eigen::Ref<const Eigen::ArrayXd>& headings) const
{
    auto sw = compute(w) ;
    if (w.size() != headings.size()) return sw ;

    auto spreadingVector = headings.unaryExpr([this](double angle)
    {   return computeSpreading(angle) ;}) ;
    return sw.cwiseProduct(spreadingVector) ;
}

double WaveSpectrum::computeSpreading(double angle) const
{
    return spreading->compute(angle);
}

Eigen::ArrayXd WaveSpectrum::computeSpreading(const Eigen::ArrayXd& headings) const
{
   return spreading->compute(headings);
}

double BV::Spectral::WaveSpectrum::getUr(double depth) const
{
    if (depth < 1e-3) 
    {
        return 0.;
    }
    else
    {
        double k1 = w2k(2 * M_PI / getTm(), depth);
        return getHs() / (pow(k1,2) * pow(depth, 3)); 
    }
}

std::tuple<double, double> WaveSpectrum::getForristall_alpha_beta(double depth) const
{
    double ur = getUr(depth);
    double s1 = getSt_Tm();
    double alpha;
    double beta;

    if (getSpreadingType() == SpreadingType::No)
    {
        alpha = 0.3536 + 0.2892 * s1 + 0.1060 * ur;
        beta = 2 - 2.1597 * s1 + 0.0968 * ur*ur;
    }
    else
    {
        alpha = 0.3536 + 0.2568 * s1 + 0.08 * ur;
        beta = 2 - 1.7912 * s1 - 0.5302 * ur + 0.284 * ur*ur;
    }
    return std::tuple<double, double>(alpha, beta);
}


std::tuple<double, double> WaveSpectrum::get_wrange(double energyRatio) const
{
    double wp = 2 * M_PI / getTp();

    if (energyRatio <= 0.95)
    {
        return std::tuple<double, double>(0.65 * wp, 2.3 * wp);
    }
    else if (energyRatio <= 0.99)
    {
        return std::tuple<double, double>(0.6 * wp, 3.4 * wp);
    }
    else if (energyRatio <= 0.999)
    {
        return std::tuple<double, double>(0.55 * wp, 6.0 * wp);
    }
    else
    {
        throw std::logic_error("get_wrange : ratio should be <= 0.999");
    }



}
