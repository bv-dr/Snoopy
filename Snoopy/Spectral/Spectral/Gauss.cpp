#include "Gauss.hpp"

using namespace BV::Spectral;

const char* Gauss::name = "Gauss";

Eigen::ArrayXd Gauss::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if (tp_ <= 0. || sigma_ <= 0.)
        return Eigen::ArrayXd::Zero(w.size());

    Eigen::ArrayXd sw(w.size());

    double wp = 2. * M_PI / tp_;
    double f = hs_ * hs_ / 16. * 1. / (sigma_ * sqrt(2. * M_PI));
    double g = 2. * sigma_ * sigma_;

    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i];
        if (wi > 0.)
        {
            sw[i] = f * exp(-pow(w[i] - wp, 2) / g);
        }
    }

    return sw;
}
