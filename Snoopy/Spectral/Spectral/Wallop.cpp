#include "Wallop.hpp"

using namespace BV::Spectral;

const char* Wallop::name = "Wallop";

Eigen::ArrayXd Wallop::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if (tp_ <= 0. || hs_ <= 0.)
        return Eigen::ArrayXd::Zero(w.size());

    Eigen::ArrayXd sw(w.size());
    double gamma = tgamma((m_ - 1.) / q_);
    double consB = 1. * m_ / q_;
    double consA = 1. * q_ / 16. * pow(m_ / q_, (m_ - 1.) / q_) / gamma;
    double wp = 2. * M_PI / tp_;
    double f = consA * (hs_ * hs_) / wp;

    for (auto i = 0; i < w.size(); ++i)
    {
        double wwp = w[i] / wp;
        sw[i] = f * pow(wwp, -m_) * exp(-consB * pow(wwp, -q_));
    }

    return sw;
}


double Wallop::getMoment(int i) const
{
    double m0 = hs_ * hs_ / 16.;
    if (i == 0) return m0;
    double n0 = 2*M_PI / tp_;
    return m0 * pow( m_ / q_ , i/q_) * pow(n0,i) * tgamma( (m_-i-1) / q_ ) / tgamma( (m_-1) / q_ );
}
