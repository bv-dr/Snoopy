#include "Gamma.hpp"

using namespace BV::Spectral;

const char* Gamma::name = "Gamma";

Eigen::ArrayXd Gamma::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    Eigen::ArrayXd sw(w.size());

    double e = hs_ * hs_ * tp_ / 16.;
    double g0 = pow(1. / m_ * pow(n_ / m_, (n_ - 1.) / m_) * exp(lgamma((n_ - 1.) / m_)), -1.);
    double f2 = (2.2 * pow(m_, -3.3) + 0.57) * pow(n_, 0.53 - 0.58 * pow(m_, 0.37)) + 0.94 - 1.04 * pow(m_, -1.9);
    double ag = (1. + 4.1 * pow(n_ - 2. * pow(m_, 0.28) + 5.3, 0.96 - 1.45 * pow(m_, 0.1)) * pow(log(gamma_), f2)) / gamma_;

    if (tp_ <= 0.  || gamma_ <= 0.9  || m_ < 0.01 || n_ < 1.01 || hs_ <= 0. )
        return Eigen::ArrayXd::Zero(w.size());

    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i];
        if (wi == 0.)
            continue;

        double f = wi / (2. * M_PI);
        double fn = tp_ * f;

        double sigma;
        if (fn < 1.)
            sigma = 0.07;
        else
            sigma = 0.09;

        double gs = pow(fn, -n_) * exp(-(n_ / m_) * pow(fn, -m_));
        double gf = pow(gamma_, exp(-1. / (2. * sigma * sigma) * pow(fn - 1., 2)));
        sw[i] = e * g0 * ag * gs * gf / (2. * M_PI);
    }

    return sw;
}
