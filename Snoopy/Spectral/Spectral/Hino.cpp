#include "Hino.hpp"

using namespace BV::Spectral;

Eigen::ArrayXd Hino::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{

    Eigen::ArrayXd sw(Eigen::ArrayXd::Zero(w.size())) ;
    double doublePi(2*M_PI) ;
    double sigma(std::sqrt(sigma2)) ;
    double xFactor(10.*sigma2*sigma*std::pow(zOverTen,1.-4.*alpha)/(0.0275*meanValue*std::pow(alpha*meanValue,3))) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi(w[i]/doublePi) ;
        double x(xFactor*fi) ;
        double x2(x*x) ;
        sw[i] = 0.475*x/std::pow(1.+x2,5./6.)*sigma2/fi ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
