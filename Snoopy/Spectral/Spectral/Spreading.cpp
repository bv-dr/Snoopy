#include "Spreading.hpp"
#include "SpectralTools.hpp"

#include <cmath>

#include <iostream>
#include <fstream>
using namespace BV::Spectral;

Eigen::ArrayXd Spreading::compute(const Eigen::ArrayXd& angle) const
{
    return angle.unaryExpr([this](double angle) { return compute(angle); });
}

double Spreading::getSwanDeg(int n) const
{
    Eigen::ArrayXd t = Eigen::ArrayXd::LinSpaced(n, 0, 2 * M_PI - 2 * M_PI / n);
    Eigen::ArrayXd int_ = Eigen::pow(2 * Eigen::sin((t - meanAngle_) / 2), 2) * compute(t);
    double res = (180 / M_PI) * pow(2 * M_PI * int_.sum() / n, 0.5);
    if ((360. / n > res / 20)) // Force more point for narrow case
    {
        return getSwanDeg(n * 4);
    }
    else
    {
        return res;
    }

}

double Spreading::getValue() const
{
    return value_;
}

int Spreading::getIntegralIntervals(const Eigen::Ref<Eigen::ArrayXd>& b, const double& db,
    Eigen::ArrayXd* b_int, Eigen::ArrayXd* db_i, Eigen::ArrayXi* d_i) const
{
    assert(b.size() > 0);
    assert(db > 1.0e-8);

    const int nbHead(b.size());
    int nZero(0);
    int nN1(0);
    if (b(0) > 1.e-8) nZero = 1;
    if (b(nbHead - 1) < 2 * M_PI - 1.e-8) nN1 = 1;

    const int nbHead_(nbHead + nZero + nN1 -1);

    db_i->resize(nbHead_);
    d_i->resize(nbHead_);

    Eigen::Index N_(0);
    if (nZero)      // [0, \beta_0)
    {
        (*d_i)(0) = int(b(0) / db) + 1;
        (*db_i)(0) = b(0) / (*d_i)(0);
        N_ += (*d_i)(0);
    }
    for (Eigen::Index i = 0; i < nbHead - 1; ++i)
    {
        (*d_i)(nZero +i) = int((b(i + 1) - b(i)) / db) +1;
        (*db_i)(nZero +i) = (b(i + 1) - b(i)) / (*d_i)(nZero +i);
        N_ += (*d_i)(nZero +i);
    }
    if (nN1)
    {
        (*d_i)(nbHead_ - 1) = int((2 * M_PI - b(nbHead - 1)) / db) + 1;
        (*db_i)(nbHead_ - 1) = (2 * M_PI - b(nbHead - 1)) / (*d_i)(nbHead_ - 1);
        N_ += (*d_i)(nbHead_ -1);
    }
    N_++;
    b_int->resize(N_);
    Eigen::Index ind(0);
    if (nZero)
    {
        for (Eigen::Index j = 0; j < (*d_i)(0); ++j)
        {
            (*b_int)(ind++) = j * (*db_i)(0);
        }
    }
    for (Eigen::Index i = 0; i < nbHead - 1; ++i)
    {
        for (Eigen::Index j = 0; j < (*d_i)(nZero +i); ++j)
        {
            (*b_int)(ind++) = b[i] + j * (*db_i)(nZero +i);
        }
    }
    if (nN1)
    {
        for (Eigen::Index j = 0; j < (*d_i)(nbHead_ -1); ++j)
        {
            (*b_int)(ind++) = b[nbHead -1] + j * (*db_i)(nbHead_ -1);
        }
    }
    (*b_int)(ind) = 2 * M_PI;
    spdlog::debug("Division intervals by subintervals for spreading integration");
    spdlog::debug("nZero / nN1 = {} / {}", nZero, nN1);
    if (nZero)
    {
        spdlog::debug("  0.000, {:>7.3} --> {} subinterval(s) of length {}", b[0], (*d_i)(0), (*db_i)(0));
    }
    for (Eigen::Index i = 0; i < nbHead -1; ++i)
    {
        spdlog::debug("{:>7.3}, {:>7.3} --> {} subinterval(s) of length {}", b[i], b[i+1], (*d_i)(nZero +i), (*db_i)(nZero +i));
    }
    if (nN1)
    {
        // nbHead_ -1 == nZero +nbHead +nN1 -2
        spdlog::debug("{:>7.3}, {:>7.3} --> {} subinterval(s) of length {}", b[nbHead-1], 2*M_PI, (*d_i)(nZero +nbHead -1), (*db_i)(nZero +nbHead -1));
    }
    return nbHead_;
}

Eigen::Tensor<double, 3> Spreading::integrate_spreading(const Eigen::Ref<Eigen::ArrayXd>& b, const int& k, const int& n, const double& db, const bool& bSym) const
{
    Eigen::ArrayXd b_int;                       // New integration points
    Eigen::ArrayXd db_i;                        // db_i on the interval [b_i, b_{i+1})
    Eigen::ArrayXi d_i;                         // Number of point per each interval
    const int nbHead_(getIntegralIntervals(b, db+1.e-8, &b_int, &db_i, &d_i));
    Eigen::Tensor<double, 3> integrals(nbHead_, k+1, n+1);
    integrals.setZero();
    Eigen::ArrayXd gb(compute(b_int));      // G(b_int, beta_mean)

    // Integration, on each [b_i, b_{i+1}] interval we use trapezoid method
    bool isFirstStep(true);
    bool isSymmetric(false);
    Eigen::Index ind1(0);
    Eigen::ArrayXXd f1(k + 1, n+1);
    double beta(0.0);
    for (Eigen::Index i = 0; i < nbHead_; ++i)
    {
        if (bSym && std::abs(b_int[ind1] -M_PI) < 1.e-8)    // in a case Symmetric Rao case, if beta passes pi, we should integrate (2pi-beta)^n G(beta)
        {
            isFirstStep = true;
            isSymmetric = true;         // Starting calculation of the symmetric part
        }
        if (isFirstStep)
        {
            isFirstStep = false;
            if (isSymmetric)
            {
                beta = 2*M_PI -b_int[ind1];
            }
            else
            {
                beta = b_int[ind1];
            }
            //std::cout << "isFirstStep = true\n";
            //std::cout << "isSymmetric = " << (isSymmetric ? "true" : "false") << "\n";
            // first node
            f1(0, 0) = gb[ind1] *0.5;                   // b^0 cos^0(b) G[ind1] *0.5
            for (Eigen::Index ik = 1; ik < k + 1; ++ik)
            {
                f1(ik, 0) = f1(ik-1, 0) * beta;    // b^k cos^0(b) G[ind1] *0.5
            }
            double cosb(std::cos(b_int[ind1]));     // cos(2pi -b) == cos(b)
            for (Eigen::Index in = 1; in < n + 1; ++in)
            {
                f1.col(in) = f1.col(in - 1) * cosb;       // b^k cos^n(b) G[ind1] * 0.5
            }
            // integrals
        }
        for (Eigen::Index j = 0; j < d_i(i); j++)
        {
            // second node
            Eigen::Index ind2(ind1 + 1);
            Eigen::ArrayXXd f2(k + 1, n +1);
            if (isSymmetric)
            {
                beta = 2*M_PI -b_int[ind2];
            }
            else
            {
                beta = b_int[ind2];
            }
            f2(0, 0) = gb[ind2] *0.5;                   // b^0 cos^0(b) G[ind2] *0.5
            for (Eigen::Index ik = 1; ik < k + 1; ++ik)
            {
                f2(ik, 0) = f2(ik-1, 0) * beta;    // b^k cos^0(b) G[ind2] *0.5
            }
            double cosb(std::cos(b_int[ind2]));         // cos(2pi -b) == cos(b)
            for (Eigen::Index in = 1; in < n + 1; ++in)
            {
                f2.col(in) = f2.col(in - 1) * cosb;       // b^k cos^n(b) G[ind2] *0.5
            }
            for (Eigen::Index ik = 0; ik < k + 1; ik++)
            {
                for (Eigen::Index in = 0; in < n + 1; in++)
                {
                    integrals(i, ik, in) += db_i(i) * (f1(ik, in) + f2(ik, in));
                }
            }
            // for the next iteration, the first node is the second node of this iteration
            ind1 = ind2;    // point index
            f1 = f2;        // function's value at this point
        }
    }
//#define OUTPUT_INTEGRALS
#ifdef OUTPUT_INTEGRALS
    std::ofstream out_beta("spreading_beta.dat");
    for (Eigen::Index i = 0; i < b.size(); i++)
    {
        out_beta << b[i] << "\t" << b[i] << "\n";
    }
    out_beta.close();
    std::ofstream out_beta_int("spreading_beta_int.dat");
    for (Eigen::Index i = 0; i < b_int.size(); i++)
    {
        out_beta_int << b_int[i] << "\t" << b_int[i] << "\n";
    }
    out_beta_int.close();
    std::ofstream out("spreading_ints.dat");
    // I(i, k, n) \equiv int_{\beta_i}^{\beta_{i+1}} G(\beta) \beta^k \cos^n \beta d\beta
    // format: i, beta_i, I(i, 0, 0), I(i, 1, 0), ..., I(i, k, 0), I(i, 0, 1), I(i, 1, 1) , ..., I(i, k, 1), I(i, 0, 2), ..., I(i, k, n)
    int nZero(0);
    for (Eigen::Index i = 0; i < nbHead_; i++)
    {
        if (i == 0 && 1.e-8 < b[i])
        {
            nZero = 1;
            out << "0\t0";
        }
        else
        {
            out << i +nZero << "\t" << b[i -nZero];
        }
        for (Eigen::Index in = 0; in < n + 1; in++)
        {
            for (Eigen::Index ik = 0; ik < k + 1; ik++)
            {
                out << "\t" << integrals(i, ik, in);
            }
        }
        out << "\n";
    }
    out.close();
#endif
    return integrals;
}

//-----------------------  Cos n
Cosn::Cosn(double n, double meanAngle)
{
    value_ = n;
    meanAngle_ = meanAngle;
    coef_ = exp(lgamma(value_ / 2. + 1.) - lgamma(value_ / 2. + 0.5)) / sqrt(M_PI);
}

double Cosn::compute(double angle) const
{
    if (cos(meanAngle_ - angle) < 0.) return 0.;
    return coef_ * pow(abs(cos(angle - meanAngle_)), value_);
}

double Cosn::coef_to_std(double n)
{
    return pow(1. / (n + 1.), 0.5);
}

double Cosn::std_to_coef(double std)
{
    return (1. / pow(std, 2)) - 1.;
}

double Cosn::getStd() const
{
    return Cosn::coef_to_std(value_);
}

//-----------------------  Cos 2s
Cos2s::Cos2s(double s, double meanAngle)
{
    value_ = s;
    meanAngle_ = meanAngle;
    coef_ = exp(lgamma(value_ + 1.0) - lgamma(value_ + 0.5)) / (2.0 * sqrt(M_PI));
}

double Cos2s::compute(double angle) const
{
    return coef_ * pow(abs(cos((angle - meanAngle_) / 2.)), 2. * value_);
}

double Cos2s::coef_to_std(double s)
{
    return pow(4. / (2 * s + 1.), 0.5);
}

double Cos2s::std_to_coef(double std)
{
    return 2. / pow(std, 2) - 0.5;
}

double Cos2s::getStd() const
{
    return Cos2s::coef_to_std(value_);
}

//-----------------------  Wrapped normal
Wnormal::Wnormal(double sigma, double meanAngle, int k_max)
{
    value_ = sigma;
    meanAngle_ = meanAngle;
    k_max_ = k_max;
    coef_ = 1. / (sigma * pow(2. * M_PI, 0.5));
}

double Wnormal::compute(double angle) const
{
    double sum = 0.;
    for (int i = -k_max_; i <= k_max_; ++i)
    {
        sum += exp(-pow(angle - meanAngle_ - 2. * i * M_PI, 2.)
            / (2. * value_ * value_));
    };
    return coef_ * sum;
}

double Wnormal::getStd() const
{
    return value_;
}

//-----------------------  No spreading
NoSpread::NoSpread(double meanAngle)
{
    meanAngle_ = meanAngle;
    value_ = 0.;
}

double NoSpread::compute(double angle) const
{
    if (almost_equal(meanAngle_, angle))
    {
        return 1.;
    }
    else
    {
        return 0.;
    };
}

double NoSpread::getStd() const
{
    return 0.;
}
