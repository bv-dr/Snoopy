#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class ModifiedHarris
 *
 * @brief Define a ModifiedHarris wind spectrum.
 */
class SPECTRAL_API ModifiedHarris: public Spectrum
{
public:

    ModifiedHarris(double meanVelocity, double C, double L, double heading=0.) :
        Spectrum("ModifiedHarris", heading, meanVelocity), C(C), L(L),
        ustar2(C * std::pow(meanVelocity, 2))
    {
    }

    double C ;
    double L ;
    double ustar2 ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 3 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "representativeLengthScale"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C, L} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.002, 1800.} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1e5} ;}
} ;

}
}
