#pragma once

#include "SpectralExport.hpp"

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "spdlog/spdlog.h"

namespace BV {
namespace Spectral {

/**
 * The available spreading types.
 */
enum class SpreadingType
{
    No = 0, Cosn, Cos2s, Wnormal
};

/**
 * @class Spreading
 *
 * @brief Define a wave spreading
 */
class SPECTRAL_API Spreading
{
public:

    virtual ~Spreading() = default;

    virtual double compute(double angle) const = 0;

    Eigen::ArrayXd compute(const Eigen::ArrayXd& angle) const;

    // Return standard deviation
    virtual double getStd() const = 0;

    // Return swan heading, in degree
    virtual double getSwanDeg(int n = 200) const;

    SpreadingType getType() const { return getTypeInternal(); }

    double getValue() const;

    /**
     * Evaluate the integrals:
     * \f$J_(\gamma, i, j) =\int_{\beta_\gamma}^{\beta_{\gamma+1}} \beta^i \cos^j \beta G(\beta, \overline{\beta}) d\beta\f$
     * for i = 0, 1, 2, ... k
     * for j = 0, 1, 2, ... n,
     * @param b Sorted array of headings \f$0 <= beta_0 < ... < beta_{N-1} <= 2\pi\f$
     * @param k the order of the RAO approximation with respect to the headings multiplied by 2. \f$RAO(\omega, \beta)=\sum_{i=0}^{m/2}\sum_{j=0}^{k/2}a_{ij}\omega^i\beta^j\f$
     * @param n the maximum spectral moment to be calculated.
     * @param db approximate heading step. Number of points on the interval \f$[\beta_\gamma,\beta_{\gamma+1})\f$ is calculated, so that the integration interval is close to db.
     * @param bSym For the pi-symmetrical case, we need to integrate (2\pi -\beta)^n G(\beta) starting from b_{nSym} == pi
     * @return Eigen::Tensor<double,3> where the first index corresponds to the intervals \f$\gamma\f$, the second one: i = 0, 1, 2, ... k, and the third one: j = 0, 1, 2, ... n.
     *
     * NOTE: that that if \f$0 < \beta_0\f$ and/or \f$\beta_{N-1} < 2\pi\f$, then additional intervals will be added: \f$[0,\beta_0)\f$ and/or \f$[\beta_{N-1}, 2\pi)\f$, respectively.
     */
    virtual Eigen::Tensor<double, 3> integrate_spreading(const Eigen::Ref<Eigen::ArrayXd>& b,
                                                         const int& k,
                                                         const int& n,
                                                         const double& db = 0.1,
                                                         const bool& bSym = false) const;

protected:

    // Mean angle
    double meanAngle_;

    //spreading value
    double value_;

    virtual SpreadingType getTypeInternal() const = 0;

    /**
     * Calculate the integration points for the array of headings b
     * @param b Array of headings sorted in the interval from 0 to 2pi
     * @param db Desired integration interval on each heading interval
     * @param[out] b_int Array of new beta points in [0, 2pi]
     * @param[out] db_i Array of lengths between b_int[k] and b_int[k+1] for b_int[k] and b_int[k+1] in [b_i, b_{i+1}]
     * @param[out] d_i Array with number of subintervals on the interval [b_i, b_{i+1}]
     * @return number of total intervals (including the additional intervals: [0,b_0), [b_{N-1}, 2pi) if necessary)
     */
    virtual int getIntegralIntervals(const Eigen::Ref<Eigen::ArrayXd>& b,
                                     const double& db,
                                     Eigen::ArrayXd* b_int,
                                     Eigen::ArrayXd* db_i,
                                     Eigen::ArrayXi* d_i) const;
};

/**
 * @class DerivedSpreading
 *
 * @brief Intermediate templated class used to override
 * getTypeInternal and to implement a static version of
 * getType.
 */
template<SpreadingType Type>
class DerivedSpreading : public Spreading
{
public:
    static SpreadingType getType() { return Type; }

protected:

    SpreadingType getTypeInternal() const override { return Type; }

};

class SPECTRAL_API Cosn : public DerivedSpreading<SpreadingType::Cosn>
{
public:

    Cosn(double n, double meanAngle);
    double compute(double angle) const override;
    double coef_;
    double getStd() const override;
    static double coef_to_std(double n);
    static double std_to_coef(double std);
    static double getMaxCoef() { return 1e5; }
    static double getMinCoef() { return 0.0; }
};

class SPECTRAL_API Cos2s : public DerivedSpreading<SpreadingType::Cos2s>
{
public:

    Cos2s(double s, double meanAngle);
    double compute(double angle) const override;
    double coef_;
    double getStd() const override;
    static double coef_to_std(double s);
    static double std_to_coef(double std);
    static double getMaxCoef() { return 1e5; }
    static double getMinCoef() { return 0.; }
};

class SPECTRAL_API Wnormal : public DerivedSpreading<SpreadingType::Wnormal>
{
public:

    Wnormal(double sigma, double meanAngle, int k_max = 2);
    double compute(double angle) const override;
    double coef_;
    int k_max_;
    double getStd() const override;
    static double coef_to_std(double std) { return std; }  // To get similar behaviour compared to Cosn / Cos2s
    static double std_to_coef(double std) { return std; }
    static double getMaxCoef() { return 2 * M_PI; }
    static double getMinCoef() { return 0.0001; }
};

class SPECTRAL_API NoSpread : public DerivedSpreading<SpreadingType::No>
{
public:

    explicit NoSpread(double meanAngle);
    double compute(double angle) const override;
    double getStd() const override;
    static double getMaxCoef() { return 0.0; }
    static double getMinCoef() { return 0.0; }
};

}
}
