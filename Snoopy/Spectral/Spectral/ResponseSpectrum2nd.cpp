#include "ResponseSpectrum2nd.hpp"

#include <iostream>


// TODO : implement everything !s

using namespace BV::Spectral;

double ResponseSpectrum2nd::getMean() const
{
    //const Eigen::Index iMode(0); // For now assume only one QTF

    auto realPart = qtf_.getReal().toWDWTensor();

    double meanValue = 0.;
    for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
        //Get closest heading
        int ihead;
        Eigen::abs((headings_ - spectrum.heading)).minCoeff(&ihead);
        Eigen::ArrayXd sw(spectrum.compute(freqs_));

        // Frequency integration
        for (Eigen::Index iw = 0; iw < freqs_.size(); ++iw)
        {
            meanValue += sw(iw) * realPart(ihead, iw, 0, 0);
        }

    }
    meanValue *= 2 * dw_;
    return meanValue;
}

/*
 * Protected method getNewmanUD_: Newman approximation for unidirection case
 * parameters:
 *      dQtf : diagonal elements of Qtf
 *      specFreq: Spectrum at frequencies(w)
 *      meanType: 0: geometric mean
 *                1: arithmetic mean
 */
Eigen::ArrayXd ResponseSpectrum2nd::getNewmanUD_(const Eigen::ArrayXd &dQtf, const Eigen::ArrayXd &specFreqs, const int meanType)
{
    const Eigen::Index nWs = freqs_.size();
    Eigen::ArrayXd resp(nWs_);

    if (mode_ == QtfMode::SUM)
    {   // Sum mode
        throw std::logic_error("ResponseSpectrum2nd: There is no Newman approximation for the sum mode!");
    }
    else
    {   // Diff mode
        for(Eigen::Index iw = 0; iw < nWs_; ++iw)   // dw: w2 = w1 +dw
        {
            resp(iw) = 0.0;
            for(Eigen::Index iw1 = 0; iw1 < nWs; ++iw1) // w1
            {
                Eigen::Index iw2 = iw1 +iw;             // w2
                if (iw2 < nWs)
                {
                    double qtf2(0.0);
                    double qtf1(0.0);
                    switch(meanType)
                    {
                    case 1:
                        qtf1 = 0.5*(dQtf(iw1) +dQtf(iw2));
                        qtf2 = qtf1 *qtf1;
                        break;
                    default:
                        qtf2 = dQtf(iw1) *dQtf(iw2);
                    }
                    resp(iw) += specFreqs(iw1) *specFreqs(iw2) *qtf2;
                }
            }
            resp(iw) *= 8.0 *dw_;
        }
    }
    return resp;
}

/*
 * Newman approximation for Qtf (unidirectional)
 * dQtf: diagonal elements for the Qtf for all headings, i.e. dQtf(head, freq)
 */
Eigen::ArrayXd ResponseSpectrum2nd::getNewman(const Eigen::ArrayXXd &dQtf, const int meanType)
{
    Eigen::ArrayXd resp(nWs_);
    resp = 0.0;
    for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
        Eigen::ArrayXd spectrumFreqs(spectrum.compute(freqs_)) ;

        if (spectrum.getSpreadingType() == SpreadingType::No)
        {
            // Get closest heading
            Eigen::ArrayXd::Index ihead ;
//                const auto headings = qtf_->getHeadings();
            // Check, that spectrum.heading is in the range qtf_.getHeadings() and print the warning otherwise
            if ((spectrum.heading < headings_[0]) || (spectrum.heading > headings_[nHs_-1]))
                std::cout << "Warning: the spectrum's heading is outside the headings range\n"
                          << "         spectrum's heading : " << spectrum.heading << " rad\n"
                          << "         min(headings)      : " << headings_[0] << " rad\n"
                          << "         max(headings)      : " << headings_[nHs_-1] << " rad\n";
            //double headDiff =
            Eigen::abs((  headings_ - spectrum.heading  )).minCoeff ( &ihead );

            //Compute response spectra
            resp += getNewmanUD_(dQtf.matrix().row(ihead), spectrumFreqs, meanType);
        }
        else
        {
            throw std::logic_error("ResponseSpectrum2nd with spreading not yet implemented");
        }
    }
    return resp;
}

Eigen::ArrayXd ResponseSpectrum2nd::getNewman(const int meanType)
{
    const Eigen::Index iMode(0) ;
    const Eigen::Tensor<double, 4> & modules(qtf_.getModules().toWDWTensor());
    const Eigen::Index nWs = modules.dimension(1);  // frequencies
    const Eigen::Index nHs = modules.dimension(0);  // headings

    Eigen::ArrayXXd dQtf(nHs, nWs);
    for(Eigen::Index ih = 0; ih < nHs; ++ih)
        for (Eigen::Index iw = 0; iw < nWs; ++iw)
            dQtf(ih, iw) = modules(ih, iw, 0, iMode);
    return getNewman(dQtf, meanType);
}

// Compute response spectrum at all rao frequency.
// In wave frequency
/*
 * Response spectra (6.68):
 *  S_{F^{(2)}_{+k}}(\Omega) = 8 \int_{0}^{\Omega/2} S(\omega)S(\Omega-\omega) ||f^{(2)}_{+k}(\omega,\Omega-\omega,\beta)||^2 d\omega
 * Let \Omega = w_p = p dw, then I_{\Omega_p/2} = {w_q : w_q <= \Omega_p/2}
 *  S = 8 \sum_{w_q \in I_{\Omega_p/2}} S(w_q) S(w_p -w_q) ||f(w_q, w_p -w_q)||^2 dw
 *    = 8 \sum_{w_q \in I_{\Omega_p/2}} S(q dw) S((p-q) dw) ||f(q dw, (p-q) dw||^2 dw
 *
 * \Omega = 2w_min, 2w_min +dw_, 2w_min +2dw_, ..., 2w_max
 */
void ResponseSpectrum2nd::compute()
{
    // FIXME all sea state spectra should have the same frequencies.

    const Eigen::Index iMode(0) ; // FIXME set in arguments???

    // rao_.getModules() from ResponseSpectrum returns const Eigen::Tensor<double, 3>
    // but qtf_.getModules() returns const QtfTensorModule
    // thus, to get const Eigen::Tensor<double, 4> from qtf_ we should get QtfTensor and then convert to Tensor<double, 4>
    const Eigen::Tensor<double, 4> & modules(qtf_.getModules().toWDWTensor());

    for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
        Eigen::ArrayXd spectrumFreqs(spectrum.compute(freqs_)) ;

        if (spectrum.getSpreadingType() == SpreadingType::No)
        {
            // Get closest heading
            Eigen::ArrayXd::Index ihead ;
//                const auto headings = qtf_->getHeadings();
            // Check, that spectrum.heading is in the range qtf_.getHeadings() and print the warning otherwise
            if ((spectrum.heading < headings_[0]) || (spectrum.heading > headings_[nHs_-1]))
                std::cout << "Warning: the spectrum's heading is outside the headings range\n"
                          << "         spectrum's heading : " << spectrum.heading << " rad\n"
                          << "         min(headings)      : " << headings_[0] << " rad\n"
                          << "         max(headings)      : " << headings_[nHs_-1] << " rad\n";
            //double headDiff =
            Eigen::abs((  headings_ - spectrum.heading  )).minCoeff ( &ihead );

            //Compute response spectra
            Eigen::Index nDifs = modules.dimension(2);

            const Eigen::Index nWs = freqs_.size();

            if (mode_ == QtfMode::SUM)
            {
//                std::cout << "sum mode \n";
                for (Eigen::Index iw=0; iw<nWs_; ++iw)  // \Omega = W_ frequencies range
                {
                    // getting n = max{l : w_l <= W_{iw}/2}
                    Eigen::Index in = 0;
                    for (; in < nWs; ++in)
                        if (2.0*freqs_(in) -W_(iw) >= 0.0)   // Exclude \omega = \Omega/2 from the possible points
                            break;
#if 0
                    std::cout << "w range for W = " << W_(iw) << " (W/2 = " << W_(iw)/2. << "): {";
                    for(Eigen::Index iw1 = 0; iw1 < in; ++iw1)
                        std::cout << freqs(iw1) << ", ";
                    std::cout << "}\n";
#endif
                    rSpec_(iw) = 0.0;
                    for (Eigen::Index iw1 = 0; iw1 < in-1; ++iw1)
                    {
                        Eigen::Index iw2 = iw -iw1;
                        Eigen::Index idw = iw -iw1 -iw1;
#if 0
                        double W = W_(iw);
                        double w1 = freqs(iw1);
                        double w2 = freqs(0) +iw2 *dw_;
                        std::cout << "W = " << W << ", w1 = " << w1 << ", w2 = " << w2 << " w1 +w2 = " << w1 +w2 << "("<< W << ")"
                                  << ", w2 -w1 = " << w2 -w1 << "(" << idw*dw_ << ")" << std::endl;
#endif
                        if (idw < nDifs)
                        {
                            if (iw2 < nWs)
                            {
                                double S1 = spectrumFreqs(iw1);
                                double S2 = spectrumFreqs(iw2);
//                                rSpec_(iw) += S1 *S2 *std::pow(modules(ihead, iw1, idw, iMode), 2.0) *(freqs(iw1+1) -freqs(iw1));
                                rSpec_(iw) += S1 *S2 *std::pow(modules(ihead, iw1, idw, iMode), 2.0) *dw_;
                            }
                        }
                    }
                    {
                        Eigen::Index iw1 = in-1;
                        Eigen::Index iw2 = iw -iw1;
                        Eigen::Index idw = iw -iw1 -iw1;
                        if (idw < nDifs)
                        {
                            if (iw2 < nWs)
                            {
                                double S1 = spectrumFreqs(iw1);
                                double S2 = spectrumFreqs(iw2);
                                rSpec_(iw) += S1 *S2 *std::pow(modules(ihead, iw1, idw, iMode), 2.0) *(W_(iw)/2. -freqs_(iw1));
                            }
                        }
                    }
                    rSpec_(iw) *= 8.0;
                }
            } else
            {
//                std::cout << "dif mode\n";
                for(Eigen::Index iw = 0; iw < nWs_; ++iw)
                {
                    rSpec_(iw) = 0.0;
                    for(Eigen::Index iw1 = 0; iw1 < nWs; ++iw1)
                    {
                        Eigen::Index iw2 = iw1 +iw;
                        if (iw2 < nWs)
                        {
#if 0
                            double W = W_(iw);
                            double w1 = freqs(iw1);
                            //double w2 = freqs(0) +iw *dw_;
                            double w2 = freqs(iw2);
                            std::cout << "W = " << W << ", iw = " << iw
                                      << ", w1 = " << w1 << ", iw1 = " << iw1
                                      << ", w2 = " << w2 << ", iw2 = " << iw2
                                      << ", w2 -w1 = " << w2 -w1 << "(" << iw *dw_ << ")" << std::endl;
#endif
                            double S1 = spectrumFreqs(iw1);
                            double S2 = spectrumFreqs(iw2);
                            rSpec_(iw) += S1 *S2 *std::pow(modules(ihead, iw1, iw, iMode), 2.0);
                        }
                    }
                    rSpec_(iw) *= 8.0 *dw_;
                }
            }
//            std::cout << rSpec_ << std::endl;
            //Compute moments
            Eigen::ArrayXd rM2Core(rSpec_ *W_ *W_);
            m0_ = BV::Math::Integration::trapz(rSpec_, dw_);
            m2_ = BV::Math::Integration::trapz(rM2Core, dw_);
        }
        else
        {
            throw std::logic_error("ResponseSpectrum2nd with spreading not yet implemented");
        }
    }
    isComputed_ = true ;
}
