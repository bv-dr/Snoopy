#pragma once

#include "WaveSpectrum.hpp"

#include "SpectralExport.h"

namespace BV
{
namespace Spectral
{

/**
 * @class WindSpectrum
 *
 * @brief Define the base class of all wind spectrums.
 */
class SPECTRAL_API WindSpectrum : public Spectrum
{
protected:
    double meanVelocity_ ;
public:
    WindSpectrum(std::string name, double heading, double meanVelocity)
        : Spectrum(std::move(name), heading, SpreadingType::No, 0.), meanVelocity_(meanVelocity)
    {
    }

    double getMeanValue() const
    {
        return meanVelocity_ ;
    }
};

}
}
