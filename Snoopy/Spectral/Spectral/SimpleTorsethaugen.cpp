#include "SimpleTorsethaugen.hpp"
#include "Gamma.hpp"

using namespace BV::Spectral;

const char* SimpleTorsethaugen::name = "Simple Torsethaugen";

Eigen::ArrayXd SimpleTorsethaugen::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    double tf = 6.6 * cbrt(hs_);
    double m = 4.;
    double n = 4.;

    double hs1, gamma1, hs2, tp2;
    double tp1 = tp_;
    double gamma2 = 1.;

    if (tp_ <= tf)
    {
        // Wind dominated
        double rpw = 0.7 + 0.3 * exp(-pow(2 * (tf - tp_) / (tf - 2 * sqrt(hs_)), 2));
        hs1 = rpw * hs_;
        gamma1 = 35. * pow(2. * M_PI * hs1 / (9.81 * tp_ * tp_), 0.857);

        hs2 = sqrt(1 - rpw * rpw) * hs_;
        tp2 = tf + 2.0;
    }
    else
    {
        double rps = 0.6 + 0.4 * exp(-pow((tp_ - tf) / (0.3 * (25. - tf)), 2));
        hs1 = rps * hs_;
        gamma1 = 35 * pow(2. * M_PI * hs_ / (9.81 * tf * tf), 0.857) * (1. + 6. * (tp_ - tf) / (25. - tf));

        hs2 = sqrt(1 - rps * rps) * hs_;
        tp2 = 6.6 * cbrt(hs2);
    }

    Eigen::ArrayXd sw1 = Gamma(hs1, tp1, gamma1, m, n, heading).compute(w);
    Eigen::ArrayXd sw2 = Gamma(hs2, tp2, gamma2, m, n, heading).compute(w);
    return sw1 + sw2;
}
