#include "Spectral/MQtf.hpp"
#include "Spectral/Qtf.hpp" // To define MQtf(Qtf)

using namespace BV::Spectral ;

MQtf::MQtf (const Qtf &qtf) :
    MQtf(qtf.getHeadings(),             // Eigen::ArrayXd   : b
         qtf.getFrequencies(),          // Eigen::ArrayXd   : w
         qtf.getDeltaFrequencies(),     // Eigen::ArrayXd   : dw
         qtf.getMQtfLikeComplexData(),  // Eigen::Tensor<std::complex<double>, 4> const & t
         qtf.getWaveReferencePoint(),   // Eigen::Vector2d  : waveRefPoint
         qtf.getReferencePoint(),       // Eigen::Vector3d  : refPoint
         qtf.getMode(),                 // QtfMode          : "sum" or "diff" mode
         qtf.getDepth(),                // double           : depth
         qtf.getForwardSpeed(),         // double           : forwardSpeed
         "",                            // std::string      : filename
         1.0,                           // double           : refLength
         1025.0,                        // double           : rho
         9.81                           // double           : g
         )
{
}

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}
static double frexp10(double arg, int * exp)
{
   *exp = (arg == 0) ? 0 : 1 + (int)std::floor(std::log10(std::fabs(arg) ) );
   return arg * std::pow(10 , -(*exp));
}
static double frexp10_s(double arg, int *exp) {
    double r = frexp10(arg, exp);
    if (r == 0.) return r;
    (*exp) --;
    return r*10.;
}

std::ostream & BV::Spectral::operator << (std::ostream & out, const MQtf& qtf)
{
    out << "Filename              : " << qtf.filename_ << "\n";
    out << "Number of headings    : " << qtf.b_.size() << "\n";
    out << "Number of frequencies : " << qtf.w_.size() << "\n";
    out << "Number of differences : " << qtf.dw_.size() << "\n";
    out << "Qtf mode              : ";
    if (qtf.isModeSum()) out << "Sum\n";
    else if (qtf.isModeDiff()) out << "Diff\n";
    else out << "Unknown\n";
    out << "Qtf type              : ";
    if (qtf.isQtf()) out << "Qtf\n";
    else if (qtf.isMQtf()) out << "MQtf\n";
    else out << "Unknown\n";
    out << "Forward speed         : " << qtf.forwardSpeed_ << "\n";
    out << "Reference length      : " << qtf.refLength_ << "\n";
    out << "Water density (rho)   : " << qtf.rho_ << "\n";
    out << "Gravity acceleration  : " << qtf.g_ << "\n";
    out << "Waterdepth            : " << qtf.depth_ << "\n";
    out << "Wave Reference Point  : (" << qtf.waveRefPoint_(0) << ", " << qtf.waveRefPoint_(1) << ")\n";
    out << "Reference Point       : (" << qtf.refPoint_(0) << ", " << qtf.refPoint_(1) << ", " << qtf.refPoint_(2) << ")";
    return out;
}

std::ofstream & BV::Spectral::operator << (std::ofstream & out, const MQtf& qtf) {
    out << "# Project : \n";
    out << "# User    : \n";
    out << "# File : " << qtf.filename_ << "\n";
    out << "# Constants used in computations :\n";
    out << std::fixed << std::setprecision(4);
    out << "#     Reference length     :  " << std::setw(9) << std::setprecision(4) << qtf.refLength_ << "\n";
    out << "#     Water density (rho)  :  " << std::setw(9) << std::setprecision(4) << qtf.rho_ << "\n";
    out << "#     Gravity acceleration :  " << std::setw(9) << std::setprecision(4) << qtf.g_ << "\n";
    if (qtf.depth_ <= 0.0) {
        out << "#     Waterdepth           :  Inf.\n";
    } else {
        out << "#     Waterdepth           :  " << std::setw(9) << std::setprecision(4) << qtf.depth_ << "\n";
    }
    out << "#     Ref.pt incident wave : (   " << std::setw(10) << qtf.waveRefPoint_(0) << std::setw(10) << qtf.waveRefPoint_(1) <<")\n";
    out << "#            Forward speed :   " << qtf.forwardSpeed_ << "  m/s   \n";
    out << "#\n";
    out << "#------------------------------------------------------------------------\n";
    out << "#QTFTYPE \"2NDLOAD\"\n";
    out << "#CPLXTYPE 0\n";
    out << "#QTFMODE ";
    if (qtf.isModeSum()) out << "Sum\n";
    else if (qtf.isModeDiff()) out << "Diff\n";
    else out << "Unknown\n";
    out << "#DIRECTION  \"           1\n";
    out << std::setprecision(3);
    out << "#COORD   " << std::setw(9) << qtf.refPoint_(0) << std::setw(9) << qtf.refPoint_(1) << std::setw(9) << qtf.refPoint_(2) << "\n";
    out << "#NBHEADING   " << qtf.b_.size() << "\n";
    out << "#HEADING";
    out << std::setprecision(2);
    for(decltype(qtf.b_.size()) i = 0; i < qtf.b_.size(); ++i)
        out << std::setw(12) << qtf.b_[i]*180.0/M_PI;
    out << "\n";
    out << "#DIFF";
    out << std::setprecision(5);
    for(decltype(qtf.dw_.size()) i = 0; i < qtf.dw_.size(); ++i)
        out << std::setw(24) << qtf.dw_[i];
    out << "\n";
    out << "#---w(r/s)-----------------------------------------------------\n";
    out << std::fixed;
    for(decltype(qtf.b_.size()) h1 = 0; h1 < qtf.b_.size(); ++h1) {
        if (qtf.qtfType_ == QtfType::QTF) {
            out << std::setprecision(2) << "#Heading = " << std::setw(7) << qtf.b_[h1]*180./M_PI << "\n";
            int exp;
            for(decltype(qtf.w_.size()) f = 0; f < qtf.w_.size(); ++f) {
                out << std::setw(8) << std::setprecision(4) << frexp10_s(qtf.w_[f], &exp) << "E"
                    << std::showpos << std::setw(4) << std::setprecision(0) << std::setfill('0') << std::internal << exp
                    << std::setfill(' ') << std::noshowpos << std::right;
                for(decltype(qtf.dw_.size()) d = 0; d < qtf.dw_.size(); ++d) {
                    out << std::setw(8) << std::setprecision(4) << frexp10_s(qtf.t_(h1, f, d, 0).real(), &exp) << "E"
                        << std::showpos << std::setw(4) << std::setprecision(0) << std::setfill('0') << std::internal << exp
                        << std::setfill(' ') << std::noshowpos << std::right;
                    out << std::setw(8) << std::setprecision(4) << frexp10_s(qtf.t_(h1, f, d, 0).imag(), &exp) << "E"
                        << std::showpos << std::setw(4) << std::setprecision(0) << std::setfill('0') << std::internal << exp
                        << std::setfill(' ') << std::noshowpos << std::right;
                }
                out << "\n";
            }
            out << " \n \n";
        } else if (qtf.qtfType_ == QtfType::MQTF) {
            out << std::setprecision(2) << "##Heading1 =" << std::setw(7) << qtf.b_[h1]*180./M_PI << "\n";
            for(decltype(qtf.b_.size()) h2 = 0; h2 < qtf.b_.size(); ++h2) {
                out << std::setprecision(2) << "#Heading2 =" << std::setw(7) << qtf.b_[h2]*180./M_PI << "\n";
                out << std::setprecision(4);
                int exp;
                for(decltype(qtf.w_.size()) f = 0; f < qtf.w_.size(); ++f) {
                    out << std::setw(8) << std::setprecision(4) << frexp10_s(qtf.w_[f], &exp) << "E"
                        << std::showpos << std::setw(4) << std::setprecision(0) << std::setfill('0') << std::internal << exp
                        << std::setfill(' ') << std::noshowpos << std::right;
                    for(decltype(qtf.dw_.size()) d = 0; d < qtf.dw_.size(); ++d) {
                        out << std::setw(8) << std::setprecision(4) << frexp10_s(qtf.t_(h1, f, d, h2).real(), &exp) << "E"
                            << std::showpos << std::setw(4) << std::setprecision(0) << std::setfill('0') << std::internal << exp
                            << std::setfill(' ') << std::noshowpos << std::right;
                        out << std::setw(8) << std::setprecision(4) << frexp10_s(qtf.t_(h1, f, d, h2).imag(), &exp) << "E"
                            << std::showpos << std::setw(4) << std::setprecision(0) << std::setfill('0') << std::internal << exp
                            << std::setfill(' ') << std::noshowpos << std::right;
                    }
                    out << "\n";
                }
                out << " \n \n";
            }
        }
    }
    return out;
}

std::ifstream & BV::Spectral::operator >> (std::ifstream & in, MQtf& qtf) {
    std::string line;
    std::istringstream iss;
    std::string txt;
    for(int i = 0; i < 3; ++i)
        getline(in, line);      // skip 2 lines
    iss.str(line);              // and read the 3th
    iss >> txt >> txt >> txt >> qtf.filename_;
    getline(in, line);          // line 4
    getline(in, line);          // line 5
    iss.str(line);
    iss.clear();
    iss >> txt >> txt >> txt >> txt >> qtf.refLength_;
    getline(in, line);          // line 6
    iss.str(line);
    iss.clear();
    iss >> txt >> txt >> txt >> txt >> txt >> qtf.rho_;
    getline(in, line);          // line 7
    iss.str(line);
    iss.clear();
    iss >> txt >> txt >> txt >> txt >> qtf.g_;
    getline(in, line);          // line 8
    iss.str(line);
    iss.clear();
    iss  >> txt >> txt >> txt >> txt;
    if (txt == "Inf.") {
        qtf.depth_ = -1;
    } else {
        iss.str(txt);
        iss.clear();
        iss >> qtf.depth_;
    }
    getline(in, line);          // line 9
    iss.str(line);
    iss.clear();
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> qtf.waveRefPoint_(0) >> qtf.waveRefPoint_(1);
    getline(in, line);          // line 10
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    iss >> txt >> txt >> txt >> txt >> qtf.forwardSpeed_;
    for(int i = 0; i < 5; ++i)
        getline(in, line);      // skip 4 lines     (11-14)
    iss.str(line);              // and read the 5th (line 15)
    iss.clear();
    iss >> txt >> txt;
    if (txt == "Sum") {
        qtf.qtfMode_ =  QtfMode::SUM;
    } else if (txt == "Diff") {
        qtf.qtfMode_ = QtfMode::DIFF;
    } else {
        qtf.qtfMode_ = QtfMode::DIFF;
    }
    getline(in, line);          // line 16 : #DIRECTION
    getline(in, line);          // line 17 ; #COORD
    iss.str(line);
    iss.clear();
    iss >> txt >> qtf.refPoint_(0) >> qtf.refPoint_(1) >> qtf.refPoint_(2);
    getline(in, line);
    iss.str(line);                          // which corresponds to the 20th line of the file
    iss.clear();                            // to clear all previous flags
    int nbhead;
    iss >> txt >> nbhead;                  // #NBHEADING 2
    getline(in, line);
    iss.str(line);
    iss.clear();
    qtf.b_.resize(nbhead);
    iss >> txt;
    for (decltype(nbhead) i = 0; i < nbhead; ++i) {
        iss >> qtf.b_(i);
    }
    qtf.b_ = qtf.b_*M_PI/180.0;                       // degrees to radians
    getline(in, line);
    iss.str(line);
    iss.clear();
    std::vector<double> diffs;
    iss >> txt;
    double tmp;
    iss >> tmp;
    while(!iss.fail()) {
        diffs.push_back(tmp);
        iss >> tmp;
    }
    qtf.dw_.resize(diffs.size());
    for(decltype(diffs.size()) i = 0; i < diffs.size(); ++i)
        qtf.dw_[i] = diffs[i];
    getline(in, line);  // skip 1 line
    std::vector<std::vector<std::string>> headings1;
    std::vector<std::string> *headings2;
    while(true) {
        if (std::regex_match(line, std::regex("(##Heading1\\s*=)(.*)"))) {
            qtf.qtfType_ = QtfType::MQTF;
            break;
        } else if (std::regex_match(line, std::regex("(#Heading\\s*=)(.*)"))) {
            qtf.qtfType_ = QtfType::QTF;
            break;
        } else {
            getline(in, line);
        }
    }
    getline(in, line);  // next line after ##Heading1 = ... or #Heading = ...
    headings2 = new std::vector<std::string>();
    do {
        rtrim(line);
        if (qtf.qtfType_ == QtfType::MQTF) {
            if (std::regex_match(line, std::regex("(##Heading1\\s*=)(.*)"))) {
                headings1.push_back(*headings2);
                delete headings2;
                headings2 = new std::vector<std::string>();
            } else {
                if (line.compare("") != 0)
                    headings2->push_back(line);
            }
        } else if (qtf.qtfType_ == QtfType::QTF) {
            if (std::regex_match(line, std::regex("(#Heading\\s*=)(.*)"))) {
                headings1.push_back(*headings2);
                delete headings2;
                headings2 = new std::vector<std::string>();
            } else {
                if (line.compare("") != 0)
                    headings2->push_back(line);
            }
        }
    } while(getline(in, line));
    headings1.push_back(*headings2);
    delete headings2;
    // Now we have for each heading (qtf) or heading1 (mqtf) 
    int nbfreq(0);
    int nbhead2(1);
    int offset(0);
    if (qtf.qtfType_ == QtfType::MQTF) {
        // table is:
        // #Heading2 = ...
        // data
        // #Heading2 = ...
        // data
        // ...
        // Thus nbfreq+1 = number of lines of #Heading2 = ... +data
        nbfreq = static_cast<int>(headings1[0].size()) /nbhead -1;
        nbhead2 = nbhead;
        offset = 1;
    } else if (qtf.qtfType_ == QtfType::QTF) {
        // table is:
        // data
        // Thus nbfreq = number of lines of data
        nbfreq = static_cast<int>(headings1[0].size());
    }
    int nbdiff(static_cast<int>(qtf.dw_.size()));
    qtf.t_.resize(nbhead, nbfreq, nbdiff, nbhead2);
    qtf.w_.resize(nbfreq);
    for(decltype(nbhead) h1 = 0; h1 < nbhead; ++h1) {
        int h2_(0);
        for(decltype(nbhead2) h2 = 0; h2 < nbhead2; ++h2) {
            h2_ += offset;
            for (decltype(nbfreq) f = 0; f < nbfreq; ++f) {
                double v_r, v_i;
                iss.str(headings1[h1][h2_++]);
                iss.clear();
                iss >> qtf.w_[f];
                for(decltype(nbdiff) d = 0; d < nbdiff; d++) {
                    iss >> v_r >> v_i;
                    qtf.t_(h1, f, d, h2) = std::complex<double>(v_r, v_i);
                }
            }
        }
    }
    return in;
}

Eigen::ArrayXXd MQtf::getQtfAt(const Eigen::ArrayXXd &pts, const int &degree, const ComplexInterpolationStrategies &interpStrategy) const {
    Eigen::ArrayXXd res;
    if (qtfType_ == QtfType::QTF) {
        auto npts(pts.rows());
        res.resize(npts, 2);
        SPLINTER::DenseVector x(3);
        if (interpStrategy == ComplexInterpolationStrategies::RE_IM) {
            SPLINTER::DataTable samplesRe;
            SPLINTER::DataTable samplesIm;
            for(decltype(b_.size()) h1 = 0; h1 < b_.size(); ++h1) {
                x(0) = b_(h1);
                for (decltype(w_.size()) f = 0; f < w_.size(); ++f) {
                    x(1) = w_(f);
                    for(decltype(dw_.size()) d = 0; d < dw_.size(); d++) {
                        x(2) = dw_(d);
                        samplesRe.addSample(x, real(t_(h1, f, d, 0)));
                        samplesIm.addSample(x, imag(t_(h1, f, d, 0)));
                    }
                }
            }
            SPLINTER::BSpline bsplineRe = SPLINTER::BSpline::Builder(samplesRe).degree(degree).build();
            SPLINTER::BSpline bsplineIm = SPLINTER::BSpline::Builder(samplesIm).degree(degree).build();
            for(decltype(npts) pt = 0; pt < npts; ++pt) {
                x(0) = pts(pt, 0);                          // h1
                x(1) = pts(pt, 1);                          // w1
                x(2) = pts(pt, 2);                          // dw
                auto re = bsplineRe.eval(x);
                auto im = bsplineIm.eval(x);
                res(pt, 0) = sqrt(re*re +im*im);
                res(pt, 1) = atan2(im, re);
            }
        } else if (interpStrategy == ComplexInterpolationStrategies::RE_IM_AMP) {
            SPLINTER::DataTable samplesRe;
            SPLINTER::DataTable samplesIm;
            SPLINTER::DataTable samplesAmp;
            for(decltype(b_.size()) h1 = 0; h1 < b_.size(); ++h1) {
                x(0) = b_(h1);
                for (decltype(w_.size()) f = 0; f < w_.size(); ++f) {
                    x(1) = w_(f);
                    for(decltype(dw_.size()) d = 0; d < dw_.size(); d++) {
                        x(2) = dw_(d);
                        samplesRe.addSample(x, real(t_(h1, f, d, 0)));
                        samplesIm.addSample(x, imag(t_(h1, f, d, 0)));
                        samplesAmp.addSample(x, abs(t_(h1, f, d, 0)));
                    }
                }
            }
            SPLINTER::BSpline bsplineRe  = SPLINTER::BSpline::Builder(samplesRe ).degree(degree).build();
            SPLINTER::BSpline bsplineIm  = SPLINTER::BSpline::Builder(samplesIm ).degree(degree).build();
            SPLINTER::BSpline bsplineAmp = SPLINTER::BSpline::Builder(samplesAmp).degree(degree).build();
            for(decltype(npts) pt = 0; pt < npts; ++pt) {
                x(0) = pts(pt, 0);                          // h1
                x(1) = pts(pt, 1);                          // w1
                x(2) = pts(pt, 2);                          // dw
                auto re = bsplineRe.eval(x);
                auto im = bsplineIm.eval(x);
                auto amp = bsplineAmp.eval(x);
                auto phi = atan2(im, re);
                res(pt, 0) = amp;
                res(pt, 1) = phi;
            }
        } else { // AMP_PHASE
            SPLINTER::DataTable samplesAmp;
            SPLINTER::DataTable samplesPhi;
            for(decltype(b_.size()) h1 = 0; h1 < b_.size(); ++h1) {
                x(0) = b_(h1);
                for (decltype(w_.size()) f = 0; f < w_.size(); ++f) {
                    x(1) = w_(f);
                    for(decltype(dw_.size()) d = 0; d < dw_.size(); d++) {
                        x(2) = dw_(d);
                        double v_r = real(t_(h1, f, d, 0));
                        double v_i = imag(t_(h1, f, d, 0));
                        samplesAmp.addSample(x, abs(t_(h1, f, d, 0)));
                        samplesPhi.addSample(x, atan2(v_i, v_r));
                    }
                }
            }
            SPLINTER::BSpline bsplineAmp = SPLINTER::BSpline::Builder(samplesAmp).degree(degree).build();
            SPLINTER::BSpline bsplinePhi = SPLINTER::BSpline::Builder(samplesPhi).degree(degree).build();
            for(decltype(npts) pt = 0; pt < npts; ++pt) {
                x(0) = pts(pt, 0);                          // h1
                x(1) = pts(pt, 1);                          // w1
                x(2) = pts(pt, 2);                          // dw
                auto amp = bsplineAmp.eval(x);
                auto phi = bsplinePhi.eval(x);
                res(pt, 0) = amp;
                res(pt, 1) = phi;
            }
        }
    } else if (qtfType_ == QtfType::MQTF) {
        auto npts(pts.rows());
        res.resize(npts, 2);
        SPLINTER::DenseVector x(4);
        if (interpStrategy == ComplexInterpolationStrategies::RE_IM) {
            SPLINTER::DataTable samplesRe;
            SPLINTER::DataTable samplesIm;
            for(decltype(b_.size()) h1 = 0; h1 < b_.size(); ++h1) {
                x(0) = b_(h1);
                for(decltype(b_.size()) h2 = 0; h2 < b_.size(); ++h2) {
                    x(3) = b_(h2);
                    for (decltype(w_.size()) f = 0; f < w_.size(); ++f) {
                        x(1) = w_(f);
                        for(decltype(dw_.size()) d = 0; d < dw_.size(); d++) {
                            x(2) = dw_(d);
                            samplesRe.addSample(x, real(t_(h1, f, d, h2)));
                            samplesIm.addSample(x, imag(t_(h1, f, d, h2)));
                        }
                    }
                }
            }
            SPLINTER::BSpline bsplineRe = SPLINTER::BSpline::Builder(samplesRe).degree(degree).build();
            SPLINTER::BSpline bsplineIm = SPLINTER::BSpline::Builder(samplesIm).degree(degree).build();
            for(decltype(npts) pt = 0; pt < npts; ++pt) {
                x(0) = pts(pt, 0);                          // h1
                x(1) = pts(pt, 1);                          // w1
                x(2) = pts(pt, 2);                          // dw
                x(3) = pts(pt, 3);                          // h2
                auto re = bsplineRe.eval(x);
                auto im = bsplineIm.eval(x);
                res(pt, 0) = sqrt(re*re +im*im);
                res(pt, 1) = atan2(im, re);
            }
        } else if (interpStrategy == ComplexInterpolationStrategies::RE_IM_AMP) {
            SPLINTER::DataTable samplesRe;
            SPLINTER::DataTable samplesIm;
            SPLINTER::DataTable samplesAmp;
            for(decltype(b_.size()) h1 = 0; h1 < b_.size(); ++h1) {
                x(0) = b_(h1);
                for(decltype(b_.size()) h2 = 0; h2 < b_.size(); ++h2) {
                    x(3) = b_(h2);
                    for (decltype(w_.size()) f = 0; f < w_.size(); ++f) {
                        x(1) = w_(f);
                        for(decltype(dw_.size()) d = 0; d < dw_.size(); d++) {
                            x(2) = dw_(d);
                            samplesRe.addSample(x, real(t_(h1, f, d, h2)));
                            samplesIm.addSample(x, imag(t_(h1, f, d, h2)));
                            samplesAmp.addSample(x, abs(t_(h1, f, d, h2)));
                        }
                    }
                }
            }
            SPLINTER::BSpline bsplineRe  = SPLINTER::BSpline::Builder(samplesRe ).degree(degree).build();
            SPLINTER::BSpline bsplineIm  = SPLINTER::BSpline::Builder(samplesIm ).degree(degree).build();
            SPLINTER::BSpline bsplineAmp = SPLINTER::BSpline::Builder(samplesAmp).degree(degree).build();
            for(decltype(npts) pt = 0; pt < npts; ++pt) {
                x(0) = pts(pt, 0);                          // h1
                x(1) = pts(pt, 1);                          // w1
                x(2) = pts(pt, 2);                          // dw
                x(3) = pts(pt, 3);                          // h2
                auto re = bsplineRe.eval(x);
                auto im = bsplineIm.eval(x);
                auto amp = bsplineAmp.eval(x);
                auto phi = atan2(im, re);
                res(pt, 0) = amp;
                res(pt, 1) = phi;
            }
        } else { // AMP_PHASE
            SPLINTER::DataTable samplesAmp;
            SPLINTER::DataTable samplesPhi;
            for(decltype(b_.size()) h1 = 0; h1 < b_.size(); ++h1) {
                x(0) = b_(h1);
                for(decltype(b_.size()) h2 = 0; h2 < b_.size(); ++h2) {
                    x(3) = b_(h2);
                    for (decltype(w_.size()) f = 0; f < w_.size(); ++f) {
                        x(1) = w_(f);
                        for(decltype(dw_.size()) d = 0; d < dw_.size(); d++) {
                            x(2) = dw_(d);
                            double v_r = real(t_(h1, f, d, h2));
                            double v_i = imag(t_(h1, f, d, h2));
                            samplesAmp.addSample(x, abs(t_(h1, f, d, h2)));
                            samplesPhi.addSample(x, atan2(v_i, v_r));
                        }
                    }
                }
            }
            SPLINTER::BSpline bsplineAmp = SPLINTER::BSpline::Builder(samplesAmp).degree(degree).build();
            SPLINTER::BSpline bsplinePhi = SPLINTER::BSpline::Builder(samplesPhi).degree(degree).build();
            for(decltype(npts) pt = 0; pt < npts; ++pt) {
                x(0) = pts(pt, 0);                          // h1
                x(1) = pts(pt, 1);                          // w1
                x(2) = pts(pt, 2);                          // dw
                x(3) = pts(pt, 3);                          // h2
                auto amp = bsplineAmp.eval(x);
                auto phi = bsplinePhi.eval(x);
                res(pt, 0) = amp;
                res(pt, 1) = phi;
            }
        }
    }
    return res;
}

// Qtf version
Eigen::Vector2d MQtf::getQtfAt(const Eigen::Vector3d &pt, const int &degree, const ComplexInterpolationStrategies &interpStrategy) const {
    Eigen::ArrayXXd pts(1, 3);
    pts(0, 0) = pt(0);  // h1
    pts(0, 1) = pt(1);  // w1
    pts(0, 2) = pt(2);  // dw
    auto v = getQtfAt(pts, degree, interpStrategy);
    return Eigen::Vector2d(v(0, 0), v(0, 1));
}

// MQtf version
Eigen::Vector2d MQtf::getQtfAt(const Eigen::Vector4d &pt, const int &degree, const ComplexInterpolationStrategies &interpStrategy) const {
    Eigen::ArrayXXd pts(1, 4);
    pts(0, 0) = pt(0);  // h1
    pts(0, 1) = pt(1);  // w1
    pts(0, 2) = pt(2);  // dw
    pts(0, 3) = pt(3);  // h2
    auto v = getQtfAt(pts, degree, interpStrategy);
    return Eigen::Vector2d(v(0, 0), v(0, 1));
}

MQtf MQtf::getQtfAt(Eigen::ArrayXd const & hs,
                  Eigen::ArrayXd const & ws, 
                  Eigen::ArrayXd const & dws, 
                  int const & degree, 
                  ComplexInterpolationStrategies const & interpStrategy) const {
    MQtf qtf;
    qtf.filename_ = filename_;
    qtf.forwardSpeed_ = forwardSpeed_;
    qtf.refLength_ = refLength_;
    qtf.rho_ = rho_;
    qtf.g_ = g_;
    qtf.depth_ = depth_;
    qtf.waveRefPoint_ = waveRefPoint_;
    qtf.refPoint_ = refPoint_;
    qtf.b_ = hs;
    qtf.dw_ = dws;
    qtf.w_ = ws;
    qtf.qtfMode_ = qtfMode_;
    qtf.qtfType_ = qtfType_;
    if (qtfType_ == QtfType::QTF) {
        qtf.t_.resize(qtf.b_.size(), qtf.w_.size(), qtf.dw_.size(), 1);
        int npts(qtf.b_.size() *qtf.w_.size() *qtf.dw_.size());
        Eigen::ArrayXXd pts(npts, 3);
        int pt = 0;
        for(decltype(qtf.b_.size()) h1 = 0; h1 < qtf.b_.size(); h1++) {
            for(decltype(qtf.w_.size()) f = 0; f < qtf.w_.size(); f++) {
                for(decltype(qtf.dw_.size()) d = 0; d < qtf.dw_.size(); d++) {
                    pts(pt, 0) = qtf.b_(h1);
                    pts(pt, 1) = qtf.w_(f);
                    pts(pt, 2) = qtf.dw_(d);
                    pt ++;
                }
            }
        }
        auto v = getQtfAt(pts, degree, interpStrategy);
        pt = 0;
        for(decltype(qtf.b_.size()) h1 = 0; h1 < qtf.b_.size(); h1++) {
            for(decltype(qtf.w_.size()) f = 0; f < qtf.w_.size(); f++) {
                for(decltype(qtf.dw_.size()) d = 0; d < qtf.dw_.size(); d++) {
                    qtf.t_(h1, f, d, 0) = v(pt, 0) *std::complex<double>(cos(v(pt, 1)), sin(v(pt, 1)));
                    pt ++;
                }
            }
        }
    } else if (qtfType_ == QtfType::MQTF) {
        qtf.t_.resize(qtf.b_.size(), qtf.w_.size(), qtf.dw_.size(), qtf.b_.size());
        int npts(qtf.b_.size() *qtf.w_.size() *qtf.dw_.size() *qtf.b_.size());
        Eigen::ArrayXXd pts(npts, 4);
        int pt = 0;
        for(decltype(qtf.b_.size()) h1 = 0; h1 < qtf.b_.size(); h1++) {
            for(decltype(qtf.b_.size()) h2 = 0; h2 < qtf.b_.size(); h2++) {
                for(decltype(qtf.w_.size()) f = 0; f < qtf.w_.size(); f++) {
                    for(decltype(qtf.dw_.size()) d = 0; d < qtf.dw_.size(); d++) {
                        pts(pt, 0) = qtf.b_(h1);
                        pts(pt, 1) = qtf.w_(f);
                        pts(pt, 2) = qtf.dw_(d);
                        pts(pt, 3) = qtf.b_(h2);
                        pt ++;
                    }
                }
            }
        }
        auto v = getQtfAt(pts, degree, interpStrategy);
        pt = 0;
        for(decltype(qtf.b_.size()) h1 = 0; h1 < qtf.b_.size(); h1++) {
            for(decltype(qtf.b_.size()) h2 = 0; h2 < qtf.b_.size(); h2++) {
                for(decltype(qtf.w_.size()) f = 0; f < qtf.w_.size(); f++) {
                    for(decltype(qtf.dw_.size()) d = 0; d < qtf.dw_.size(); d++) {
                        qtf.t_(h1, f, d, h2) = v(pt, 0) *std::complex<double>(cos(v(pt, 1)), sin(v(pt, 1)));
                        pt ++;
                    }
                }
            }
        }
    }
    return qtf;
}


MQtf MQtf::getQtfAtHeadings(Eigen::ArrayXd const & hs, int const & degree, ComplexInterpolationStrategies const & interpStrategy) const {
    return getQtfAt(hs, w_, dw_, degree, interpStrategy);
}

MQtf MQtf::getQtfAtFrequencies(Eigen::ArrayXd const & ws, int const & degree, ComplexInterpolationStrategies const & interpStrategy) const {
    double dw(ws(1)-ws(0));
    double dwMax(dw_(dw_.size()-1));
    int dwMaxI(static_cast<int>(dwMax/dw));
    dwMax = dw *dwMaxI;
    Eigen::ArrayXd dws(Eigen::ArrayXd::LinSpaced(dwMaxI, dw_(0), dwMax));
    return getQtfAt(b_, ws, dws, degree, interpStrategy);
}

void MQtf::writeHeadHeadTensor(std::string const & filename) const {
    if (qtfType_ == QtfType::MQTF) {
        std::ofstream out(filename);
        out << std::scientific;
        for(int f = 0; f < w_.size(); f++) {
            for(int d = 0; d < dw_.size(); d++) {
                for(int h1 = 0; h1 < b_.size(); h1++) {
                    for(int h2 = 0; h2 < b_.size(); h2 ++) {
                        out 
                            << std::setw(18) << std::setprecision(11) <<  b_(h1) << "\t"
                            << std::setw(18) << std::setprecision(11) <<  b_(h2) << "\t"
                            << std::setw(18) << std::setprecision(11) <<  w_( f) << "\t"
                            << std::setw(18) << std::setprecision(11) << dw_( d) << "\t"
                            << std::setw(18) << std::setprecision(11) << real(t_(h1, f, d, h2)) << "\t"
                            << std::setw(18) << std::setprecision(11) << imag(t_(h1, f, d, h2)) << "\t"
                            << "\n";
                    }
                    out << "\n";
                }
                out << "\n\n";
            }
            out << "\n\n";
        }
        out.close();
    } else if (qtfType_ == QtfType::QTF) {
        std::ofstream out(filename);
        out << std::scientific;
        for(int f = 0; f < w_.size(); f++) {
            for(int d = 0; d < dw_.size(); d++) {
                for(int h1 = 0; h1 < b_.size(); h1++) {
                    out 
                        << std::setw(18) << std::setprecision(11) <<  b_(h1) << "\t"
                        << std::setw(18) << std::setprecision(11) <<  b_(h1) << "\t"
                        << std::setw(18) << std::setprecision(11) <<  w_( f) << "\t"
                        << std::setw(18) << std::setprecision(11) << dw_( d) << "\t"
                        << std::setw(18) << std::setprecision(11) << real(t_(h1, f, d, 0)) << "\t"
                        << std::setw(18) << std::setprecision(11) << imag(t_(h1, f, d, 0)) << "\t"
                        << "\n";
                    out << "\n";
                }
                out << "\n\n";
            }
            out << "\n\n";
        }
        out.close();
    }
}


void MQtf::writeWdWTensor(std::string const & filename) const {
    if (qtfType_ == QtfType::MQTF) {
        std::ofstream out(filename);
        out << std::scientific;
        for(int h1 = 0; h1 < b_.size(); h1++) {
            for(int h2 = 0; h2 < b_.size(); h2 ++) {
                for(int f = 0; f < w_.size(); f++) {
                    for(int d = 0; d < dw_.size(); d++) {
                        out 
                            << std::setw(18) << std::setprecision(11) <<  w_( f) << "\t"
                            << std::setw(18) << std::setprecision(11) << dw_( d) << "\t"
                            << std::setw(18) << std::setprecision(11) <<  b_(h1) << "\t"
                            << std::setw(18) << std::setprecision(11) <<  b_(h2) << "\t"
                            << std::setw(18) << std::setprecision(11) << real(t_(h1, f, d, h2)) << "\t"
                            << std::setw(18) << std::setprecision(11) << imag(t_(h1, f, d, h2)) << "\t"
                            << "\n";
                    }
                    out << "\n";
                }
                out << "\n\n";
            }
            out << "\n\n";
        }
        out.close();
    } else if (qtfType_ == QtfType::QTF) {
        std::ofstream out(filename);
        out << std::scientific;
        for(int h1 = 0; h1 < b_.size(); h1++) {
            for(int f = 0; f < w_.size(); f++) {
                for(int d = 0; d < dw_.size(); d++) {
                    out 
                        << std::setw(18) << std::setprecision(11) <<  w_( f) << "\t"
                        << std::setw(18) << std::setprecision(11) << dw_( d) << "\t"
                        << std::setw(18) << std::setprecision(11) <<  b_(h1) << "\t"
                        << std::setw(18) << std::setprecision(11) <<  b_(h1) << "\t"
                        << std::setw(18) << std::setprecision(11) << real(t_(h1, f, d, 0)) << "\t"
                        << std::setw(18) << std::setprecision(11) << imag(t_(h1, f, d, 0)) << "\t"
                        << "\n";
                }
                out << "\n";
            }
            out << "\n\n";
        }
        out.close();
    }
}

Eigen::Tensor<std::complex<double>, 4> MQtf::getQtfLikeComplexData() const
{
    auto nbhead(getHeadings().size());
    auto nbfreq(getFrequencies().size());
    auto nbdiff(getDeltaFrequencies().size());
    auto nbmodes(getModes().size());
    auto data(getComplexData());
    Eigen::Tensor<std::complex<double>, 4> res(nbhead, nbfreq, nbdiff, nbmodes);
    for(Eigen::Index b = 0; b < nbhead; b++)
        for(Eigen::Index w = 0; w < nbfreq; w++)
            for(Eigen::Index dw = 0; dw < nbdiff; dw++)
                for(Eigen::Index m = 0; m < nbmodes; m++)
                    res(b, w, dw, m) = data(b, w, dw, b);
    return res;
}
