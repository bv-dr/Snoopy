#include "Jonswap.hpp"

using namespace BV::Spectral;

const char* Jonswap::name = "Jonswap";

Eigen::ArrayXd Jonswap::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if (tp_ <= 0.  || gamma_ < 0.9999  || hs_ <= 0.  )
        throw std::logic_error("JONSWAP : Incorrect parameters, Hs and Tp should be positive and gamma >= 1  ") ;

    Eigen::ArrayXd sw(w.size());

    double tp4 = pow(1. / tp_, 4.);
    double hs2 = hs_ * hs_;
    double wp = (M_PI + M_PI) / tp_;

    // Normalizing factor (ISO - 19901)
    double a = 487.045 / (0.325 * pow(gamma_, 0.803) + 0.675);

    // #pragma omp parallel for  // Speed-up a bit, but significant overhead with n=1, so that it is not worth it...
    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i];
        if (wi == 0)
            continue;

        double wpsw4 = wp / wi;
        wpsw4 = wpsw4 * wpsw4;
        wpsw4 = -1.25 * wpsw4 * wpsw4;
        double wswp2 = (wi - wp) / wp;
        wswp2 = wswp2 * wswp2;

        if (wi < wp)
            wswp2 = -wswp2 * 1. / (2. * sigmaA_ * sigmaA_);
        else
            wswp2 = -wswp2 * 1. / (2. * sigmaB_ * sigmaB_);

        double w5 = pow(wi, 5.);
        sw[i] = a * hs2 * tp4 * exp(wpsw4) * pow(gamma_, exp(wswp2)) / w5;
    }

    return sw;
}


std::tuple<double, double> Jonswap::get_wrange(double energyRatio) const
{
    double wp = 2 * M_PI / getTp();
    double wmax = (0.75 + 0.34 * std::exp(-0.159 * gamma_)) * std::pow( (1 - energyRatio), (-1./4.));
    double wmin = std::min(1.0, 1.09 - 0.44 * energyRatio);
    return std::tuple<double, double>(wmin * wp, wmax * wp);
}