#include "Wif.hpp"

#include "Dispersion.hpp"
#include "SeaState.hpp"
#include "SpectralTools.hpp"
#include "Rao.hpp"

#include "Math/Tools.hpp"

#include <fstream>
#include <regex>
#include <set>

using namespace BV::Spectral;
using namespace BV::Math;

namespace {

/**
 * Helper function to get the number of waves given a spectrum, a list of
 * frequencies and a list of headings
 */
Eigen::Index getNumberOfWaves(const WaveSpectrum & spectrum,
                              const Eigen::Ref<const Eigen::ArrayXd>& w,
                              const Eigen::Ref<const Eigen::ArrayXd>& b)
{
    const auto noFrequencies = w.size() ;
    Eigen::Index noHeadings = 1 ;
    if (spectrum.getSpreadingType() != SpreadingType::No)
    {
        noHeadings = b.size() ;
    }

    return noFrequencies * noHeadings ;
}
}

Wif::Wif(const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& a,
         const Eigen::Ref<const Eigen::ArrayXd>& phi,
         const Eigen::Ref<const Eigen::ArrayXd>& b, double depth):
    w_(w), amp_(a), phi_(phi), b_(b), depth_(depth)
{
    initialize() ;
}

 Wif::Wif(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& cos_sin, double b, double depth):
    w_(w), depth_(depth)
{
     auto n = cos_sin.size() / 2;
     amp_ = (cos_sin.segment(0,n).pow(2.0) + cos_sin.segment(n, n).pow(2.0)).pow(0.5);
     phi_ = cos_sin.segment(n, n).binaryExpr(cos_sin.segment(0, n), [](double a, double b) { return std::atan2(a, b); });
     b_ = Eigen::ArrayXd::Constant(n, b);
     initialize();
}

Wif::Wif(const std::string& filename, double depth):
    depth_(depth), isWidth_(false)
{
    std::fstream f ;
    f.open(filename, std::ios::in) ;
    if (f.is_open())
    {  // file is opened successfully!
        std::string line ;
        std::regex commentLine("^\\s*#") ;
        std::regex emptyLine("^\\s*$") ;
        std::smatch m ;
        int counter = 0 ;
        int resizeIncrement = 100 ;
        w_.resize(resizeIncrement) ;
        amp_.resize(resizeIncrement) ;
        phi_.resize(resizeIncrement) ;
        b_.resize(resizeIncrement) ;
        while (std::getline(f, line))
        {
            if (std::regex_search(line, m, commentLine)) continue ; // it is not a comment line '#', '  #', ...
            if (std::regex_match(line, m, emptyLine)) continue ; // it is not an empty line '', '  ', '\t', ...
            // all non-comment lines
            std::istringstream iss(line) ;
            if (counter >= w_.size())
            {
                w_.conservativeResize(counter + resizeIncrement) ;
                amp_.conservativeResize(counter + resizeIncrement) ;
                phi_.conservativeResize(counter + resizeIncrement) ;
                b_.conservativeResize(counter + resizeIncrement) ;
            }
            double w, a, p, h ;
            iss >> w >> a >> p >> h ;
            w_(counter) = w ;
            amp_(counter) = a ;
            phi_(counter) = p ;
            b_(counter) = h ;
            counter++ ;
        }
        f.close() ;
        if (w_.size() != counter)
        {
            w_.conservativeResize(counter) ;
            amp_.conservativeResize(counter) ;
            phi_.conservativeResize(counter) ;
            b_.conservativeResize(counter) ;
        }

        b_ *= M_PI / 180. ;   // Wif text format is (deg), while internal use is rad
        initialize() ;
    }
    else
    {
        //std::cout << "Error in opening the file " << filename << std::endl;
        throw std::invalid_argument("File not found :" + filename);
    }
}

/*
 * Generates the rdtsc instruction, which returns the processor time stamp. The processor time stamp records the number of clock cycles since the last reset.
 *
 * Normally, for Windows VS 2015 there is intrinsic function __rdtsc(), which is not defined for g++.
 *
 * This function is used for seeding.
 * Explanation: from https://stackoverflow.com/questions/7617587/is-there-an-alternative-to-using-time-to-seed-a-random-number-generation
 *      Given the high frequency of today's machines, it's extremely unlikely that two processors will return the same value even
 * if they booted at the same time and are clocked at the same speed.
 *
 * Some comments about the seedig:
 * 1)
 * #include <ctime>
 * seed = std::time(NULL);
 * Fails if few random data sets are required within 1 second (std::time(NULL) resolution is 1 second)
 * 2) better but ...
 * Normally, std::chrono::system_clock::now().time_since_epoch().count() returns very big number:
 *      time passed from Jan. 01 1970 in nanoseconds (on my computer, ITEN).
 *      for example:    1515593769846931979_10
 *                      0001 0101 0000 1000 0111 1000 1000 0000  1111 0011 1000 0101 1000 0010 0000 1011_2
 *      Convertion to unsigned int gives:
 *                      4085613067_10
 *                      1111 0011 1000 0101 1000 0010 0000 1011_2
 * Thus, in even 1 nanosecond the seed will be different
 * #include <chrono>
 * seed = (unsigned int)std::chrono::system_clock::now().time_since_epoch().count();
 * Works well on Linux, but fails on virtual Windows (1 processor) may be because of Windows or Virtualisation.
 * Probably, on Linux there some cases, where it is failed.
 * It may also fails, if the code is running on the cluster
 * 3) rdtsc.
 * At least passed few tests on Linux and virtual Windows.
 *
 * TODO: may have a problems with compilation with other compiler rather than gnu (vs2015++). Not verified
 */

#ifdef _MSC_VER
#include <intrin.h>
#pragma intrinsic(__rdtsc)
#endif

#ifndef _MSC_VER
#include <x86intrin.h>
#endif

/* Not usefull, as most of the compiler as built-in __rdtsc function
 static inline unsigned long long rdtsc()
 {
 unsigned int lo,hi;
 __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
 return ((unsigned long long)hi << 32) | lo;
 }
 */

void Wif::computeWifParameters(const WaveSpectrum & spectrum,
                               const Eigen::Ref<const Eigen::ArrayXd>& w,
                               const Eigen::Ref<const Eigen::ArrayXd>& b,
                               unsigned int seed, Eigen::Index segment)
{
    const auto noFrequencies = w.size() ;
    Eigen::Index noHeadings = 1 ;
    if (spectrum.getSpreadingType() != SpreadingType::No)
    {
        noHeadings = b.size() ;
    }

    if (w.size() < 2) 
    {
        throw std::invalid_argument("Invalid size for w (should be >=2)");
    }

    // Compute delta omega and delta beta
    double dw = w[1] - w[0] ;
    double db = 1. ;
    if (spectrum.getSpreadingType() != SpreadingType::No)
    {
        db = b[1] - b[0] ;
    }

    Eigen::ArrayXd sw = spectrum.compute(w) ;
    double heading = spectrum.heading ;

    auto index = segment ;
    for (Eigen::Index i = 0; i < noHeadings; ++i)
    {
        double spreadCoef ;
        if (spectrum.getSpreadingType() != SpreadingType::No)
        {
            heading = b[i] ;
            spreadCoef = spectrum.computeSpreading(heading) ;
        }
        else
        {
            spreadCoef = 1. ;
        }

        w_.segment(index, noFrequencies) = w ;

        Eigen::ArrayXd iSw = sw * spreadCoef * db ;
        amp_.segment(index, noFrequencies) = Eigen::sqrt(2. * iSw * dw) ; //iSw.unaryExpr([dw](double val) { return sqrt(2. * val * dw); });

        b_.segment(index, noFrequencies) = Eigen::ArrayXd::Constant(
            noFrequencies, heading) ;
        index += noFrequencies ;
    }

    //Random phases
    // Generate a random seed if needed
    if (seed == 0)
    {
        seed = static_cast<unsigned int>(__rdtsc()) ; // see comments for rdtsc few lines above
    }
    std::srand(seed) ;
    phi_.segment(segment, noFrequencies * noHeadings) = Eigen::ArrayXd::Random(
        noFrequencies * noHeadings) * M_PI + M_PI ;
}

void Wif::computeWifParameters(const Spectrum & spectrum,
                               const Eigen::Ref<const Eigen::ArrayXd>& w,
                               unsigned int seed, Eigen::Index segment)
{
    const bool testMeanValue = !(almost_equal(spectrum.getMeanValue(), 0.)) ;
    unsigned int indexFirst = testMeanValue ? 1 : 0 ;
    const auto noFrequencies = w.size() ;
    // Compute delta omega and delta beta
    double dw = w[1] - w[0] ;
    Eigen::ArrayXd sw = spectrum.compute(w) ;
    const double heading = spectrum.heading ;
    auto index = segment ;
    w_.segment(index + indexFirst, noFrequencies) = w ;
    amp_.segment(index + indexFirst, noFrequencies) = Eigen::sqrt(
        2. * sw * dw) ; //iSw.unaryExpr([dw](double val) { return sqrt(2. * val * dw); });
    b_.segment(index, noFrequencies + indexFirst) = Eigen::ArrayXd::Constant(
        noFrequencies, heading) ;
    //Random phases
    // Generate a random seed if needed
    if (seed == 0)
    {
        seed = static_cast<unsigned int>(__rdtsc()) ; // see comments for rdtsc few lines above
    }
    std::srand(seed) ;
    phi_.segment(index + indexFirst, noFrequencies) = Eigen::ArrayXd::Random(
        noFrequencies) * M_PI + M_PI ;
    if (testMeanValue)
    {
        w_(index) = 0. ;
        amp_(index) = spectrum.getMeanValue() ;
        phi_(index) = 0. ;
    }
}

void Wif::setDepth(double depth)
{
    if (almost_equal(depth, depth_))
    {
        return ;
    }
    depth_ = depth ;
    k_ = w2k(w_, depth_) ;
}

void BV::Spectral::Wif::offset(double dt, double dx, double dy)
{
    auto k = getWaveNumbers();
    auto w = getFrequencies();
    auto dphi = -w * dt + k * (dx * getCosHeadings() + dy * getSinHeadings());
    setPhases(phi_ + dphi);
}

void Wif::setHeadings(const Eigen::ArrayXd& b)
{
    if (w_.size() == b.size())
    {
        b_ = b ;
        initialize(true) ;
    }
    else
    {
        throw std::invalid_argument("Invalid size for b") ;
    }
}

void Wif::setHeadings(double b)
{
    b_ = Eigen::ArrayXd::Constant(w_.size(), b);
    initialize(true);
}

void Wif::initialize(bool onlyHeadings)
{
    if ((amp_.size() != w_.size()) ||
        (amp_.size() != b_.size()) ||
        (amp_.size() != phi_.size()))
    {
        std::stringstream ss;
        ss << "Wif is initialized with wrong sizes:\n"
           << "\tlen(amp)  :" << amp_.size() << "\n"
           << "\tlen(freq) :" << w_.size() << "\n"
           << "\tlen(phi)  :" << phi_.size() << "\n"
           << "\tlen(head) :" << b_.size() << "\n"
           ;
        throw std::invalid_argument(ss.str());
    }
    BV::Math::WrapAngle0_2PI(b_);
    cb_ = Eigen::cos(b_) ;
    sb_ = Eigen::sin(b_) ;
    if (!onlyHeadings)
    {
        k_ = w2k(w_, depth_) ;
    }
    std::vector<double> independentHeadings ;
    Eigen::Index nComponents(b_.size()) ;
    double previousB(b_(0)) ;
    independentHeadingsIndices_.clear() ;
    independentHeadingsIndices_.push_back(0) ;
    independentHeadings.push_back(b_(0)) ;
    for (unsigned iComp = 1; iComp < nComponents; ++iComp)
    {
        if (IsClose(b_(iComp), previousB))
        {
            continue ;
        }
        previousB = b_(iComp) ;
        independentHeadingsIndices_.push_back(iComp) ;
        independentHeadings.push_back(previousB) ;
    }
    independentHeadings_ = Eigen::Map<Eigen::ArrayXd>(independentHeadings.data(),
                                                      independentHeadings.size()) ;


    unidirectional_ = (independentHeadings_.size() == 1);
}

Wif::Wif(const WaveSpectrum& spectrum,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& b, unsigned int seed,
         double depth) :
    depth_(depth)

{
    if (b.size() <= 1 && spectrum.spreading->getType() != SpreadingType::No)
    {
        throw std::invalid_argument("Spectrum with spreading should be discretized with more than one value");
    }

    auto noWaves = getNumberOfWaves(spectrum, w, b) ;

    w_ = Eigen::ArrayXd(noWaves) ;
    amp_ = Eigen::ArrayXd(noWaves) ;
    phi_ = Eigen::ArrayXd(noWaves) ;
    b_ = Eigen::ArrayXd(noWaves) ;

    computeWifParameters(spectrum, w, b, seed) ;
    initialize() ;
}

Wif::Wif(const Spectrum& spectrum, const Eigen::Ref<const Eigen::ArrayXd>& w,
         unsigned int seed, double depth) :
    depth_(depth)

{


    const bool testMeanValue = !(almost_equal(spectrum.getMeanValue(), 0.)) ;
    const auto noWaves = testMeanValue ? w.size() + 1 : w.size() ;

    w_ = Eigen::ArrayXd(noWaves) ;
    amp_ = Eigen::ArrayXd(noWaves) ;
    phi_ = Eigen::ArrayXd(noWaves) ;
    b_ = Eigen::ArrayXd(noWaves) ;

    computeWifParameters(spectrum, w, seed) ;
    initialize() ;
}


void Wif::setSpectrum(const Spectrum& spectrum)
{
    if (isWidth_ == false)
    {
        throw std::invalid_argument(
            "Can not set spectrum without bins widths") ;
    }
    amp_ = pow( 2 * spectrum.compute(w_) * wWidth_, 0.5) ;
    b_ = Eigen::ArrayXd::Constant(amp_.size(), spectrum.heading) ;
    cb_ = Eigen::cos(b_) ;
    sb_ = Eigen::sin(b_) ;
    const bool testMeanValue = !(almost_equal(spectrum.getMeanValue(), 0.)) ;
    if (testMeanValue)
    {
        amp_(0) = spectrum.getMeanValue() ;
    }
    // initialize(true) ; FIXME requested for Spreading
}

void Wif::setSpectrum(const WaveSpectrum& spectrum)
{
    if (isWidth_ == false)
    {
        throw std::invalid_argument(
            "Can not set spectrum without bins widths") ;
    }

    for (auto iwave = 0; iwave < amp_.size() + 1; ++iwave)
    {
        // optimization could be performed here, as spreading is the same for most of the headings. Should not be a bottleneck though
        double spreadFactor = 1. ;
        if (spectrum.getSpreadingType() != SpreadingType::No)
        {
            spreadFactor = spectrum.computeSpreading(b_(iwave))
                * bWidth_(iwave) ;
        }
        amp_(iwave) = pow(
            (2 * spectrum.compute(w_(iwave)) * wWidth_(iwave) * spreadFactor),
            0.5) ;
    }
}

Wif::Wif(const Spectrum& spectrum, double wmin, double wmax,
         unsigned int nbSeed, unsigned int seed,
         double depth) :
    depth_(depth)
{
    const bool testMeanValue = !(almost_equal(spectrum.getMeanValue(), 0.)) ;
    //Generate a random seed if needed
    if (seed == 0)
    {
        seed = static_cast<unsigned int>(__rdtsc()) ; // see comments for rdtsc few lines above
    }
    unsigned int offset = testMeanValue ? 1 : 0 ;
    std::srand(seed) ;
    b_ = Eigen::ArrayXd::Zero(offset + nbSeed) ;
    b_ += spectrum.heading ;
    cb_ = Eigen::cos(b_);
    sb_ = Eigen::sin(b_);
    w_ = Eigen::ArrayXd::Zero(offset + nbSeed);
    amp_ = Eigen::ArrayXd::Zero(offset + nbSeed) ;
    k_ = Eigen::ArrayXd::Zero(offset + nbSeed) ;
    phi_ = Eigen::ArrayXd::Random(offset + nbSeed) * M_PI + M_PI ;
    wWidth_ = Eigen::ArrayXd::Zero(offset + nbSeed) ;
    isWidth_ = true ;

    Eigen::ArrayXd list_freq;
    list_freq = spectrum.getDiscreteFrequencyEdges( wmin, wmax, nbSeed, seed);

    for (Eigen::Index i = 0; i < nbSeed; ++i)
    {
        w_(offset + i) = 0.5*(list_freq(i+1) + list_freq(i));
        wWidth_(offset + i) = list_freq(i + 1) - list_freq(i);
        double sw = spectrum.compute(w_(offset + i));
        amp_(offset + i) = pow((2 * sw * wWidth_(offset + i)), 0.5);
    }
    if (testMeanValue)
    {
        w_(0) = 0. ;
        wWidth_(0) = w_(1) - w_(0) ;
        amp_(0) = spectrum.getMeanValue() ;
        phi_(0) = 0. ;
    }
    initialize() ;
}

Wif::Wif(const WaveSpectrum& spectrum, double wmin, double wmax,
         unsigned int nbSeed, unsigned int spreadNbHeading, unsigned int seed,
         double depth) :
    depth_(depth)
{

    //waveRefPoint_ = 0.

    //Generate a random seed if needed
    if (seed == 0)
    {
        seed = static_cast<unsigned int>(__rdtsc()) ; // see comments for rdtsc few lines above
    }
    std::srand(seed) ;

    // Handle headings
    int nbheading = 1 ;

    // Ensure that nbHeading is even
    if (spectrum.getSpreadingType() != SpreadingType::No)
    {
        if (spreadNbHeading == 1)
            {
                throw std::logic_error( "More than one heading should be used to discritize a spectrum with spreading" );
            }
        nbheading = static_cast<int>(std::ceil(spreadNbHeading / 2)) * 2;
    }

    Eigen::ArrayXd wif_heading = Eigen::ArrayXd::Constant(nbheading,
                                                          spectrum.heading) ;
    double hstep = 10. ;
    if (nbheading > 1)
    {
        hstep = 2 * M_PI / nbheading ;
    }

    //Fill heading, evenly spaced, centered on main direction
    for (Eigen::Index ihead = 0; ihead < nbheading; ++ihead)
    {
        wif_heading(ihead) = std::fmod( spectrum.heading  + ihead * hstep , 2*M_PI) ;
    }

    int nWave = nbSeed * nbheading ;

    w_ = Eigen::ArrayXd::Zero(nWave) ;
    b_ = Eigen::ArrayXd::Zero(nWave) ;
    amp_ = Eigen::ArrayXd::Zero(nWave) ;
    phi_ = Eigen::ArrayXd::Random(nWave) * M_PI + M_PI ;
    wWidth_ = Eigen::ArrayXd::Zero(nWave) ;
    bWidth_ = Eigen::ArrayXd::Constant(nWave, hstep) ;
    isWidth_ = true ;

    // TODO Compute the range containing 85% of the energy.
    //auto [eMin, eMax] = spectrum.energyRange( 0.85, wmin ,  wmax  );
    double eMin = 0.0, eMax = 0.0 ;
    std::tie(eMin, eMax) = spectrum.energyRange(0.85, wmin, wmax) ;

    int nbSeed_nose = std::max(
        1, int(nbSeed * 0.5 * (eMin - wmin) / (wmax - wmin - (eMax - eMin)))) ;
    int nbSeed_tail = std::max(
        1, int(nbSeed * 0.5 * (wmax - eMax) / (wmax - wmin - (eMax - eMin)))) ;
    int nbSeed_peak = nbSeed - nbSeed_nose - nbSeed_tail ;

    double meanDw_nose = (eMin - wmin) / nbSeed_nose ;
    double meanDw_tail = (wmax - eMax) / nbSeed_tail ;
    double meanDw_peak = (eMax - eMin) / nbSeed_peak ;

    //Random vector for frequency shift
    Eigen::ArrayXd randomShift ;
    Eigen::ArrayXd list_freq = Eigen::ArrayXd::Zero(nbSeed + 1) ;

    int iwave = 0 ;

    double sw = 0. ;

    for (Eigen::Index ihead = 0; ihead < nbheading; ++ihead)
    {
        double spreadFactor = 1. ;
        //If spreading
        if (spectrum.getSpreadingType() != SpreadingType::No)
        {
            spreadFactor = spectrum.computeSpreading(wif_heading(ihead))
                * hstep ;
        }
        randomShift = Eigen::ArrayXd::Random(nbSeed) ;  //Between -1 and 1 !
        randomShift *= 0.475 ;

        //-------- Nose
        list_freq(0) = wmin ;
        for (Eigen::Index i = 1; i < nbSeed_nose; ++i)
        {
            list_freq(i) = wmin + i * meanDw_nose
                + randomShift(i - 1) * meanDw_nose ;
        }
        //-------- Peak
        list_freq(nbSeed_nose) = eMin ;
        for (Eigen::Index i = 1; i < nbSeed_peak; ++i)
        {
            list_freq(i + nbSeed_nose) = eMin + i * meanDw_peak
                + randomShift(i + nbSeed_nose - 1) * meanDw_peak ;
        }
        //-------- Tail
        list_freq(nbSeed_nose + nbSeed_peak) = eMax ;
        for (Eigen::Index i = 1; i < nbSeed_tail; ++i)
        {
            list_freq(i + nbSeed_nose + nbSeed_peak) = eMax + i * meanDw_tail
                + randomShift(i + nbSeed_nose + nbSeed_peak - 1) * meanDw_tail ;
        }
        list_freq(nbSeed) = wmax ;
        for (Eigen::Index i = 0; i < nbSeed; ++i)
        {
            w_(iwave) = 0.5 * (list_freq(i + 1) + list_freq(i)) ;
            wWidth_(iwave) = list_freq(i + 1) - list_freq(i) ;
            sw = spectrum.compute(w_(iwave)) * spreadFactor ;
            amp_(iwave) = pow((2 * sw * wWidth_(iwave)), 0.5) ;
            b_(iwave) = wif_heading(ihead) ;
            iwave += 1 ;
        }
    }
    initialize() ;
}

Wif::Wif(const SeaState& seaState, double wmin, double wmax,
         unsigned int nbSeed, unsigned int spreadNbHeading, unsigned int seed,
         double depth) :
    // Construct from spectrum, and use the += operator to sum over the different spectrum of the seaState
    Wif(seaState.getSpectrum(0), wmin, wmax, nbSeed, spreadNbHeading, seed, depth )
{
    for (size_t i = 1; i < seaState.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spec_i = seaState.getSpectrum(i) ;

        int newSeed = 0 ;
        if (seed != 0)
        {
            newSeed += 1 ;
        }  // Avoid using same seed for all spectra !
        Wif wifTmp = Wif(spec_i, wmin, wmax, nbSeed, spreadNbHeading, newSeed) ;
        *this += wifTmp ;
    }
}

Wif::Wif(const SeaState& seaState, const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& b, unsigned int seed,
         double depth) :
    Wif(seaState.getSpectrum(0), w, b, seed, depth)
{
    for (size_t i = 1; i < seaState.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spec_i = seaState.getSpectrum(i);
        Wif wifTmp = Wif(spec_i, w, b, seed, depth);
        *this += wifTmp;
    }
}

Eigen::ArrayXd Wif::getDensity() const
{
    if (isWidth_)
        {
        auto density = amp_.pow(2) / (2*wWidth_);
        return density;
        }
    throw std::logic_error( "Bandwidth not known for this wif object, can not compute spectral density" );
}

Wif& Wif::operator+=(  const Wif& wif )
{
    //TODO check same depth / headingType

    //Concatenates w_ , amp_ , phi_ and b_
    Eigen::ArrayXd::Index size0 = amp_.size() ;
    Eigen::ArrayXd::Index addSize = wif.amp_.size() ;
    Eigen::ArrayXd::Index newSize = size0 + addSize ;

    amp_.conservativeResize(newSize) ;
    phi_.conservativeResize(newSize) ;
    w_.conservativeResize(newSize) ;
    b_.conservativeResize(newSize) ;

    amp_.segment(size0, addSize) = wif.amp_ ;
    phi_.segment(size0, addSize) = wif.phi_ ;
    w_.segment(size0, addSize) = wif.w_ ;
    b_.segment(size0, addSize) = wif.b_ ;

    if (isWidth_ == true)
    {
        wWidth_.conservativeResize(newSize);
        bWidth_.conservativeResize(newSize);
        wWidth_.segment(size0, addSize) = wif.wWidth_;
        bWidth_.segment(size0, addSize) = wif.bWidth_;
    }

    initialize() ;
    return *this ;
}

Wif Wif::operator+(const Wif& rhs) const
{
    Wif wif(*this) ;
    wif += rhs ;
    return wif ;
}

Wif & Wif::operator*=(double scale)
{
    amp_ *= scale;
    return *this;
}

void Wif::removeZeroFrequency()
{
    Eigen::Index imin;
    auto minVal = w_.minCoeff(&imin);

    if (minVal > 1e-12) { return; }

    Eigen::Index nbfreq = w_.size() - 1;

    if (imin < nbfreq)
    {
        w_.segment(imin, nbfreq - imin) = w_.segment(imin + 1, nbfreq - imin);
        amp_.segment(imin, nbfreq - imin) = amp_.segment(imin + 1, nbfreq - imin);
        phi_.segment(imin, nbfreq - imin) = phi_.segment(imin + 1, nbfreq - imin);
        b_.segment(imin, nbfreq - imin) = b_.segment(imin + 1, nbfreq - imin);
        cb_.segment(imin, nbfreq - imin) = cb_.segment(imin + 1, nbfreq - imin);
        sb_.segment(imin, nbfreq - imin) = sb_.segment(imin + 1, nbfreq - imin);
        k_.segment(imin, nbfreq - imin) = k_.segment(imin + 1, nbfreq - imin);
    }

        if (isWidth_ == true) 
        {
            wWidth_.segment(imin, nbfreq - imin) = wWidth_.segment(imin + 1, nbfreq - imin);
            bWidth_.segment(imin, nbfreq - imin) = bWidth_.segment(imin + 1, nbfreq - imin);
        }

    w_.conservativeResize(nbfreq);
    amp_.conservativeResize(nbfreq);
    phi_.conservativeResize(nbfreq);
    b_.conservativeResize(nbfreq);
    k_.conservativeResize(nbfreq);
    
    sb_.conservativeResize(nbfreq);
    cb_.conservativeResize(nbfreq);

    if (isWidth_) 
    {
        bWidth_.conservativeResize(nbfreq);
        wWidth_.conservativeResize(nbfreq);
    }

    return;
}

void Wif::resize(const Eigen::Index &nbfreq)
{
    w_.conservativeResize(nbfreq);
    amp_.conservativeResize(nbfreq);
    phi_.conservativeResize(nbfreq);
    b_.conservativeResize(nbfreq);
    k_.conservativeResize(nbfreq);
    sb_.conservativeResize(nbfreq);
    cb_.conservativeResize(nbfreq);
    if (isWidth_)
    {
        bWidth_.conservativeResize(nbfreq);
        wWidth_.conservativeResize(nbfreq);
    }
}
void Wif::removeFreq(const Eigen::Index &ifreq, const bool toResize = true)
{
    Eigen::Index nbfreq = w_.size();
    if (nbfreq == 0)    return;
    if (ifreq < (nbfreq -1))    // if ifreq == nbfreq -1 (last element) no need to shift
    {
        Eigen::Index len = nbfreq-1 -ifreq;
        w_.segment(ifreq, len) = w_.segment(ifreq +1, len);
        amp_.segment(ifreq, len) = amp_.segment(ifreq +1, len);
        phi_.segment(ifreq, len) = phi_.segment(ifreq +1, len);
        b_.segment(ifreq, len) = b_.segment(ifreq +1, len);
        k_.segment(ifreq, len) = k_.segment(ifreq +1, len);
        sb_.segment(ifreq, len) = sb_.segment(ifreq +1, len);
        cb_.segment(ifreq, len) = cb_.segment(ifreq +1, len);
        if (isWidth_)
        {
            bWidth_.segment(ifreq, len) = bWidth_.segment(ifreq +1, len);
            wWidth_.segment(ifreq, len) = wWidth_.segment(ifreq +1, len);
        }
    }
    if (toResize)
        resize(nbfreq -1);
}

void Wif::removeZero(const double threshold = 1.e-12)
{
    Eigen::Index nbfreq = w_.size();
    for (Eigen::Index i = 0; i < nbfreq; ++i)
        if (abs(amp_(i)) < threshold)
        {
            removeFreq(i--, false);
            nbfreq--;
        }
    if (nbfreq != w_.size())
        resize(nbfreq);
}

Eigen::ArrayXd Wif::getEncounterFrequencies(double speed) const
{
    auto encFreq = w2we(w_, b_, speed, k_, depth_) ;
    return encFreq ;
}

Wifm::Wifm(const std::vector<std::shared_ptr<Wif> > & wifs, const Eigen::MatrixX2d & bounds):
    bounds_(bounds)
{
    nWifs_ = wifs.size() ;
    std::size_t nBounds(bounds.rows()) ;
    if (nWifs_ != nBounds)
    {
        throw std::invalid_argument("Wif vector size and bounds rows are not coherent") ;
    }
    wifs_.clear() ;
    for (std::size_t index=0; index < nWifs_; index++)
    {
        wifs_.push_back(BoundedWif(wifs[index], bounds.row(index))) ;
    }
}

std::size_t Wifm::getWifIndex(const double & time) const
{
    bool found(false) ;
    std::size_t is ;
    std::size_t ie ;
    std::size_t im ;
    is = 0 ;
    ie = nWifs_ ;
    while(!found && ((ie - is) > 1))
    {
        im = (is + ie) / 2 ;
        levels_ level = wifs_[im].isTimeInBounds(time) ;
        found = (level == levels_::EQUAL) ;
        if(level == levels_::LOWER)
        {
            ie = im ;
        }
        else
        {
            is = im ;
        }
    }
    return std::min(is, nWifs_-1) ; ;
}

std::shared_ptr<Wif> Wifm::getWif(const double & time)
{
    return wifs_[getWifIndex(time)].getWif() ;
}

std::shared_ptr<Wif> Wifm::getWifAtIndex(const std::size_t & index)
{
    return wifs_[index].getWif();
}

const Eigen::MatrixX2d & Wifm::getBounds() const
{
    return bounds_ ;
}

void BV::Spectral::Wifm::setHeadings(double heading)
{
    for (std::size_t index = 0; index < nWifs_; index++)
    {
        wifs_[index].getWif()->setHeadings(heading);
    }
}
