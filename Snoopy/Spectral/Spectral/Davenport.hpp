#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class Davenport
 *
 * @brief Define a Davenport wind spectrum.
 */
class SPECTRAL_API Davenport: public Spectrum
{
public:

    Davenport(double meanVelocity, double C10, double L, double heading=0.) :
        Spectrum("Davenport", heading, meanVelocity), C10(C10), L(L)
    {
    }

    // k Surface drag coefficient
    double C10 ;

    //Representative length scale
    double L ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 3 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "representativeLengthScale"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C10, L} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.0044, 1200.} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1e5} ;}

} ;

}
}
