#pragma once

#include "WaveSpectrum.hpp"

#include "SpectralExport.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class Tabulated
 *
 * @brief Define a tabulated spectrum e.g. a spectrum defined by
 * a set of frequencies and their corresponding densities.
 */
class SPECTRAL_API WaveTabulatedSpectrum : public WaveSpectrum
{
public:

    WaveTabulatedSpectrum(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& sw, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : WaveSpectrum("WaveTabulatedSpectrum", heading, spreadingType, spreadingValue)
        , w(w), sw(sw)
    {
		tailOrder_ = -5;
	}

    // Frequencies
    Eigen::ArrayXd w;

    // Spectrum density.
    Eigen::ArrayXd sw;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    virtual double getTp() const override;

    virtual std::tuple<double, double> get_wrange(double energyRatio = 0.99) const override;

};

}
}
