#pragma once
#include <math.h>
#include "SpectralExport.hpp"
#include <Eigen/Dense>

namespace BV
{
namespace Spectral
{


// w => k
SPECTRAL_API double w2k(double w, double depth = 0.  );
SPECTRAL_API Eigen::ArrayXd w2k( const Eigen::ArrayXd& w, double depth = 0.);

// k => w
SPECTRAL_API double k2w(double k, double depth = 0.  );
SPECTRAL_API Eigen::ArrayXd k2w( const Eigen::ArrayXd& k, double depth = 0.);


// w => lambda

//..


/**
 *    Encounter frequency stuff
 */
//scalar version
SPECTRAL_API double w2we(double w, double heading, double speed, double depth = -1);
//Vectorized version
SPECTRAL_API Eigen::ArrayXd w2we( const Eigen::ArrayXd& w, double heading, double speed, double depth = -1 );
//Vectorized version
SPECTRAL_API Eigen::ArrayXd w2we( const Eigen::ArrayXd& w, const Eigen::ArrayXd& heading, double speed, double depth = -1 );
//Vectorized version
SPECTRAL_API Eigen::ArrayXd w2we(double w, const Eigen::ArrayXd& heading, const Eigen::ArrayXd& speed, double depth = -1);
//Slight optimization in case k is already computed
SPECTRAL_API Eigen::ArrayXd w2we(const Eigen::ArrayXd& w, const Eigen::ArrayXd& heading, double speed, const Eigen::ArrayXd& k, double depth = -1);
SPECTRAL_API double k2Cp(double k , double depth, double gravity) ;
SPECTRAL_API double w2Cg(double w , double depth, double gravity) ;
SPECTRAL_API double k2Cg(double k , double depth, double gravity) ;

}
}
