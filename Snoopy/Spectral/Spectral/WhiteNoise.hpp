#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class WhiteNoise
 *
 * @brief Create a white noise along given frequencies.
 */
class SPECTRAL_API WhiteNoise : public ParametricSpectrum
{
public:

    WhiteNoise(double hs, double w1, double w2, double w3, double w4,
               double heading=0., SpreadingType spreadingType=SpreadingType::No,
               double spreadingValue=0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
          , hs_(hs), w1_(w1), w2_(w2), w3_(w3), w4_(w4)
    {}

    static const char* name;

    // Significant wave height.
    double hs_;

    // Parameters
    double w1_;
    double w2_;
    double w3_;
    double w4_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    static int getNParams() { return 5; }
    static std::vector<const char*> getCoefs_name() { return{ "hs", "w1" , "w2" , "w3" , "w4"  }; }
    std::vector<double> getCoefs() const override   { return{  hs_,  w1_ ,  w2_ ,  w3_ ,  w4_  }; }
    static std::vector<double> getCoefs_0()         { return{  1. ,  0.4 ,  0.8 ,  1.2 ,  1.5  }; }
    static std::vector<double> getCoefs_min()       { return{  0. ,  0.01,  0.01,  0.01,  0.01 }; }
    static std::vector<double> getCoefs_max()       { return{  30.,  10. ,  10. ,  10. ,  10.  }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<WhiteNoise>();
    }

    std::tuple<double, double> get_wrange(double energyRatio = -1) const override;

};

}
}
