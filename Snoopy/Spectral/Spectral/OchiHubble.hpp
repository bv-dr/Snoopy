#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class OchiHubble
 *
 * @brief Define an Ochi-Hubble spectrum as a sum of two Wallop spectrums with
 * q = 4 and m = 4 * lambda + 1.
 */
class SPECTRAL_API OchiHubble : public ParametricSpectrum
{
public:

    OchiHubble(double hs1, double tp1, double lambda1, double hs2, double tp2, double lambda2, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs1_(hs1), tp1_(tp1), lambda1_(lambda1), hs2_(hs2), tp2_(tp2), lambda2_(lambda2)
    {
      tailOrder_ = std::max( -(4*lambda1+1), -(4 * lambda2 + 1));
    }

    static const char* name;

    // First significant wave height.
    double hs1_;

    // tp1 First peak period.
    double tp1_;

    // First shape parameter.
    double lambda1_;

    // Second significant wave height.
    double hs2_;

    // Second peak period.
    double tp2_;

    // Second shape parameter.
    double lambda2_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    static int getNParams() { return 6; }
    static std::vector<const char*> getCoefs_name() { return { "hs1", "tp1" , "lambda1", "hs2", "tp2" , "lambda2" }; }
    std::vector<double> getCoefs() const override   { return {  hs1_,  tp1_ ,  lambda1_,  hs2_,  tp2_ ,  lambda2_ }; }
    static std::vector<double> getCoefs_0()         { return {  1. ,   10. ,   1.      ,  1. ,   10. ,   2.       }; }
    static std::vector<double> getCoefs_min()       { return {  0. ,   0.01,   0.01    ,  0. ,   0.01,   0.01     }; }
    static std::vector<double> getCoefs_max()       { return {  30.,   60. ,   30.     ,  30.,   60. ,   30.      }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<OchiHubble>();
    }

};

}
}
