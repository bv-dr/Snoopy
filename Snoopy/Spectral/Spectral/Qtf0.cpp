#include "Qtf0.hpp"

using namespace BV::Spectral ;
using namespace BV::Math ;

Qtf0::Qtf0(const Eigen::Ref<const Eigen::ArrayXd>& b,
           const Eigen::Ref<const Eigen::ArrayXd>& w,
           const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
           const ModesType & modes, const Eigen::Tensor<double, 3> & data,
           const Eigen::Ref<const Eigen::Vector3d>& refPoint,
           const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
           double forwardSpeed,
           double depth) :
    HydroTransferFunction<Storage>(
                         Storage({b, w, modeCoefficients}, data),
                         modes, refPoint, waveRefPoint, forwardSpeed, depth)
{
}

Qtf0::Qtf0(const Eigen::Ref<const Eigen::ArrayXd>& b,
           const Eigen::Ref<const Eigen::ArrayXd>& w,
           const Eigen::Tensor<double, 3> & data,
           const Eigen::Ref<const Eigen::Vector3d>& refPoint,
           const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
           double forwardSpeed,
           double depth) :
    Qtf0(b, w, Eigen::ArrayXd::Zero(data.dimension(2)),
         ModesType::Constant(data.dimension(2), Modes::NONE), data, refPoint,
         waveRefPoint, forwardSpeed, depth)
//To add enum for Qtf0 type (Pressure, loads, motion... ). This allows easy scaling
{
}

Qtf0::Qtf0(const Eigen::Ref<const Eigen::ArrayXd>& b,
           const Eigen::Ref<const Eigen::ArrayXd>& w, const ModesType & modes,
           const Eigen::TensorRef<const Eigen::Tensor<double, 3> > & data,
           const Eigen::Ref<const Eigen::Vector3d>& refPoint,
           const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
           double forwardSpeed,
           double depth) :
    Qtf0(b, w, Eigen::ArrayXd::Zero(data.dimension(2)), modes, data, refPoint,
         waveRefPoint, forwardSpeed, depth)
//To add enum for Qtf0 type (Pressure, loads, motion... ). This allows easy scaling
{
}

Eigen::ArrayXXd Qtf0::getAtHeadings(
    const Eigen::ArrayXd & relativeHeadings,
    const std::vector<unsigned> & indices,
    const BV::Math::Interpolators::InterpScheme & interpScheme,
    BV::Math::Interpolators::ExtrapolationType extrapType) const
{
    using IndexType = Eigen::Tensor<double, 3>::Index ;
    IndexType iHeading(0) ;
    std::vector<unsigned>::const_iterator nextIndex = indices.begin() ;
    IndexType nModes(getNModes()) ;
    IndexType nFreqs(getNFrequencies()) ;
    Eigen::ArrayXXd result(nFreqs, nModes) ;
    Eigen::DSizes<IndexType, 3> offsets { 0, 0, 0 } ;
    Eigen::DSizes<IndexType, 3> extents { getNHeadings(), 1, nModes } ;
    Eigen::Tensor<double, 3> tmp ;
    Eigen::ArrayXd relativeHeading(1) ;
    IndexType iFreq(0) ;
    bool isEnd(nextIndex == indices.end()) ;
    if (relativeHeadings.size() == 1)
    {
        // Easy optimization when there is only one heading
        relativeHeading(0) = relativeHeadings(0) ;
        tmp = this->getData(0, relativeHeading, interpScheme, extrapType) ;
        isEnd = true ;
    }
    for (IndexType iComponent = 0; iComponent < nFreqs; ++iComponent)
    {
        if ((!isEnd) && ((*nextIndex) == iComponent))
        {
            ++nextIndex ;
            isEnd = nextIndex != indices.end() ;
            relativeHeading(0) = relativeHeadings(++iHeading) ;
            iFreq = 0 ;
            offsets[1] = iComponent ;
            if (isEnd)
            {
                extents[1] = static_cast<IndexType>(*nextIndex) - iComponent ;
            }
            else
            {
                extents[1] = nFreqs - iComponent ;
            }
            tmp = this->getData(0, relativeHeading, offsets, extents, interpScheme, extrapType) ;
        }
        for (IndexType iMode = 0; iMode < nModes; ++iMode)
        {
            result(iComponent, iMode) = tmp(0, iFreq, iMode) ;
        }
        ++iFreq ;
    }
    return result ;
}

Qtf0 Qtf0::getQtfAtFrequencies(
    const Eigen::ArrayXd & ws,
    const Interpolators::InterpScheme & interpScheme,
    BV::Math::Interpolators::ExtrapolationType extrapType) const
{
    Eigen::Tensor<double, 3> dataInterp(this->getData(1, ws, interpScheme, extrapType)) ;
    return Qtf0(axes_[0], ws, axes_[2], modes_, dataInterp, refPoint_,
                waveRefPoint_, forwardSpeed_, depth_) ;
}
