#include "SpectralMoments.hpp"
#include <list>
#include <fstream>

using namespace BV::Spectral;

SpectralMoments::SpectralMoments(const std::vector<std::shared_ptr<SeaState>>& seaStates,
                                 const Rao& rao,
                                 const bool& isM4,
                                 const double& dw,
                                 const double& db,
                                 const double& w_min,
                                 const double& w_max,
                                 const int& num_threads,
                                 const double& g) :
    rao_(rao.getFreqExtRao(w_min, w_max)),
    num_threads_(num_threads),
    isRaoShort_(rao_.getHeadingIn2PiRange()[rao_.getHeadings().size() -1] < M_PI +1.e-8),
    isComputed_(false),
    dw_(dw),
    isM4_(isM4),
    maxMoment_(isM4 ? 4 : 2),
    maxMoment2_(maxMoment_/2),
    db_(db*M_PI/180.),
    g_(g)
{
    m0_ = Eigen::ArrayXXd::Zero(seaStates.size(), rao_.getNModes());
    m2_ = Eigen::ArrayXXd::Zero(seaStates.size(), rao_.getNModes());
    m4_ = Eigen::ArrayXXd::Zero(seaStates.size(), rao_.getNModes());
    //auto complexData_(rao_.getComplexData());
    //auto nbhead(rao_.getHeadings().size());
    //auto nbfreq(rao_.getFrequencies().size());
    //auto nbmode(rao_.getModes().size());
    // Copy the sea states to the class's attribute seaStates:
    for (auto& seaState : seaStates)
    {
        seaStates_.push_back(seaState);
    }

    spdlog::debug("Number of threads is {}", num_threads_);
    spdlog::debug("is RAO defined from 0° to 180°: {}", (isRaoShort_? "true" : "false"));
}

/*
 * Help function to calculate |RAO(w)|^2 using the coeffs2
 *
 * return value: |RAO(w; headings, modes)|^2
 * uncomment this function, if you need it to use.
 *
 * It is commented to avoid a warning that function is defined, but not used
 */
/*
Eigen::ArrayXXd static getRao2(Eigen::Tensor<double, 4>& coeffs2, const double& w, const Eigen::Index& interval, const Eigen::Index& degree)
{
    Eigen::ArrayXXd mod2(Eigen::ArrayXXd::Zero(coeffs2.dimension(0), coeffs2.dimension(3)));
    double wn(1.);
    for (Eigen::Index pw = 0; pw < degree + degree + 1; pw++)
    {
        for (Eigen::Index ihead = 0; ihead < coeffs2.dimension(0); ihead++)
        {
            for (Eigen::Index mode = 0; mode < coeffs2.dimension(3); mode++)
            {
                mod2(ihead, mode) += coeffs2(ihead, interval, pw, mode) *wn;
            }
        }
        wn *= w;
    }
    return mod2;
}
*/

void SpectralMoments::compute_()
{
    // Collecting all headings used in seaStates in order to approximate rao
    // Assuming that headings in spectra are in range [0, 2\pi)
    std::list<double> all_possible_headings;
    bool isAnySpreadingTypeNo = false;      // if true => at least 1 spectrum has SpreadingType SpreadingType::No
    bool isAnySpreadingType   = false;      // if true => at least 1 spectrum has SpreadingType different from SpreadingType::No
//#pragma omp parallel for num_threads(num_threads_)    does not work due to the race conditions: all_possible_headings can be modified by many threadings simultaneously
// as for isAnySpreadingTypeNo or isAnySpreadingType we do not care if it is modified by 1 or many thread simultaneously, the result will be true
    //for (const std::shared_ptr<SeaStateABC>& seaState : seaStates_)
    for (Eigen::Index seaStateIndex = 0; seaStateIndex < (Eigen::Index)seaStates_.size(); seaStateIndex ++)
    {
        const std::shared_ptr<SeaStateABC>& seaState(seaStates_[seaStateIndex]);
        for (Eigen::Index s = 0; s < (Eigen::Index)seaState->getSpectrumCount(); ++s)
        {
            auto& spectrum(seaState->getSpectrum(s));
            if (spectrum.getSpreadingType() == SpreadingType::No)
            {
                double heading(spectrum.getHeading());
                //while (heading < 0.0) heading += 2 * M_PI;
                //while (heading >= 2 * M_PI) heading -= 2 * M_PI;
                if (isRaoShort_)
                {
                    if (heading > M_PI +1.e-8)
                    {
                        double heading_old(heading);
                        heading = 2*M_PI -heading;
                        spdlog::warn("Rao is given in range 0° to 180°. And spectrum heading is greater than 180°");
                        spdlog::warn("    In this case Rao is assumed to be symmetric");
                        spdlog::warn("    {:>5.2f} => {:>5.2f}", heading_old *180./M_PI, heading *180./M_PI);
                    }
                }
                all_possible_headings.push_back(heading);                               // omp shared variable, so every thread will add heading
                isAnySpreadingTypeNo = true;                                            // omp shared variable
            }
            else
            {
                isAnySpreadingType = true;                                              // omp shared variable
            }
        }
    }   // end of seaStates_ for omp parallelization
    spdlog::debug("is any spectrum WITH spreading : {}", (isAnySpreadingType ? "true" : "false"));
    spdlog::debug("is any spectrum WITHOUT spreading : {}", (isAnySpreadingTypeNo ? "true" : "false"));
    // Sort them
    all_possible_headings.sort();
    // Make unique headings with tolerance db_
    //all_possible_headings.unique([db_ = db_](double x, double y) {return std::abs(x - y) < db_; });   // >= c++14
    all_possible_headings.unique([db_ = db_](double x, double y) {return std::abs(x - y) < db_ -1.e-10; });   // >= c++14
    //all_possible_headings.unique([this](double x, double y) {return std::abs(x - y) < db_; });      // < c++14
    // Creating a copy of the headings in Eigen::ArrayXd
    all_headings_.resize(all_possible_headings.size());
    Eigen::Index head(0);
    for (std::list<double>::iterator it = all_possible_headings.begin(); it != all_possible_headings.end(); ++it)
    {
        all_headings_(head++) = *it;
    }
    spdlog::debug("Rao headings:");
    for (Eigen::Index head = 0; head < rao_.getHeadings().size(); head ++)
        spdlog::debug("    {:>4d} : {:>11.4f} = {:>5.2f}°", head, rao_.getHeadings()(head), rao_.getHeadings()(head) *180./M_PI);
    spdlog::debug("Spectra Unique headings:");
    for (Eigen::Index  head = 0; head < all_headings_.size(); head ++)
        spdlog::debug("    {:>4d} : {:>11.4f} = {:>5.2f}°", head, all_headings_(head), all_headings_(head) *180./M_PI);

    // |RAO(w)|^2 is interpolated by linear interpolation
    const Eigen::Index degree(1);
    // we need only 0, 2 and 4th (not always) moments
    const int nbHead(rao_.getHeadings().size());
    //const int nbFreq(rao_.getFrequencies().size());
    const double forwardSpeed(rao_.getForwardSpeed());
    const Eigen::ArrayXd w(rao_.getFrequencies());
    // Generating the frequencies range, at which calculate the unit moments
    //const Eigen::Tensor<double, 4>& coeffs(rao_.getRao2LinearCoefficientsForOmega());
    Eigen::Tensor<double, 4> coeffs;    // coeffs for spectrum SpreadingType::No
    Eigen::Tensor<double, 5> coeffsSpr; // coeffs for spectrum different from SpreadingType::No
    if (isAnySpreadingTypeNo)
    {
        // coeffs [nbHead, nbFreq -1, w_pow : 2*degree +1, nbMode]
        coeffs = rao_.getRao2LinearCoefficientsForOmegaAtBeta(all_headings_);
    }
    int nZero(0);
    int nN1(0);
    bool isSymmetric(false);
    if (isAnySpreadingType) {
        /*
         * nZero : == 1 if an interval added from 0 to beta[0]
         * nN1   : == 1 if an interval added from beta[N] to 2pi
         * headExtrapolation : 0 => from beta[N] to beta[0] +2pi == 0
         *                     1 => 2pi cyclic interpolation
         *                     2 => headings are from 0 to pi and symmetry w.r.t. the point \pi condition is used for the interpolation
         */
        // coeffsSpr [nbHead -1 +nZero +nN1, nbFreq -1, w_pow : 2*degree +1, b_pow : 2*degree +1, nbMode ]
        isSymmetric = isRaoShort_;
        if (isSymmetric)
        {
            //coeffsSpr = rao_.getRao2LinearCoefficients(&nZero, &nN1, 2);
            coeffsSpr = rao_.getRao2LinearCoefficientsSymmetric(&nZero, &nN1);
        }
        else
        {
            coeffsSpr = rao_.getRao2LinearCoefficients(&nZero, &nN1, 1);
        }
    }

#pragma omp parallel for num_threads(num_threads_)
    // no need to reduct m0_, m2_ and m4_ here, because the loop to be parallelise has index ss
    // and thus, each thread will write to independent from others position of m0_(ss, mode) for example
    // mode is internal loop
    for (Eigen::Index ss = 0; ss < (Eigen::Index)seaStates_.size(); ss ++)
    {
        spdlog::debug("SeaState index : {:>6d}", ss);
        const std::shared_ptr<SeaStateABC> seaState = seaStates_[ss];
        const int nmod(seaState->getSpectrumCount());
        for (Eigen::Index s = 0; s < nmod; ++s)
        {
            spdlog::debug("Spectrum index : {:>6d}", s);
            const WaveSpectrum& spectrum(seaState->getSpectrum(s));
            /*
             * calculating the unitMoments with the following parameters :
             * w = rao_.getFrequencies(),
             * maxMoment = 2*maxMoment +2*degree (2n +2m), where n is the maximum moment desired, m is the order of the |RAO(w)| interpolation polynom
             * dw = dw_
             */
            const Eigen::ArrayXXd& unitMoments(spectrum.integrate_partial_moments(rao_.getFrequencies(), maxMoment_+maxMoment_ +degree +degree, dw_));  // rao_,  maxMoment_, degree & w_ are omp shared variables

            if (spectrum.getSpreadingType() == SpreadingType::No)
            {
                spdlog::debug("Spectrum WITHOUT spreading");
                spdlog::debug("         heading : {:>11.5f} = {:>5.2f}°", spectrum.heading, spectrum.heading *180./M_PI);
            // Get closest heading
                Eigen::ArrayXd::Index ihead ;
                //Eigen::abs((  rao_.getHeadings() - spectrum.heading  )).minCoeff ( &ihead );
                //const double heading(rao_.getHeadings()[ihead]);
                //Eigen::abs((  all_headings_ - spectrum.heading  )).minCoeff ( &ihead );
                /*
                 * Replace the Eigen minCoeff to find the closest heading by a loop:
                 * the reason is: we know, that the spectrum.heading is 1 of the all_headings_ list (with db_ tolerance),
                 *
                 * all_headings_ is sorted array => searching the first element for which 
                 * spectrum.heading is less
                 */
                double heading(spectrum.heading);
                if (isRaoShort_ && heading > M_PI +1.e-8)
                    heading = 2*M_PI -heading;
                for (ihead = 0; ihead < all_headings_.size() -1; ++ihead)
                {
                    if (heading < all_headings_[ihead +1])
                        break;
                }
                Eigen::ArrayXd::Index ihead_(ihead+1);
                /*
                 * In a case all_headings_[-1] == 2pi => ihead != all_headings_.size() -1
                 * otherwise if ihead == all_headings_.size() -1 (last element)
                 * all_headings_[-1] < spectrum.heading < all_headings[0]
                 */
                if (ihead == all_headings_.size() -1)
                    ihead_ = 0;
                spdlog::debug("         {:>5.2f}° <= {:>5.2f}°", all_headings_[ihead] *180./M_PI, (isRaoShort_ && spectrum.heading > M_PI +1.e-8 ? (2*M_PI-spectrum.heading)*180./M_PI : spectrum.heading *180./M_PI));
                /*
                 * now spectrum heading is between 2 headings
                 * all_headings_[ihead] < spectrum.heading < all_headings_[ihead +1]
                 *
                 * we need to chose the closest boundary
                 */
                if (std::abs(all_headings_[ihead] - heading) >
                        std::abs(all_headings_[ihead_] -heading))
                {
                        ihead = ihead_;
                }
                heading = all_headings_[ihead];
                spdlog::debug("         calculation heading : {:>5.2f}°", heading *180./M_PI);

                double fcbsg(forwardSpeed * std::cos(heading) / g_);                            // forwardSpeed is omp shared variable
                bool isForwardSpeed(std::fabs(fcbsg) > 1.0e-8);
                Eigen::ArrayXd Cl;
                // for zero speed case or \cos\beta == 0 (w_e == w)
                // we do not use the Cl coefficients
                if (isForwardSpeed) // forward speed case with \cos \beta != 0
                {
                    spdlog::debug("forward speed is not zero! U*cos(beta)/g = {}", fcbsg);
                    Cl.resize(maxMoment_ + 1);
                    Cl(0) = 1.;
                    // (-U cos(beta)/g) ^l for l = 0, 1, .., maxMoment
                    for (Eigen::Index k = 1; k < maxMoment_ + 1; k++)
                    {
                        Cl(k) = (-fcbsg) * Cl(k - 1);
                    }
                }
                for (Eigen::Index i = 0; i < unitMoments.rows(); ++i)       // intervals [w_i, w_i+1)
                {
                    for (Eigen::Index k = 0; k < degree +degree+1; ++k)     // interpolation polynom squared (\sum_{k=0}^{degree} a_k x^k)^2 = \sum_{k=0}^{2*degree} \tilde{a}_k x^k
                    {
                        // mode independent parts
                        double m0_tmp_;
                        double m2_tmp_;
                        double m4_tmp_;
                        m0_tmp_ = unitMoments(i, k);
                        m2_tmp_ = unitMoments(i, 2 + k);
                        if (isM4_)                                                              // isM4 is omp shared variable
                            m4_tmp_ = unitMoments(i, 4 + k);
                        if (isForwardSpeed)
                        {
                            m2_tmp_ += 2. * Cl(1) * unitMoments(i, 2 + k + 1)
                                     +      Cl(2) * unitMoments(i, 2 + k + 2);
                            if(isM4_)
                            {
                                m4_tmp_ += 4. * Cl(1) * unitMoments(i, 4 + k + 1)
                                         + 6. * Cl(2) * unitMoments(i, 4 + k + 2)
                                         + 4. * Cl(3) * unitMoments(i, 4 + k + 3)
                                         +      Cl(4) * unitMoments(i, 4 + k + 4);
                            }
                        }
                        for (Eigen::Index mode = 0; mode < rao_.getNModes(); ++mode)            // rao_ is omp shared variable
                        {
                            m0_(ss, mode) += coeffs(ihead, i, k, mode) * m0_tmp_;               // coeffs
                            m2_(ss, mode) += coeffs(ihead, i, k, mode) * m2_tmp_;               // &
                            if (isM4_)                                                          // isM4_
                                m4_(ss, mode) += coeffs(ihead, i, k, mode) * m4_tmp_;           // are omp shared variable
                        }
                    }
                }
            }
            else
            {
                spdlog::debug("Spectrum WITH spreading");
                if (isRaoShort_)
                {
                    spdlog::warn("Rao is given in range 0° to 180°.");
                    spdlog::warn("    In this case Rao is assumed to be symmetric");
                }
                /*
                 * calculating the integrals of the spreading functions:
                 * b = rao_.getHeadings(),
                 * k   : 2*degree +1
                 *       RAO   = \sum_{i=0}^{m/2}\sum_{j=0}^{degree} c_{ij} \omega^i \beta^j
                 *    => RAO^2 = \sum_{i=0}^{m}  \sum_{j=0}^{2degree} C_{ij} \omega^i \beta^j
                 * n = maxMoment
                 * db = db_
                 */
                auto b(rao_.getHeadingIn2PiRange());                                            // rao_ is omp shared variable
                Eigen::Tensor<double, 3> sIntegrals;
                if (!isSymmetric)                                                               // isSymmetric is omp shared variable
                {
                    // sIntegrals [nbHead -1 +nZero +nN1, 0:k, 0:n ]
                    sIntegrals = spectrum.spreading->integrate_spreading(b, coeffsSpr.dimension(3), maxMoment_, db_);   // coeffsSpr, maxMoment_ & db_ are omp shared variable
                }
                else
                {
                    // in symmetric rao case we need extend b:
                    //Eigen::Index nbHead = 10;
                    //Eigen::ArrayXd b(Eigen::ArrayXd::LinSpaced(nbHead, 0.1, M_PI-0.1)); nZero = 1; nN1 = 1;
                    //Eigen::ArrayXd b(Eigen::ArrayXd::LinSpaced(nbHead, 0.1, M_PI)); nZero = 1; nN1 = 0;
                    //Eigen::ArrayXd b(Eigen::ArrayXd::LinSpaced(nbHead, 0., M_PI-0.1)); nZero = 0; nN1 = 1;
                    //Eigen::ArrayXd b(Eigen::ArrayXd::LinSpaced(nbHead, 0., M_PI)); nZero = 0; nN1 = 0;
                    Eigen::ArrayXd b_ext;
                    // size of b_ext = 2*nZero +2*nN1 +2*nbHead -1
                    if (1.e-8 < b[0] && b[nbHead -1 ] < M_PI-1.e-8)     // 0 < b_0 < b_1 < ... < b_n-1 < pi => b_ext = 0, b_0, b_1, ..., b_n-1, pi, 2pi-b_n-1, 2pi-b_n-2, ..., 2pi-b_0, 2pi
                    {
                        b_ext.resize(2*nbHead +3);
                        b_ext[0] = 0.0;
                        for(Eigen::Index ib = 0; ib < nbHead; ib ++)
                        {
                            b_ext[ib+1] = b[ib];
                        }
                        b_ext[nbHead +1] = M_PI;
                        for(Eigen::Index ib = 0; ib < nbHead; ib++)
                        {
                            b_ext[nbHead +2 +ib] = 2*M_PI -b[nbHead -1 -ib];
                        }
                        b_ext[nbHead +nbHead +2] = 2*M_PI;
                    }
                    else if (1.e-8 < b[0])                              // 0 < b_0 < b_1 < ... < b_n-1 == pi => b_ext = 0, b_0, b_1, ..., b_n-1, 2pi-b_n-2, 2pi-b_n-3, ..., 2pi-b_0, 2pi
                    {
                        b_ext.resize(2*nbHead +1);
                        b_ext[0] = 0.0;
                        for (Eigen::Index ib = 0; ib < nbHead; ib++)
                        {
                            b_ext[ib +1] = b[ib];
                        }
                        for (Eigen::Index ib = 0; ib < nbHead - 1; ib++)
                        {
                            b_ext[nbHead +1 +ib] = 2*M_PI -b[nbHead-2-ib];
                        }
                        b_ext[nbHead +nbHead] = 2*M_PI;
                    }
                    else if (b[nbHead - 1] < M_PI - 1.e-8)             // 0 = b_0 < b_1 < .. < b_n-1 < pi => b_ext = b_0, b_1, ..., b_n-1, pi, 2pi-b_n-1, 2pi-b_n-2, ..., 2pi-b_0, 2pi
                    {
                        b_ext.resize(2*nbHead +1);
                        for (Eigen::Index ib = 0; ib < nbHead; ib++)
                        {
                            b_ext[ib] = b[ib];
                        }
                        b_ext[nbHead] = M_PI;
                        for (Eigen::Index ib = 0; ib < nbHead; ib++)
                        {
                            b_ext[nbHead +1 +ib] = 2*M_PI -b[nbHead -1 -ib];
                        }
                    }
                    else                                                // 0 = b_0 < b_1 < .. < b_n-1 = pi => b_ext = b_0, b_1, ..., b_n-1, 2pi-b_n-2, 2pi-b_n-3, ..., 2pi -b_0, 2pi
                    {
                        b_ext.resize(2*nbHead -1);
                        for (Eigen::Index ib = 0; ib < nbHead; ib++)
                        {
                            b_ext[ib]= b[ib];
                        }
                        for (Eigen::Index ib = 0; ib < nbHead -1; ib++)
                        {
                            b_ext[nbHead +ib] = 2*M_PI -b[nbHead -2 -ib];
                        }
                    }
                    // b_ext [2nbHead -1 +2nZero +2nN1]
                    // sIntegrals_temp [2nbHead -2 +2nZero +2nN1, 0:k, 0:n ]
                    Eigen::Tensor<double, 3> sIntegrals_temp(spectrum.spreading->integrate_spreading(b_ext, coeffsSpr.dimension(3), maxMoment_, db_, true));    // coeffsSpr, maxMoment_ & db_ are omp shared variables
                    /*
                     * Let N = 2(nbHead +nZero +nN1 -1) +1 == b_ext.size()
                     * sIntegrals.dimension(0) = N -1 ( == 2(nbHead +nZero +nN1 -1) )
                     * then if 
                     *        b in [b_{n}, b_{n+1}) then 
                     *   2\pi-b in [b_{N-1-n -1}, b_{N-1-n}]
                     * for n = 0, 1, 2, 3, ..., (nbHead +nZero +nN1 -1) -1;
                     * 0                                : 2(nbHead +nZero +nN1 -1) -1
                     * 1                                : 2(nbHead +nZero +nN1 -1) -2
                     * n                                : 2(nbHead +nZero +nN1 -1) -1 -n
                     * nbHead +nZero +nN1 -2            : 2(nbHead +nZero +nN1 -1) -1 -nbHead -nZero -nN1 +2 = nbHead +nZero +nN1 -1
                     */
                    sIntegrals = Eigen::Tensor<double,3>(nbHead +nZero +nN1 -1, sIntegrals_temp.dimension(1), sIntegrals_temp.dimension(2));        // nbHead, nZero, nN1 are omp shared variables
                    Eigen::Index temp_val(nbHead +nZero +nN1 -1);
                    Eigen::Index temp_val2(temp_val +temp_val);  // N -1
                    for(Eigen::Index ib = 0; ib < temp_val; ib++)
                    {
                        for(Eigen::Index ik = 0; ik < sIntegrals.dimension(1); ik ++)
                        {
                            for (Eigen::Index in = 0; in < sIntegrals.dimension(2); in ++)
                            {
                                sIntegrals(ib, ik, in) = sIntegrals_temp(ib, ik, in) +sIntegrals_temp(temp_val2 -1 -ib, ik, in);
                            }
                        }
                    }
                }
//#define DEBUG_OUT
#ifdef DEBUG_OUT
    // ATTENTION: it is not recomended to use this debug part with OpenMP > 1 threads!!!
                std::cout << "Spectrum with spreading\n";
                std::cout << "    nZero = " << nZero << "\n";
                std::cout << "    nN1   = " << nN1 << "\n";
                std::cout << "    isForwardSpeed = " << (forwardSpeed / g_ > 1.0e-8 ? "true" : "false") << "\n";
                std::cout << "    sIntegrals dimensions: nbHead = " << sIntegrals.dimension(0) << " x k = " << sIntegrals.dimension(1)
                    << " x n = " << sIntegrals.dimension(2) << "\n";
                std::cout << "    unitMoments dimensions: nbFreq = " << unitMoments.rows() << " x 2 *n +2*d = " << unitMoments.cols() << "\n";
                std::cout << "    coeffsSpr dimensions: nbHead +nZero +nN1 = " << coeffsSpr.dimension(0) << " x nbFreq = " << coeffsSpr.dimension(1)
                    << " x k = " << coeffsSpr.dimension(2) << " x n = " << coeffsSpr.dimension(3) << " x nbMode = " << coeffsSpr.dimension(4) << "\n";
                std::cout << "    nbFreq from frequencies  = " << rao_.getFrequencies().size() << "\n";
                std::cout << "    nbHead from headings     = " << rao_.getHeadings().size() << "\n";
                std::cout << rao_.getHeadings() << "\n";
                std::cout << "    nbHead from 2pi headings = " << rao_.getHeadingIn2PiRange().size() << "\n";
                std::cout << rao_.getHeadingIn2PiRange() << "\n";
                //throw std::logic_error("Spectrum moments are not calculated yet for the spectra with spreading");
#endif
                Eigen::ArrayXd Cl;                      // Cl(k) = (U/g)^k
                double Ug(forwardSpeed / g_);           // Ug == U/g                    // forwardSpeed is omp shared variable
                bool isForwardSpeed(Ug > 1.0e-8);
                if (isForwardSpeed) {
                    Cl.resize(maxMoment_ +1);                                           // maxMoment_ is omp shared variable
                    Cl(0) = 1.0;
                    for(Eigen::Index k = 1; k < maxMoment_ +1; k++)
                    {
                        Cl(k) = -Cl(k-1) *Ug;
                    }
                }
//#define TEST_COEFFSSPR
#ifdef TEST_COEFFSSPR
    // ATTENTION: must not be used with OpenMP > 1 threads!!!
                // Test of the interpolation coefficients
                // Original 
                rao_.Rao2ToFile("rao_base.dat");                                        // rao_ is omp shared variable
                // Interpolated
                rao_.Rao2ToFile("rao_int.dat", coeffsSpr, nZero, nN1);                  // rao_, coeffsSpr, nZero, nN1 are omp shared variables
#endif
                int nbHead_(nZero +nN1 +nbHead -1);
                for(Eigen::Index iw = 0; iw < unitMoments.rows(); iw ++)
                {
                    for(Eigen::Index ib = 0; ib < nbHead_; ib ++)
                    {
                        double m0_tmp_(0.);
                        double m2_tmp_(0.);
                        double m4_tmp_(0.);
                        for(Eigen::Index pw = 0; pw < degree +degree +1; pw ++)
                        {
                            for(Eigen::Index pb = 0; pb < degree +degree +1; pb ++)
                            {
                                m0_tmp_ = unitMoments(iw, pw) *sIntegrals(ib, pb, 0);
                                m2_tmp_ = unitMoments(iw, 2 +pw) *sIntegrals(ib, pb, 0);
                                if (isM4_)
                                {
                                    m4_tmp_ = unitMoments(iw, 4 +pw) *sIntegrals(ib, pb, 0);
                                }
                                if (isForwardSpeed)
                                {
                                    m2_tmp_ += 2. *Cl(1) *unitMoments(iw, 2 +pw +1) *sIntegrals(ib, pb, 1)
                                             +     Cl(2) *unitMoments(iw, 2 +pw +2) *sIntegrals(ib, pb, 2);

                                    if (isM4_) {
                                        m4_tmp_ += 4. *Cl(1) *unitMoments(iw, 4 +pw +1) *sIntegrals(ib, pb, 1)
                                                 + 6. *Cl(2) *unitMoments(iw, 4 +pw +2) *sIntegrals(ib, pb, 2)
                                                 + 4. *Cl(3) *unitMoments(iw, 4 +pw +3) *sIntegrals(ib, pb, 3)
                                                 +     Cl(4) *unitMoments(iw, 4 +pw +4) *sIntegrals(ib, pb, 4);
                                    }
                                }
                                for (Eigen::Index mode = 0; mode < rao_.getNModes(); ++mode)
                                {
                                    m0_(ss, mode) += coeffsSpr(ib, iw, pw, pb, mode) * m0_tmp_;
                                    m2_(ss, mode) += coeffsSpr(ib, iw, pw, pb, mode) * m2_tmp_;
                                    if (isM4_)
                                        m4_(ss, mode) += coeffsSpr(ib, iw, pw, pb, mode) * m4_tmp_;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    isComputed_ = true ;
}
