#include "Davenport.hpp"
#include "SpectralTools.hpp"

using namespace BV::Spectral ;

Eigen::ArrayXd Davenport::compute(
    const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if ((L <= 0.) || (almost_equal(C10, 0.)))
        return Eigen::ArrayXd::Zero(w.size()) ;

    Eigen::ArrayXd sw(w.size()) ;
    const double a = 4. / std::pow(L, 2. / 3.) * C10
        * std::pow(meanValue, 8. / 3.) ;
    const double b = (meanValue / L) * (meanValue / L) ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi = w(i) / doublePi ;
        sw[i] = a * fi / std::pow((fi * fi + b), 4. / 3.) ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
