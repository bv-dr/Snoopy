#include "Spectral/ResponseSpectrum2ndMQtf.hpp"

#include <iostream>

using namespace BV::Spectral;

void ResponseSpectrum2ndMQtf::compute() {
    if (qtf_.isQtf() && !seaState_.isUnidirectional()) {
        throw std::logic_error("Error: Qtf is unidirectional (QTF) but the seaState is not!");
    }

    const Eigen::Tensor<double, 4> modules(qtf_.getModules());
    Eigen::Index nbfreq(freqs_.size());
    if (seaState_.isUnidirectional()) {
        for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
        {
            const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
            // Calculation the S and G functions
            Eigen::ArrayXd S(spectrum.compute(freqs_)) ;
            Eigen::Index ib1;    // closest heading of the mqtf to the spectrum heading
            Eigen::Index ib2;
            Eigen::abs((  headings_ - spectrum.heading  )).minCoeff ( &ib1 );
            if (qtf_.isQtf())
                ib2 = 0;
            else
                ib2 = ib1;

            if (mode_ == QtfMode::DIFF) { // difference mode
                /*
                 * S(d\omega) = 2 \int_0^{2\pi} \int_0^{2\pi} \int_0^\infty
                 *      | QTF(\omega, \omega +d\omega, \beta_1, \beta_2) |^2
                 *      S(\omega, \beta_1) S(\omega +d\omega, \beta_2) d\omega d\beta_1 d\beta_2
                 */
                // Assuming that W_ and qtf_.DeltaFrequencies are the same
                // if not we have to approximate
                for(decltype(W_.size()) idw = 0; idw < W_.size(); idw++) {          // dw = 0, .., N    : d\omega
                    Eigen::ArrayXd wDensity(nbfreq -idw);
                    for(int iw = 0; iw < nbfreq -idw; iw ++) {           // w = 0, .., M     : \omega
                        // integral with respect to \beta_2
                        // (w, dw) = BV::Math::Integration::trapz((b2,dw).col(dw), dh_) <=>
                        //              \int_0^{2\pi} \int_0^{2\pi} (b2 ; b1,dw; w) db1 db2
                        wDensity(iw) = std::pow(modules(ib1, iw, idw, ib2), 2.0) *S(iw) *S(iw +idw);
                    }   // iw
                    // integral with respect to \omega
                    //   (dw) = 8.0 *BV::Math::Integration::trapz((w,dw).col(dw), dw_) <=> S(d\omega)
                    rSpec_(idw) = BV::Math::Integration::trapz(wDensity, dw_) *8.0;
                }   // idw
            }   // difference mode
            else if (mode_ == QtfMode::SUM) {
                throw std::logic_error("Second order response spectrum for the sum mode is not implemented");
            }
        }   // seaState_.getSpectrumCount()
        Eigen::ArrayXd rM2Core(rSpec_ *W_ *W_); // S(\omega) *\omega^2, where \omega = 0.0, dw, 2dw, ...
        m0_ = BV::Math::Integration::trapz(rSpec_, dw_);
        m2_ = BV::Math::Integration::trapz(rM2Core, dw_);
        isComputed_ = true ;
        return;
    } // unidirectional spectral for MQtf

//    std::cout << "ResponseSpectrum2ndMQtf.compute()\n";
//    std::cout << "rSpec_.size() = " << rSpec_.size() << "\n";
//    std::cout << "W_ : \n" << W_ << "\n";
//    std::cout << "dw_ = " << dw_ << "\n";
//    std::cout << "nWs_ = " << nWs_ << "\n";
//    std::cout << "dh_ = " << dh_ << "\n";
//    std::cout << "headings : \n" << headings_ << "\n";
//    std::cout << "nHs_ = " << nHs_ << "\n";
    for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
        /*
         * S(\omega, \theta) = S_0(\omega) x G(\theta), where
         * spectrumFreqs : S_0(\omega) is calculated by spectrum.compute(Qtf_frequencies);
         * spectrumHeads : G(\theta)   is calculated by spectrum.computeSpreading(Qtf_headings);
         * because w_k = w_{k-1} +dw = w_1 +(k-1)dw
         * and we need S(\omega, \theta) till w_n+N dw, where N = dw.size() -1 (excluding dw(0) == 0.0)
         * w_1, w_1 +dw, w_1 +2dw, w_1 +3dw, ..., w_1 +(dw.size()-1)dw
         *      w_2,     w_2 + dw, w_2 +2dw, ..., w_2 +(dw.size()-2)dw, w_2 +(dw.size()-1)dw
         *               w_3,      w_3 + dw, ..., w_3 +(dw.size()-3)dw, w_3 +(dw.size()-2)dw, w_3 +(dw.size()-1)dw
         *  ...
         * w_n, w_n +dw, w_n +2dw, w_n +3dw, ..., w_n +(dw.size()-1)dw
         */

        // Calculation the S and G functions
        Eigen::ArrayXd S(spectrum.compute(freqs_)) ;
        Eigen::ArrayXd G(spectrum.computeSpreading(headings_));

        /*
        std::ofstream s_out("S.dat"), g_out("G.dat");
        for(int f = 0; f < nbfreq; f ++) {
            s_out << freqs_[f] << "\t" << S[f];
            for(int idw = 0; idw < W_.size(); idw ++) {
                s_out << "\t" << S[f] *S[f +idw];
            }
            s_out << "\n";
        }

        for(int h = 0; h < headings_.size(); h ++)
            g_out << headings_[h] << "\t" << G[h] << "\n";
        s_out.close();
        g_out.close();
        */
        /*
        std::cout << "SpectrumType: ";
        switch(spectrum.getSpreadingType()) {
        case SpreadingType::No:
            std::cout << "No";
            break;
        case SpreadingType::Cosn:
            std::cout << "Cosn, Value = " << spectrum.getSpreadingValue() << "\n";
            break;
        case SpreadingType::Cos2s:
            std::cout << "Cos2s, Value = " << spectrum.getSpreadingValue() << "\n";
            break;
        case SpreadingType::Wnormal:
            std::cout << "Wnormal, Value = " << spectrum.getSpreadingValue() << "\n";
            break;
        }
        std::cout << "Integral of G = " << Math::Integration::trapz(G, dh_) << "\n";
        std::cout << "Integral of S = " << Math::Integration::trapz(S, dw_) << "\n";
        */

        if (mode_ == QtfMode::DIFF) { // difference mode
            /*
             * S(d\omega) = 2 \int_0^{2\pi} \int_0^{2\pi} \int_0^\infty
             *      | QTF(\omega, \omega +d\omega, \beta_1, \beta_2) |^2
             *      S(\omega, \beta_1) S(\omega +d\omega, \beta_2) d\omega d\beta_1 d\beta_2
             */
            // Assuming that W_ and qtf_.DeltaFrequencies are the same
            // if not we have to approximate
            for(decltype(W_.size()) idw = 0; idw < W_.size(); idw++) {          // dw = 0, .., N    : d\omega
                Eigen::ArrayXd wb2b1Density(nbfreq -idw);
                for(int iw = 0; iw < nbfreq -idw; iw ++) {           // w = 0, .., M     : \omega
                    Eigen::ArrayXd b2b1Density(nHs_);
                    for(int ib2 = 0; ib2 < nHs_; ib2 ++) {      // b2 = 0, .., B    : \beta_2
                        Eigen::ArrayXd b1Density(nHs_);
                        for(int ib1 = 0; ib1 < nHs_; ib1 ++) {  // b1 = 0, .., B    : \beta_1
                            // (b1, dw) = | QTF( w, w +dw, b1, b2) | ^2 S(w, b1) S(w+dw, b2)
                            //b1Density(ib1) = std::pow(modules(ib1, iw, idw, ib2, iMode), 2.0) *G(ib1);
                            b1Density(ib1) = std::pow(modules(ib1, iw, idw, ib2), 2.0) *G(ib1);
                            //b1Density(ib1) = G(ib1);
                        }   // ib1
                        // integral with respect to \beta_1
                        // (b2, dw) = BV::Math::Integration::trapz((b1,dw).col(dw), dh_) <=>
                        //              \int_0^{2\pi} (b1,dw; b2, w) db1
                        b2b1Density(ib2) = BV::Math::Integration::trapz(b1Density, dh_) *G(ib2);
                    }   // ib2
                    // integral with respect to \beta_2
                    // (w, dw) = BV::Math::Integration::trapz((b2,dw).col(dw), dh_) <=>
                    //              \int_0^{2\pi} \int_0^{2\pi} (b2 ; b1,dw; w) db1 db2
                    wb2b1Density(iw) = BV::Math::Integration::trapz(b2b1Density, dh_) *S(iw) *S(iw+idw);
                }   // iw
                // integral with respect to \omega
                //   (dw) = 2.0 *BV::Math::Integration::trapz((w,dw).col(dw), dw_) <=> S(d\omega)
                rSpec_(idw) = BV::Math::Integration::trapz(wb2b1Density, dw_) *8.0;
            }   // idw
        }   // difference mode
        else if (mode_ == QtfMode::SUM) {
            throw std::logic_error("Second order response spectrum for the sum mode is not implemented");
        }
    }   // seaState_.getSpectrumCount()
    Eigen::ArrayXd rM2Core(rSpec_ *W_ *W_); // S(\omega) *\omega^2, where \omega = 0.0, dw, 2dw, ...
    m0_ = BV::Math::Integration::trapz(rSpec_, dw_);
    m2_ = BV::Math::Integration::trapz(rM2Core, dw_);
    isComputed_ = true ;
}
