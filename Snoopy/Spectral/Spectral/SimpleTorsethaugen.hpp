#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class SimpleTorsethaugen
 *
 * @brief Define a simplified Torsethaugen spectrum.
 */
class SPECTRAL_API SimpleTorsethaugen : public ParametricSpectrum
{
public:

    SimpleTorsethaugen(double hs, double tp, double heading = 0., SpreadingType spreadingType = SpreadingType::No,
                       double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
          , hs_(hs), tp_(tp)
    {
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    static int getNParams() { return 2; }
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp", }; }
    std::vector<double> getCoefs() const override   { return {  hs_,  tp_, }; }
    static std::vector<double> getCoefs_0()         { return {  1. ,  10., }; }
    static std::vector<double> getCoefs_min()       { return {  0. ,  1. , }; }
    static std::vector<double> getCoefs_max()       { return {  30.,  20., }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<SimpleTorsethaugen>();
    }

};

}
}
