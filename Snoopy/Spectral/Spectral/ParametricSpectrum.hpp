#pragma once
#include <iostream>
#include <sstream>
#include <vector>
#include "WaveSpectrum.hpp"
#include "SpectralExport.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class ParametricSpectrum
 *
 * @brief Define the base class of all parametric spectrums.
 */
class ParametricSpectrum : public WaveSpectrum
{
public:
    ParametricSpectrum(const char* name, double heading, SpreadingType spreadingType, double spreadingValue)
        : WaveSpectrum(std::string(name), heading, spreadingType, spreadingValue)
    {
    }

    // Return number of parameter
    static int getNParams() { throw std::logic_error( "getNParams : not implemented for this wave spectrum" ); }

    // Return sensible parameter array ( can be use as starting value for optimization algorithm )
    static std::vector<double> getCoefs_0() { throw std::logic_error( "getCoefs_0 : not implemented for this wave spectrum" ); }
    static std::vector<double> getCoefs_min() { throw std::logic_error( "getCoefs_min : not implemented for this wave spectrum" ); }
    static std::vector<double> getCoefs_max() { throw std::logic_error( "getCoefs_max : not implemented for this wave spectrum" ); }

    // Return parameter names
    static std::vector<const char*> getCoefs_name() { throw std::logic_error( "getCoefs_name : not implemented for this wave spectrum" ); }

    // Return parameters values
    virtual std::vector<double> getCoefs() const = 0;

    // Return Hs, to be sub-classed by spectrum where 1st coefficients is not Hs
    double getHs() const override { return getCoefs()[0]; }

    // Return Tp, to be sub-classed by spectrum where 1st coefficients is not Hs
    double getTp() const override { return getCoefs()[1]; }

    virtual std::string print() const = 0;

protected:

    // Write spectrum info (StarSpec format rad-deg conversion)
    template<typename T>
    std::string print() const
    {
        std::stringstream ss;
        ss << name_ << " ";
        for (unsigned i = 0; i < getCoefs().size(); ++i)
        {
            ss << T::getCoefs_name()[i] << " " << getCoefs()[i] << " ";
        }

        ss << " HEADING" << " " << heading * 180. / M_PI;

        if (getSpreadingType() != SpreadingType::No)
        {
            if (getSpreadingType() == SpreadingType::Wnormal)
            {
                ss << " SPREADING " << getSpreadingFunctionName() << " " << getSpreadingValue() * 180 / M_PI;
            }
            else
            {
                ss << " SPREADING " << getSpreadingFunctionName() << " " << getSpreadingValue();
            }
        }

        return ss.str();
    }
};

}
}
