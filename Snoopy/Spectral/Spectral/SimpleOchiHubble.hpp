#pragma once

#include "SpectralExport.hpp"
#include "Wallop.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class SimpleOchiHubble
 *
 * @brief Define a simplified Ochi-Hubble spectrum as a Wallop spectrum with
 * q = 4 and m = 4 * lambda + 1.
 */
class SPECTRAL_API SimpleOchiHubble : public Wallop
{
public:

    SimpleOchiHubble(double hs, double tp, double lambda, double heading = 0.,
                     SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : Wallop(hs, tp, 4*lambda+1, 4, heading, spreadingType, spreadingValue), lambda_(lambda)
    {
        name_ = name;
    }

    static const char* name;

    // Shape parameter.
    double lambda_;

    static int getNParams() { return 3; }
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp" , "lambda" }; }
    std::vector<double> getCoefs() const override   { return { hs_,   tp_ ,  lambda_ }; }
    static std::vector<double> getCoefs_0()         { return { 1. ,   10. ,  1.      }; }
    static std::vector<double> getCoefs_min()       { return { 0. ,   0.01,  0.01    }; }
    static std::vector<double> getCoefs_max()       { return { 30.,   60. ,  30.     }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<SimpleOchiHubble>();
    }

};

}
}
