#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class OchiShin
 *
 * @brief Define a OchiShin wind spectrum.
 */
class SPECTRAL_API OchiShin: public Spectrum
{
public:

    OchiShin(double meanVelocity, double C, double z, double heading=0.) :
        Spectrum("OchiShin", heading, meanVelocity), C(C), z(z)
    {
    }

    // Surface drag coefficient
    double C ;

    // Elevation
    double z ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 3 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "height"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C, z} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.002, 10.} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1e3} ;}
} ;

}
}
