#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class API
 *
 * @brief Define a API wind spectrum.
 */
class SPECTRAL_API API: public Spectrum
{
public:

    API(double meanVelocity, double fp, double heading=0.) :
        Spectrum("API", heading, meanVelocity), fp(fp)
    {
    }

    // fp is the average factor (by default: 0.0025*VW10)
    double fp ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 2 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "fp"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , fp} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.0025} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1.} ;}

} ;

}
}
