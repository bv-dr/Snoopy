#pragma once

#include <vector>
#include <memory>

#include <Eigen/Dense>

#include "SpectralExport.hpp"

namespace BV {
namespace Spectral {

class Spectrum;
class WaveSpectrum;
class SeaState;
class Rao;

/**
 * @class Wif
 *
 * @brief Define a unique wave time series by its discrete spectrum that
 * includes phases
 */
class SPECTRAL_API Wif
{
public:

    ~Wif()
    {
    }

    /**
     * Construct a Wif using its parameters directly.
     *
     * @param w The frequency list.
     * @param a The amplitude list.
     * @param phi The phase list.
     * @param b The heading list.
     * @param depth The water depth
     */
    Wif(const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& a,
        const Eigen::Ref<const Eigen::ArrayXd>& phi,
        const Eigen::Ref<const Eigen::ArrayXd>& b, double depth = -1.) ;

    /**
     * Construct a Wif from a sea state and fixed frequencies and headings.
     *
     * @param spectrum The sea state.
     * @param w The frequency list.
     * @param b The heading list.
     * @param seed The seed to use for generating random numbers for phasis, 0 to use a random seed.
     * @param depth The water depth
     */
    Wif(const WaveSpectrum& spectrum, const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& b, unsigned int seed = 0,
        double depth = -1.);

    /**
     * Construct a Wif from a sea state and fixed frequencies and spectrum headings (without spreading).
     *
     * @param spectrum The sea state.
     * @param w The frequency list.
     * @param seed The seed to use for generating random numbers for phasis, 0 to use a random seed.
     * @param depth The water depth
     */
    Wif(const Spectrum& spectrum, const Eigen::Ref<const Eigen::ArrayXd>& w,
        unsigned int seed = 0, double depth = -1.);

    /**
     * Construct a Wif from a sea state and fixed spectrum headings (without spreading).
     * With random shift to avoid repetition period.
     *
     * @param spectrum The sea state.
     * @param wmin The minimum frequency
     * @param wmax The maximum frequency
     * @param nbSeed The number of seeds to use
     * @param seed The seed to use for generating random numbers for phasis, 0 to use a random seed.
     * @param depth The water depth
     */
    Wif(const Spectrum& spectrum, double wmin = 0.1, double wmax = 2.0,
        unsigned int nbSeed = 200, unsigned int seed = 0,
        double depth = -1.) ;

    /**
     * Construct a Wif from a spectrum and fixed frequencies and headings.
     *
     * @param seaState The sea state.
     * @param w The frequency list.
     * @param b The heading list.
     * @param seed The seed to use for generating random numbers for phasis, 0 to use a random seed.
     * @param depth The water depth
     */
    Wif(const SeaState& seaState, const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& b = Eigen::ArrayXd(0),
        unsigned int seed = 0, double depth = -1.);

    /**
     * Construct a Wif from a spectrum. With random shift to avoid repetition period
     *
     * @param spectrum The sea state.
     * @param wmin The minimum frequency
     * @param wmax The maximum frequency
     * @param nbSeed The number of seeds to use
     * @param spreadNbheading The number of headings to use for spreading.
     * @param seed The seed to use for generating random numbers for phasis, 0 to use a random seed.
     * @param depth The water depth
     */
    Wif(const WaveSpectrum& spectrum, double wmin = 0.1, double wmax = 2.0,
        unsigned int nbSeed = 200, unsigned int spreadNbheading = 72,
        unsigned int seed = 0, double depth = -1.);

    /**
     * Construct a Wif from a spectrum. With random shift to avoid repetition period
     *
     * @param spectrum The sea state.
     * @param wmin The minimum frequency
     * @param wmax The maximum frequency
     * @param nbSeed The number of seeds to use
     * @param spreadNbheading The number of headings to use for spreading.
     * @param seed The seed to use for generating random numbers for phasis, 0 to use a random seed.
     * @param depth The water depth
     */
    Wif(const SeaState& seaState, double wmin = 0.1, double wmax = 2.0,
        unsigned int nbSeed = 200, unsigned int spreadNbheading = 72,
        unsigned int seed = 0, double depth = -1.);


    /**
     * Construct a Wif from cos and sin vector (eta = a_i * cos(wt) + b_i * sin(wt) )
     *
     * @param w The frequency list.
     * @param cos_sin The a_i + b_i vector
     * @param b The heading
     * @param depth The water depth
     */
    Wif(const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& cos_sin,
        double b, double depth = -1.);

    /**
     * Construct a Wif from a file. It does not check the validity of it.
     *
     * @param filename The file path.
     * @param depth The water depth.
     */
    explicit Wif(const std::string& filename, double depth = -1);

    // Modify spectrum density without changing frequencies or phases.
    // TO DO : once code is finalized (i.e. handle spreading), should be called by the different constructors
    void setSpectrum(const WaveSpectrum& spectrum);
    void setSpectrum(const Spectrum& spectrum);

    Wif& operator+=(const Wif& wif);

    Wif operator+(const Wif& rhs) const;

    Wif & operator*=(double scale);

    void removeZeroFrequency();

    /**
     * @brief removeFreq: removes a frequency with index ifreq
     * @param ifreq
     * @param toResize. Default toResize = true. If toResize == false, then after the removing
     * and shifting the array elements, the size of the array is kept the same,
     * otherwise, it is resized to n-1, where n was the initial array size
     */
    void removeFreq(const Eigen::Index & ifreq, const bool toResize);
    /**
     * @brief resize: apply conservative resize for all variables: w_, amp_, ...
     * @param nbfreq
     */
    void resize(const Eigen::Index & nbfreq);
    /**
     * @brief removeZero: removes waves with amplitudes less than the threshold
     * This subroutine is taken from the original fortran subroutine removeZero from wif2_module.f90
     */
    void removeZero(const double threshold);

    inline double getDepth() const;

    void setDepth(double depth);

    void offset(double dt, double dx, double dy);

    inline const Eigen::ArrayXd& getFrequencies() const;

    inline const Eigen::ArrayXd& getAmplitudes() const;

    inline void setAmplitudes(const Eigen::ArrayXd&);

    inline const Eigen::ArrayXd& getPhases() const;

    inline void setPhases(const Eigen::ArrayXd&);

    inline Eigen::ArrayXcd getComplexData() const;

    void setHeadings(const Eigen::ArrayXd&);

    void setHeadings(double);

    inline const Eigen::ArrayXd& getHeadings() const;

    inline const Eigen::ArrayXd& getCosHeadings() const;

    inline const Eigen::ArrayXd& getWWidth() const;

    inline const Eigen::ArrayXd& getBWidth() const;

    // Return energy density
    Eigen::ArrayXd getDensity() const;

    inline const Eigen::ArrayXd& getSinHeadings() const;

    /**
     * Get the wave number for each frequency/heading.
     */
    inline const Eigen::ArrayXd& getWaveNumbers() const;

    /**
     * Get the encounter frequencies for each frequency/heading.
     */
    Eigen::ArrayXd getEncounterFrequencies(double speed) const;


    inline bool getUniDirectional() const
    {
        return unidirectional_;
    }

    inline double getReferencePointX() const
    {
        return referencePointX_ ;
    }

    inline double getReferencePointY() const
    {
        return referencePointY_ ;
    }

    inline const Eigen::ArrayXd & getIndependentHeadings() const
    {
        return independentHeadings_ ;
    }

    inline const std::vector<unsigned> & getIndependentHeadingsIndices() const
    {
        return independentHeadingsIndices_ ;
    }
/*
    inline const std::vector<unsigned> & getDep2IndHeadingsIndices() const
    {
        return d2iHeadingsIndices_ ;
    }
*/
private:

    double referencePointX_ = 0. ;
    double referencePointY_ = 0. ;
    std::vector<unsigned> independentHeadingsIndices_ ;
    Eigen::ArrayXd independentHeadings_ ;
    /**/
/*     * @brief d2iHeadingsIndices_ contains the indices of the unique heading from independentHeadings_
     * for each original headings vector b_, i.e.
     *  b_(k) == independentHeadings_( d2iHeadingsIndices[k] )
     * which implies that for any k and l, so
     *  b_(k) == b_(l)
     * it follows
     *  d2iHeadingsIndices[k] == d2iHeadingsIndices[l]
     * This is used in Multidirectional QTF, where QTF is interpolated by using independentHeadings_
     * vector, but to get MQtf for a given wave from the list of waves in wif file we need to know
     * the index of the heading of the wave in the list of all independent headings.
     */
//    std::vector<unsigned> d2iHeadingsIndices_ ;

    /**
     * Compute the Wif frequency, amplitude, phase and heading lists for the given spectrum.
     */
    void computeWifParameters(const WaveSpectrum& spectrum,
                              const Eigen::Ref<const Eigen::ArrayXd>& w,
                              const Eigen::Ref<const Eigen::ArrayXd>& b,
                              unsigned int seed, Eigen::Index segment = 0);

    /**
     * Compute the Wif frequency, amplitude, phase and heading lists for the given spectrum. Without spreading!
     */
    void computeWifParameters(const Spectrum& spectrum,
                              const Eigen::Ref<const Eigen::ArrayXd>& w,
                              unsigned int seed, Eigen::Index segment = 0);

    /**
     * Initialize some Wif attributes after it has been constructed.
     */
    void initialize(bool initialise=false);

protected:
    /**
     * The frequency list (center).
     */
    Eigen::ArrayXd w_ ;

    /**
     * The table of amplitude.
     */
    Eigen::ArrayXd amp_ ;

    /**
     * The table of phases.
     */
    Eigen::ArrayXd phi_ ;

    /**
     * The heading list in radians.
     */
    Eigen::ArrayXd b_ ;

    /**
     * The heading cosinus list.
     */
    Eigen::ArrayXd cb_ ;

    /**
     * The heading sinus list.
     */
    Eigen::ArrayXd sb_ ;


    /**
     * True if wif is uni-directional
     */
    bool unidirectional_;

    /**
     * The depth used to compute the wave numbers.
     */
    double depth_ ;

    /**
     * The wave number list.
     */
    Eigen::ArrayXd k_ ;

    /**
     * The bins widths (Not always there, size = nbwave).
     */
    bool isWidth_ = false;    //Is the information there ?
    Eigen::ArrayXd wWidth_;   //Frequency width
    Eigen::ArrayXd bWidth_;   //Heading width

} ;

const Eigen::ArrayXd& Wif::getWWidth() const
{
    if (isWidth_)
        {
        return wWidth_;
        }
    throw std::logic_error( "Bandwidth not known for this wif object" );
}

const Eigen::ArrayXd& Wif::getBWidth() const
{
    if (isWidth_)
        {
        return bWidth_;
        }
    throw std::logic_error( "Bandwidth not known for this wif object" );
}

const Eigen::ArrayXd& Wif::getFrequencies() const
{
    return w_ ;
}

double Wif::getDepth() const
{
    return depth_ ;
}

const Eigen::ArrayXd& Wif::getAmplitudes() const
{
    return amp_ ;
}

void Wif::setAmplitudes(const Eigen::ArrayXd& amp)
{
    if (w_.size() == amp.size())
    {
        amp_ = amp ;
    }
    else
    {
        throw std::invalid_argument("Invalid size for amp");
    }
}

void Wif::setPhases(const Eigen::ArrayXd& phi)
{
    if (w_.size() == phi.size())
    {
        phi_ = phi ;
    }
    else
    {
        throw std::invalid_argument("Invalid size for phi");
    }
}

const Eigen::ArrayXd& Wif::getPhases() const
{
    return phi_ ;
}

const Eigen::ArrayXd& Wif::getHeadings() const
{
    return b_ ;
}

const Eigen::ArrayXd& Wif::getCosHeadings() const
{
    return cb_ ;
}

const Eigen::ArrayXd& Wif::getSinHeadings() const
{
    return sb_ ;
}

const Eigen::ArrayXd& Wif::getWaveNumbers() const
{
    return k_ ;
}

Eigen::ArrayXcd Wif::getComplexData() const
{
    std::complex<double> i(0, 1);
    Eigen::ArrayXcd cvalue(amp_.size());
    cvalue = amp_.array() * Eigen::exp(i * phi_.array());
    return cvalue ;
}

class SPECTRAL_API Wifm
{
private:
    enum levels_{LOWER, EQUAL, HIGHER} ;
    class BoundedWif
    {
        std::shared_ptr<Wif> p_wif_ ;
        Eigen::Vector2d timeBounds_ ;
    public:
        BoundedWif(std::shared_ptr<const Wif> p_wif, const Eigen::Vector2d & bounds):
            p_wif_(std::make_shared<Wif>(*(p_wif.get()))), timeBounds_(bounds)
        {
        }

        BoundedWif(const BoundedWif & other)
        {
            if (&other == this)
            {
                return ;
            }
            p_wif_ = std::make_shared<Wif>(*(other.p_wif_.get())) ;
            timeBounds_ = other.timeBounds_ ;
        }

        levels_ isTimeInBounds(const double & time) const
        {
            if (time < timeBounds_(0))
                return levels_::LOWER ;
            if (time > timeBounds_(1))
                return levels_::HIGHER ;
            return levels_::EQUAL ;
        }
        std::shared_ptr<Wif> getWif()
        {
            return p_wif_ ;
        }
    } ;

    std::vector<BoundedWif> wifs_ ;
    std::size_t nWifs_ ;
    Eigen::MatrixX2d bounds_ ;
public:

    ~Wifm()
    {
    }

    Wifm(const std::vector<std::shared_ptr<Wif> > & wifs,
         const Eigen::MatrixX2d & bounds) ;

    std::size_t getWifIndex(const double & time) const ;
    inline std::size_t getNWifs() const
    {
        return nWifs_ ;
    }
    std::shared_ptr<Wif> getWifAtIndex(const std::size_t & index) ;
    std::shared_ptr<Wif> getWif(const double & time) ;
    const Eigen::MatrixX2d & getBounds() const ;

    void setHeadings(double heading);

} ;

}
}
