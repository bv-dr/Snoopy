#pragma once

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <regex>

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

// Splinter part
#include <datatable.h>
#include <bspline.h>
#include <bsplinebuilder.h>

#include <math.h>
#include <cctype>

#include "Spectral/TransferFunction.hpp"    // ComplexInterpolationStrategies

#include "SpectralExport.hpp"

namespace BV {
namespace Spectral {

enum class QtfType {UNKNOWN, QTF, MQTF};
enum QtfMode
{
    DIFF = -1, SUM = 1
} ;

class Qtf;

class SPECTRAL_API MQtf {
    public:
        MQtf() :
            waveRefPoint_(Eigen::Vector2d::Zero()),
            refPoint_(Eigen::Vector3d::Zero()),
            qtfMode_(QtfMode::DIFF),
            depth_(-1.0),
            forwardSpeed_(0.0),
            filename_(""),
            refLength_(1.0),
            rho_(1025.0),
            g_(9.81),
            qtfType_(QtfType::QTF)
            {}

        // QtfType == QTF
        MQtf(Eigen::ArrayXd const & b,
            Eigen::ArrayXd const & w,
            Eigen::ArrayXd const & dw,
            Eigen::Tensor<std::complex<double>, 3> const & t,
            Eigen::Vector2d const & waveRefPoint = Eigen::Vector2d::Zero(),
            Eigen::Vector3d const & refPoint = Eigen::Vector3d::Zero(),
            QtfMode const & qtfMode = QtfMode::DIFF,
            double const & depth = 0,
            double const & forwardSpeed = 0.0,
            std::string const & filename = "",
            double const & refLength = 1.0,
            double const & rho = 1025.0,
            double const & g = 9.81) :
                b_(b),
                w_(w),
                dw_(dw),
                waveRefPoint_(waveRefPoint),
                refPoint_(refPoint),
                qtfMode_(qtfMode),
                depth_(depth),
                forwardSpeed_(forwardSpeed),
                filename_(filename),
                refLength_(refLength),
                rho_(rho),
                g_(g),
                qtfType_(QtfType::QTF)
                {
                    t_.resize(b_.size(), w_.size(), dw_.size(), 1);
                    for(int h1 = 0; h1 < b_.size(); h1++)
                        for(int f = 0; f < w_.size(); f++)
                            for(int d = 0; d < dw_.size(); d++)
                                t_(h1, f, d, 0) = t(h1, f, d);
                }

        // QtfType = MQTF
        MQtf(Eigen::ArrayXd const & b,
            Eigen::ArrayXd const & w,
            Eigen::ArrayXd const & dw,
            Eigen::Tensor<std::complex<double>, 4> const & t,
            Eigen::Vector2d const & waveRefPoint = Eigen::Vector2d::Zero(),
            Eigen::Vector3d const & refPoint = Eigen::Vector3d::Zero(),
            QtfMode const & qtfMode = QtfMode::DIFF,
            double const & depth = 0,
            double const & forwardSpeed = 0.0,
            std::string const & filename = "",
            double const & refLength = 1.0,
            double const & rho = 1025.0,
            double const & g = 9.81) :
                b_(b),
                w_(w),
                dw_(dw),
                t_(t),
                waveRefPoint_(waveRefPoint),
                refPoint_(refPoint),
                qtfMode_(qtfMode),
                depth_(depth),
                forwardSpeed_(forwardSpeed),
                filename_(filename),
                refLength_(refLength),
                rho_(rho),
                g_(g),
                qtfType_(QtfType::MQTF)
                {}

        MQtf(MQtf const & qtf) :
            b_(qtf.b_),
            w_(qtf.w_),
            dw_(qtf.dw_),
            t_(qtf.t_),
            waveRefPoint_(qtf.waveRefPoint_),
            refPoint_(qtf.refPoint_),
            qtfMode_(qtf.qtfMode_),
            depth_(qtf.depth_),
            forwardSpeed_(qtf.forwardSpeed_),
            filename_(qtf.filename_),
            refLength_(qtf.refLength_),
            rho_(qtf.rho_),
            g_(qtf.g_),
            qtfType_(qtf.qtfType_)
            {}

        MQtf(MQtf && qtf) :
            b_(std::move(qtf.b_)),
            w_(std::move(qtf.w_)),
            dw_(std::move(qtf.dw_)),
            t_(std::move(qtf.t_)),
            waveRefPoint_(std::move(qtf.waveRefPoint_)),
            refPoint_(std::move(qtf.refPoint_)),
            qtfMode_(std::move(qtf.qtfMode_)),
            depth_(std::move(qtf.depth_)),
            forwardSpeed_(std::move(qtf.forwardSpeed_)),
            filename_(std::move(qtf.filename_)),
            refLength_(std::move(qtf.refLength_)),
            rho_(std::move(qtf.rho_)),
            g_(std::move(qtf.g_)),
            qtfType_(std::move(qtf.qtfType_))
            {}

        MQtf(const Qtf & qtf);

        void operator=(MQtf const & qtf) {
            b_ = qtf.b_;
            w_ = qtf.w_;
            dw_ = qtf.dw_;
            qtfType_ = qtf.qtfType_;
            if (qtf.isQtf()) t_.resize(b_.size(), w_.size(), dw_.size(), 1);
            else if (qtf.isMQtf()) t_.resize(b_.size(), w_.size(), dw_.size(), b_.size());
            else t_.resize(0, 0, 0, 0);
            for(int h1 = 0; h1 < t_.dimension(0); h1 ++)
                for(int f = 0; f < t_.dimension(1); f++)
                    for(int d = 0; d < t_.dimension(2); d++)
                        for(int h2 = 0; h2 < t_.dimension(3); h2++)
                            t_(h1, f, d, h2) = qtf.t_(h1, f, d, h2);
            waveRefPoint_ = qtf.waveRefPoint_;
            refPoint_ = qtf.refPoint_;
            qtfMode_ = qtf.qtfMode_;
            depth_ = qtf.depth_;
            forwardSpeed_ = qtf.forwardSpeed_;
            filename_ = qtf.filename_;
            refLength_ = qtf.refLength_;
            rho_ = qtf.rho_;
            g_ = qtf.g_;
        }

        inline double getForwardSpeed() const {return forwardSpeed_;}
        inline const Eigen::Vector2d& getWaveRefPoint() const {return waveRefPoint_;}
        inline const Eigen::Vector3d& getRefPoint() const {return refPoint_;}
        inline const Eigen::ArrayXd& getHeadings() const {return b_;}
        inline const Eigen::ArrayXd& getDeltaFrequencies() const {return dw_;}
        inline const Eigen::ArrayXd& getFrequencies() const {return w_;}
        inline const Eigen::Tensor<std::complex<double>, 4> getMQtfTensor() const {return t_;}
        inline double getRho() const {return rho_;}
        inline double getGrav() const {return g_;}
        inline double getDepth() const {return depth_;}
        inline double getRefLength() const {return refLength_;}
        inline bool isQtf() const {return qtfType_ == QtfType::QTF;}
        inline bool isMQtf() const {return qtfType_ == QtfType::MQTF;}
        inline bool isModeSum() const {return qtfMode_ == QtfMode::SUM;}
        inline bool isModeDiff() const {return qtfMode_ == QtfMode::DIFF;}

        inline double getSumMode() const {return static_cast<double>(qtfMode_);}
        inline QtfMode getMode() const {return qtfMode_;}
        inline double getDwMax() const {return dw_(dw_.size()-1);}

        inline Eigen::Tensor<double, 4> getModules() const {return t_.abs();}

        // Modes methods (does not support the modes, but these methods are need for the compatibility with Qtf and MQtf)
        inline size_t getNModes() const {return 1;}
        inline Eigen::Array<Modes, Eigen::Dynamic, 1> getModes() const {return Eigen::Array<Modes, Eigen::Dynamic, 1>::Constant(1, Modes::NONE);}

        inline void setFilename(std::string const & filename) {filename_ = filename;}

        friend SPECTRAL_API std::ostream & operator << (std::ostream & out, const MQtf& qtf);
        friend SPECTRAL_API std::ofstream & operator << (std::ofstream & out, const MQtf& qtf);
        friend SPECTRAL_API std::ifstream & operator >> (std::ifstream & in, MQtf& qtf);

        MQtf getQtfAtFrequencies(Eigen::ArrayXd const & ws, int const & degree = 1, ComplexInterpolationStrategies const & interpStrategy = ComplexInterpolationStrategies::RE_IM_AMP) const ;
        MQtf getQtfAtHeadings(Eigen::ArrayXd const & hs, int const & degree = 1, ComplexInterpolationStrategies const & interpStrategy = ComplexInterpolationStrategies::RE_IM_AMP) const ;
        MQtf getQtfAt(Eigen::ArrayXd const & hs, Eigen::ArrayXd const & ws, Eigen::ArrayXd const &dws, int const & degree = 1, ComplexInterpolationStrategies const & interpStrategy = ComplexInterpolationStrategies::RE_IM_AMP) const ;

        /**
         * @brief getQtfAt  for Qtf
         * @param pt is Eigen::Vector3d, where the first value is h1, the second value is w1, the third is dw (w2 = w1 +dw)
         * @param degree
         * @param interpStrategy
         * @return Eigen::Vector2d, where first value is Amp Qtf, the second is Phase Qtf
         */
        Eigen::Vector2d getQtfAt(const Eigen::Vector3d & pt, const int & degree = 1, const ComplexInterpolationStrategies & interpStrategy = ComplexInterpolationStrategies::RE_IM_AMP) const ;
        /**
         * @brief getQtfAt  for MQtf
         * @param pt is Eigen::Vector4d, where the first value is h1, the second value is w1, the third is dw (w2 = w1 +dw), the fourth is h2
         * @param degree
         * @param interpStrategy
         * @return Eigen::Vector2d, where first value is Amp Qtf, the second is Phase Qtf
         */
        Eigen::Vector2d getQtfAt(const Eigen::Vector4d & pt, const int & degree = 1, const ComplexInterpolationStrategies & interpStrategy = ComplexInterpolationStrategies::RE_IM_AMP) const ;
        /**
         * @brief getQtfAt
         * @param pts : for Qtf:  ArrayXXd[pts_number, 3], for each point there are 4 values: h1, w1, dw (w2 = w1 +dw)
         *              for MQtf: ArrayXXd[pts_number, 4], for each point there are 4 values: h1, w1, dw (w2 = w1 +dw), h2
         * @param degree
         * @param interpStrategy
         * @return ArrayXXd[pts_number, 2], for each point there are 2 values: Amp Qtf, Phase Qtf
         */
        Eigen::ArrayXXd getQtfAt(const Eigen::ArrayXXd &pts, const int & degree = 1, const ComplexInterpolationStrategies & interpStrategy = ComplexInterpolationStrategies::RE_IM_AMP) const ;

        void writeHeadHeadTensor(std::string const & filename) const ;
        void writeWdWTensor(std::string const & filename) const ;

        void readQtfFromFile(std::string const & filename) {
            std::ifstream in(filename);
            if (!in.is_open()) throw std::logic_error("Cannot open the file '"+filename+"'!\n");
            in >> *this;
            in.close();
            setFilename(filename);
        }
        void writeQtfToFile(std::string const & filename) const {
            std::ofstream out(filename);
            out << *this;
            out.close();
        }

        inline const Eigen::Vector3d getReferencePoint() const { return refPoint_ ;}
        inline const Eigen::Vector2d getWaveReferencePoint() const { return waveRefPoint_ ;}

        Eigen::Tensor<std::complex<double>, 4> getQtfLikeComplexData() const;
        inline const Eigen::Tensor<std::complex<double>, 4> getComplexData() const {return t_;}
    protected:

    private:
        Eigen::ArrayXd b_;
        Eigen::ArrayXd w_;
        Eigen::ArrayXd dw_;
        Eigen::Tensor<std::complex<double>, 4> t_;
        Eigen::Vector2d waveRefPoint_;
        Eigen::Vector3d refPoint_;
        QtfMode qtfMode_;
        double depth_;
        double forwardSpeed_;
        std::string filename_;
        double refLength_;
        double rho_;
        double g_;
        QtfType qtfType_;
};
    SPECTRAL_API std::ostream & operator << (std::ostream & out, const MQtf& qtf);
    SPECTRAL_API std::ofstream & operator << (std::ofstream & out, const MQtf& qtf);
    SPECTRAL_API std::ifstream & operator >> (std::ifstream & in, MQtf& qtf);
}
}
