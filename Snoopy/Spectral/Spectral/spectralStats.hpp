#pragma once
#include <math.h>
#include "SpectralExport.hpp"
#include <Eigen/Dense>

namespace BV
{
namespace Spectral
{

SPECTRAL_API double ampFromRisk(  double risk, double duration,  double m0, double m2 );

SPECTRAL_API std::tuple<double, double> m0m2FromRsRtz(double, double);

SPECTRAL_API std::tuple<double, double> RsRtzFromM0M2(double, double);

SPECTRAL_API float RtzFromM0M2(double, double);

}
}
