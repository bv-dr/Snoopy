#pragma once
#include <stdexcept>
#include <Eigen/Dense>
#include "SpectralExport.hpp"
#include "SeaState.hpp"
#include "Rao.hpp"
#include "Math/Integration/simpson.hpp"



namespace BV
{
namespace Spectral
{

/**
 * @class ResponseSpectrum
 *
 */
class SPECTRAL_API ResponseSpectrum
{

protected:

    using ModesType = Eigen::Array<Modes, Eigen::Dynamic, 1>;

    const SeaStateABC& seaState_;

    const Rao& rao_;

    // Number of threads to use in the calculation
    int num_threads_ = 1;  

    // Frequency step
    double dw_ ;

    // Heading step
    double dh_ ;

    // Response spectrum value at RAO frequency location
    Eigen::ArrayXXd rSpec_ ;
    Eigen::ArrayXd m0_;
    Eigen::ArrayXd m2_;
    Eigen::ArrayXd m4_;

    // Compute the response spectrum at a given wave frequency. Set isComputed to True
    void compute() ;

    bool isComputed_;

    bool computeM4_;

    inline int _get_imode(int imode) const;


public:


    /**
     * @param seaState : sea-wave spectrum
     * @param rao : transfer function (already interpolated and symetrized)
     */
    ResponseSpectrum(const SeaStateABC& seaState, const Rao& rao, int num_threads = 1, bool checkRao = true, bool computeM4_ = false) ;

    // Getter for the response spectrum
    inline const Eigen::ArrayXXd&  get();

    // Getter for the rao
    inline const Rao&  getRao() const;

    // Return "2D spectrum" (not stored)
    Eigen::ArrayXXd compute2D(int imode = -1) const;

    // Getter for the seaState
    inline const SeaStateABC&  getSeaState() const;

    // Getter (or lazy evaluation) for the moment (m0 and m2). Only for one Modes case !
    inline double getM0(int imode = -1) ;
    inline double getM2(int imode = -1) ;
    inline double getM4(int imode = -1);

    inline Eigen::ArrayXd getM0s();
    inline Eigen::ArrayXd getM2s();
    inline Eigen::ArrayXd getM4s();

    inline std::tuple<double, double> getM0M2(int imode) ;

    inline Eigen::ArrayXd getFrequencies();

    inline Eigen::Array<Modes, Eigen::Dynamic, 1> getModes() const;

    inline int getNModes() const;

} ;


//Getter for response spectrum (lazy evaluation)
const Eigen::ArrayXXd& ResponseSpectrum::get()
{
    if ( isComputed_ == false )
        compute();

    return rSpec_;
}

int ResponseSpectrum::_get_imode(int imode) const
{
    if ( (imode == -1)  & ( getNModes() == 1 ) )
    {
        return 0 ;
    }
    else if (imode == -1) 
    {
        throw std::invalid_argument("Mode position should be specified");
    }
    else
    {
        if (imode >= getNModes()) 
        {
            throw std::invalid_argument("imode out of bounds");
        }
        else 
        {
            return imode;
        }
        
    }
}

//Getter for moment (lazy evaluation)
double ResponseSpectrum::getM0( int imode )
{
    if ( isComputed_ == false )
        compute();
    return m0_(_get_imode(imode));
}

//Getter for moment (lazy evaluation)
double ResponseSpectrum::getM2(int imode)
{
    if ( isComputed_ == false )
        compute();
    return m2_(_get_imode(imode));
}

//Getter for moment (lazy evaluation)
double ResponseSpectrum::getM4(int imode)
{
    if (computeM4_ == false)
        throw std::invalid_argument("M4 not calculated, use ResponseSpectrum( ... , computeM4 = True) to enable m4 calculation");
    if (isComputed_ == false)
        compute();
    return m4_(_get_imode(imode));
}

inline Eigen::ArrayXd ResponseSpectrum::getM0s()
{
    if (isComputed_ == false)
        compute();
    return m0_;
}

inline Eigen::ArrayXd ResponseSpectrum::getM2s()
{
    if (isComputed_ == false)
        compute();
    return m2_;
}


//Getter for moment (lazy evaluation)
inline Eigen::ArrayXd ResponseSpectrum::getM4s()
{
    if (computeM4_ == false)
        throw std::invalid_argument("M4 not calculated, use ResponseSpectrum( ... , computeM4 = True) to enable m4 calculation");
    if (isComputed_ == false)
        compute();
    return m4_;
}


//Getter for moment (lazy evaluation)
std::tuple<double, double> ResponseSpectrum::getM0M2(int imode)
{
    int i = _get_imode(imode);
    if ( isComputed_ == false )
        compute();
    return std::make_tuple( m0_(i), m2_(i) ) ;
}

inline Eigen::ArrayXd ResponseSpectrum::getFrequencies()
{
    return rao_.getFrequencies();
}

inline Eigen::Array<Modes, Eigen::Dynamic, 1> ResponseSpectrum::getModes() const
{
    return rao_.getModes();
}

inline int ResponseSpectrum::getNModes() const
{
    return rao_.getNModes();
}


const Rao& ResponseSpectrum::getRao() const
{
    return rao_;
}

const SeaStateABC& ResponseSpectrum::getSeaState() const
{
    return seaState_;
}





}
}
