#pragma once

#include "SpectralExport.hpp"
#include "ParametricSpectrum.hpp"
#include <set>

namespace BV
{
namespace Spectral
{

/**
 * @class LogNormal
 *
 * @brief Define a LogNormal spectrum.
 */
class SPECTRAL_API LogNormal : public ParametricSpectrum
{
public:

    LogNormal(double hs, double tp, double sigma, double heading = 0., SpreadingType spreadingType = SpreadingType::No, double spreadingValue = 0.)
        : ParametricSpectrum(name, heading, spreadingType, spreadingValue)
        , hs_(hs), tp_(tp), sigma_(sigma)
    {
        tailOrder_ = +1; // Not constant ==> > 0
        double wp = 2 * M_PI / tp;
        mu_ = log(wp) + pow(sigma_, 2);
    }

    static const char* name;

    // Significant wave height.
    double hs_;

    // Peak period.
    double tp_;

    // Peak parameter.
    double sigma_;

    double mu_;


    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;



    static double sigma_from_goda(double goda)
    {
        return 1. / (pow(M_PI, 0.5) * goda);
    };

    static double goda_from_sigma(double sigma)
    {
        return 1. / (pow(M_PI, 0.5) * sigma);
    };

    double getTz() const override
    {
        return 2 * M_PI *  sqrt( getMoment(0) / getMoment(2)) ;
    }


    double getTm() const override
    {
        return 2* M_PI / std::exp( mu_ + 0.5*sigma_*sigma_);
    }

    double getStd() const
    {
        return pow((exp(sigma_ * sigma_) - 1) * exp(2 * mu_ + sigma_ * sigma_), 0.5);
    }

    double getMoment(int i) const override
    {
        double m0 = pow(getHs(), 2) / 16;
        return exp( i * mu_ + 0.5 * i * i * sigma_* sigma_) * m0;
    }

    static int getNParams() { return 3; }
    static std::vector<const char*> getCoefs_name() { return { "hs", "tp"  , "sigma" }; }
    std::vector<double> getCoefs() const override   { return {  hs_,  tp_  ,  sigma_ }; }
    static std::vector<double> getCoefs_0()         { return {  1. ,  10.  ,  0.2     }; }
    static std::vector<double> getCoefs_min()       { return {  0. ,  0.001,  0.001   }; }
    static std::vector<double> getCoefs_max()       { return {  30.,  40.  ,  2.000   }; }

    inline std::string print() const override
    {
        return ParametricSpectrum::print<LogNormal>();
    }

};

}
}
