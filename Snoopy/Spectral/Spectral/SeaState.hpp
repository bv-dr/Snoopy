#pragma once

#include "WaveSpectrum.hpp"
#include "Jonswap.hpp"
#include "SpectralExport.hpp"

// std
#include <vector>
#include <memory>
#include <iostream>

// Eigen
#include <Eigen/Dense>


namespace BV
{
namespace Spectral
{

class WaveSpectrum;
class ParametricSpectrum;
class WaveTabulatedSpectrum;

/**
 * The available sea states types.
 */
enum class SeaStateType
{
    Parametric = 0,   // Fully parametric spectra (Jonswap... )
    SemiParametric,   // Tabulated in frequency + spreading
    Full              // Full 2D density
};


class SPECTRAL_API SeaStateABC
{
public :

    virtual ~SeaStateABC()
    {
    }

    /**
     * Compute the global spectrum considering all spectrum modes.
     *
     * @param w The list of wave frequencies for which to compute S(w).
     * @return The global density spectrum.
     */
    virtual Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const=0;


    /**
     * Compute the global directional spectrum considering all spectrum modes.
     *
     * @param w The list of wave frequencies for which to compute S(w).
     * @param heading The direction of propagation in radians.
     * @return The global density spectrum.
     */
    virtual Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double heading) const;

    /**
     * Compute the global directional spectrum considering all spectrum modes. The
     * vector of angles has to be of the same size as the vector of frequencies.
     *
     * @param w The list of wave frequencies for which to compute S(w).
     * @param headings The directions of propagation in radians.
     * @return The global density spectrum.
     */
    virtual Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const=0;


	virtual double getTailOrder() const 
    {
        return -5.;
    };


    // getSpectrumCount and getSpectrum are only relevant for the SeaState subclass. 
    // But reference in ResponseSpectrum.compute(). So we put here dummy default function.
    // There is probably a cleaner way
    virtual inline size_t getSpectrumCount() const
    {
        return 1;
    };

    virtual inline const WaveSpectrum& getSpectrum(size_t index) const
    {
        throw std::invalid_argument("No getSpectrum available for this SeaState");
        return Jonswap( 1. , 10., 1.0, 0.0, Spectral::SpreadingType::No, 2.0);
    };
       

    /**
     * Compute the directional density (integration in frequency)
     */
    virtual Eigen::ArrayXd computeSpreading(const Eigen::ArrayXd& b) const=0;

    virtual double compute(double w) const;

    virtual double compute(double w, double heading) const;

    inline virtual SeaStateType getType() const;

    inline double getProbability() const;

    // Check if sea-state is unidirectional (single wave direction, without any spreading)
    inline virtual bool isUnidirectional() const=0;

    // Check if sea-state is spreaded (so that directional integration make sense)
    inline virtual bool isSpreaded() const = 0;


protected:
    /**
     * The sea state probability of occurence.
     */
    double probability_;

    /**
     * The sea state type.
     */
    SeaStateType type_;

};

inline SeaStateType SeaStateABC::getType() const
{
    return type_;
}

inline double SeaStateABC::getProbability() const
{
    return probability_;
}





/**
 * @class SeaState
 *
 * Describes a multi-modal sea state. The sea state is composed of a list of 1D spectrum (or 1D with spreading)
 */
class SPECTRAL_API SeaState: public SeaStateABC
{
public:


    ~SeaState();

    /**
     * Create a parametric sea state.
     */
    explicit SeaState(std::vector<std::shared_ptr<ParametricSpectrum>> spectrums, double probability = 1.0);
	explicit SeaState(std::vector<std::shared_ptr<WaveTabulatedSpectrum>> spectrums, double probability = 1.0);

    /**
     * For convenience, create a parametric sea state using only one spectrum.
     */
    explicit SeaState(std::shared_ptr<ParametricSpectrum> spectrum, double probability = 1.0);

    /**
     * Create a sea state defined by a tabulated spectrum.
     */
    explicit SeaState(std::shared_ptr<WaveTabulatedSpectrum> spectrum, double probability = 1.0);


    inline size_t getSpectrumCount() const;

    inline const WaveSpectrum& getSpectrum(size_t index) const;

    double getHs() const;

	double getTailOrder() const;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double heading) const override;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const override;

    Eigen::ArrayXd computeSpreading(const Eigen::ArrayXd& b) const override;

    inline bool isUnidirectional() const;

    inline bool isSpreaded() const;
private:

    /**
     * The list of spectrums defining the sea state.
     */
    std::vector<std::shared_ptr<WaveSpectrum>> spectrums_;

};


inline size_t SeaState::getSpectrumCount() const
{
    return spectrums_.size();
}

inline const WaveSpectrum& SeaState::getSpectrum(size_t index) const
{
    assert(index < spectrums_.size());
    return *spectrums_[index].get();
}

inline bool SeaState::isUnidirectional() const
{
    bool unidirectional(true);
    double head1 = getSpectrum(0).heading;
    for(auto spec : spectrums_)
        unidirectional = unidirectional && (spec->getSpreadingType() == Spectral::SpreadingType::No) && ( spec->getHeading()==head1 ) ;
    return unidirectional;
}

inline bool SeaState::isSpreaded() const
{
    for (auto spec : spectrums_)
        if (spec->getSpreadingType() == Spectral::SpreadingType::No) { return false; };
    return true;
}


/**
 * @class SeaState
 *
 * Describes a sea-state, from full 2D density. Density at arbitrary frequency and heading is interpolated
 */
class SPECTRAL_API SeaState2D: public SeaStateABC
{

public :
    /**
     * Create a 2D spectrum arrayXXd
     */
    SeaState2D(const Eigen::ArrayXd& w ,const Eigen::ArrayXd& b,const Eigen::ArrayXXd& sw , double probability = 1.0)
:w_(w), b_(b) , sw_(sw)
{
    probability_ = probability;
    type_  = SeaStateType::Full;

    // Shape
    if (sw.rows() != w.rows()) 
    {
        throw std::invalid_argument("Data and frequency shape does not match");
    }

    if (sw.cols() != b.rows())
    {
        throw std::invalid_argument("Data and heading shape does not match");
    }

    // Check that b_ is "closed"  b_[-1] = b_[0]
    if ( b_[0] != 0 ||   b_[ b_.size()-1 ] != 2*M_PI )
    {
        std::cout << b_[0] * 180. / M_PI << " " << b_[b_.size() - 1]*180. / M_PI << std::endl;
        throw std::invalid_argument("Headings bound should be 0-360 for now") ;
    }
}

    /**
     * Create from parametric spectrum
     */

    SeaState2D(const SeaStateABC& ss, double wmin = 0.1, double wmax = 3.0 , int nbFreq = 150, int nbHead = 72) ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const override;
    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, double heading) const override;
    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;
    Eigen::ArrayXd computeSpreading(const Eigen::ArrayXd& b) const override;

    inline Eigen::ArrayXd getFrequencies() const ;
    inline Eigen::ArrayXd getHeadings() const ;
    inline Eigen::ArrayXXd getDensity() const ;
    inline bool isUnidirectional() const { return false; };
    inline bool isSpreaded() const { return true; };


protected:

    Eigen::ArrayXd w_;

    Eigen::ArrayXd b_;

    Eigen::ArrayXXd sw_;

};

    inline Eigen::ArrayXd SeaState2D::getFrequencies() const {return w_;}
    inline Eigen::ArrayXd SeaState2D::getHeadings() const {return b_;}
    inline Eigen::ArrayXXd SeaState2D::getDensity() const {return sw_;}
    



/**
    * @class SeaState
    *
    * Describes a sea-state, from tabulated frequency and fourier coefficients
    */
class SPECTRAL_API SeaState2D_Fourier : public SeaStateABC
{

public:
    /**
      * Create a 2D spectrum arrayXXd
      */
    SeaState2D_Fourier(const Eigen::ArrayXd& w, const Eigen::ArrayXd& a0,
                                                const Eigen::ArrayXd& a1,
                                                const Eigen::ArrayXd& b1,
                                                const Eigen::ArrayXd& a2,
                                                const Eigen::ArrayXd& b2,
                                                double probability = 1.0)
        : w_(w), a0_(a0), a1_(a1), b1_(b1), a2_(a2), b2_(b2)
    {
        probability_ = probability;
        type_ = SeaStateType::Full;
    }

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w, const Eigen::Ref<const Eigen::ArrayXd>& headings) const override;


    inline Eigen::ArrayXd get_a0() const { return a0_; }
    inline Eigen::ArrayXd get_a1() const { return a1_; }
    inline Eigen::ArrayXd get_a2() const { return a2_; }
    inline Eigen::ArrayXd get_b1() const { return b1_; }
    inline Eigen::ArrayXd get_b2() const { return b2_; }

    inline Eigen::ArrayXd getFrequencies() const { return w_; }

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const override;

    // To implement later on    
    Eigen::ArrayXd computeSpreading(const Eigen::ArrayXd& b) const override;

    inline bool isUnidirectional() const { return false; };
    inline bool isSpreaded() const { return true; };

protected:

    Eigen::ArrayXd w_;
    Eigen::ArrayXd a0_;
    Eigen::ArrayXd a1_;
    Eigen::ArrayXd b1_;
    Eigen::ArrayXd a2_;
    Eigen::ArrayXd b2_;
};


}
}
