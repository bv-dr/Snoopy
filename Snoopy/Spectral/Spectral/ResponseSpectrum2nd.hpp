#pragma once
#include <stdexcept>
#include <Eigen/Dense>
#include "SpectralExport.hpp"
#include "SeaState.hpp"
#include "Qtf.hpp"
#include "MQtf.hpp"
#include "Math/Integration/simpson.hpp"
#include "Spectral/ResponseSpectrum2ndABC.hpp"

namespace BV
{
namespace Spectral
{

/**
 * @class ResponseSpectrum2nd
 *
 */
class SPECTRAL_API ResponseSpectrum2nd  : public ResponseSpectrum2ndABC<Qtf>
{

protected:
    // Compute the response spectrum at a given wave frequency. Set isComputed to True
    void compute() ;
    Eigen::ArrayXd getNewmanUD_(const Eigen::ArrayXd& dQtf, const Eigen::ArrayXd& specFreqs, const int meanType = 0);

public:

    ResponseSpectrum2nd(const SeaState & seaState, const Qtf & qtf) :
        ResponseSpectrum2ndABC(seaState, qtf) {}
    double getMean() const;
    /**
     * @brief getNewman : returns the Newman approximation of the 2nd order Response Spectrum
     * @param meanType :
     *  0 - Geometric mean (default): QTF(w1, w2) = \sqrt(|QTF(w1,w1) QTF(w2,w2)|) \sgn(QTF(w1, w1))
     *  1 - Arithmetic mean         : QTF(w1, w2) = 1/2 (QTF(w1,w1) +QTF(w2,w2))
     * @return 2nd order Response Spectrum
     */
    Eigen::ArrayXd getNewman(const int meanType = 0);
    Eigen::ArrayXd getNewman(const Eigen::ArrayXXd &dQtf, const int meanType = 0);
} ;
}
}
