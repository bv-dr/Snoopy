#pragma once

#include <iostream>
#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class Hino
 *
 * @brief Define a Hino wind spectrum.
 */
class SPECTRAL_API Hino: public Spectrum
{
public:

    Hino(double meanVelocity, double C, double z, double vwz,
         double alpha, double heading=0.) :
        Spectrum("Hino", heading, meanVelocity), C(C), z(z), vwz(vwz),
        zOverTen(z / 10.), alpha(alpha),
        sigma2(5.6 * C * vwz * vwz * std::pow(1. / zOverTen, 2. * alpha))
    {
    }

    double C ;
    double z ;
    double vwz ;
    double zOverTen ;
    double alpha ;
    double sigma2 ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 5 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "height", "vwz", "alpha"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C, z, vwz, alpha} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.002, 10., 10., 0.16} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0., 0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1e3, 70., 1.} ;}
} ;

}
}
