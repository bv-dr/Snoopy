#pragma once

#include "SpectralExport.hpp"

#include "Tools/BVException.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "TransferFunction.hpp"
#include "SpectralTools.hpp"

namespace BV {
namespace Spectral {

template <int Rank_, typename Derived>
class RealTensorStorage
{
public:
    using IndexType = Eigen::Index ;
    enum {
        Rank = Rank_
    };
protected:
    std::array<Eigen::ArrayXd, Rank> axes_ ;
    Eigen::Tensor<double, Rank> data_ ;

    template <typename TensorType>
    Eigen::Tensor<double, Rank> getData_(IndexType axisIndex,
                                      const Eigen::ArrayXd & values,
                                      const TensorType & data,
                                      const Math::Interpolators::InterpScheme & interpScheme,
                                      const Math::Interpolators::ExtrapolationType & extrapType) const
    {
        if (axisIndex >= static_cast<IndexType>(Rank))
            throw BV::Tools::Exceptions::BVException("Error: Index out of range") ;
        if (interpScheme == Math::Interpolators::InterpScheme::LINEAR)
        {
            return Math::Interpolators::Linear1D::get(axes_[axisIndex],
                                                      axisIndex,
                                                      data, values,
                                                      extrapType) ;
        }
        throw BV::Tools::Exceptions::BVException(
            "Error: not implemented interpolator scheme") ;
    }
public:
    RealTensorStorage(const std::array<Eigen::ArrayXd, Rank> & axes,
                      const Eigen::Tensor<double, Rank> & data) :
        axes_(axes), data_(data)
    {
    }

    const Eigen::ArrayXd & getAxis(IndexType index) const
    {
        if (index >= static_cast<IndexType>(Rank))
            throw BV::Tools::Exceptions::BVException(
                "Error: axis index out of range (getAxis)") ;
        return axes_[index] ;
    }

    Eigen::Tensor<double, Rank> getData(IndexType axisIndex,
                                     const Eigen::ArrayXd & values,
                                     const Math::Interpolators::InterpScheme & interpScheme,
                                     BV::Math::Interpolators::ExtrapolationType extrapType) const
    {
        return getData_(axisIndex, values, data_, interpScheme, extrapType) ;
    }

    Eigen::Tensor<double, Rank> getData(IndexType axisIndex,
                                     const Eigen::ArrayXd & values,
                                     const Eigen::DSizes<IndexType, static_cast<IndexType>(Rank)> & offsets,
                                     const Eigen::DSizes<IndexType, static_cast<IndexType>(Rank)> & extents,
                                     const Math::Interpolators::InterpScheme & interpScheme,
                                     BV::Math::Interpolators::ExtrapolationType extrapType) const
    {
            Eigen::Tensor<double, Rank> re(data_.slice(offsets, extents)) ;
            return getData_(0, values,
                            //ampData_.slice(offsets, extents).eval(), // 18s
                            re, // 13.8s
                            interpScheme, extrapType) ;
    }

    const Eigen::Tensor<double, Rank> & getData() const
    {
        return data_ ;
    }

    Derived& operator+=(const Derived & rhs)
    {
        data_ += rhs.getData() ;
        return static_cast<Derived&>(*this) ;
    }

    Derived operator+(const Derived & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy += rhs ;
        return copy ;
    }

    Derived & operator-=(const Derived & rhs)
    {
        data_ -= rhs.getData() ;
        return static_cast<Derived&>(*this) ;
    }

    Derived operator-(const Derived & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy -= rhs ;
        return copy ;
    }

    Derived & operator*=(double rhs)
    {
        data_ = data_ * rhs ;
        return static_cast<Derived&>(*this) ;
    }

    Derived operator*(double rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy *= rhs ;
        return copy ;
    }

    Derived & operator/=(double rhs)
    {
        data_ = 1. / rhs * data_ ;
        return static_cast<Derived&>(*this) ;
    }

    Derived operator/(double rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy /= rhs ;
        return copy ;
    }

} ;

/**
 * @class Qtf0
 *
 * @brief Define a quadratic transfer function
 */
class SPECTRAL_API Qtf0: public HydroTransferFunction<RealTensorStorage<3, Qtf0> >
{
public:
    using Storage = RealTensorStorage<3, Qtf0> ;
    using IndexType = typename Storage::IndexType ;

    ~Qtf0()
    {
    }

    /**
     * Construct a Qtf0 using its parameters directly.
     *
     * @param b The heading list.
     * @param w The frequency list.
     * @param modeCoefficients A coefficient on each of the last axis values.
     * @param modes The labels (enum values) corresponding to each of the
     *     modes of the last Qtf axis
     * @param data The data value
     * @param refPoint The point in body on which the qtf values were calculated
     * @param waveRefPoint The point in body according to which the values
     *     phases were calculated.
     * @param speed
     * TODO add metadata* @param metadata
     */
    Qtf0(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
         const ModesType & modes, const Eigen::Tensor<double, 3> & data,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         double forwardSpeed = 0.,
         double depth=-1.) ;

    Qtf0(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Tensor<double, 3> & data,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         double forwardSpeed = 0.,
         double depth=-1.) ;

    Qtf0(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w, const ModesType & modes,
         const Eigen::TensorRef<const Eigen::Tensor<double, 3> > & data,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         double forwardSpeed = 0.,
         double depth=-1.) ;

    Eigen::ArrayXXd getAtHeadings(
        const Eigen::ArrayXd & relativeHeadings,
        const std::vector<unsigned> & indices,
        const BV::Math::Interpolators::InterpScheme & interpScheme,
        BV::Math::Interpolators::ExtrapolationType extrapType) const ;

    Qtf0 getQtfAtFrequencies(
        const Eigen::ArrayXd & ws,
        const Math::Interpolators::InterpScheme & interpScheme,
        BV::Math::Interpolators::ExtrapolationType extrapType) const ;
} ;

}
}
