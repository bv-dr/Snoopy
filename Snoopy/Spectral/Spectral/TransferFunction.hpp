#pragma once

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "SpectralTools.hpp"
#include "Math/Interpolators/Interpolators.hpp"

namespace BV {
namespace Spectral {

enum struct ComplexInterpolationStrategies
{
    RE_IM, RE_IM_AMP, AMP_PHASE
} ;

enum struct Optimisation{
    CPU, RAM
} ;

enum struct Modes
{
    NONE, SURGE, SWAY, HEAVE, ROLL, PITCH, YAW, FX, FY, FZ, MX, MY, MZ, WAVE, RWE, SECTFX, SECTFY, SECTFZ, SECTMX, SECTMY, SECTMZ, 
    WATERVELOCITY_X, WATERVELOCITY_Y, WATERVELOCITY_Z, PRESSURE, 
    VSURGE, VSWAY, VHEAVE, VROLL, VPITCH, VYAW, ASURGE, ASWAY, AHEAVE, AROLL ,APITCH, AYAW,
    CA_11,CA_12,CA_13,CA_14,CA_15,CA_16,
    CA_21,CA_22,CA_23,CA_24,CA_25,CA_26,
    CA_31,CA_32,CA_33,CA_34,CA_35,CA_36,
    CA_41,CA_42,CA_43,CA_44,CA_45,CA_46,
    CA_51,CA_52,CA_53,CA_54,CA_55,CA_56,
    CA_61,CA_62,CA_63,CA_64,CA_65,CA_66,
    CM_11,CM_12,CM_13,CM_14,CM_15,CM_16,
    CM_21,CM_22,CM_23,CM_24,CM_25,CM_26,
    CM_31,CM_32,CM_33,CM_34,CM_35,CM_36,
    CM_41,CM_42,CM_43,CM_44,CM_45,CM_46,
    CM_51,CM_52,CM_53,CM_54,CM_55,CM_56,
    CM_61,CM_62,CM_63,CM_64,CM_65,CM_66,
    AXIAL_LINESTRESS,PT1_LINESTRESS,PT2_LINESTRESS,PT3_LINESTRESS,PT4_LINESTRESS,
    BOTTOM_X_STRESS,BOTTOM_Y_STRESS,BOTTOM_XY_STRESS,TOP_X_STRESS,
    TOP_Y_STRESS,TOP_XY_STRESS,BOTTOM_ENVELOPE_STRESS,TOP_ENVELOPE_STRESS
};

template <int Rank_, typename Derived,
          typename ComplexStorageType_=Eigen::Tensor<std::complex<double>, Rank_>,
          typename ModuleStorageType_=Eigen::Tensor<double, Rank_>,
          typename PhasisStorageType_=Eigen::Tensor<double, Rank_>,
          typename RealStorageType_=Eigen::Tensor<double, Rank_>,
          typename ImagStorageType_=Eigen::Tensor<double, Rank_> >
class AllTensorsStorage
{
public:
    enum {
        Rank = Rank_
    } ;
    using ComplexStorageType = ComplexStorageType_ ;
    using ModuleStorageType = ModuleStorageType_ ;
    using PhasisStorageType = PhasisStorageType_ ;
    using RealStorageType = RealStorageType_ ;
    using ImagStorageType = ImagStorageType_ ;
    using IndexType = Eigen::Index ;

protected:
    template <typename Derived1, typename Derived2, typename Derived3,
              typename Derived4>
    ComplexStorageType getComplexData_(
                         IndexType axisIndex,
                         const Derived1 & amp, const Derived2 & phi,
                         const Derived3 & re, const Derived4 & im,
                         const Eigen::Ref<const Eigen::ArrayXd> & values,
                         const Math::Interpolators::InterpScheme & interpScheme,
                         const ComplexInterpolationStrategies & interpStrategy,
                         const Math::Interpolators::ExtrapolationType & extrapType
                                      ) const
    {
        if (axisIndex >= static_cast<IndexType>(Rank))
            throw BV::Tools::Exceptions::BVException("Error: Index out of range") ;
        if (interpScheme == Math::Interpolators::InterpScheme::LINEAR)
        {
            if (interpStrategy == ComplexInterpolationStrategies::AMP_PHASE)
            {
                ModuleStorageType dataInterpAmp(
                    Math::Interpolators::Linear1D::get(axes_[axisIndex],
                                                       axisIndex,
                                                       amp, values,
                                                       extrapType)) ;
                PhasisStorageType dataInterpPhi(
                    Math::Interpolators::Linear1D::get(axes_[axisIndex],
                                                       axisIndex,
                                                       phi, values,
                                                       extrapType)) ;
                return EigenFuture::ComplexFromAmpPhi(dataInterpAmp, dataInterpPhi) ;
            }
            RealStorageType dataInterpRe(
                Math::Interpolators::Linear1D::get(axes_[axisIndex], axisIndex,
                                                   re, values, extrapType)) ;
            ImagStorageType dataInterpIm(
                Math::Interpolators::Linear1D::get(axes_[axisIndex], axisIndex,
                                                   im, values, extrapType)) ;
            ComplexStorageType dataInterpReIm(
                EigenFuture::ComplexFromReIm(dataInterpRe, dataInterpIm)) ;
            if (interpStrategy == ComplexInterpolationStrategies::RE_IM_AMP)
            {
                ModuleStorageType dataInterpAmp(
                    Math::Interpolators::Linear1D::get(axes_[axisIndex], axisIndex,
                                                       amp, values, extrapType)) ;
                return EigenFuture::ComplexFromAmpPhi(
                    dataInterpAmp, EigenFuture::arg(dataInterpReIm)) ;
            }
            return dataInterpReIm ;
        }
        throw BV::Tools::Exceptions::BVException(
            "Error: not implemented interpolator scheme") ;
    }

    std::array<Eigen::ArrayXd, Rank> axes_ ;
    ComplexStorageType complexData_ ;
    ModuleStorageType ampData_ ;
    PhasisStorageType phiData_ ;
    mutable RealStorageType reData_ ;
    mutable ImagStorageType imData_ ;

    Optimisation optimisation_ ;

    // refresh reData_ and imData if necessary. To call EVERY time data are modified by internal function
    void refresh_() 
    {
        ampData_ = complexData_.abs();
        phiData_ = EigenFuture::arg(complexData_);
        reData_ = RealStorageType(); // Empty reData, that will be evaluated lazily if necessary
        imData_ = ImagStorageType();
    }

public:
    AllTensorsStorage(const std::array<Eigen::ArrayXd, Rank> & axes,
                      const ModuleStorageType & dataAmp,
                      const PhasisStorageType & dataPhi,
                      Optimisation optimisation=Optimisation::CPU) :
        axes_(axes),
        complexData_(EigenFuture::ComplexFromAmpPhi(dataAmp, dataPhi)),
        ampData_(dataAmp), phiData_(dataPhi),
        optimisation_(optimisation)
    {
    }

    AllTensorsStorage(const std::array<Eigen::ArrayXd, Rank> & axes,
                      const ComplexStorageType & complexData,
                      Optimisation optimisation=Optimisation::CPU) :
        axes_(axes), complexData_(complexData), ampData_(complexData.abs()),
        phiData_(EigenFuture::arg(complexData)),
        optimisation_(optimisation)
    {
    }

    const Eigen::ArrayXd & getAxis(IndexType index) const
    {
        if (index >= static_cast<IndexType>(Rank))
            throw BV::Tools::Exceptions::BVException(
                "Error: axis index out of range (getAxis)") ;
        return axes_[index] ;
    }

    ComplexStorageType getComplexData(
        IndexType axisIndex, const Eigen::ArrayXd & values,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const Math::Interpolators::ExtrapolationType & extrapType) const
    {
        if (optimisation_ == Optimisation::CPU)
        {
            if (reData_.size() == 0)
            {
                getReal() ;
                getImag() ;
            }
            return getComplexData_(axisIndex, ampData_, phiData_,
                                   reData_, imData_,
                                   values, interpScheme, interpStrategy,
                                   extrapType) ;
        }
        else
        {
            return getComplexData_(axisIndex, ampData_, phiData_,
                                   getReal(), getImag(),
                                   values, interpScheme, interpStrategy,
                                   extrapType) ;
        }
    }

    ComplexStorageType getComplexData(
        IndexType axis1Index, IndexType axis2Index,
        const Eigen::ArrayXd & values1, const Eigen::ArrayXd & values2,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const Math::Interpolators::ExtrapolationType & extrapType) const
    {
        if ((axis1Index >= static_cast<IndexType>(Rank)) || (axis2Index >= static_cast<IndexType>(Rank)))
            throw BV::Tools::Exceptions::BVException("Error: Index out of range") ;
        if (interpScheme == Math::Interpolators::InterpScheme::LINEAR)

        {
            if (interpStrategy == ComplexInterpolationStrategies::AMP_PHASE)
            {
                ModuleStorageType dataInterpAmp(
                    Math::Interpolators::Linear2D::get(this->axes_[axis1Index],
                                                       this->axes_[axis2Index],
                                                       axis1Index, axis2Index,
                                                       this->ampData_,
                                                       values1, values2,
                                                       extrapType)) ;
                PhasisStorageType dataInterpPhi(
                    Math::Interpolators::Linear2D::get(this->axes_[axis1Index],
                                                       this->axes_[axis2Index],
                                                       axis1Index, axis2Index,
                                                       this->phiData_, values1, values2,
                                                       extrapType)) ;
                return EigenFuture::ComplexFromAmpPhi(dataInterpAmp, dataInterpPhi) ;
            }
            RealStorageType dataRe(this->complexData_.real()) ;
            ImagStorageType dataIm(this->complexData_.imag()) ;
            RealStorageType dataInterpRe(
                    Math::Interpolators::Linear2D::get(this->axes_[axis1Index],
                                                       this->axes_[axis2Index],
                                                       axis1Index, axis2Index,
                                                       dataRe, values1, values2,
                                                       extrapType)) ;
            ImagStorageType dataInterpIm(
                    Math::Interpolators::Linear2D::get(this->axes_[axis1Index],
                                                       this->axes_[axis2Index],
                                                       axis1Index, axis2Index,
                                                       dataIm, values1, values2,
                                                       extrapType)) ;
            ComplexStorageType dataInterpReIm(
                EigenFuture::ComplexFromReIm(dataInterpRe, dataInterpIm)) ;
            if (interpStrategy == ComplexInterpolationStrategies::RE_IM_AMP)
            {
                ModuleStorageType dataInterpAmp(
                    Math::Interpolators::Linear2D::get(this->axes_[axis1Index],
                                                       this->axes_[axis2Index],
                                                       axis1Index, axis2Index,
                                                       this->ampData_, values1, values2,
                                                       extrapType)) ;
                return EigenFuture::ComplexFromAmpPhi(
                    dataInterpAmp, EigenFuture::arg(dataInterpReIm)) ;
            }
            return dataInterpReIm ;
        }
        throw BV::Tools::Exceptions::BVException(
            "Error: not implemented interpolator scheme") ;
    }

    ModuleStorageType getModules(
        IndexType axisIndex, const Eigen::ArrayXd & values,
        const Math::Interpolators::InterpScheme & interpScheme,
        const Math::Interpolators::ExtrapolationType & extrapType) const
    {
        if (axisIndex >= 3)
            throw BV::Tools::Exceptions::BVException("Error: Index out of range") ;
        if (interpScheme == Math::Interpolators::InterpScheme::LINEAR)
        {
            return Math::Interpolators::Linear1D::get(axes_[axisIndex],
                                                      axisIndex,
                                                      ampData_, values,
                                                      extrapType) ;
        }
        throw BV::Tools::Exceptions::BVException(
            "Error: not implemented interpolator scheme") ;
    }

    const ModuleStorageType & getModules() const
    {
        return ampData_ ;
    }
    const PhasisStorageType & getPhases() const
    {
        return phiData_ ;
    }

    PhasisStorageType getPhases(
        IndexType axisIndex, const Eigen::ArrayXd & values,
        const Math::Interpolators::InterpScheme & interpScheme,
        const Math::Interpolators::ExtrapolationType & extrapType) const
    {
        if (axisIndex >= 3)
            throw BV::Tools::Exceptions::BVException("Error: Index out of range") ;
        if (interpScheme == Math::Interpolators::InterpScheme::LINEAR)
        {
            return Math::Interpolators::Linear1D::get(axes_[axisIndex],
                                                      axisIndex,
                                                      phiData_, values,
                                                      extrapType) ;
        }
        throw BV::Tools::Exceptions::BVException(
            "Error: not implemented interpolator scheme") ;
    }

    RealStorageType getReal() const
    {
        if (optimisation_ == Optimisation::RAM)
        {
            return complexData_.real() ;
        }
        if (optimisation_ == Optimisation::CPU)
        {
            if (reData_.size() == 0)
            {
                reData_ = complexData_.real() ;
            }
            return reData_ ;
        }
        throw BV::Tools::Exceptions::BVException("Unknown optimisation") ;
    }

    ImagStorageType getImag() const
    {
        if (optimisation_ == Optimisation::RAM)
        {
            return complexData_.imag() ;
        }
        if (optimisation_ == Optimisation::CPU)
        {
            if (imData_.size() == 0)
            {
                imData_ = complexData_.imag() ;
            }
            return imData_ ;
        }
        throw BV::Tools::Exceptions::BVException("Unknown optimisation") ;
    }

    const ComplexStorageType & getComplexData() const
    {
        return complexData_ ;
    }

    Derived& operator+=(const Derived & rhs)
    {
        complexData_ += rhs.complexData_ ;
        this->refresh_();
        return static_cast<Derived&>(*this) ;
    }

    Derived operator+(const Derived & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy += rhs ;
        return copy ;
    }

    Derived & operator-=(const Derived & rhs)
    {
        complexData_ -= rhs.complexData_ ;
        this->refresh_();
        return static_cast<Derived&>(*this) ;
    }

    Derived operator-(const Derived & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy -= rhs ;
        return copy ;
    }

    Derived & operator*=(const double & rhs)
    {
        complexData_ = std::complex<double>(rhs, 0.) * complexData_ ;
        this->refresh_();
        return static_cast<Derived&>(*this) ;
    }

    Derived operator*(const double & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy *= rhs ;
        return copy ;
    }

    Derived & operator/=(const double & rhs)
    {
        complexData_ = std::complex<double>(1. / rhs, 0.) * complexData_ ;
        this->refresh_();
        return static_cast<Derived&>(*this) ;
    }

    Derived operator/(const double & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy /= rhs ;
        return copy ;
    }

    Derived & operator*=(const std::complex<double> & rhs)
    {
        complexData_ = complexData_ * rhs ;
        this->refresh_();
        return static_cast<Derived&>(*this) ;
    }

    Derived operator*(const std::complex<double> & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy *= rhs ;
        return copy ;
    }

    Derived & operator/=(const std::complex<double> & rhs)
    {
        complexData_ = complexData_ * (1. / rhs) ;
        this->refresh_();
        return static_cast<Derived&>(*this) ;
    }

    Derived operator/(const std::complex<double> & rhs) const
    {
        Derived copy(static_cast<const Derived &>(*this)) ;
        copy /= rhs ;
        return copy ;
    }
} ;

template <typename StorageType>
class TransferFunction : public StorageType
{
protected:
// The reference point of the transfer function
    Eigen::Vector3d refPoint_ ;

// The reference point of the wave origin (phasing issue)
    Eigen::Vector2d waveRefPoint_ ;

public:
    using IndexType = typename StorageType::IndexType ;

    TransferFunction(const StorageType & storage,
                     const Eigen::Vector3d & refPoint,
                     const Eigen::Vector2d & waveRefPoint) :
        StorageType(storage), refPoint_(refPoint), waveRefPoint_(waveRefPoint)
    {
    }

    inline const Eigen::Vector3d getReferencePoint() const
    {
        return refPoint_ ;
    }

    inline void setReferencePoint(const Eigen::Vector3d & refPoint)
    {
        refPoint_ = refPoint ;
    }

    inline const Eigen::Vector2d getWaveReferencePoint() const
    {
        return waveRefPoint_ ;
    }

    inline void setWaveReferencePoint(const Eigen::Vector2d & waveRefPoint)
    {
        waveRefPoint_ = waveRefPoint ;
    }

} ;

template <typename StorageType>
class HydroTransferFunction: public TransferFunction<StorageType>
{
protected:
    using ModesType=Eigen::Array<Modes, Eigen::Dynamic, 1> ;
    ModesType modes_ ;
    double forwardSpeed_ ;
    double depth_ ;

public:
    using IndexType = typename TransferFunction<StorageType>::IndexType ;
    enum {
        Rank = StorageType::Rank
    } ;

    HydroTransferFunction(const StorageType & storage,
                          const ModesType & modes,
                          const Eigen::Vector3d & refPoint,
                          const Eigen::Vector2d & waveRefPoint,
                          double forwardSpeed,
                          double depth=-1.) :
        TransferFunction<StorageType>(storage, refPoint, waveRefPoint),
        modes_(modes), forwardSpeed_(forwardSpeed), depth_(depth)
    {
    }

    inline IndexType getNHeadings() const
    {
        return this->getAxis(0).size() ;
    }

    inline IndexType getNFrequencies() const
    {
        return this->getAxis(1).size() ;
    }

    inline IndexType getClosestHeadingIndex(double heading) const
    {
        return BV::Math::Interpolators::GetClosest(this->getHeadings(), heading) ;
    }

    inline const Eigen::ArrayXd& getHeadings() const
    {
        return this->getAxis(0) ;
    }

    inline const Eigen::ArrayXd getHeadingIn2PiRange() const {
        Eigen::ArrayXd heading(this->getAxis(0));
        auto n = heading.size();
        for(decltype(n) i = 0; i < n; ++i) {
            while(heading[i] < 0)
                heading[i] += 2*M_PI;
            while(heading[i] >2*M_PI)
                heading[i] -= 2*M_PI;
        }
        return heading;
    }

    inline const Eigen::ArrayXd& getFrequencies() const
    {
        return this->getAxis(1) ;
    }

    inline double getForwardSpeed() const
    {
        return forwardSpeed_ ;
    }

    inline double getDepth() const
    {
        return depth_ ;
    }

    inline ModesType getModes() const
    {
        return modes_ ;
    }

    inline IndexType getNModes() const
    {
        return this->getAxis(static_cast<IndexType>(Rank) - 1).size() ;
    }

    inline const Eigen::ArrayXd& getModeCoefficients() const
    {
        return this->getAxis(static_cast<IndexType>(Rank) - 1) ;
    }
} ;
}
}
