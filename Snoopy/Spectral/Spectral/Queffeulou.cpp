#include "Queffeulou.hpp"
#include "SpectralTools.hpp"

using namespace BV::Spectral ;

Eigen::ArrayXd Queffeulou::compute(
    const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if ((almost_equal(Ri, 0.)) || (almost_equal(C10, 0.)))
        return Eigen::ArrayXd::Zero(w.size()) ;

    Eigen::ArrayXd sw(w.size()) ;
    const double a = 2.05 * C10 * meanValue / Ri * z ;
    const double b = z / meanValue / Ri ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi = w(i) / doublePi ;
        sw[i] = a / (1. + 0.5206675 * std::pow(b * fi, 5. / 3.)) ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
