#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class Harris
 *
 * @brief Define a Harris wind spectrum.
 */
class SPECTRAL_API Harris: public Spectrum
{
public:

    Harris(double meanVelocity, double C, double L, double heading=0.) :
        Spectrum("Harris", heading, meanVelocity), C(C), L(L)
    {
    }

    // k Surface drag coefficient
    double C ;

    //Representative length scale
    double L ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 3 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "representativeLengthScale"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C, L} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.002, 1800.} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0.} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1e5} ;}
} ;

}
}
