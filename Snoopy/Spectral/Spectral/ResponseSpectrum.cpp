#include <iostream>
#include "ResponseSpectrum.hpp"

using namespace BV::Spectral;

ResponseSpectrum::ResponseSpectrum( const SeaStateABC& seaState, const Rao& rao, int num_threads, bool checkRao, bool computeM4 ) :
     seaState_(seaState), rao_(rao), num_threads_(num_threads), computeM4_(computeM4)
    {
    dw_ = rao_.getFrequencies()(1) - rao_.getFrequencies()(0);
    dh_ = rao_.getHeadings()(1) - rao_.getHeadings()(0);
    isComputed_ = false ;
    m0_ = Eigen::ArrayXd::Zero( rao_.getNModes() ) ;
    m2_ = Eigen::ArrayXd::Zero(rao_.getNModes());

    if (computeM4_) { m4_ = Eigen::ArrayXd::Zero(rao_.getNModes()); }

    if (checkRao)
        { 
        if (rao_.isReadyForSpectral() == false)
            {
            throw std::logic_error("RAO should be interpolated with constant step, and no duplicate, before spectral calculation");
            }
        }
    }


// Compute response spectrum at all rao frequency, In wave frequency
void ResponseSpectrum::compute()
    {

        size_t nbhead (rao_.getHeadings().size());
        size_t nbfreq (rao_.getFrequencies().size());

        rSpec_ = Eigen::ArrayXXd(nbfreq, rao_.getNModes()) ;
        Eigen::ArrayXXd rSpecSpr(nbhead, nbfreq);

        rSpec_ = 0.0 ;
        rSpecSpr = 0.0;
        const Eigen::Tensor<double, 3> & modules(rao_.getModules()) ;
        m0_ = 0.0;
        m2_ = 0.0;

        if (computeM4_) { m4_ = 0.0; }

        const auto we = rao_.getEncFrequencies();

        if (seaState_.getType() == SeaStateType::Full)
        {
            throw std::logic_error("Response spectrum on full spectra not implemented yet");
        }

        
        int nmod(seaState_.getSpectrumCount());
        std::vector< Eigen::ArrayXd > swVector;
        for (size_t i = 0; i < nmod; ++i)
        {
            const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
            swVector.push_back ( spectrum.compute(rao_.getFrequencies()) );
        }

        // Loop on all RAOs
        for (auto iMode = 0; iMode < rao_.getNModes(); ++iMode)
        {
            // integrate on all spectra that constitute overall sea-state
            for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
            {
                  // TODO: optimize! spectrum is calculated nrao times!
                const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
                Eigen::ArrayXd rSpec = Eigen::ArrayXd::Zero(nbfreq);  // Response Spectrum for the current wave spectrum

                if (spectrum.getSpreadingType() == SpreadingType::No)
                {
                   
                    // Get closest heading
                    Eigen::ArrayXd::Index ihead ;
                    //double headDiff =
                    Eigen::abs((  rao_.getHeadings() - spectrum.heading  )).minCoeff ( &ihead );

                    //Compute response spectra
                    for (size_t iw=0; iw<nbfreq; ++iw)
                    {
                        rSpec(iw) = std::pow(modules(ihead, iw, iMode), 2.) * swVector[i](iw) ;
                    }
                    //Compute moments
                    /*
                     * Compute moments:
                     * The zero moment m_0 is independent on encounter frequency, which is heading dependent variable
                     * thus, the total moment m_0 can be calculated using the total response spectrum.
                     * On the other side, the second moment m_2 is the integral of the product of
                     * the response spectrum and encounter frequency squared, which is dependent on the heading.
                     * So, if there are two spectrums with different headings
                     * \int S_R(w, \theta_1) w_e^2(\theta_1) dw +\int S_R(w, \theta_2) w_e^2(\theta_2) dw
                     *  != \int (S_R(w, \theta_1) +S_R(w, \theta_2)) w_e^2 ( ??? ) dw
                     * That is why m2 is calculated for each spectrum and added to the total second moment
                     */

                    m0_(iMode) += rSpec.sum() * dw_;
                    m2_(iMode) += ( rSpec * we.row(ihead).pow(2).transpose()).sum() * dw_ ;
                    if (computeM4_) { m4_(iMode) += (rSpec * we.row(ihead).pow(4).transpose()).sum() * dw_; }
                }
                else
                {
                    Eigen::ArrayXd spectrumHeads = spectrum.computeSpreading(rao_.getHeadings());
                    // S(\omega, \theta) = S_0(\omega) x G(\theta), where
                    // spectrumFreqs : S_0(\omega) is calculated by spectrum.compute(rao_.getFrequencies());
                    // spectrumHeads : G(\theta)   is calculated by spectrum.computeSpreading(rao_.getHeadings());

                    // Response spectrum density: RAO^2(\omega, \theta) *S(\omega, \theta)
                    for (size_t ih = 0; ih < nbhead; ++ih)
                    {
                        for (size_t iw = 0; iw < nbfreq; ++iw)
                        {
                            rSpecSpr(ih, iw) = std::pow(modules(ih, iw, iMode), 2.) * swVector[i](iw) * spectrumHeads(ih);
                        }
                    }

                    //Response spectrum integrated over all direction
                    for (size_t i = 0; i < nbfreq; ++i)
                    {
                        rSpec(i) = rSpecSpr.col(i).sum() * dh_;
                    }

                    //Integrate moments
                    for (size_t ih = 0; ih < nbhead; ++ih)
                    {
                        m2_(iMode) += (rSpecSpr.row(ih) * we.row(ih) * we.row(ih)).sum() * dw_ * dh_;
                    }

                    if (computeM4_) 
                    {
                        for (size_t ih = 0; ih < nbhead; ++ih)
                        {
                            m4_(iMode) += (rSpecSpr.row(ih) * we.row(ih).pow(4)  ).sum() * dw_ * dh_;
                        }
                    }

                    m0_(iMode) += rSpec.sum() * dw_;
                }
                rSpec_.block(0, iMode, nbfreq, 1) += rSpec;
            }
        }
        isComputed_ = true ;
   }



// Compute response spectrum and keep 2D information (For some EDW procedure, very few actual application cases)
Eigen::ArrayXXd ResponseSpectrum::compute2D(int imode ) const
{
    auto iMode_ = _get_imode(imode);

    size_t nbhead(rao_.getHeadings().size());
    size_t nbfreq(rao_.getFrequencies().size());

    Eigen::ArrayXXd rSpecSpr(nbhead, nbfreq);
    rSpecSpr = 0.0;

    const Eigen::Tensor<double, 3> & modules(rao_.getModules());
    const auto we = rao_.getEncFrequencies();

    if (seaState_.getType() == SeaStateType::Full)
    {
        throw std::logic_error("Response spectrum on full spectra not implemented yet");
    }

    // integrate on all spectra that constitute overall sea-state
    for (size_t i = 0; i < seaState_.getSpectrumCount(); ++i)
    {
        const WaveSpectrum& spectrum = seaState_.getSpectrum(i);
        Eigen::ArrayXd spectrumFreqs(spectrum.compute(rao_.getFrequencies()));
        Eigen::ArrayXd rSpec = Eigen::ArrayXd::Zero(nbfreq);  // Response Spectrum for the current wave spectrum

        if (spectrum.getSpreadingType() == SpreadingType::No)
        {
            // Get closest heading
            Eigen::ArrayXd::Index ihead;
            //double headDiff =
            Eigen::abs((rao_.getHeadings() - spectrum.heading)).minCoeff(&ihead);

            //Compute response spectra
            for (size_t iw = 0; iw < nbfreq; ++iw)
            {
                rSpecSpr(ihead, iw) += std::pow(modules(ihead, iw, iMode_), 2.) * spectrumFreqs(iw);
            }
        }
        else
        {
            Eigen::ArrayXd spectrumHeads = spectrum.computeSpreading(rao_.getHeadings());
            // S(\omega, \theta) = S_0(\omega) x G(\theta), where
            // spectrumFreqs : S_0(\omega) is calculated by spectrum.compute(rao_.getFrequencies());
            // spectrumHeads : G(\theta)   is calculated by spectrum.computeSpreading(rao_.getHeadings());

            // Response spectrum density: RAO^2(\omega, \theta) *S(\omega, \theta)
            for (size_t ih = 0; ih < nbhead; ++ih)
            {
                for (size_t iw = 0; iw < nbfreq; ++iw)
                {
                    rSpecSpr(ih, iw) += std::pow(modules(ih, iw, iMode_), 2.) * spectrumFreqs(iw) * spectrumHeads(ih);
                }
            }
        }
    }
    return rSpecSpr;
}


