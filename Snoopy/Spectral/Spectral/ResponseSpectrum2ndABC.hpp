#pragma once
#include <stdexcept>
#include <Eigen/Dense>
#include "SpectralExport.hpp"
#include "SeaState.hpp"
#include "Qtf.hpp"
#include "MQtf.hpp"
#include "Math/Integration/simpson.hpp"



namespace BV
{
namespace Spectral
{

/**
 * @class ResponseSpectrum2ndABC: base class for ResponseSpectrum2nd and ResponseSpectrum2nd_MQtf
 *
 */
template <class QtfType>
class ResponseSpectrum2ndABC
{

protected:
    const SeaState& seaState_;

    /*
     * qtf_ is a common for all classes. It corresponds to simple Qtf or MQtf type
     */
    const QtfType & qtf_;

    double dw_ ;

    // Response spectrum value at QTF frequency location
    Eigen::ArrayXd rSpec_ ;
    /**
     * @brief W_ array of 2nd order frequencies: for sum: 2w_min, 2w_min+dw_, ..., 2w_max
     */
    Eigen::ArrayXd W_ ;
    const Eigen::ArrayXd &freqs_;
    Eigen::Index nWs_;
    double m0_;
    double m2_;

    /*
     * Compute the response spectrum at a given wave frequency. Set isComputed to True
     * This function must be implemented by all child class
     */
    virtual void compute() = 0;

    const Eigen::ArrayXd &headings_;
    Eigen::Index nHs_;
    double dh_;
    const QtfMode mode_;

    bool isComputed_;
public:
    /**
     * @param seaState : sea-wave spectrum
     * @param rao : transfer function (already interpolated and symetrized)
     */
    ResponseSpectrum2ndABC(const SeaState& seaState, const QtfType & qtf ) ;

    // Getter for the response spectrum
    inline const Eigen::ArrayXd&  get();

    const Eigen::ArrayXd& getFrequencies() const {return W_;}

    // Getter for the qtf or mqtf
    const QtfType &  getQtf() const {return qtf_;}

    // Getter for the seaState
    const SeaState&  getSeaState() const {return seaState_;}

    bool isComputed() const {return isComputed_;}

    // Getter (or lazy evaluation) for the moment (m0 and m2)
    inline double getM0() ;
    inline double getM2() ;
    inline std::tuple<double, double> getM0M2() ;

} ;

template <class QtfType>
ResponseSpectrum2ndABC<QtfType>::ResponseSpectrum2ndABC(const SeaState &seaState, const QtfType &qtf) :
    seaState_(seaState),
    qtf_(qtf),
    freqs_(qtf.getFrequencies()),
    m0_(-1.), m2_(-1.),
    headings_(qtf.getHeadings()),
    mode_(qtf.getMode()),
    isComputed_(false)
{
    dw_ = freqs_(1) -freqs_(0);
    nHs_ = headings_.size();
    if (nHs_ > 1)
        dh_ = headings_(1) -headings_(0);
    else
        dh_ = 0.0;
    if (mode_ == QtfMode::SUM)
    {
        double w_min2 = freqs_(0);
        double w_max2 = freqs_(freqs_.size()-1);
        w_min2 += w_min2;
        w_max2 += w_max2;
        nWs_ = static_cast<int>((w_max2 -w_min2)/dw_)+1;
        W_ = Eigen::ArrayXd::LinSpaced(nWs_, w_min2, w_max2);
    } else
    {
        double w_min = 0.0;
        double w_max = qtf_.getDwMax();
        nWs_ = static_cast<int>((w_max -w_min)/dw_)+1;
        W_ = Eigen::ArrayXd::LinSpaced(nWs_, w_min, w_max);
    }
    rSpec_ = Eigen::ArrayXd::Zero(nWs_);
}

//Getter for response spectrum (lazy evaluation)
template <class QtfType>
const Eigen::ArrayXd& ResponseSpectrum2ndABC<QtfType>::get()
{
    if ( isComputed_ == false )
        compute();

    return rSpec_;
}

//Getter for moment (lazy evaluation)
template <class QtfType>
double ResponseSpectrum2ndABC<QtfType>::getM0()
{
    if ( isComputed_ == false )
        compute();
    return m0_;
}

//Getter for moment (lazy evaluation)
template <class QtfType>
double ResponseSpectrum2ndABC<QtfType>::getM2()
{
    if ( isComputed_ == false )
        compute();
    return m2_;
}

//Getter for moment (lazy evaluation)
template <class QtfType>
std::tuple<double, double> ResponseSpectrum2ndABC<QtfType>::getM0M2()
{
    if ( isComputed_ == false )
        compute();
    return std::make_tuple( m0_, m2_ ) ;
}

}
}
