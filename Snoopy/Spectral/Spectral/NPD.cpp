#include "NPD.hpp"

using namespace BV::Spectral ;

Eigen::ArrayXd NPD::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{

    Eigen::ArrayXd sw(Eigen::ArrayXd::Zero(w.size())) ;
    double doublePi(2 * M_PI) ;
    double n(0.468) ;
    double meanOverTen(meanValue / 10.) ;
    double squaredMeanOverTen(meanOverTen * meanOverTen) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi(w[i] / doublePi) ;
        double fprime(172. * fi * std::pow(meanOverTen, -0.75)) ;
        sw[i] = 320. * squaredMeanOverTen
            / std::pow(1 + std::pow(fprime, n), 5. / (3. * n)) ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}


Eigen::ArrayXd NPD::compute(const Eigen::Ref<const Eigen::ArrayXd>& w,
                            double hAboveSea) const
{

    Eigen::ArrayXd sw(Eigen::ArrayXd::Zero(w.size())) ;
    double doublePi(2 * M_PI) ;
    double n(0.468) ;
    double meanOverTen(meanValue / 10.) ;
    double hOverTen(hAboveSea / 10.) ;
    double squaredMeanOverTen(meanOverTen * meanOverTen) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi(w[i] / doublePi) ;
        double fprime(172. * fi * std::pow(hOverTen, 2./3.) * std::pow(meanOverTen, -0.75)) ;
        sw[i] = 320. * squaredMeanOverTen * std::pow(hOverTen, 0.45)
            / std::pow(1 + std::pow(fprime, n), 5. / (3. * n)) ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
