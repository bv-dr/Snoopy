#include "Kaimal.hpp"
#include "SpectralTools.hpp"

using namespace BV::Spectral ;

Eigen::ArrayXd Kaimal::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    if ((almost_equal(z, 0.)) || (almost_equal(C10, 0.)))
        return Eigen::ArrayXd::Zero(w.size()) ;

    Eigen::ArrayXd sw(w.size()) ;
    const double a = 200. * z * C10 * meanValue ;
    const double b = 50. * z / meanValue ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi = w(i) / doublePi ;
        sw[i] = a / pow(1. + b * fi, 5. / 3.) ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
