#include "WhiteNoise.hpp"

using namespace BV::Spectral;

const char* WhiteNoise::name = "WhiteNoise";

Eigen::ArrayXd WhiteNoise::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    Eigen::ArrayXd sw(Eigen::ArrayXd::Zero(w.size()));

    double c = hs_ * hs_ / (8. * (w4_ + w3_ - w2_ - w1_));

    for (auto i = 0; i < w.size(); ++i)
    {
        double wi = w[i];
        if (wi < w4_)
        {
            if (wi > w3_)
                sw[i] = c * (w4_ - wi) / (w4_ - w3_);
            else if (wi > w2_)
                sw[i] = c;
            else if (wi > w1_)
                sw[i] = c * (w1_ - wi) / (w1_ - w2_);
        }
    }

    return sw;
}


std::tuple<double, double> WhiteNoise::get_wrange(double energyRatio) const
{
    return std::tuple<double, double>( w1_, w4_);
}