#include "Kareem.hpp"

using namespace BV::Spectral;

Eigen::ArrayXd Kareem::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{

    Eigen::ArrayXd sw(Eigen::ArrayXd::Zero(w.size())) ;
    double doublePi(2*M_PI) ;
    for (auto i = 0; i < w.size(); ++i)
    {
        double fi(w[i]/doublePi) ;
        double fstar(fi*z/vwz) ;
        double sf(335.*fstar/std::pow(1.+71.*fstar, 5./3.)) ;
        sw[i] = sf*ustar2/fi ;
    }
    // convert spectral density from m2/Hz to m2/(rad/s)
    sw /= doublePi ;
    return sw ;
}
