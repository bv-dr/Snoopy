#pragma once
#include "SpectralExport.hpp"
#include <Eigen/Dense>
#include <memory>
#include "Spectrum.hpp"
#include "Spreading.hpp"

namespace BV {
namespace Spectral {


/**
 * @class Spectrum
 *
 * @brief Define a wave spectrum, directional or not.
 */
class SPECTRAL_API WaveSpectrum: public Spectrum
{
public:

    WaveSpectrum(std::string name, double heading, SpreadingType spreadingType,double spreadingValue) ;


    virtual ~WaveSpectrum()
    {
    }

    std::string getSpreadingFunctionName() const ;

    using Spectrum::compute ;

    /**
     * Compute the spectral density taking the directional spreading function into account.
     *
     * @param w The wave frequencies.
     * @param heading The direction of propagation in radians.
     * @return sw The wave density spectrum S(w).
     */
    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w,
                           double heading) const ;

    /**
     * Compute the spectral density taking the directional spreading function into account. The
     * vector of angles has to be of the same size as the vector of frequencies.
     *
     * @param w The wave frequencies.
     * @param headings The directions of propagation in radians.
     * @return sw The wave density spectrum S(w).
     */
    Eigen::ArrayXd compute(
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& headings) const ;

    /**
     * Compute the directional spreading.
     *
     * @param angle The direction of propagation in radians.
     */
    double computeSpreading(double headings) const ;

    Eigen::ArrayXd computeSpreading(const Eigen::ArrayXd& headings) const ;

    // Get Hs (exact for parametric spectra, integrated for tabulated)
    virtual double getHs() const {throw std::logic_error( "getHs : not implemented for this wave spectrum" );};

    // Get Hs (exact for parametric spectra, integrated for tabulated)
    virtual double getM0() const { return pow( getHs(), 2 ) / 16. ; };

    // Return moments
    virtual double getMoment(int i) const  { throw std::logic_error( "getMoment : not yet implemented for this wave spectrum" ); }  ;

    // Return Tm
    virtual double getTm() const  { return 2*M_PI * getMoment(0) / getMoment(1); };

    // Return Tz
    virtual double getTz() const  { return 2*M_PI * pow(getMoment(0) / getMoment(2), 0.5); };

    // Return Tp
    virtual double getTp() const { throw std::logic_error("getTp : not yet implemented for this wave spectrum"); };

    // Return Steepness, based on up-crossing period
    double getSt() const
    {
        return 2*M_PI*getHs() / ( 9.81 * pow(getTz(), 2))  ;
    };

    // Return Steepness, based on mean period
    double getSt_Tm() const
    {
        return 2 * M_PI*getHs() / (9.81 * pow(getTm(), 2));
    };

    //Return Ursell number
    double getUr(double depth) const;

    //Return coefficient for Forristall crest distribution (fitted on 2nd order).
    std::tuple<double, double> getForristall_alpha_beta(double depth = 0.) const;

    std::shared_ptr<Spreading> spreading;

    //TODO : remove ? (included in spreading)
    inline SpreadingType getSpreadingType() const ;

    inline double getSpreadingValue() const ;


    virtual std::tuple<double, double> get_wrange(double energyRatio = 0.99) const;

} ;





SpreadingType WaveSpectrum::getSpreadingType() const
{
    return spreading->getType();
}

double WaveSpectrum::getSpreadingValue() const
{
    return spreading->getValue();
}





}
}
