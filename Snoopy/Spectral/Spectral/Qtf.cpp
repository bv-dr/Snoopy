#include "Spectral/Qtf.hpp"

using namespace BV::Spectral ;
using namespace BV::Math ;

Qtf::Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& dw,
         const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
         const ModesType & modes, const Eigen::Tensor<double, 4> & dataAmp,
         const Eigen::Tensor<double, 4> & dataPhi,
         const QtfStorageType & qtfStorageType,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         const QtfMode & mode, double forwardSpeed, double depth) :
    HydroTransferFunction<Storage>(
        Storage( { b, w, dw, modeCoefficients },
                typename Storage::ModuleStorageType(dataAmp, qtfStorageType),
                typename Storage::PhasisStorageType(dataPhi, qtfStorageType)),
        modes, refPoint, waveRefPoint, forwardSpeed, depth),
    sumMode_(mode),
    dwMax_(
        (qtfStorageType == QtfStorageType::W_DW) ?
            dw.maxCoeff() : (w(w.size() - 1) - w(0)))
{
    if (qtfStorageType == QtfStorageType::W_W)
    {
        // FIXME we adopt the convention to store the dw on axis_[2]
        // This is necessary to interpolate the values either in W_W or W_DW
        // This also implies that the frequencies are uniformly spaced
        // TODO it also could be useful to provide a dwMax value.
        using IndexType = Eigen::ArrayXd::Index ;
        IndexType nws(w.size()) ;
        Eigen::ArrayXd dw(nws) ;
        double w0(w(0)) ;
        for (IndexType iw = 0; iw < nws; ++iw)
        {
            dw(iw) = w(iw) - w0 ;
        }
        this->axes_[2] = dw ;
    }
}

Qtf::Qtf(
    const Eigen::Ref<const Eigen::ArrayXd>& b,
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const Eigen::Ref<const Eigen::ArrayXd>& dw,
    const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
    const ModesType & modes,
    const Eigen::TensorRef<const Eigen::Tensor<std::complex<double>, 4> > & dataReIm,
    const QtfStorageType & qtfStorageType,
    const Eigen::Ref<const Eigen::Vector3d>& refPoint,
    const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint, const QtfMode & mode,
    double forwardSpeed, double depth) :
    HydroTransferFunction<Storage>(
        Storage( { b, w, dw, modeCoefficients },
                typename Storage::ComplexStorageType(dataReIm, qtfStorageType)),
        modes, refPoint, waveRefPoint, forwardSpeed, depth),
    sumMode_(mode),
    dwMax_(
        (qtfStorageType == QtfStorageType::W_DW) ?
            dw.maxCoeff() : (w(w.size() - 1) - w(0)))
{
    if (qtfStorageType == QtfStorageType::W_W)
    {
        // FIXME we adopt the convention to store the dw on axis_[2]
        // This is necessary to interpolate the values either in W_W or W_DW
        // This also implies that the frequencies are uniformly spaced
        // TODO it also could be useful to provide a dwMax value.
        using IndexType = Eigen::ArrayXd::Index ;
        IndexType nws(w.size()) ;
        Eigen::ArrayXd dw(nws) ;
        double w0(w(0)) ;
        for (IndexType iw = 0; iw < nws; ++iw)
        {
            dw(iw) = w(iw) - w0 ;
        }
        this->axes_[2] = dw ;
    }
}

Qtf::Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& dw,
         const Eigen::Tensor<double, 4> & dataAmp,
         const Eigen::Tensor<double, 4> & dataPhi,
         const QtfStorageType & qtfStorageType,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         const QtfMode & mode, double forwardSpeed, double depth) :
    Qtf(b, w, dw, Eigen::ArrayXd::Zero(dataAmp.dimension(3)),
        ModesType::Constant(dataAmp.dimension(3), Modes::NONE), dataAmp,
        dataPhi, qtfStorageType, refPoint, waveRefPoint, mode, forwardSpeed,
        depth)
//To add enum for Qtf type (Pressure, loads, motion... ). This allows easy scaling
{
}

Qtf::Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& dw, const ModesType & modes,
         const Eigen::TensorRef<const Eigen::Tensor<double, 4> > & dataAmp,
         const Eigen::TensorRef<const Eigen::Tensor<double, 4> > & dataPhi,
         const QtfStorageType & qtfStorageType,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         const QtfMode & mode, double forwardSpeed, double depth) :
    Qtf(b, w, dw, Eigen::ArrayXd::Zero(dataAmp.dimension(3)), modes, dataAmp,
        dataPhi, qtfStorageType, refPoint, waveRefPoint, mode, forwardSpeed,
        depth)
//To add enum for Qtf type (Pressure, loads, motion... ). This allows easy scaling
{
}

Qtf::Qtf(
    const Eigen::Ref<const Eigen::ArrayXd>& b,
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const Eigen::Ref<const Eigen::ArrayXd>& dw,
    const Eigen::TensorRef<const Eigen::Tensor<std::complex<double>, 4> > & dataReIm,
    const QtfStorageType & qtfStorageType,
    const Eigen::Ref<const Eigen::Vector3d>& refPoint,
    const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint, const QtfMode & mode,
    double forwardSpeed, double depth) :
    Qtf(b, w, dw, Eigen::ArrayXd::Zero(dataReIm.dimension(3)),
        ModesType::Constant(dataReIm.dimension(3), Modes::NONE), dataReIm,
        qtfStorageType, refPoint, waveRefPoint, mode, forwardSpeed, depth)
//To add enum for Qtf type (Pressure, loads, motion... ). This allows easy scaling
{
}

Qtf::Qtf(
    const Eigen::Ref<const Eigen::ArrayXd>& b,
    const Eigen::Ref<const Eigen::ArrayXd>& w,
    const Eigen::Ref<const Eigen::ArrayXd>& dw,
    const ModesType & modes,
    const Eigen::TensorRef<const Eigen::Tensor<std::complex<double>, 4> > & dataReIm,
    const QtfStorageType & qtfStorageType,
    const Eigen::Ref<const Eigen::Vector3d>& refPoint,
    const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint, const QtfMode & mode,
    double forwardSpeed, double depth) :
    Qtf(b, w, dw, Eigen::ArrayXd::Zero(dataReIm.dimension(3)), modes, dataReIm,
        qtfStorageType, refPoint, waveRefPoint, mode, forwardSpeed, depth)
//To add enum for Qtf type (Pressure, loads, motion... ). This allows easy scaling
{
}

Qtf::Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
         const Eigen::Ref<const Eigen::ArrayXd>& w,
         const Eigen::Ref<const Eigen::ArrayXd>& dw, const ModesType & modes,
         const typename Qtf::Storage::ComplexStorageType & complexData,
         const Eigen::Ref<const Eigen::Vector3d>& refPoint,
         const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
         const QtfMode & mode, double forwardSpeed, double depth) :
    HydroTransferFunction<Storage>(
        Storage( { b, w, dw, Eigen::ArrayXd::Zero(complexData.dimension(2)) },
                complexData),
        modes, refPoint, waveRefPoint, forwardSpeed, depth),
    sumMode_(mode), dwMax_(dw.maxCoeff())
//To add enum for Qtf type (Pressure, loads, motion... ). This allows easy scaling
{
}

Qtf::Qtf(const MQtf &mqtf) :
    Qtf(mqtf.getHeadings(),                                         // Eigen::ArrayXd   : b
        mqtf.getFrequencies(),                                      // Eigen::ArrayXd   : w
        mqtf.getDeltaFrequencies(),                                 // Eigen::ArrayXd   : dw
        Eigen::ArrayXd::Zero(mqtf.getNModes()),                     // Eigen::ArrayXd   : modeCoefficients
        mqtf.getModes(),                                            // ModesType        : modes
        mqtf.getQtfLikeComplexData(),                               // Qtf::Storage     : reIm
        QtfStorageType::W_DW,                                       // QtfStorageType   : W_DW
        mqtf.getReferencePoint(),                                   // Eigen::Vector3d  : refPoint
        mqtf.getWaveReferencePoint(),                               // Eigen::Vector2d  : waveRefPoint
        mqtf.getMode(),                                             // QtfMode          : sum or dif mode
        mqtf.getForwardSpeed(),                                     // double           : forward speed
        mqtf.getDepth())                                            // double           : depth
{
    // Construct Qtf based on MQtf:
    // Qtf(w, w+dw, b) = MQtf(w, b1 = b, w+dw, b2 = b) for all modes
}

Eigen::Tensor<std::complex<double>, 4> Qtf::getMQtfLikeComplexData() const
{
    auto nbhead(getHeadings().size());
    auto nbfreq(getFrequencies().size());
    auto nbdiff(getDeltaFrequencies().size());
    //auto nbmodes(getModes().size());
    auto data(getComplexData());
    //Eigen::Tensor<std::complex<double>, 4> res(nbhead, nbfreq, nbdiff, nbhead, nbmodes);
    Eigen::Tensor<std::complex<double>, 4> res(nbhead, nbfreq, nbdiff, nbhead);
    res.setZero();
    for(Eigen::Index b = 0; b < nbhead; b++)
        for(Eigen::Index w = 0; w < nbfreq; w++)
            for(Eigen::Index dw = 0; dw < nbdiff; dw++)
                //for(Eigen::Index m = 0; m < nbmodes; m++)
                res(b, w, dw, b) = data.getFromiWjDW(b, w, dw, 0);
    return res;
}

double Qtf::getSumMode() const
{
    return static_cast<double>(sumMode_) ;
}

QtfMode Qtf::getMode() const
{
    return sumMode_ ;
}

typename Qtf::Storage::ComplexStorageType Qtf::getComplexAtHeading(
    double relativeHeading, // only 1 heading per couple of ws
    const BV::Math::Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const BV::Math::Interpolators::ExtrapolationType & extrapType) const
{
    Eigen::ArrayXd head(1) ;
    head(0) = relativeHeading ;
    // Easy optimization when there is only one heading
    return this->getComplexData(0, head, interpScheme, interpStrategy,
                                extrapType) ;
}

typename Qtf::Storage::ComplexStorageType Qtf::getComplexAtW(
    double w,
    const BV::Math::Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const BV::Math::Interpolators::ExtrapolationType & extrapType) const
{
    Eigen::ArrayXd ws(1);
    ws(0) = w ;
    // Easy optimization when there is only one heading
    return this->getComplexData(1, ws, interpScheme, interpStrategy,
                                extrapType) ;
}

Qtf Qtf::getQtfAtFrequencies(
    const Eigen::ArrayXd & ws, const std::vector<Eigen::ArrayXd> & wj_wi,
    const Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const FrequencyInterpolationStrategies & frequencyInterpStrategy,
    const BV::Math::Interpolators::ExtrapolationType & extrapType) const
{
    using IndexType = Eigen::ArrayXd::Index ;
    IndexType nws(ws.size()) ;
    IndexType innerSize = 0 ;
    typename ComplexStorageType::ArrayXu dWSizes(wj_wi.size()) ;
    typename ComplexStorageType::ArrayXu dWSizesSum(wj_wi.size()) ;
    dWSizesSum(0) = 0 ;
    for (IndexType iw = 0; iw < nws; ++iw)
    {
        dWSizesSum(iw) = innerSize ;
        innerSize += wj_wi[iw].size() ;
        dWSizes(iw) = wj_wi[iw].size() ;
    }
    typename ComplexStorageType::Dimensions shape(complexData_.dimensions()) ;
    shape[1] = innerSize ;
    ComplexStorageType res(shape, dWSizes, dWSizesSum) ;
    this->setfrequencyInterpStrategy(frequencyInterpStrategy) ;
    Eigen::ArrayXd newAxis(innerSize) ;
    IndexType lastIndex(0) ;
    for (IndexType iw = 0; iw < nws; ++iw)
    {
        double wi(ws(iw)) ;
        Eigen::ArrayXd wiArray(1) ;
        wiArray << wi ;
        IndexType segmentSize(wj_wi[iw].size()) ;
        if (frequencyInterpStrategy == FrequencyInterpolationStrategies::W_DW)
        {
            // w, dw interpolation
            ComplexStorageType dataInterp(
                this->getComplexData(1, 2, wiArray, wj_wi[iw], interpScheme,
                                     interpStrategy, extrapType)) ;
            res.setFrequency(iw, dataInterp) ;
        }
        else if (frequencyInterpStrategy
            == FrequencyInterpolationStrategies::W_W)
        {
            // w, w interpolation
            ComplexStorageType dataInterp(
                this->getComplexData(1, 1, wiArray, ws.tail(nws - iw),
                                     interpScheme, interpStrategy,
                                     extrapType)) ;
            res.setFrequency(iw, dataInterp) ;
        }
        newAxis.segment(lastIndex, segmentSize) = wj_wi[iw] ;
        lastIndex += segmentSize ;
    }
    Qtf qtf(axes_[0], ws, newAxis, modes_, res, refPoint_, waveRefPoint_,
            sumMode_, forwardSpeed_, depth_) ;
    return qtf ;
}

Eigen::Tensor<std::complex<double>, 1> Qtf::get(
    double relativeHeading, double w, double dw,
    const BV::Math::Interpolators::InterpScheme & interpScheme,
    const ComplexInterpolationStrategies & interpStrategy,
    const FrequencyInterpolationStrategies & frequencyInterpStrategy,
    const BV::Math::Interpolators::ExtrapolationType & extrapType) const
{
    // FIXME extrapType not used yet in 3D linear interpolation!!
    this->setfrequencyInterpStrategy(frequencyInterpStrategy) ;
    if (interpScheme == Math::Interpolators::InterpScheme::LINEAR)
    {
        if (interpStrategy == ComplexInterpolationStrategies::AMP_PHASE)
        {
            Eigen::Tensor<double, 1> amp(
                BV::Math::Interpolators::Linear3D::get(getHeadings(),
                                                       getFrequencies(),
                                                       getDeltaFrequencies(),
                                                       ampData_,
                                                       relativeHeading, w, dw)
                                        ) ;
            Eigen::Tensor<double, 1> phi(
                BV::Math::Interpolators::Linear3D::get(getHeadings(),
                                                       getFrequencies(),
                                                       getDeltaFrequencies(),
                                                       phiData_,
                                                       relativeHeading, w, dw)
                                        ) ;
            return amp.template cast<std::complex<double> >() * (
                (std::complex<double>(0., 1.) * phi.template cast<std::complex<double> >()).exp()
                                                                ) ;
        }
        // FIXME recreating these temporary arrays all the time takes too long...
        //RealStorageType dataRe(getReal()) ;
        //ImagStorageType dataIm(getImag()) ;
        if (reData_.size() == 0)
        {
            reData_ = getReal() ;
            imData_ = getImag() ;
        }
        Eigen::Tensor<double, 1> re(
            BV::Math::Interpolators::Linear3D::get(getHeadings(),
                                                   getFrequencies(),
                                                   getDeltaFrequencies(),
                                                   reData_,
                                                   relativeHeading, w, dw)
                                    ) ;
        Eigen::Tensor<double, 1> im(
            BV::Math::Interpolators::Linear3D::get(getHeadings(),
                                                   getFrequencies(),
                                                   getDeltaFrequencies(),
                                                   imData_,
                                                   relativeHeading, w, dw)
                                    ) ;
        Eigen::Tensor<std::complex<double>, 1> dataInterpReIm(
            re.template cast<std::complex<double> >()
            + std::complex<double>(0., 1.) * im.template cast<std::complex<double> >()
                                                             ) ;
        if (interpStrategy == ComplexInterpolationStrategies::RE_IM_AMP)
        {
            Eigen::Tensor<double, 1> amp(
                BV::Math::Interpolators::Linear3D::get(getHeadings(),
                                                       getFrequencies(),
                                                       getDeltaFrequencies(),
                                                       ampData_,
                                                       relativeHeading, w, dw)
                                        ) ;
            Eigen::Tensor<std::complex<double>, 1> tmp ;
            tmp = dataInterpReIm.unaryExpr([](std::complex<double> x)
                                {
                                    return std::complex<double>(std::arg(x), 0.) ;
                                }) ;
            return amp.template cast<std::complex<double> >() * (
                (std::complex<double>(0., 1.) * tmp).exp()
                                                                ) ;
        }
        return dataInterpReIm ;
    }
    throw BV::Tools::Exceptions::BVException(
        "Error: not implemented interpolator scheme") ;
}

Eigen::Tensor<double, 5> Qtf::getLinearCoefsForHeadings(
        const ComplexInterpolationStrategies & interpStrategy
            ) const
{
    Eigen::Tensor<double, 5> coeffs(1, 1, 1, 1, 4);
    return coeffs;
}
