#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class NPD
 *
 * @brief Define a NPD wind spectrum.
 */
class SPECTRAL_API NPD: public Spectrum
{
public:

    NPD(double meanVelocity, double heading=0.) :
        Spectrum("NPD", heading, meanVelocity)
    {
    }

    using Spectrum::compute ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w,
                           double hAboveSea) const ;

    static int getNParams() { return 1 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity"} ;}
    std::vector<double> getCoefs() const            {return {meanValue } ;}
    static std::vector<double> getCoefs_0()         {return {10.} ;}
    static std::vector<double> getCoefs_min()       {return {0.} ;}
    static std::vector<double> getCoefs_max()       {return {70.} ;}
} ;

}
}
