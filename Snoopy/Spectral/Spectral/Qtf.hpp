#pragma once

#include "SpectralExport.hpp"

#include "Tools/BVException.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Spectral/TransferFunction.hpp"
#include "Spectral/SpectralTools.hpp"
#include "Spectral/QtfTensor.hpp"
#include "Spectral/MQtf.hpp"

namespace BV {
namespace Spectral {

template <int Rank_, typename Derived>
class QtfStorage: public AllTensorsStorage<Rank_, Derived, QtfTensorComplex,
    QtfTensorModule, QtfTensorPhasis, QtfTensorReal, QtfTensorImag>
{
public:
    enum
    {
        Rank = Rank_
    } ;

    using ParentType = AllTensorsStorage<Rank_, Derived,
    QtfTensorComplex,
    QtfTensorModule,
    QtfTensorPhasis,
    QtfTensorReal,
    QtfTensorImag > ;
    using IndexType = typename ParentType::IndexType ;
    using ComplexStorageType = typename ParentType::ComplexStorageType ;
    using ModuleStorageType = typename ParentType::ModuleStorageType ;
    using PhasisStorageType = typename ParentType::PhasisStorageType ;
    using RealStorageType = typename ParentType::RealStorageType ;
    using ImagStorageType = typename ParentType::ImagStorageType ;

    using AllTensorsStorage<Rank_, Derived, QtfTensorComplex, QtfTensorModule,
        QtfTensorPhasis, QtfTensorReal, QtfTensorImag>::AllTensorsStorage ;

    void setfrequencyInterpStrategy(
        const FrequencyInterpolationStrategies & strategy) const
    {
        this->complexData_.setfrequencyInterpStrategy(strategy) ;
        this->ampData_.setfrequencyInterpStrategy(strategy) ;
        this->phiData_.setfrequencyInterpStrategy(strategy) ;
    }

    inline IndexType getNDeltaFrequencies() const
    {
        return this->axes_[2].size() ;
    }

    inline const Eigen::ArrayXd& getDeltaFrequencies() const
    {
        return this->axes_[2] ;
    }

    using ParentType::getComplexData ;

} ;

/**
 * @class Qtf
 *
 * @brief Define a quadratic transfer function (FullQtf unidirectional)
 *
 * Note that the frequencies should be equally spaced as we are storing
 * the following tensor: (headings, frequencies, delta frequencies, modes)
 * With delta frequencies a vector of difference frequencies with the
 * corresponding step.
 * This is not the most general way of storing the values !!!
 */
class SPECTRAL_API Qtf : public HydroTransferFunction<QtfStorage<4, Qtf> >
{
private:
    QtfMode sumMode_ ;
    double dwMax_ ;
    ModuleStorageType re_ ;
    ModuleStorageType imag_ ;
public:

    using Storage = QtfStorage<4, Qtf> ;
    using IndexType = typename Storage::IndexType ;

    ~Qtf()
    {
    }

    /**
     * Construct a Qtf using its parameters directly.
     *
     * @param b The heading list.
     * @param w The frequency list.
     * @param dw The delta frequency list or the frequency list, depending on
     *      the QtfStorageType provided.
     *      If QtfStorageType::W_W, the same frequency list as for the second axis
     *      should be provided.
     * @param modeCoefficients A coefficient on each of the last axis values.
     * @param modes The labels (enum values) corresponding to each of the
     *     modes of the last Qtf axis
     * @param dataAmp The data module value
     * @param dataPhi The data phase value (in rad)
     * @param qtfStorageType The type of the qtf provided (w, w) or (w, dw)
     * @param refPoint The point in body on which the qtf values were calculated
     * @param waveRefPoint The point in body according to which the values
     *     phases were calculated.
     * @param mode Whether the frequencies are summed or subtracted.
     * @param speed
     * TODO add metadata* @param metadata
     */
    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw, // or w
        const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
        const ModesType & modes,
        const Eigen::Tensor<double, 4> & dataAmp,
        const Eigen::Tensor<double, 4> & dataPhi,
        const QtfStorageType & qtfStorageType,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw,// or w
        const Eigen::Ref<const Eigen::ArrayXd>& modeCoefficients,
        const ModesType & modes,
        const Eigen::TensorRef<const Eigen::Tensor<std::complex<double>, 4> > & dataReIm,
        const QtfStorageType & qtfStorageType,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw,// or w
        const Eigen::Tensor<double, 4> & dataAmp,
        const Eigen::Tensor<double, 4> & dataPhi,
        const QtfStorageType & qtfStorageType,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw,// or w
        const ModesType & modes,
        const Eigen::TensorRef<const Eigen::Tensor<double, 4> > & dataAmp,
        const Eigen::TensorRef<const Eigen::Tensor<double, 4> > & dataPhi,
        const QtfStorageType & qtfStorageType,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    /*
     *  Construct from complex values
     */
    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw, // or w
        const Eigen::TensorRef<const Eigen::Tensor<std::complex<double>, 4> > & dataReIm,
        const QtfStorageType & qtfStorageType,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw,// or w
        const ModesType & modes,
        const Eigen::TensorRef<const Eigen::Tensor<std::complex<double>, 4> > & dataReIm,
        const QtfStorageType & qtfStorageType,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    // Constructor used after frequencies interpolation
    Qtf(const Eigen::Ref<const Eigen::ArrayXd>& b,
        const Eigen::Ref<const Eigen::ArrayXd>& w,
        const Eigen::Ref<const Eigen::ArrayXd>& dw,// or w
        const ModesType & modes,
        const typename Storage::ComplexStorageType & complexData,
        const Eigen::Ref<const Eigen::Vector3d>& refPoint,
        const Eigen::Ref<const Eigen::Vector2d>& waveRefPoint,
        const QtfMode & mode=QtfMode::DIFF,
        double forwardSpeed = 0.,
        double depth=-1.) ;

    Qtf(const MQtf&);

    Eigen::Tensor<std::complex<double>, 4> getMQtfLikeComplexData() const ;

    double getSumMode() const ;
    QtfMode getMode() const ;

        typename Storage::ComplexStorageType getComplexAtHeading(
        double relativeHeading,
        const BV::Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const BV::Math::Interpolators::ExtrapolationType & extrapType) const ;

    typename Storage::ComplexStorageType getComplexAtW(
        double w,
        const BV::Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const BV::Math::Interpolators::ExtrapolationType & extrapType) const ;

    Qtf getQtfAtFrequencies(
        const Eigen::ArrayXd & ws,
        const std::vector<Eigen::ArrayXd> & wj_wi,
        const Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const FrequencyInterpolationStrategies & frequencyInterpStrategy,
        const BV::Math::Interpolators::ExtrapolationType & extrapType) const ;

    Eigen::Tensor<std::complex<double>, 1> get(
        double relativeHeading, double w, double dw,
        const BV::Math::Interpolators::InterpScheme & interpScheme,
        const ComplexInterpolationStrategies & interpStrategy,
        const FrequencyInterpolationStrategies & frequencyInterpStrategy,
        const BV::Math::Interpolators::ExtrapolationType & extrapType) const ;

    inline double getDwMax() const
    {
        return dwMax_ ;
    }

    /**
     * returns the coefficients to calculate linearly the both module and argument of ComplexData
     * Assumption: 0<= \beta <= 2\pi
     *   for \beta >= \beta_n => \beta \in [\beta_n, \beta_n+1}
     *   or if \beta < \beta_0 => \beta \in [\beta_N, \beta_0]
     *      Mod_n = a_n \beta +b_n 
     *      \phi_n = c_n \beta +d_n for \be
     * where coefficients are calculated depending on the interpStrategy variable
     *  The first index corresponds to the heading index
     *  The second index corresponds to the frequency
     *  The third index corresponds to the delta frequency
     *  The forth index corresponds to the mode
     *  The fifth index corresponds to the module coefficients (0: a_n, 1: b_n) and argument coefficients (2: c_n, 3: d_n)
     */
    Eigen::Tensor<double, 5> getLinearCoefsForHeadings(
        const ComplexInterpolationStrategies & interpStrategy
            ) const;
} ;

}
 // End of namespace Spectral
}// End of namespace BV
