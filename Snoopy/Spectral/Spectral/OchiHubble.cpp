#include "OchiHubble.hpp"
#include "Wallop.hpp"

using namespace BV::Spectral;

const char* OchiHubble::name = "Ochi-Hubble";

Eigen::ArrayXd OchiHubble::compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
{
    double q = 4.;
    double m = 4. * lambda1_ + 1.;
    Eigen::ArrayXd sw1 = Wallop(hs1_, tp1_, m, q, heading).compute(w);
    m = 4 * lambda2_ + 1.;
    Eigen::ArrayXd sw2 = Wallop(hs2_, tp2_, m, q, heading).compute(w);
    return sw1 + sw2;
}
