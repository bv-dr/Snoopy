#pragma once

#include "SpectralExport.hpp"

#include "Spectrum.hpp"

namespace BV {
namespace Spectral {

/**
 * @class Queffeulou
 *
 * @brief Define a Queffeulou wind spectrum.
 */
class SPECTRAL_API Queffeulou: public Spectrum
{
public:

    Queffeulou(double meanVelocity, double C10, double z,
               double Ri, double heading=0.) :
        Spectrum("Queffeulou", heading, meanVelocity), C10(C10), z(z), Ri(Ri)
    {
    }

    // Surface drag coefficient
    double C10 ;

    // Elevation
    double z ;

    // Richardson constant (0.05 < Ri > 0.2)
    double Ri ;

    Eigen::ArrayXd compute(const Eigen::Ref<const Eigen::ArrayXd>& w) const
        override ;

    static int getNParams() { return 4 ;}
    static std::vector<const char*> getCoefs_name() {return {"meanVelocity", "surfaceDragCoefficient", "height", "richardsonConstant"} ;}
    std::vector<double> getCoefs() const            {return {meanValue , C10, z, Ri} ;}
    static std::vector<double> getCoefs_0()         {return {10., 0.0044, 10., 0.1} ;}
    static std::vector<double> getCoefs_min()       {return {0., 0., 0., 0.05} ;}
    static std::vector<double> getCoefs_max()       {return {70., 1., 1.e3, 0.2} ;}
} ;

}
}
