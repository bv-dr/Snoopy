def test_read_rao():
    import Snoopy.Spectral as sp
    rao = sp.Rao(f"{sp.TEST_DATA:}/rao/heave.rao")
    assert rao.rho is not None
    assert rao.grav is not None

    rao2 = sp.Rao.ReadHstar(f"{sp.TEST_DATA:}/rao/heave.rao")
    assert rao2.rho is not None
    assert rao2.grav is not None


if __name__ == "__main__":
    test_read_rao()