from Snoopy import Spectral as sp
import numpy as np
import pandas as pd



def test_stochlin_starspec():

    raos = sp.Rao( f"{sp.TEST_DATA:}/rao/roll_multi.rao" ).getRaoForSpectral(wmin = 0.05, wmax = 3.5 , dw = 0.005, db_deg = 5)

    #In Snoopy, roll RAO in radians by default !
    raos /= (180 / np.pi)

    #Read starspec res
    ref = pd.read_csv( f"{sp.TEST_DATA:}/hspec_reference/stochLin_spec/Short_term/stochLin_moments.csv" , delimiter = ";" )

    hsList = np.arange( 2 , 10 , 2 )

    ssList = [sp.SeaState.Jonswap( hs , 10.0 , 1.0  , np.pi / 2 ) for hs in hsList]

    smom_sl = sp.dampingLinearization.SpectralMomentsSL( ssList , raos, [raos],  bLin = 2e+08 , bQuad = 2e10  )
    beq = smom_sl.beq

    for i , hs in enumerate( hsList ) :

        rSpec = sp.StochasticDamping(  ssList[i] , raos , bLin = 2e+08 , bQuad = 2e10 )

        print (rSpec.beq,  ref.iloc[i,:]["Beq"] , beq[i] )

        # Check StochasticDamping
        assert(  np.isclose( rSpec.beq ,  ref.iloc[i,:]["Beq"] , rtol = 1e-2))
        assert(  np.isclose( rSpec.beq ,  beq[i] , rtol = 1e-3))


    for i , hs in enumerate( hsList ) :
        print ( smom_sl.getM0s()[i,0] * (180/np.pi)**2 ,  ref.iloc[i,:]["m0"]  )
        assert(  np.isclose( smom_sl.getM0s()[i,0] * (180/np.pi)**2,  ref.iloc[i,:]["m0"]  , rtol = 1e-3))
        assert(  np.isclose( smom_sl.getM2s()[i,0] * (180/np.pi)**2,  ref.iloc[i,:]["m2"]  , rtol = 1e-3))


    print ("Snoopy and Starspec agree on roll damping stochastic linearization!")


if __name__ == "__main__" :

    test_stochlin_starspec()
