import numpy as np
import os
import pytest
from matplotlib import pyplot as plt
from Snoopy import Spectral as sp
from Snoopy import TimeDomain as td
from Snoopy import WaveKinematic as wk
from Snoopy import logger

logger.setLevel(10)


ss = sp.SeaState( sp.Jonswap(hs=5, tp = 10, gamma= 1.0, heading= np.pi) )
target = 10

def test_newWave(display = False):

    wdir = f"{sp.TEST_DATA:}/test_write"
    if not os.path.exists(wdir) :
        os.mkdir(wdir)

    #Method one : Analytical
    unitRao = sp.getUnitRao( dw = 0.01, wmin = 0.2 , wmax = 2.0 , heading = np.arange(0,2*np.pi , np.deg2rad(30)) )
    rcw_1 = sp.edw.rcwCalc( targetVal = target, rao = unitRao, seaState=ss )

    #Method two, using generic constraint minimization
    rcw_2 = sp.edw.constrainedWave( [(0, target)] , ss, waveModel=wk.FirstOrderKinematic, wmin = 0.2, wmax=2.0, nbSeed = 100 )

    #Method three, (same as method 1)
    rcw_3 = sp.edw.newWave( targetVal = target, seaState=ss, dw = 0.05 )

    rcw_2.write(f"{wdir:}/newWave2.wib")
    rcw_2_read = sp.Wib.Read(f"{wdir:}/newWave2.wib")

    rcw_3.write(f"{wdir:}/newWave3.wif")
    rcw_3_read = sp.Wif(f"{wdir:}/newWave3.wif")

    #Method 4, using HLCL :
    # TODO

    #Compare results :
    rec_1 = td.ReconstructionWifLocal(rcw_1 )
    rec_2 = td.ReconstructionWifLocal(rcw_2_read.getWif() )
    rec_3 = td.ReconstructionWifLocal(rcw_3_read )

    assert( np.isclose( rec_1(0.0) , target, rtol = 1e-3)   )
    assert( np.isclose( rec_2(0.0) , target, rtol = 1e-3)   )
    assert( np.isclose( rec_3(0.0) , target, rtol = 1e-3)   )

    if display :
        time = np.arange( -50, 50 , 0.5)
        fig, ax = plt.subplots()
        rec_1.evalSe( time ).plot(ax=ax, label = "Analytic")
        rec_2.evalSe( time ).plot(ax=ax, marker = "+", label = "Numeric")
        ax.legend()

def test_edw_classInterface(display = False) :
    unitRao = sp.getUnitRao( dw = 0.05, wmin = 0.2 , wmax = 2.0 , heading = np.linspace(0,2*np.pi , 13 ))
    edw_scipy = sp.edw.Edw_Rao( 10 , ss , unitRao, method ="scipy" )
    edw_mhlga = sp.edw.Edw_Rao( 10 , ss , unitRao, method ="MHLGA" )
    edw_analytical = sp.edw.Edw_Rao( 10 , ss , unitRao.getSorted(duplicatesBounds=False), method ="analytical" )

    if display:
        fig, ax = plt.subplots()
        edw_scipy.plot_eta1(ax=ax)
        edw_mhlga.plot_eta1(ax=ax)
        edw_analytical.plot_eta1(ax=ax)


def test_edw_rao_interp(): 
    rao = sp.Rao( f"{sp.TEST_DATA:}/my_10.rao")
    rao = rao.getSymmetrized().getSorted()\
        .getRaoAtHeadings( np.deg2rad(np.arange( 0, 360.,  10.)))\
        .getRaoAtFrequencies( np.arange( 0.2 , 1.8, 0.05 ) )

    """
    It is expected that with heading in between data, edw will not exactly achieved the targeted value : 
        ResponseSpectrum use closest heading, while ReconstructRaoLocal interpolates.
    """
    for head in [ 180., 330, 350, 90 ] :  
        ss = sp.SeaState( sp.Jonswap( hs = 1.0, tp = 7.0, gamma = 1.0 , heading = np.deg2rad(head) ))
        resp = sp.ResponseSpectrum( ss, rao )
        target = resp.getSpectralStats().rp_to_x( 10800. )
        rcw = sp.edw.Edw_Rao(target, ss, rao)
        check_target = td.ReconstructionRaoLocal( rcw.wif, rao.getSorted() )(0.)[0]
        print (check_target / target)
        assert( np.isclose( target , check_target, rtol = 0.001))
        
        
def test_drcw_rao(display = False) :
    rao = sp.Rao( f"{sp.TEST_DATA:}/my_10.rao").getSymmetrized().getSorted(duplicatesBounds=True).getRaoForSpectral(dw = 0.05 )
    ss = sp.SeaState( sp.Jonswap(hs=5, tp = 10, gamma= 1.0, heading= np.pi, spreading_type = sp.SpreadingType.Cosn, spreading_value = 3) )
    target = 10.0
    edw = sp.edw.Drcw_Rao(target, ss, rao)
    time = np.arange( -50., 50. , 0.2 )
    rec = td.ReconstructionRaoLocal(edw.wif, rao.getSorted(duplicatesBounds=True))
    assert( np.isclose( rec(0)[0] , target, rtol = 1e-6 ) )

    if display: 
        fig, ax= plt.subplots()
        rec.evalSe( time ).plot(ax=ax)


if __name__ == "__main__" :
    test_edw_rao_interp()
    test_newWave(True)
    test_drcw_rao(True)
    test_edw_classInterface(True)

