import numpy as np
from Snoopy import Spectral as sp


def test_SpectralStats() :
    rs, rtz = 1, 10
    d = 10800

    m0, m2 = sp.m0m2FromRsRtz(rs, rtz)
    stats = sp.SpectralStats( m0 , m2 )

    stats2 = sp.SpectralStats.FromRsRtz( rs , rtz )
    assert( np.isclose(  stats2.m0 , stats.m0  ) )

    m1 = stats.rp_to_x( d  )
    m2 = stats.risk_to_x( 1 - 1 / np.exp(1) , d)
    assert (np.isclose( m1 , m2 , 1e-4 ))  # Approximate, expected ok when N large

    m3 = stats.getShtMaxDistribution(d).isf( 1 - 1 / np.exp(1)  )
    assert (np.isclose( m2 , m3 ))  # Exact
    assert( (2*stats).getAs() == 2*stats.getAs() )
    assert( (stats*2).getAs() == 2*stats.getAs() )

    a = stats.getAs()
    stats *= 2
    assert( 2*a == stats.getAs() )

    r = 0.05
    x = stats.risk_to_x( r, 3600 )
    r_bis = stats.x_to_risk( x , 3600 )
    assert(np.isclose(r, r_bis))

    duration = 3600.
    assert(np.isclose( stats.x_to_rp( stats.rp_to_x(duration,1), 1), duration) )
    assert(np.isclose( stats.x_to_rp( stats.rp_to_x(duration,2), 2), duration) )
    
    print ("SpectralStats ok")


if __name__ == "__main__":

    test_SpectralStats()
