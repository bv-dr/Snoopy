#include <iostream>
#define DISABLE_TESTS

#ifndef DISABLE_TESTS
#include "Spectral/WaveTabulatedSpectrum.hpp"
#include "Spectral/SeaState.hpp"
#include "Spectral/Wallop.hpp"
#include "Spectral/Jonswap.hpp"
#include "Spectral/Gauss.hpp"
#include "Spectral/OchiHubble.hpp"
#include "Spectral/SimpleOchiHubble.hpp"
#include "Spectral/WhiteNoise.hpp"
#include "Spectral/Gamma.hpp"
#include "Spectral/Torsethaugen.hpp"
#include "Spectral/SimpleTorsethaugen.hpp"
#include "Spectral/Qtf.hpp"
#include "Spectral/MQtf.hpp"
#include "Spectral/Rao.hpp"
#include "Spectral/ResponseSpectrum.hpp"
#include "Spectral/SpectralMoments.hpp"
#include "Spectral/ResponseSpectrum2nd.hpp"
#include "Spectral/ResponseSpectrum2ndMQtf.hpp"
#include "Spectral/TransferFunction.hpp"
#include "Tools/Sort.hpp"

#include "Spectral/Wif.hpp"

// STL
#include <fstream>
#include <iomanip>
#include <regex>
#include <cctype>
#include <random>
#include <chrono>
#include <ctime>

#include <bspline.h>
#include <bsplinebuilder.h>
#include <datatable.h>

using namespace BV::Spectral;

template<typename T, typename... Args>
void generateSpectrumFile(Args&&... parameters)
{
    std::shared_ptr<Spectrum> spectrum = std::make_shared<T>(std::forward<Args>(parameters)...);

    // Generate the wave frequencies list
    double wmin = 0.1;
    double wmax = 2.0;
    double dw = 0.001;
    Eigen::Index size = static_cast<Eigen::Index>((wmax - wmin) / dw) + 1;
    Eigen::VectorXd w = Eigen::VectorXd::LinSpaced(size, wmin + dw, wmax);

    auto sw = spectrum->compute(w);

    // Write the spectrum to a csv file
    std::ofstream outputFile;
    std::string fileName = "spectrum_" + spectrum->getName() + "_cpp.csv";
    outputFile.open(fileName);
    outputFile << "sep=," << std::endl;
    for (auto i = 0; i < w.size(); ++i)
    {
        outputFile << std::fixed << std::setprecision(8) << w[i] << "," << sw[i] << "," << std::endl;
    }

    outputFile.close();
}

// Spectrum test
void Spectrum_test()
{
    std::cout << "Spectrum test\n";
    generateSpectrumFile<Wallop>(1., 10., 4., 5.);
    generateSpectrumFile<Jonswap>(1., 10., 3.3, 0.06, 0.08);
    generateSpectrumFile<Gauss>(1., 10., 10.);
    generateSpectrumFile<Jonswap>(1., 10., 3.3);
    generateSpectrumFile<OchiHubble>(1., 10., 10., 1., 10., 8.);
    generateSpectrumFile<SimpleOchiHubble>(1., 10., 10.);
    generateSpectrumFile<WhiteNoise>(1., 0.4, 0.8, 1.2, 1.5);
    generateSpectrumFile<Gamma>(1., 10., 1., 4., 4.);
    generateSpectrumFile<Torsethaugen>(1., 10.);
    generateSpectrumFile<SimpleTorsethaugen>(1.0, 10.);

    // Generate a list of 191 wave frequencies with their corresponding
    // densities using a Gauss spectrum.
    double wmin = 0.1;
    double wmax = 2.0;
    double dw = 0.01;
    Eigen::Index size = static_cast<Eigen::Index>((wmax - wmin) / dw) + 1;
    Eigen::VectorXd w = Eigen::VectorXd::LinSpaced(size, wmin + dw, wmax);
    Eigen::VectorXd sw = Gauss(1., 10., 10.).compute(w);

    // Test the tabulated spectrum which should result in file similar to
    // the Gauss one.
    generateSpectrumFile<WaveTabulatedSpectrum>(w, sw);
}

// ResponseSpectrum2nd test
void ResponseSpectrum2nd_test()
{
    std::cout << "Response Spectrum 2nd test\n";
    Eigen::ArrayXd b = Eigen::VectorXd::LinSpaced(3, 0.0, 180.0);       // the heading list
    Eigen::ArrayXd w = Eigen::VectorXd::LinSpaced(5, 0.2, 1.0);         // the frequency list
    Eigen::ArrayXd dw = Eigen::VectorXd::LinSpaced(2, 0.0, 0.2);        // the dw frequencies list
    Eigen::Tensor<double, 4> dataAmp(3, 5, 2, 1);
    Eigen::Tensor<double, 4> dataPhi(3, 5, 2, 1);
    for(int head = 0; head < 3; ++head)
        for(int freq = 0; freq < 5; ++freq)
            for(int dfreq = 0; dfreq < 2; ++dfreq)
                for(int m = 0; m < 1; ++m)
                {
                    dataAmp(head, freq, dfreq, m) = 0.0;
                    dataPhi(head, freq, dfreq, m) = 0.0;
                }
    dataAmp(0, 0, 0, 0) = 1.0;                  // QTF(head = 0.0, w = 0.2, dw = 0.0 (w2 = 0.2), mode 1)
    dataAmp(0, 1, 0, 0) = 2.0;                  // QTF(head = 0.0, w = 0.4, dw = 0.0 (w2 = 0.2), mode 1)
    dataAmp(0, 2, 0, 0) = 2.0;                  // QTF(head = 0.0, w = 0.6, dw = 0.0 (w2 = 0.2), mode 1)
    dataAmp(0, 3, 0, 0) = 1.0;                  // QTF(head = 0.0, w = 0.8, dw = 0.0 (w2 = 0.2), mode 1)
    dataAmp(0, 4, 0, 0) = 1.0;                  // QTF(head = 0.0, w = 1.0, dw = 0.0 (w2 = 0.2), mode 1)
    dataAmp(0, 0, 1, 0) = 2.0;                  // QTF(head = 0.0, w = 0.2, dw = 0.2 (w2 = 0.4), mode 1)
    dataAmp(0, 1, 1, 0) = 1.0;                  // QTF(head = 0.0, w = 0.4, dw = 0.2 (w2 = 0.6), mode 1)
    dataAmp(0, 2, 1, 0) = 1.0;                  // QTF(head = 0.0, w = 0.6, dw = 0.2 (w2 = 0.8), mode 1)
    dataAmp(0, 3, 1, 0) = 2.0;                  // QTF(head = 0.0, w = 0.8, dw = 0.2 (w2 = 1.0), mode 1)
    Eigen::Vector3d refPoint(0.0, 0.0, 0.0);
    Eigen::Vector2d waveRefPoint(0.0, 0.0);
    Qtf qtf(b, w, dw, dataAmp, dataPhi, QtfStorageType::W_DW, refPoint, waveRefPoint, QtfMode::SUM, 0.0);

    Jonswap swell(5.0, 14.0, 3.3);      //   swell = sp.Jonswap( hs  = 5.0 , tp = 14.0 , gamma = 3.3 , heading = 0.0 )

    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(swell));
    SeaState ss(spectrums);             //   ss = sp.SeaState( [ windsea  ] )

    ResponseSpectrum2nd rs(ss, qtf);
    auto rspec = rs.get();
    std::cout << rspec << std::endl;
}

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// line parser
// freq re1 im1 re2 im2 ...
void parseLines(const std::string & line, const int & ihead, const int & iw, const int & nbDif, const int & iMode, Eigen::Tensor<std::complex<double>, 4> & dat)
{
    std::istringstream iss(line);
    double freq, re, im;
    iss >> freq;
    for(int idw = 0; idw < nbDif; ++idw)
    {
        iss >> re >> im;
        dat(ihead, iw, idw, iMode) = std::complex<double>(re, im);
    }
}

void ResponseSpectrum2nd_test2(const std::string & filename)
{
    std::fstream in(filename, std::ios::in);
    //std::cout << in.is_open() << std::endl;
    std::string line;
    //std::istringstream iss(line);
    std::istringstream iss;
    std::string txt;
    double x,y,z;

    for(int i = 0; i < 9; i ++) // skip the first 8 lines with data
        getline(in, line);
    iss.str(line);
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> x >> y;
    Eigen::Vector2d waveRefPoint(x, y);

    QtfMode qtfMode = QtfMode::DIFF;
    for(int i = 0; i < 6; i ++)
        getline(in, line);
    {
        std::istringstream iss(line);
        iss >> txt >> txt;
        rtrim(txt);
        if (txt.compare("Sum") == 0)
            qtfMode = QtfMode::SUM;
    }
    getline(in, line);
    getline(in, line);
    iss.str(line);
    iss >> txt >> x >> y >> z;
    Eigen::Vector3d refPoint(x, y, z);

    int nbHead;
    std::getline(in, line);
    {
        std::istringstream iss(line);
        iss >> txt >> nbHead;
    }
    Eigen::ArrayXd b(nbHead);
    std::getline(in, line);
    {
        std::istringstream iss(line);
        iss >> txt;
        double headdeg;
        for(int i = 0; i < nbHead; ++i)
        {
            iss >> headdeg;
            b(i) = headdeg *M_PI/180.0;
        }
    }
    int nbDif = 0;
    std::getline(in, line);
    {
        std::istringstream iss(line);
        iss >> txt;
        while (iss >> x)
            nbDif ++;
    }
    Eigen::ArrayXd dw(nbDif);
    {
        std::istringstream iss(line);
        iss >> txt;
        for(int i = 0; i < nbDif; ++i)
            iss >> dw(i);
    }

    std::getline(in, line);
    bool freqRead = false;
    bool freqReadStart = false;
    std::vector<double> freqs;
    std::getline(in, line);
    std::shared_ptr<std::vector<std::string>> heading(nullptr);
    std::vector<std::shared_ptr<std::vector<std::string>>> headings;
    do {
        if (std::regex_match(line, std::regex("(#Heading)(.*)")))
        {   // found #Heading = ... line
            if (heading != nullptr) {
                headings.push_back(heading);
            }
            heading = std::make_shared<std::vector<std::string>>(std::vector<std::string>());
            if (freqReadStart)
                freqRead = true;
        } else
        {
            rtrim(line);
            if(line.compare("") != 0) {
                // The line is not empty and is not heading => data
                heading->push_back(line);
                std::istringstream iss(line);
                double freq;
                iss >> freq;
                if (!freqReadStart)
                    freqReadStart = true;
                if (!freqRead)
                    freqs.push_back(freq);
            }
        }
    } while(std::getline(in, line));
    if (heading != nullptr) {
        headings.push_back(heading);
    }
    in.close();
    Eigen::Map<Eigen::ArrayXd> w(freqs.data(), freqs.size());
    Eigen::Tensor<std::complex<double>, 4> amp(nbHead, w.size(), nbDif, 1);
    int ihead = -1;
    for(auto h : headings)
    {
        ihead ++;
        int iw = -1;
        for(auto l : *h)
        {
            iw ++;
            parseLines(l, ihead, iw, nbDif, 0, amp);
        }
    }
    /*
    for(int h = 0; h < nbHead; ++h) {
        std::cout << "Heading = " << b(h) << std::endl;
        std::cout << std::scientific ;
        std::cout << std::setprecision(4);
        for(int iw = 0; iw < freqs.size(); ++iw)
        {
            std::cout << w(iw) << "  ";
            for(int ndif = 0; ndif < nbDif; ++ndif)
            {
                std::cout << amp(h, iw, ndif, 0) << "  ";
            }
            std::cout << std::endl;
        }
    }
    */
    Qtf qtf(b, w, dw, amp, QtfStorageType::W_DW, refPoint, waveRefPoint, qtfMode, 0.0);

    Jonswap spec(1.0, 10.0, 1.0, 0.0);
    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(spec));
    SeaState seaState(spectrums);
    ResponseSpectrum2nd rsp(seaState, qtf);
    auto rspec = rsp.get();

    std::fstream f("rspec.dat", std::ios::out);
    f << std::scientific << std::setprecision(4);
    const auto f2 = rsp.getFrequencies();
    std::cout << rspec.size() << " / " << f2.size() << std::endl;
    for(Eigen::Index i = 0; i < f2.size(); ++i)
    {
        f << f2(i) << "\t" << rspec(i) << std::endl;
    }
    f.close();

}
double frexp10(double arg, int * exp)
{
   *exp = (arg == 0) ? 0 : 1 + (int)std::floor(std::log10(std::fabs(arg) ) );
   return arg * std::pow(10 , -(*exp));
}
double frexp10_s(double arg, int *exp) {
    double r = frexp10(arg, exp);
    if (r == 0.) return r;
    (*exp) --;
    return r*10.;
}

std::unique_ptr<Qtf> readQtfFromFile(std::string const &filename) {
    std::fstream in(filename, std::ios::in);
    std::string line;
    std::istringstream iss;
    std::string txt;
    double x,y,z;

    for(int i = 0; i < 9; i ++) // skip the first 8 lines with data
        getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> x >> y;
    Eigen::Vector2d waveRefPoint(x, y);

    QtfMode qtfMode = QtfMode::DIFF;
    for(int i = 0; i < 6; i ++)
        getline(in, line);
    iss.str(line);
    iss.clear();

    iss >> txt >> txt;
    rtrim(txt);
    if (txt.compare("Sum") == 0)
        qtfMode = QtfMode::SUM;
    getline(in, line);
    getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> txt >> x >> y >> z;
    Eigen::Vector3d refPoint(x, y, z);

    int nbHead;
    std::getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> txt >> nbHead;
    Eigen::ArrayXd b(nbHead);
    std::getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> txt;
    double headdeg;
    for(int i = 0; i < nbHead; ++i)
    {
        iss >> headdeg;
        b(i) = headdeg *M_PI/180.0;
    }
    int nbDif = 0;
    std::getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> txt;
    while (iss >> x)
        nbDif ++;
    Eigen::ArrayXd dw(nbDif);
    iss.str(line);
    iss.clear();
    iss >> txt;
    for(int i = 0; i < nbDif; ++i)
        iss >> dw(i);

    std::getline(in, line);
    bool freqRead = false;
    bool freqReadStart = false;
    std::vector<double> freqs;
    std::getline(in, line);
    std::shared_ptr<std::vector<std::string>> heading(nullptr);
    std::vector<std::shared_ptr<std::vector<std::string>>> headings;
    do {
        if (std::regex_match(line, std::regex("(#Heading)(.*)")))
        {   // found #Heading = ... line
            if (heading != nullptr) {
                headings.push_back(heading);
            }
            heading = std::make_shared<std::vector<std::string>>(std::vector<std::string>());
            if (freqReadStart)
                freqRead = true;
        } else
        {
            rtrim(line);
            if(line.compare("") != 0) {
                // The line is not empty and is not heading => data
                heading->push_back(line);
                iss.str(line);
                iss.clear();
                double freq;
                iss >> freq;
                if (!freqReadStart)
                    freqReadStart = true;
                if (!freqRead)
                    freqs.push_back(freq);
            }
        }
    } while(std::getline(in, line));
    if (heading != nullptr) {
        headings.push_back(heading);
    }
    in.close();
    Eigen::Map<Eigen::ArrayXd> w(freqs.data(), freqs.size());
    Eigen::Tensor<std::complex<double>, 4> amp(nbHead, w.size(), nbDif, 1);
    int ihead = -1;
    for(auto h : headings)
    {
        ihead ++;
        int iw = -1;
        for(auto l : *h)
        {
            iw ++;
            parseLines(l, ihead, iw, nbDif, 0, amp);
        }
    }
    /*
    for(int h = 0; h < nbHead; ++h) {
        std::cout << "Heading = " << b(h) << std::endl;
        std::cout << std::scientific ;
        std::cout << std::setprecision(4);
        for(int iw = 0; iw < freqs.size(); ++iw)
        {
            std::cout << w(iw) << "  ";
            for(int ndif = 0; ndif < nbDif; ++ndif)
            {
                std::cout << amp(h, iw, ndif, 0) << "  ";
            }
            std::cout << std::endl;
        }
    }
    */
    Qtf qtf(b, w, dw, amp, QtfStorageType::W_DW, refPoint, waveRefPoint, qtfMode, 0.0);
    return std::make_unique<Qtf>(qtf);
}

void ResponseSpectrum2nd_test4(const std::string & filename)
{
    const int nbhead(3);
    const int nbfreq(5);
    const int nbdiff(5);
    const int nbmods(1);
    Eigen::ArrayXd b (Eigen::VectorXd::LinSpaced(nbhead, 0.0, 180.0));       // the heading list
    Eigen::ArrayXd w (Eigen::VectorXd::LinSpaced(nbfreq, 0.2, 1.0));         // the frequency list
    Eigen::ArrayXd dw(Eigen::VectorXd::LinSpaced(nbdiff, 0.0, 0.8));        // the dw frequencies list
    Eigen::Tensor<double, 4> dataAmp(nbhead, nbfreq, nbdiff, nbmods);
    Eigen::Tensor<double, 4> dataPhi(nbhead, nbfreq, nbdiff, nbmods);
    // Setting the diagonal elements of the Qtf
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(0.0, 10.0);    // [0.0, 10.0)

    for(int head = 0; head < nbhead; ++head)
        for(int freq = 0; freq < nbfreq; ++freq) {
            for(int m = 0; m < nbmods; ++m) {
                dataAmp(head, freq, 0, m) = dis(gen);
                dataPhi(head, freq, 0, m) = 0.0;
            }
            for(int dfreq = 1; dfreq < nbdiff; ++dfreq)
                for(int m = 0; m < nbmods; ++m)
                {
                    dataAmp(head, freq, dfreq, m) = 0.0;
                    dataPhi(head, freq, dfreq, m) = 0.0;
                }
        }
    // Output
    std::cout << "QTF values:\n";
    std::cout << std::fixed;
    for(int head = 0; head < nbhead; ++head)
    {
        std::cout << "Heading[" << head << "] = " << std::setw(3) << std::setprecision(0) << b(head) << "°\n";
        std::cout << "   w_1\\w_2";
        for(int freq = 0; freq < nbfreq; ++freq)
            std:: cout << std::setw(10) << std::setprecision(5) << w(freq);
        std::cout << "\n";
        for(int freq = 0; freq < nbfreq; ++freq) {
            std::cout << std::setw(10) << std::setprecision(5) << w(freq);
            for(int dfreq = 0; dfreq < freq; ++dfreq)
                std::cout << "          ";
            for(int dfreq = 0; dfreq < nbdiff; ++dfreq) {
                if (w(freq)+dw(dfreq) <= w(nbfreq -1))
                    std::cout << std::setw(10) << std::setprecision(5) << dataAmp(head, freq, dfreq, 0);
            }
            std::cout << "\n";
        }
    }
    Eigen::Vector3d refPoint(0.0, 0.0, 0.0);
    Eigen::Vector2d waveRefPoint(0.0, 0.0);
    auto mode=QtfMode::DIFF;
    //auto mode = QtfMode::SUM;
    Qtf qtf1(b, w, dw, dataAmp, dataPhi, QtfStorageType::W_DW, refPoint, waveRefPoint, mode, 0.0);
    // Geometric mean
    for(int head = 0; head < nbhead; ++head)
        for(int freq = 0; freq < nbfreq; ++freq) {
            for(int dfreq = 1; dfreq < nbdiff; ++dfreq)
                for(int m = 0; m < nbmods; ++m)
                {
                    if (freq +dfreq < nbfreq) {
                        dataAmp(head, freq, dfreq, m) = sqrt(dataAmp(head, freq, 0, m)*dataAmp(head, freq+dfreq, 0, m));
                        dataPhi(head, freq, dfreq, m) = 0.0;
                    }
                }
        }
    // Output
    std::cout << "QTF values (geometric mean):\n";
    std::cout << std::fixed;
    for(int head = 0; head < nbhead; ++head)
    {
        std::cout << "Heading[" << head << "] = " << std::setw(3) << std::setprecision(0) << b(head) << "°\n";
        std::cout << "   w_1\\w_2";
        for(int freq = 0; freq < nbfreq; ++freq)
            std:: cout << std::setw(10) << std::setprecision(5) << w(freq);
        std::cout << "\n";
        for(int freq = 0; freq < nbfreq; ++freq) {
            std::cout << std::setw(10) << std::setprecision(5) << w(freq);
            for(int dfreq = 0; dfreq < freq; ++dfreq)
                std::cout << "          ";
            for(int dfreq = 0; dfreq < nbdiff; ++dfreq) {
                if (w(freq)+dw(dfreq) <= w(nbfreq -1))
                    std::cout << std::setw(10) << std::setprecision(5) << dataAmp(head, freq, dfreq, 0);
            }
            std::cout << "\n";
        }
    }
    Qtf qtf2(b, w, dw, dataAmp, dataPhi, QtfStorageType::W_DW, refPoint, waveRefPoint, mode, 0.0);
    // Arithmetic mean
    for(int head = 0; head < nbhead; ++head)
        for(int freq = 0; freq < nbfreq; ++freq) {
            for(int dfreq = 1; dfreq < nbdiff; ++dfreq)
                for(int m = 0; m < nbmods; ++m)
                {
                    if (freq +dfreq < nbfreq) {
                        dataAmp(head, freq, dfreq, m) = 0.5*(dataAmp(head, freq, 0, m) +dataAmp(head, freq+dfreq, 0, m));
                        dataPhi(head, freq, dfreq, m) = 0.0;
                    }
                }
        }
    // Output
    std::cout << "QTF values (arithmetic mean):\n";
    std::cout << std::fixed;
    for(int head = 0; head < nbhead; ++head)
    {
        std::cout << "Heading[" << head << "] = " << std::setw(3) << std::setprecision(0) << b(head) << "°\n";
        std::cout << "   w_1\\w_2";
        for(int freq = 0; freq < nbfreq; ++freq)
            std:: cout << std::setw(10) << std::setprecision(5) << w(freq);
        std::cout << "\n";
        for(int freq = 0; freq < nbfreq; ++freq) {
            std::cout << std::setw(10) << std::setprecision(5) << w(freq);
            for(int dfreq = 0; dfreq < freq; ++dfreq)
                std::cout << "          ";
            for(int dfreq = 0; dfreq < nbdiff; ++dfreq) {
                if (w(freq)+dw(dfreq) <= w(nbfreq -1))
                    std::cout << std::setw(10) << std::setprecision(5) << dataAmp(head, freq, dfreq, 0);
            }
            std::cout << "\n";
        }
    }
    Qtf qtf3(b, w, dw, dataAmp, dataPhi, QtfStorageType::W_DW, refPoint, waveRefPoint, mode, 0.0);

    Jonswap spec(1.0, 10.0, 1.0, 0.0);
    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(spec));
    SeaState seaState(spectrums);
    ResponseSpectrum2nd rsp1(seaState, qtf1);
    ResponseSpectrum2nd rsp2(seaState, qtf2);
    ResponseSpectrum2nd rsp3(seaState, qtf3);
    // Geometrical mean
    auto rspec10 = rsp1.getNewman(0);
    auto rspec0 = rsp2.get();
    auto rspec01 = rsp2.getNewman();
    // Arithmetic mean
    auto rspec11 = rsp1.getNewman(1);
    auto rspec1 = rsp3.get();
    auto rspec02 = rsp3.getNewman(1);

    const auto f2 = rsp1.getFrequencies();
    std::cout << rspec10.size() << " / " << f2.size() << "\n";
    std::cout << rspec11.size() << " / " << f2.size() << "\n";
    std::cout << "  omega\t|\t    Newman Geometric\t|\t    Newman Arithmetic\n";
    std::cout << "\t|\tDiag\tget\tgetN\t|\tDiag\tget\tgetN\n";
    for(Eigen::Index i = 0; i < f2.size(); ++i) {
        std::cout << f2(i) << "\t|\t" << rspec10(i) << "\t" << rspec0(i) << "\t" << rspec01(i)
                  << "\t|\t" << rspec11(i) << "\t" << rspec1(i) << "\t" << rspec02(i) << "\n";
    }
    std::cout << "Diag: Qtf drift (rsp1),\n";
    std::cout << "get : Qtf geometric or arithmetic,\n";
    std::cout << "getN: Qtf geometric or arithmetic using getNewman.\n";
/*
    std::fstream f("rspec.dat", std::ios::out);
    f << std::scientific << std::setprecision(4);
    const auto f2 = rsp1.getFrequencies();
    std::cout << rspec11.size() << " / " << f2.size() << std::endl;
    for(Eigen::Index i = 0; i < f2.size(); ++i)
    {
        f << f2(i) << "\t" << rspec11(i) << std::endl;
    }
    f.close();
*/

}

void writeRaoToFile(Rao rao, std::string const &name) {
    auto waveRefPoint = rao.getWaveReferencePoint();
    auto forwardSpeed = rao.getForwardSpeed();
    auto refPoint = rao.getReferencePoint();
    std::fstream out(name, std::ios::out);
    out << "# Project :\n";
    out << "# User    :\n";
    out << "# File : " << name << "\n";
    out << "#\n";
    out << "# Constants used in computations :\n";
    out << "#     Reference length     :     1.0000\n";
    out << "#     Water density (rho)  :  1025.0000\n";
    out << "#     Gravity acceleration :     9.8100\n";
    out << "#     Waterdepth           :  Inf.\n";
    out << "#     Ref.pt incident wave : (    " << waveRefPoint(0) << "  " << waveRefPoint(1) <<")\n";
    out << "#            Forward speed :   " << forwardSpeed << "  m/s\n";
    out << "#\n";
    out << "# Reference point of body 1: (  " << refPoint(0) << "   " << refPoint(1) << "   " << refPoint(2) << ")\n";
    out << "# MEANVALUE :   0.0000E+00\n";
    out << "#   AMP/PHASE\n";
    out << "#------------------------------------------------------------------------\n";
    out << "#RAOTYPE    :  MOTION\n";
    out << "#COMPONENT  :  3\n";
    out << "#UNIT       :  m/m\n";
    out << "#NBHEADING  "<< rao.getHeadings().size() << "\n";
    out << "#HEADING    ";
    auto bs =  rao.getHeadings();
    for(int i = 0; i < bs.size(); ++i)
        out << "   " << std::setw(12) << bs(i) *180.0/M_PI;
    out << "\n";
    out << "#---w(r/s)-----------------------------------------------------\n";
    out.setf(std::ios::fixed, std::ios::floatfield) ;
    out.precision(10) ;
    auto ws = rao.getFrequencies();
    auto ms = rao.getModules();
    auto ps = rao.getPhases();
    for (int i = 0; i < ws.size(); ++i) {
        out << ws(i);
        for (int j = 0; j < bs.size(); ++j) {
            out << "   " << ms(j, i, 0);
        }
        for (int j = 0; j < bs.size(); ++j) {
            out << "   " << ps(j, i, 0);
        }
        out << "\n";
    }

    out << "#------------------------------------------------------------\n";
    out << "#ENDFILE "<< name;
    out.close();

}

Rao readRaoFromFile(const std::string& filename, const double& freqShift = 0.0)
{
    std::fstream in(filename, std::ios::in);    // rao file
    std::string line;
    std::istringstream iss;
    std::string txt;
    int nbheads;
    for (int i = 0; i < 10; ++i)
        getline(in, line);                  // skip 9 lines,
    iss.str(line);                          // and reading the 10th one
    Eigen::Vector2d waveRefPoint;
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> waveRefPoint(0) >> waveRefPoint(1);
    getline(in, line);
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    double forwardSpeed = 0.;
    iss >> txt >> txt >> txt >> txt >> forwardSpeed;
    getline(in, line);                      // skip 1 line
    getline(in, line);
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    Eigen::Vector3d refPoint;
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> txt >> refPoint(0) >> refPoint(1) >> refPoint(2);
    for (int i = 0; i < 7; ++i)             // skip next 6 lines
        getline(in, line);                  // and read the 7th one,
    iss.str(line);                          // which corresponds to the 20th line of the file
    iss.clear();                            // to clear all previous flags
    iss >> txt >> nbheads;                  // #NBHEADING 12
    getline(in, line);
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    iss >> txt;                             // #HEADING
    Eigen::ArrayXd b(nbheads);              // array of headings
    for (int i = 0; i < nbheads; ++i) {
        iss >> b(i);
    }
    b = b*M_PI/180.0;                       // degrees to radians
    getline(in, line);                      // skip this line
    std::vector<double> freqs;
    std::vector<std::vector<double>> amps(nbheads);
    std::vector<std::vector<double>> phase(nbheads);
    while (getline(in, line)) {
        iss.str(line);
        iss.clear();
        double v;
        iss >> v;
        if (!iss.fail()) {
            freqs.push_back(v);
            for(int i = 0; i < nbheads; ++i) {
                iss >> v;
                amps[i].push_back(v);
            }
            for(int i = 0; i < nbheads; ++i) {
                iss >> v;
                phase[i].push_back(v);
            }
        }
    }
    in.close();
    // converting std::vector to Eigen::ArrayXd
    Eigen::ArrayXd w(freqs.size());
    for (size_t i = 0; i < freqs.size(); ++i) {
        w(i) = freqs[i];
    }
    freqs.clear();
    // converting std::vectors to Eigen::Tensor
    Eigen::Tensor<double, 3> dataAmp(b.size(), w.size(), 1);
    Eigen::Tensor<double, 3> dataPhi(b.size(), w.size(), 1);
    for(int i = 0; i < w.size(); ++i)
        for (int j = 0; j < b.size(); ++j) {
            dataAmp(j, i, 0) = amps[j][i];
            dataPhi(j, i, 0) = phase[j][i];
        }
    for(int i = 0; i < b.size(); ++i) {
        amps[i].clear();
        phase[i].clear();
    }
    for(int i = 0; i < b.size(); ++i) {
        b[i] += freqShift;
    }
    return Rao(b, w, dataAmp, dataPhi, refPoint, waveRefPoint, forwardSpeed);
}
void ResponseSpectrum_test1(const std::string & filename) {
    std::cout << "ResponseSpectrum_test1\n";
    std::fstream in(filename, std::ios::in);    // rao file
    std::string line;
    std::istringstream iss;
    std::string txt;
    int nbheads;
    for (int i = 0; i < 10; ++i)
        getline(in, line);                  // skip 9 lines,
    iss.str(line);                          // and reading the 10th one
    Eigen::Vector2d waveRefPoint;
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> waveRefPoint(0) >> waveRefPoint(1);
    getline(in, line);
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    double forwardSpeed = 0.;
    iss >> txt >> txt >> txt >> txt >> forwardSpeed;
    getline(in, line);                      // skip 1 line
    getline(in, line);
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    Eigen::Vector3d refPoint;
    iss >> txt >> txt >> txt >> txt >> txt >> txt >> txt >> refPoint(0) >> refPoint(1) >> refPoint(2);
    for (int i = 0; i < 7; ++i)             // skip next 6 lines
        getline(in, line);                  // and read the 7th one,
    iss.str(line);                          // which corresponds to the 20th line of the file
    iss.clear();                            // to clear all previous flags
    iss >> txt >> nbheads;                  // #NBHEADING 12
    getline(in, line);
    iss.str(line);
    iss.clear();                            // to clear all previous flags
    iss >> txt;                             // #HEADING
    Eigen::ArrayXd b(nbheads);              // array of headings
    for (int i = 0; i < nbheads; ++i) {
        iss >> b(i);
    }
    b = b*M_PI/180.0;                       // degrees to radians
    getline(in, line);                      // skip this line
    std::vector<double> freqs;
    std::vector<std::vector<double>> amps(nbheads);
    std::vector<std::vector<double>> phase(nbheads);
    while (getline(in, line)) {
        iss.str(line);
        iss.clear();
        double v;
        iss >> v;
        if (!iss.fail()) {
            freqs.push_back(v);
            for(int i = 0; i < nbheads; ++i) {
                iss >> v;
                amps[i].push_back(v);
            }
            for(int i = 0; i < nbheads; ++i) {
                iss >> v;
                phase[i].push_back(v);
            }
        }
    }
    in.close();
    // converting std::vector to Eigen::ArrayXd
    Eigen::ArrayXd w(freqs.size());
    for (size_t i = 0; i < freqs.size(); ++i) {
        w(i) = freqs[i];
    }
    freqs.clear();
    // converting std::vectors to Eigen::Tensor
    Eigen::Tensor<double, 3> dataAmp(b.size(), w.size(), 1);
    Eigen::Tensor<double, 3> dataPhi(b.size(), w.size(), 1);
    for(int i = 0; i < w.size(); ++i)
        for (int j = 0; j < b.size(); ++j) {
            dataAmp(j, i, 0) = amps[j][i];
            dataPhi(j, i, 0) = phase[j][i];
        }
    for(int i = 0; i < b.size(); ++i) {
        amps[i].clear();
        phase[i].clear();
    }
    std::cout << "Read file : " << filename << "\n";
    std::cout << "Number of headings    : " << nbheads << "\n";
    std::cout << "Forward speed         : " << forwardSpeed << "\n";
    std::cout << "refPoint              : " << refPoint << "\n";
    std::cout << "waveRefPoint          : " << waveRefPoint << "\n";
    std::cout << "Number of frequencies : " << w.size() << "\n";
/*    for (int i = 0; i < w.size(); ++i) {
        std::cout << "  " << w(i) << "\n";
        for (int j = 0; j < b.size(); ++j) {
            std::cout << "    head : " << b(j) << " a = " << dataAmp(j,i,0) << " / phi = " << dataPhi(j,i,0) << "\n";
        }
    }*/
    //forwardSpeed = 2.0;
    for(int i = 0; i < b.size(); ++i) {
        b[i] -= M_PI;
    }
    Rao rao(b, w, dataAmp, dataPhi, refPoint, waveRefPoint, forwardSpeed);
    //writeRaoToFile(rao, "rao_or.rao");
    rao = rao.getRaoIn2piRange();
    writeRaoToFile(rao, "rao.rao");

    Rao rao2 ( rao.getRaoAtFrequencies(Eigen::ArrayXd::LinSpaced(50, 0.05, 2.0),
                                       BV::Math::Interpolators::InterpScheme::LINEAR,
                                       ComplexInterpolationStrategies::AMP_PHASE,
                                       BV::Math::Interpolators::ExtrapolationType::BOUNDARY
                                       ));
    Rao rao3 ( rao2.getRaoAtHeadings  (Eigen::ArrayXd::LinSpaced(12, 0.0, 2*M_PI),
                                       BV::Math::Interpolators::InterpScheme::LINEAR,
                                       ComplexInterpolationStrategies::AMP_PHASE,
                                       BV::Math::Interpolators::ExtrapolationType::BOUNDARY
                                       ));

    writeRaoToFile(rao3, "rao3.rao");

    Jonswap spec1(1.0, 10.0, 1.0, 0.0, SpreadingType::Cosn, 2.);
    Jonswap spec2(3.0,  8.0, 1.0, 0.0, SpreadingType::Cosn, 3.);
    Jonswap spec3(2.0,  8.0, 1.0,  M_PI/2, SpreadingType::No, 0.);
    Jonswap spec4(2.0,  8.0, 1.0, -M_PI/2, SpreadingType::No, 0.);
    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(spec1));
    spectrums.push_back(std::make_shared<Jonswap>(spec2));
    spectrums.push_back(std::make_shared<Jonswap>(spec3));
    spectrums.push_back(std::make_shared<Jonswap>(spec4));
    SeaState seaState(spectrums);
    ResponseSpectrum rsp(seaState, rao3);
    std::cout << rsp.getM0() << "\n";
    std::cout << rsp.getM2() << "\n";
}

void moveHeadingsTo2PiRange(Eigen::Ref<Eigen::ArrayXd> b_) {
    auto n = b_.size();
    for(decltype(n) i = 0; i < n; ++i) {
        while(b_[i] < 0)
            b_[i] += 2*M_PI;
        while(b_[i] > 2*M_PI)
            b_[i] -= 2*M_PI;
    }
    // b_ in [0, 2*\pi]
}


void sort_test() {
    Eigen::ArrayXd b(5);
    b << 0.01, -0.3, 2*M_PI -0.1, -21, 4;
    moveHeadingsTo2PiRange(b);
    for(int i = 0; i < 5; ++i)
        std::cout << b[i] << "\t";
    std::cout << "\n";

    int i0_ = 5;
    int i1_ = 4;
    int i2_ = 5;
    Eigen::Tensor<double, 3> t(i0_, i1_, i2_);
    for(int i0 = 0; i0 < i0_; i0++)
        for(int i1 = 0; i1 < i1_; i1++)
            for(int i2 = 0; i2 < i2_; i2++)
                t(i0,i1,i2) = (i0+1)*100 +(i1+1)*10 +(i2+1);

    auto ind = BV::Tools::sortArray(b);
    BV::Tools::sortTensorByCol(t, 0, ind);
    BV::Tools::sortTensorByCol(t, 2, ind);

    std::cout << t << "\n";
    std::cout << b << "\n";
    std::cout << "---\n";

    BV::Tools::boundTensor(0, b, t, 0.0, 2*M_PI, true, false);
    BV::Tools::boundTensor(2, b, t, 0.0, 2*M_PI, true);
    std::cout << b << "\n";

    std::cout << t << "\n";

}

void wif_test() {
    std::cout << "Wif test\n";
    Eigen::ArrayXd w(Eigen::ArrayXd::Ones(10));
    Eigen::ArrayXd a(Eigen::ArrayXd::Ones(10));
    Eigen::ArrayXd phi(Eigen::ArrayXd::Ones(10));
    Eigen::ArrayXd b(Eigen::ArrayXd::Ones(10));

    for(unsigned i = 0; i < 10; ++i)
        b(i) = i*10;

    b(5) = 20;

    BV::Spectral::Wif wif(w, a, phi, b);
    auto headings = wif.getHeadings();
    auto indHeadings = wif.getIndependentHeadings();
    auto iHIndices = wif.getIndependentHeadingsIndices();
    /*auto d2iHIndices = wif.getDep2IndHeadingsIndices();
    std::cout << "Input headings: \n";
    for(unsigned i = 0; i < headings.size(); ++i)
        std::cout << "b(" << i << ") = " << headings(i)
                  << " <==> indepHeading(" << d2iHIndices[i]
                  << ") : " << indHeadings[d2iHIndices[i]] << "\n";
    std::cout << "Independent headings: \n";
    for(unsigned i = 0; i < indHeadings.size(); ++i)
        std::cout << "ib(" << i << ") = " << indHeadings(i)
                  << " <==> b_(" << iHIndices[i]
                  << ") : " << headings(iHIndices[i]) << "\n";
     */
}

static double f(SPLINTER::DenseVector const & x) {
    return x(0)*x(1);   // f(x,y) = xy
}

void splinterTest() {
    SPLINTER::DataTable samples;
    SPLINTER::DenseVector x(2);
    double y;
    std::ofstream out("splinter.dat");
    for(int i = 0; i < 21; i ++) {
        for(int j = 0; j < 21; j++) {
            x(0) = i*0.1 -1;
            x(1) = j*0.1 -1;
            y = f(x);
            samples.addSample(x, y);
            out << x(0) << "\t" << x(1) << "\t" << y << "\n";
        }
        out << "\n";
    }
    out.close();

    SPLINTER::BSpline bspline = SPLINTER::BSpline::Builder(samples).degree(1).build();
    out.open("splinter_inter.dat");
    for(int i = 0; i < 31; i ++) {
        for (int j = 0; j < 31; j++) {
            x(0) = i * 2./30. -1.;
            x(1) = j * 2./30. -1.;
            out << x(0) << "\t" << x(1) << "\t" << f(x) << "\t" << bspline.eval(x) << "\n";
        }
        out << "\n";
    }

    x(0) = 0.15;
    x(1) = 0.17;
    std::cout << "f(x, y) = " << f(x) << "\n";
    std::cout << "spline  = " << bspline.eval(x) << "\n";
}

void QtfSTest() {
    std::cout << "MQtf test\n";
    std::chrono::time_point<std::chrono::system_clock> start, end;
    MQtf qtf, mqtf;
    std::ifstream qtf_data("test_data/qtf_fx.qtf");
    std::ofstream qtf_cp("test_qtf.qtf");
    std::ifstream mqtf_data("test_data/mqtf_fx.qtf");
    std::ofstream mqtf_cp("test_mqtf.qtf");
    Eigen::ArrayXd hs(Eigen::ArrayXd::LinSpaced(15, 0.0, 2*M_PI));
    Eigen::ArrayXd ws(Eigen::ArrayXd::LinSpaced(15, 0.123, 1.503));
    std::cout << " 0. Initial qtf and mqtf variables\n";
    std::cout << "    Qtf:\n";
    std::cout << qtf << "\n";
    std::cout << "    MQtf:\n";
    std::cout << mqtf << "\n";
    int elapsed_milliseconds(0);

    std::cout << " 1. Reading Qtf file: data/qtf_fx.qtf\n";
    start = std::chrono::system_clock::now();
    qtf_data >> qtf;
    end = std::chrono::system_clock::now();
    //int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";
    std::cout << "    Qtf after the reading:\n";
    std::cout << qtf << "\n";

    std::cout << " 2. Writing Qtf file: data/test_qtf.qtf\n";
    start = std::chrono::system_clock::now();
    qtf_cp << qtf;
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << " 3. Reading MQtf file: data/mqtf_fx.qtf\n";
    start = std::chrono::system_clock::now();
    mqtf_data >> mqtf;
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";
    std::cout << "    MQtf after the reading:\n";
    std::cout << mqtf << "\n";

    std::cout << " 4. Writing MQtf file: data/test_mqtf.qtf\n";
    start = std::chrono::system_clock::now();
    mqtf_cp << mqtf;
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << " 5. Writing hhQTensor.dat\n";
    start = std::chrono::system_clock::now();
    qtf.writeHeadHeadTensor("hhQTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << " 6. Writing hhTensor.dat\n";
    start = std::chrono::system_clock::now();
    mqtf.writeHeadHeadTensor("hhTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << " 7. Interpolating Qtf at headings\n";
    start = std::chrono::system_clock::now();
    MQtf subQtf(qtf.getQtfAtHeadings(hs, 1, ComplexInterpolationStrategies::RE_IM));
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << " 8. Writing hhSubQTensor.dat\n";
    start = std::chrono::system_clock::now();
    subQtf.writeHeadHeadTensor("hhSubQTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << " 9. Writing wdwSubQTensor.dat\n";
    start = std::chrono::system_clock::now();
    subQtf.writeWdWTensor("wdwSubQTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "10. Interpolating MQtf at headings\n";
    start = std::chrono::system_clock::now();
    MQtf subMQtf(mqtf.getQtfAtHeadings(hs, 1, ComplexInterpolationStrategies::RE_IM));
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "11. Writing hhSubTensor.dat\n";
    start = std::chrono::system_clock::now();
    subMQtf.writeHeadHeadTensor("hhSubTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "12. Writing wdwSubTensor.dat\n";
    start = std::chrono::system_clock::now();
    subMQtf.writeWdWTensor("wdwSubTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "13. Interpolating Qtf at frequencies\n";
    start = std::chrono::system_clock::now();
    subQtf = subQtf.getQtfAtFrequencies(ws, 1, ComplexInterpolationStrategies::RE_IM);
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "14. Writing wdwSubSubQTensor.dat\n";
    start = std::chrono::system_clock::now();
    subQtf.writeWdWTensor("wdwSubSubQTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "15. Interpolating MQtf at frequencies\n";
    start = std::chrono::system_clock::now();
    subMQtf = subMQtf.getQtfAtFrequencies(ws, 1, ComplexInterpolationStrategies::RE_IM);
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "16. Writing wdwSubSubTensor.dat\n";
    start = std::chrono::system_clock::now();
    subMQtf.writeWdWTensor("wdwSubSubTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    auto dw(mqtf.getDeltaFrequencies());
    auto w(mqtf.getFrequencies());
    double dw_(ws(1)-ws(0));
    double dwMax(dw(dw.size()-1));
    int dwMaxI(dwMax/dw_);
    dwMax = dw_ *dwMaxI;
    Eigen::ArrayXd dwsm(Eigen::ArrayXd::LinSpaced(dwMaxI, dw(0), dwMax));
    dw = qtf.getDeltaFrequencies();
    w = qtf.getFrequencies();
    dw_ = ws(1) -ws(0);
    dwMax = dw(dw.size() -1);
    dwMaxI = dwMax /dw_;
    dwMax = dw_ *dwMaxI;
    Eigen::ArrayXd dwsq(Eigen::ArrayXd::LinSpaced(dwMaxI, dw(0), dwMax));

    std::cout << "17. Heading and frequency interpolation at once for Qtf\n";
    start = std::chrono::system_clock::now();
    subQtf = qtf.getQtfAt(hs, ws, dwsq, 1, ComplexInterpolationStrategies::RE_IM_AMP);
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "18. Writing wdwFinalQTensor.dat\n";
    start = std::chrono::system_clock::now();
    subQtf.writeWdWTensor("wdwFinalQTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "19. Heading and frequency interpolation at once for MQtf\n";
    start = std::chrono::system_clock::now();
    subMQtf = mqtf.getQtfAt(hs, ws, dwsm, 1, ComplexInterpolationStrategies::RE_IM_AMP);
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";

    std::cout << "20. Writing wdwFinalTensor.dat\n";
    start = std::chrono::system_clock::now();
    subMQtf.writeWdWTensor("wdwFinalTensor.dat");
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";
}

void ResponseSpectrum2ndS_test1()
{
    std::cout << "Response Spectrum 2nd (MQtf) test\n";
    BV::Spectral::MQtf mqtf;
    mqtf.readQtfFromFile("test_data/mqtf_d.qtf");

    //Jonswap swell(5.0, 14.0, 3.3);      //   swell = sp.Jonswap( hs  = 5.0 , tp = 14.0 , gamma = 3.3 , heading = 0.0 )
    // Jonswap(hs = 1.0, tp = 10.0, gamma = 1.0, heading = 0.0, spreadingType = Cosn, spreadingValue = 4)
    BV::Spectral::Jonswap swell = BV::Spectral::Jonswap( 1.0, 10.0, 1., 0., BV::Spectral::SpreadingType::Cosn, 4);

    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(swell));
    SeaState ss(spectrums);             //   ss = sp.SeaState( [ windsea  ] )

    ResponseSpectrum2ndMQtf rs(ss, mqtf);
    auto rspec = rs.get();
    std::cout << rspec << std::endl;
}

void ResponseSpectrum2ndS_test2() {
    std::cout << "Response Spectrum 2nd (MQtf) test" << __LINE__ << " / " << __FILE__ << "\n";

    std::cout << "Reading mqtf\n";
    BV::Spectral::MQtf mqtf;
    //mqtf.readQtfFromFile("test_data/mqtf_fx.qtf");  // diff
mqtf.readQtfFromFile("test_data/munit_00d.qtf");  // diff
    //mqtf.readQtfFromFile("test_data/m2qtf_00d.qtf");  // diff

    std::cout << "Jonswap spectrum\n";
    // Jonswap(hs = 1.0, tp = 10.0, gamma = 1.0, heading = 0.0, spreadingType = Cosn, spreadingValue = 4)
    BV::Spectral::Jonswap swell = BV::Spectral::Jonswap( 1.0, 10.0, 1., 0., BV::Spectral::SpreadingType::Cosn, 4);

    std::cout << "SeaState\n";
    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(swell));
    SeaState seaState(spectrums);

    /*
    std::cout << "Wif\n";
    // Spectrum, wmin, wmax, nbSeed, spreadNbheading, seed = 0, depth = -1
    BV::Spectral::Wif wif = BV::Spectral::Wif( swell, 0.025, 2.0, 50, 10);
    std::shared_ptr<BV::Spectral::Wif> p_wif1(std::make_shared<BV::Spectral::Wif>(wif));
    //printWifInfo(p_wif1, "Wif1");
    */

    std::cout << "Response spectrum (MQtf)\n";
    BV::Spectral::ResponseSpectrum2ndMQtf rsp = BV::Spectral::ResponseSpectrum2ndMQtf(seaState, mqtf);
    auto frq = rsp.getFrequencies();
    const Eigen::ArrayXd & response = rsp.get();
    std::ofstream r_out("response.dat");
    for(decltype(frq.size())f = 0; f < frq.size(); f ++)
        r_out << frq[f] << "\t" << response[f] << "\n";
    std::cout << "Done!\n";
}
void SpectrumMoments_test1()
{
    std::cout << "SpectrumMoments: test no 1\n";

    //Jonswap swell(5.0, 14.0, 3.3);      //   swell = sp.Jonswap( hs  = 5.0 , tp = 14.0 , gamma = 3.3 , heading = 0.0 )
    // Jonswap(hs = 1.0, tp = 10.0, gamma = 1.0, heading = 0.0, spreadingType = Cosn, spreadingValue = 4)
    BV::Spectral::Jonswap swell = BV::Spectral::Jonswap( 1.0, 10.0, 1., 0., BV::Spectral::SpreadingType::Cosn, 4);

    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(swell));
    SeaState ss(spectrums);             //   ss = sp.SeaState( [ windsea  ] )

    // Rao
    Rao rao(readRaoFromFile("my_10.rao"));
    //Eigen::ArrayXd w(Eigen::ArrayXd::LinSpaced(10, 0.1, 1.0));
    const Eigen::ArrayXd w(rao.getFrequencies());
    std::cout << "Number of headings    : " << rao.getHeadings().size()<< "\n";
    std::cout << "Forward speed         : " << rao.getForwardSpeed() << "\n";
    std::cout << "refPoint              : (" << rao.getReferencePoint()[0] << ", " << rao.getReferencePoint()[1] << ", " << rao.getReferencePoint()[2] << ")\n";
    std::cout << "waveRefPoint          : (" << rao.getWaveReferencePoint()[0] << ", " << rao.getWaveReferencePoint()[1] << ")\n";
    std::cout << "Number of frequencies : " << w.size() << "\n";
    std::cout << "Number of modes       : " << rao.getModes().size() << "\n";
    std::cout << "min frequency         : " << w[0] << "\n";
    std::cout << "max frequency         : " << w[w.size() - 1] << "\n";

    auto unitMoments(spectrums[0]->integrate_partial_moments(w, 15, 0.1));
    //auto unitMoments(spectrums[0]->integrate_partial_moments(w, 15));

    // Output to the file for the first beta
    Eigen::Index beta(0);
    Eigen::Index mode(0);
    std::ofstream out_1("r2_orig.dat");
    std::ofstream out_2("r2_inter.dat");
    Eigen::Index ptsPerInterval(5);
    const Eigen::Tensor<double, 4> coeffs(rao.getRao2LinearCoefficientsForOmega());
    const Eigen::Tensor<double, 3> modules(rao.getModules());
    for (Eigen::Index freq = 0; freq < w.size() - 1; freq++)
    {
        auto dw((w[freq + 1] - w[freq]) / ptsPerInterval);
        out_1 << w[freq] << "\t" << std::pow(modules(beta, freq, mode), 2) << "\n";
        std::cout << freq << ": " << coeffs(beta, freq, mode, 0) << " , " << coeffs(beta, freq, mode, 1) << "\n";
        for (Eigen::Index pts = 0; pts < ptsPerInterval; pts++)
        {
            auto w_(w[freq] + dw * pts);
            out_2 << w_ << "\t" << coeffs(beta, freq, mode, 1)*w_ +coeffs(beta, freq, mode, 0) << "\n";
        }
    }
    out_1 << w[w.size()-1] << "\t" << std::pow(modules(beta, w.size()-1, mode), 2) << "\n";
    out_2 << w[w.size()-1] << "\t" << std::pow(modules(beta, w.size()-1, mode), 2) << "\n";
}
void SpectrumMoments_test2()
{
    std::cout << "SpectrumMoments: test no 2\n";

    // Rao
    //Rao rao(readRaoFromFile("my_10.rao"));
    Rao rao(readRaoFromFile("Heave.rao"));
    //Eigen::ArrayXd w(Eigen::ArrayXd::LinSpaced(10, 0.1, 1.0));
    const Eigen::ArrayXd w(rao.getFrequencies());
    std::cout << "Number of headings    : " << rao.getHeadings().size()<< "\n";
    std::cout << "Forward speed         : " << rao.getForwardSpeed() << "\n";
    std::cout << "refPoint              : (" << rao.getReferencePoint()[0] << ", " << rao.getReferencePoint()[1] << ", " << rao.getReferencePoint()[2] << ")\n";
    std::cout << "waveRefPoint          : (" << rao.getWaveReferencePoint()[0] << ", " << rao.getWaveReferencePoint()[1] << ")\n";
    std::cout << "Number of frequencies : " << w.size() << "\n";
    std::cout << "Number of modes       : " << rao.getModes().size() << "\n";
    std::cout << "min frequency         : " << w[0] << "\n";
    std::cout << "max frequency         : " << w[w.size() - 1] << "\n";

    rao = rao.getRaoIn2piRange();

    Rao rao2 ( rao.getRaoAtFrequencies(Eigen::ArrayXd::LinSpaced(50, 0.05, 2.0),
                                       BV::Math::Interpolators::InterpScheme::LINEAR,
                                       ComplexInterpolationStrategies::AMP_PHASE,
                                       BV::Math::Interpolators::ExtrapolationType::BOUNDARY
                                       ));
    Rao rao3 ( rao2.getRaoAtHeadings  (Eigen::ArrayXd::LinSpaced(12, 0.0, 2*M_PI),
                                       BV::Math::Interpolators::InterpScheme::LINEAR,
                                       ComplexInterpolationStrategies::AMP_PHASE,
                                       BV::Math::Interpolators::ExtrapolationType::BOUNDARY
                                       ));

    Jonswap spec1(1.0, 10.0, 1.0, M_PI/2, SpreadingType::Cosn, 2.);
    //Jonswap spec2(3.0,  8.0, 1.0, 0.0, SpreadingType::Cosn, 3.);
    //Jonswap spec3(2.0,  8.0, 1.0,  M_PI/2, SpreadingType::No, 0.);
    //Jonswap spec4(2.0,  8.0, 1.0, -M_PI/2, SpreadingType::No, 0.);
    std::vector<std::shared_ptr<ParametricSpectrum>> spectrums;
    spectrums.push_back(std::make_shared<Jonswap>(spec1));
    //spectrums.push_back(std::make_shared<Jonswap>(spec2));
    //spectrums.push_back(std::make_shared<Jonswap>(spec3));
    //spectrums.push_back(std::make_shared<Jonswap>(spec4));
    SeaState seaState(spectrums);
    int elapsed_milliseconds(0);
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::vector<std::shared_ptr<SeaState>> seaStates;
    seaStates.push_back(std::make_shared<SeaState>(seaState));

    start = std::chrono::system_clock::now();
    /*
     * seaStates : array of SeaState
     * rao3      : Rao
     * true      : isM4 = true (compute m4)
     * 0.05      : dw
     * 2         : db = 2°
     */
    SpectralMoments sm(seaStates, rao3, true, 0.05, 2);
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";
    std::cout << "Res.SpectrumMoments: m0 = " << sm.getM0s()(0,0) << "\n";
    std::cout << "Res.SpectrumMoments: m2 = " << sm.getM2s()(0,0) << "\n";
    std::cout << "Res.SpectrumMoments: m4 = " << sm.getM4s()(0,0) << "\n";

    start = std::chrono::system_clock::now();
    ResponseSpectrum rsp(seaState, rao3, 1, false, true);   // checkRAo == false, because rao3 has headings 0 and 2pi, which are "identical"
    end = std::chrono::system_clock::now();
    elapsed_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "Time elapsed: " << elapsed_milliseconds/1000. << " sec\n";
    std::cout << "Res.ResponseSpectrum: m0 = " << rsp.getM0() << "\n";
    std::cout << "Res.ResponseSpectrum: m2 = " << rsp.getM2() << "\n";
    std::cout << "Res.ResponseSpectrum: m4 = " << rsp.getM4() << "\n";
}

void SpectrumMoments_test3()
{
    std::cout << "SpectrumMoments: test no 3\n";

    // Rao
    Rao rao(readRaoFromFile("my_10.rao", 0.35));

    Eigen::ArrayXd beta(10);
    beta << 0.0, 0.32, 0.33, 0.5, 1.0, 1.2, 1.3, 1.8, 4.0, 5.0;
    const Eigen::Tensor<double, 4> coeffs(rao.getRao2LinearCoefficientsForOmegaAtBeta(beta));

    std::list<double> l = { 1.5, 1.3, 1.3, 1.4 , 1.1, 2.0};
    for (std::list<double>::iterator it = l.begin(); it != l.end(); it++)
        std::cout << *it << "\n";
    std::cout << "unique\n";
    l.unique([](double x, double y) {return std::abs(x - y) < 1.0e-8; });
    for (std::list<double>::iterator it = l.begin(); it != l.end(); it++)
        std::cout << *it << "\n";
    std::cout << "sorted\n";
    l.sort();
    for (std::list<double>::iterator it = l.begin(); it != l.end(); it++)
        std::cout << *it << "\n";

    int nZero(0);
    int nN1(0);
    const Eigen::Tensor<double, 5> coeffs2(rao.getRao2LinearCoefficients(&nZero, &nN1, 1));
    std::cout << "nZero = " << nZero << "\n";
    std::cout << "nN1   = " << nN1 << "\n";

    //Jonswap spec(2.0,  8.0, 1.0,  M_PI/2, SpreadingType::No, 0.);
    Jonswap spec(2.0,  8.0, 1.0,  M_PI/2, SpreadingType::Cosn, 2.);
    auto b(rao.getHeadingIn2PiRange());
    spec.spreading->integrate_spreading(b, 7, 4, 0.044);
}
#endif

int main()
{
#ifdef DISABLE_TESTS
    std::cout << "The tests are disabled\n";
    return 0;
#else
//    Spectrum_test();

//    ResponseSpectrum2nd_test();
//    ResponseSpectrum2nd_test2("/media/igor/Linux_Partition/dr_git/Snoopy/TimeDomain/Tests/test_data/qtf_fx.qtf");   // diff mode
//    ResponseSpectrum2nd_test2("/media/iten/SSD-Disk/AllSeas/Calculations/Elastic/rao/spg_m7.qtf"); // sum mode
//    ResponseSpectrum_test1("/home/iten/Work/dr_git/tester/Spec/Heave.rao");
//    ResponseSpectrum2nd_test3("/home/iten/Work/calculations/hsomf/sym0m/rao", "qtffx");
//    ResponseSpectrum2nd_test4("/home/iten/Work/dr_git/Snoopy/TimeDomain/Tests/test_data/qtf_fx.qtf");

//    BV::Tools::sort_test();
//    sort_test();

//    wif_test();
//    splinterTest();
//    QtfSTest();
//    ResponseSpectrum2ndS_test1();
//    ResponseSpectrum2ndS_test2();
//    SpectrumMoments_test1();
    SpectrumMoments_test2();
//    SpectrumMoments_test3();
    return 0;
#endif
}
