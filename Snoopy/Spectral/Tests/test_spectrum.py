from Snoopy import Spectral as sp
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import pytest

def test_jonswap_regression():
    assert( np.isclose( sp.Jonswap.tzOverTp2gamma(  sp.Jonswap.tzOverTp( 2.0 )  ) , 2.0 ))
    assert( np.isclose( sp.Jonswap.tmOverTp2gamma(  sp.Jonswap.tmOverTp( 2.0 )  ) , 2.0 ))
    assert( np.isclose( sp.Jonswap.tzOverTm2gamma(  sp.Jonswap.tzOverTm( 2.0 )  ) , 2.0 ))

    gamma = 1.5
    tp = 10.0
    assert( np.isclose( sp.Jonswap.t0m12tz(  sp.Jonswap.tz2t0m1( tp,gamma ) , gamma  ) , tp ))
    assert( np.isclose( sp.Jonswap.t0m12tp(  sp.Jonswap.tp2t0m1( tp,gamma ) , gamma  ) , tp ))

    ss = sp.SeaState( sp.Jonswap( 1. , tp , gamma ) )
    assert( np.isclose( ss.integrate_tz() , sp.Jonswap.tp2tz( tp, gamma ), rtol = 0.01))
    assert( np.isclose( ss.integrate_t0m1() , sp.Jonswap.tp2t0m1( tp, gamma ), rtol = 0.01))
    assert( np.isclose( ss.integrate_tm() , sp.Jonswap.tp2tm( tp, gamma ), rtol = 0.01))
    assert( np.isclose( ss.integrate_t0m1() , sp.Jonswap.tz2t0m1( ss.integrate_tz(), gamma ), rtol = 0.01))

    #Goda / gamma
    gamma = np.arange(1.0 , 3.0, 0.5)
    assert(np.isclose( sp.Jonswap.goda_to_gamma( sp.Jonswap.gamma_to_goda( gamma, variant = "ecmwf" ), variant = "ecmwf") , gamma ).all())

    gamma = 1.5
    assert(np.isclose( sp.SeaState( sp.Jonswap( 1.0 , 10.0 , gamma ) ).integrate_spectral_peakness() ,  sp.Jonswap.gamma_to_goda( gamma ) , 5e-3))

    # The regression for Goda ECMWF is not great... 5% tolerance
    gamma = 1.5
    assert(np.isclose( sp.SeaState( sp.Jonswap( 1.0 , 10.0 , gamma ) ).integrate_spectral_peakness(variant="ecmwf", w = np.linspace(0.1 , 2.0, 2000)) ,  sp.Jonswap.gamma_to_goda( gamma, variant = "ecmwf" ) , 0.05))



def test_logNormal() :
    tp = 10.0
    hs = 2.0
    goda = 2.0

    sigma = sp.LogNormal.sigma_from_goda( goda ) # With analytical expression
    spec = sp.LogNormal( hs , tp , sigma )
    ss = sp.SeaState( spec )

    assert( np.isclose( goda, ss.integrate_spectral_peakness() ) )
    assert( np.isclose( spec.getTm(), ss.integrate_tm() ) )
    print ("LogNormal ok")



def test_tz_jonswap() :
    hs_1 = 4.0
    tp_1 = 10.0
    gamma = 3.0
    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = gamma, heading = np.deg2rad(90) , spreading_type = sp.SpreadingType.No  )
    tz_regression = sp.Jonswap.tp2tz( tp_1, gamma )
    tz_integration = sp.SeaState(windSea).integrate_tz( w = np.arange(0.01, 4.0 , 0.025) )
    print ("Tz_regression  = {}".format(tz_regression))
    print ("Tz_integration = {}".format(tz_integration))
    assert(   0.98 < tz_regression / tz_integration <  1.02 )

    tm_regression = sp.Jonswap.tp2tm( tp_1, gamma )
    tm_integration = sp.SeaState(windSea).integrate_tm( w = np.arange(0.01, 4.0 , 0.025) )
    print ("Tm_regression  = {}".format(tm_regression))
    print ("Tm_integration = {}".format(tm_integration))
    assert(   0.98 < tm_regression / tm_integration <  1.02 )

    tp_integration = sp.SeaState(windSea).find_tp( )
    print ("Tp             = {}".format(tp_1))
    print ("Tp_integration = {}".format(tp_integration))
    assert(   0.98 < tp_integration / tp_1 <  1.02 )

    assert( sp.Jonswap.tp2tz(8.5,3.0) ==  sp.Jonswap.tp2tz([8.5],3.0)[0]   )
    assert( sp.Jonswap.tp2tm(8.5,3.0) ==  sp.Jonswap.tp2tm([8.5],3.0)[0]   )
    assert( sp.Jonswap.tz2tp(8.5,3.0) ==  sp.Jonswap.tz2tp([8.5],3.0)[0]   )
    assert( sp.Jonswap.tm2tp(8.5,3.0) ==  sp.Jonswap.tm2tp([8.5],3.0)[0]   )
    print( "Test period ok")



@pytest.mark.parametrize("spec1dof, dofRange, extrap", [( sp.Jonswap ,np.arange(1,11,1) , True),
                                                        ( sp.SimpleOchiHubble ,np.arange(1,11,1), True ),
                                                        ( sp.LogNormal ,np.arange(0.05,0.2,0.05), False ),
                                                       ])
def test_tz( spec1dof, dofRange, extrap ):
    print ("Test moment, Tm and Tz")
    hs = 1.0
    tp = 10.0
    res = pd.DataFrame( index = dofRange, columns = ["Analytical_Tm", "Numerical_Tm", "Analytical_Tz", "Numerical_Tz"] , dtype = float )
    for v in res.index :
        spec = spec1dof(hs, tp, v)
        res.loc[v,"Analytical_Tz"] = spec.getTz()
        res.loc[v,"Numerical_Tz"] = spec.integrate_tz(extrap = extrap)
        res.loc[v,"Analytical_Tm"] = spec.getTm()
        res.loc[v,"Numerical_Tm"] = spec.integrate_tm(extrap = extrap)
        res.loc[v,"Analytical_m2"] = spec.getMoment(2)
        res.loc[v,"Numerical_m2"] = spec.integrate_moment(2,extrap = extrap)
        res.loc[v,"Analytical_m1"] = spec.getMoment(1)
        res.loc[v,"Numerical_m1"] = spec.integrate_moment(1,extrap = extrap)
    assert( np.isclose( res.loc[:,"Analytical_Tz"].values , res.loc[:,"Numerical_Tz"].values , rtol = 0.01).all())
    assert( np.isclose( res.loc[:,"Analytical_Tm"].values , res.loc[:,"Numerical_Tm"].values , rtol = 0.01).all())
    assert( np.isclose( res.loc[:,"Analytical_m1"].values , res.loc[:,"Numerical_m1"].values , rtol = 0.01).all())
    assert( np.isclose( res.loc[:,"Analytical_m2"].values , res.loc[:,"Numerical_m2"].values , rtol = 0.01).all())


def demo_Jonswap(ax=None) :
    if ax is None :
        fig, ax = plt.subplots()
    for gamma in [1., 3, 5, 10.] :
        spec = sp.Jonswap( 1,10,gamma )
        spec.plot(ax=ax, wMax = 1.8, label = r"$\gamma = {:.0f}$".format(gamma))
        ax.set_ylim([1e-4,0.7])
        ax.legend()
    return ax

def demo_OchhiHubble(ax= None) :
    if ax is None :
        fig, ax = plt.subplots()
    for lambda_ in [0.1, 1., 3, 5, 15.] :
        spec = sp.SimpleOchiHubble( 1,10,lambda_ )
        spec.plot(ax=ax, wMax = 1.8, label = r"$\lambda_ = {:.0f}$".format(lambda_))
        ax.set_ylim([1e-4,0.7])
        ax.legend()
    return ax

def demo_Wallop() :
    fig, ax = plt.subplots()
    for m in [3,4,5] :
        for q in [ 4 ] :
            spec = sp.Wallop( 1,10,m,q )
            spec.plot(ax=ax, wMax = 1.8, label = r"$m = {:.0f}, q={:.0f}$".format(m,q))
            ax.set_ylim([0.,0.5])
            ax.legend()
    ax.set_yscale("log")
    return ax

def compareJonswapOchihubble():

    hs = 1.0
    tp = 10.0
    gamma = 2.0

    jonswap = sp.Jonswap( hs, tp, gamma )

    ss = sp.SeaState(jonswap)
    ochiLsq = ss.fitParametric1D( [sp.SimpleOchiHubble] , solver = "global")
    wallopLsq = ss.fitParametric1D( [sp.Wallop] , solver = "global")
    gammaLsq = ss.fitParametric1D( [sp.Gamma] , solver = "global")

    ax = jonswap.plot()
    ax = ochiLsq.plot(ax=ax, label = "OchiHubble")
    ax = wallopLsq.plot(ax=ax, label = "Wallop")
    ax = gammaLsq.plot(ax=ax, label = "Gamma")
    ax.legend()
    ax.set_xlim([0.1,1.5])
    return ax


def test_energyRange():

    eRatio = 0.85
    spec1 = sp.Jonswap(1,10.,1.)
    erange = spec1.energyRange( eRatio , 0.0 , 5.0 , 0.0001 )
    energy85 = spec1.integrate_moment( n = 0, wmin = erange[0] ,wmax = erange[1] , extrap = False)
    energyFull = spec1.integrate_moment( n = 0 , wmin = 0.0 , wmax = 3.0 , extrap = True)
    print (energyFull*eRatio / energy85 )
    assert( np.isclose(  energyFull*eRatio , energy85, rtol = 1e-3  ) )

    spec2 = sp.API(1. , 0.1, 0.)
    erange = spec2.energyRange( eRatio , 0.0 , 5.0 , 0.0001 )
    energy85 = spec2.integrate_moment( n = 0, wmin = erange[0] ,wmax = erange[1] , extrap = False)
    energyFull = spec2.integrate_moment( n = 0 , wmin = 0.0 , wmax = 5.0 , extrap = False)
    print (energyFull*eRatio / energy85 )
    assert( np.isclose(  energyFull*eRatio , energy85 , rtol = 5e-3 ) )


def test_read_write():

    for t , v in (  ( sp.SpreadingType.Wnormal , 0.25) , (sp.SpreadingType.Cosn , 3) ,(sp.SpreadingType.Cos2s , 6),(sp.SpreadingType.No , 6) ):
        spec = sp.Jonswap(1.0, 10.0, 1.5, spreading_type = sp.SpreadingType.Wnormal , spreading_value = 0.28)
        ss_str = spec.hspecString()
        spec_back = sp.Spectrum.FromHspecString( ss_str )
        ss_str_back = spec_back.hspecString()
        assert( ss_str_back == ss_str)

if __name__ == "__main__" :

    test_jonswap_regression()
    test_tz_jonswap()
    test_tz(  spec1dof = sp.SimpleOchiHubble, dofRange = np.arange(1,11,1) , extrap = True)
    test_tz(  sp.Jonswap ,  np.arange(1,11,1) , True)
    test_logNormal()
    test_energyRange()
    
    test_read_write()
    
    
    
    
        
    
    



