
"""
Compare Snoopy to StarSpec
"""

import numpy as np
from scipy import stats
from Snoopy import Spectral as sp
from Snoopy import Statistics as st
from Snoopy import logger
import pandas as pd
import pytest

logger.setLevel(10)

def test_startspec():
    rtol = 0.02


    for rao in ["my_10.rao" ,"stress_nonsym.rao" ]:

        ref = pd.read_csv( f"{sp.TEST_DATA:}/hspec_reference/genericSeaState_spec/Short_term/genericSeaState_moments.csv", delimiter=";")
        ref = ref.loc[ ref.loc[:,"#raofile"] == rao[:-4] , :].reset_index()

        rao_file = f"{sp.TEST_DATA:}/hspec_reference/RAO/" + rao

        rao_o = sp.Rao(rao_file)

        #RAO interpolation, same as in StarSpec
        rao = rao_o.getRaoForSpectral( wmin = 0.05 , wmax = 3.5 , db_deg = 5. )

        spec1 = sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(180.) )
        spec2 = sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(180.), spreading_type = sp.SpreadingType.Wnormal , spreading_value = np.deg2rad(15.) )
        spec3 = sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(210.), spreading_type = sp.SpreadingType.Wnormal , spreading_value = np.deg2rad(15.) )
        spec4 = sp.LogNormal(   hs= 1.0,  tp = 13.376, sigma = 0.05 , heading = np.deg2rad(180.0) )

        caseDf = pd.DataFrame( data = {
                                        0 : [sp.SeaState(spec1)],
                                        1 : [sp.SeaState(spec2)],
                                        2 : [sp.SeaState( [spec1, spec1] )],
                                        3 : [sp.SeaState( [spec2, spec2] )],
                                        4 : [sp.SeaState( [spec1, spec3] )],
                                        5 : [sp.SeaState( [spec4] )],
                                        6 : [ sp.SeaState( sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(300.) ))] ,
                                        7 : [ sp.SeaState( sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(297.501) ))] ,
                                        8 : [ sp.SeaState( sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(300.) , spreading_type = sp.SpreadingType.Wnormal , spreading_value = np.deg2rad(15.)))] ,
                                        9 : [ sp.SeaState( sp.Jonswap(   hs= 1.0,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(297.501), spreading_type = sp.SpreadingType.Wnormal , spreading_value = np.deg2rad(15.) ))] ,
                                        10 : [ sp.SeaState( sp.Jonswap(   hs= 1.5,  tp = 13.376, gamma = 1.0 , heading = np.deg2rad(355.) ))] ,
                                       } , index = ["ss"] ).transpose()


        for icase, case in caseDf.iterrows() :
            logger.debug( f"test case {icase:}, {case.ss:}")
            rSpec = sp.ResponseSpectrum( case.ss, rao, computeM4 = True)

            print (icase, case.ss)

            m0, m2 = rSpec.getM0M2()

            #Check consistency of RAO.derivate() and m4 calculation
            rSpecDeriv = sp.ResponseSpectrum( case.ss, rao.getDerivate(n=1) )
            assert( np.isclose( rSpecDeriv.getM2(), rSpec.getM4() ))
            assert( np.isclose( rSpecDeriv.getM0(), rSpec.getM2() ))

            logger.debug( f"{m0:} { m0 / ref.m0[icase]:}" )
            logger.debug( f"{m2:} { m2 / ref.m2[icase]:}" )
            assert( np.isclose( m0, ref.m0[icase], rtol=rtol ))
            assert( np.isclose( m2, ref.m2[icase], rtol=rtol ))

        print (rao , "Ok")


@pytest.mark.parametrize("engine", [ "ResponseSpectrum", "SpectralMoments" ] )
def test_starspec_LT(engine):
    """ test up to long term calculation
    """
    from Snoopy import Fatigue as ft
    rao = sp.Rao(f"{sp.TEST_DATA:}/hspec_reference/RAO/my_10.rao")

    w_min, w_max, dw = 0.05, 3.5 , 0.005

    #RAO interpolation, same as in StarSpec
    if engine == "ResponseSpectrum":
        rao = rao.getRaoForSpectral(wmin= w_min, wmax = w_max, db_deg = 5. , dw = dw )
        tol = 0.001
    else:
        tol = 0.015


    starSpec_sht_ref = pd.read_csv( f"{sp.TEST_DATA:}/hspec_reference/iacs_spec/Short_term/iacs_moments.csv", sep = ";")
    starSpec_sht_ref = starSpec_sht_ref.rename( columns = {"Hs   1" : "hs_0" , "Tp   1" : "tp_0" , "Gamma      " : "gamma_0" , "Prob": "PROB"})
    starSpec_sht_ref = starSpec_sht_ref.loc[starSpec_sht_ref["#raofile"] == "my_10" ]
    
    starSpec_sht_ref.loc[:, "Heading_0"] = -starSpec_sht_ref.loc[:, "Wave direction   1"] + starSpec_sht_ref.loc[:, "Azimuth"] + 180
    starSpec_sht_ref.loc[:, "Heading_0"] = np.mod( starSpec_sht_ref.loc[:, "Heading_0"], 360.)

    ss_df2 = sp.SeaStatesDF(starSpec_sht_ref)
    ss_df2.loc[:, "Spectrum_0"] = "Jonswap"
    ss_df2.loc[:, "SpreadingType_0"] = "Cosn"
    ss_df2.loc[:, "SpreadingValue_0"] = 2.0

    #--------- Check short term
    m0_, m2_ = ss_df2.computeSpectral(rao , linear_hs=True, progressBar = False, engine = engine, num_threads = 5, w_min = w_min, w_max = w_max, dw = dw)
    ss_df2.loc[: , "m0_Snoopy"] = m0_[:,0]
    ss_df2.loc[: , "m2_Snoopy"] = m2_[:,0]

    ss_df2.loc[: , "err"] = ss_df2.m0_Snoopy / ss_df2.m0 - 1
    err_df = ss_df2.loc[ss_df2.hs_0 == 1.5 , : ].set_index( ["tp_0" , "Heading_0"] ).loc[: , "err"].unstack()

    print (f"Max error on m0 : {err_df.abs().max().max():.3%}")
    assert( err_df.abs().max().max() < tol )

    ss_df2.loc[: , "err_m2"] = ss_df2.m2_Snoopy / ss_df2.m2 - 1
    err_df_m2 = ss_df2.loc[ss_df2.hs_0 == 1.5 , : ].set_index( ["tp_0" , "Heading_0"] ).loc[: , "err_m2"].unstack()
    print (f"Max error on m2 : {err_df_m2.abs().max().max():.3%}")
    assert( err_df_m2.abs().max().max() < tol )

    #dfSurface(err_df_m2 ,interpolate = False, colorbar = True)

    sStat_Snoopy = sp.SpectralStats( m0_[:,0],  m2_[:,0])
    sStat_StarSpec = sp.SpectralStats( ss_df2.m0.values,  ss_df2.m2.values)


    #--------- Check long-term
    lt = st.LongTermSpectral(  sStat_Snoopy.getRs() , sStat_Snoopy.Rtz, probabilityList = ss_df2.PROB.values, dss = 10800. )
    lt_StarSpecSht = st.LongTermSpectral(  sStat_StarSpec.getRs() , sStat_StarSpec.Rtz, probabilityList = ss_df2.PROB.values, dss = 10800. )

    starSpec_lt_ref = pd.read_csv( f"{sp.TEST_DATA:}/hspec_reference/iacs_spec/report_iacs.csv", sep = ";")
    resp_25_starspec = starSpec_lt_ref.loc[0 , "Long term value with prob= 0.5000E+00 in  25.00 years (with dss=   0.1080E+05 )"] * 0.5
    resp_25 = lt.p_to_x( 0.5 , 25 )
    resp_25_starspec_sht = lt_StarSpecSht.p_to_x( 0.5 , 25 , preconditioner =  stats.norm().ppf )
    
    
    nb_cycle_starspec = starSpec_lt_ref.loc[0 , "Total number of cycles in   25.00 years"]
    nb_cycle_starspec_snoopy = 365.24*25*24*3600 / lt.meanRtz()

    print ( "Ref (starspec) :", resp_25_starspec )
    print ( "Full Snoopy :", resp_25 )
    print ( "LT Snoopy (Starspec moment)  :", resp_25_starspec_sht )

    assert( np.isclose( resp_25_starspec_sht ,  resp_25_starspec , rtol = 0.001))
    assert( np.isclose( resp_25 ,  resp_25_starspec , rtol = 0.001))
    assert( np.isclose( nb_cycle_starspec ,  nb_cycle_starspec_snoopy , rtol = 0.001))

    
    #--------- Check long-term (PROB 1e-2)
    

    #-------- Check fatigue
    starspec_fat = pd.read_csv( f"{sp.TEST_DATA:}/hspec_reference/iacs_spec/report_iacs.csv", sep = ";").loc[1 , "Damage in  25.00 years"]
    lt = st.LongTermSpectral(  sStat_Snoopy.getRs()*1e-6 , sStat_Snoopy.Rtz, probabilityList = ss_df2.PROB.values, dss = 10800. )
    data = np.array( [[53.4, 3,  1.520E12],
                      [0.1, 5, 4.330E15],], dtype = float )

    sn_curve = ft.SnCurve(data)
    snoopy_fat = lt.fatigue_damage(sn_curve, 25.)
    snoopy_fat_num = lt.fatigue_damage_num(sn_curve, 25.)
    
    print ( snoopy_fat / snoopy_fat )
    print ( snoopy_fat / snoopy_fat_num )

    assert( np.isclose( snoopy_fat, starspec_fat, rtol = 1e-3 ) )
    assert( np.isclose( snoopy_fat_num, starspec_fat, rtol = 5e-3 ) )
    

if __name__ == "__main__" :
    # test_startspec()
    # test_starspec_LT(engine = "ResponseSpectrum")
    test_starspec_LT(engine = "SpectralMoments")    

