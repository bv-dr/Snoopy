import os
import numpy as np
from Snoopy import Spectral as sp

test_file = f"{sp.TEST_DATA:}/spec2D.csv"

wdir = f"{sp.TEST_DATA:}/test_write"

def test_seastate2D_basic():

    ss2d = sp.SeaState2D.Read_csv( test_file )
    ss2dm = ss2d.toMultiModal()

    assert( np.isclose( ss2dm.integrate_hs() ,  ss2d.integrate_hs() ))
    assert( np.isclose( ss2dm.integrate_tz() ,  ss2d.integrate_tz() ))
    print ("SeaState2D toMultiModal Ok")

    hs = 4.0
    ss = sp.SeaState( sp.Jonswap( hs , 12 , 1, np.pi , sp.SpreadingType.Cosn , 3 ) )
    ss2d = sp.SeaState2D( ss )

    ss2dm = ss2d.toMultiModal()

    ss2dm.hspecString( f"{wdir:}/ss2Dm.dat")

    assert( np.isclose( ss2d.integrate_hs() , hs , rtol = 0.001) )
    assert( np.isclose( ss2dm.integrate_hs() , hs , rtol = 0.001) )

    assert( np.isclose( ss2d.integrate_mean_direction() , np.pi , rtol = 0.001) )
    assert( np.isclose( ss.integrate_mean_direction() , np.pi , rtol = 0.001) )


if __name__ == "__main__" :

    print ("Run test")

    test_seastate2D_basic()
