import numpy as np
from Snoopy import Spectral as sp
from Snoopy import Statistics as st

def test_idss():

    rtz = 1.0
    hs1 = 1.0
    rp1 = 10800
    hs2 = 1.1

    rp2 = sp.idss_convert_rp( rp1 , rtz , hs1 , hs2 )

    assert( np.isclose( rp1, sp.idss_convert_rp( rp2 , rtz, hs2, hs1 )))

    x_1 = st.ReturnLevel.rp_to_x_distribution( sp.Jonswap( hs1 , rtz , 1.0).getLinearCrestDistribution(), rtz , rp1 )
    x_2 = st.ReturnLevel.rp_to_x_distribution( sp.Jonswap( hs2 , rtz , 1.0).getLinearCrestDistribution(), rtz , rp2 )

    assert( np.isclose(x_1 , x_2) )


if __name__ == "__main__" :

    test_idss()


