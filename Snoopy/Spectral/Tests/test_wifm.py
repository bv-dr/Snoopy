from Snoopy import Spectral as sp
import numpy as np


wdir = f"{sp.TEST_DATA:}/test_write"

def test_readWrite():

    wif  = sp.Wif.Jonswap( hs = 1.0 , tp = 10.0, gamma = 1.0, wifArgs = {"seed" : 15 } )
    wif2 = sp.Wif.Jonswap( hs = 5.0 , tp = 10.0, gamma = 1.0, wifArgs = {"seed" : 15 } )
    wifm = sp.Wifm( [wif, wif2 ], np.array([[0.,100], [100., 200.]]) )
    wifm.write(f"{wdir:}/test")
    wifm2 = sp.Wifm.Read( f"{wdir:}/test.wifm" )
    assert( np.isclose( sp.Wif(wifm2.getWifAtIndex(0)).hs , wif.hs ))

    print (wifm2)


def test_copy():
    wif  = sp.Wif.Jonswap( hs = 1.0 , tp = 10.0, gamma = 1.0, wifArgs = {"seed" : 15 } )
    wif2 = sp.Wif.Jonswap( hs = 5.0 , tp = 10.0, gamma = 1.0, wifArgs = {"seed" : 15 } )
    wifm = sp.Wifm( [wif, wif2 ], np.array([[0.,100], [100., 200.]]) )
    wifm_loc = sp.Wifm(wifm)  # shallow copy for now
    wifm_loc.setHeadings(1.)
    assert(np.isclose(wifm.getWifAtIndex(0).getHeadings(), 0.).all())
    assert(np.isclose(wifm_loc.getWifAtIndex(0).getHeadings(), 1.).all())

if __name__ == "__main__" :

    test_readWrite()
