import numpy as np
from Snoopy import Spectral as sp



def test_heading_convention():

    glob_wave_heading = np.pi/4
    vessel_azimuth = np.pi/2
    local = sp.headingConvention.local_from_wave_and_vessel(glob_wave_heading, vessel_azimuth, convention = "starspec")

    new_azimuth = 0.0

    new_glob_wave_heading = sp.headingConvention.get_new_wave_heading(glob_wave_heading, vessel_azimuth, new_azimuth, convention= "starspec")

    local_new = sp.headingConvention.local_from_wave_and_vessel(new_glob_wave_heading, new_azimuth, convention = "starspec")

    assert( local_new%(2*np.pi) == local%(2*np.pi) )




