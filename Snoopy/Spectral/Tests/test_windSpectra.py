from Snoopy import Spectral
import numpy 
from matplotlib import pyplot 
import pandas 
import os

curDir = os.path.abspath(os.path.dirname(__file__))

def assertSpectral(dataSpectral, dataOther, eps=1.e-5):
    keepInds = ((dataSpectral > 1.e-8) * (dataOther > 1.e-8))
    relativeDiffs = (dataSpectral[keepInds] - dataOther[keepInds])/ dataSpectral[keepInds]
    assert(numpy.max(numpy.abs(relativeDiffs)) < eps)
    
def checkWithPublication(publicationFile, spectrumFromSpectral, epsDensity=1.e-5):
    # check spectral density with publication    
    spectrumPublication = numpy.loadtxt(publicationFile)
    wsPublication = spectrumPublication[:, 0]*2.*numpy.pi #in Hz to rad/s
    swsPublication = spectrumPublication[:, 1]/2./numpy.pi #in m2/Hz to m2/(rad/s)
    ampPublication = numpy.sqrt(2. * swsPublication * numpy.median(wsPublication[1:] - wsPublication[:-1]))
    swsSpectral = spectrumFromSpectral.compute(wsPublication)
    assertSpectral(swsSpectral, swsPublication, epsDensity)
    
def checkWithAr8(ar8File, spectrumFromSpectral, epsDensity=1.e-5):
    spectrumAr8 = numpy.loadtxt(ar8File)
    wsAr8 = spectrumAr8[:, 0]*2.*numpy.pi #in Hz to rad/s
    swsAr8 = spectrumAr8[:, 1]/2./numpy.pi #in m2/Hz to m2/(rad/s)
    ampAr8 = spectrumAr8[:, 2] #in m
    swsSpectral = spectrumFromSpectral.compute(wsAr8)
    assertSpectral(swsSpectral, swsAr8, epsDensity)
    
def test_API() :
    # define API spectrum from Spectral
    V10 = 5.
    fp = 0.0025 * V10
    apiSpectral = Spectral.API(V10, fp, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/APIPublication.txt"), 
                         apiSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/APIAr8Data.txt"), 
                 apiSpectral)

def test_NPD() :
    # define NPD spectrum from Spectral
    V10 = 10.
    npdSpectral = Spectral.NPD(V10, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/NPDPublication.txt"), 
                         npdSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/NPDAr8Data.txt"), 
                 npdSpectral)

def test_Kareem() :
    # define Kareem spectrum from Spectral
    V10 = 20.
    k = 0.002
    z = 10.
    kareemSpectral = Spectral.Kareem(V10, k, z, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/KareemPublication.txt"), 
                         kareemSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/KareemAr8Data.txt"), 
                 kareemSpectral)

def test_ModifiedHarris() :
    # define ModifiedHarris spectrum from Spectral
    V10 = 5.
    k = 0.002
    L = 1800
    modifiedHarrisSpectral = Spectral.ModifiedHarris(V10, k, L, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/WillsPublication.txt"), 
                         modifiedHarrisSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/WillsAr8Data.txt"), 
                 modifiedHarrisSpectral)

def test_Hino() :
    # define Hino spectrum from Spectral
    V10 = 5.
    k = 0.002
    z = 10.
    shearVel = k**(0.5)*V10
    vwz = V10 + 2.5 * shearVel * numpy.log(z/10.)
    alpha = 0.16
    hinoSpectral = Spectral.Hino(V10, k, z, vwz, alpha, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/HinoPublication.txt"), 
                         hinoSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/HinoAr8Data.txt"), 
                 hinoSpectral, 1.e-4)

def test_OchiShin() :
    # define OchiShin spectrum from Spectral
    V10 = 10.
    k = 0.002
    z = 10.
    ochiShinSpectral = Spectral.OchiShin(V10, k, z, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/OchiShinPublication.txt"), 
                         ochiShinSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/OchiShinAr8Data.txt"), 
                 ochiShinSpectral)

def test_Kaimal() :
    # define Kaimal spectrum from Spectral
    V10 = 10.
    k = 0.0044
    z = 10.
    kaimalSpectral = Spectral.Kaimal(V10, k, z, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/KaimalPublication.txt"), 
                         kaimalSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/KaimalAr8Data.txt"), 
                 kaimalSpectral)

def test_Queffeulou() :
    # define Queffeulou spectrum from Spectral
    V10 = 10.
    k = 0.0044
    z = 10.
    Ri = 0.1
    queffeulouSpectral = Spectral.Queffeulou(V10, k, z, Ri, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/QueffeulouPublication.txt"), 
                         queffeulouSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/QueffeulouAr8Data.txt"), 
                 queffeulouSpectral)

def test_Harris() :
    # define Harris spectrum from Spectral
    V10 = 30.
    k = 0.002
    L = 1800.
    harrisSpectral = Spectral.Harris(V10, k, L, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/HarrisPublication.txt"), 
                         harrisSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/HarrisAr8Data.txt"), 
                 harrisSpectral)

def test_Davenport() :
    # define Davenport spectrum from Spectral
    V10 = 30.
    k = 0.0044
    L = 1200.
    davenportSpectral = Spectral.Davenport(V10, k, L, 0.)
    # check spectral density with analytical
    checkWithPublication(os.path.join(curDir, "test_data/DavenportPublication.txt"), 
                         davenportSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/DavenportAr8Data.txt"), 
                 davenportSpectral)

