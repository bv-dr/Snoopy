from Snoopy import Spectral
import numpy 
from matplotlib import pyplot 
import pandas 
import os

from Snoopy import Spectral
import numpy 
from matplotlib import pyplot 
import pandas 

curDir = os.path.abspath(os.path.dirname(__file__))

def assertSpectral(dataSpectral, dataOther, eps=1.e-5):
    keepInds = ((dataSpectral > 1.e-8) * (dataOther > 1.e-8))
    relativeDiffs = (dataSpectral[keepInds] - dataOther[keepInds])/ dataSpectral[keepInds]
    assert(numpy.max(numpy.abs(relativeDiffs)) < eps)
    
def checkWithPublication(publicationFile, spectrumFromSpectral, 
                         epsDensity=1.e-5):
    # check spectral density with publication    
    spectrumPublication = numpy.loadtxt(publicationFile)
    wsPublication = spectrumPublication[:, 0] #in rad/s
    swsPublication = spectrumPublication[:, 1] #in m2/(rad/s)
    ampPublication = numpy.sqrt(2. * swsPublication * numpy.median(wsPublication[1:] - wsPublication[:-1]))
    swsSpectral = spectrumFromSpectral.compute(wsPublication)
    assertSpectral(swsSpectral, swsPublication, epsDensity)
    
def checkWithAr8(ar8File, spectrumFromSpectral,
                 epsDensity=1.e-5):
    spectrumAr8 = numpy.loadtxt(ar8File)
    wsAr8 = spectrumAr8[:, 0]*2.*numpy.pi #in Hz to rad/s
    swsAr8 = spectrumAr8[:, 1]/2./numpy.pi #in m2/Hz to m2/(rad/s)
    ampAr8 = spectrumAr8[:, 2] #in m
    swsSpectral = spectrumFromSpectral.compute(wsAr8)
    assertSpectral(swsSpectral, swsAr8, epsDensity)
    
def test_Gauss() :
    # define Gauss spectrum from Spectral
    hs = 3.
    tp = 10.
    sigma = 0.03 * 2. * numpy.pi
    gaussSpectral = Spectral.Gauss(hs, tp, sigma)
    # check spectral density with publication
    checkWithPublication(os.path.join(curDir, "test_data/GaussPublication.txt"), 
                         gaussSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/GaussAr8Data.txt"), 
                 gaussSpectral, 2.e-4)

def test_Jonswap():
    # define API spectrum from Spectral
    hs = 6.
    tp = 12.
    gamma = 3.3
    jonswapSpectral = Spectral.Jonswap(hs, tp, gamma)
    # check spectral density with publication
    checkWithPublication(os.path.join(curDir, "test_data/JonswapPublication.txt"), 
                         jonswapSpectral, 2.e-3)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/JonswapAr8Data.txt"), 
                 jonswapSpectral, 2.e-3)

def test_OchiHubble():
    # define OchiHubble spectrum from Spectral
    hs1 = 8.
    tp1 = 13.
    lambda1 = 6.
    hs2 = 5.
    tp2 = 7.
    lambda2 = 0.875
    ochiHubbleSpectral = Spectral.OchiHubble(hs1, tp1, lambda1, hs2, tp2, lambda2)
    # check spectral density with publication
    checkWithPublication(os.path.join(curDir, "test_data/OchiHubblePublication.txt"), 
                         ochiHubbleSpectral, 2.e-4)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/OchiHubbleAr8Data.txt"), 
                 ochiHubbleSpectral, 2.e-4)

def test_PiersonMokowitz():
    # define PiersonMoskowitz spectrum from Spectral
    hs = 5.
    tz = 12.
    tp = Spectral.PiersonMoskowitz.tz2tp(tz)
    piersonMoskowitzSpectral = Spectral.PiersonMoskowitz(hs, tp)
    # check spectral density with publication
    checkWithPublication(os.path.join(curDir, "test_data/PiersonMoskowitzPublication.txt"), 
                         piersonMoskowitzSpectral)
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir,"test_data/PiersonMoskowitzAr8Data.txt"), 
                 piersonMoskowitzSpectral, 2.e-4)

def test_SimpleOchiHubble():
    pass

def test_SimpleTorsethaugen():
    # define SimpleTorsethaugen spectrum from Spectral
    hs = 5.
    tz = 15.
    simpleTorsethaugenSpectral = Spectral.SimpleTorsethaugen(hs, tz)
    # check spectral density with publication #FIXME TODO
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir, "test_data/SimpleTorsethaugenAr8Data.txt"), 
                 simpleTorsethaugenSpectral, 1.e-2)

def test_Torsethaugen():
    # define Torsethaugen spectrum from Spectral
    hs = 5.
    tz = 15.
    torsethaugenSpectral = Spectral.Torsethaugen(hs, tz)
    # check spectral density with publication #FIXME TODO
    # check spectral density with ar8
    checkWithAr8(os.path.join(curDir,"test_data/TorsethaugenAr81Data.txt"), 
                 torsethaugenSpectral, 2.e-4)

def test_Wallop():
    pass

def test_WhiteNoise():
    pass
