from Snoopy import Spectral as sp
import numpy as np
from matplotlib import pyplot as plt
from math import isclose

angles = np.linspace(-np.pi,np.pi,500)

def test_spreading(sigDeg = 20, plot = False):

    sig = np.radians(sigDeg)

    n = sp.Cosn.std_to_coef(sig)
    s = sp.Cos2s.std_to_coef(sig)

    cosn = sp.Cosn(n , 0.)
    cos2s = sp.Cos2s(s , 0.)
    wnormal = sp.Wnormal(sig , 0.)

    assert ( isclose( cosn.getStd() , sig ) )
    assert ( isclose( cos2s.getStd() , sig  ))
    assert ( isclose( wnormal.getStd() , sig  ))

    resCosn = cosn.compute(angles)
    resCos2s = cos2s.compute(angles)
    resWnormal = wnormal.compute(angles)

    if plot :
        fig, ax = plt.subplots()
        ax.plot( np.rad2deg(angles), resWnormal , label = "Wrapped normal (sig={:.1f})".format(sigDeg) )
        ax.plot( np.rad2deg(angles),resCosn , label = "Cos n (n={:.1f})".format(n) )
        ax.plot( np.rad2deg(angles), resCos2s , label = "Cos 2s (s={:.1f})".format(s) )

        ax.legend(loc = 3)
        ax.set_xlabel("Angle (degree)")

    eps = 0.01
    assert (  1-eps < np.sum( resCosn ) * (angles[1]-angles[0]) < 1+eps )
    assert (  1-eps < np.sum( resCos2s ) * (angles[1]-angles[0]) < 1+eps )
    assert (  1-eps < np.sum( resWnormal ) * (angles[1]-angles[0]) < 1+eps )

    sigNumCosn =    (np.sum( resCosn * angles**2 ) * (angles[1]-angles[0]))**0.5
    sigNumCos2s =   (np.sum( resCos2s * angles**2) * (angles[1]-angles[0]))**0.5
    sigNumWnormal = (np.sum( resWnormal * angles**2 ) * (angles[1]-angles[0]))**0.5

    print ("Numerical sigma / sigma :" , sigNumCosn / sig)
    print ("Numerical sigma / sigma :" , sigNumCos2s / sig )
    print ("Numerical sigma / sigma :" , sigNumWnormal / sig )

    eps = 0.05
    assert (  1-eps < sigNumCosn / sig < 1+eps )
    assert (  1-eps < sigNumCos2s / sig < 1+eps )
    assert (  1-eps < sigNumWnormal / sig < 1+eps )


def test_spreading_integration(display = False):

    mean_direction = 130
    spreading_value = np.deg2rad(30.0)
    ss = sp.SeaState.Jonswap( heading = np.deg2rad(mean_direction), spreading_type = sp.SpreadingType.Wnormal, spreading_value = spreading_value)

    ss2D = sp.SeaState2D(ss)

    assert( np.isclose( mean_direction , np.rad2deg(ss.integrate_mean_direction()) ))
    spec = ss.getSpectrum(0)
    spread_integ = np.rad2deg(ss.integrate_spreading(wmin = 0.2, wmax = 3.0))
    spread_integ2 = np.rad2deg(ss2D.integrate_spreading())
    spread_analytic = spec.spreading.getSwanDeg()
    print (spread_integ , spread_integ2, spread_analytic )
    assert(  np.isclose ( spread_integ , spread_analytic , rtol = 0.05 ))

    # spreading should not depend on tp
    nList = np.arange(1, 12, 1)
    spread = [sp.SeaState.Jonswap( hs = 1.0, tp  = 6., gamma = 1.0, heading = np.deg2rad(10.), spreading_type = sp.SpreadingType.Cosn, spreading_value = n).integrate_spreading(variant = "ww3", method = "simps") for n in nList ]
    spread2 = [sp.SeaState.Jonswap( hs = 1.0, tp  = 120., gamma = 1.5, heading = np.deg2rad(180.), spreading_type = sp.SpreadingType.Cosn, spreading_value = n).integrate_spreading(variant = "ww3", method = "simps") for n in nList ]
    spread3 = [sp.SeaState.Jonswap( hs = 1.0, tp  = 6., gamma = 1.0, heading = np.deg2rad(0.), spreading_type = sp.SpreadingType.Cosn, spreading_value = n).integrate_spreading(variant = "era5", method = "simps") for n in nList ]
    spread4 = [sp.SeaState.Jonswap( hs = 1.0, tp  = 120., gamma = 1.5, heading = np.deg2rad(180.), spreading_type = sp.SpreadingType.Cosn, spreading_value = n).integrate_spreading(variant = "era5", method = "simps") for n in nList ]

    sp.SeaState.Jonswap( hs = 1.0, tp  = 20., gamma = 1.5, heading = np.deg2rad(0.), spreading_type = sp.SpreadingType.Cosn, spreading_value = 3).integrate_spreading(variant = "era5", method = "simps")

    if display :
        fig, ax = plt.subplots()
        ax.plot( spread, nList , label = "1")
        ax.plot( spread2, nList , label = "2")
        ax.plot( spread3, nList , label = "3")
        ax.plot( spread4, nList , label = "4")
        ax.legend()

    assert(np.isclose( spread, spread2, rtol = 0.01 ).all())
    assert(np.isclose( spread, spread3, rtol = 0.01 ).all())
    assert(np.isclose( spread, spread4, rtol = 0.01 ).all())

def test_spreading_int1()    :

    from scipy.integrate import quad
    for spread in [ sp.Wnormal( np.deg2rad(180.) , np.deg2rad(20.), k_max = 2) ,
                    sp.Cos2s( np.deg2rad(180.), 6 ),
                    sp.Cosn( np.deg2rad(180.), 8 )
                  ] :
        one = quad( spread.compute , 0. , np.pi*2 )[0]
        print (one)
        assert( np.isclose(one, 1., rtol = 0.001) )



def test_spreading_regression(display = False) :
    nList = np.arange(1,15,1)
    spread = [sp.SeaState.Jonswap( hs = 1.0, tp  = 10, gamma = 1.0, heading = np.deg2rad(180.), spreading_type = sp.SpreadingType.Cosn, spreading_value = n).integrate_spreading(variant = "ww3", method = "simps") for n in nList ]
    regression = sp.Cosn.n_to_spreading( nList )

    if display :
        fig, ax = plt.subplots()
        ax.plot( spread, nList , label = "exact")
        ax.plot( regression, nList, label = "regression" )
        ax.legend()

    assert(np.isclose(spread, regression, rtol = 0.05).all())


if __name__ == "__main__" :

    test_spreading( sigDeg = 30. )
    test_spreading_integration(True)
    test_spreading_int1()
    test_spreading_regression(True)




