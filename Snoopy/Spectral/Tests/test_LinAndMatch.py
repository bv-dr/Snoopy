import numpy as np
from Snoopy import Spectral as sp
from Snoopy import logger

logger.setLevel(10)

def test_lm():
    #---------- Data
    hs = 10.0
    tp = 11.3
    gamma = 10.0
    dw = 0.005
    wmax = 3.5

    m = 5.35e10
    B = 60
    L = 250
    Cd = 0.1
    rho = 1000.
    bquad = .5*rho*Cd*B**4*L
    blin = 1.4e9
    Tr = 11.3
    k = m*(2*np.pi/Tr)**2
    bcr = 2*(k*m)**0.5
    mx = 5.e8

    #Create sea-state
    spec = sp.Jonswap(hs = hs , tp = tp , gamma = gamma , heading = np.deg2rad(90.) )
    ss = sp.SeaState( spec )

    #Create constant excitation RAO
    mx_rao = mx * sp.getUnitRao( dw = dw, wmin = 0.1 , wmax = wmax , heading = np.arange(0,2*np.pi , np.deg2rad(30)) )

    #------- Compute in frequency domain
    blinList = np.linspace( 0.9*blin , 50.*blin , 50 )
    metadata = sp.Rao.getMetaData(mx_rao)
    metadata.pop("modesCoefficients")
    rollList = [sp.Rao( w = mx_rao.freq , b = mx_rao.head ,
               cvalue = ((mx_rao.cvalues[:,:,0]) / (k + beq*mx_rao.freq*1j - m*mx_rao.freq**2 ))[:,:,np.newaxis] ,
               modesCoefficients = [beq],
               **metadata )
               for beq in blinList ]
    rollCollection = sp.Rao(  rollList   )


    #Linearised roll damping
    from Snoopy.Spectral.linearize_and_match import  LinearizeAndMatch
    n = 4
    linMatch = LinearizeAndMatch( ss , rollCollection, 0.0 , bquad , loadRao = mx_rao , n = n)
    alpha , gamma = linMatch.getAlphaGamma()

    #Compute moment with linearized and match :
    mu_linAndMatch, vmu_linAndMatch = linMatch.computeLinAndMatchMoments(  )

    adimMom = sp.linearize_and_match.LinearizeAndMatch.getCrossMoment( linMatch._moment, linMatch._vmoment )

    logger.debug ( "Adim moments : \n" + str(adimMom) )

    #For now just check that results are not too far from the published ones
    r2d = 180/np.pi
    assert( np.isclose( adimMom.loc[2,0]*r2d**2 , 45.96, rtol = 0.05) )
    assert( np.isclose( adimMom.loc[4,0]        , 2.20 , rtol = 0.05) )
    assert( np.isclose( adimMom.loc[6,0]        , 6.12 , rtol = 0.05) )

    assert( np.isclose( adimMom.loc[0,2]*r2d**2 , 14.65, rtol = 0.05) )
    assert( np.isclose( adimMom.loc[0,4]        , 2.14 , rtol = 0.05) )
    assert( np.isclose( adimMom.loc[0,6]        , 6.16 , rtol = 0.05) )


if __name__ == "__main__" :
    test_lm()
