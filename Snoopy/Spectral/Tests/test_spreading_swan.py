
import numpy as np
from Snoopy import Spectral as sp
from Snoopy.Spectral.spreading import swandeg_to_spreading


def integral2(n):
    r""" integrage:
    \int_{-\pi/2}^{\pi/2} (2\sin(x/2))^2 \cos^n(x) dx = 2\int \cos^n(x) dx - 2\int\cos^{n+1}(x) dx = 2J_n - 2J_{n+1}
    """
    def int_cosn(n):
        r"""
        J_n(a,b) \equiv \int_a^b \cos^n(x) dx
        J_n(a, b) = (\cos^{n-1}(x) \sin(x))/n |_a^b +(n-1)/n J_{n-2}(a,b)
        J_n(-\pi/2,\pi/2) = (n-1)/n J_{n-2}(-\pi/2, \pi/2)
        J_0(-\pi/2, \pi/2) = \pi
        J_1(-\pi/2, \pi/2) = 2
        J_2(-\pi/2, \pi/2) = 1/2 \pi
        J_3(-\pi/2, \pi/2) = 2/3 *2
        J_4(-\pi/2, \pi/2) = 3/8 \pi
        J_{n}(-\pi/2, \pi/2) = (n-1)/n J_{n-2}(-\pi/2, \pi/2)
        """
        if n == 0:
            return np.pi
        elif n == 1:
            return 2
        else:
            return (n-1.)/n *int_cosn(n-2)
    return 2*(int_cosn(n) - int_cosn(n+1))



def spreading_to_swandeg_wnormal_analytical( sigma ) :
    """Analytical formula to convert from Wrapped normal to swan spreading
    """
    def integral1(a, sigma):
        r""" integrage:
        1/sigma / sqrt(2pi)\int_0^{2\pi} (2\sin(x/2))^2 exp(-(x+a)^2/(2\sigma^2)) dx
        """
        from scipy.special import erf
        sqrtb = sigma * 2**0.5
        b = sigma * sigma * 2.

        def int_(x):
            r"""
            \int (2\sin(x/2))^2 exp(-(x-a)^2/b) dx
            """
            erf1 = np.exp(1j*a) * erf((a+b*0.5j + x)/sqrtb)
            return erf((a+x)/sqrtb) - np.exp(-b*0.25) * np.real(erf1)

        return sqrtb * (int_(2*np.pi) - int_(0)) / sigma / 2**0.5

    res = 0.0
    for k in np.linspace(-1, 1, 3):
        res += integral1( 2*k*np.pi, sigma )
    return res**0.5 * 180 / np.pi


def test_swan_backAndForth():
    for spreadModel, val in [  ( sp.Wnormal, 0.1  ) , (sp.Cosn,4 ) , (sp.Cos2s, 8 )  ] :
        spread = spreadModel( val, 10 )
        swandeg = spread.getSwanDeg()
        re_val = swandeg_to_spreading(swandeg, spreadModel)
        print ( val, swandeg, re_val , spread.getSwanDeg())
        assert( np.isclose( val, re_val ) )

def test_swan_analytical():
    for val in np.linspace( 0.001, 0.4 , 10 ) :
        analytical = spreading_to_swandeg_wnormal_analytical( val )
        numerical = sp.Wnormal(val, 0).getSwanDeg()
        print ( np.rad2deg(val), analytical , numerical)
        assert( np.isclose( analytical , numerical ) )

def print_convertion_table()   :

    import pandas as pd
    df = pd.DataFrame( index = pd.Index( np.linspace(1.0, 60, 60), name = "Wrapped normal") , columns = [ "COSN", "COS2S", "SWAN" ] )

    for irow, row in df.iterrows() :
        df.loc[irow , "COSN"] = sp.Cosn.std_to_coef(np.deg2rad(irow))
        df.loc[irow , "COS2S"] = sp.Cos2s.std_to_coef(np.deg2rad(irow))
        df.loc[irow , "SWAN"] = sp.Wnormal( np.deg2rad(irow), 0 ).getSwanDeg()
        
    #df.to_csv("spreading_conversion.csv")
    print (df)


if __name__ == "__main__" :
    test_swan_backAndForth()
    test_swan_analytical()
    print_convertion_table()






