import numpy as np
from Snoopy import Spectral as sp

def test_mii() :
    """Non-regression test for MII
    """

    rao_accy = sp.Rao( f"{sp.TEST_DATA:}/rao/ACCY_POINT_E.rao" ).getRaoForSpectral()
    rao_aroll = sp.Rao( f"{sp.TEST_DATA:}/rao/AROLL_POINT_E.rao" ).getRaoForSpectral() * np.pi / 180.
    rao_aheave = sp.Rao( f"{sp.TEST_DATA:}/rao/AHEAVE_POINT_E.rao" ).getRaoForSpectral()
    rao_roll = sp.Rao( f"{sp.TEST_DATA:}/rao/GROLL_POINT_COG.rao" ).getRaoForSpectral() * np.pi / 180.


    heading = np.deg2rad(105)
    ss = sp.SeaState.Jonswap( hs = 5.0 , tp = 12.4, gamma = 1.0 , heading = heading )

    rollSpec = sp.StochasticDamping(ss, raos = rao_roll, bLin = 9.53E+06, bQuad = 1.22E+08)

    rao_accy_ = rao_accy.getRaoAtModeCoefficients([rollSpec.beq])
    rao_aroll_ = rao_aroll.getRaoAtModeCoefficients([rollSpec.beq])
    rao_aheave_ = rao_aheave.getRaoAtModeCoefficients([rollSpec.beq])

    mii = sp.spectralStats.mii( ss, rao_accy=rao_accy_, rao_aheave=rao_aheave_, rao_aroll=rao_aroll_ , duration = 60)


    # Regression test, value checked with DA R&P (using Snoopy for everything expect mii itself)
    assert( np.isclose( mii, 5.281477) )


if __name__ == "__main__" :
    test_mii()


