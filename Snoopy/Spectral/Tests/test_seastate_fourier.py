import os
import numpy as np
import xarray
import pickle
from Snoopy import Spectral as sp
from matplotlib import pyplot as plt


def test_seastate_fourier(display = False):
    """Regression test
    """

    ds = xarray.open_dataset( sp.TEST_DATA + "/46006/46006w2016.nc")

    ds_param = xarray.open_dataset( sp.TEST_DATA + "/46006/46006h2016.nc")
    hs_ref = ds_param.wave_height.to_dataframe().reset_index().set_index("time").loc[: , "wave_height"].dropna()

    t_max = hs_ref.idxmax().to_datetime64()
    i_max = np.abs((ds.time - t_max).values).argmin() - 1  # 2D and parameter value does not match exactly value -1 seems to match better...

    w = ds.frequency.values * 2*np.pi

    a0 = ds.spectral_wave_density[i_max,:,0,0].values / (2*np.pi)
    alpha1 = np.deg2rad( ds["mean_wave_dir"][i_max,:,0,0].values )
    alpha2 = np.deg2rad( ds["principal_wave_dir"][i_max,:,0,0].values )
    r1 = ds["wave_spectrum_r1"][i_max,:,0,0].values # / (2.*np.pi)
    r2 = ds["wave_spectrum_r2"][i_max,:,0,0].values # / (2.*np.pi)

    ss = sp.SeaState2D_Fourier.From_NDBC(w, a0, alpha1, alpha2, r1, r2)

    t_range = np.linspace(0. , 2*np.pi , 1000)
    hs_spread  = 4 * (np.sum( ss.computeSpreading( t_range ) * (t_range[1]-t_range[0]) ))**0.5

    if display :
        ss.plot2D()
        ss.plot()
        ss.plotHeading()

    assert( np.isclose( ss.integrate_hs() , hs_ref.loc[t_max], rtol = 0.01))
    assert( np.isclose( ss.integrate_mean_direction_simps() , 5.0248, rtol = 1e-3))
    assert( np.isclose( hs_spread,   ss.integrate_hs() , rtol = 0.005))

    # Write and read back
    ftmp = sp.TEST_DATA + "/test.pkl"
    with open( ftmp , "wb") as f :
        pickle.dump( ss , f)
    with open(ftmp , "rb") as f :
        ss_re = pickle.load( f  )
    os.remove(ftmp)

    assert( np.isclose( ss.integrate_hs() , ss_re.integrate_hs()  ))

if __name__ == "__main__" :

    test_seastate_fourier(False)


