import os
import numpy as np
from Snoopy import Spectral as sp
from Snoopy import logger

logger.setLevel(10)

f = f"{sp.TEST_DATA:}/rao/roll_multi.rao"

wdir = f"{sp.TEST_DATA:}/test_write"
if not os.path.exists(wdir) :
    os.mkdir(wdir)

def test_readWrite() :

    rao = sp.Rao( f )
    rao.write(f"{wdir:}/rao_multi_rewrite.rao")
    rao_re = sp.Rao( f"{wdir:}/rao_multi_rewrite.rao" )
    assert(True)


def test_toDataArray():
    rao = sp.Rao( f )
    data = rao.toDataArray()
    newRao = sp.Rao.FromDataArray( data )
    assert(True)


def test_interpolate():
    rao = sp.Rao( f )
    assert( len(rao.getModes()) == len(rao.getModeCoefficients()))


    rao_i = rao.getRaoAtModeCoefficients( [2.0e8] )
    assert( len(rao_i.getModes()) == len(rao_i.getModeCoefficients()))


    # Interpolate in data that only has 1 values. Should take this value and display a warning
    rao_ii = rao_i.getRaoAtModeCoefficients( [3.0e8] )
    assert( np.isclose( rao_ii.getModules() , rao_i.getModules() ).all() )



if __name__ == "__main__" :
    test_readWrite()
    test_toDataArray()
    test_interpolate()

    rao = sp.Rao( f )

