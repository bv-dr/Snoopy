import numpy as np
from matplotlib import pyplot as plt
from Snoopy import TimeDomain as td
from Snoopy import Spectral as sp


def test_mean(display = False):
    """Test calculation of mean response on a sea-state
    """
    #Read test QTf
    qtf = sp.Qtf(  f"{sp.TEST_DATA:}/FPSO_QTF_FX.qtf" )

    #Define seastate
    spec = sp.Jonswap(hs = 10, tp = 10 , gamma = 1.0 , heading = np.deg2rad(180.))
    ss = sp.SeaState(spec)
    wif = sp.Wif(ss, nbSeed = 200)

    #Compute mean
    rSpec2 = sp.ResponseSpectrum2nd( ss, qtf )
    mean_spec = rSpec2.getMean()

    #Compute mean from time domain reconstruction
    rec2 = td.ReconstructionQtfLocal(wif, qtf, dwMax = 0.001)
    mean_time = rec2( np.linspace(0., 100.0 , 200) ).mean()

    #Check consistency
    print ("Mean time :",  mean_time)
    print ("Mean spec :",  mean_spec)
    assert( np.isclose( mean_time , mean_spec , rtol=0.01 ) )
    
    #Compare Full QTF and Newman
    if display : 
        fig, ax = plt.subplots()
        rSpec2.plot(ax=ax, label = "Full QTF")
        rSpec2.getNewmanSe().plot(ax=ax, label = "Newman")
        ax.legend()
        plt.show()


if __name__ == "__main__" :

    test_mean(display = True)
    
    
    
    






