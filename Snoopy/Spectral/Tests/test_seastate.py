import os
import numpy as np
from Snoopy import Spectral as sp
import _Spectral

wdir = f"{sp.TEST_DATA:}/test_write"

def test_SeaState_Spread(plot = False):

    hs_1 = 4.0
    tp_1 = 8.0
    hs_2 = 2.0
    tp_2 = 14.0

    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = 1.0 , heading = np.deg2rad(90) , spreading_type = sp.SpreadingType.Cosn , spreading_value = 4. )
    swell = sp.Jonswap( hs = hs_2 , tp = tp_2 , gamma = 3.3 , heading = np.deg2rad(180)  , spreading_type = sp.SpreadingType.Cosn , spreading_value = 16. )
    ss = sp.SeaState( [windSea , swell] )

    print (ss.hspecString())

    print ("SeaState coefficients are :" , ss.getCoefsDict())


    hs_analytical = (hs_1**2+hs_2**2)**0.5
    hs_num = ss.integrate_hs(method = "quad")
    hs_num_simps = ss.integrate_hs(method = "simpson" )

    print ("HS analytical = {}".format(hs_analytical))
    print ("HS num        = {}".format(hs_num))
    print ("HS num (simps = {}".format(hs_num_simps))

    print ("Wind-sea steepness : {}".format(windSea.getSt()) )
    print ("Swell steepness : {}".format(swell.getSt()) )

    print ("Check sea-state : {}".format(ss.check() ))

    assert(   0.99 < hs_num / hs_analytical <  1.01 )
    assert(   0.99 < hs_num_simps / hs_analytical <  1.01 )

    wif = sp.Wif( ss , spreadNbheading = 72)
    wif.write(f"{wdir:}/tmp.wif")

    if plot :
        ax = ss.plot2D(polar = False, colorbar = True, colorbar_label = "Spectral density")
        ax.get_figure().tight_layout()
        ax = ss.plot()

    #Test 2D tabulated
    ss2D = sp.SeaState2D( ss , wmin = 0.2, wmax= 2.5, nbFreq = 300, nbHead = 720 )

    hs_num_tab = ss2D.integrate_hs( w = np.arange( 0.1, 2.5 , 0.005 ) )
    print ("HS num tab    = {}".format(hs_num_tab))

    assert(   0.98 < hs_num_tab / hs_analytical <  1.02 )
    assert(  ss2D.compute( ss2D.w[20], np.pi ) == ss.compute( ss2D.w[20], np.pi ) )

    print ("Compute spreading", ss2D.computeSpreading( np.deg2rad([180,0]) ) ,  ss.computeSpreading( np.deg2rad([180,0]) )  )
    assert(  np.isclose(ss2D.computeSpreading( np.deg2rad([180]) ) ,  ss.computeSpreading( np.deg2rad([180]) ) , rtol = 1e-2 ))


    if plot :
        ax = ss2D.plot( label = "Tabulated" )
        ss.plot(ax=ax, label = "Original")
        ax.legend()
    print( "Test 2D spectrum ok")


def test_pickle():

    import pickle
    windSea = sp.Jonswap( hs = 4.0 , tp = 8.0 , gamma = 1.0 , heading = np.deg2rad(90) , spreading_type = sp.SpreadingType.Cosn , spreading_value = 4. )
    swell = sp.Jonswap( hs = 2.0 , tp = 14 , gamma = 3.3 , heading = np.deg2rad(180)  , spreading_type = sp.SpreadingType.Cosn , spreading_value = 16. )

    ss = sp.SeaState( [windSea , swell] )

    #Pickle without python layer
    windSea_cpp = _Spectral.PiersonMoskowitz( hs = 4.0 , tp = 8.0 , heading = np.deg2rad(90) , spreading_type = sp.SpreadingType.Cosn , spreading_value = 4. )
    with open( f"{wdir:}/windSea_cpp.pkl" , 'wb' ) as f : pickle.dump( windSea_cpp , f)
    with open( f"{wdir:}/windSea_cpp.pkl" , 'rb' ) as f : reloaded_windSea_cpp = pickle.load(f)

    #Pickle with python layer
    with open( f"{wdir:}/windSea.pkl" , 'wb' ) as f : pickle.dump( windSea , f)
    with open( f"{wdir:}/windSea.pkl" , 'rb' ) as f : reloaded_windSea = pickle.load(f)
    assert( reloaded_windSea.hs == windSea.hs)
    assert( reloaded_windSea.spreading.value == windSea.spreading.value)

    with open( f"{wdir:}/ss.pkl" , 'wb' ) as f : pickle.dump( ss , f)
    with open( f"{wdir:}/ss.pkl" , 'rb' ) as f : reloaded_ss = pickle.load(f)
    assert( ss.getSpectrum(0).hs == reloaded_ss.getSpectrum(0).hs)

    tabSpec = sp.WaveTabulatedSpectrum( w = [0,1,2], sw = [4,5,6] )
    with open( f"{wdir:}/tabSpec.pkl" , 'wb' ) as f : pickle.dump( tabSpec , f)
    with open( f"{wdir:}/tabSpec.pkl" , 'rb' ) as f : reloaded_tabSpec = pickle.load(f)
    assert( reloaded_tabSpec.compute(1.5) == tabSpec.compute(1.5))

    tabSpecSS = sp.SeaState( [tabSpec] )
    with open( f"{wdir:}/tabSpecSS.pkl" , 'wb' ) as f : pickle.dump( tabSpecSS , f)
    with open( f"{wdir:}/tabSpecSS.pkl" , 'rb' ) as f : reloaded_tabSpecSS = pickle.load(f)
    assert( reloaded_tabSpecSS.compute(1.5) == tabSpecSS.compute(1.5))

    spec2D = sp.SeaState2D( [1,4], [0,2*np.pi] , [[1,4],[20,30]])
    with open( f"{wdir:}/spec2D.pkl" , 'wb' ) as f : pickle.dump( spec2D , f)
    with open( f"{wdir:}/spec2D.pkl" , 'rb' ) as f : reloaded_spec2D = pickle.load(f)
    assert( reloaded_spec2D.integrate_hs() == spec2D.integrate_hs())

    #Clean test files
    os.remove(f"{wdir:}/windSea.pkl")
    os.remove(f"{wdir:}/ss.pkl")
    os.remove(f"{wdir:}/tabSpec.pkl")
    os.remove(f"{wdir:}/tabSpecSS.pkl")
    os.remove(f"{wdir:}/spec2D.pkl")
    os.remove(f"{wdir:}/windSea_cpp.pkl")


def test_normmalized():
    t1 = sp.SeaState.Jonswap(  1.0 , 10 , 1.0).getNormalized(period = "tp")
    t2 = sp.SeaState.Jonswap(  5.0 , 12 , 1.0).getNormalized(period = "tp")
    ax = t2.plot()
    t1.plot(ax=ax)



def test_energyRange():
    hs_1 = 4.0
    tp_1 = 8.0
    hs_2 = 2.0
    tp_2 = 14.0

    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = 1.0 , heading = np.deg2rad(90) , spreading_type = sp.SpreadingType.Cosn , spreading_value = 4. )
    swell = sp.Jonswap( hs = hs_2 , tp = tp_2 , gamma = 3.3 , heading = np.deg2rad(180)  , spreading_type = sp.SpreadingType.Cosn , spreading_value = 16. )
    ss1 = sp.SeaState( [windSea ] )
    assert (  np.isclose( np.array(ss1.energyRange(0.95)) ,  np.array(windSea.energyRange(0.95)), rtol = 0.001).all() )
    freq = np.arange( 0.2 , 2.0 , 0.01)
    specTabl = sp.WaveTabulatedSpectrum( freq, ss1.compute(freq) )
    assert( np.isclose( specTabl.getTp() , tp_1 , rtol = 0.01 ))

    ss2 = sp.SeaState( [windSea, swell ] )
    print (ss2.energyRange(0.95))

    specTab2 = sp.WaveTabulatedSpectrum( freq, ss2.compute(freq) )
    print (specTab2.energyRange(0.95))




def test_integrate_moment_extrap():

    j = sp.SeaState.Jonswap(1. , 10.0 , 1.0)

    w_trunc = 3.0
    for exponent in (1, 2) :
        analitical = j.integrate_moment_tail(2, w_trunc, exponent = exponent)
        numerical = j.integrate_moment(2, method="simpson", w = np.arange(w_trunc , 50.0 , 0.1), exponent = exponent, extrap = False)
        print (numerical / analitical)
        assert( np.isclose( analitical , numerical , rtol = 0.003) )


def test_integrate_direction():

    heading = np.deg2rad(45)
    ss = sp.SeaState.Jonswap( hs = 1.0 , tp = 10. , gamma = 1.0 , heading = heading , spreading_type = sp.SpreadingType.Cosn , spreading_value = 4. )
    assert(np.isclose( ss.integrate_spreading(variant = "ww3") ,  ss.integrate_spreading(variant = "ww3", method = "simps") , rtol = 1e-3))

    m1 = ss.integrate_mean_direction(method = "simps") / heading
    m2 = ss.integrate_mean_direction()  / heading
    m1_b = ss.integrate_mean_direction_simps()  / heading
    
    dom_dir = ss.integrate_dominant_direction() / heading
    
    assert( np.isclose(dom_dir,1) )
    
    assert( np.isclose(m1,1) )
    assert( np.isclose(m2,1) )
    assert( np.isclose(m1_b,1) )





if __name__ == "__main__" :
    print ("Run")
    print (sp.getCompilerVersion())
    
    test_integrate_direction()
    test_SeaState_Spread()
    test_pickle()
    test_normmalized()
    test_energyRange()
    test_integrate_moment_extrap()
