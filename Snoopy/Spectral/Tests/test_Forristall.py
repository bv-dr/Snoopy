from Snoopy import Spectral as sp
from Snoopy import WaveKinematic as wk
from Snoopy.TimeDomain import upCrossMinMax
from Snoopy.PyplotTools import distPlot
import numpy as np
from matplotlib import pyplot as plt


def test_unidirectional_Forristall(display = False):
    """
    Compare results with Figure 13 from "Wave Crest Distributions: Observations and Second-Order Theory"
    """

    depth = 20
    spec = sp.Jonswap( hs = 10.93, tp = 10, gamma = 3.3 )

    dist = spec.getForristallCrestDistribution(depth = depth)
    distLin = spec.getLinearCrestDistribution()

    r = dist.isf(0.01) / distLin.isf(0.01)
    print (r)
    assert ( np.isclose( r , 1.28, rtol = 0.02 ))  # 1.28, is the ratio value graphically read on Fig 13.

    if display :
        ss = sp.SeaState(spec)
        wif = sp.Wif(ss, depth = depth)

        kinModel = wk.SecondOrderKinematic

        time = np.arange( 0, 50800, 0.5 )
        ts = kinModel(wif).getElevation_SE( time, x = 0 , y = 0 )
        upCross = upCrossMinMax( ts )

        
        fig , ax = plt.subplots()
        distPlot( upCross.Maximum, frozenDist = dist , ax=ax , labelFit="Forristal", label = "Time domain")
        distPlot( upCross.Maximum, frozenDist = distLin, noData = True , ax=ax, labelFit="Linear" )
        ax.legend()


if __name__ == "__main__" :

    test_unidirectional_Forristall( True )
