

import numpy as np
import pandas as pd
from Snoopy import Spectral as sp


if __name__ == "__main__" :

    import seaborn as sns
    
    rao = sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" )
    rao = rao.getRaoForSpectral()
    ss = sp.SeaState.Jonswap( 5. , 12.0 , 1.0 , np.pi / 4. )

    print (sp.msi_from_motion( ss , rao , 60)) # Return % of people (between 0. and 100.) sick in 60 minutes

    """
    Check with respec to orignal publication (Figure 8 McCauley 1976)
    """

    msi_table2 = df = pd.DataFrame( index  = pd.Index( np.arange( 0.1, 0.7 , 0.05) , name ="f" ) ,
                                   columns = pd.Index( np.arange(0.1, 0.6 , 0.05) , name = "Acc"), dtype = float )
    for f, acc in msi_table2.unstack().index :
        msi_table2.loc[ f , acc ] = sp.spectralStats.msi_McCauley( acc , f , 120  ) / 100

    ax = sns.heatmap( msi_table2,
                 annot=True, fmt=".0%", cbar = True ,cmap ="cividis", vmin = 0.0,
                 linewidths = 1.0,
                )

    plt.show()