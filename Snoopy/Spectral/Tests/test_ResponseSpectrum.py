from Snoopy import Spectral as sp
import os
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd


f = f"{sp.TEST_DATA:}/my_10.rao"


def test_spec_unit() :
    """ Check response spectrum with unit rao => wave spectrum
    """
    wmin = 0.1
    wmax = 5.0
    rao = sp.Rao(f)
    rao = sp.getUnitRao( dw = 0.01, wmin = wmin , wmax = wmax , heading = np.arange(0,2*np.pi , np.deg2rad(30)) )
    spec =  sp.Jonswap(1 , 10 , 1.5, 0.0)
    ss = sp.SeaState( spec )

    rSpec = sp.ResponseSpectrum( ss, rao )

    m0_r = rSpec.getM0()
    m0_w =  ss.integrate_moment( 0, wmin =wmin, wmax = wmax )
    assert( np.isclose(m0_r  ,m0_w , rtol = 0.005 ))

    m2_r = rSpec.getM2()
    m2_w =  spec.getMoment(2)
    assert( np.isclose(m2_r  ,m2_w , rtol = 0.02 ))

    tz_r = rSpec.getTz()
    assert( np.isclose(tz_r  ,sp.Jonswap.tp2tz(10,1.5) , rtol = 0.05 ))

    hs_r = rSpec.getRs()
    assert( np.isclose(hs_r  ,1. , rtol = 0.01 ))

    print ("Ok")


def test_conversion():
    """Check M0/M2 <--> Rs/Rtz conversion
    """
    a, b = 5., 10.
    new_a, new_b = sp.RsRtzFromM0M2(*sp.m0m2FromRsRtz(a, b))

    print (a, b)
    print (new_a, new_b)
    assert( np.isclose(a,new_a) )
    assert( np.isclose(b,new_b) )

def test_moment():
    """
    """

    f = f"{sp.TEST_DATA:}/my_10.rao"

    rao = sp.Rao( f )
    rao = rao.getSymmetrized()
    spec =  sp.Jonswap(1 , 10 , 1.5, np.deg2rad(15.) )
    ss = sp.SeaState( spec )
    rSpec = sp.ResponseSpectrum( ss, rao )

    rSpec.getM0()
    rSpec.getM2()

    return


def test_moment_2Dspec(writeSpc = False) :
    """ Compare full and parametric response spectral moments
    """
    f = f"{sp.TEST_DATA:}/my_10.rao"
    rao = sp.Rao( f )

    wmin, wmax, dw = 0.1, 2.0, 0.05
    rao = rao.getRaoAtFrequencies( np.arange( wmin, wmax, dw ) )


    spec =  sp.Jonswap(1 , 10 , 1.5, np.deg2rad(15.),  sp.SpreadingType.Wnormal , np.deg2rad(20) )
    ss = sp.SeaState( spec )

    ss2D = sp.SeaState2D(ss, wmin = wmin, wmax = wmax , nbFreq = 1 + int( 0.5 + (wmax - wmin) / dw))

    #Check StarSpec "interface". StarSpec has to be run manually for now
    if writeSpc :
        rao.write("tmp/rao")
        with open("specTemplate.spc", "r") as f : specString = f.read()
        with open("spec.spc", "w") as f : f.write( specString.format( ss1=ss.hspecString() , ss2=ss2D.hspecString("spec2D.dat"),
                                                   dw = dw , wmin = wmin , wmax = wmax ) )

    """
    #TODO : Enable test once spectral calculation in Snoopy handles spreading
    rSpec = sp.ResponseSpectrum( ss, rao )
    r = rSpec.getM0M2()

    rSpec2D = sp.ResponseSpectrum( ss2D, rao )
    r2D = rSpec2D.getM0M2()

    #Check that moment from parametric spectrum and full spectrum are identical
    assert( np.isclose( r[0] , r2D[0] ) )
    assert( np.isclose( r[1] , r2D[1] ) )
    """


def test_multiMode():
    rao = sp.Rao( [ sp.Rao( f"{sp.TEST_DATA:}/rao/{f:}.rao") for f in ["surge" , "sway", "heave"]  ] )
    rao = rao.getSymmetrized()
    spec =  sp.Jonswap(1 , 10 , 1.5, np.deg2rad(15.),  sp.SpreadingType.Wnormal , np.deg2rad(20) )
    ss = sp.SeaState( spec )
    rSpec = sp.ResponseSpectrum( ss , rao )

    dw = rSpec.freq[1] - rSpec.freq[0]
    m0s = rSpec.getDf().sum()*dw

    assert( np.isclose( m0s.values , rSpec.getM0s() ).all() )


def test_rSpec_seastate2D():

    f, idx = f"{sp.TEST_DATA:}/my_10.rao", 0                # test on moment rao
    f, idx = f"{sp.TEST_DATA:}/stress_nonsym.rao", 1      # test on stress rao

    rao = sp.Rao( f )
    rao = rao.getRaoForSpectral( db_deg = 5.0, wmax = 4.0 )


    ss = sp.SeaState.Jonswap(hs=1, tp = 10 , gamma = 1.0 , heading = np.pi,
                             spreading_type = sp.SpreadingType.Cosn , spreading_value = 3)


    ss2D = sp.SeaState2D( ss , nbHead = 72)
    ss2Dm = ss2D.toMultiModal()
    # ss2D.hspecString(filename=r"test_data\spec2d_fromSS.csv")

    rSpec_param = sp.ResponseSpectrum( ss, rao )
    rSpec2Dm = sp.ResponseSpectrum( ss2Dm, rao )

    print ( "Rs Snoopy SpecParam :", rSpec_param.getRs() )
    print ( "Rs Snoopy SpecMultiModal :", rSpec2Dm.getRs() )
    print ( "Tz Snoopy SpecParam :", rSpec_param.getTz() )
    print ( "Tz Snoopy SpecMultiModal :", rSpec2Dm.getTz() )

    assert( np.isclose( rSpec_param.getRs()  , rSpec2Dm.getRs() , rtol = 0.001  ) )
    assert( np.isclose( rSpec_param.getTz()  , rSpec2Dm.getTz() , rtol = 0.001  ) )

    # read results from StarSpec using parametric spectrum
    df = pd.read_csv(f"{sp.TEST_DATA:}/hspec_reference/calcul_specParam_spec/All.csv", sep = None, engine = 'python')
    rs_StarSpec = df.iloc[idx, 9]
    tz_StarSpec = df.iloc[idx, 10]

    print ( "Rs StarSpec SpecParam :", rs_StarSpec)
    print ( "Tz StarSpec SpecParam :", tz_StarSpec)

    assert( np.isclose( rSpec_param.getRs()  , rs_StarSpec , rtol = 0.001  ) )
    assert( np.isclose( rSpec_param.getTz()  , tz_StarSpec , rtol = 0.001  ) )

    # read results from StarSpec using 2D spectrum
    df = pd.read_csv(f"{sp.TEST_DATA:}/hspec_reference/calcul_spec2D_spec/All.csv", sep = None, engine = 'python')
    rs_StarSpec = df.iloc[idx, 9]
    tz_StarSpec = df.iloc[idx, 10]

    print ( "Rs StarSpec Spectrum3D :", rs_StarSpec)
    print ( "Tz StarSpec Spectrum3D :", tz_StarSpec)

    assert( np.isclose( rSpec2Dm.getRs()  , rs_StarSpec , rtol = 0.001  ) )
    assert( np.isclose( rSpec2Dm.getTz()  , tz_StarSpec , rtol = 0.001  ) )


if __name__ == "__main__" :
    test_spec_unit()
    test_moment()
    test_moment_2Dspec()
    test_conversion()
    test_multiMode()
    test_rSpec_seastate2D()
