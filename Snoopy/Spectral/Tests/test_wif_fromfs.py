import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from Snoopy import WaveKinematic as wk
from Snoopy import Spectral as sp
from Snoopy import TimeDomain as td
import pytest


@pytest.mark.parametrize("heading", [0.0, np.pi] )
def test_wif_fromfs(heading, display = False):
    """Construct a wave elavation snapshot from a given wif, retrieve the wif back using FFT and compares to starting point.    
    """

    wif = sp.Wif.Jonswap(hs = 1.0 , tp = 10.0 , gamma = 1.0 , heading = heading,  wifArgs={"seed" : 15} )

    x = np.arange( 200 , 1800 , 4 )
    t = 500.

    kin = wk.FirstOrderKinematic(wif)
    eta = kin.getElevation( time = np.full( (len(x)) , t) ,
                            x = x, y = np.full( (len(x)) , 0.0)  ,
                            speed = 0.0 )

    fs_snapshot_se = pd.Series(index = x, data = eta)

    wif_back = sp.Wif.FromFS( fs_snapshot_se, b = heading, t = t, depth = -1 )

    kin_back = wk.FirstOrderKinematic(wif_back)
    eta_back = kin_back.getElevation( time = np.full( (len(x)) , t) ,
                            x = x ,
                            y = np.full( (len(x)) , 0.0)  ,
                            speed = 0.0 )


    if display : 
        fig, ax = plt.subplots()
        ax.plot(x, eta, label = "original")
        ax.plot(x , eta_back , label = "reconstructed", marker  = "+", linestyle = "")
        
        rec = td.ReconstructionWif(wif)
        rec_back = td.ReconstructionWif(wif_back)
        
        # Reconstructed time-serie should match on the window where the snapshot has propagated.
        fig, ax= plt.subplots()
        rec.evalSe(time = np.arange(-100, 1000, 0.5)).plot(ax=ax)
        rec_back.evalSe(time = np.arange(-100, 1000, 0.5)).plot(ax=ax)
        ax.set(xlabel = "Time", title = f"x=0, heading = {heading:}" )
        
    # Reconstructed snapshot should match exactly
    assert( np.isclose(eta, eta_back, rtol = 1e-3, atol = 1e-2).all() )
    
        
if __name__ == "__main__":
    
    test_wif_fromfs( 0.0, display=True)
    test_wif_fromfs( np.pi, display=True)
    
    
