import numpy as np
from Snoopy import Spectral as sp
from Snoopy import logger

def test_encounter() :
    """Test encounter frequency formula
    """
    w = np.arange(0.2, 1.8 , 0.1)
    speed = 10.

    # Head sea-case
    for heading in [np.pi, 3*np.pi/4 , 5*np.pi/4]:
        we = sp.w2we( w = w, speed = speed, b = heading )
        new_w = sp.we2w( we = we, speed = 10., b = heading )
        assert( (we-w > 0).all() )
        assert( np.isclose(w, new_w).all() )
    

    # Following sea, several solutions
    heading = np.pi / 4

    we_lim = 9.81 / (4*speed*np.cos(heading))
    for we in [ 0.5*we_lim , we_lim , 1.2*we_lim  ] :
        new_w = sp.we2w( we = we, speed = speed, b = heading, return_type = "tuple" )
        logger.debug( f"we = {we:}, w = {new_w:}")
        for new_w_ in new_w :
            assert( np.isclose(we, np.abs(sp.w2we(new_w_, b=heading, speed = speed) )) )

    depth = 0.0
    assert( sp.T2Te( sp.cp2t(speed, depth ) + 0.0001, speed , depth ) > 1000)

def test_w_heading() :
    """Test iso we contour
    """
    c = sp.ContourWeSpeed(speed = 5.0)
    for we in np.arange(0.1, 3.0, 0.1):
        w, b = c(we)
        we_ = sp.w2we( w , b , speed = 5.0 )
        assert( np.isclose( we_[~np.isnan(we_)] , we, rtol = 1e-3).all())

def test_dispersion_back_forth():

    for w , h in (
                   [  0.5  , 1.0 ],
                   [  0.01 , 10. ],
                   [  0.01 , 0. ],
                   [  0.5 ,  0. ],
                   [  2.5 ,  100. ],
                   [  2.5 ,  0. ],
                 ) :

        assert( np.isclose( sp.k2w( sp.w2k( w ,h ) , h) , w)  )
        assert( np.isclose( sp.l2w( sp.w2l( w ,h ) , h) , w)  )


if __name__ == "__main__" :
    logger.setLevel(10)
    test_encounter()
    test_dispersion_back_forth()
    test_w_heading()
