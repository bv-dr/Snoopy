import numpy as np
import pytest
from Snoopy import Spectral as sp

def test_wif1(  ):
    hs, tp , gamma = 1.0 , 10.0 , 2.0
    tz = sp.Jonswap.tp2tz(tp, gamma)
    spec = sp.Jonswap(hs, tp, gamma, heading = 0.0)
    seastate = sp.SeaState(spec)

    wif = sp.Wif( seastate , wmin = 0.01, wmax = 10., nbSeed = 500, seed = 15)
    assert np.isclose( wif.hs , hs , atol = 0.01, rtol = 1e-2)
    assert np.isclose( wif.tz , tz , atol = 0.01, rtol = 1e-2)

    wif = sp.Wif( spec , wmin = 0.01, wmax = 10., nbSeed = 500, seed = 15, spreadNbheading = 72)
    assert np.isclose( wif.hs , hs , atol = 0.01, rtol = 1e-2)
    assert np.isclose( wif.tz , tz , atol = 0.01, rtol = 1e-2)


def test_wif_maker_fixed_freq():
    """
    Test wif constructor based on fixed frequency

    """
    hs1 = 5.0
    hs2 = 0.001
    spec1 = sp.Jonswap( hs1,10,1, 0., sp.SpreadingType.Cosn, 4 )
    spec2 = sp.Jonswap( hs2,12,1, np.deg2rad(90) )

    ss = sp.SeaState( [spec1, spec2] )

    wif = sp.Wif( ss , w = np.arange(0.1,2.0,0.05) , b = np.arange(0., 2*np.pi, np.deg2rad(5) )  )

    assert ( np.isclose( wif.hs, (hs1**2+hs2**2)**0.5 , 0.05) )


def test_wif_maker_random_freq():
    """
    Test wif constructor based on fixed frequency

    """
    hs1 = 5.0
    hs2 = 2
    spec1 = sp.Jonswap( hs1,10,1, 0., sp.SpreadingType.Cosn, 2 )
    spec2 = sp.Jonswap( hs2,12,1, np.deg2rad(90) )

    ss = sp.SeaState( [spec1, spec2] )

    wif = sp.Wif( ss , wmin = 0.2, wmax = 1.8, nbSeed = 200  )

    assert ( np.isclose( wif.hs, (hs1**2+hs2**2)**0.5 , 0.05) )

@pytest.mark.parametrize("spec", [(sp.Jonswap(1.0 , 10.0 , 1.0) ),
                                  (sp.API(1.0 , 0.2 , 0.0)),
                                  ])
def test_spectrum_distretization( spec  ):

    if hasattr( spec , "spreading" ) : # WaveSpectrum
        wmin, wmax = 0.1 , 4.0
    else : # WindSpectrum
        wmin, wmax = 0.0 , 1.0
    wrange = np.arange( wmin, wmax , 0.001)
    m0 = spec.integrate_moment(0 , wmin=wmin, wmax=wmax, extrap = False)
    wif = sp.Wif( spec , wmin = wmin, wmax = wmax , nbSeed = 500)
    wif_reg = sp.Wif( spec , w = wrange )

    print (m0)
    print (wif.m0)
    print (wif_reg.m0 )

    assert( np.sum( wif.freq == 0.) <= 1 )
    assert( np.isclose( m0, wif.m0, rtol = 0.01 ))
    assert( np.isclose( m0, wif_reg.m0, rtol = 0.01 ))


if __name__ == "__main__" :
    test_wif1()
    test_wif_maker_fixed_freq()
    test_wif_maker_random_freq()

    test_spectrum_distretization( sp.Jonswap(1.0 , 10.0 , 1.0) )
    test_spectrum_distretization( sp.API(1.0 , 0.2 , 0.0) )
