import Snoopy.Spectral as sp
import numpy as np
import time
import datetime
from matplotlib import pyplot as plt
import pytest

#sp.set_logger_level(10, "")



@pytest.mark.parametrize("rao, spreading", [
                                                ( sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ) , False ),
                                                ( sp.Rao( f"{sp.TEST_DATA:}/rao/B01/mx_05.rao" ) , False ),
                                                ( sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ) , True ),
                                                ( sp.Rao( f"{sp.TEST_DATA:}/rao/B01/mx_05.rao" ) , True ),
                                                ( sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ).getSymmetrized() , False ),
                                                (sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ).getSymmetrized(), False ),
                                                (sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ).getSymmetrized(), True ),
                                            ])
def test_SpectralMoments( rao, spreading ):
    """
        Spectrum with Spreading
    """
    print(f"RAO : {rao:}")
    w_min = min(np.min(rao.getFrequencies()), 0.01)
    w_max = max(np.max(rao.getFrequencies()), 2.50)
    print(f"    w_min = {w_min}")
    print(f"    w_max = {w_max}")
    print(f"    U     = {rao.getForwardSpeed():}")
    if spreading :
        s = sp.SpreadingType.Cosn
    else :
        s = sp.SpreadingType.No

    ss = sp.SeaState([
                       sp.Jonswap(2.0,  8.0, 1.0,  np.pi, s, 2.),
                       sp.Jonswap(2.0, 10.0, 1.5, -np.pi/3, s, 2.),
                       sp.Jonswap(2.0, 10.0, 1.5,  np.pi/2, s, 2.),
                       sp.Jonswap(2.0, 10.0, 1.5, -np.pi/4, s, 2.),
                      ])


    print("ResponseSpectrum: start")
    # total time:
    #   rao interpolation
    #   ResponseSpectrum initialization
    #   moment calculations
    t_start = time.time()
    rao_ = rao.getRaoForSpectral( db_deg = 1.0, wmax = w_max + 0.0005 , dw = 0.001, wmin = w_min )
    t_end_int = time.time()
    rs = sp.ResponseSpectrum(ss, rao_, computeM4=True)
    t_end_init = time.time()
    rs.getM0()                  # because the total time should also take into account the moment calculation, not only initialization
    t_end = time.time()
    print(f"ResponseSpectrum: time passed   : {datetime.timedelta(seconds = t_end -t_start):}")
    print(f"                  interpolation : {datetime.timedelta(seconds = t_end_int -t_start):}")
    print(f"                  initialization: {datetime.timedelta(seconds = t_end_init -t_end_int):}")
    print(f"                  moment calcs  : {datetime.timedelta(seconds = t_end -t_end_init):}")
    print(f"ResponseSpectrum: m0 = {rs.getM0():}")
    print(f"ResponseSpectrum: m2 = {rs.getM2():}")
    print(f"ResponseSpectrum: m4 = {rs.getM4():}")

    print("SpectralMoments: start")
    print("  1. same rao as for the ResponseSpectrum")
    # For this case no need to calculate the time.
    # this is just to check, that for the same rao_ we obtain the same results
    sm = sp.SpectralMoments([ss], rao_, computeM4 = True, w_min = w_min, w_max = w_max)

    print(f"SpectralMoments : m0 = {sm.getM0s():}, diff = {sm.getM0s()[0,0] / rs.getM0() - 1:.3%}")
    print(f"SpectralMoments : m2 = {sm.getM2s():}, diff = {sm.getM2s()[0,0] / rs.getM2() - 1:.3%}")
    print(f"SpectralMoments : m4 = {sm.getM4s():}, diff = {sm.getM4s()[0,0] / rs.getM4() - 1:.3%}")

    assert( np.isclose(sm.getM0s()[0,0] , rs.getM0(), rtol = 1e-3))
    assert( np.isclose(sm.getM2s()[0,0] , rs.getM2(), rtol = 1e-3))

    print("  2. original rao")
    # total time:
    #   SpectralMoments initialization
    #   moment calculations
    t_start = time.time()
    sm2 = sp.SpectralMoments([ss], rao, computeM4 = True, dw = 0.001, db = 1.0, w_min = w_min, w_max = w_max)    #  No Rao interpolation
    t_end_int = time.time()
    sm2.getM0s()                 # calculate the moments. Added here to calculate the time of the moments calculation
    t_end = time.time()
    print(f"SpectralMoments : time passed   : {datetime.timedelta(seconds = t_end -t_start):}")
    print(f"                  initialization: {datetime.timedelta(seconds = t_end_int -t_start):}")
    print(f"                  moment calcs  : {datetime.timedelta(seconds = t_end -t_end_int):}")
    print(f"SpectralMoments : m0 = {sm2.getM0s():}, diff = {sm2.getM0s()[0,0] / rs.getM0() - 1:.3%}")
    print(f"SpectralMoments : m2 = {sm2.getM2s():}, diff = {sm2.getM2s()[0,0] / rs.getM2() - 1:.3%}")
    print(f"SpectralMoments : m4 = {sm2.getM4s():}, diff = {sm2.getM4s()[0,0] / rs.getM4() - 1:.3%}")


    assert( np.isclose(sm.getM0s()[0,0] , rs.getM0(), rtol = 1e-3))
    assert( np.isclose(sm.getM2s()[0,0] , rs.getM2(), rtol = 1e-3))

    assert( np.isclose(sm2.getM0s()[0,0] , rs.getM0(), rtol = 1e-3))
    assert( np.isclose(sm2.getM2s()[0,0] , rs.getM2(), rtol = 1e-3))




@pytest.mark.parametrize("spreading", [ sp.SpreadingType.Cosn , sp.SpreadingType.No ])
def test_SpectralMoments_interpolation( spreading, display = False ):
    """Check heading interpolation behavior
    """

    heads = [ i * 2*np.pi/72 for i in range(72) ]

    # in order the second point (5°, 15°, 25°, ...) to be little bit shifted to the right
    # in this case n*10 +5°'s the closest point is (n+1)*10° (no uncertanity due to numerical number error)
    for h in range(1, len(heads), 2): heads[h] += 1.e-3

    ss = [ sp.SeaState.Jonswap( hs = 1.0 , tp = 10.0 , gamma = 1.5 , heading = h, spreading_type = spreading , spreading_value = 2.0 ) for h in heads ]

    rao = sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ).getRaoForSpectral( db_deg = 10. )
    m0s = sp.SpectralMoments( ss , rao, db = 10 ).getM0s()
    m0s_spec = np.array( [sp.ResponseSpectrum( ss_ , rao ).getM0s() for ss_ in ss] )

    if display :
        fig, ax = plt.subplots()
        ax.plot( np.rad2deg(heads) , m0s[:,0] , label = "SpectralMoments", marker = "*")
        ax.plot( np.rad2deg(heads) , m0s_spec[:,0] , label = "ResponseSpectrum", marker = "o")
        ax.legend()
        plt.show()

    assert(np.isclose( m0s , m0s_spec , rtol=1e-2 ).all())


if __name__ == "__main__":
    test_SpectralMoments_interpolation(sp.SpreadingType.No , display = True)
    test_SpectralMoments_interpolation(sp.SpreadingType.Cosn , display = True)

    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ), spreading = False)
    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/mx_05.rao" ), spreading = False)

    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ), spreading = True)
    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/mx_05.rao" ), spreading = True)
    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/heave.rao" ).getSymmetrized(), spreading = True)

    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ), spreading = False)
    test_SpectralMoments( rao = sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ), spreading = True)


