import numpy as np
from Snoopy import Spectral as sp
import pytest

import pytest
@pytest.mark.parametrize("spec", [   sp.Jonswap( 1 , 10, 2. )  ,
                                     sp.PiersonMoskowitz( 1 , 10 )    ,
                                     sp.SimpleOchiHubble( 1 , 10, 4 )
                                   ])

def test_extrap( spec  ) :
    wt = 6.0
    exact = spec.getTz()
    test = spec.integrate_tz( wmin=0.0001 , wmax=wt, extrap = False )
    test_asympt = spec.integrate_tz( wmin=0.0001 , wmax=wt, extrap = True )
    print ( exact )
    print ( test )
    print ( test_asympt )
    assert(  np.isclose(exact , test_asympt) )

    ss = sp.SeaState( spec )
    test_ss_asympt = ss.integrate_tz( wmin = 0.0001, wmax = wt, method = "quad")
    print (test_ss_asympt)
    assert(  np.isclose(exact , test_ss_asympt) )

if __name__ == "__main__" :
    print ("Run")
    test_extrap(  sp.PiersonMoskowitz( 1 , 10 )  )



