import os
import numpy as np
from Snoopy import Spectral as sp
from Snoopy import logger

logger.setLevel(10)

f =  f"{sp.TEST_DATA:}/my_10.rao"
f0 = f"{sp.TEST_DATA:}/my_10_0speed.rao"


wdir = f"{sp.TEST_DATA:}/test_write"

def test_refresh_real_imag():
    import _TimeDomain
    rwe_rao = sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ).getRaoForSpectral( wmin=0.2, wmax = 1.8, dw = 0.05 )
    a = rwe_rao.real[5,5,0]
    rwe_rao.derivate(1)
    b = rwe_rao.real[5,5,0]
    assert( a != b )

    rwe_rao = sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ).getRaoForSpectral( wmin=0.2, wmax = 1.8, dw = 0.05 )
    vz_rao = rwe_rao.getDerivate(n = 1)

    wif = sp.Wif( a = [1., ], w = [0.5,] , b =[np.pi] , phi = [0.])

    rwe_ts = _TimeDomain.ReconstructionRaoLocal( wif , rwe_rao.getSorted() ) # This line use to make the test fails (wrong/inconsistent refresh of reData_ / imData_)

    assert(np.isclose( vz_rao.phasis , rwe_rao.getDerivate(n=1).phasis ).all())
    assert(np.isclose( vz_rao.module , rwe_rao.getDerivate(n=1).module).all())
    assert(np.isclose( vz_rao.cvalues , rwe_rao.getDerivate(n=1).cvalues ).all())
    assert(np.isclose( vz_rao.getReal() , rwe_rao.getDerivate(n=1).getReal() ).all())
    assert(np.isclose( vz_rao.getImag() , rwe_rao.getDerivate(n=1).getImag() ).all())

def test_rao(plot = False) :

    rao = sp.Rao(f)
    rao.write( f"{sp.TEST_DATA:}/my_10_0speed_rewrite.rao")

    print (rao)

    v = rao.toDataFrame(cplxType = "module").loc[ 0.8412, np.radians( 15 ) ]

    raoSym = rao.getSymmetrized()
    assert( v == raoSym.toDataFrame(cplxType = "module").loc[ 0.8412, np.radians( 345 ) ] )

    rao2 = 2*rao
    assert( rao2.module[5,5,0] == 2*rao.module[5,5,0] )

    rao1 = rao2 / rao2

    assert( np.isclose( rao1.module[0,0,0], 1) )
    assert(  (rao1 / 3.).module[0,0,0] == rao1.module[0,0,0] / 3 )

    rao1 = rao2 * rao2

    assert( np.isclose( rao1.module[1,2,0], rao2.module[1,2,0]*rao2.module[1,2,0]) )

    rao1 = rao2 + rao2
    rao1.write(f"{wdir:}/rao_ope.rao")
    assert(np.isclose( rao1.cvalues[1,2,0], rao2.cvalues[1,2,0]+rao2.cvalues[1,2,0]))

    if plot :
        ax = rao2.plot()
        return ax


def test_rao_pickle():

    import pickle
    rao = sp.Rao(f)


    if not os.path.exists(wdir) :
        os.mkdir(wdir)

    with open(f"{wdir:}/rao.pkl", "wb") as _f:
        pickle.dump( rao, _f  )

    with open(f"{wdir:}/rao.pkl", "rb") as _f:
        rao2 = pickle.load( _f )

    assert( np.isclose( rao.getModules() , rao2.getModules() ).all() )


def test_rao_checks():

    rao = sp.Rao(f0)

    assert( rao.getRaoForSpectral().isReadyForSpectral() )

    print (rao.headDeg , rao.isReadyForSpectral(), rao.isReadyForInterpolation())
    assert ( rao.isReadyForSpectral() is False)
    assert(  rao.isReadyForInterpolation() is False)

    rao2 = rao.getSymmetrized()
    print (rao2.headDeg , rao2.isReadyForSpectral(), rao2.isReadyForInterpolation())
    assert ( rao2.isReadyForSpectral() is True)
    assert( rao2.isReadyForInterpolation() is False)

    rao3 = rao2.getSorted()
    print (rao3.headDeg , rao3.isReadyForSpectral(), rao3.isReadyForInterpolation())
    assert ( rao3.isReadyForSpectral() is False)
    assert (rao3.isReadyForInterpolation() is True)


def test_rao_interp_perf():

    rao = sp.Rao(f0)
    logger.info("With Python START")
    rao1 = rao.getFreqInterpolate(0.05, 2.0, 0.001, usePythonInterp = True, k = 1)
    logger.info("Python interp STOP")
    logger.info("With cpp START")
    rao2 = rao.getFreqInterpolate(0.05, 2.0, 0.001, usePythonInterp = False)
    logger.info("Cpp interp STOP")
    raodiff = rao1-rao2

    assert( np.max(raodiff.module) < 1e-5 )


def test_filter_we(display = False) :

    rao = sp.Rao( f )
    rao_f = rao.getRaoWeFiltered(we_max = 1.2)

    #Quick check
    assert( rao_f.module[-1, -1 , 0] == 0.0)

    if display :
        ax = rao.plot( headingsDeg = [90, 180.])
        rao_f.plot(headingsDeg = [90 , 180.] , ax=ax)



def test_opposite_side( display = False):

    rao_ps = sp.Rao( f"{sp.TEST_DATA:}/rao/mid_ship_wl_ps.rao" )
    rao_sb = sp.Rao( f"{sp.TEST_DATA:}/rao/mid_ship_wl_sb.rao" )

    rao_ps_re = rao_sb.get_opposite_side()

    if display :
        ax = rao_ps.plot( headingsDeg = [150.], ls = "", part = "module", label_prefix = "Ref")
        rao_ps_re.plot(headingsDeg = [150.] , ax=ax, marker = None, part = "module", label_prefix = "New")
        ax.legend()

    assert( rao_ps.isClose( rao_ps_re, rtol = 1e-4 ) )



def test_acc_from_motion(display = False):
    """Test the calculation of acceleration from motion and compare to HydroStar results, considered as reference.
    """
    
    rao = sp.Rao( [sp.Rao( f"{sp.TEST_DATA:}/rao/B01/{rao_:}.rao") for rao_ in [ "surge", "sway" , "heave", "roll" , "pitch", "yaw"] ])


    for f, component in [ ( "accz_x21_y2" , 2),
                          ( "accy_x10_z6" , 1),
                          ("accx_z2_y0",0),
                          ]:
        
        ref_accz = sp.Rao( f"{sp.TEST_DATA:}/rao/B01/{f:}.rao" )
        coords = ref_accz.getReferencePoint()
        rao_snoopy = rao.getAccAtPoint(component, coords, angleUnit = "deg", gravity = True)
        

        if display:
            from matplotlib import pyplot as plt
            fig, ax = plt.subplots()
            ref_accz.plot( headingsDeg = [150., 180.], ax=ax)
            rao_snoopy.plot(  headingsDeg = [150., 180.],ax=ax, ls = "")
            
        assert( np.isclose( rao_snoopy.getReal(),  ref_accz.getReal(), rtol = 1e-6, atol = 0.001 ).all() )
        


if __name__ == "__main__" :
    

    test_rao(False)
    test_rao_pickle()
    test_rao_interp_perf()
    test_rao_checks()
    test_filter_we()
    test_refresh_real_imag()
    test_opposite_side()
    test_acc_from_motion(True)

    print ("All tests OK!")




