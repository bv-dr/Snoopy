import numpy as np
from matplotlib import pyplot as plt
from Snoopy import Spectral as sp
from Snoopy import WaveKinematic as wk
import pytest

#Time range to match
tmin, tmax, dt = 0, 50, 0.4
t = np.arange(tmin, tmax, dt)

#Extended time range to see what happen outside the fitted range
t2 = np.arange(tmin-60, tmax+60, 0.4)

#Wif file
hs = 10.

ss = sp.SeaState.Jonswap( hs=hs, tp=12., gamma=1.0, heading = np.pi )

wif = sp.Wif( ss, nbSeed = 100, seed = 12 )

pos = [0,0]

def wif_fit(kinModel = wk.FirstOrderKinematic, plot = False ):

    kin = kinModel(wif = wif)

    eta = kin.getElevation_SE( t, x = 0.0 , y = 0.0 )

    #Wif through FFT
    wif_fft = sp.Wif.FromTS( eta , b=0.0 )

    #Frequency range for LSQ
    lst_freq = wif_fft.freq  #
    #lst_freq = np.linspace( 0.2, 2.0 , int(len(eta)/6) )
    #lst_freq = wif.freq

    print ("Linear LSQ")
    wif_llsq = sp.Wif.FromTS_LLSQ( eta,  freq = lst_freq )

    # So far, second-order wave model crashes if w=0
    if kinModel is wk.SecondOrderKinematic21 or kinModel is wk.SecondOrderKinematic :
        wif_llsq.removeZeroFrequency()

    print ("Non-linear LSQ")
    #Initialized with linear least-square
    wif_lsq = sp.Wif.FromTS_LSQ( eta, model = kinModel, freq = wif_llsq.freq, x0 = wif_llsq.getCosSin(),
                                 solver_kwargs = { } )


    caseList = [   (wif, "original", None, "-"),
                   #(wif_llsq, "linear lsq", None, "-" ),
                    (wif_lsq, "NL LSQ", "+" , "-")
                  ]

    eta_rec = {}
    for w, name, marker, linestyle in caseList:
        if w is not None :
            print (name , w)
            kin_rec = kinModel(wif = w)
            eta_rec[name] = kin_rec.getElevation_SE( t2, x = 0.0 , y = 0.0  )


    if plot :
       fig , ax = plt.subplots()
       for w, name, marker, linestyle in caseList :
           eta_rec[name].plot(marker = marker, linestyle = linestyle, ax=ax , label = name)
       ax.legend()

    err =  np.max(np.abs(eta_rec["original"].loc[tmin+dt:tmax-dt] - eta_rec["NL LSQ"].loc[tmin+dt:tmax-dt] )) / np.max( eta_rec["original"].loc[tmin+dt:tmax-dt] )
    print ("Max error {:.1%}".format(err))
    assert (err < 0.02)

    return wif_lsq

def test_wif_fit_lin():
    wif_fit()

def test_wif_fit_2nd():
    wif_fit(wk.SecondOrderKinematic21)



def test_iterative_2nd(display = False):
    kin = wk.SecondOrderKinematic21(wif = wif)
    eta = kin.getElevation_SE( t, x = 0.0 , y = 0.0 )

    #Wif through FFT
    wif_fft = sp.Wif.FromTS_FFT2( eta , wmax = 1.9)

    eta_re = wk.SecondOrderKinematic21(wif = wif_fft).getElevation_SE( t, x = 0.0 , y = 0.0 )

    if display :
        fig, ax = plt.subplots()
        eta.plot( ax=ax, label = "Original" )
        eta_re.plot(ax=ax, label = "Recalculated")
        ax.legend()

    assert ( (eta_re - eta).loc[10:40].abs().max() < hs / 20 )


@pytest.mark.parametrize("heading" , [ np.pi , 5*np.pi / 4 ])
def test_wif_fromTS( heading, speed = 10.0, display = False ):
    """Test Wif.FromTS_FFT by constructing free-surface and retrieving it back. 

    Include forward speed.
    """

    from Snoopy import TimeDomain as td
    import pandas as pd
    from Snoopy import PyplotTools as dplt
    
    ss = sp.SeaState.Jonswap( hs=hs, tp=12., gamma=1.0, heading = heading )
    
    #------ Create "original" wave elevation on a space/time mesh.
    time_vect = np.arange(-200, 700. , 0.5 )
    x_vect = np.arange(-100, 600 , 10. )
    time_, x_range_ = np.meshgrid( time_vect , x_vect )

    wif = sp.Wif( ss, nbSeed = 100, seed = 12 )
    rec_eta = td.ReconstructionWifLocal(wif)
    eta_surf = rec_eta( time = time_ , x = x_range_ , y = 0.0 )
    df = pd.DataFrame( index = pd.Index( time_vect , name = "Time"), columns = pd.Index( x_vect , name = "x")    , data = eta_surf.T )

    # Create wave elevation at vessel position    
    x_track = time_vect * speed
    eta_track = pd.Series( data = rec_eta( time_vect , x_track , 0.0 ), index = time_vect ) 
    
    
    #------ Compute back wif from wave probe fixed to the vessel
    wif_back = sp.Wif.FromTS(eta_track, b = heading, speed = speed)
    wif_back.optimize(0.9999)

    # Compue back the elevation, at ship location (should be exactly the same, by construction)    
    rec_eta_back = td.ReconstructionWifLocal(wif_back)
    eta_track_back = pd.Series( data = rec_eta_back( time_vect , x_track , 0.0 ), index = time_vect ) 
    
    
    eta_surf_back = rec_eta_back( time = time_ , x = x_range_ , y = 0.0 )
    df_back = pd.DataFrame( index = pd.Index( time_vect , name = "Time"), columns = pd.Index( x_vect , name = "x")    , data = eta_surf_back.T )
    
    
    if display :     
        dplt.dfSlider( [ df, df_back ], labels = ["Reference" , "Reconstructed"] )
        
        fig, ax= plt.subplots()
        eta_track.plot(ax=ax, label ="original")
        eta_track_back.plot(ax=ax, marker = "+", linestyle = "", label = "reconstructed")
        ax.legend()
        ax.set(title = "Elevation at ship position")
                
    # For folliwing seas, cannot work perfectly... 
    if np.cos(heading) <= 0.0 :
        assert( np.isclose( eta_track_back , eta_track , rtol = 1e-3 , atol = 1e-2).all() )
        
        # Compare eta space/time surface, slight deviation can be expected.
        assert(np.isclose( df, df_back, rtol = 0.1, atol = 0.1 ).all())




if __name__ == "__main__":

    print ("Run")
    test_wif_fromTS(heading = np.pi)
    test_wif_fromTS(heading = np.pi*5/4)
    test_wif_fromTS(heading = 0.0, speed = 2.0)
    
    test_wif_fit_2nd()
    test_iterative_2nd()

