import pickle
import Snoopy.Spectral as sp
import numpy as np
import os

def test_pickle():
    storage_filename = "pickle_test.dat"
    qtf_filename = f"{sp.TEST_DATA:}/FPSO_QTF_FX.qtf"

    print("test_pickle")
    print(f"Reading qtf file: {qtf_filename:}")
    qtf = sp.Qtf(qtf_filename)
    print(f"File is going to be pickled (stored) to: {storage_filename:}")
    pickle.dump(qtf, open(storage_filename, "wb"))

    print("Unpickle qtf")
    qtf_ = pickle.load(open(storage_filename, "rb"))

    print("Remove storage file")
    if os.path.exists(storage_filename): os.remove(storage_filename)

    #      qtf - original data,                 qtf_ - restored data
    assert(abs(qtf.getDepth()                     - qtf_.getDepth()) < 1.e-10)
    assert(abs(qtf.getForwardSpeed()              - qtf_.getForwardSpeed()) < 1.e-10)
    assert(abs(qtf.getSumMode()                   - qtf_.getSumMode()) < 1.e-10)
    assert(np.allclose(qtf.getHeadings(),           qtf_.getHeadings()))
    assert(np.allclose(qtf.getFrequencies(),        qtf_.getFrequencies()))
    assert(np.allclose(qtf.getDeltaFrequencies(),   qtf_.getDeltaFrequencies()))
    assert(np.allclose(qtf.getReferencePoint(),     qtf_.getReferencePoint()))
    assert(np.allclose(qtf.getWaveReferencePoint(), qtf_.getWaveReferencePoint()))
    assert(np.allclose(qtf.getModeCoefficients(),   qtf_.getModeCoefficients()))
    assert(np.allclose(qtf.getModes(),              qtf_.getModes()))
    assert(np.allclose(qtf.getComplexData(),        qtf_.getComplexData()))
    print("end of test_pickle")

if __name__ == "__main__":
    test_pickle()


