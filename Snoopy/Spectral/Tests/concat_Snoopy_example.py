import os
import copy
import numpy as np

import Snoopy.Spectral as sp
""" 
Example of the concatenation of several qtf files using Snoopy 
    Parameters:
        outputDir - the folder the result files are going to be written
        filelist  - a list of files to be merged. NOTE: The files are located in different folders!
        folders   - a list of folders, where the files to be concatenated are located

    Attention: It merges by following the rule:
        The files to be merged must be located in different folders but with the same name!
        For example: 
            file "rwe_RU_01.qtf" from the folder "hstar_calc_01_res" is going to be merged with
            file "rwe_RU_01.qtf" from the folder "hstar_calc_02_res" and with
            file "rwe_RU_01.qtf" from the folder "hstar_calc_03_res" and put the results to
            "rwe_RU_01.qtf" from the folder "hstar_calc_res".
            In this case the parameters are as the following:
            filelist = ["rwe_RU_01.qtf"]
            folders  = ["hstar_calc_01_res", "hstar_calc_02_res", "hstar_calc_03_res"]
            outputDir = "hstar_calc_res
"""

def concatQtf_DIF_sn(filelist, folders, outputDir = "."):
    """
    concatination of the files from the filelist, where the difference is the differentf frequencies deltas
    the files are located in the different folders from the folders list.
    """
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    for f in filelist:
        qtfs = []
        for d in folders:
            qtfs.append(sp.Qtf(os.path.join(d, f)))
        filename = os.path.join(outputDir, f)
        print(filename)
        if len(qtfs) > 1:
            qtf1 = qtfs[0]
            for qtf in qtfs[1:]:
                qtf1 = sp.Qtf.MergeQtf_Diff(qtf1, qtf, checkUniformness = False)    # here we switched off the checking the uniformity of the difference frequencies
        else:
            qtf1 = qtfs[0]
        i, dw, dw_next = sp.qtf.checkArrayUniformness(qtf1.getDeltaFrequencies())   # and check it in the final qtf object
        if i > 0:
            print(filename +" has non uniform delta frequencies")
        qtf1.write(filename)
        #return qtf1

if __name__ == "__main__":
    outputDir = "hstar_final_res_Snoopy"
    filelist = ["rwe_RU_01.qtf",
                "rwe_RU_02.qtf",
                "rwe_RU_03.qtf",
                "rwe_RU_04.qtf",
                "rwe_RU_05.qtf",
                "rwe_RU_06.qtf",
                "rwe_RU_07.qtf",
                "rwe_RU_08.qtf",
                "rwe_RU_09.qtf",
                "rwe_RU_10.qtf",
                "rwe_RU_11.qtf",
                "rwe_RU_12.qtf",
#                "rwe_RU_13.qtf",
                "rwe_RU_14.qtf",
                "rwe_RU_15.qtf",
                "rwe_RU_16.qtf",
#                "rwe_RU_17.qtf",
                "rwe_RU_18.qtf",
                "rwe_RU_19.qtf",
                "rwe_RU_20.qtf",
                "rwe_RU_21.qtf",
                "rwe_RU_22.qtf",
                ]
    folders = ["hstar_calc_03_res/rao",     # calculation for diff:       0.22, 0.24, 0.26, 0.28, 0.30
               "hstar_calc_01_res/rao",     # calculation for diff: 0.00, 0.02, 0.04, 0.06, 0.08, 0.10
               "hstar_calc_02_res/rao",     # calculation for diff:       0.12, 0.14, 0.16, 0.18, 0.20
               ]
    concatQtf_DIF_sn(filelist = filelist, folders = folders, outputDir = outputDir)
