from Snoopy import Spectral as sp
import numpy as np
import pandas as pd

hspecTemplate ="""RAO_PATH
test_data
ENDRAO_PATH

RAO_FILE
my_10.rao
ENDRAO_FILE

LIST_GENERIC
{}
{}
ENDLIST_GENERIC

AZIMUTH
150.
1.0

DREF 25.                                                                        .
NB_HSTEP 72
STEP 0.005
OMEGA 0.05  3.5

OUTPUT
RAO NO
RPERIOD 25
LONGTERM  NO
SHORTTERM EXCEL
WAVESPECTRUM
ENDOUTPUT

ENDFILE
"""


def create_hspec_file():

    hs_1 = 4.0
    tp_1 = 8.0
    hs_2 = 2.0
    tp_2 = 14.0
    heading = np.deg2rad(180)
    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = 1.0 , heading = heading , spreading_type = sp.SpreadingType.No )
    swell = sp.Jonswap( hs = hs_2 , tp = tp_2 , gamma = 3.3 , heading = heading   , spreading_type = sp.SpreadingType.No )
    ss = sp.SeaState( [windSea , swell] )
    w = np.linspace( 0.2 , 2.5, 100 )
    ssTab = sp.WaveTabulatedSpectrum( w = w, sw = ss.compute(w) , heading = heading)


    parametric = ss.hspecString()
    tabulated = ssTab.hspecString(filename = "tabulatedSpec.dat")

    hspecInput = hspecTemplate.format( parametric, tabulated )
    with open("test.spc", "w") as f : f.write(hspecInput)


def create_hspec_file_2D():

    hs_1 = 4.0
    tp_1 = 8.0
    hs_2 = 2.0
    tp_2 = 14.0
    heading = np.deg2rad(180)
    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = 1.0 , heading = heading , spreading_type = sp.SpreadingType.Cosn, spreading_value = 4.0 )
    swell = sp.Jonswap( hs = hs_2 , tp = tp_2 , gamma = 3.3 , heading = heading   , spreading_type = sp.SpreadingType.Cos2s , spreading_value = 16.0)
    ss = sp.SeaState( [windSea , swell] )

    ss2D = sp.SeaState2D(ss)

    parametric = ss.hspecString()
    tabulated2D = ss2D.hspecString(filename = "tabulatedSpec2D.dat")

    hspecInput = hspecTemplate.format( parametric, tabulated2D )
    with open("test2D.spc", "w") as f : f.write(hspecInput)


if __name__ == "__main__" :

    create_hspec_file()
    create_hspec_file_2D()

    #Run StarSpec

    #Compare wave spec read by StarSpec and results
    f1 = r"C:\drsvn\Snoopy\Spectral\SpectralTests\test_spec\Wavespectrum\wavespec001.dat"
    f2 = r"C:\drsvn\Snoopy\Spectral\SpectralTests\test_spec\Wavespectrum\wavespec002.dat"

    #Compare Spectrum
    d1 = pd.read_csv(f1 , sep='\\s+', index_col= 0, comment = "#", header = None)
    d2 = pd.read_csv(f2 , sep='\\s+', index_col= 0, comment = "#", header = None)
    ax = d1.plot()
    ax = d2.plot(ax = ax)

    #Compare results









