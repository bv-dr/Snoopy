import numpy as np
from matplotlib import pyplot as plt
from Snoopy.Spectral.waveMaker import adim_transfer_function_hinged, adim_transfer_function_piston, adim_transfer_function_flap



def test_wavemaker(display = False):

    assert( np.isclose( adim_transfer_function_flap(0.5),  np.abs(adim_transfer_function_hinged(0.5 , 0.0)) ))

    if display :
        # Should get figure from NI 666.
        kh = np.linspace( 0.1, 8.0, 200 )
        fig, ax = plt.subplots()
        ax.plot( kh,  np.abs(adim_transfer_function_hinged(kh, 0.0)) , label = "Flap" )
        ax.plot( kh,  np.abs(adim_transfer_function_piston(kh)) , label = "Piston" , linestyle = "--" )
        ax.legend()
        ax.set(xlabel = "$kh$", ylabel = r"$A / X_0$")

    return


if __name__ == "__main__" :


    test_wavemaker(True)

