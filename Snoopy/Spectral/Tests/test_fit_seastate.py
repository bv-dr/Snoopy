# import os
# os.environ["SNOOPY_TOOLSET"] = "MINGW"
import netCDF4
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from Snoopy import Spectral as sp

hs_1 = 4.0
tp_1 = 8.0
gamma_1 = 1.0
heading_1 = np.deg2rad(90)
spreading_1 = 4.0


hs_2 = 2.0
tp_2 = 14
gamma_2 = 3.3
heading_2 = np.deg2rad(180)
spreading_2 = 16.0


def test_unimodal(plot = False) :
    # ----------- Uni-modal spectrum
    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = gamma_1 , heading = heading_1 , spreading_type = sp.SpreadingType.Cosn , spreading_value = spreading_1 )
    ss1 = sp.SeaState( [windSea] )

    reFit_ss1 = ss1.fitParametric2D( [sp.Jonswap], spreadingType = sp.SpreadingType.Cosn , coefs_0=[ hs_1*1.1, tp_1*0.8, gamma_1*2 , heading_1*0.9, spreading_1*5] , solver = "local")

    print ("Re - fit", reFit_ss1.getSpectrum(0) )
    print ("Original", ss1.getSpectrum(0) )

    for iv, v in enumerate(reFit_ss1.getSpectrum(0).getCoefs()) :
            assert(  0.99 < v / ss1.getSpectrum(0).getCoefs()[iv] < 1.01 )

    if plot :
        ss1.plot2D(polar = True )
        reFit_ss1.plot2D(polar = True )


def test_bimodal(plot = False) :
    # ----------  Bi-modal sea-state
    windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = gamma_1 , heading = heading_1 , spreading_type = sp.SpreadingType.Cosn , spreading_value = spreading_1 )
    swell = sp.Jonswap( hs = hs_2 , tp = tp_2 , gamma = gamma_2 , heading = heading_2  , spreading_type = sp.SpreadingType.Cosn , spreading_value = spreading_2)
    ss2 = sp.SeaState( [windSea , swell] )

    coefs_0 = np.array([ hs_1, tp_1, gamma_1, heading_1, spreading_1, hs_2, tp_2, gamma_2 , heading_2, spreading_2])
    noise = np.array([0.01, 0.01 , 0.05 , 0.1 , -0.08, 0.05 , -0.03, 0.1, 0.1, 0.2]) * 1. + 1
    reFit_ss2 = ss2.fitParametric2D( [sp.Jonswap, sp.Jonswap], spreadingType = sp.SpreadingType.Cosn , coefs_0 = coefs_0*noise,
                                      solver = "local", method = "Powell" )
    for i in [0,1] :
        print ("Re - fit", reFit_ss2.getSpectrum(i) )
        print ("Original", ss2.getSpectrum(i) )
        for iv, v in enumerate(reFit_ss2.getSpectrum(i).getCoefs()) :
            assert(  0.99 < v / ss2.getSpectrum(i).getCoefs()[iv] < 1.01 )
        assert(  abs( ss2.getSpectrum(i).heading - reFit_ss2.getSpectrum(i).heading ) % 2*np.pi <  np.pi / 180 )

    if plot :
        sp.compareSeaStatePlot(ss2, reFit_ss2)


def ttest_hindcast():

    #Read netcdf ERA-5 and construct Snoopy spectrum
    tmp = netCDF4.Dataset( r"E:\EcmwfData\ERA5\spec2D_NA_2018_jan.nc", 'r', format='NETCDF4')
    ecmwf_freq = [0.03453 * 2*np.pi * 1.1**n for n in range(0,30)]
    ecmwf_dir = np.linspace( 0. , 345. , 24 )
    spec2D = tmp.variables["d2fd"][200, :, :, 0, 0]
    df = pd.DataFrame( index = ecmwf_freq , columns = tmp.variables["direction"] , data = spec2D)
    df = 10**df
    df.fillna(0.0, inplace = True)
    df.columns = np.deg2rad( ecmwf_dir )

    ss2D = sp.SeaState2D.FromDataFrame( df )

    #Initial guess
    coefs_0 = None  #Far from solution, require a global optimizer, slow !

    coefs_0 =  [9.59582  , 9.9746,  1.85138  , np.deg2rad(91.036) , 3.40539 ] + \
               [ 8.19677 , 11.2563,  1.63306  , np.deg2rad(0.9239) , 4.36692 ]

    reFit_ss2 = ss2D.fitParametric2D( [sp.Jonswap, sp.Jonswap], spreadingType = sp.SpreadingType.Cosn ,
                                      coefs_0 = coefs_0, solver = "local"    )

    for i in [0,1] :
        print ("Re - fit", reFit_ss2.getSpectrum(i) )

def test_spectrumShapeFit():

    for gamma in [1.0 , 5.0 , 10.0] :
        windSea = sp.Jonswap( hs = hs_1 , tp = tp_1 , gamma = gamma )
        refitted = windSea.fitSpectrumShape( sp.Jonswap )
        print ( gamma , refitted.gamma )
        assert( np.isclose( gamma, refitted.gamma  ) )


if __name__ == "__main__" :

    import time
    t0 = time.time()
    test_unimodal()
    test_bimodal()
    test_spectrumShapeFit()
    print ("OK in {:.1f}".format(time.time()-t0))


