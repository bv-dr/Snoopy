import numpy as np
from matplotlib import pyplot as plt
from Snoopy import Spectral as sp
import os
data_dir = f"{sp.TEST_DATA:}/rao"

def test_motionRaoAtPoint(display = False) :
    """Check moving ref points using HydroStar as a reference
    """

    motionRaos = sp.Rao(  [ sp.Rao( os.path.join( data_dir, f"{mode:}.rao" )) for mode in ["surge", "sway", "heave", "roll", "pitch", "yaw"] ] )
    movedRaos_ref = sp.Rao(  [ sp.Rao( os.path.join( data_dir, f"{mode:}_100.rao" )) for mode in ["surge", "sway", "heave", "roll", "pitch", "yaw"] ] )

    refPoint = np.array([100.,100.,100.])
    raos = motionRaos.getMotionRaoAtPoint( refPoint, angleUnit = "deg" )

    assert(  movedRaos_ref.isClose( raos ) )
    assert( np.isclose(raos.getReferencePoint(), refPoint).all())

    if display :
        for imode in [0,1,2] :
            fig, ax = plt.subplots()
            raos.plot(imode = imode, ax=ax, headingsDeg = [45.] )
            movedRaos_ref.plot(imode = imode, ax=ax, headingsDeg = [45.] , marker = None)

    print ("Moving motions Ok")


def test_LoadAtPoint(display = False):
    """Check moving ref points using HydroStar as a reference
    """

    loadRaos = sp.Rao(  [ sp.Rao( os.path.join( data_dir, "B01", f"{mode:}.rao" )) for mode in ["fx_05", "fy_05", "fz_05", "mx_05", "my_05", "mz_05"] ] )

    mx_ref = sp.Rao( os.path.join( data_dir,"B01", "mx_05_z00.rao" ))

    refPoint = mx_ref.getReferencePoint()

    loadRaos = loadRaos.getLoadRaoAtPoint( refPoint )

    mx_new = loadRaos.getRaoAtMode(3)

    assert(  mx_new.isClose( mx_ref, rtol = 1e-3 ) )

    if display :
        fig, ax = plt.subplots()
        mx_new.plot( ax=ax, headingsDeg= [45.] )
        mx_ref.plot( ax=ax, headingsDeg=[45.] )

    print ("Moving loads Ok")


if __name__ == "__main__"  :

    test_motionRaoAtPoint(True)
    test_LoadAtPoint(True)
