#ifndef _BRENTOPTIMIZE_HPP_
#define _BRENTOPTIMIZE_HPP_

namespace BV {
namespace Math {
namespace Solvers {
namespace R1ToR1 {
namespace Details {

/*!
 \brief Particular sign function for Brent Optimize
 \param[in] a First value
 \param[in] a Second value
 \returns a with the signus of b
 */
inline double mysign(double a, double b)
{
    if (b > 0)
    {
        return std::abs(a) ;
    }
    else
    {
        return -std::abs(a) ;
    }
}
}

/*!
 \brief Minimization by the Brent method
 \param[in] fct Function to be evaluated
 \param[in] a Left boundary of an interval to search minimum in
 \param[in] b Right boundary of an interval to search minimum in
 \param[in] X_tolerance Absolute error of the value of the function minimum
 \param[in] fct_lower_threshold Function minimum value. Usefull for a positive function for example
 \param[in] max_it Maximum number of iterations
 \param[out] XMin Point of minimum
 \returns Value at the point of minimum
 */
template <typename FCT>
inline double BrentOptimize(const FCT & fct, double a, double b,
                            double X_tolerance, double fct_lower_threshold,
                            int max_it, double & XMin)
{
    double result ;
    double ia ;
    double ib ;
    double bx ;
    double d = 0. ;
    double e ;
    double etemp ;
    double fu ;
    double fv ;
    double fw ;
    double fx ;
    int iter ;
    double p ;
    double q ;
    double r ;
    double u ;
    double v ;
    double w ;
    double x ;
    double xm ;
    const double cgold = 0.3819660 ;
    bx = 0.5 * (a + b) ;
    if (a < b)
    {
        ia = a ;
    }
    else
    {
        ia = b ;
    }
    if (a > b)
    {
        ib = a ;
    }
    else
    {
        ib = b ;
    }
    v = bx ;
    w = v ;
    x = v ;
    e = 0.0 ;
    fx = fct(x) ;
    fv = fx ;
    fw = fx ;
    for (iter = 1 ; iter <= max_it; iter++)
    {
        xm = 0.5 * (ia + ib) ;
        if (std::abs(x - xm) <= X_tolerance * 2 - 0.5 * (ib - ia))
        {
            break ;
        }
        if (fx < fct_lower_threshold)
        {
            break ;
        }
        if (std::abs(e) > X_tolerance)
        {
            r = (x - w) * (fx - fv) ;
            q = (x - v) * (fx - fw) ;
            p = (x - v) * q - (x - w) * r ;
            q = 2 * (q - r) ;
            if (q > 0)
            {
                p = -p ;
            }
            q = std::abs(q) ;
            etemp = e ;
            e = d ;
            if (!((std::abs(p) >= std::abs(0.5 * q * etemp))
                || (p <= q * (ia - x)) || (p >= q * (ib - x))))
            {
                d = p / q ;
                u = x + d ;
                if (((u - ia) < (X_tolerance * 2))
                    || ((ib - u) < (X_tolerance * 2)))
                {
                    d = Details::mysign(X_tolerance, xm - x) ;
                }
            }
            else
            {
                if (x >= xm)
                {
                    e = ia - x ;
                }
                else
                {
                    e = ib - x ;
                }
                d = cgold * e ;
            }
        }
        else
        {
            if (x >= xm)
            {
                e = ia - x ;
            }
            else
            {
                e = ib - x ;
            }
            d = cgold * e ;
        }
        if (std::abs(d) >= X_tolerance)
        {
            u = x + d ;
        }
        else
        {
            u = x + Details::mysign(X_tolerance, d) ;
        }
        fu = fct(u) ;
        if (fu <= fx)
        {
            if (u >= x)
            {
                ia = x ;
            }
            else
            {
                ib = x ;
            }
            v = w ;
            fv = fw ;
            w = x ;
            fw = fx ;
            x = u ;
            fx = fu ;
        }
        else
        {
            if (u < x)
            {
                ia = u ;
            }
            else
            {
                ib = u ;
            }
            if ((fu <= fw) || (w == x))
            {
                v = w ;
                fv = fw ;
                w = u ;
                fw = fu ;
            }
            else
            {
                if ((fu <= fv) || (v == x) || (v == 2))
                {
                    v = u ;
                    fv = fu ;
                }
            }
        }
    }
    XMin = x ;
    result = fx ;
    return result ;
}

/*!
 \brief Minimization by the Brent method (iterative to be more robust)
 \param[in] fct Function to be evaluated
 \param[in] a Left boundary of an interval to search minimum in
 \param[in] b Right boundary of an interval to search minimum in
 \param[in] X_tolerance Absolute error of the value of the function minimum
 \param[in] fct_lower_threshold Function minimum value. Usefull for a positive function for example
 \param[in] max_it Maximum number of iterations
 \param[out] XMin Point of minimum
 \returns Value at the point of minimum
 */
template <typename FCT>
inline double BrentOptimizeIterative(const FCT & fct, double a, double b,
                                     double X_tolerance,
                                     double fct_lower_threshold, int max_it,
                                     double & XMin,
                                     const unsigned & maxRecursiveIterations,
                                     const double & epsilon,
                                     unsigned iIterations = 0
                                     )
{
    double X0 = a ;
    double X2 = b ;
    double res0 = BrentOptimize(fct, a, b, X_tolerance, fct_lower_threshold,
                                max_it, XMin) ;
    double X1 = XMin ;
    if (iIterations > maxRecursiveIterations)
    {
        return res0 ;
    }
    const double resLeft = BrentOptimize(fct, X0, X1, X_tolerance,
                                         fct_lower_threshold, max_it, XMin) ;
    if (resLeft < res0 - epsilon)
    {
        // A new minimum is found
        return BrentOptimizeIterative(fct, X0, X1, X_tolerance,
                                      fct_lower_threshold, max_it, XMin,
                                      maxRecursiveIterations, epsilon,
                                      iIterations + 1) ;
    }
    const double resRight = BrentOptimize(fct, X1, X2, X_tolerance,
                                          fct_lower_threshold, max_it, XMin) ;
    if (resRight < res0 - epsilon)
    {
        // A new minimum is found
        return BrentOptimizeIterative(fct, X1, X2, X_tolerance,
                                      fct_lower_threshold, max_it, XMin,
                                      maxRecursiveIterations, epsilon,
                                      iIterations + 1) ;
    }
    return res0 ;
}
} // end of namespace R1ToR1
} //end of namespace Solvers
} //end of namespace Math
} //end of namespace BV

#endif
