#pragma once
#ifndef BV_Math_Solvers_R1ToR1_Laguerre_hpp
#define BV_Math_Solvers_R1ToR1_Laguerre_hpp

#include "Math/Tools.hpp"
#include "Math/Functions/Polynomial.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace R1ToR1 {

/*!
 \brief Laguerre's method is a root-finding algorithm tailored to polynomials.
 \param[in] x0 start point
 \param[in] polynomial Polynomial object
 \param[in] x_tolerance Absolute error of the value of the function minimum
 \param[in] max_it Maximum number of iterations
 \param[out] Xres Solution if found
 \returns true if NO solution is found. Else returns false.
 */
inline bool Laguerre(const double & x0,
                     const BV::Math::Functions::Polynomial<1> & polynomial, // not const due to polynomial eval method
                     const double & xTolerance, const double & fctTolerance,
                     const unsigned & nMaxIter, double & xRes)
{
    // Variables initializations
    const std::size_t polynomial_order(polynomial.getOrder()) ;
    //BV::Math::Functions::Polynomial<1> derPoly = polynomial.getPolynomialDer() ;
    //BV::Math::Functions::Polynomial<1> der2Poly = derPoly.getPolynomialDer() ;
    if (polynomial_order < 1)
    {
        // Polynomial order must be higher than 0"
        return true ;
    }
    if (polynomial_order == 1)
    {
        xRes = -polynomial.eval(0.) / polynomial.dEval(0.) ;
        return false ;
    }
    double x = x0 ;
    double G ;
    double H ;
    double a ;
    double p_xi, inv_p_xi, p_der_xi, p_der_prim_xi ;
    for (unsigned i = 0; i < nMaxIter; i++)
    {
        p_xi = polynomial.eval(x) ;
        if (BV::Math::IsNull(p_xi, fctTolerance))
        {
            xRes = x ;
            return false ;
        }
        inv_p_xi = 1. / p_xi ;
        p_der_xi = polynomial.dEval(x) ;
        p_der_prim_xi = polynomial.dEval2(x) ;
//        p_der_xi = derPoly.eval(x) ;
//        p_der_prim_xi = der2Poly.eval(x) ;
        G = p_der_xi * inv_p_xi ;
        H = G * G - p_der_prim_xi * inv_p_xi ;
        if (G > 0.)
        {
            a =
                polynomial_order
                    / (G
                        + std::sqrt(
                            (polynomial_order - 1)
                                * (polynomial_order * H - G * G))) ;
        }
        else
        {
            a =
                polynomial_order
                    / (G
                        - std::sqrt(
                            (polynomial_order - 1)
                                * (polynomial_order * H - G * G))) ;
        }
        if (BV::Math::IsNull(a, xTolerance))
        {
            xRes = x ;
            return true ;
        }
        x -= a ;
    }
    xRes = x ;
    return true ;
}

} // End of namespace R1ToR1
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_Laguerre_hpp
