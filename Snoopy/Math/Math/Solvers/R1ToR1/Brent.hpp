#ifndef _BRENT_HPP_
#define _BRENT_HPP_

#include "Math/Tools.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace R1ToR1 {

/*!
 \brief Brent method to find a zero of a function
 \param[in] fct Function to be evaluated
 \param[in] a Left boundary of an interval to search minimum in
 \param[in] b Right boundary of an interval to search minimum in
 \param[in] x_tolerance Absolute error of the value of the function minimum
 \param[in] fct_tolerance Function minimum value accepted
 \param[in] max_it Maximum number of iterations
 \param[out] x_res Result point
 \returns true if no solution is found. Else returns false.
 */
template <typename FCT>
bool Brent(const FCT & fct, double a, double b, const double & xTolerance,
           const double & fctTolerance, const unsigned int & nMaxIter,
           double & xRes, const bool & checkConvergence, double & XtoleranceRes)
{
    unsigned int iter(0) ;
    double fa, fb, fc, fs ;
    double c, s, temp ;
    double d = 0. ;
    bool mflag ;
    bool test ;
    fa = fct(a) ;
    iter++ ;
    if (BV::Math::IsNull(fa, fctTolerance))
    {
        xRes = a ;
        XtoleranceRes = 0. ;
        return false ;
    }
    fb = fct(b) ;
    iter++ ;
    if (BV::Math::IsNull(fb, fctTolerance))
    {
        xRes = b ;
        XtoleranceRes = 0. ;
        return false ;
    }
    if (fa * fb >= 0)
    {
        // Error: fa and fb have the same signus
        XtoleranceRes = b - a ;
        return true ;
    }
    if (std::abs(fa) < std::abs(fb))
    {
        BV::Math::switch_x1_x2(a, b) ;
        BV::Math::switch_x1_x2(fa, fb) ;
    }
    c = a ;
    fc = fa ;
    mflag = true ;
    for ( ; iter < nMaxIter; iter++)
    {
        if (BV::Math::IsNull(fb, fctTolerance))
        {
            xRes = b ;
            XtoleranceRes = 0. ;
            return false ;
        }
        if (BV::Math::IsClose(a, b, xTolerance))
        {
            // Error: x tolerance underpassed
            xRes = b ; // res is set (we shouldn't be too far from the solution)
            XtoleranceRes = b - a ;
            if (checkConvergence)
            {
                throw BV::Tools::Exceptions::BVException(
                    "Brent: delta X too small") ;
            }
            return true ;
        }
        if (!(BV::Math::IsClose(fa, fc)) && !(BV::Math::IsClose(fb, fc)))
        {
            // Inverse quadratic interpolation
            s = a * fb * fc / ((fa - fb) * (fa - fc))
                + b * fa * fc / ((fb - fa) * (fb - fc))
                + c * fa * fb / ((fc - fa) * (fc - fb)) ;
        }
        else
        {
            // Secant rule
            s = b - fb * (b - a) / (fb - fa) ;
        }
        temp = 0.25 * (3 * a + b) ;
        if (temp <= b)
        {
            test = !(s < b && s > temp) ;
        }
        else
        {
            test = !(s > b && s < temp) ;
        }
        if (test || (mflag && (std::abs(s - b) >= (std::abs(b - c) * 0.5)))
            || ((!mflag) && (std::abs(s - b) >= (std::abs(c - d) * 0.5)))
            || (mflag && BV::Math::IsNull(std::abs(b - c)))
            || ((!mflag) && BV::Math::IsNull(std::abs(c - d))))
        {
            s = (a + b) * 0.5 ; // bisection method
            mflag = true ;
        }
        else
        {
            mflag = false ;
        }
        fs = fct(s) ;
        d = c ;
        c = b ;
        fc = fb ;
        if (fa * fs < 0.)
        {
            b = s ;
            fb = fs ;
        }
        else
        {
            a = s ;
            fa = fs ;
        }
        if (std::abs(fa) < std::abs(fb))
        {
            BV::Math::switch_x1_x2(a, b) ;
            BV::Math::switch_x1_x2(fa, fb) ;
        }
    }
    // Convergence not reached
    xRes = b ; // res is set (we shouldn't be too far from the solution)
    XtoleranceRes = b - a ;
    if (checkConvergence)
    {
        throw BV::Tools::Exceptions::BVException(
            "Brent: last iteration reached") ;
    }
    return true ;
}

template <typename FCT>
bool Brent(const FCT & fct, double a, double b, double xTolerance,
           double fctTolerance, int max_it, double & x_res)
{
    double temp ;
    return Brent(fct, a, b, xTolerance, fctTolerance, max_it, x_res, false,
                 temp) ;
}
} // End of namespace R1ToR1
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV
#endif
