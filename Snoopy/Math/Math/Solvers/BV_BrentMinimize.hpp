#pragma once
#ifndef BV_Math_Solvers_BV_BrentMinimize_hpp
#define BV_Math_Solvers_BV_BrentMinimize_hpp

#include <functional>

#include "Math/Solvers/BrentABC.hpp"
#include "Math/Solvers/R1ToR1/BrentOptimize.hpp"
#include "Math/Solvers/SolverFactory.hpp"
#include "Math/Solvers/Types.hpp"

namespace BV {
namespace Math {
namespace Solvers {

template <typename Scalar=double>
class BV_BrentMinimize : public BrentABC<BV_BrentMinimize<Scalar>,Scalar>
{
protected:
    double fct_lower_threshold_ ;

public:

    BV_BrentMinimize(void) :
        BrentABC<BrentMinimize<Scalar>,Scalar>(),
        fct_lower_threshold_(0.)
    {
    }

    BV_BrentMinimize(const SolverParameters & params) :
        BrentABC<BrentMinimize<Scalar>,Scalar>(params),
        fct_lower_threshold_(params.get<Scalar>("lower_threshold", 0.))
    {
    }

    virtual ~BV_BrentMinimize(void)
    {
    }

    template <typename System, typename StateIn, typename StateOut,
              typename TType=double>
    void solve_impl(System system, const StateIn & state, StateOut & res,
                    const TType & t)
    {
        typedef typename std::remove_reference<System>::type SystemType ;
        typedef typename std::remove_reference<typename SystemType::first_type>::type FEvaluateType ;

        FEvaluateType & fEvaluate(system.first) ;
        this->reset_() ;
        this->Y_ = Solvers::R1ToR1::BrentOptimize(
                                     fEvaluate,
                                     a_, b_, this->absError_,
                                     fct_lower_threshold_,
                                     this->nMaxIter_, XMin_
                                              ) ;
        res = this->X_ ;
    }

} ;

template <typename Scalar>
struct SolverFactory<SolverType::BV_BRENT_MINIMIZE, Scalar>
{
    typedef BV_BrentMinimize<Scalar> Type ;
} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_BV_BrentMinimize_hpp
