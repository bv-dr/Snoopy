#include "Math/Solvers/SolverParameters.hpp"

namespace BV {
namespace Math {
namespace Solvers {

SolverParameters::SolverParameters(void)
{
}

void SolverParameters::set(const std::string & name, const double & val)
{
    nameToDouble_[name] = val ;
}

void SolverParameters::set(const std::string & name, const unsigned & val)
{
    nameToUnsigned_[name] = val ;
}

void SolverParameters::set(const std::string & name, const int & val)
{
    nameToInt_[name] = val ;
}

void SolverParameters::set(const std::string & name, const bool & val)
{
    nameToBool_[name] = val ;
}

void SolverParameters::set(const std::string & name, const std::string & val)
{
    nameToString_[name] = val ;
}

std::ostream & operator<<(std::ostream & os,
                          const SolverParameters & params)
{
    os << std::left ;
    for (const std::pair<const std::string, double> & item : params.nameToDouble_)
    {
        os << std::setw(20) << item.first << item.second << std::endl ;
    }
    for (const std::pair<const std::string, unsigned> & item : params.nameToUnsigned_)
    {
        os << std::setw(20) << item.first << item.second << std::endl ;
    }
    for (const std::pair<const std::string, int> & item : params.nameToInt_)
    {
        os << std::setw(20) << item.first << item.second << std::endl ;
    }
    for (const std::pair<const std::string, bool> & item : params.nameToBool_)
    {
        os << std::setw(20) << item.first << item.second << std::endl ;
    }
    for (const std::pair<const std::string, std::string> & item : params.nameToString_)
    {
        os << std::setw(20) << item.first << item.second << std::endl ;
    }
    return os ;
}

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV
