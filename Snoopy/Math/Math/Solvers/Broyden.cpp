#include "Math/Solvers/Broyden.hpp"

namespace BV {
namespace Math {
namespace Solvers {

using StateT = BV::Math::State ;
using StateDerT = BV::Math::StateDerivative ;

Broyden::Broyden(Eigen::Index xSize, Eigen::Index fSize, const BroydenParameters & params,
                 std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                                    Eigen::VectorXd & fOutput)> F,
                 std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                                    Eigen::MatrixXd & jOutput)> J) :
    xSize_(xSize), fSize_(fSize),
    params_(params), F_(F), fJ_(J),
    updateJacobian_(true),
    convergeFailures_(0), stallFailures_(0), nUpdateJ_(0),
    J_(Eigen::MatrixXd::Zero(fSize_, fSize_)),
    B_(Eigen::MatrixXd::Zero(fSize_, fSize_)),
    FX_(Eigen::VectorXd::Zero(fSize_)),
    FXP1_(Eigen::VectorXd::Zero(fSize_)),
    y_(Eigen::VectorXd::Zero(fSize_)),
    weights_(Eigen::VectorXd::Zero(fSize_)),
    //solver_(fSize_, fSize_),
    solver_(fSize_),
    usePreviousSolveB_(false)
{
}

void Broyden::reset()
{
    usePreviousSolveB_ = false ;
    updateJacobian_ = true ;
}

const BroydenInformation & Broyden::solve(Eigen::Ref<Eigen::VectorXd> xInOut) //,
                                          //std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                                          //                   Eigen::VectorXd & fOutput)> F,
                                          //std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                                          //                   Eigen::MatrixXd & jOutput)> fJ)
{
    return solve_<Eigen::VectorXd, Eigen::VectorXd, Eigen::Ref<Eigen::VectorXd> >(xInOut) ;//, F, fJ) ;
}

const BroydenInformation & Broyden::solve(StateT & xInOut) //,
                                          //std::function<void(const StateT & vals,
                                          //                   Eigen::VectorXd & fOutput)> F,
                                          //std::function<void(const StateT & vals,
                                          //                   Eigen::MatrixXd & jOutput)> fJ)
{
    return solve_<StateT, StateDerT, StateT&>(xInOut) ; //, F, fJ) ;
}

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV