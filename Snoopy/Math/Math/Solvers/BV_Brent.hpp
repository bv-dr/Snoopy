#pragma once
#ifndef BV_Math_Solvers_BV_Brent_hpp
#define BV_Math_Solvers_BV_Brent_hpp

#include <functional>

#include "Math/Solvers/BrentABC.hpp"
#include "Math/Solvers/R1ToR1/Brent.hpp"
#include "Math/Solvers/SolverFactory.hpp"
#include "Math/Solvers/Types.hpp"

namespace BV {
namespace Math {
namespace Solvers {

template <typename Scalar=double>
class BV_Brent : public BrentABC<BV_Brent<Scalar>,Scalar>
{
protected:
    double fct_tolerance_ ;

public:

    BV_Brent(void) :
        BrentABC<BV_Brent<Scalar>,Scalar>(),
        fct_tolerance_(1.e-4)
    {
    }

    BV_Brent(const SolverParameters & params) :
        BrentABC<BV_Brent<Scalar>,Scalar>(params),
        fct_tolerance_(params.get<Scalar>("fct_tolerance", 1.e-4))
    {
    }

    virtual ~BV_Brent(void)
    {
    }

    template <typename System, typename StateIn, typename StateOut,
              typename TType=double>
    void solve_impl(System system, const StateIn & state, StateOut & res,
                    const TType & t)
    {
        typedef typename std::remove_reference<System>::type SystemType ;
        typedef typename std::remove_reference<typename SystemType::first_type>::type FEvaluateType ;

        FEvaluateType & fEvaluate(system.first) ;
        this->reset_() ;
        this->hasConverged_ = Solvers::R1ToR1::Brent(
                                     fEvaluate,
                                     this->a_, this->b_, this->absError_,
                                     fct_tolerance_,
                                     this->nMaxIter_, this->X_
                                              ) ;
        this->Y_ = fEvaluate(this->X_) ;
        res = this->X_ ;
    }

} ;

template <typename Scalar>
struct SolverFactory<SolverType::BV_BRENT, Scalar>
{
    typedef BV_Brent<Scalar> Type ;
} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_BV_Brent_hpp
