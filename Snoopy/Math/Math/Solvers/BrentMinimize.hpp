#pragma once
#ifndef BV_Math_Solvers_BrentMinimize_hpp
#define BV_Math_Solvers_BrentMinimize_hpp

#include <functional>
#include <boost/math/tools/minima.hpp>

#include "Math/Solvers/BrentABC.hpp"
#include "Math/Solvers/SolverFactory.hpp"
#include "Math/Solvers/Types.hpp"

namespace BV {
namespace Math {
namespace Solvers {

template <typename Scalar=double>
class BrentMinimize : public BrentABC<BrentMinimize<Scalar>,Scalar>
{
public:

    BrentMinimize(void) :
        BrentABC<BrentMinimize<Scalar>,Scalar>()
    {
    }

    BrentMinimize(const SolverParameters & params) :
        BrentABC<BrentMinimize<Scalar>,Scalar>(params)
    {
    }

    virtual ~BrentMinimize(void)
    {
    }

    template <typename System, typename StateIn, typename StateOut,
              typename TType=double>
    void solve_impl(System system, const StateIn & state, StateOut & res,
                    const TType & t)
    {
        typedef typename std::remove_reference<System>::type SystemType ;
        typedef typename std::remove_reference<typename SystemType::first_type>::type FEvaluateType ;

        FEvaluateType & fEvaluate(system.first) ;
        this->reset_() ;
        unsigned bits(std::numeric_limits<double>::digits) ;
        boost::uintmax_t maxit(this->nMaxIter_) ;
        std::pair<StateIn, StateOut> r(boost::math::tools::brent_find_minima(
                                            fEvaluate, this->a_, this->b_, bits, maxit
                                                                             )
                                      ) ;
        this->X_ = r.first ;
        this->Y_ = r.second ;
        res = this->X_ ;
    }

} ;

template <typename Scalar>
struct SolverFactory<SolverType::BRENT_MINIMIZE, Scalar>
{
    typedef BrentMinimize<Scalar> Type ;
} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_BrentMinimize_hpp
