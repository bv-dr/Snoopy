#pragma once
#ifndef BV_Math_Solvers_SolverFactory_hpp
#define BV_Math_Solvers_SolverFactory_hpp

namespace BV {
namespace Math {
namespace Solvers {

template <int SolverName, typename Scalar=double>
struct SolverFactory ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_SolverFactory_hpp_
