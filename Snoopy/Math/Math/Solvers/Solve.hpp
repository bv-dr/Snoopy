#pragma once
#ifndef BV_Math_Solvers_Solver_hpp
#define BV_Math_Solvers_Solver_hpp

#include <tuple>
#include <type_traits>

#include "Math/Solvers/Newton.hpp"
#include "Math/Solvers/SolverFactory.hpp"
#include "Math/Solvers/SolverParameters.hpp"

namespace BV {
namespace Math {
namespace Solvers {

template <int SolverType, typename Scalar=double>
class Solver
{
private:
    typedef typename SolverFactory<SolverType, Scalar>::Type Solver_ ;
    Solver_ solver_ ;

public:
    Solver(const SolverParameters & params) : solver_(params)
    {
    }

    template <typename System, typename State, typename TType=Scalar>
    void solve(System system, const State & state, State & res, TType t=0.)
    {
        solver_.solve(system, state, res, t) ;
    }

    bool hasConverged(void) const
    {
        return solver_.hasConverged() ;
    }

    unsigned getNIterations(void) const
    {
        return solver_.getNIterations() ;
    }

} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_Solve_hpp_
