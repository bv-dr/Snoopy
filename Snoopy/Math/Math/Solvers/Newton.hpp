#pragma once
#ifndef BV_Math_Solvers_Newton_hpp
#define BV_Math_Solvers_Newton_hpp

#include <functional>
#include <exception>

#include "Math/Solvers/SolverABC.hpp"
#include "Math/Solvers/SolverFactory.hpp"
#include "Math/Solvers/SolverParameters.hpp"
#include "Math/Solvers/Types.hpp"
#include "Math/Tools.hpp"

namespace BV {
namespace Math {
namespace Solvers {

// FIXME Newton type also depends on J inversion scheme (LU, QR, SVD...)

class CalculationFailed : public std::exception
{
protected:
    std::string message_ ;
public:
    CalculationFailed(std::string message) :
        message_(message)
    {
    }

    const char* what() const noexcept
    {
        return message_.c_str() ;
    }

    std::string getMessage() const noexcept
    {
        return message_ ;
    }
} ;

template <typename Scalar=double>
class Newton : public SolverABC<Newton<Scalar>,Scalar>
{
public:

    Newton(void) :
        SolverABC<Newton<Scalar>,Scalar>()
    {
    }

    Newton(const SolverParameters & params) :
        SolverABC<Newton<Scalar>,Scalar>(params)
    {
    }

    virtual ~Newton(void)
    {
    }

    template <typename System, typename StateIn, typename StateOut,
              typename TType=double>
    void solve_impl(System system, const StateIn & state, StateOut & res,
                    const TType & t)
    {
        typedef typename std::remove_reference<System>::type SystemType ;
        typedef typename std::remove_reference<typename SystemType::first_type>::type FEvaluateType ;
        typedef typename std::remove_reference<typename SystemType::second_type>::type FDEvaluateType ;
        //typedef typename Details::JTypeHelper<StateIn>::Type JType ;
        typedef typename FDEvaluateType::JType JType ;

        FEvaluateType & fEvaluate(system.first) ;
        FDEvaluateType & fDEvaluate(system.second) ;

        this->reset_() ;

        StateIn Xn(state) ;
        StateIn b(Details::TypeTraits<StateIn>::getZero(state)) ;
        JType J(fDEvaluate.getJInitialized(state)) ; // FIXME this should be done only once... as it may be time consuming
        //JType J(Details::TypeTraits<StateIn>::getJInitialized(state)) ;
        fEvaluate(Xn, b, t) ;
        fDEvaluate(Xn, J, t) ;
        StateIn X(Details::Jm1B<JType, StateIn>::get(J, b)) ;
        res = Xn - X ;

        if (this->verbose_)
        {
            Details::Writer<StateIn, JType>::write(this->nIter_, Xn, b, J, X, res) ;
        }

        while ((!Details::IsLower<StateIn>::check(b, this->absError_))
               && (this->nIter_ < this->nMaxIter_))
        {
            this->nIter_ += 1 ;
            Xn = res ;
            fEvaluate(Xn, b, t) ;
            if ((!this->computeJOnce_) || (this->nIter_ > (this->nMaxIter_/2))) // FIXME set as parameter
            {
                fDEvaluate(Xn, J, t) ;
            }
            X = Details::Jm1B<JType, StateIn>::get(J, b) ;
            res = Xn - X ;

            if (this->verbose_)
            {
                Details::Writer<StateIn, JType>::write(this->nIter_, Xn, b, J, X, res) ;
            }
            if (!BV::Math::IsFinite(X))
            {
                this->hasConverged_ = false ;
                if (this->verbose_)
                {
                    Details::Writer<StateIn, JType>::write(b) ;
                    std::cout << "Newton optimisation failed after "
                              << this->nMaxIter_
                              << " iterations"
                              << std::endl ;
                }
                res = state ;
                //return ;
                throw CalculationFailed("Calculation failed in Newton solver") ;
            }
        }
        //std::cout << "n iterations: " << this->nIter_ << std::endl ;
        if (!Details::IsLower<StateIn>::check(b, this->absError_))
        {
            if (this->verbose_)
            {
                Details::Writer<StateIn, JType>::write(b) ;
                std::cout << "Newton optimisation failed after "
                          << this->nMaxIter_
                          << " iterations"
                          << std::endl ;
            }
            res = state ; // FIXME
            this->hasConverged_ = false ;
        }
        else
        {
            if (this->verbose_)
            {
                std::cout << std::endl
                          << "Solution found in " << this->nIter_+1 << " iterations"
                          << std::endl ;
            }
            this->hasConverged_ = true ;
        }
    }

} ;

template <typename Scalar>
struct SolverFactory<SolverType::NEWTON, Scalar>
{
    typedef Newton<Scalar> Type ;
} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

// TODO remove all this !
//#include <functional>
//#include <Eigen/Dense>
//#include <cmath>
//
//#include "Math/Solvers/SolverABC.hpp"
//
//
//
//namespace BV {
//namespace Math {
//namespace Solvers {
//
//
//class Newton : public SolverABC
//{
//private:
//    double absoluteEps_ ;
//    unsigned nMaxIter_ ;
//public:
//    Newton(fEvaluateType fEvaluate,
//           fDEvaluateType fDEvaluate,
//           double absoluteEps=1.e-4,
//           unsigned nMaxIter=20) :
//        SolverABC(fEvaluate, fDEvaluate),
//        absoluteEps_(absoluteEps), nMaxIter_(nMaxIter)
//    {
//    }
//
//    Newton(double absoluteEps=1.e-4, unsigned nMaxIter=20) :
//        SolverABC(), absoluteEps_(absoluteEps), nMaxIter_(nMaxIter)
//    {
//    }
//
//    bool checkConverged_(const Eigen::VectorXd & X) const
//    {
//        // FIXME find a better way to check this (no loop!)
//        for (unsigned i=0; i<X.rows(); ++i)
//        {
//            if (std::abs(X(i)) > absoluteEps_)
//            {
//                // Not converged
//                return false ;
//            }
//        }
//        // It has converged
//        return true ;
//    }
//
//    template <typename MatrixType>
//    Eigen::VectorXd solveBlock_(const MatrixType & J, const Eigen::VectorXd & b)
//    {
//        return J.partialPivLu().solve(b) ;
//    }
//
//    //template <>
//    //Eigen::VectorXd solveBlock_<Tools::BlockMatrix<Tools::MatrixRowMajor> >(
//    //                       const Tools::BlockMatrix<Tools::MatrixRowMajor> & J,
//    //                       const Eigen::VectorXd & b
//    //                                                                  )
//    //{
//    //    Eigen::VectorXd X(b.rows()) ;
//    //    unsigned i(0), nRows ;
//    //    for (const Tools::MatrixRowMajor & block : J.blocks())
//    //    {
//    //        nRows = block.rows() ;
//    //        X.segment(i, nRows) = block.partialPivLu().solve(b.segment(i, nRows)) ;
//    //        i += nRows ;
//    //    }
//    //    return X ;
//    //}
//
//    template <typename JType=Eigen::MatrixXd>
//    Eigen::VectorXd solve_(const Eigen::VectorXd & X0, bool verbose=false)
//    {
//        hasConverged_ = false ;
//        if ((!hasfEvaluate_) || (!hasfDEvaluate_))
//        {
//            // TODO if only fDEvaluate_ is missing, we could do
//            // a basis finite difference...
//            throw BV::Tools::Exceptions::InitialisationException("Newton not initialised") ;
//        }
//        kiter_ = 0 ;
//        Eigen::VectorXd Xn(X0) ;
//        Eigen::VectorXd b(fEvaluate_(Xn)) ;
//        JType J(fDEvaluate_(Xn)) ;
//        Eigen::VectorXd X(solveBlock_(J, b)) ;
//        Eigen::VectorXd Xnp1(Xn - X) ;
//        std::cout << std::scientific ;
//        Eigen::IOFormat fmt(Eigen::StreamPrecision, 0, "    ", "\n", "", "", "", "") ;
//        if (verbose)
//        {
//            std::cout << std::endl ;
//            std::cout << "iter: " << kiter_ << std::endl ;
//            std::cout << "X: " << std::endl << Xn.transpose().format(fmt) << std::endl ;
//            std::cout << "b: " << std::endl << b.transpose().format(fmt) << std::endl ;
//            std::cout << "J: " << std::endl << J.format(fmt) << std::endl ;
//            std::cout << "sol: " << std::endl << X.transpose().format(fmt) << std::endl ;
//            std::cout << "Xnp1: " << std::endl << Xnp1.transpose().format(fmt) << std::endl ;
//        }
//        while ((! checkConverged_(b)) && (kiter_ < nMaxIter_))
//        {
//            kiter_ += 1 ;
//            Xn = Xnp1 ;
//            b = fEvaluate_(Xn) ;
//            J = fDEvaluate_(Xn) ;
//            X = solveBlock_(J, b) ;
//            Xnp1 = Xn - X ;
//            if (verbose)
//            {
//                std::cout << std::endl ;
//                std::cout << "iter: " << kiter_ << std::endl ;
//                std::cout << "X: " << std::endl << Xn.transpose().format(fmt) << std::endl ;
//                std::cout << "b: " << std::endl << b.transpose().format(fmt) << std::endl ;
//                std::cout << "J: " << std::endl << J.format(fmt) << std::endl ;
//                std::cout << "sol: " << std::endl << X.transpose().format(fmt) << std::endl ;
//                std::cout << "Xnp1: " << std::endl << Xnp1.transpose().format(fmt) << std::endl ;
//            }
//        }
//        if (! checkConverged_(b))
//        {
//            std::cout << b.transpose() << std::endl ;
//            std::cout << "Newton optimisation failed after "
//                      << nMaxIter_
//                      << " iterations"
//                      << std::endl ;
//            //return X0 ; // FIXME
//        }
//        else
//        {
//            if (verbose)
//            {
//                std::cout << std::endl << "Solution found in " << kiter_+1 << " iterations" << std::endl ;
//            }
//            hasConverged_ = true ;
//        }
//        return Xnp1 ;
//    }
//
//    virtual Eigen::VectorXd solve(const Eigen::VectorXd & X0,
//                                  bool verbose=false)
//    {
//        return solve_(X0, verbose) ;
//    }
//
//} ;
//
//} // End of namespace Solvers
//} // End of namespace Math
//} // End of namespace BV

#endif // BV_Math_Solvers_Newton_hpp
