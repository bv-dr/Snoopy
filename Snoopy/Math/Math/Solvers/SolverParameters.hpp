#pragma once
#ifndef BV_Math_Solvers_SolverParameters_hpp
#define BV_Math_Solvers_SolverParameters_hpp

#include "SolversExport.hpp"

#include <map>
#include <iostream>
#include <iomanip>

#include "Tools/BVException.hpp"

namespace BV {
namespace Math {
namespace Solvers {

class SOLVERS_API SolverParameters
{
private:
    std::map<std::string, double> nameToDouble_ ;
    std::map<std::string, unsigned> nameToUnsigned_ ;
    std::map<std::string, int> nameToInt_ ;
    std::map<std::string, bool> nameToBool_ ;
    std::map<std::string, std::string> nameToString_ ;
public:
    SolverParameters(void) ;

    void set(const std::string & name, const double & val) ;
    void set(const std::string & name, const unsigned & val) ;
    void set(const std::string & name, const int & val) ;
    void set(const std::string & name, const bool & val) ;
    void set(const std::string & name, const std::string & val) ;

    template <typename T>
    T get(const std::string & name) const
    {
        throw BV::Tools::Exceptions::BVException(
                              "Solver parameter not found: " + name
                                                ) ;
    }

    template <typename T>
    T get(const std::string & name, const T & defaults) const
    {
        try
        {
            return get<T>(name) ;
        }
        catch (...)
        {
        }
        return defaults ;
    }

    friend SOLVERS_API std::ostream & operator<<(std::ostream & os,
                                                 const SolverParameters & params) ;
} ;

// specializations
template <>
inline SOLVERS_API double SolverParameters::get<double>(const std::string & name) const
{
    return nameToDouble_.at(name) ;
}

template <>
inline SOLVERS_API unsigned SolverParameters::get<unsigned>(const std::string & name) const
{
    return nameToUnsigned_.at(name) ;
}

template <>
inline SOLVERS_API int SolverParameters::get<int>(const std::string & name) const
{
    return nameToInt_.at(name) ;
}

template <>
inline SOLVERS_API bool SolverParameters::get<bool>(const std::string & name) const
{
    return nameToBool_.at(name) ;
}

template <>
inline SOLVERS_API std::string SolverParameters::get<std::string>(const std::string & name) const
{
    return nameToString_.at(name) ;
}

SOLVERS_API std::ostream & operator<<(std::ostream & os,
                          const SolverParameters & params) ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_SolverParameters_hpp
