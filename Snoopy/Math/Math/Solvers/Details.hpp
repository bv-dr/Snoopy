#ifndef BV_Math_Solvers_Details_hpp
#define BV_Math_Solvers_Details_hpp

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>
#include <type_traits>
#include <functional>
#include <utility>
#include <cmath>

#include "Math/FiniteDifference/FiniteDifference.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace Details {

namespace TypeCheck {

template <template<class> class BaseType, typename T>
std::true_type IsEigen(const BaseType<T>*) ;
template <template<class> class BaseType>
std::false_type IsEigen(...) ;

} // End of namespace TypeCheck

template <typename T>
using GetType = std::remove_cv<typename std::remove_reference<T>::type> ;

template <typename T, template<class> class BaseType>
struct IsEigen : decltype(TypeCheck::IsEigen<BaseType>(std::declval<typename GetType<T>::type*>()))
{
} ;

template <typename T>
struct IsPair
{
    static const bool value = false ;
} ;

template <typename A, typename B>
struct IsPair<std::pair<A, B> >
{
    static const bool value = true ;
} ;

template <typename State, class Enable=void>
struct TypeTraits ;

template <typename State>
struct TypeTraits<State, typename std::enable_if<std::is_scalar<State>::value>::type>
{
    typedef State CopyType ;

    static State getZero(const State & x)
    {
        return static_cast<State>(0) ;
    }

} ;

template <typename State>
struct TypeTraits<State,
                  typename std::enable_if<IsEigen<State, Eigen::EigenBase>::value>::type>
{
    typedef Eigen::Matrix<typename State::Scalar,
                          State::RowsAtCompileTime,
                          State::ColsAtCompileTime> CopyType ;

    static CopyType getZero(const State & x)
    {
        return CopyType::Zero(x.size()) ;
    }

} ;

template <typename JType, typename BType, class Enable=void>
struct Jm1B ;

template <typename JType, typename BType>
struct Jm1B<JType, BType, typename std::enable_if<std::is_scalar<JType>::value>::type>
{
    static BType get(const JType & J, const BType & b)
    {
        return b / J ;
    }
} ;

template <typename JType, typename BType>
struct Jm1B<JType, BType,
            typename std::enable_if<IsEigen<JType, Eigen::DenseBase>::value
                                    && IsEigen<BType, Eigen::EigenBase>::value>::type>
{
    static BType get(const JType & J, const BType & b)
    {
        Eigen::ColPivHouseholderQR<Eigen::MatrixXd> qrDecomposition(J) ;
        qrDecomposition.setThreshold(1.e-6) ;
        return qrDecomposition.solve(b) ;
        //return J.partialPivLu().solve(b) ;
    }

} ;

template <typename JType, typename BType>
struct Jm1B<JType, BType,
            typename std::enable_if<IsEigen<JType, Eigen::SparseMatrixBase>::value
                                    && IsEigen<BType, Eigen::EigenBase>::value>::type>
{
    static BType get(const JType & J, const BType & b)
    {
        Eigen::SparseQR<JType, Eigen::COLAMDOrdering<int> > solver ;
        //Eigen::SparseLU<JType, Eigen::COLAMDOrdering<int> > solver ;
        //Eigen::IncompleteLUT<typename JType::Scalar> solver ;
        //Eigen::LeastSquaresConjugateGradient<JType> solver ;
        solver.compute(J) ;
        if (solver.info() != Eigen::Success)
        {
            std::cout << "Decomposition failed" << std::endl ;
            throw "Decomposition failed !" ;
        }
        return solver.solve(b) ;
    }
} ;

template <typename State, class Enable=void>
struct IsLower ;

template <typename State>
struct IsLower<State, typename std::enable_if<std::is_scalar<State>::value>::type>
{
    template <typename Tol>
    static bool check(const State & X, const Tol & tol)
    {
        return std::abs(X) < tol ;
    }

} ;

template <typename State>
struct IsLower<State,
               typename std::enable_if<IsEigen<State, Eigen::EigenBase>::value>::type>
{
    template <typename Tol>
    static bool check(const State & X, const Tol & tol)
    {
        return (X.array().abs() < tol).all() ;
    }
} ;

template <typename State, typename JType, class Enable=void>
struct Writer ;

template <typename State, typename JType>
struct Writer<State, JType, typename std::enable_if<std::is_scalar<State>::value>::type>
{
    static void write(const unsigned & iIter, const State & Xn, const State & b,
                      const JType & J, const State & X, const State & Xnp1)
    {
        std::cout << std::scientific ;
        std::cout << std::endl ;
        std::cout << "iter: " << iIter << std::endl ;
        std::cout << "X: " << std::endl << Xn << std::endl ;
        std::cout << "b: " << std::endl << b << std::endl ;
        std::cout << "J: " << std::endl << J << std::endl ;
        std::cout << "sol: " << std::endl << X << std::endl ;
        std::cout << "Xnp1: " << std::endl << Xnp1 << std::endl ;
    }

    static void write(const State & b)
    {
        std::cout << b << std::endl ;
    }

} ;

template <typename State, typename JType>
struct Writer<State, JType,
              typename std::enable_if<IsEigen<State, Eigen::EigenBase>::value
                                      && IsEigen<JType, Eigen::DenseBase>::value>::type>
{
    static void write(const unsigned & iIter, const State & Xn, const State & b,
                      const JType & J, const State & X, const State & Xnp1)
    {
        std::cout << std::scientific ;
        Eigen::IOFormat fmt(Eigen::StreamPrecision, 0, "    ", "\n",
                            "", "", "", "") ;
        std::cout << std::endl ;
        std::cout << "iter: " << iIter << std::endl ;
        std::cout << "X: " << std::endl << Xn.transpose().format(fmt) << std::endl ;
        std::cout << "b: " << std::endl << b.transpose().format(fmt) << std::endl ;
        //std::cout << "J: " << std::endl << J.format(fmt) << std::endl ;
        //Eigen::JacobiSVD<JType> svd(J);
        //typename JType::Scalar cond(svd.singularValues()(0) / svd.singularValues()(svd.singularValues().size()-1)) ;
        //std::cout << "Jcond = " << cond << std::endl ;
        std::cout << "sol: " << std::endl << X.transpose().format(fmt) << std::endl ;
        std::cout << "Xnp1: " << std::endl << Xnp1.transpose().format(fmt) << std::endl ;
    }

    static void write(const State & b)
    {
        std::cout << b.transpose() << std::endl ;
    }

} ;

template <typename State, typename JType>
struct Writer<State, JType,
              typename std::enable_if<IsEigen<State, Eigen::EigenBase>::value
                                      && IsEigen<JType, Eigen::SparseMatrixBase>::value>::type>
{
    static void write(const unsigned & iIter, const State & Xn, const State & b,
                      const JType & J, const State & X, const State & Xnp1)
    {
        std::cout << std::scientific ;
        Eigen::IOFormat fmt(Eigen::StreamPrecision, 0, "    ", "\n",
                            "", "", "", "") ;
        std::cout << std::endl ;
        std::cout << "iter: " << iIter << std::endl ;
        std::cout << "X: " << std::endl << Xn.transpose().format(fmt) << std::endl ;
        std::cout << "b: " << std::endl << b.transpose().format(fmt) << std::endl ;
        std::cout << "J: " << std::endl << J << std::endl ;
        //Eigen::JacobiSVD<JType> svd(J);
        //typename JType::Scalar cond(svd.singularValues()(0) / svd.singularValues()(svd.singularValues().size()-1)) ;
        //std::cout << "Jcond = " << cond << std::endl ;
        std::cout << "sol: " << std::endl << X.transpose().format(fmt) << std::endl ;
        std::cout << "Xnp1: " << std::endl << Xnp1.transpose().format(fmt) << std::endl ;
    }

    static void write(const State & b)
    {
        std::cout << b.transpose() << std::endl ;
    }

} ;

template <typename State, class Enable=void>
struct JTypeHelper ;

template <typename State>
struct JTypeHelper<State, typename std::enable_if<std::is_scalar<State>::value>::type>
{
    typedef State Type ;

    static State getJInitialized(const State & x)
    {
        return static_cast<State>(0) ;
    }
} ;

template <typename State>
struct JTypeHelper<State,
                   typename std::enable_if<IsEigen<State, Eigen::EigenBase>::value>::type>
{
    typedef Eigen::Matrix<typename State::Scalar,
                          State::RowsAtCompileTime,
                          State::RowsAtCompileTime> Type ;

    static Type getJInitialized(const State & x)
    {
        std::size_t size(x.size()) ;
        return Type::Zero(size, size) ;
    }
} ;

template <typename System, typename TType=double>
class SystemFDWrapper
{
private:
    System & system_ ;
    TType t_ ;
public:
    SystemFDWrapper(System & system, const TType & t) :
        system_(system), t_(t)
    {
    }

    // FIXME finite difference functor has the following signature...
    // See if we need another one.
    template <typename State>
    auto get(const State & x) -> typename TypeTraits<State>::CopyType
    {
        typename TypeTraits<State>::CopyType res(TypeTraits<State>::getZero(x)) ;
        system_(x, res, t_) ;
        return res ;
    }
} ;

namespace FD = BV::Math::FiniteDifference ;

template <typename System, typename State, typename Scalar=double>
struct FDSystem
{
private:
    typedef typename std::remove_reference<System>::type SystemType ;
    SystemType & system_ ;
    Scalar delta_ ;

public:

    typedef typename JTypeHelper<State>::Type JType ;

    template <typename StateIn>
    static JType getJInitialized(const StateIn & state)
    {
        return JTypeHelper<State>::getJInitialized(state) ;
    }

    FDSystem(SystemType & system, Scalar delta) :
        system_(system), delta_(delta)
    {
    }

    template <typename StateIn, typename JTypeIn, typename TType=double>
    void operator()(const StateIn & x, JTypeIn & J, TType t=0.)
    {
        typedef SystemFDWrapper<SystemType, TType> Wrapper ;
        Wrapper wrapper(system_, t) ;
        // FIXME FD type, order...
        typedef FD::FiniteDifference<FD::FDScheme::CENTRAL,
                                     FD::IthDerivative::FIRST, 2,
                                     FD::FDContext::JACOBIAN_CALCULATION> FDType ;
        // FIXME we copy x to make sure of its type...
        typedef typename TypeTraits<State>::CopyType CopyType ;
        std::function<CopyType(const CopyType &)> f(
                            std::bind(&Wrapper::template get<CopyType>, wrapper,
                                      std::placeholders::_1)
                                                   ) ;
        typename TypeTraits<State>::CopyType tmp(x) ;
        J = FDType::get(tmp, f, delta_) ;
    }

} ;


} // End of namespace Details
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_Details_hpp
