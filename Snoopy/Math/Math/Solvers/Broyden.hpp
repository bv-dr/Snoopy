#pragma once

#include <functional>
#include <cmath>
#include <Eigen/Dense>

#include "SolversExport.hpp"

#include "Math/State.hpp"

namespace BV {
namespace Math {
namespace Solvers {

enum class BroydenType {GOOD_WITH_INV, GOOD, BAD} ;

struct BroydenParameters
{
    std::size_t maxIterations = 100 ;
    double aTol = 1.e-8 ;
    double rTol = 1.e-4 ;
    double divergeLimit = 2. ;
    double convergeLimit = 1. ;
    std::size_t maxConvergeFailures = 3 ;
    BroydenType type = BroydenType::BAD ;
} ;

struct BroydenInformation
{
    bool success = false ;
    std::size_t nIterations = 0 ;
    std::string errorMessage ;
    std::size_t nUpdateJ = 0 ;
    double error ;
} ;

class SOLVERS_API Broyden
{
private:
    Eigen::Index xSize_ ;
    Eigen::Index fSize_ ;
    BroydenParameters params_ ;
    std::function<void(const Eigen::Ref<const Eigen::VectorXd> &,
                        Eigen::VectorXd &)> F_ ;
    std::function<void(const Eigen::Ref<const Eigen::VectorXd> &,
                        Eigen::MatrixXd &)> fJ_ ;
    BroydenInformation info_ ;
    bool updateJacobian_ ;
    std::size_t convergeFailures_ ;
    std::size_t stallFailures_ ;
    std::size_t nUpdateJ_ ;
    Eigen::MatrixXd J_ ;
    Eigen::MatrixXd B_ ;
    Eigen::VectorXd FX_ ;
    Eigen::VectorXd FXP1_ ;
    Eigen::VectorXd y_ ;
    Eigen::VectorXd weights_ ;
    //Eigen::ColPivHouseholderQR<Eigen::MatrixXd> solver_ ;
    Eigen::PartialPivLU<Eigen::MatrixXd> solver_ ;
    bool usePreviousSolveB_ ;

    template <typename InputType, typename InputIncrType,
              typename std::enable_if<!std::is_same<InputType, BV::Math::State>::value, bool>::type = true>
    void setDiffAsInputIncrType_(const InputType & y1, const InputType & y2,
                                 InputIncrType & diff)
    {
        diff = y1 - y2 ;
    }

    template <typename InputType, typename InputIncrType,
              typename std::enable_if<std::is_same<InputType, BV::Math::State>::value, bool>::type = true>
    void setDiffAsInputIncrType_(const InputType & y1, const InputType & y2,
                                 InputIncrType & diff)
    {
        BV::Math::SetDiffAsStateDerivative(y1, y2, diff) ;
    }

    template <typename InputType, typename InputIncrType, typename InputTypeRef>
    const BroydenInformation & solve_(InputTypeRef xInOut)//,
                                      //std::function<void(const InputType & vals,
                                      //                   Eigen::VectorXd & fOutput)> F,
                                      //std::function<void(const InputType & vals,
                                      //                   Eigen::MatrixXd & jOutput)> fJ)
    {
        InputType xP1(xSize_) ;
        InputIncrType d(InputIncrType::Zero(fSize_)) ;
        InputIncrType s(fSize_) ;

        InputType x(xInOut) ;

        F_(x, FX_) ;
        double norm(FX_.cwiseAbs().norm()) ;
        double normP1(norm) ;

        // TODO add a relative norm computation on solution vector? That may mess with State vectors for rotations...
        // TODO optional linesearch algorithm. See OpenMDAO or scipy for examples.

        if (norm <= params_.aTol)
        {
            info_.success = true ;
            info_.nIterations = 0 ;
            return info_ ;
        }

        if (!usePreviousSolveB_)
        {
            fJ_(x, J_) ;
            nUpdateJ_ = 1 ;
            solver_.compute(J_) ;
            B_ = solver_.inverse() ;
            usePreviousSolveB_ = true ;
        }
        else
        {
            nUpdateJ_ = 0 ;
        }

        double convergeRatio ;
        updateJacobian_ = false ;
        convergeFailures_ = 0 ;
        stallFailures_ = 0 ;

        bool converged(false) ;
        double error(0.) ;
        // FIXME below won't work if x doesn't have the same size as f...
        weights_ = (params_.rTol * x.cwiseAbs() + Eigen::VectorXd::Constant(xSize_, params_.aTol)).cwiseInverse() ;
        std::size_t it(0) ;

        while (it < params_.maxIterations)
        {
            ++it ;
            d = -B_ * FX_ ;
            xP1 = x + d ;

            F_(xP1, FXP1_) ;

            normP1 = FXP1_.norm() ;
            error = Eigen::numext::sqrt(d.cwiseProduct(weights_).cwiseAbs2().sum() / fSize_) ;
            //std::cout << "error: " << error << " norm: " << normP1 << " it: " << it << " nJ: " << nUpdateJ_ << std::endl ;
            //if ((normP1 < params_.aTol) && ((normP1 / norm0) < params_.rTol))
            if ((normP1 < params_.aTol) || ((error < 1.) && (normP1 < (10.*params_.aTol))))
            {
                std::cout << "error: " << error << " norm: " << normP1 << " it: " << it << " nJ: " << nUpdateJ_ << std::endl ;
                converged = true ;
                norm = normP1 ;
                break ;
            }


            if (updateJacobian_)
            {
                fJ_(xP1, J_) ;
                solver_.compute(J_) ;
                B_ = solver_.inverse() ;
                ++nUpdateJ_ ;
            }
            else
            {
                setDiffAsInputIncrType_(xP1, x, s) ;
                y_ = FXP1_ - FX_ ;
                if (params_.type == BroydenType::GOOD_WITH_INV)
                {
                    J_ += (y_ - J_ * s) * s.transpose() / (s.dot(s) + 1.e-14) ;
                    solver_.compute(J_) ;
                    B_ = solver_.inverse() ;
                }
                else if (params_.type == BroydenType::GOOD)
                {
                    B_ += (s - B_ * y_) * (s.transpose() * B_) / (s.transpose() * B_ * y_ + 1.e-14) ;
                }
                else if (params_.type == BroydenType::BAD)
                {
                    B_ += (s - B_ * y_) * y_.transpose() / (y_.dot(y_) + 1.e-14) ;
                }
            }
            // Determine whether to update Jacobian
            updateJacobian_ = false ;
            //if (normP1 < (2. * params_.aTol))
            //{
            //    convergeRatio = 1. ;
            //}
            //else
            //{
                convergeRatio = norm / normP1 ;
            //}
            if ((convergeRatio > params_.divergeLimit) || ((normP1 > 1.e2) && (!nUpdateJ_)))
            {
                updateJacobian_ = true ;
                convergeFailures_ = 0 ;
                stallFailures_ = 0 ;
            }
            else if (convergeRatio >= params_.convergeLimit)
            {
                ++convergeFailures_ ;
                if (convergeFailures_ >= params_.maxConvergeFailures)
                {
                    updateJacobian_ = true ;
                    convergeFailures_ = 0 ;
                    stallFailures_ = 0 ;
                }
            }
            else if (stallFailures_ >= params_.maxConvergeFailures)
            {
                    updateJacobian_ = true ;
                    convergeFailures_ = 0 ;
                    stallFailures_ = 0 ;
            }
            else
            {
                ++stallFailures_ ;
            }

            x = xP1 ;
            FX_ = FXP1_ ;
            norm = normP1 ;
        }

        info_.nIterations = it ;
        info_.nUpdateJ = nUpdateJ_ ;
        info_.error = error ;
        if (converged)
        {
            info_.success = true ;
            xInOut = xP1 ;
            info_.errorMessage = "" ;
        }
        else if (!std::isfinite(norm))
        {
            reset() ;
            info_.success = false ;
            info_.errorMessage = "Non-finite values encountered in Broyden solver" ;
        }
        else // (it >= params_.maxIterations)
        {
            reset() ;
            info_.success = false ;
            info_.errorMessage = "Maximum number of iterations reached" ;
        }
        return info_ ;
    }

public:
    Broyden(Eigen::Index xSize, Eigen::Index fSize,
            const BroydenParameters & params=BroydenParameters(),
            std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                               Eigen::VectorXd & fOutput)> F=nullptr,
            std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                               Eigen::MatrixXd & jOutput)> J=nullptr) ;

    void reset() ;

    const BroydenInformation & solve(Eigen::Ref<Eigen::VectorXd> x) ;//,
                                     //std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                                     //                   Eigen::VectorXd & fOutput)> F=nullptr,
                                     //std::function<void(const Eigen::Ref<const Eigen::VectorXd> & vals,
                                     //                   Eigen::MatrixXd & jOutput)> J=nullptr) ;

    const BroydenInformation & solve(BV::Math::State & x) ;//,
                                     //std::function<void(const BV::Math::State & vals,
                                     //                   Eigen::VectorXd & fOutput)> F=nullptr,
                                     //std::function<void(const BV::Math::State & vals,
                                     //                   Eigen::MatrixXd & jOutput)> J=nullptr) ;

} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV