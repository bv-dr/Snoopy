#pragma once
#ifndef BV_Math_Solvers_Types_hpp
#define BV_Math_Solvers_Types_hpp

namespace BV {
namespace Math {
namespace Solvers {

enum SolverType {
             NEWTON,
             BV_BRENT,
             BRENT_MINIMIZE,
             BV_BRENT_MINIMIZE
                } ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_Solve_hpp_
