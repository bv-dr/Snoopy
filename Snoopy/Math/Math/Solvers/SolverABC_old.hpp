#pragma once
#ifndef BV_Math_Solvers_SolverABC_hpp
#define BV_Math_Solvers_SolverABC_hpp

#include <functional>
#include <Eigen/Dense>

#include "Tools/EigenTypedefs.hpp"

namespace BV {
namespace Math {
namespace Solvers {

class SolverABC
{
protected:
    typedef std::function<Eigen::VectorXd(const Eigen::VectorXd & x)> fEvaluateType ;
    typedef std::function<Eigen::MatrixXd(const Eigen::VectorXd & x)> fDEvaluateType ;
    fEvaluateType fEvaluate_ ;
    fDEvaluateType fDEvaluate_ ;
    bool hasfEvaluate_ ;
    bool hasfDEvaluate_ ;
    bool hasConverged_ ;
    unsigned kiter_ ;
public:
    SolverABC(fEvaluateType fEvaluate,
              fDEvaluateType fDEvaluate) :
        fEvaluate_(fEvaluate), fDEvaluate_(fDEvaluate),
        hasfEvaluate_(true), hasfDEvaluate_(true),
        hasConverged_(false), kiter_(0)
    {
    }

    SolverABC(void) :
        hasfEvaluate_(false), hasfDEvaluate_(false),
        hasConverged_(false), kiter_(0)
    {
    }

    virtual ~SolverABC(void)
    {
    }

    void setFEvaluate(fEvaluateType fEvaluate)
    {
        fEvaluate_ = fEvaluate ;
        hasfEvaluate_ = true ;
    }

    void setFDEvaluate(fDEvaluateType fDEvaluate)
    {
        fDEvaluate_ = fDEvaluate ;
        hasfDEvaluate_ = true ;
    }

    unsigned getIteration(void) const
    {
        return kiter_ ;
    }

    virtual Eigen::VectorXd solve(const Eigen::VectorXd & X0,
                                  bool verbose=true) = 0 ;

    bool hasConverged(void) const
    {
        return hasConverged_ ;
    }

} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_SolverABC_hpp_
