#pragma once
#ifndef BV_Math_Solvers_BrentABC_hpp
#define BV_Math_Solvers_BrentABC_hpp

#include "Math/Solvers/SolverABC.hpp"
#include "Math/Solvers/SolverParameters.hpp"

namespace BV {
namespace Math {
namespace Solvers {

template <class Solver, class Scalar=double>
class BrentABC : public SolverABC<Solver,Scalar>
{
protected:
    double a_ ;
    double b_ ;
    double X_ ;
    double Y_ ;

    virtual void reset_(void)
    {
        SolverABC<Solver,Scalar>::reset_() ;
        X_ = 0. ;
        Y_ = 0. ;
    }

public:

    BrentABC(void) :
        SolverABC<Solver,Scalar>(),
        a_(0.), b_(0.),
        X_(0.), Y_(0.)
    {
    }

    BrentABC(const SolverParameters & params) :
        SolverABC<Solver,Scalar>(params),
        a_(params.get<Scalar>("a", 0.)),
        b_(params.get<Scalar>("b", 0.)),
        X_(0.), Y_(0.)
    {
    }

    virtual ~BrentABC(void)
    {
    }

} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_BrentOptimize_hpp
