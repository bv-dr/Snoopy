#pragma once
#ifndef BV_Math_Solvers_SolverABC_hpp
#define BV_Math_Solvers_SolverABC_hpp

#include <functional>

#include "Math/Solvers/Details.hpp"
#include "Math/Solvers/SolverParameters.hpp"

namespace BV {
namespace Math {
namespace Solvers {

template <class Solver, class Scalar=double>
class SolverABC
{
protected:

    unsigned nMaxIter_ ;
    Scalar absError_ ;
    bool verbose_ ;
    Scalar jacDelta_ ;
    bool computeJOnce_ ;
    bool hasConverged_ ;
    unsigned nIter_ ;

    virtual void reset_(void)
    {
        hasConverged_ = false ;
        nIter_ = 0 ;
    }

public:
    typedef Solver solver_type;

    SolverABC(void) :
        nMaxIter_(20), absError_(1.e-4), verbose_(false), jacDelta_(1.e-4),
        computeJOnce_(false), hasConverged_(false), nIter_(0)
    {
    }

    SolverABC(const SolverParameters & params) :
        nMaxIter_(params.get<unsigned>("maxIterations", 20)),
        absError_(params.get<Scalar>("absoluteError", 1.e-4)),
        verbose_(params.get<bool>("verbose", false)),
        jacDelta_(params.get<Scalar>("jacobianDelta", 1.e-4)),
        computeJOnce_(params.get<bool>("computeJOnce", false)),
        hasConverged_(false),
        nIter_(0)
    {
    }

    virtual ~SolverABC(void)
    {
    }

    void setParameters(const SolverParameters & params)
    {
        nMaxIter_ = params.get<unsigned>("maxIterations", 20) ;
        absError_ = params.get<Scalar>("absoluteError", 1.e-4) ;
        verbose_ = params.get<bool>("verbose", false) ;
        jacDelta_ = params.get<Scalar>("jacobianDelta", 1.e-4) ;
        computeJOnce_ = params.get<bool>("computeJOnce", false) ;
    }

    template <typename System, typename StateIn, typename StateOut,
              typename TType=double,
              typename std::enable_if<
                  Details::IsPair<typename Details::GetType<System>::type>::value, int
                                     >::type=0>
    void solve(System system, const StateIn & state, StateOut & res,
               const TType & t)
    {
        this->solver().solve_impl( system , state, res, t ) ;
    }

    template <typename System, typename StateIn, typename StateOut,
              typename TType=Scalar,
              typename std::enable_if<
                  !Details::IsPair<
                      typename Details::GetType<System>::type
                                  >::value, int
                                     >::type=0>
    void solve(System system, const StateIn & state, StateOut & res, TType t=0.)
    {
        // Jacobian is evaluated via a finite difference scheme
        Details::FDSystem<System, StateIn, Scalar> fdSys(system, jacDelta_) ;
        solve(std::make_pair(std::ref(system), std::ref(fdSys)), state, res, t) ;
    }

    bool hasConverged(void) const
    {
        return hasConverged_ ;
    }

    unsigned getNIterations(void) const
    {
        return nIter_ ;
    }

private:
    solver_type& solver( void )
    {
        return *static_cast< solver_type* >( this );
    }

} ;

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_SolverABC_hpp
