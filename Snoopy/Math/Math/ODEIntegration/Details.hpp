#ifndef BV_Math_ODEIntegration_Details_hpp
#define BV_Math_ODEIntegration_Details_hpp

#include <Eigen/Dense>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Details {

namespace TypeCheck {

template <template<class> class BaseType, typename T>
std::true_type IsEigen(const BaseType<T>*) ;
template <template<class> class BaseType>
std::false_type IsEigen(...) ;

} // End of namespace TypeCheck

template <typename T>
using GetType = std::remove_cv<typename std::remove_reference<T>::type> ;

template <typename T, template<class> class BaseType>
struct IsEigen : decltype(TypeCheck::IsEigen<BaseType>(std::declval<typename GetType<T>::type*>()))
{
} ;

template <typename T>
struct IsPair
{
    static const bool value = false ;
} ;

template <typename A, typename B>
struct IsPair<std::pair<A, B> >
{
    static const bool value = true ;
} ;

} // End of namespace Details

template <typename State1, typename State2, class Enable=void>
struct DotTraits ;

template <typename State1, typename State2>
struct DotTraits<State1, State2,
                 typename std::enable_if<
                         std::is_scalar<State1>::value
                         && std::is_scalar<State2>::value
                                        >::type
                 >
{
    typedef State1 result_type ;
    static result_type get(const State1 & val1, const State2 & val2)
    {
        return val1 * val2 ;
    }
} ;

template <typename State1, typename State2>
struct DotTraits<State1, State2,
                 typename std::enable_if<
                                 Details::IsEigen<State1, Eigen::EigenBase>::value
                                 && Details::IsEigen<State2, Eigen::EigenBase>::value
                                 && State1::IsVectorAtCompileTime
                                 && State2::IsVectorAtCompileTime
                                        >::type
                >
{
    typedef typename State1::Scalar result_type ;
    static result_type get(const State1 & v1, const State2 & v2)
    {
        return v1.dot(v2) ;
    }
} ;

struct dot_operator
{
    template< class State1 , class State2 , class State3>
    void operator()( State1 & v1, const State2 & v2, const State3 & v3 ) const
    {
        v1 = DotTraits<State2, State3>::get(v2, v3) ;
    }
} ;

} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

// FIXME HACK FOR EIGEN > 3.3.0 and boost <= 1.63
// TODO remove when boost has fixed it !
#if EIGEN_VERSION_AT_LEAST(3,3,0)

namespace Eigen {
namespace internal {

template <typename Scalar>
struct scalar_add_op
{
    EIGEN_DEVICE_FUNC inline scalar_add_op(const scalar_add_op & other) :
        m_other(other.m_other)
    {
    }

    EIGEN_DEVICE_FUNC inline scalar_add_op(const Scalar & other) :
        m_other(other)
    {
    }

    EIGEN_DEVICE_FUNC inline Scalar operator()(const Scalar & a) const
    {
        return a + m_other ;
    }
    template <typename Packet>
    EIGEN_DEVICE_FUNC EIGEN_STRONG_INLINE const Packet packetOp(const Packet & a) const
    {
        return internal::padd(a, pset1<Packet>(m_other)) ;
    }

    const Scalar m_other ;
} ;

template <typename Scalar>
struct functor_traits<scalar_add_op<Scalar> >
{
    enum {Cost=NumTraits<Scalar>::AddCost,
          PacketAccess=packet_traits<Scalar>::HasAdd} ;
} ;

} // End of namespace internal
} // End of namespace Eigen

#endif // EIGEN_VERSION_AT_LEAST(3,3,0)

// Specific include to be able to use eigen in boost::numeric::odeint
#include <boost/numeric/odeint/external/eigen/eigen.hpp>
#include <boost/numeric/odeint/stepper/detail/generic_rk_operations.hpp>
#include <boost/numeric/odeint/stepper/detail/generic_rk_call_algebra.hpp>

namespace boost {
namespace numeric {
namespace odeint {

namespace detail {

// Explicit specialisation of error calculation for our ERK...
// which use the generic rk error generators


template< class Operations, class Fac, class Time>
struct generic_rk_scale_sum_err<8, Operations, Fac, Time> :
    public Operations::template scale_sum8< Time >
{
    generic_rk_scale_sum_err(const boost::array<Fac, 8> &a, Time dt) :
        Operations::template scale_sum8< Time >(
          a[0]*dt, a[1]*dt, a[2]*dt, a[3]*dt, a[4]*dt, a[5]*dt, a[6]*dt, a[7]*dt
                                           )
    {
    }
    typedef void result_type;
} ;

template< class Operations, class Fac, class Time>
struct generic_rk_scale_sum_err<10, Operations, Fac, Time> :
    public Operations::template scale_sum10< Time >
{
    generic_rk_scale_sum_err(const boost::array<Fac, 10> &a, Time dt) :
        Operations::template scale_sum10< Time >(
                  a[0]*dt, a[1]*dt, a[2]*dt, a[3]*dt, a[4]*dt, a[5]*dt, a[6]*dt,
                  a[7]*dt, a[8]*dt, a[9]*dt
                                           )
    {
    }
    typedef void result_type;
} ;

template< class Operations, class Fac, class Time>
struct generic_rk_scale_sum_err<16, Operations, Fac, Time>
{
    const boost::array<Fac, 16> a_ ;
    const Time dt_ ;

    generic_rk_scale_sum_err(const boost::array<Fac, 16> &a, Time dt) :
        a_(a), dt_(dt)
    {
    }

    template<class T1, class T2, class T3, class T4, class T5, class T6,
             class T7, class T8, class T9, class T10, class T11, class T12,
             class T13, class T14, class T15, class T16, class T17>
    void operator()(T1 &t1, const T2 &t2, const T3 &t3, const T4 &t4,
                    const T5 &t5, const T6 &t6, const T7 &t7, const T8 &t8,
                    const T9 &t9, const T10 &t10, const T11 &t11,
                    const T12 &t12, const T13 &t13, const T14 &t14,
                    const T15 &t15, const T16 & t16, const T17 & t17) const
    {
        t1 = dt_*(a_[0]*t2 + a_[1]*t3 + a_[2]*t4 + a_[3]*t5 + a_[4]*t6
                  + a_[5]*t7 + a_[6]*t8 + a_[7]*t9 + a_[8]*t10 + a_[9]*t11
                  + a_[10]*t12 + a_[11]*t13 + a_[12]*t14 + a_[13]*t15
                  + a_[14]*t16 + a_[15]*t17) ;
    }
    typedef void result_type;
} ;

//FIXME small hack for default algebra operations

struct extended_vector_space_algebra
{
    template<class S1, class S2, class S3, class S4, class S5, class S6,
             class S7, class S8, class S9, class S10, class S11, class S12,
             class S13, class S14, class S15, class S16, class Op>
    static void for_each16(S1 &s1, S2 &s2, S3 &s3, S4 &s4, S5 &s5, S6 &s6,
                           S7 &s7, S8 &s8, S9 &s9, S10 &s10, S11 &s11, S12 &s12,
                           S13 &s13, S14 &s14, S15 &s15, S16 &s16, Op op)
    {
        op(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16);
    }

    template<class S1, class S2, class S3, class S4, class S5, class S6,
             class S7, class S8, class S9, class S10, class S11, class S12,
             class S13, class S14, class S15, class S16, class S17, class Op>
    static void for_each17(S1 &s1, S2 &s2, S3 &s3, S4 &s4, S5 &s5, S6 &s6,
                           S7 &s7, S8 &s8, S9 &s9, S10 &s10, S11 &s11, S12 &s12,
                           S13 &s13, S14 &s14, S15 &s15, S16 &s16, S17 &s17,
                           Op op)
    {
        op(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17);
    }

    template<class S1, class S2, class S3, class S4, class S5, class S6,
             class S7, class S8, class S9, class S10, class S11, class S12,
             class S13, class S14, class S15, class S16, class S17, class S18,
             class Op>
    static void for_each18(S1 &s1, S2 &s2, S3 &s3, S4 &s4, S5 &s5, S6 &s6,
                           S7 &s7, S8 &s8, S9 &s9, S10 &s10, S11 &s11, S12 &s12,
                           S13 &s13, S14 &s14, S15 &s15, S16 &s16, S17 &s17,
                           S18 &s18, Op op)
    {
        op(s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18);
    }
} ;

template<class Algebra>
struct generic_rk_call_algebra<14, Algebra>
{
    template<class S1, class S2, class S3, class S4, class Op>
    void operator()(Algebra &algebra, S1 &s1, S2 &s2, S3 &s3,
                    S4 s4_array[13], Op op) const
    {
        extended_vector_space_algebra::for_each16(
                    s1, s2, s3, s4_array[0].m_v, s4_array[1].m_v,
                    s4_array[2].m_v, s4_array[3].m_v, s4_array[4].m_v ,
                    s4_array[5].m_v, s4_array[6].m_v, s4_array[7].m_v,
                    s4_array[8].m_v, s4_array[9].m_v, s4_array[10].m_v,
                    s4_array[11].m_v, s4_array[12].m_v, op
                                                 ) ;
    }

    template<class S1, class S2, class S4, class Op>
    void operator()(Algebra &algebra, S1 &s1, S2 &s2,
                    S4 s4_array[13], Op op) const
    {
        algebra.for_each15(s1, s2, s4_array[0].m_v, s4_array[1].m_v,
                           s4_array[2].m_v, s4_array[3].m_v, s4_array[4].m_v ,
                           s4_array[5].m_v, s4_array[6].m_v, s4_array[7].m_v,
                           s4_array[8].m_v, s4_array[9].m_v, s4_array[10].m_v,
                           s4_array[11].m_v, s4_array[12].m_v, op) ;
    }
} ;

template<class Algebra>
struct generic_rk_call_algebra<15, Algebra>
{
    template<class S1, class S2, class S3, class S4, class Op>
    void operator()(Algebra &algebra, S1 &s1, S2 &s2, S3 &s3,
                    S4 s4_array[14], Op op) const
    {
        extended_vector_space_algebra::for_each17(
                    s1, s2, s3, s4_array[0].m_v, s4_array[1].m_v,
                    s4_array[2].m_v, s4_array[3].m_v, s4_array[4].m_v ,
                    s4_array[5].m_v, s4_array[6].m_v, s4_array[7].m_v,
                    s4_array[8].m_v, s4_array[9].m_v, s4_array[10].m_v,
                    s4_array[11].m_v, s4_array[12].m_v, s4_array[13].m_v, op
                                                 ) ;
    }

    template<class S1, class S2, class S4, class Op>
    void operator()(Algebra &algebra, S1 &s1, S2 &s2,
                    S4 s4_array[14], Op op) const
    {
        extended_vector_space_algebra::for_each16(
                    s1, s2, s4_array[0].m_v, s4_array[1].m_v, s4_array[2].m_v,
                    s4_array[3].m_v, s4_array[4].m_v, s4_array[5].m_v,
                    s4_array[6].m_v, s4_array[7].m_v, s4_array[8].m_v,
                    s4_array[9].m_v, s4_array[10].m_v, s4_array[11].m_v,
                    s4_array[12].m_v, s4_array[13].m_v, op
                                                 ) ;
    }
} ;


template<class Algebra>
struct generic_rk_call_algebra<16, Algebra>
{
    template<class S1, class S2, class S3, class S4, class Op>
    void operator()(Algebra &algebra, S1 &s1, S2 &s2, S3 &s3,
                    S4 s4_array[15], Op op) const
    {
        extended_vector_space_algebra::for_each18(
                    s1, s2, s3, s4_array[0].m_v, s4_array[1].m_v,
                    s4_array[2].m_v, s4_array[3].m_v, s4_array[4].m_v ,
                    s4_array[5].m_v, s4_array[6].m_v, s4_array[7].m_v,
                    s4_array[8].m_v, s4_array[9].m_v, s4_array[10].m_v,
                    s4_array[11].m_v, s4_array[12].m_v, s4_array[13].m_v,
                    s4_array[14].m_v, op
                                                 ) ;
    }

    template<class S1, class S2, class S4, class Op>
    void operator()(Algebra &algebra, S1 &s1, S2 &s2,
                    S4 s4_array[15], Op op) const
    {
        extended_vector_space_algebra::for_each17(
                    s1, s2, s4_array[0].m_v, s4_array[1].m_v, s4_array[2].m_v,
                    s4_array[3].m_v, s4_array[4].m_v, s4_array[5].m_v,
                    s4_array[6].m_v, s4_array[7].m_v, s4_array[8].m_v,
                    s4_array[9].m_v, s4_array[10].m_v, s4_array[11].m_v,
                    s4_array[12].m_v, s4_array[13].m_v, s4_array[14].m_v, op
                                                 ) ;
    }
} ;

template<class Operations, class Fac, class Time>
struct generic_rk_scale_sum<14, Operations, Fac, Time>
{
    const boost::array<Fac, 14> a_ ;
    const Time dt_ ;

    generic_rk_scale_sum(const boost::array<Fac, 14> &a, Time dt) :
        a_(a), dt_(dt)
    {
    }

    template<class T1, class T2, class T3, class T4, class T5, class T6,
             class T7, class T8, class T9, class T10, class T11, class T12,
             class T13, class T14, class T15, class T16>
    void operator()(T1 &t1, const T2 &t2, const T3 &t3, const T4 &t4,
                    const T5 &t5, const T6 &t6, const T7 &t7, const T8 &t8,
                    const T9 &t9, const T10 &t10, const T11 &t11,
                    const T12 &t12, const T13 &t13, const T14 &t14,
                    const T15 &t15, const T16 & t16) const
    {
        t1 = t2 + dt_*(a_[0]*t3 + a_[1]*t4 + a_[2]*t5 + a_[3]*t6 + a_[4]*t7
                       + a_[5]*t8 + a_[6]*t9 + a_[7]*t10 + a_[8]*t11 + a_[9]*t12
                       + a_[10]*t13 + a_[11]*t14 + a_[12]*t15 + a_[13]*t16) ;
    }

    typedef void result_type ;
} ;

template<class Operations, class Fac, class Time>
struct generic_rk_scale_sum<15, Operations, Fac, Time>
{
    const boost::array<Fac, 15> a_ ;
    const Time dt_ ;

    generic_rk_scale_sum(const boost::array<Fac, 15> &a, Time dt) :
        a_(a), dt_(dt)
    {
    }

    template<class T1, class T2, class T3, class T4, class T5, class T6,
             class T7, class T8, class T9, class T10, class T11, class T12,
             class T13, class T14, class T15, class T16, class T17>
    void operator()(T1 &t1, const T2 &t2, const T3 &t3, const T4 &t4,
                    const T5 &t5, const T6 &t6, const T7 &t7, const T8 &t8,
                    const T9 &t9, const T10 &t10, const T11 &t11,
                    const T12 &t12, const T13 &t13, const T14 &t14,
                    const T15 &t15, const T16 & t16, const T17 & t17) const
    {
        t1 = t2 + dt_*(a_[0]*t3 + a_[1]*t4 + a_[2]*t5 + a_[3]*t6 + a_[4]*t7
                       + a_[5]*t8 + a_[6]*t9 + a_[7]*t10 + a_[8]*t11 + a_[9]*t12
                       + a_[10]*t13 + a_[11]*t14 + a_[12]*t15 + a_[13]*t16
                       + a_[14]*t17) ;
    }

    typedef void result_type ;
} ;

template<class Operations, class Fac, class Time>
struct generic_rk_scale_sum<16, Operations, Fac, Time>
{
    const boost::array<Fac, 16> a_ ;
    const Time dt_ ;

    generic_rk_scale_sum(const boost::array<Fac,16> &a, Time dt) :
        a_(a), dt_(dt)
    {
    }

    template<class T1, class T2, class T3, class T4, class T5, class T6,
             class T7, class T8, class T9, class T10, class T11, class T12,
             class T13, class T14, class T15, class T16, class T17, class T18>
    void operator()(T1 &t1, const T2 &t2, const T3 &t3, const T4 &t4,
                    const T5 &t5, const T6 &t6, const T7 &t7, const T8 &t8,
                    const T9 &t9, const T10 &t10, const T11 &t11,
                    const T12 &t12, const T13 &t13, const T14 &t14,
                    const T15 &t15, const T16 & t16, const T17 & t17,
                    const T18 & t18) const
    {
        t1 = t2 + dt_*(a_[0]*t3 + a_[1]*t4 + a_[2]*t5 + a_[3]*t6 + a_[4]*t7
                       + a_[5]*t8 + a_[6]*t9 + a_[7]*t10 + a_[8]*t11 + a_[9]*t12
                       + a_[10]*t13 + a_[11]*t14 + a_[12]*t15 + a_[13]*t16
                       + a_[14]*t17 + a_[15]*t18) ;
    }

    typedef void result_type ;
} ;

} // End of namespace detail

} // End of namespace odeint
} // End of namespace numeric
} // End of namespace boost

#endif // BV_Math_ODEIntegration_Details_hpp
