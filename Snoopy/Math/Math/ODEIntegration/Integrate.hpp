#pragma once
#ifndef BV_Math_ODEIntegration_Integrate_hpp
#define BV_Math_ODEIntegration_Integrate_hpp

#include <boost/serialization/array_wrapper.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/preprocessor/repetition/repeat.hpp>
#include <boost/preprocessor/cat.hpp>

#include "Tools/BVException.hpp"
#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/ODEIntegrationParameters.hpp"
#include "Math/ODEIntegration/Steppers/Steppers.hpp"

namespace boostode = boost::numeric::odeint ;

// FIXME all these macros are really bad...
#define N_STEPPERS 54 // Change this if new steppers are added

#define BV_BAD_ODE_STEPPER_FACTORY_MINIMAL_CASE(rep, n, _)                     \
    case n:                                                                    \
    {                                                                          \
        typename Steppers::Stepper<n, State, Value>::Type stepper ## n ## 1(   \
                               Steppers::Stepper<n, State, Value>::get(params) \
                                                                      ) ;      \
        return Integrate(stepper ## n ## 1, system, state, xStart, xEnd, xStep) ; \
        break ;                                                                \
    }

#define BV_BAD_ODE_ADAPTIVE_STEPPER_FACTORY_MINIMAL_CASE(rep, n, _)            \
    case n:                                                                    \
    {                                                                          \
        typename Steppers::AdaptiveStepper<n, State, Value>::Type stepper ## n ## 2(\
                        Steppers::AdaptiveStepper<n, State, Value>::get(params)\
                                                                        ) ;    \
        return Integrate(stepper ## n ## 2, system, state, xStart, xEnd, xStep) ; \
        break ;                                                                \
    }

#define BV_BAD_ODE_STEPPER_FACTORY_OBSERVER_CASE(rep, n, _)                    \
    case n:                                                                    \
    {                                                                          \
        typename Steppers::Stepper<n, State, Value>::Type stepper ## n ## 3(   \
                               Steppers::Stepper<n, State, Value>::get(params) \
                                                                      ) ;      \
        return Integrate(stepper ## n ## 3, system, state, xStart, xEnd, xStep,\
                         observer) ;                                           \
        break ;                                                                \
    }

#define BV_BAD_ODE_ADAPTIVE_STEPPER_FACTORY_OBSERVER_CASE(rep, n, _)           \
    case n:                                                                    \
    {                                                                          \
        typename Steppers::AdaptiveStepper<n, State, Value>::Type stepper ## n ## 4(\
                        Steppers::AdaptiveStepper<n, State, Value>::get(params)\
                                                                        ) ;    \
        return Integrate(stepper ## n ## 4, system, state, xStart, xEnd, xStep,\
                         observer) ;                                           \
        break ;                                                                \
    }

#define BV_BAD_ODE_STEPPER_FACTORY(option)                                     \
    Steppers::StepperScheme scheme(params.getStepperScheme()) ;                \
    bool useAdaptive(params.getUseAdaptive()) ;                                \
    if (useAdaptive)                                                           \
    {                                                                          \
        switch(scheme)                                                         \
        {                                                                      \
            BOOST_PP_REPEAT(N_STEPPERS, BOOST_PP_CAT(BV_BAD_ODE_ADAPTIVE_STEPPER_FACTORY_, BOOST_PP_CAT(option, _CASE)), _) \
        default:                                                               \
            throw "Invalid ODE integration scheme specified" ;                 \
        }                                                                      \
    }                                                                          \
    switch(scheme)                                                             \
    {                                                                          \
        BOOST_PP_REPEAT(N_STEPPERS, BOOST_PP_CAT(BV_BAD_ODE_STEPPER_FACTORY_, BOOST_PP_CAT(option, _CASE)), _) \
    default:                                                                   \
        throw "Invalid ODE integration scheme specified" ;                     \
    }

namespace BV {
namespace Math {
namespace ODEIntegration {

namespace Details {

struct IntegratorStopException : public BV::Tools::Exceptions::BVException
{
    IntegratorStopException(std::string message) : BVException(message)
    {
    }
} ;

struct NullObserver
{
    template <typename State, typename XType>
    void operator()(const State & state, const XType & x) const
    {
    }
} ;

} // End of namespace Details

class IntegrationInformation
{
private:
    bool hasFinished_ ;
    std::size_t nSteps_ ;
public:
    IntegrationInformation(void) : hasFinished_(false), nSteps_(0)
    {
    }

    bool hasFinished(void) const
    {
        return hasFinished_ ;
    }

    void setHasFinished(const bool & val)
    {
        hasFinished_ = val ;
    }

    std::size_t nIntegrationSteps(void) const
    {
        return nSteps_ ;
    }

    void setNIntegrationSteps(const std::size_t & nSteps)
    {
        nSteps_ = nSteps ;
    }
} ;

// FIXME deal with the references for system and observer !

template <typename Stepper, typename System, typename State, typename XType>
inline IntegrationInformation Integrate(Stepper stepper, System system,
                                        State & state, XType xStart, XType xEnd,
                                        XType xStep)
{
    std::size_t nSteps(boostode::integrate_adaptive(stepper, system, state,
                                                    xStart, xEnd, xStep)) ;
    IntegrationInformation info ;
    info.setHasFinished(true) ;
    info.setNIntegrationSteps(nSteps) ;
    return info ;
}

template <typename System, typename State, typename XType, typename Value>
inline IntegrationInformation Integrate(
                      const ODEIntegrationParameters<Value, XType> & params,
                      System system, State & state, XType xStart,
                      XType xEnd, XType xStep
                                       )
{
    BV_BAD_ODE_STEPPER_FACTORY(MINIMAL)
}

template <typename Stepper, typename System, typename State,
          typename XType, typename Observer>
inline IntegrationInformation Integrate(Stepper stepper, System system,
                                        State & state, XType xStart, XType xEnd,
                                        XType xStep, Observer observer)
{
    IntegrationInformation info ;
    try
    {
        std::size_t nSteps(boostode::integrate_adaptive(stepper, system, state,
                                                        xStart, xEnd, xStep,
                                                        observer)) ;
        info.setHasFinished(true) ;
        info.setNIntegrationSteps(nSteps) ;
    }
    catch (Details::IntegratorStopException & e)
    {
    	(void)e ;
        info.setHasFinished(false) ;
        // FIXME current time has to be dealt with stopper object...
        // FIXME nSteps not available for the moment.
    }
    return info ;
}

template <typename System, typename State, typename XType, typename Value,
          typename Observer>
inline IntegrationInformation Integrate(
                      const ODEIntegrationParameters<Value, XType> & params,
                      System system, State & state, XType xStart,
                      XType xEnd, XType xStep, Observer observer
                                       )
{
    BV_BAD_ODE_STEPPER_FACTORY(OBSERVER)
}

} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#undef BV_BAD_ODE_STEPPER_FACTORY
#undef BV_BAD_ODE_STEPPER_FACTORY_MINIMAL_CASE
#undef BV_BAD_ODE_ADAPTIVE_STEPPER_FACTORY_MINIMAL_CASE
#undef BV_BAD_ODE_STEPPER_FACTORY_OBSERVER_CASE
#undef BV_BAD_ODE_ADAPTIVE_STEPPER_FACTORY_OBSERVER_CASE
#undef N_STEPPERS

#endif // BV_Math_ODEIntegration_Integrate_hpp
