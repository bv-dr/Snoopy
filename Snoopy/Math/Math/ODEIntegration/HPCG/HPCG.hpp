/*
 * HPCG.hpp
 *
 *  Created on: 16 d�c. 2015
 *      Author: cbrun
 */

#ifndef BV_Math_ODEIntegration_HPCG_HPCG_hpp_
#define BV_Math_ODEIntegration_HPCG_HPCG_hpp_

#include <cmath>
#include <vector>
#include <iostream>

namespace BV {
namespace Math {
namespace ODEIntegration {

struct HPCGParameters
{
    double startX ;
    double endX ;
    double stepX ;
    double tolerance ;
    double minimumTolerance ;
    bool stop ;
    bool useAdaptative ;
    int maxXStepDivision ;
    int maxXStepMultiplication ;
    HPCGParameters(void) :
        startX(0.), endX(0.), stepX(0.), tolerance(0.), minimumTolerance(0.),
        stop(true), useAdaptative(false), maxXStepDivision(10),
        maxXStepMultiplication(0)
    {
    }

    HPCGParameters(const double & startX, const double & endX,
                   const double & stepX, const double & tolerance,
                   const bool & useAdaptative = false,
                   const double & adaptativeToleranceCoefficient = 50.,
                   const int & maxXStepDivision = 10,
                   const int & maxXStepMultiplication = 0) :
        startX(startX), endX(endX), stepX(stepX), tolerance(tolerance),
        minimumTolerance(tolerance / adaptativeToleranceCoefficient),
        stop(false), useAdaptative(useAdaptative),
        maxXStepDivision(maxXStepDivision),
        maxXStepMultiplication(maxXStepMultiplication)
    {
    }
} ;

enum struct HPCGErrors
    :int
    {
        CALCULATION_STOPPED_BY_USER = 20,
    BISECTIONS_METHOD_FAILED = 30,
    NOT_ENOUGH_STEPS_FOR_CALCULATION = 40,
    EXCEED_MAX_DEPTH_STEP = 50
} ;

template <typename FCT, typename OUTP>
void HPCG(FCT & fct, OUTP & outp, HPCGParameters & params, double* Y,
          double* derY, const unsigned & ndim,
          const std::vector<double> ponderationCoefficients)
{
    // Variables declaration
    double* y0_ ;
    double* dery0_ ;
    double* ponderationCoefficients_ ;
    double* P_CTemp_ ;
    double** yTemp_ ;
    double** deryTemp_ ;
    double** rkTemp_ ;
    double x_, h_ ;
    int n_ ;
    int ihlf_ = 0 ;
    auto deallocTables_ = [&yTemp_, &deryTemp_, &y0_, &dery0_, &rkTemp_,
    &P_CTemp_, &ponderationCoefficients_](void)->void
    {
        if (yTemp_ != 0)
        {
            for (int i = 0 ; i < 7 ; ++i)
            delete[] yTemp_[i] ;
        }
        delete[] yTemp_ ;
        if (deryTemp_ != 0)
        {
            for (int i = 0 ; i < 7 ; ++i)
            delete[] deryTemp_[i] ;
        }
        delete[] deryTemp_ ;
        delete[] y0_ ;
        delete[] dery0_ ;
        delete[] P_CTemp_ ;
        delete[] ponderationCoefficients_ ;
        if (rkTemp_ != 0)
        {
            for (int i = 0 ; i < 3 ; ++i)
            delete[] rkTemp_[i] ;
        }
        delete[] rkTemp_ ;
    } ;

    /*!
     * Computes by means of
     * runge-kutta method starting values for the not self-starting
     * predictor-corrector method.
     */
    auto rungeKutta_ =
        [ndim, &h_, &n_, &yTemp_, &deryTemp_, &rkTemp_, &Y, &derY, &x_, fct, &outp, &ihlf_, &params](const bool & withOutput)->void
        {
            fct(x_, Y, derY, ndim) ;
            if (params.stop)
            {
                throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
            }
            if (withOutput)
            {
                params.stepX = h_ ;
                outp(x_, Y, derY, ihlf_, ndim, params) ;
            }
            for (unsigned i = 0 ; i < ndim ; ++i)
            {
                yTemp_[n_][i] = Y[i] ;
                deryTemp_[n_][i] = derY[i] ;
            }
            double z ;
            for (unsigned i = 0 ; i < ndim ; ++i)
            {
                z = h_ * deryTemp_[n_][i] ;
                rkTemp_[0][i] = z ;
                Y[i] = yTemp_[n_][i] + 0.4 * z ;
            }
            z = x_ + .4 * h_ ;
            //EVAL
            fct(z, Y, derY, ndim) ;
            if (params.stop)
            {
                throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
            }
            for (unsigned i = 0 ; i < ndim ; ++i)
            {
                z = h_ * derY[i] ;
                rkTemp_[1][i] = z ;
                Y[i] = yTemp_[n_][i] + 0.2969776 * rkTemp_[0][i] + 0.1587596 * z ;
            }
            z = x_ + 0.4557372 * h_ ;
            //EVAL
            fct(z, Y, derY, ndim) ;
            if (params.stop)
            {
                throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
            }
            for (unsigned i = 0 ; i < ndim ; ++i)
            {
                z = h_ * derY[i] ;
                rkTemp_[2][i] = z ;
                Y[i] = yTemp_[n_][i] + 0.2181004 * rkTemp_[0][i]
                - 3.050965 * rkTemp_[1][i] + 3.832865 * z ;
            }
            z = x_ + h_ ;
            //EVAL
            fct(z, Y, derY, ndim) ;
            if (params.stop)
            {
                throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
            }
            for (unsigned i = 0 ; i < ndim ; ++i)
            {
                Y[i] = yTemp_[n_][i] + 0.1747603 * rkTemp_[0][i]
                - 0.5514807 * rkTemp_[1][i] + 1.205536 * rkTemp_[2][i]
                + 0.1711848 * h_ * derY[i] ;
            }
        } ;

    auto computeStartValues_ =
        [&Y, &derY, &x_, &y0_, &params, rungeKutta_, &h_, &n_, &dery0_, &ndim, &ihlf_, &yTemp_, &ponderationCoefficients_](void)->void
        {
            /* Sets the initial step using bisection method*/
            double delt(params.tolerance + 1.) ;
            while (delt > params.tolerance)
            {
                for (unsigned i = 0 ; i < ndim ; ++i)
                {
                    Y[i] = y0_[i] ; // reset reference X0
                }
                n_ = 0 ;
                rungeKutta_(false) ;
                for (unsigned i = 0 ; i < ndim ; ++i)
                {
                    yTemp_[0][i] = Y[i] ; // save computed X after h
                    Y[i] = y0_[i] ;// reset reference X0
                    derY[i] = dery0_[i] ;// reset reference X0
                }
                n_ = 1 ;
                h_ /= 2. ;
                rungeKutta_(false) ;
                x_ += h_ ;
                rungeKutta_(false) ;
                x_ -= h_ ;
                h_ *= 2. ;
                delt = 0. ;
                for (unsigned i = 0 ; i < ndim ; ++i)
                {
                    delt += ponderationCoefficients_[i]
                    * std::abs(Y[i] - yTemp_[0][i]) ;
                }
                delt *= 0.06666667 ;
                if (delt < params.tolerance)
                {
                    break ;
                }
                ++ihlf_ ;
                // no satisfactory accuracy after 10 bisections. error message.
                if (ihlf_ > params.maxXStepDivision)
                {
                    throw(HPCGErrors::BISECTIONS_METHOD_FAILED) ;
                }
                h_ *= 0.5 ;
            }
            // Reset initial values
            for (unsigned i = 0 ; i < ndim ; ++i)
            {
                Y[i] = y0_[i] ;
                derY[i] = dery0_[i] ;
            }
            x_ = params.startX ;
            for (n_ = 0 ; n_ < 4 ; ++n_, x_ += h_)
            {
                rungeKutta_(true) ;
            }
            x_ -= h_ ;
            --n_ ;
        } ;

    auto hammingModifiedMethod_ =
        [&h_, &ihlf_, ndim, &Y, &yTemp_, &deryTemp_, &n_, &x_, &P_CTemp_, &derY, fct, &outp, &params, &ponderationCoefficients_](void)->void
        {
            int istep(3) ;
            while (!(x_ >= params.endX || std::abs(x_ - params.endX) < 0.1 * h_))
            {
                x_ += h_ ;
                istep += 1 ;
                for (unsigned i = 0 ; i < ndim ; ++i)
                {
                    double P1 = yTemp_[n_ - 3][i]
                    + 4. / 3. * h_
                    * (2. * (deryTemp_[n_][i] + deryTemp_[n_ - 2][i])
                        - deryTemp_[n_ - 1][i]) ;
                    /* modified predictor is generated in y*/
                    Y[i] = P1 - 112. / 121. * P_CTemp_[i] ;
                    P_CTemp_[i] = P1 ;
                }
                //EVAL
                fct(x_, Y, derY, ndim) ;
                if (params.stop)
                {
                    throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
                }
                for (unsigned i = 0 ; i < ndim ; ++i)
                {
                    double C = 0.125
                    * (9. * yTemp_[n_][i] - yTemp_[n_ - 2][i]
                        + 3. * h_
                        * (derY[i] + 2. * deryTemp_[n_][i]
                            - deryTemp_[n_ - 1][i])) ;
                    P_CTemp_[i] -= C ;
                    Y[i] = C + 9. / 121. * P_CTemp_[i] ;
                }
                if (params.useAdaptative)
                {
                    double delt = 0. ;
                    for (unsigned i = 0 ; i < ndim ; ++i)
                    {
                        delt += ponderationCoefficients_[i] * std::abs(P_CTemp_[i]) ;
                    }
                    if (ihlf_ > params.maxXStepDivision)
                    {
                        throw(HPCGErrors::EXCEED_MAX_DEPTH_STEP) ;
                    }
                    if (delt >= params.tolerance)
                    {
                        h_ = 0.5 * h_ ;
                        ++ihlf_ ;
                        for (unsigned i = 0 ; i < ndim ; ++i)
                        {
                            Y[i] = .00390625
                            * (80. * yTemp_[n_][i] + 135. * yTemp_[n_ - 1][i]
                                + 40. * yTemp_[n_ - 2][i] + yTemp_[n_ - 3][i])
                            - 0.1171875
                            * (deryTemp_[n_][i] - 6. * deryTemp_[n_ - 1][i]
                                - deryTemp_[n_ - 2][i]) * h_ ;
                            yTemp_[n_ - 3][i] = .00390625
                            * (12. * yTemp_[n_][i] + 135. * yTemp_[n_ - 1][i]
                                + 108. * yTemp_[n_ - 2][i] + yTemp_[n_ - 3][i])
                            - .0234375
                            * (deryTemp_[n_][i] + 18. * deryTemp_[n_ - 1][i]
                                - 9. * deryTemp_[n_ - 2][i]) * h_ ;
                            yTemp_[n_ - 2][i] = yTemp_[n_ - 1][i] ;
                            deryTemp_[n_ - 2][i] = deryTemp_[n_ - 1][i] ;
                        }
                        x_ -= h_ ;
                        double delt = x_ - 2. * h_ ;
                        //EVAL
                        fct(delt, Y, derY, ndim) ;
                        if (params.stop)
                        {
                            throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
                        }
                        for (unsigned i = 0 ; i < ndim ; ++i)
                        {
                            yTemp_[n_ - 1][i] = Y[i] ;
                            deryTemp_[n_ - 1][i] = derY[i] ;
                            Y[i] = yTemp_[n_ - 3][i] ;
                        }
                        delt -= 2. * h_ ;
                        //EVAL
                        fct(delt, Y, derY, ndim) ;
                        if (params.stop)
                        {
                            throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
                        }
                        for (unsigned i = 0 ; i < ndim ; ++i)
                        {
                            delt = 3. * (deryTemp_[n_ - 1][i] + deryTemp_[n_ - 2][i]) ;
                            P_CTemp_[i] = 8.962963 * (yTemp_[n_][i] - Y[i])
                            - 3.361111 * h_ * (deryTemp_[n_][i] + delt + derY[i]) ;
                            deryTemp_[n_ - 3][i] = derY[i] ;
                        }
                        istep = 0 ;
                        x_ -= h_ ;
                        continue ;
                    }
                }
                fct(x_, Y, derY, ndim) ;
                if (params.stop)
                {
                    throw(HPCGErrors::CALCULATION_STOPPED_BY_USER) ;
                }
                params.stepX = h_ ;
                outp(x_, Y, derY, ihlf_, ndim, params) ;
                if (params.useAdaptative)
                {
                    double delt = 0. ;
                    for (unsigned i = 0 ; i < ndim ; ++i)
                    {
                        delt += ponderationCoefficients_[i] * std::abs(P_CTemp_[i]) ;
                    }
                    if (delt < params.minimumTolerance
                        //&& ihlf_ > -params.maxXStepMultiplication
                        && n_ >= 6
                        && istep >= 4 && (istep % 2) == 0)
                    {
                        h_ += h_ ;
                        ihlf_-- ;
                        for (unsigned i = 0 ; i < ndim ; ++i)
                        {
                            yTemp_[n_][i] = yTemp_[n_ - 1][i] ;
                            yTemp_[n_ - 1][i] = yTemp_[n_ - 3][i] ;
                            yTemp_[n_ - 2][i] = yTemp_[n_ - 5][i] ;
                            deryTemp_[n_][i] = deryTemp_[n_ - 1][i] ;
                            deryTemp_[n_ - 1][i] = deryTemp_[n_ - 3][i] ;
                            deryTemp_[n_ - 2][i] = deryTemp_[n_ - 5][i] ;
                            P_CTemp_[i] = 8.962963 * (Y[i] - yTemp_[n_ - 3][i])
                            - 3.361111 * h_
                            * (derY[i]
                                + 3. * (deryTemp_[n_ - 1][i] + deryTemp_[n_ - 2][i])
                                + deryTemp_[n_ - 3][i]) ;
                        }
                        istep = 0 ;
                    }
                }
                // SLIDE N
                if (n_ == 6)
                {
                    for (n_ = 1 ; n_ < 7 ; ++n_)
                    {
                        for (unsigned i = 0 ; i < ndim ; ++i)
                        {
                            yTemp_[n_ - 1][i] = yTemp_[n_][i] ;
                            deryTemp_[n_ - 1][i] = deryTemp_[n_][i] ;
                        }
                    }
                    n_ = 5 ;
                }
                ++n_ ;
                for (unsigned i = 0 ; i < ndim ; ++i)
                {
                    yTemp_[n_][i] = Y[i] ;
                    deryTemp_[n_][i] = derY[i] ;
                }
            }
        } ;
    // Variables initialization
    y0_ = new double[ndim] ;
    dery0_ = new double[ndim] ;
    P_CTemp_ = new double[ndim] ;
    ponderationCoefficients_ = new double[ndim] ;
    for (unsigned i = 0; i < ndim; ++i)
    {
        y0_[i] = Y[i] ;
        dery0_[i] = derY[i] ;
        P_CTemp_[i] = 0. ;
        ponderationCoefficients_[i] = 0. ;
    }
    rkTemp_ = new double*[3] ;
    for (int i = 0; i < 3; ++i)
    {
        rkTemp_[i] = new double[ndim] ;
        for (unsigned j = 0; j < ndim; ++j)
            rkTemp_[i][j] = 0. ;
    }
    yTemp_ = new double*[7] ;
    deryTemp_ = new double*[7] ;
    for (int i = 0; i < 7; ++i)
    {
        yTemp_[i] = new double[ndim] ;
        deryTemp_[i] = new double[ndim] ;
        for (unsigned j = 0; j < ndim; ++j)
        {
            yTemp_[i][j] = 0. ;
            deryTemp_[i][j] = 0. ;
        }
    }
    // first value of
    x_ = params.startX ;
    h_ = params.stepX ;
    if (params.useAdaptative)
    {
        if (ponderationCoefficients.size() != ndim)
        {
            std::cout
                << "Warning: Wrong number of ponderation coefficients. 1. is used"
                << std::endl ;
            for (unsigned i = 0; i < ndim; ++i)
                ponderationCoefficients_[i] = 1 ;
        }
        else
        {
            for (unsigned i = 0; i < ndim; ++i)
                ponderationCoefficients_[i] = ponderationCoefficients[i] ;
        }
    }
    if (h_ * (params.endX - x_) <= 0.)
    {
        deallocTables_() ;
        throw(HPCGErrors::NOT_ENOUGH_STEPS_FOR_CALCULATION) ;
    }
    try
    {
        computeStartValues_() ;
        hammingModifiedMethod_() ;
    }
    catch (int & iError)
    {
        deallocTables_() ;
        throw(iError) ;
    }
    deallocTables_() ;
}

} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif /* BV_Math_ODEIntegration_HPCG_HPCG_hpp_ */
