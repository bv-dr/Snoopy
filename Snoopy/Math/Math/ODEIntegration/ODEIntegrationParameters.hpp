#pragma once
#ifndef BV_Math_ODEIntegration_ODEIntegrationParameters_hpp
#define BV_Math_ODEIntegration_ODEIntegrationParameters_hpp

#include <iostream>
#include <iomanip>

#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {

template <typename Value=double, typename Time=double>
class ODEIntegrationParameters
{
private:
    Steppers::StepperScheme scheme_ ;
    bool useAdaptive_ ;
    Value aTol_ ;
    Value rTol_ ;
    Value scaleYError_ ;
    Value scaleDYError_ ;
    Time maxStepSize_ ;

public:
    ODEIntegrationParameters(Steppers::StepperScheme scheme=Steppers::StepperScheme::ERK_DOPRI5,
                             bool useAdaptive=true,
                             Value aTol=1.e-6, Value rTol=1.e-6,
                             Value scaleYError=1., Value scaleDYError=0.,
                             Time maxStepSize=200) :
        scheme_(scheme), useAdaptive_(useAdaptive), aTol_(aTol), rTol_(rTol),
        scaleYError_(scaleYError), scaleDYError_(scaleDYError),
        maxStepSize_(maxStepSize)
    {
    }

    const Steppers::StepperScheme & getStepperScheme(void) const
    {
        return scheme_ ;
    }

    const bool & getUseAdaptive(void) const
    {
        return useAdaptive_ ;
    }

    const Value & getATol(void) const
    {
        return aTol_ ;
    }

    const Value & getRTol(void) const
    {
        return rTol_ ;
    }

    const Value & getScaleYError(void) const
    {
        return scaleYError_ ;
    }

    const Value & getScaleDYError(void) const
    {
        return scaleDYError_ ;
    }

    const Time & getMaxStepSize(void) const
    {
        return maxStepSize_ ;
    }

    friend std::ostream & operator<<(std::ostream & os,
                                     const ODEIntegrationParameters & params)
    {
        os << std::left ;
        os << std::setw(20) << "scheme" << params.getStepperScheme() << std::endl ;
        os << std::setw(20) << "use adaptive" << params.getUseAdaptive() << std::endl ;
        os << std::setw(20) << "absolute tolerance" << params.getATol() << std::endl ;
        os << std::setw(20) << "relative tolerance" << params.getRTol() << std::endl ;
        os << std::setw(20) << "scale y error" << params.getScaleYError() << std::endl ;
        os << std::setw(20) << "scale dy error" << params.getScaleDYError() << std::endl ;
        os << std::setw(20) << "max step size" << params.getMaxStepSize() << std::endl ;
        return os ;
    }
} ;

} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_ODEIntegrationParameters_hpp
