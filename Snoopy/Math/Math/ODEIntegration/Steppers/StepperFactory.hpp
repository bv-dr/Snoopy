#ifndef BV_Math_ODEIntegration_Steppers_StepperFactory_hpp
#define BV_Math_ODEIntegration_Steppers_StepperFactory_hpp

#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>

#include "Math/ODEIntegration/Details.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

template <
    int Scheme,
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
struct StepperFactory ;

template <
    int Scheme,
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer,
    class ErrorChecker=boostode::default_error_checker<Value, Algebra, Operations>,
    class StepAdjuster=boostode::default_step_adjuster<Value, Time>
         >
struct AdaptiveStepperFactory ;

template <int Scheme>
struct HasAdaptiveStep
{
    static constexpr bool value = true ;

} ;

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_StepperFactory_hpp
