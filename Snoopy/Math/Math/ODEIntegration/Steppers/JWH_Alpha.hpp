#pragma once


#include "Math/ODEIntegration/Steppers/Details/JWH_Alpha.hpp"

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/Details/ControlledNLImplicitStepper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer
         >
struct StepperFactory<StepperScheme::JWH_ALPHA,
                      State, Value, Deriv, Time, Algebra,
                      Operations, Resizer>
{
    typedef Details::JWH_Alpha<State, Value, Deriv, Time, Algebra,
                                   Operations, Resizer> StepperType ;
} ;

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer,
    class ErrorChecker,
    class StepAdjuster
         >
struct AdaptiveStepperFactory<StepperScheme::JWH_ALPHA,
                              State, Value, Deriv, Time, Algebra,
                              Operations, Resizer,
                              ErrorChecker, StepAdjuster>
{
    typedef Details::ControlledNLImplicitStepper<
                   Details::JWH_Alpha<State, Value, Deriv, Time, Algebra,
                                      Operations, Resizer>
                                                > StepperType ;

} ;

//template <>
//struct HasAdaptiveStep<StepperScheme::JWH_ALPHA>
//{
//    static constexpr bool value = false ;
//} ;

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV




