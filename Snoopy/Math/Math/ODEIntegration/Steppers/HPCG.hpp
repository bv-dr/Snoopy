#ifndef BV_Math_ODEIntegration_Steppers_HPCG_hpp
#define BV_Math_ODEIntegration_Steppers_HPCG_hpp

#include "Math/ODEIntegration/Steppers/Details/HPCG.hpp"

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer
         >
struct StepperFactory<StepperScheme::HPCG,
                      State, Value, Deriv, Time, Algebra,
                      Operations, Resizer>
{
    typedef Details::ExplicitWrapper<
                Details::HPCG<State, Value, Deriv, Time, Algebra,
                              Operations, Resizer>
                                    > StepperType ;
} ;

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer,
    class ErrorChecker,
    class StepAdjuster
         >
struct AdaptiveStepperFactory<StepperScheme::HPCG,
                              State, Value, Deriv, Time, Algebra,
                              Operations, Resizer,
                              ErrorChecker, StepAdjuster>
{
    typedef Details::ExplicitControlledWrapper<
                Details::ControlledHPCG<
                    Details::HPCG<State, Value, Deriv, Time, Algebra,
                                  Operations, Resizer, 4, 7>,
                    ErrorChecker
                                       >
                                              > StepperType ;
} ;


} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_HPCG_hpp
