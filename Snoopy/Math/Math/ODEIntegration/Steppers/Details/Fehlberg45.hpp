#ifndef BV_Math_ODEIntegration_Steppers_Details_Fehlberg45_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Fehlberg45_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Fehlberg45_a1 : boost::array<Value, 1>
{
    Fehlberg45_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct Fehlberg45_a2 : boost::array<Value, 2>
{
    Fehlberg45_a2(void)
    {
        (*this)[0] = static_cast<Value>(3.) / static_cast<Value>(32.) ;
        (*this)[1] = static_cast<Value>(9.) / static_cast<Value>(32.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg45_a3 : boost::array<Value, 3>
{
    Fehlberg45_a3(void)
    {
        (*this)[0] = static_cast<Value>(1932.) / static_cast<Value>(2197.) ;
        (*this)[1] = static_cast<Value>(-7200.) / static_cast<Value>(2197.) ;
        (*this)[2] = static_cast<Value>(7296.) / static_cast<Value>(2197.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg45_a4 : boost::array<Value, 4>
{
    Fehlberg45_a4(void)
    {
        (*this)[0] = static_cast<Value>(439.) / static_cast<Value>(216.) ;
        (*this)[1] = static_cast<Value>(-8.) ;
        (*this)[2] = static_cast<Value>(3680.) / static_cast<Value>(513.) ;
        (*this)[3] = static_cast<Value>(-845.) / static_cast<Value>(4104.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg45_a5 : boost::array<Value, 5>
{
    Fehlberg45_a5(void)
    {
        (*this)[0] = static_cast<Value>(-8.) / static_cast<Value>(27.) ;
        (*this)[1] = static_cast<Value>(2.) ;
        (*this)[2] = static_cast<Value>(-3544.) / static_cast<Value>(2565.) ;
        (*this)[3] = static_cast<Value>(1859.) / static_cast<Value>(4104.) ;
        (*this)[4] = static_cast<Value>(-11.) / static_cast<Value>(40.) ;
    }
} ;

// 4th order coefficients
template <typename Value=double>
struct Fehlberg45_b : boost::array<Value, 6>
{
    Fehlberg45_b(void)
    {
        (*this)[0] = static_cast<Value>(25.) / static_cast<Value>(216.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(1408.) / static_cast<Value>(2565.) ;
        (*this)[3] = static_cast<Value>(2197.) / static_cast<Value>(4104.) ;
        (*this)[4] = static_cast<Value>(-1.) / static_cast<Value>(5.) ;
        (*this)[5] = static_cast<Value>(0.) ;
    }
} ;

// These values correspond to the difference of the 5th order and 4th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Fehlberg45_db : boost::array<Value, 6>
{
    Fehlberg45_db(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(360.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(-128.) / static_cast<Value>(4275.) ;
        (*this)[3] = static_cast<Value>(-2197) / static_cast<Value>(75240.) ;
        (*this)[4] = static_cast<Value>(1.) / static_cast<Value>(50.) ;
        (*this)[5] = static_cast<Value>(2.) / static_cast<Value>(55.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg45_c : boost::array<Value, 6>
{
    Fehlberg45_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[2] = static_cast<Value>(3) / static_cast<Value>(8) ;
        (*this)[3] = static_cast<Value>(12) / static_cast<Value>(13) ;
        (*this)[4] = static_cast<Value>(1) ;
        (*this)[5] = static_cast<Value>(1) / static_cast<Value>(2) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Fehlberg45 :
            public boostode::explicit_error_generic_rk<6, 4, 4, 5, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<6, 4, 4, 5, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Fehlberg45(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Fehlberg45_a1<Value>(),
                                                     Fehlberg45_a2<Value>(),
                                                     Fehlberg45_a3<Value>(),
                                                     Fehlberg45_a4<Value>(),
                                                     Fehlberg45_a5<Value>()),
                          Fehlberg45_b<Value>(), Fehlberg45_db<Value>(),
                          Fehlberg45_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Fehlberg45_hpp
