#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner89_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner89_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Verner89_a1 : boost::array<Value, 1>
{
    Verner89_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(12) ;
    }
} ;

template <typename Value=double>
struct Verner89_a2 : boost::array<Value, 2>
{
    Verner89_a2(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(27.) ;
        (*this)[1] = static_cast<Value>(2.) / static_cast<Value>(27.) ;
    }
} ;

template <typename Value=double>
struct Verner89_a3 : boost::array<Value, 3>
{
    Verner89_a3(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(24.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3.) / static_cast<Value>(24.) ;
    }
} ;

template <typename Value=double>
struct Verner89_a4 : boost::array<Value, 4>
{
    Verner89_a4(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(4. + 94. * sqrt6) / static_cast<Value>(375.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-282. - 252. * sqrt6) / static_cast<Value>(375.) ;
        (*this)[3] = static_cast<Value>(328. + 208. * sqrt6) / static_cast<Value>(375.) ;
    }
} ;

template <typename Value=double>
struct Verner89_a5 : boost::array<Value, 5>
{
    Verner89_a5(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(9. - sqrt6) / static_cast<Value>(150) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(312. + 32. * sqrt6) / static_cast<Value>(1425) ;
        (*this)[4] = static_cast<Value>(69. + 29. * sqrt6) / static_cast<Value>(570) ;
    }
} ;

template <typename Value=double>
struct Verner89_a6 : boost::array<Value, 6>
{
    Verner89_a6(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(927. - 347. * sqrt6) / static_cast<Value>(1250) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-16248. + 7328. * sqrt6) / static_cast<Value>(9375) ;
        (*this)[4] = static_cast<Value>(-489. + 179. * sqrt6) / static_cast<Value>(3750) ;
        (*this)[5] = static_cast<Value>(14268. - 5798. * sqrt6) / static_cast<Value>(9375) ;
    }
} ;

template <typename Value=double>
struct Verner89_a7 : boost::array<Value, 7>
{
    Verner89_a7(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(4) / static_cast<Value>(54) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(16. - sqrt6) / static_cast<Value>(54) ;
        (*this)[6] = static_cast<Value>(16. + sqrt6) / static_cast<Value>(54) ;
    }
} ;

template <typename Value=double>
struct Verner89_a8 : boost::array<Value, 8>
{
    Verner89_a8(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(38) / static_cast<Value>(512) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(118. - 23. * sqrt6) / static_cast<Value>(512) ;
        (*this)[6] = static_cast<Value>(118. + 23. * sqrt6) / static_cast<Value>(512) ;
        (*this)[7] = static_cast<Value>(-18) / static_cast<Value>(512) ;
    }
} ;

template <typename Value=double>
struct Verner89_a9 : boost::array<Value, 9>
{
    Verner89_a9(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(11) / static_cast<Value>(144) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(266. - sqrt6) / static_cast<Value>(864) ;
        (*this)[6] = static_cast<Value>(266. + sqrt6) / static_cast<Value>(864) ;
        (*this)[7] = static_cast<Value>(-1) / static_cast<Value>(16) ;
        (*this)[8] = static_cast<Value>(-8) / static_cast<Value>(27) ;
    }
} ;

template <typename Value=double>
struct Verner89_a10 : boost::array<Value, 10>
{
    Verner89_a10(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(5034. - 271. * sqrt6) / static_cast<Value>(61440) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(0) ;
        (*this)[6] = static_cast<Value>(7859. - 1626. * sqrt6) / static_cast<Value>(10240) ;
        (*this)[7] = static_cast<Value>(-2232. + 813. * sqrt6) / static_cast<Value>(20480) ;
        (*this)[8] = static_cast<Value>(-594.  + 271. * sqrt6) / static_cast<Value>(960) ;
        (*this)[9] = static_cast<Value>(657. - 813. * sqrt6) / static_cast<Value>(5120) ;
    }
} ;

template <typename Value=double>
struct Verner89_a11 : boost::array<Value, 11>
{
    Verner89_a11(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(5996. - 3794. * sqrt6) / static_cast<Value>(405) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-4342. - 338. * sqrt6) / static_cast<Value>(9) ;
        (*this)[6] = static_cast<Value>(154922. - 40458. * sqrt6) / static_cast<Value>(135) ;
        (*this)[7] = static_cast<Value>(-4176. + 3794. * sqrt6) / static_cast<Value>(45) ;
        (*this)[8] = static_cast<Value>(-340864. + 242816. * sqrt6) / static_cast<Value>(405) ;
        (*this)[9] = static_cast<Value>(26304. - 15176. * sqrt6) / static_cast<Value>(45) ;
        (*this)[10] = static_cast<Value>(-26624) / static_cast<Value>(81) ;
    }
} ;

template <typename Value=double>
struct Verner89_a12 : boost::array<Value, 12>
{
    Verner89_a12(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(3793. + 2168. * sqrt6) / static_cast<Value>(103680) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(4042. + 2263. * sqrt6) / static_cast<Value>(13824) ;
        (*this)[6] = static_cast<Value>(-231278. + 40717. * sqrt6) / static_cast<Value>(69120) ;
        (*this)[7] = static_cast<Value>(7947. - 2168. * sqrt6) / static_cast<Value>(11520) ;
        (*this)[8] = static_cast<Value>(1048. - 542. * sqrt6) / static_cast<Value>(405) ;
        (*this)[9] = static_cast<Value>(-1383. + 542. * sqrt6) / static_cast<Value>(720) ;
        (*this)[10] = static_cast<Value>(2624) / static_cast<Value>(1053) ;
        (*this)[11] = static_cast<Value>(3) / static_cast<Value>(1664) ;
    }
} ;

template <typename Value=double>
struct Verner89_a13 : boost::array<Value, 13>
{
    Verner89_a13(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(-137) / static_cast<Value>(1296) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(5642. - 337. * sqrt6) / static_cast<Value>(864) ;
        (*this)[6] = static_cast<Value>(5642. + 337. * sqrt6) / static_cast<Value>(864) ;
        (*this)[7] = static_cast<Value>(-299) / static_cast<Value>(48) ;
        (*this)[8] = static_cast<Value>(184) / static_cast<Value>(81) ;
        (*this)[9] = static_cast<Value>(-44) / static_cast<Value>(9) ;
        (*this)[10] = static_cast<Value>(-5120) / static_cast<Value>(1053) ;
        (*this)[11] = static_cast<Value>(-11) / static_cast<Value>(468) ;
        (*this)[12] = static_cast<Value>(16) / static_cast<Value>(9) ;
    }
} ;

template <typename Value=double>
struct Verner89_a14 : boost::array<Value, 14>
{
    Verner89_a14(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(33617. - 2168. * sqrt6) / static_cast<Value>(518400) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-3846. + 31. * sqrt6) / static_cast<Value>(13824) ;
        (*this)[6] = static_cast<Value>(155338. - 52807. * sqrt6) / static_cast<Value>(345600) ;
        (*this)[7] = static_cast<Value>(-12537. + 2168. * sqrt6) / static_cast<Value>(57600) ;
        (*this)[8] = static_cast<Value>(92. + 542. * sqrt6) / static_cast<Value>(2025) ;
        (*this)[9] = static_cast<Value>(-1797. - 542. * sqrt6) / static_cast<Value>(3600) ;
        (*this)[10] = static_cast<Value>(320) / static_cast<Value>(567) ;
        (*this)[11] = static_cast<Value>(-1) / static_cast<Value>(1920) ;
        (*this)[12] = static_cast<Value>(4) / static_cast<Value>(105) ;
        (*this)[13] = static_cast<Value>(0) ;
    }
} ;

template <typename Value=double>
struct Verner89_a15 : boost::array<Value, 15>
{
    Verner89_a15(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(-36487. - 30352. * sqrt6) / static_cast<Value>(279600) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-29666. - 4499. * sqrt6) / static_cast<Value>(7456) ;
        (*this)[6] = static_cast<Value>(2779182. - 615973. * sqrt6) / static_cast<Value>(186400) ;
        (*this)[7] = static_cast<Value>(-94329. + 91056. * sqrt6) / static_cast<Value>(93200) ;
        (*this)[8] = static_cast<Value>(-232192. + 121408. * sqrt6) / static_cast<Value>(17475) ;
        (*this)[9] = static_cast<Value>(101226. - 22764. * sqrt6) / static_cast<Value>(5825) ;
        (*this)[10] = static_cast<Value>(-169984) / static_cast<Value>(9087) ;
        (*this)[11] = static_cast<Value>(-87) / static_cast<Value>(30290) ;
        (*this)[12] = static_cast<Value>(492) / static_cast<Value>(1165) ;
        (*this)[13] = static_cast<Value>(0) ;
        (*this)[14] = static_cast<Value>(1260) / static_cast<Value>(233) ;
    }
} ;

// 8th order coefficients
template <typename Value=double>
struct Verner89_b : boost::array<Value, 16>
{
    Verner89_b(void)
    {
        (*this)[0] = static_cast<Value>(103) / static_cast<Value>(1680) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(0) ;
        (*this)[6] = static_cast<Value>(0) ;
        (*this)[7] = static_cast<Value>(-27) / static_cast<Value>(140) ;
        (*this)[8] = static_cast<Value>(76) / static_cast<Value>(105) ;
        (*this)[9] = static_cast<Value>(-201) / static_cast<Value>(280) ;
        (*this)[10] = static_cast<Value>(1024) / static_cast<Value>(1365) ;
        (*this)[11] = static_cast<Value>(3) / static_cast<Value>(7280) ;
        (*this)[12] = static_cast<Value>(12) / static_cast<Value>(35) ;
        (*this)[13] = static_cast<Value>(9) / static_cast<Value>(280) ;
        (*this)[14] = static_cast<Value>(0) ;
        (*this)[15] = static_cast<Value>(0) ;
    }
} ;

// These values correspond to the difference of the 9th order and 8th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Verner89_db : boost::array<Value, 16>
{
    Verner89_db(void)
    {
        (*this)[0] = static_cast<Value>(-1911) / static_cast<Value>(109200) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(0) ;
        (*this)[6] = static_cast<Value>(0) ;
        (*this)[7] = static_cast<Value>(34398) / static_cast<Value>(109200) ;
        (*this)[8] = static_cast<Value>(-61152) / static_cast<Value>(109200) ;
        (*this)[9] = static_cast<Value>(114660) / static_cast<Value>(109200) ;
        (*this)[10] = static_cast<Value>(-114688) / static_cast<Value>(109200) ;
        (*this)[11] = static_cast<Value>(-63) / static_cast<Value>(109200) ;
        (*this)[12] = static_cast<Value>(-13104) / static_cast<Value>(109200) ;
        (*this)[13] = static_cast<Value>(-3510) / static_cast<Value>(109200) ;
        (*this)[14] = static_cast<Value>(39312) / static_cast<Value>(109200) ;
        (*this)[15] = static_cast<Value>(6058) / static_cast<Value>(109200) ;
    }
} ;

template <typename Value=double>
struct Verner89_c : boost::array<Value, 16>
{
    Verner89_c(void)
    {
        const double sqrt6 = std::sqrt(6.) ;
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(12) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(9) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[4] = static_cast<Value>(2. * (1. + sqrt6)) / static_cast<Value>(15) ;
        (*this)[5] = static_cast<Value>(6. + sqrt6) / static_cast<Value>(15) ;
        (*this)[6] = static_cast<Value>(6. - sqrt6) / static_cast<Value>(15) ;
        (*this)[7] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[8] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[9] = static_cast<Value>(1) / static_cast<Value>(3) ;
        (*this)[10] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[11] = static_cast<Value>(4) / static_cast<Value>(3) ;
        (*this)[12] = static_cast<Value>(5) / static_cast<Value>(6) ;
        (*this)[13] = static_cast<Value>(1) ;
        (*this)[14] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[15] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner89 :
            public boostode::explicit_error_generic_rk<16, 8, 8, 9, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<16, 8, 8, 9, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner89(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner89_a1<Value>(),
                                                     Verner89_a2<Value>(),
                                                     Verner89_a3<Value>(),
                                                     Verner89_a4<Value>(),
                                                     Verner89_a5<Value>(),
                                                     Verner89_a6<Value>(),
                                                     Verner89_a7<Value>(),
                                                     Verner89_a8<Value>(),
                                                     Verner89_a9<Value>(),
                                                     Verner89_a10<Value>(),
                                                     Verner89_a11<Value>(),
                                                     Verner89_a12<Value>(),
                                                     Verner89_a13<Value>(),
                                                     Verner89_a14<Value>(),
                                                     Verner89_a15<Value>()),
                          Verner89_b<Value>(), Verner89_db<Value>(),
                          Verner89_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner89_hpp
