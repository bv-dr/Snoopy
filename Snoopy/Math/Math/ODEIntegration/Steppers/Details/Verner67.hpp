#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner67_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner67_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Verner67_a1 : boost::array<Value, 1>
{
    Verner67_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(12) ;
    }
} ;

template <typename Value=double>
struct Verner67_a2 : boost::array<Value, 2>
{
    Verner67_a2(void)
    {
        (*this)[0] = static_cast<Value>(0.) ;
        (*this)[1] = static_cast<Value>(1.) / static_cast<Value>(6.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a3 : boost::array<Value, 3>
{
    Verner67_a3(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(16.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3.) / static_cast<Value>(16.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a4 : boost::array<Value, 4>
{
    Verner67_a4(void)
    {
        (*this)[0] = static_cast<Value>(21.) / static_cast<Value>(16.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-81.) / static_cast<Value>(16.) ;
        (*this)[3] = static_cast<Value>(72.) / static_cast<Value>(16.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a5 : boost::array<Value, 5>
{
    Verner67_a5(void)
    {
        (*this)[0] = static_cast<Value>(1344688.) / static_cast<Value>(250563.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-5127552.) / static_cast<Value>(250563.) ;
        (*this)[3] = static_cast<Value>(4096896.) / static_cast<Value>(250563.) ;
        (*this)[4] = static_cast<Value>(-78208.) / static_cast<Value>(250563.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a6 : boost::array<Value, 6>
{
    Verner67_a6(void)
    {
        (*this)[0] = static_cast<Value>(-341549.) / static_cast<Value>(234624.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(1407744.) / static_cast<Value>(234624.) ;
        (*this)[3] = static_cast<Value>(-1018368.) / static_cast<Value>(234624.) ;
        (*this)[4] = static_cast<Value>(84224.) / static_cast<Value>(234624.) ;
        (*this)[5] = static_cast<Value>(-14739.) / static_cast<Value>(234624.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a7 : boost::array<Value, 7>
{
    Verner67_a7(void)
    {
        (*this)[0] = static_cast<Value>(-381875.) / static_cast<Value>(136864.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(1642368.) / static_cast<Value>(136864.) ;
        (*this)[3] = static_cast<Value>(-1327872.) / static_cast<Value>(136864.) ;
        (*this)[4] = static_cast<Value>(72192.) / static_cast<Value>(136864.) ;
        (*this)[5] = static_cast<Value>(14739.) / static_cast<Value>(136864.) ;
        (*this)[6] = static_cast<Value>(117312.) / static_cast<Value>(136864.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a8 : boost::array<Value, 8>
{
    Verner67_a8(void)
    {
        (*this)[0] = static_cast<Value>(-2070757.) / static_cast<Value>(16755336.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(9929088.) / static_cast<Value>(16755336.) ;
        (*this)[3] = static_cast<Value>(584064.) / static_cast<Value>(16755336.) ;
        (*this)[4] = static_cast<Value>(3023488.) / static_cast<Value>(16755336.) ;
        (*this)[5] = static_cast<Value>(-447083.) / static_cast<Value>(16755336.) ;
        (*this)[6] = static_cast<Value>(151424.) / static_cast<Value>(16755336.) ;
        (*this)[7] = static_cast<Value>(0.) ;
    }
} ;

template <typename Value=double>
struct Verner67_a9 : boost::array<Value, 9>
{
    Verner67_a9(void)
    {
        (*this)[0] = static_cast<Value>(130521209.) / static_cast<Value>(10743824.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-499279872.) / static_cast<Value>(10743824.) ;
        (*this)[3] = static_cast<Value>(391267968.) / static_cast<Value>(10743824.) ;
        (*this)[4] = static_cast<Value>(13012608.) / static_cast<Value>(10743824.) ;
        (*this)[5] = static_cast<Value>(-3522621.) / static_cast<Value>(10743824.) ;
        (*this)[6] = static_cast<Value>(9033024.) / static_cast<Value>(10743824.) ;
        (*this)[7] = static_cast<Value>(0.) ;
        (*this)[8] = static_cast<Value>(-30288492.) / static_cast<Value>(10743824.) ;
    }
} ;

// 6th order coefficients
template <typename Value=double>
struct Verner67_b : boost::array<Value, 10>
{
    Verner67_b(void)
    {
        (*this)[0] = static_cast<Value>(7.) / static_cast<Value>(90.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(32.) / static_cast<Value>(90.) ;
        (*this)[4] = static_cast<Value>(32.) / static_cast<Value>(90.) ;
        (*this)[5] = static_cast<Value>(0.) ;
        (*this)[6] = static_cast<Value>(12.) / static_cast<Value>(90.) ;
        (*this)[7] = static_cast<Value>(7.) / static_cast<Value>(90.) ;
        (*this)[8] = static_cast<Value>(0.) ;
        (*this)[9] = static_cast<Value>(0.) ;
    }
} ;

// These values correspond to the difference of the 7th order and 6th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Verner67_db : boost::array<Value, 10>
{
    Verner67_db(void)
    {
        (*this)[0] = static_cast<Value>(-1090635.) / static_cast<Value>(172448640.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(9504768.) / static_cast<Value>(172448640.) ;
        (*this)[4] = static_cast<Value>(-171816960.) / static_cast<Value>(172448640.) ;
        (*this)[5] = static_cast<Value>(72412707.) / static_cast<Value>(172448640.) ;
        (*this)[6] = static_cast<Value>(-55840512.) / static_cast<Value>(172448640.) ;
        (*this)[7] = static_cast<Value>(-13412672.) / static_cast<Value>(172448640.) ;
        (*this)[8] = static_cast<Value>(181730952.) / static_cast<Value>(172448640.) ;
        (*this)[9] = static_cast<Value>(-21487648.) / static_cast<Value>(172448640.) ;
    }
} ;

template <typename Value=double>
struct Verner67_c : boost::array<Value, 10>
{
    Verner67_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(12) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[4] = static_cast<Value>(3) / static_cast<Value>(4) ;
        (*this)[5] = static_cast<Value>(16) / static_cast<Value>(17) ;
        (*this)[6] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[7] = static_cast<Value>(1) ;
        (*this)[8] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[9] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner67 :
            public boostode::explicit_error_generic_rk<10, 6, 6, 7, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<10, 6, 6, 7, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner67(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner67_a1<Value>(),
                                                     Verner67_a2<Value>(),
                                                     Verner67_a3<Value>(),
                                                     Verner67_a4<Value>(),
                                                     Verner67_a5<Value>(),
                                                     Verner67_a6<Value>(),
                                                     Verner67_a7<Value>(),
                                                     Verner67_a8<Value>(),
                                                     Verner67_a9<Value>()),
                          Verner67_b<Value>(), Verner67_db<Value>(),
                          Verner67_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner67_hpp
