#ifndef BV_Math_ODEIntegration_Steppers_Details_Wrapper_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Wrapper_hpp

#include <boost/numeric/odeint/stepper/controlled_step_result.hpp>

#include "Math/ODEIntegration/Details.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// The aim of this wrapper is to be able to specify both the system as
// a single derivate function or a pair of derivative and jacobian functions

template <class Stepper>
class ExplicitWrapper
{
public:
    typedef Stepper stepper_type ;

    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef typename stepper_type::resizer_type resizer_type ;
    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_type::stepper_category stepper_category ;
    typedef unsigned short order_type ;

    ExplicitWrapper(const algebra_type & algebra=algebra_type()) :
        stepper_(algebra)
    {
    }

    ExplicitWrapper(Stepper stepper) : stepper_(stepper)
    {
    }

    order_type order(void) const
    {
        return stepper_.order() ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, StateInOut & x, time_type t, time_type dt)
    {
        stepper_.do_step(system, x, t, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, StateInOut & x, time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step(sys.first, x, t, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt)
    {
        stepper_.do_step(system, x, t, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step(sys.first, x, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, StateInOut & x, const DerivIn & dxdt, time_type t, time_type dt)
    {
        stepper_.do_step(system, x, dxdt, t, x, dt) ;
    }

    template <class System, class StateInOut, class DerivIn,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, StateInOut & x, const DerivIn & dxdt, time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step(sys.first, x, dxdt, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step_dxdt_impl(System system, StateInOut & x, const DerivIn & dxdt,
                           time_type t, time_type dt)
    {
        stepper_.do_step_dxdt_impl(system, x, dxdt, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_dxdt_impl(System system, StateInOut & x, const DerivIn & dxdt,
                           time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step_dxdt_impl(sys.first, x, dxdt, t, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out, time_type dt)
    {
        stepper_.do_step(system, in, t, out, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step(sys.first, in, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, time_type dt)
    {
        stepper_.do_step(system, in, dxdt, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step(sys.first, in, dxdt, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step_dxdt_impl(System system, const StateIn & in,
                           const DerivIn & dxdt, time_type t, StateOut & out,
                           time_type dt)
    {
        stepper_.do_step_dxdt_impl(system, in, dxdt, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_dxdt_impl(System system, const StateIn & in,
                           const DerivIn & dxdt, time_type t, StateOut & out,
                           time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        stepper_.do_step_dxdt_impl(sys.first, in, dxdt, t, out, dt) ;
    }

protected:
    stepper_type stepper_ ;
} ;

template <class Stepper>
class ExplicitErrorWrapper : public ExplicitWrapper<Stepper>
{
public:
    typedef Stepper stepper_type ;
    typedef ExplicitWrapper<Stepper> base_type ;

    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef typename stepper_type::resizer_type resizer_type ;
    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_type::stepper_category stepper_category ;
    typedef unsigned short order_type ;
    static const order_type order_value = Stepper::order_value ;
    static const order_type stepper_order_value = Stepper::stepper_order_value ;
    static const order_type error_order_value = Stepper::error_order_value ;


    ExplicitErrorWrapper(const algebra_type & algebra=algebra_type()) :
        base_type(algebra)
    {
    }

    order_type stepper_order(void) const
    {
        return stepper_order_value ;
    }

    order_type error_order(void) const
    {
        return error_order_value ;
    }

    algebra_type & algebra(void)
    {
        return this->stepper_.algebra() ;
    }

    const algebra_type & algebra(void) const
    {
        return this->stepper_.algebra() ;
    }

    template <class System, class StateInOut, class Err,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, StateInOut & x, time_type t, time_type dt, Err & xerr)
    {
        this->stepper_.do_step(system, x, t, dt, xerr) ;
    }

    template <class System, class StateInOut, class Err,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, StateInOut & x, time_type t, time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        this->stepper_.do_step(sys.first, x, t, dt, xerr) ;
    }

    template <class System, class StateInOut, class Err,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt, Err & xerr)
    {
        this->stepper_.do_step(system, x, t, dt, xerr) ;
    }

    template <class System, class StateInOut, class Err,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        this->stepper_.do_step(sys.first, x, t, dt, xerr) ;
    }

    template <class System, class StateInOut, class DerivIn, class Err,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, StateInOut & x, const DerivIn & dxdt, time_type t,
            time_type dt, Err & xerr)
    {
        this->stepper_.do_step(system, x, dxdt, t, dt, xerr) ;
    }

    template <class System, class StateInOut, class DerivIn, class Err,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, StateInOut & x, const DerivIn & dxdt, time_type t,
            time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        this->stepper_.do_step(sys.first, x, dxdt, t, dt, xerr) ;
    }

    template <class System, class StateIn, class StateOut, class Err,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out,
                 time_type dt, Err & xerr)
    {
        this->stepper_.do_step(system, in, t, out, dt, xerr) ;
    }

    template <class System, class StateIn, class StateOut, class Err,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out,
                 time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        this->stepper_.do_step(sys.first, in, t, out, dt, xerr) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut, class Err,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, time_type dt, Err & xerr)
    {
        this->stepper_.do_step(system, in, dxdt, t, out, dt, xerr) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut, class Err,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        this->stepper_.do_step(sys.first, in, dxdt, t, out, dt, xerr) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              class DerivOut, class Err,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, DerivOut & dxdt_out,
                 time_type dt, Err & xerr)
    {
        this->stepper_.do_step(system, in, dxdt, t, out, dxdt_out, dt, xerr) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              class DerivOut, class Err,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, DerivOut & dxdt_out,
                 time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        this->stepper_.do_step(sys.first, in, dxdt, t, out, dxdt_out, dt, xerr) ;
    }

} ;

template <typename Value>
struct DumbErrorChecker
{
    Value eps_abs ;
    Value eps_rel ;
    Value factor_x ;
    Value factor_dxdt ;

    DumbErrorChecker(const Value & e_abs, const Value & e_rel,
                     const Value & f_x, const Value & f_dxdt) :
        eps_abs(e_abs), eps_rel(e_rel), factor_x(f_x), factor_dxdt(f_dxdt)
    {
    }

} ;

template <typename Time>
struct DumbStepAdjuster
{
    Time max_dt ;

    DumbStepAdjuster(const Time & m_dt) : max_dt(m_dt)
    {
    }

} ;

template <typename Stepper>
struct ErrorStepperTraits
{
    typedef typename Stepper::error_checker_type error_checker_type ;
    typedef typename Stepper::step_adjuster_type step_adjuster_type ;
} ;

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer>
struct ErrorStepperTraits<boost::numeric::odeint::bulirsch_stoer<
                         State, Value, Deriv, Time, Algebra, Operations, Resizer
                                                                > >
{
    typedef DumbErrorChecker<Value> error_checker_type ;
    typedef DumbStepAdjuster<Time> step_adjuster_type ;
} ;

template <class Stepper>
class ExplicitControlledWrapper : public ExplicitWrapper<Stepper>
{
public:
    typedef Stepper stepper_type ;
    typedef ExplicitWrapper<Stepper> base_type ;

    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef typename stepper_type::resizer_type resizer_type ;
    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_type::stepper_category stepper_category ;
    typedef unsigned short order_type ;

    typedef typename ErrorStepperTraits<Stepper>::error_checker_type error_checker_type ;
    typedef typename ErrorStepperTraits<Stepper>::step_adjuster_type step_adjuster_type ;

    ExplicitControlledWrapper(const error_checker_type & errCheck,
                              const step_adjuster_type & stepAdjust) :
         base_type(Stepper(errCheck, stepAdjust))
    {
    }

    ExplicitControlledWrapper(Stepper stepper) : base_type(stepper)
    {
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return this->stepper_.try_step(system, x, t, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        return this->stepper_.try_step(sys.first, x, t, dt) ;
    }

    template <class System , class StateInOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, const StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return this->stepper_.try_step(system, x, t, dt) ;
    }

    template <class System , class StateInOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, const StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        return this->stepper_.try_step(sys.first, x, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              const DerivIn & dxdt, time_type & t,
                                              time_type & dt)
    {
        return this->stepper_.try_step(system, x, dxdt, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              const DerivIn & dxdt, time_type & t,
                                              time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        return this->stepper_.try_step(sys.first, x, dxdt, t, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<StateIn, time_type>,
                               boostode::controlled_step_result>::type
    try_step(System system, const StateIn & in, time_type & t, StateOut & out,
             time_type & dt)
    {
        return this->stepper_.try_step(system, in, t, out, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<StateIn, time_type>,
                               boostode::controlled_step_result>::type
    try_step(System system, const StateIn & in, time_type & t, StateOut & out,
             time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        return this->stepper_.try_step(sys.first, in, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, const StateIn & in,
                                              const DerivIn & dxdt, time_type & t,
                                              StateOut & out, time_type & dt)
    {
        return this->stepper_.try_step(system, in, dxdt, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step(System system, const StateIn & in,
                                              const DerivIn & dxdt, time_type & t,
                                              StateOut & out, time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        system_type & sys = system ;
        return this->stepper_.try_step(sys.first, in, dxdt, t, out, dt) ;
    }

} ;

template <class Stepper>
class ExplicitBSWrapper : public ExplicitControlledWrapper<Stepper>
{
public:
    typedef Stepper stepper_type ;
    typedef ExplicitControlledWrapper<Stepper> base_type ;

    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef typename stepper_type::resizer_type resizer_type ;
    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_type::stepper_category stepper_category ;
    typedef unsigned short order_type ;

    typedef typename ErrorStepperTraits<Stepper>::error_checker_type error_checker_type ;
    typedef typename ErrorStepperTraits<Stepper>::step_adjuster_type step_adjuster_type ;

    ExplicitBSWrapper(const error_checker_type & errCheck,
                      const step_adjuster_type & stepAdjust) :
         base_type(Stepper(errCheck.eps_abs, errCheck.eps_rel,
                           errCheck.factor_x, errCheck.factor_dxdt,
                           stepAdjust.max_dt))
    {
    }

    ExplicitBSWrapper(value_type eps_abs=1.e-6,
                      value_type eps_rel=1.e-6,
                      value_type factor_x=1.,
                      value_type factor_dxdt=1.0 ,
                      time_type max_dt=static_cast<time_type>(0)) :
         base_type(Stepper(eps_abs, eps_rel, factor_x, factor_dxdt, max_dt))
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Wrapper_hpp
