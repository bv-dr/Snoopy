#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner78_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner78_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Verner78_a1 : boost::array<Value, 1>
{
    Verner78_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct Verner78_a2 : boost::array<Value, 2>
{
    Verner78_a2(void)
    {
        (*this)[0] = static_cast<Value>(5.) / static_cast<Value>(72.) ;
        (*this)[1] = static_cast<Value>(1.) / static_cast<Value>(72.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a3 : boost::array<Value, 3>
{
    Verner78_a3(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(32.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3.) / static_cast<Value>(32.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a4 : boost::array<Value, 4>
{
    Verner78_a4(void)
    {
        (*this)[0] = static_cast<Value>(106.) / static_cast<Value>(125.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-408.) / static_cast<Value>(125.) ;
        (*this)[3] = static_cast<Value>(352.) / static_cast<Value>(125.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a5 : boost::array<Value, 5>
{
    Verner78_a5(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(48.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(8.) / static_cast<Value>(33.) ;
        (*this)[4] = static_cast<Value>(125.) / static_cast<Value>(528.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a6 : boost::array<Value, 6>
{
    Verner78_a6(void)
    {
        (*this)[0] = static_cast<Value>(-13893.) / static_cast<Value>(26411.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(39936.) / static_cast<Value>(26411.) ;
        (*this)[4] = static_cast<Value>(-64125.) / static_cast<Value>(26411.) ;
        (*this)[5] = static_cast<Value>(60720.) / static_cast<Value>(26411.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a7 : boost::array<Value, 7>
{
    Verner78_a7(void)
    {
        (*this)[0] = static_cast<Value>(37.) / static_cast<Value>(392.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(0.) ;
        (*this)[4] = static_cast<Value>(1625.) / static_cast<Value>(9408.) ;
        (*this)[5] = static_cast<Value>(-2.) / static_cast<Value>(15.) ;
        (*this)[6] = static_cast<Value>(61.) / static_cast<Value>(6720.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a8 : boost::array<Value, 8>
{
    Verner78_a8(void)
    {
        (*this)[0] = static_cast<Value>(17176.) / static_cast<Value>(25515.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(-47104.) / static_cast<Value>(25515.) ;
        (*this)[4] = static_cast<Value>(1325.) / static_cast<Value>(504.) ;
        (*this)[5] = static_cast<Value>(-41792.) / static_cast<Value>(25515.) ;
        (*this)[6] = static_cast<Value>(20237.) / static_cast<Value>(145800.) ;
        (*this)[7] = static_cast<Value>(4312.) / static_cast<Value>(6075.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a9 : boost::array<Value, 9>
{
    Verner78_a9(void)
    {
        (*this)[0] = static_cast<Value>(-23834.) / static_cast<Value>(180075.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(-77824.) / static_cast<Value>(1980825.) ;
        (*this)[4] = static_cast<Value>(-636635.) / static_cast<Value>(633864.) ;
        (*this)[5] = static_cast<Value>(254048.) / static_cast<Value>(300125.) ;
        (*this)[6] = static_cast<Value>(-183.) / static_cast<Value>(7000.) ;
        (*this)[7] = static_cast<Value>(8.) / static_cast<Value>(11.) ;
        (*this)[8] = static_cast<Value>(-324.) / static_cast<Value>(3773.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a10 : boost::array<Value, 10>
{
    Verner78_a10(void)
    {
        (*this)[0] = static_cast<Value>(12733.) / static_cast<Value>(7600.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(-20032.) / static_cast<Value>(5225.) ;
        (*this)[4] = static_cast<Value>(456485.) / static_cast<Value>(80256.) ;
        (*this)[5] = static_cast<Value>(-42599.) / static_cast<Value>(7125.) ;
        (*this)[6] = static_cast<Value>(339227.) / static_cast<Value>(912000.) ;
        (*this)[7] = static_cast<Value>(-1029.) / static_cast<Value>(4180.) ;
        (*this)[8] = static_cast<Value>(1701.) / static_cast<Value>(1408.) ;
        (*this)[9] = static_cast<Value>(5145.) / static_cast<Value>(2432.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a11 : boost::array<Value, 11>
{
    Verner78_a11(void)
    {
        (*this)[0] = static_cast<Value>(-27061.) / static_cast<Value>(204120.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(40448.) / static_cast<Value>(280665.) ;
        (*this)[4] = static_cast<Value>(-1353775.) / static_cast<Value>(1197504.) ;
        (*this)[5] = static_cast<Value>(17662.) / static_cast<Value>(25515.) ;
        (*this)[6] = static_cast<Value>(-71687.) / static_cast<Value>(1166400.) ;
        (*this)[7] = static_cast<Value>(98.) / static_cast<Value>(225.) ;
        (*this)[8] = static_cast<Value>(1.) / static_cast<Value>(16.) ;
        (*this)[9] = static_cast<Value>(3773.) / static_cast<Value>(11664.) ;
        (*this)[10] = static_cast<Value>(0.) ;
    }
} ;

template <typename Value=double>
struct Verner78_a12 : boost::array<Value, 12>
{
    Verner78_a12(void)
    {
        (*this)[0] = static_cast<Value>(11203.) / static_cast<Value>(8680.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(-38144.) / static_cast<Value>(11935.) ;
        (*this)[4] = static_cast<Value>(2354425.) / static_cast<Value>(458304.) ;
        (*this)[5] = static_cast<Value>(-84046.) / static_cast<Value>(16275.) ;
        (*this)[6] = static_cast<Value>(673309.) / static_cast<Value>(1636800.) ;
        (*this)[7] = static_cast<Value>(4704.) / static_cast<Value>(8525.) ;
        (*this)[8] = static_cast<Value>(9477.) / static_cast<Value>(10912.) ;
        (*this)[9] = static_cast<Value>(-1029.) / static_cast<Value>(992.) ;
        (*this)[10] = static_cast<Value>(0.) ;
        (*this)[11] = static_cast<Value>(729.) / static_cast<Value>(341.) ;
    }
} ;

// 7th order coefficients
template <typename Value=double>
struct Verner78_b : boost::array<Value, 13>
{
    Verner78_b(void)
    {
        (*this)[0] = static_cast<Value>(13.) / static_cast<Value>(288.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(0.) ;
        (*this)[4] = static_cast<Value>(0.) ;
        (*this)[5] = static_cast<Value>(32.) / static_cast<Value>(125.) ;
        (*this)[6] = static_cast<Value>(31213.) / static_cast<Value>(144000.) ;
        (*this)[7] = static_cast<Value>(2401.) / static_cast<Value>(12375.) ;
        (*this)[8] = static_cast<Value>(1701.) / static_cast<Value>(14080.) ;
        (*this)[9] = static_cast<Value>(2401.) / static_cast<Value>(19200.) ;
        (*this)[10] = static_cast<Value>(19.) / static_cast<Value>(450.) ;
        (*this)[11] = static_cast<Value>(0.) ;
        (*this)[12] = static_cast<Value>(0.) ;
    }
} ;

// These values correspond to the difference of the 8th order and 7th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Verner78_db : boost::array<Value, 13>
{
    Verner78_db(void)
    {
        (*this)[0] = static_cast<Value>(-6600.) / static_cast<Value>(3168000.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(0.) ;
        (*this)[4] = static_cast<Value>(0.) ;
        (*this)[5] = static_cast<Value>(-135168.) / static_cast<Value>(3168000.) ;
        (*this)[6] = static_cast<Value>(-14406.) / static_cast<Value>(3168000.) ;
        (*this)[7] = static_cast<Value>(57624.) / static_cast<Value>(3168000.) ;
        (*this)[8] = static_cast<Value>(54675.) / static_cast<Value>(3168000.) ;
        (*this)[9] = static_cast<Value>(-396165.) / static_cast<Value>(3168000.) ;
        (*this)[10] = static_cast<Value>(-133760.) / static_cast<Value>(3168000.) ;
        (*this)[11] = static_cast<Value>(437400.) / static_cast<Value>(3168000.) ;
        (*this)[12] = static_cast<Value>(136400.) / static_cast<Value>(3168000.) ;
    }
} ;

template <typename Value=double>
struct Verner78_c : boost::array<Value, 13>
{
    Verner78_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(12) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(8) ;
        (*this)[4] = static_cast<Value>(2) / static_cast<Value>(5) ;
        (*this)[5] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[6] = static_cast<Value>(6) / static_cast<Value>(7) ;
        (*this)[7] = static_cast<Value>(1) / static_cast<Value>(7) ;
        (*this)[8] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[9] = static_cast<Value>(2) / static_cast<Value>(7) ;
        (*this)[10] = static_cast<Value>(1) ;
        (*this)[11] = static_cast<Value>(1) / static_cast<Value>(3) ;
        (*this)[12] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner78 :
            public boostode::explicit_error_generic_rk<13, 7, 7, 8, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<13, 7, 7, 8, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner78(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner78_a1<Value>(),
                                                     Verner78_a2<Value>(),
                                                     Verner78_a3<Value>(),
                                                     Verner78_a4<Value>(),
                                                     Verner78_a5<Value>(),
                                                     Verner78_a6<Value>(),
                                                     Verner78_a7<Value>(),
                                                     Verner78_a8<Value>(),
                                                     Verner78_a9<Value>(),
                                                     Verner78_a10<Value>(),
                                                     Verner78_a11<Value>(),
                                                     Verner78_a12<Value>()),
                          Verner78_b<Value>(), Verner78_db<Value>(),
                          Verner78_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner78_hpp
