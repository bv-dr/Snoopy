#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner76_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner76_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

#include "Math/ODEIntegration/Steppers/Details/Verner67.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

// 7th order coefficients
template <typename Value=double>
struct Verner76_b : boost::array<Value, 10>
{
    Verner76_b(void)
    {
        (*this)[0] = static_cast<Value>(2881.) / static_cast<Value>(40320.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(1216.) / static_cast<Value>(2961.) ;
        (*this)[4] = static_cast<Value>(-2624.) / static_cast<Value>(4095.) ;
        (*this)[5] = static_cast<Value>(72412707.) / static_cast<Value>(172448640.) ;
        (*this)[6] = static_cast<Value>(-4.) / static_cast<Value>(21.) ;
        (*this)[7] = static_cast<Value>(0.) ;
        (*this)[8] = static_cast<Value>(181730952.) / static_cast<Value>(172448640.) ;
        (*this)[9] = static_cast<Value>(-21487648.) / static_cast<Value>(172448640.) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner76 :
            public boostode::explicit_error_generic_rk<10, 7, 7, 6, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<10, 7, 7, 6, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner76(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner67_a1<Value>(),
                                                     Verner67_a2<Value>(),
                                                     Verner67_a3<Value>(),
                                                     Verner67_a4<Value>(),
                                                     Verner67_a5<Value>(),
                                                     Verner67_a6<Value>(),
                                                     Verner67_a7<Value>(),
                                                     Verner67_a8<Value>(),
                                                     Verner67_a9<Value>()),
                          Verner76_b<Value>(), Verner67_db<Value>(),
                          Verner67_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner76_hpp
