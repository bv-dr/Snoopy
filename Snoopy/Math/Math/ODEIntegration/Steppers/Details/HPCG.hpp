#ifndef BV_Math_ODEIntegration_Steppers_Details_HPCG_hpp
#define BV_Math_ODEIntegration_Steppers_Details_HPCG_hpp

#include <boost/numeric/odeint/algebra/range_algebra.hpp>
#include <boost/numeric/odeint/algebra/default_operations.hpp>
#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>

#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>
#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>

#include <boost/numeric/odeint/stepper/stepper_categories.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/stepper/extrapolation_stepper.hpp>
#include <boost/numeric/odeint/stepper/base/algebra_stepper_base.hpp>
#include <boost/numeric/odeint/stepper/base/explicit_error_stepper_base.hpp>
#include <boost/numeric/odeint/stepper/detail/rotating_buffer.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/StepAdjuster.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template<
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer = boostode::initially_resizer,
    std::size_t Steps=4,
    std::size_t StorageSize=4,
    class InitializingStepper=boostode::runge_kutta4_classic<
                                                   State, Value, Deriv, Time,
                                                   Algebra, Operations, Resizer
                                                            >
        >
class HPCG : public boostode::explicit_error_stepper_base<
                HPCG<State, Value, Deriv, Time, Algebra, Operations,
                     Resizer, Steps, StorageSize, InitializingStepper>,
                4, 4, 4, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                                         >
{

public :

    typedef boostode::explicit_error_stepper_base<
                HPCG<State, Value, Deriv, Time, Algebra, Operations,
                     Resizer, Steps, StorageSize, InitializingStepper>,
                4, 4, 4, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                                 > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;
    typedef boostode::stepper_tag stepper_category ;

    typedef InitializingStepper initializing_stepper_type ;

    static constexpr size_t steps = Steps ;
    static constexpr size_t storageSize = StorageSize ;

    typedef boostode::detail::rotating_buffer<wrapped_state_type, storageSize> xStorageType ;
    typedef boostode::detail::rotating_buffer<wrapped_deriv_type, storageSize> dXStorageType ;

    HPCG(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(algebra),
        xStorage_(), dXStorage_(),
        resizer_(),
        initializingStepper_(algebra),
        errorInitialized_(false),
        stepsHistory_(0),
        lastTime_(static_cast<time_type>(0)),
        lastTimeStep_(static_cast<time_type>(0))
    {
    }

    template <class ExplicitStepper, class System, class StateIn>
    void initialize(ExplicitStepper explicitStepper, System system, StateIn & x,
                    time_type & t, time_type dt)
    {
        typename std::remove_reference<ExplicitStepper>::type & stepper = explicitStepper ;
        typename std::remove_reference<System>::type & sys = system ;

        resizer_.adjust_size(x, boostode::detail::bind(
                             &stepper_type::template resize_impl<StateIn>,
                             boostode::detail::ref(*this), boostode::detail::_1
                                                      )) ;

        for(size_t i=0; i+1<steps; ++i)
        {
            if(i != 0)
            {
                xStorage_.rotate() ;
                dXStorage_.rotate() ;
            }
            xStorage_[0].m_v = x ;
            sys(x, dXStorage_[0].m_v, t) ;
            stepper.do_step_dxdt_impl(system, x, dXStorage_[0].m_v, t, dt) ;
            t += dt ;
        }
        stepsHistory_ = steps ;
    }

    template <class System, class StateIn>
    void initialize(System system, StateIn & x, time_type & t, time_type dt)
    {
        initialize(boostode::detail::ref(initializingStepper_),
                   system, x, t, dt) ;
    }

    void reset(void)
    {
        stepsHistory_ = 0 ;
        errorInitialized_ = false ;
    }

    bool is_initialized(void) const
    {
        return ((stepsHistory_ >= steps) && (errorInitialized_)) ;
    }

    template<class System, class StateIn, class DerivIn, class StateOut,
             class Err>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt, Err &xerr)
    {
        // Perform a normal step
        do_step_impl(system, in, dxdt, t, out, dt) ;
        // Compute the error
        xerr = error_.m_v ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        typename std::remove_reference<System>::type & sys = system ;

        if (resizer_.adjust_size(in, boostode::detail::bind(
                             &stepper_type::template resize_impl<StateIn>,
                             boostode::detail::ref(*this), boostode::detail::_1
                                                           )))
        {
            stepsHistory_ = 0 ;
        }

        if (stepsHistory_ + 1 < steps)
        {
            // FIXME dichotomy to find the correct step ?
            if (t > lastTime_) // FIXME assume dt > 0.
            {
                xStorage_.rotate() ;
                dXStorage_.rotate() ;
                ++stepsHistory_ ;
            }
            xStorage_[0].m_v = in ;
            dXStorage_[0].m_v = dxdt ;
            initializingStepper_.do_step_dxdt_impl(system, in, dxdt, t, out, dt) ;
            lastTimeStep_ = dt ;
        }
        else
        {
            // FIXME assume dt > 0. !
            // we may use boostode::less_with_sign
            if (dt < lastTimeStep_)
            {
                stepHalved_(system, in, dxdt, t, dt) ;
            }
            else if (dt > lastTimeStep_)
            {
                stepDoubled_(system, in, dxdt, t, dt) ;
            }
            if (t > lastTime_) // FIXME assume dt > 0.
            {
                xStorage_.rotate();
                dXStorage_.rotate();
                if (stepsHistory_ < storageSize)
                {
                    ++stepsHistory_ ;
                }
            }
            xStorage_[0].m_v = in ;
            dXStorage_[0].m_v = dxdt ;
            // Prediction
            wrapped_state_type P ;
            // P_{n+1} = X_{n-3} + 4dt/3*(2 dX_{n} - dX_{n-1} + 2 dX_{n-2})
            time_type coefP(static_cast<time_type>(4.*dt)/static_cast<time_type>(3)) ;
            this->m_algebra.for_each5(
                         P.m_v, xStorage_[3].m_v,
                         dXStorage_[0].m_v, dXStorage_[1].m_v,
                         dXStorage_[2].m_v,
                         typename Operations::template scale_sum4<
                                     value_type, time_type, time_type, time_type
                                                                 >(
                                             1., coefP*2., -coefP, coefP*2.
                                                                  )
                                     ) ;
            // Modification
            wrapped_state_type M ;
            // M_{n+1} = P_{n+1} - 112/121 * (P_n - C_n)
            value_type coefM(static_cast<value_type>(-112)/static_cast<value_type>(121)) ;
            // The first time we reach here, error_ is not initialized...
            if (!errorInitialized_)
            {
                M.m_v = P.m_v ;
                errorInitialized_ = true ;
            }
            else
            {
                this->m_algebra.for_each3(
                            M.m_v, P.m_v, error_.m_v,
                            typename Operations::template scale_sum2<
                                                         value_type, value_type
                                                                    >(1., coefM)
                                         ) ;
            }

            // Correction
            // C_{n+1} = 1/8 * (9 X_n - X_{n-2} + 3 dt *(f(M_{n+1}, dX, t_{n+1}) + 2 dX_n - dX_{n-1}))
            wrapped_deriv_type dX ;
            sys(M.m_v, dX.m_v, t+dt) ;
            wrapped_state_type C ;
            value_type coefC(static_cast<value_type>(1)/static_cast<value_type>(8)) ;
            time_type coefC2(static_cast<time_type>(3.*dt)/static_cast<time_type>(8)) ;
            this->m_algebra.for_each6(
                        C.m_v, in, xStorage_[2].m_v, dX.m_v,
                        dXStorage_[0].m_v, dXStorage_[1].m_v,
                        typename Operations::template scale_sum5<
                                              value_type, value_type, time_type,
                                              time_type, time_type
                                                                >(
                                                 9.*coefC, -coefC, coefC2,
                                                 2.*coefC2, -coefC2
                                                                 )
                                     ) ;
            error_.m_v = P.m_v - C.m_v ; // Prediction minus correction

            // Final estimate
            // X_{n+1} = C_{n+1} + 9/121 * (P_{n+1} - C_{n+1})
            value_type coef(static_cast<value_type>(9)/static_cast<value_type>(121)) ;
            this->m_algebra.for_each3(
                        out, C.m_v, error_.m_v,
                        typename Operations::template scale_sum2<
                                                       value_type, value_type
                                                                >(1., coef)
                                     ) ;
        }
        lastTime_ = t ;
        lastTimeStep_ = dt ;
    }

    bool hasAllHistory(void) const
    {
        return stepsHistory_ >= storageSize ;
    }

    template <class StateIn>
    void adjust_size(const StateIn & x)
    {
        resize_impl( x );
        stepper_base_type::adjust_size( x );
    }
private:

    xStorageType xStorage_ ;
    dXStorageType dXStorage_ ;
    resizer_type resizer_ ;
    initializing_stepper_type initializingStepper_ ;
    bool errorInitialized_ ;
    size_t stepsHistory_ ;
    wrapped_state_type error_ ;
    time_type lastTime_ ;
    time_type lastTimeStep_ ;

    template <class StateIn>
    bool resize_impl(const StateIn & x)
    {
        bool resized(false) ;
        for (size_t i=0; i<storageSize; ++i)
        {
            resized |= boostode::adjust_size_by_resizeability(
                            dXStorage_[i], x ,
                            typename boostode::is_resizeable<deriv_type>::type()
                                                             ) ;
            resized |= boostode::adjust_size_by_resizeability(
                            xStorage_[i], x ,
                            typename boostode::is_resizeable<state_type>::type()
                                                             ) ;
        }
        return resized ;
    }

    template <class System, class StateIn, class DerivIn>
    void stepHalved_(System system, const StateIn & in, const DerivIn & dxdt,
                     const time_type & t, const time_type & dt)
    {
        typename std::remove_reference<System>::type & sys = system ;

        // X_{n-1} = 1/256*(80*X_n + 135*X_{n-1} + 40*X_{n-2} + X_{n-3})
        //             + 60*dt/512*(-dX_n + 6*dX_{n-1} + dX_{n-2})
        wrapped_state_type xTmp ;
        value_type coef1(static_cast<value_type>(1)/static_cast<value_type>(256)) ;
        time_type coef2(static_cast<time_type>(60.*dt)/static_cast<time_type>(512)) ;
        this->m_algebra.for_each8(
                    xTmp.m_v, xStorage_[0].m_v, xStorage_[1].m_v,
                    xStorage_[2].m_v, xStorage_[3].m_v, dXStorage_[0].m_v,
                    dXStorage_[1].m_v, dXStorage_[2].m_v,
                    typename Operations::template scale_sum7<
                                value_type, value_type, value_type, value_type,
                                time_type, time_type, time_type
                                                            >(
                                     coef1*80., coef1*135., coef1*40., coef1,
                                     -coef2, coef2*6., coef2
                                                             )
                                 ) ;
        // X_{n-3} = 1/256*(12*X_n + 135*X_{n-1} + 108*X_{n-2} + X_{n-3})
        //              -3*dt/128*(dX_{n} + 18*dX_{n-1} - 9*dX_{n-2})
        time_type coef3(static_cast<time_type>(-3.*dt)/static_cast<time_type>(128)) ;
        this->m_algebra.for_each8(
                    xStorage_[3].m_v, xStorage_[0].m_v, xStorage_[1].m_v,
                    xStorage_[2].m_v, xStorage_[3].m_v, dXStorage_[0].m_v,
                    dXStorage_[1].m_v, dXStorage_[2].m_v,
                    typename Operations::template scale_sum7<
                                value_type, value_type, value_type, value_type,
                                time_type, time_type, time_type
                                                            >(
                                     coef1*12., coef1*135., coef1*108., coef1,
                                     coef3, coef3*18., -coef3*9.
                                                             )
                                 ) ;
        // X_{n-2} = X_{n-1}
        // dX_{n-2} = dX_{n-1}
        xStorage_[2].m_v = xStorage_[1].m_v ;
        dXStorage_[2].m_v = dXStorage_[1].m_v ;

        // dX_{n-1} = f(X_{n-1})
        wrapped_deriv_type dXTmp ;
        sys(xTmp.m_v, dXTmp.m_v, t-dt) ;
        xStorage_[1].m_v = xTmp.m_v ;
        dXStorage_[1].m_v = dXTmp.m_v ;

        // dX_{n-3} = f(X_{n-3})
        xTmp.m_v = xStorage_[3].m_v ;
        sys(xTmp.m_v, dXTmp.m_v, t-3*dt) ;
        dXStorage_[3].m_v = dXTmp.m_v ;
        // Error adjustment
        value_type coef4(static_cast<value_type>(242)/static_cast<value_type>(27)) ;
        time_type coef5(static_cast<time_type>(-121.*dt)/static_cast<time_type>(36)) ;
        this->m_algebra.for_each7(
                    error_.m_v, xStorage_[0].m_v, xTmp.m_v,
                    dXStorage_[0].m_v, dXStorage_[1].m_v, dXStorage_[2].m_v,
                    dXTmp.m_v,
                    typename Operations::template scale_sum6<
                                           value_type, value_type, time_type,
                                           time_type, time_type, time_type
                                                            >(
                                 coef4, -coef4, coef5, coef5*3., coef5*3., coef5
                                                             )
                                 ) ;
    }

    template <class System, class StateIn, class DerivIn>
    void stepDoubled_(System system, const StateIn & in, const DerivIn & dxdt,
                      const time_type & t, const time_type & dt)
    {
        xStorage_[0].m_v = xStorage_[1].m_v ;
        xStorage_[1].m_v = xStorage_[3].m_v ;
        xStorage_[2].m_v = xStorage_[5].m_v ;
        dXStorage_[0].m_v = dXStorage_[1].m_v ;
        dXStorage_[1].m_v = dXStorage_[3].m_v ;
        dXStorage_[2].m_v = dXStorage_[5].m_v ;
        // Error adjustment
        value_type coef1(static_cast<value_type>(242)/static_cast<value_type>(27)) ;
        time_type coef2(static_cast<time_type>(-121.*dt)/static_cast<time_type>(36)) ;
        this->m_algebra.for_each7(
                    error_.m_v, xStorage_[0].m_v, xStorage_[3].m_v,
                    dXStorage_[0].m_v, dXStorage_[1].m_v, dXStorage_[2].m_v,
                    dXStorage_[3].m_v,
                    typename Operations::template scale_sum6<
                                          value_type, value_type, time_type,
                                          time_type, time_type, time_type
                                                            >(
                                                   coef1, -coef1, coef2,
                                                   3.*coef2, 3.*coef2, coef2
                                                             )
                                 ) ;
    }

} ;


template<
class ErrorStepper,
class ErrorChecker=boostode::default_error_checker<typename ErrorStepper::value_type,
                                                   typename ErrorStepper::algebra_type,
                                                   typename ErrorStepper::operations_type>,
class Resizer=typename ErrorStepper::resizer_type,
class ErrorStepperCategory=typename ErrorStepper::stepper_category
        >
class ControlledHPCG
{
public:
    typedef ErrorStepper stepper_type ;
    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef Resizer resizer_type ;
    typedef ErrorChecker error_checker_type ;
    typedef StepAdjuster2<value_type, time_type> step_adjuster_type ;
    typedef boostode::explicit_controlled_stepper_tag stepper_category ;
    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;

    typedef ControlledHPCG<ErrorStepper, ErrorChecker, Resizer> controlled_stepper_type ;


    ControlledHPCG(
            const error_checker_type & errorChecker=error_checker_type(),
            const step_adjuster_type & stepAdjuster=step_adjuster_type(),
            const stepper_type & stepper=stepper_type()
                  ) :
        stepper_(stepper), errorChecker_(errorChecker),
        stepAdjuster_(stepAdjuster),
        cleanSteps_(0)
    {
    }

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return try_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step(System system, const StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return try_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              const DerivIn &dxdt,
                                              time_type & t, time_type & dt)
    {
        xnewResizer_.adjust_size(x, boostode::detail::bind(
                    &ControlledHPCG::template resize_m_xnew_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                          )) ;
        boostode::controlled_step_result res(try_step(system, x, dxdt, t,
                                                      xnew_.m_v, dt)) ;
        if (res == boostode::success)
        {
            boostode::copy(xnew_.m_v, x) ;
        }
        return res ;
    }

    template <class System, class StateIn, class StateOut>
    typename boost::disable_if<boost::is_same<StateIn, time_type>,
                               boostode::controlled_step_result>::type
    try_step(System system, const StateIn & in, time_type & t, StateOut & out,
             time_type & dt)
    {
        typename std::remove_reference<System>::type & sys = system ;
        dxdtResizer_.adjust_size(in, boostode::detail::bind(
                    &ControlledHPCG::template resize_m_dxdt_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                           )) ;
        sys(in, dxdt_.m_v, t) ;
        return try_step(system, in, dxdt_.m_v, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    boostode::controlled_step_result try_step(System system, const StateIn & in,
                                              const DerivIn & dxdt,
                                              time_type & t, StateOut & out,
                                              time_type & dt)
    {
        if (!stepAdjuster_.check_step_size_limit(dt))
        {
            // given dt was above step size limit - adjust and return fail;
            dt = stepAdjuster_.get_max_dt() ;
            return boostode::fail ;
        }
        // FIXME initial HPCG performed a bisection to get the initial step size...

        xerrResizer_.adjust_size(in, boostode::detail::bind(
                    &ControlledHPCG::template resize_m_xerr_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                           )) ;

        // do one step with error calculation
        stepper_.do_step(system, in, dxdt, t, out, dt, xerr_.m_v) ;

        if (!stepper_.is_initialized())
        {
            t += dt ;
            ++cleanSteps_ ;
            return boostode::success ;
        }

        value_type max_rel_err(errorChecker_.error(stepper_.algebra(), in,
                                                   dxdt, xerr_.m_v, dt)) ;

        if (max_rel_err > 1.0)
        {
            // error too big, decrease step size and reject this step
            dt = stepAdjuster_.decrease_step(dt, max_rel_err,
                                             stepper_.error_order()) ;
            cleanSteps_ = 0 ;
            return boostode::fail ;
        }
        else
        {
            // otherwise, increase step size and accept
            t += dt ;
            if ((!stepper_.hasAllHistory())
                || (cleanSteps_ < stepper_.stepper_order()*2))
                //|| ((cleanSteps_%2) != 0)) // Don't know what this is for...
            {
                ++cleanSteps_ ;
                return boostode::success ;
            }
            dt = stepAdjuster_.increase_step(dt, max_rel_err,
                                             stepper_.stepper_order()) ;
            cleanSteps_ = 0 ;
            return boostode::success ;
        }
    }

    template <class StateType>
    void adjust_size(const StateType & x)
    {
        resize_m_xerr_impl(x) ;
        resize_m_dxdt_impl(x) ;
        resize_m_xnew_impl(x) ;
        stepper_.adjust_size(x) ;
    }

    stepper_type & stepper(void)
    {
        return stepper_ ;
    }

    const stepper_type & stepper(void) const
    {
        return stepper_ ;
    }

private:

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step_v1(System system, StateInOut & x,
                                                 time_type & t, time_type & dt)
    {
        typename std::remove_reference<System>::type & sys = system ;
        dxdtResizer_.adjust_size(x, boostode::detail::bind(
                    &ControlledHPCG::template resize_m_dxdt_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                          )) ;
        sys(x, dxdt_.m_v, t) ;
        return try_step(system, x, dxdt_.m_v, t, dt) ;
    }

    template <class StateIn>
    bool resize_m_xerr_impl(const StateIn & x)
    {
        return boostode::adjust_size_by_resizeability(
                  xerr_, x, typename boostode::is_resizeable<state_type>::type()
                                                     ) ;
    }

    template <class StateIn>
    bool resize_m_dxdt_impl(const StateIn & x)
    {
        return boostode::adjust_size_by_resizeability(
                  dxdt_, x, typename boostode::is_resizeable<deriv_type>::type()
                                                     ) ;
    }

    template <class StateIn>
    bool resize_m_xnew_impl(const StateIn & x)
    {
        return boostode::adjust_size_by_resizeability(
                  xnew_, x, typename boostode::is_resizeable<state_type>::type()
                                                     ) ;
    }

    stepper_type stepper_ ;
    error_checker_type errorChecker_ ;
    step_adjuster_type stepAdjuster_ ;

    resizer_type dxdtResizer_ ;
    resizer_type xerrResizer_ ;
    resizer_type xnewResizer_ ;

    wrapped_deriv_type dxdt_ ;
    wrapped_state_type xerr_ ;
    wrapped_state_type xnew_ ;

    std::size_t cleanSteps_ ;
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_HPCG_hpp
