#ifndef BV_Math_ODEIntegration_Steppers_Details_Fehlberg78_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Fehlberg78_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Fehlberg78_a1 : boost::array<Value, 1>
{
    Fehlberg78_a1(void)
    {
        (*this)[0] = static_cast<Value>(2) / static_cast<Value>(27) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a2 : boost::array<Value, 2>
{
    Fehlberg78_a2(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(36.) ;
        (*this)[1] = static_cast<Value>(3.) / static_cast<Value>(36.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a3 : boost::array<Value, 3>
{
    Fehlberg78_a3(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(24.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3.) / static_cast<Value>(24.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a4 : boost::array<Value, 4>
{
    Fehlberg78_a4(void)
    {
        (*this)[0] = static_cast<Value>(20.) / static_cast<Value>(48.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-75.) / static_cast<Value>(48.) ;
        (*this)[3] = static_cast<Value>(75.) / static_cast<Value>(48.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a5 : boost::array<Value, 5>
{
    Fehlberg78_a5(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(20.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(5.) / static_cast<Value>(20.) ;
        (*this)[4] = static_cast<Value>(4.) / static_cast<Value>(20.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a6 : boost::array<Value, 6>
{
    Fehlberg78_a6(void)
    {
        (*this)[0] = static_cast<Value>(-25.) / static_cast<Value>(108.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(125.) / static_cast<Value>(108.) ;
        (*this)[4] = static_cast<Value>(-260.) / static_cast<Value>(108.) ;
        (*this)[5] = static_cast<Value>(250.) / static_cast<Value>(108.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a7 : boost::array<Value, 7>
{
    Fehlberg78_a7(void)
    {
        (*this)[0] = static_cast<Value>(31.) / static_cast<Value>(300.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(61.) / static_cast<Value>(225.) ;
        (*this)[5] = static_cast<Value>(-2.) / static_cast<Value>(9.) ;
        (*this)[6] = static_cast<Value>(13.) / static_cast<Value>(900.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a8 : boost::array<Value, 8>
{
    Fehlberg78_a8(void)
    {
        (*this)[0] = static_cast<Value>(2.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-53.) / static_cast<Value>(6.) ;
        (*this)[4] = static_cast<Value>(704.) / static_cast<Value>(45.) ;
        (*this)[5] = static_cast<Value>(-107.) / static_cast<Value>(9.) ;
        (*this)[6] = static_cast<Value>(67.) / static_cast<Value>(90.) ;
        (*this)[7] = static_cast<Value>(3.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a9 : boost::array<Value, 9>
{
    Fehlberg78_a9(void)
    {
        (*this)[0] = static_cast<Value>(-91.) / static_cast<Value>(108.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(23.) / static_cast<Value>(108.) ;
        (*this)[4] = static_cast<Value>(-976.) / static_cast<Value>(135.) ;
        (*this)[5] = static_cast<Value>(311.) / static_cast<Value>(54.) ;
        (*this)[6] = static_cast<Value>(-19.) / static_cast<Value>(60.) ;
        (*this)[7] = static_cast<Value>(17.) / static_cast<Value>(6.) ;
        (*this)[8] = static_cast<Value>(-1.) / static_cast<Value>(12.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a10 : boost::array<Value, 10>
{
    Fehlberg78_a10(void)
    {
        (*this)[0] = static_cast<Value>(2383.) / static_cast<Value>(4100.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-341.) / static_cast<Value>(164.) ;
        (*this)[4] = static_cast<Value>(4496.) / static_cast<Value>(1025.) ;
        (*this)[5] = static_cast<Value>(-301.) / static_cast<Value>(82.) ;
        (*this)[6] = static_cast<Value>(2133.) / static_cast<Value>(4100.) ;
        (*this)[7] = static_cast<Value>(45.) / static_cast<Value>(82.) ;
        (*this)[8] = static_cast<Value>(45.) / static_cast<Value>(164.) ;
        (*this)[9] = static_cast<Value>(18.) / static_cast<Value>(41.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a11 : boost::array<Value, 11>
{
    Fehlberg78_a11(void)
    {
        (*this)[0] = static_cast<Value>(3.) / static_cast<Value>(205.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-6.) / static_cast<Value>(41.) ;
        (*this)[6] = static_cast<Value>(-3.) / static_cast<Value>(205.) ;
        (*this)[7] = static_cast<Value>(-3.) / static_cast<Value>(41.) ;
        (*this)[8] = static_cast<Value>(3.) / static_cast<Value>(41.) ;
        (*this)[9] = static_cast<Value>(6.) / static_cast<Value>(41.) ;
        (*this)[10] = static_cast<Value>(0.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_a12 : boost::array<Value, 12>
{
    Fehlberg78_a12(void)
    {
        (*this)[0] = static_cast<Value>(-1777.) / static_cast<Value>(4100.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-341.) / static_cast<Value>(164.) ;
        (*this)[4] = static_cast<Value>(4496.) / static_cast<Value>(1025.) ;
        (*this)[5] = static_cast<Value>(-289.) / static_cast<Value>(82.) ;
        (*this)[6] = static_cast<Value>(2193.) / static_cast<Value>(4100.) ;
        (*this)[7] = static_cast<Value>(51.) / static_cast<Value>(82.) ;
        (*this)[8] = static_cast<Value>(33.) / static_cast<Value>(164.) ;
        (*this)[9] = static_cast<Value>(12.) / static_cast<Value>(41.) ;
        (*this)[10] = static_cast<Value>(0.) ;
        (*this)[11] = static_cast<Value>(1.) ;
    }
} ;

// 7th order coefficients
template <typename Value=double>
struct Fehlberg78_b : boost::array<Value, 13>
{
    Fehlberg78_b(void)
    {
        (*this)[0] = static_cast<Value>(41.) / static_cast<Value>(840.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(0.) ;
        (*this)[4] = static_cast<Value>(0.) ;
        (*this)[5] = static_cast<Value>(34.) / static_cast<Value>(105.) ;
        (*this)[6] = static_cast<Value>(9.) / static_cast<Value>(35.) ;
        (*this)[7] = static_cast<Value>(9.) / static_cast<Value>(35.) ;
        (*this)[8] = static_cast<Value>(9.) / static_cast<Value>(280.) ;
        (*this)[9] = static_cast<Value>(9.) / static_cast<Value>(280.) ;
        (*this)[10] = static_cast<Value>(41.) / static_cast<Value>(840.) ;
        (*this)[11] = static_cast<Value>(0.) ;
        (*this)[12] = static_cast<Value>(0.) ;
    }
} ;

// These values correspond to the difference of the 5th order and 4th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Fehlberg78_db : boost::array<Value, 13>
{
    Fehlberg78_db(void)
    {
        (*this)[0] = static_cast<Value>(-41.) / static_cast<Value>(840.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(0.) ;
        (*this)[6] = static_cast<Value>(0.) ;
        (*this)[7] = static_cast<Value>(0.) ;
        (*this)[8] = static_cast<Value>(0.) ;
        (*this)[9] = static_cast<Value>(0.) ;
        (*this)[10] = static_cast<Value>(-41.) / static_cast<Value>(840.) ;
        (*this)[11] = static_cast<Value>(41.) / static_cast<Value>(840.) ;
        (*this)[12] = static_cast<Value>(41.) / static_cast<Value>(840.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg78_c : boost::array<Value, 13>
{
    Fehlberg78_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(2) / static_cast<Value>(27) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(9) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[4] = static_cast<Value>(5) / static_cast<Value>(12) ;
        (*this)[5] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[6] = static_cast<Value>(5) / static_cast<Value>(6) ;
        (*this)[7] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[8] = static_cast<Value>(2.) / static_cast<Value>(3.) ;
        (*this)[9] = static_cast<Value>(1.) / static_cast<Value>(3.) ;
        (*this)[10] = static_cast<Value>(1.) ;
        (*this)[11] = static_cast<Value>(0.) ;
        (*this)[12] = static_cast<Value>(1.) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Fehlberg78 :
            public boostode::explicit_error_generic_rk<13, 7, 7, 8, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<13, 7, 7, 8, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Fehlberg78(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Fehlberg78_a1<Value>(),
                                                     Fehlberg78_a2<Value>(),
                                                     Fehlberg78_a3<Value>(),
                                                     Fehlberg78_a4<Value>(),
                                                     Fehlberg78_a5<Value>(),
                                                     Fehlberg78_a6<Value>(),
                                                     Fehlberg78_a7<Value>(),
                                                     Fehlberg78_a8<Value>(),
                                                     Fehlberg78_a9<Value>(),
                                                     Fehlberg78_a10<Value>(),
                                                     Fehlberg78_a11<Value>(),
                                                     Fehlberg78_a12<Value>()),
                          Fehlberg78_b<Value>(), Fehlberg78_db<Value>(),
                          Fehlberg78_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Fehlberg78_hpp
