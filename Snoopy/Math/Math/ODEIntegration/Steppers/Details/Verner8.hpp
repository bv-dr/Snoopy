#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner8_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner8_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Verner8_a1 : boost::array<Value, 1>
{
    Verner8_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(2) ;
    }
} ;

template <typename Value=double>
struct Verner8_a2 : boost::array<Value, 2>
{
    Verner8_a2(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct Verner8_a3 : boost::array<Value, 3>
{
    Verner8_a3(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(7) ;
        (*this)[1] = static_cast<Value>(-7.-3.*sqrt21) / static_cast<Value>(98.) ;
        (*this)[2] = static_cast<Value>(21.+5.*sqrt21) / static_cast<Value>(49.) ;
    }
} ;

template <typename Value=double>
struct Verner8_a4 : boost::array<Value, 4>
{
    Verner8_a4(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(11+sqrt21) / static_cast<Value>(84) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(18.+4.*sqrt21) / static_cast<Value>(63) ;
        (*this)[3] = static_cast<Value>(21.-sqrt21) / static_cast<Value>(252) ;
    }
} ;

template <typename Value=double>
struct Verner8_a5 : boost::array<Value, 5>
{
    Verner8_a5(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(5.+sqrt21) / static_cast<Value>(48.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(9.+sqrt21) / static_cast<Value>(36) ;
        (*this)[3] = static_cast<Value>(-231.+14.*sqrt21) / static_cast<Value>(360.) ;
        (*this)[4] = static_cast<Value>(63.-7.*sqrt21) / static_cast<Value>(80.) ;
    }
} ;

template <typename Value=double>
struct Verner8_a6 : boost::array<Value, 6>
{
    Verner8_a6(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(10.-sqrt21) / static_cast<Value>(42.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(-432.+92.*sqrt21) / static_cast<Value>(315.) ;
        (*this)[3] = static_cast<Value>(633.-145.*sqrt21) / static_cast<Value>(90.) ;
        (*this)[4] = static_cast<Value>(-504.+115.*sqrt21) / static_cast<Value>(70.) ;
        (*this)[5] = static_cast<Value>(63.-13.*sqrt21) / static_cast<Value>(35.) ;
    }
} ;

template <typename Value=double>
struct Verner8_a7 : boost::array<Value, 7>
{
    Verner8_a7(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(14.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(14.-3.*sqrt21) / static_cast<Value>(126.) ;
        (*this)[5] = static_cast<Value>(13.-3.*sqrt21) / static_cast<Value>(63.) ;
        (*this)[6] = static_cast<Value>(1.) / static_cast<Value>(9.) ;
    }
} ;

template <typename Value=double>
struct Verner8_a8 : boost::array<Value, 8>
{
    Verner8_a8(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(32.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(91.-21.*sqrt21) / static_cast<Value>(576.) ;
        (*this)[5] = static_cast<Value>(11.) / static_cast<Value>(72.) ;
        (*this)[6] = static_cast<Value>(-385.-75.*sqrt21) / static_cast<Value>(1152.) ;
        (*this)[7] = static_cast<Value>(63.+13.*sqrt21) / static_cast<Value>(128.) ;
    }
} ;

template <typename Value=double>
struct Verner8_a9 : boost::array<Value, 9>
{
    Verner8_a9(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(14.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(1.) / static_cast<Value>(9.) ;
        (*this)[5] = static_cast<Value>(-733.-147.*sqrt21) / static_cast<Value>(2205.) ;
        (*this)[6] = static_cast<Value>(515.+111.*sqrt21) / static_cast<Value>(504.) ;
        (*this)[7] = static_cast<Value>(-51.-11.*sqrt21) / static_cast<Value>(56.) ;
        (*this)[8] = static_cast<Value>(132.+28.*sqrt21) / static_cast<Value>(245.) ;
    }
} ;

template <typename Value=double>
struct Verner8_a10 : boost::array<Value, 10>
{
    Verner8_a10(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(-42.+7.*sqrt21) / static_cast<Value>(18.) ;
        (*this)[5] = static_cast<Value>(-18.+28.*sqrt21) / static_cast<Value>(45.) ;
        (*this)[6] = static_cast<Value>(-273.-53.*sqrt21) / static_cast<Value>(72.) ;
        (*this)[7] = static_cast<Value>(301.+53.*sqrt21) / static_cast<Value>(72.) ;
        (*this)[8] = static_cast<Value>(28.-28.*sqrt21) / static_cast<Value>(45.) ;
        (*this)[9] = static_cast<Value>(49.-7*sqrt21) / static_cast<Value>(18.) ;
    }
} ;

template <typename Value=double>
struct Verner8_b : boost::array<Value, 11>
{
    Verner8_b(void)
    {
        (*this)[0] = static_cast<Value>(9.) / static_cast<Value>(180.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(0) ;
        (*this)[6] = static_cast<Value>(0) ;
        (*this)[7] = static_cast<Value>(49.) / static_cast<Value>(180.) ;
        (*this)[8] = static_cast<Value>(64.) / static_cast<Value>(180.) ;
        (*this)[9] = static_cast<Value>(49.) / static_cast<Value>(180.) ;
        (*this)[10] = static_cast<Value>(9.) / static_cast<Value>(180.) ;
    }
} ;

template <typename Value=double>
struct Verner8_c : boost::array<Value, 11>
{
    Verner8_c(void)
    {
        const Value sqrt21(std::sqrt(21.)) ;
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[3] = static_cast<Value>(7.+sqrt21) / static_cast<Value>(14.) ;
        (*this)[4] = static_cast<Value>(7.+sqrt21) / static_cast<Value>(14.) ;
        (*this)[5] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[6] = static_cast<Value>(7.-sqrt21) / static_cast<Value>(14.) ;
        (*this)[7] = static_cast<Value>(7.-sqrt21) / static_cast<Value>(14.) ;
        (*this)[8] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[9] = static_cast<Value>(7.+sqrt21) / static_cast<Value>(14.) ;
        (*this)[10] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner8 :
            public boostode::explicit_generic_rk<11, 6, State, Value, Deriv,
                                                 Time, Algebra, Operations,
                                                 Resizer>
{
public:
    typedef boostode::explicit_generic_rk<11, 6, State, Value, Deriv,
                                          Time, Algebra, Operations,
                                          Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner8(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner8_a1<Value>(),
                                                     Verner8_a2<Value>(),
                                                     Verner8_a3<Value>(),
                                                     Verner8_a4<Value>(),
                                                     Verner8_a5<Value>(),
                                                     Verner8_a6<Value>(),
                                                     Verner8_a7<Value>(),
                                                     Verner8_a8<Value>(),
                                                     Verner8_a9<Value>(),
                                                     Verner8_a10<Value>()),
                          Verner8_b<Value>(),
                          Verner8_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner8_hpp
