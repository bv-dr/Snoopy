#ifndef BV_Math_ODEIntegration_Steppers_Details_RK4Implicit_hpp
#define BV_Math_ODEIntegration_Steppers_Details_RK4Implicit_hpp

#include <cmath>
#include <type_traits>
#include <utility>

#include <boost/numeric/odeint/algebra/range_algebra.hpp>
#include <boost/numeric/odeint/algebra/default_operations.hpp>
#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/ImplicitGenericRK.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template <typename Value=double>
struct RK4Implicit_a1 : std::array<Value, 2>
{
    RK4Implicit_a1(void)
    {
        static const Value sqrt3 = static_cast<Value>(std::sqrt(3.)) ;
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(4)
                    - sqrt3 / static_cast<Value>(6) ;
    }
} ;

template <typename Value=double>
struct RK4Implicit_a2 : std::array<Value, 2>
{
    RK4Implicit_a2(void)
    {
        static const Value sqrt3 = static_cast<Value>(std::sqrt(3.)) ;
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4)
                    + sqrt3 / static_cast<Value>(6) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct RK4Implicit_b : std::array<Value, 2>
{
    RK4Implicit_b(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(2) ;
    }
} ;

template <typename Value=double>
struct RK4Implicit_c : std::array<Value, 2>
{
    RK4Implicit_c(void)
    {
        static const Value sqrt3 = static_cast<Value>(std::sqrt(3.)) ;
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(2)
                    - sqrt3 / static_cast<Value>(6) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(2)
                    + sqrt3 / static_cast<Value>(6) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class RK4Implicit : public implicit_generic_rk<2, 4, State, Value, Deriv,
                                               Time, Algebra, Operations,
                                               Resizer>
{
public:
    typedef implicit_generic_rk<2, 4, State, Value, Deriv, Time,
                                Algebra, Operations, Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    RK4Implicit(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(RK4Implicit_a1<Value>(),
                                                     RK4Implicit_a2<Value>()),
                          RK4Implicit_b<Value>(),
                          RK4Implicit_c<Value>(),
                          algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_RK4Implicit_hpp
