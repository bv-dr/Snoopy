#ifndef BV_Math_ODEIntegration_Steppers_Details_PrinceDormand87_hpp
#define BV_Math_ODEIntegration_Steppers_Details_PrinceDormand87_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...
// FIXME the coefficients provided are approximated ! (those given by the GSL)
// The real coefficients can be found in the following paper, but the
// precision may not be reached...
// High order embedded Runge-Kutta formulae, by P.J.Prince and J.R.Dormand,
// Journal of Computational and Applied Mathematics, vol. 7, 1981, pages 67-75
// or in following document:
// www.peterstone.name/Maplepgs/Maple/nmthds/RKcoeff/Runge_Kutta_schemes/RK8/RKcoeff8d_1.pdf

template <typename Value=double>
struct PrinceDormand87_a1 : boost::array<Value, 1>
{
    PrinceDormand87_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(18) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a2 : boost::array<Value, 2>
{
    PrinceDormand87_a2(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(48.) ;
        (*this)[1] = static_cast<Value>(1.) / static_cast<Value>(16.) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a3 : boost::array<Value, 3>
{
    PrinceDormand87_a3(void)
    {
        (*this)[0] = static_cast<Value>(1.) / static_cast<Value>(32.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3.) / static_cast<Value>(32.) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a4 : boost::array<Value, 4>
{
    PrinceDormand87_a4(void)
    {
        (*this)[0] = static_cast<Value>(5) / static_cast<Value>(16) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(-75) / static_cast<Value>(64.) ;
        (*this)[3] = static_cast<Value>(75) / static_cast<Value>(64.) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a5 : boost::array<Value, 5>
{
    PrinceDormand87_a5(void)
    {
        (*this)[0] = static_cast<Value>(3) / static_cast<Value>(80) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(3) / static_cast<Value>(16) ;
        (*this)[4] = static_cast<Value>(3) / static_cast<Value>(20) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a6 : boost::array<Value, 6>
{
    PrinceDormand87_a6(void)
    {
        (*this)[0] = static_cast<Value>(29443841) / static_cast<Value>(614563906) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(77736538) / static_cast<Value>(692538347) ;
        (*this)[4] = static_cast<Value>(-28693883.) / static_cast<Value>(1125000000) ;
        (*this)[5] = static_cast<Value>(23124283) / static_cast<Value>(1800000000) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a7 : boost::array<Value, 7>
{
    PrinceDormand87_a7(void)
    {
        (*this)[0] = static_cast<Value>(16016141) / static_cast<Value>(946692911) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(61564180) / static_cast<Value>(158732637) ;
        (*this)[4] = static_cast<Value>(22789713) / static_cast<Value>(633445777) ;
        (*this)[5] = static_cast<Value>(545815736) / static_cast<Value>(2771057229) ;
        (*this)[6] = static_cast<Value>(-180193667.) / static_cast<Value>(1043307555) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a8 : boost::array<Value, 8>
{
    PrinceDormand87_a8(void)
    {
        (*this)[0] = static_cast<Value>(39632708) / static_cast<Value>(573591083) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-433636366.) / static_cast<Value>(683701615) ;
        (*this)[4] = static_cast<Value>(-421739975.) / static_cast<Value>(2616292301) ;
        (*this)[5] = static_cast<Value>(100302831) / static_cast<Value>(723423059) ;
        (*this)[6] = static_cast<Value>(790204164) / static_cast<Value>(839813087) ;
        (*this)[7] = static_cast<Value>(800635310) / static_cast<Value>(3783071287) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a9 : boost::array<Value, 9>
{
    PrinceDormand87_a9(void)
    {
        (*this)[0] = static_cast<Value>(246121993) / static_cast<Value>(1340847787) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-37695042795.) / static_cast<Value>(15268766246) ;
        (*this)[4] = static_cast<Value>(-309121744.) / static_cast<Value>(1061227803) ;
        (*this)[5] = static_cast<Value>(-12992083.) / static_cast<Value>(490766935) ;
        (*this)[6] = static_cast<Value>(6005943493) / static_cast<Value>(2108947869) ;
        (*this)[7] = static_cast<Value>(393006217) / static_cast<Value>(1396673457) ;
        (*this)[8] = static_cast<Value>(123872331) / static_cast<Value>(1001029789) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a10 : boost::array<Value, 10>
{
    PrinceDormand87_a10(void)
    {
        (*this)[0] = static_cast<Value>(-1028468189.) / static_cast<Value>(846180014) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(8478235783) / static_cast<Value>(508512852) ;
        (*this)[4] = static_cast<Value>(1311729495) / static_cast<Value>(1432422823) ;
        (*this)[5] = static_cast<Value>(-10304129995.) / static_cast<Value>(1701304382) ;
        (*this)[6] = static_cast<Value>(-48777925059.) / static_cast<Value>(3047939560) ;
        (*this)[7] = static_cast<Value>(15336726248) / static_cast<Value>(1032824649) ;
        (*this)[8] = static_cast<Value>(-45442868181.) / static_cast<Value>(3398467696) ;
        (*this)[9] = static_cast<Value>(3065993473) / static_cast<Value>(597172653) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a11 : boost::array<Value, 11>
{
    PrinceDormand87_a11(void)
    {
        (*this)[0] = static_cast<Value>(185892177) / static_cast<Value>(718116043) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-3185094517.) / static_cast<Value>(667107341) ;
        (*this)[4] = static_cast<Value>(-477755414.) / static_cast<Value>(1098053517) ;
        (*this)[5] = static_cast<Value>(-703635378.) / static_cast<Value>(230739211) ;
        (*this)[6] = static_cast<Value>(5731566787) / static_cast<Value>(1027545527) ;
        (*this)[7] = static_cast<Value>(5232866602) / static_cast<Value>(850066563) ;
        (*this)[8] = static_cast<Value>(-4093664535.) / static_cast<Value>(808688257) ;
        (*this)[9] = static_cast<Value>(3962137247) / static_cast<Value>(1805957418) ;
        (*this)[10] = static_cast<Value>(65686358) / static_cast<Value>(487910083) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_a12 : boost::array<Value, 12>
{
    PrinceDormand87_a12(void)
    {
        (*this)[0] = static_cast<Value>(403863854) / static_cast<Value>(491063109) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(-5068492393.) / static_cast<Value>(434740067) ;
        (*this)[4] = static_cast<Value>(-411421997.) / static_cast<Value>(543043805) ;
        (*this)[5] = static_cast<Value>(652783627) / static_cast<Value>(914296604) ;
        (*this)[6] = static_cast<Value>(11173962825) / static_cast<Value>(925320556) ;
        (*this)[7] = static_cast<Value>(-13158990841.) / static_cast<Value>(6184727034) ;
        (*this)[8] = static_cast<Value>(3936647629) / static_cast<Value>(1978049680) ;
        (*this)[9] = static_cast<Value>(-160528059.) / static_cast<Value>(685178525) ;
        (*this)[10] = static_cast<Value>(248638103) / static_cast<Value>(1413531060) ;
        (*this)[11] = static_cast<Value>(0) ;
    }
} ;

// 8th order coefficients
template <typename Value=double>
struct PrinceDormand87_b : boost::array<Value, 13>
{
    PrinceDormand87_b(void)
    {
        (*this)[0] = static_cast<Value>(14005451) / static_cast<Value>(335480064) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-59238493.) / static_cast<Value>(1068277825) ;
        (*this)[6] = static_cast<Value>(181606767) / static_cast<Value>(758867731) ;
        (*this)[7] = static_cast<Value>(561292985) / static_cast<Value>(797845732) ;
        (*this)[8] = static_cast<Value>(-1041891430.) / static_cast<Value>(1371343529) ;
        (*this)[9] = static_cast<Value>(760417239) / static_cast<Value>(1151165299) ;
        (*this)[10] = static_cast<Value>(118820643) / static_cast<Value>(751138087) ;
        (*this)[11] = static_cast<Value>(-528747749.) / static_cast<Value>(2220607170) ;
        (*this)[12] = static_cast<Value>(1) / static_cast<Value>(4) ;
    }
} ;

// These values correspond to the difference of the 8th order and 7th order
// coefficients in Butcher tableau
template <typename Value=double>
struct PrinceDormand87_db : boost::array<Value, 13>
{
    PrinceDormand87_db(void)
    {
        (*this)[0] = static_cast<Value>(206899875720925) / static_cast<Value>(16966964735038208) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(161224140072326693) / static_cast<Value>(208527862420056925) ;
        (*this)[6] = static_cast<Value>(-308134860501296901.) / static_cast<Value>(4283929245060770651) ;
        (*this)[7] = static_cast<Value>(-187090058122256469.) / static_cast<Value>(106070073963259076) ;
        (*this)[8] = static_cast<Value>(3721643503328385829) / static_cast<Value>(2082408744123259974) ;
        (*this)[9] = static_cast<Value>(-290897219666967667.) / static_cast<Value>(371523099811498965) ;
        (*this)[10] = static_cast<Value>(39496005864008611) / static_cast<Value>(501397231350176553) ;
        (*this)[11] = static_cast<Value>(-627441401.) / static_cast<Value>(2220607170) ;
        (*this)[12] = static_cast<Value>(1) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct PrinceDormand87_c : boost::array<Value, 13>
{
    PrinceDormand87_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(18) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(12) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(8) ;
        (*this)[4] = static_cast<Value>(5) / static_cast<Value>(16) ;
        (*this)[5] = static_cast<Value>(3) / static_cast<Value>(8) ;
        (*this)[6] = static_cast<Value>(59) / static_cast<Value>(400) ;
        (*this)[7] = static_cast<Value>(93) / static_cast<Value>(200) ;
        (*this)[8] = static_cast<Value>(5490023248) / static_cast<Value>(9719169821) ;
        (*this)[9] = static_cast<Value>(13) / static_cast<Value>(20) ;
        (*this)[10] = static_cast<Value>(1201146811) / static_cast<Value>(1299019798) ;
        (*this)[11] = static_cast<Value>(1) ;
        (*this)[12] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class PrinceDormand87 :
            public boostode::explicit_error_generic_rk<13, 8, 8, 7, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<13, 8, 8, 7, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    PrinceDormand87(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(PrinceDormand87_a1<Value>(),
                                                     PrinceDormand87_a2<Value>(),
                                                     PrinceDormand87_a3<Value>(),
                                                     PrinceDormand87_a4<Value>(),
                                                     PrinceDormand87_a5<Value>(),
                                                     PrinceDormand87_a6<Value>(),
                                                     PrinceDormand87_a7<Value>(),
                                                     PrinceDormand87_a8<Value>(),
                                                     PrinceDormand87_a9<Value>(),
                                                     PrinceDormand87_a10<Value>(),
                                                     PrinceDormand87_a11<Value>(),
                                                     PrinceDormand87_a12<Value>()),
                          PrinceDormand87_b<Value>(), PrinceDormand87_db<Value>(),
                          PrinceDormand87_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_PrinceDormand87_hpp
