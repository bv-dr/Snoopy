#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner65_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner65_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

#include "Math/ODEIntegration/Steppers/Details/Verner56.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

// 6th order coefficients
template <typename Value=double>
struct Verner65_b : boost::array<Value, 8>
{
    Verner65_b(void)
    {
        (*this)[0] = static_cast<Value>(57.) / static_cast<Value>(640.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(-16.) / static_cast<Value>(65.) ;
        (*this)[3] = static_cast<Value>(1377.) / static_cast<Value>(2240.) ;
        (*this)[4] = static_cast<Value>(121.) / static_cast<Value>(320.) ;
        (*this)[5] = static_cast<Value>(0.) ;
        (*this)[6] = static_cast<Value>(31185.) / static_cast<Value>(291200.) ;
        (*this)[7] = static_cast<Value>(16640.) / static_cast<Value>(291200.) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner65 :
            public boostode::explicit_error_generic_rk<8, 6, 6, 5, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<8, 6, 6, 5, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner65(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner56_a1<Value>(),
                                                     Verner56_a2<Value>(),
                                                     Verner56_a3<Value>(),
                                                     Verner56_a4<Value>(),
                                                     Verner56_a5<Value>(),
                                                     Verner56_a6<Value>(),
                                                     Verner56_a7<Value>()),
                          Verner65_b<Value>(), Verner56_db<Value>(),
                          Verner56_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner65_hpp
