#ifndef BV_Math_ODEIntegration_Steppers_Details_Fehlberg54_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Fehlberg54_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

#include "Math/ODEIntegration/Steppers/Details/Fehlberg45.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

// 5th order coefficients
template <typename Value=double>
struct Fehlberg54_b : boost::array<Value, 6>
{
    Fehlberg54_b(void)
    {
        (*this)[0] = static_cast<Value>(16.) / static_cast<Value>(135.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(6656.) / static_cast<Value>(12825.) ;
        (*this)[3] = static_cast<Value>(28561.) / static_cast<Value>(56430.) ;
        (*this)[4] = static_cast<Value>(-9.) / static_cast<Value>(50.) ;
        (*this)[5] = static_cast<Value>(2.) /static_cast<Value>(55.) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Fehlberg54 :
            public boostode::explicit_error_generic_rk<6, 5, 5, 4, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<6, 5, 5, 4, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Fehlberg54(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Fehlberg45_a1<Value>(),
                                                     Fehlberg45_a2<Value>(),
                                                     Fehlberg45_a3<Value>(),
                                                     Fehlberg45_a4<Value>(),
                                                     Fehlberg45_a5<Value>()),
                          Fehlberg54_b<Value>(), Fehlberg45_db<Value>(),
                          Fehlberg45_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Fehlberg54_hpp
