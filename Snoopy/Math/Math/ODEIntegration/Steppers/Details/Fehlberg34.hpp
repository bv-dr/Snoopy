#ifndef BV_Math_ODEIntegration_Steppers_Details_Fehlberg34_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Fehlberg34_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Fehlberg34_a1 : boost::array<Value, 1>
{
    Fehlberg34_a1(void)
    {
        (*this)[0] = static_cast<Value>(2) / static_cast<Value>(7) ;
    }
} ;

template <typename Value=double>
struct Fehlberg34_a2 : boost::array<Value, 2>
{
    Fehlberg34_a2(void)
    {
        (*this)[0] = static_cast<Value>(77.) / static_cast<Value>(900.) ;
        (*this)[1] = static_cast<Value>(343.) / static_cast<Value>(900.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg34_a3 : boost::array<Value, 3>
{
    Fehlberg34_a3(void)
    {
        (*this)[0] = static_cast<Value>(805.) / static_cast<Value>(1444.) ;
        (*this)[1] = static_cast<Value>(-77175.) / static_cast<Value>(54872.) ;
        (*this)[2] = static_cast<Value>(97125.) / static_cast<Value>(54872.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg34_a4 : boost::array<Value, 4>
{
    Fehlberg34_a4(void)
    {
        (*this)[0] = static_cast<Value>(79.) / static_cast<Value>(490.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(2175.) / static_cast<Value>(3626.) ;
        (*this)[3] = static_cast<Value>(2166.) / static_cast<Value>(9065.) ;
    }
} ;

// 3rd order coefficients
template <typename Value=double>
struct Fehlberg34_b : boost::array<Value, 5>
{
    Fehlberg34_b(void)
    {
        (*this)[0] = static_cast<Value>(229.) / static_cast<Value>(1470.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(1125.) / static_cast<Value>(1813.) ;
        (*this)[3] = static_cast<Value>(13718.) / static_cast<Value>(81585.) ;
        (*this)[4] = static_cast<Value>(1.) / static_cast<Value>(18.) ;
    }
} ;

// These values correspond to the difference of the 4th order and 3rd order
// coefficients in Butcher tableau
template <typename Value=double>
struct Fehlberg34_db : boost::array<Value, 5>
{
    Fehlberg34_db(void)
    {
        (*this)[0] = static_cast<Value>(-888.) / static_cast<Value>(163170.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3375.) / static_cast<Value>(163170.) ;
        (*this)[3] = static_cast<Value>(-11552.) / static_cast<Value>(163170.) ;
        (*this)[4] = static_cast<Value>(9065.) / static_cast<Value>(163170.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg34_c : boost::array<Value, 5>
{
    Fehlberg34_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(2) / static_cast<Value>(7) ;
        (*this)[2] = static_cast<Value>(7) / static_cast<Value>(15) ;
        (*this)[3] = static_cast<Value>(35) / static_cast<Value>(38) ;
        (*this)[4] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Fehlberg34 :
            public boostode::explicit_error_generic_rk<5, 3, 3, 4, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<5, 3, 3, 4, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Fehlberg34(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Fehlberg34_a1<Value>(),
                                                     Fehlberg34_a2<Value>(),
                                                     Fehlberg34_a3<Value>(),
                                                     Fehlberg34_a4<Value>()),
                          Fehlberg34_b<Value>(), Fehlberg34_db<Value>(),
                          Fehlberg34_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Fehlberg34_hpp
