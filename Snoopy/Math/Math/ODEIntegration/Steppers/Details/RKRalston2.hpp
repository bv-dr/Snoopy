#ifndef BV_Math_ODEIntegration_Steppers_Details_RKRalston2_hpp
#define BV_Math_ODEIntegration_Steppers_Details_RKRalston2_hpp

#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct RKRalston2_a : boost::array<Value, 1>
{
    RKRalston2_a(void)
    {
        (*this)[0] = static_cast<Value>(2) / static_cast<Value>(3) ;
    }
} ;

template <typename Value=double>
struct RKRalston2_b : boost::array<Value, 2>
{
    RKRalston2_b(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[1] = static_cast<Value>(3) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct RKRalston2_c : boost::array<Value, 2>
{
    RKRalston2_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(2) / static_cast<Value>(3) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class RKRalston2 :
            public boostode::explicit_generic_rk<2, 2, State, Value, Deriv,
                                                 Time, Algebra, Operations,
                                                 Resizer>
{
public:
    typedef boostode::explicit_generic_rk<2, 2, State, Value, Deriv,
                                          Time, Algebra, Operations,
                                          Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    RKRalston2(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(RKRalston2_a<Value>()),
                          RKRalston2_b<Value>(), RKRalston2_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_RKRalston2_hpp
