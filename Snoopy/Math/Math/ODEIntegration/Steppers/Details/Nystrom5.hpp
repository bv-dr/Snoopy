#ifndef BV_Math_ODEIntegration_Steppers_Details_Nystrom5_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Nystrom5_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Nystrom5_a1 : boost::array<Value, 1>
{
    Nystrom5_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(3) ;
    }
} ;

template <typename Value=double>
struct Nystrom5_a2 : boost::array<Value, 2>
{
    Nystrom5_a2(void)
    {
        (*this)[0] = static_cast<Value>(4) / static_cast<Value>(25) ;
        (*this)[1] = static_cast<Value>(6) / static_cast<Value>(25) ;
    }
} ;

template <typename Value=double>
struct Nystrom5_a3 : boost::array<Value, 3>
{
    Nystrom5_a3(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(4) ;
        (*this)[1] = static_cast<Value>(-12) / static_cast<Value>(4) ;
        (*this)[2] = static_cast<Value>(15) / static_cast<Value>(4) ;
    }
} ;

template <typename Value=double>
struct Nystrom5_a4 : boost::array<Value, 4>
{
    Nystrom5_a4(void)
    {
        (*this)[0] = static_cast<Value>(6) / static_cast<Value>(81) ;
        (*this)[1] = static_cast<Value>(90) / static_cast<Value>(81) ;
        (*this)[2] = static_cast<Value>(-50) / static_cast<Value>(81) ;
        (*this)[3] = static_cast<Value>(8) / static_cast<Value>(81) ;
    }
} ;

template <typename Value=double>
struct Nystrom5_a5 : boost::array<Value, 5>
{
    Nystrom5_a5(void)
    {
        (*this)[0] = static_cast<Value>(6) / static_cast<Value>(75) ;
        (*this)[1] = static_cast<Value>(36) / static_cast<Value>(75) ;
        (*this)[2] = static_cast<Value>(10) / static_cast<Value>(75) ;
        (*this)[3] = static_cast<Value>(8) / static_cast<Value>(75) ;
        (*this)[4] = static_cast<Value>(0) ;
    }
} ;

template <typename Value=double>
struct Nystrom5_b : boost::array<Value, 6>
{
    Nystrom5_b(void)
    {
        (*this)[0] = static_cast<Value>(23) / static_cast<Value>(192) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(125) / static_cast<Value>(192) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(-81) / static_cast<Value>(192) ;
        (*this)[5] = static_cast<Value>(125) / static_cast<Value>(192) ;
    }
} ;

template <typename Value=double>
struct Nystrom5_c : boost::array<Value, 6>
{
    Nystrom5_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(3) ;
        (*this)[2] = static_cast<Value>(2) / static_cast<Value>(5) ;
        (*this)[3] = static_cast<Value>(1) ;
        (*this)[4] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[5] = static_cast<Value>(4) / static_cast<Value>(5) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Nystrom5 :
            public boostode::explicit_generic_rk<6, 5, State, Value, Deriv,
                                                 Time, Algebra, Operations,
                                                 Resizer>
{
public:
    typedef boostode::explicit_generic_rk<6, 5, State, Value, Deriv,
                                          Time, Algebra, Operations,
                                          Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Nystrom5(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Nystrom5_a1<Value>(),
                                                     Nystrom5_a2<Value>(),
                                                     Nystrom5_a3<Value>(),
                                                     Nystrom5_a4<Value>(),
                                                     Nystrom5_a5<Value>()),
                          Nystrom5_b<Value>(),
                          Nystrom5_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Nystrom5_hpp
