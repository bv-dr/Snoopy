#ifndef BV_Math_ODEIntegration_Steppers_Details_GPS_hpp
#define BV_Math_ODEIntegration_Steppers_Details_GPS_hpp

#include <type_traits>

#include <boost/numeric/odeint/algebra/range_algebra.hpp>
#include <boost/numeric/odeint/algebra/default_operations.hpp>
#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>

#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>
#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>

#include <boost/numeric/odeint/stepper/stepper_categories.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/stepper/extrapolation_stepper.hpp>
#include <boost/numeric/odeint/stepper/base/algebra_stepper_base.hpp>
#include <boost/numeric/odeint/stepper/base/explicit_error_stepper_base.hpp>
#include <boost/numeric/odeint/stepper/detail/rotating_buffer.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/StepAdjuster.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template<
    class Stepper ,
    unsigned short Order,
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer = boostode::initially_resizer
        >
class GPSABC : public boostode::explicit_stepper_base<
                Stepper, Order, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                                         >
{
public :

    typedef boostode::explicit_stepper_base<
                Stepper, Order, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                                 > stepper_base_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::time_type time_type ;

    GPSABC(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(algebra),
        eta_(1.), lastTime_(0.)
    {
    }


protected:
    time_type eta_ ;
    time_type lastTime_ ;

    template <class StateIn, class DerivIn, class StateOut>
    void push_GPS_(const StateIn & in, const DerivIn & dxdt,
                   StateOut & out)
    {
        out = in + this->eta_ * dxdt ;
    }
} ;



template<
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer = boostode::initially_resizer
        >
class GPS2 : public GPSABC<GPS2<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
                           2, State, Value, Deriv, Time, Algebra, Operations, Resizer
                          >
{

public :
    typedef boostode::explicit_stepper_base<
          GPS2<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
          2, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                           > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    GPS2(const algebra_type & algebra=algebra_type()) :
        GPSABC<GPS2<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
               2, State, Value, Deriv, Time, Algebra, Operations, Resizer>(algebra)
    {
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        time_type tau(dt/2.) ;
        time_type tau2(tau*tau) ;

        value_type x2(DotTraits<state_type, state_type>::get(in, in)) ;
        value_type f2(DotTraits<deriv_type, deriv_type>::get(dxdt, dxdt)) ;
        value_type tau2F2(tau2*f2) ;

        // if time step is not small enough we perform an euler
        if(sqrt(x2)>sqrt(tau2F2))
        {
            value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
            this->eta_ = 2*tau*(x2+tau*fx)/(x2-tau2F2) ;
            this->push_GPS_(in, dxdt, out) ;
        }
//        else if(sqrt(x2)<1.e-12)
//        {
//            value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
//            this->eta_ = 2*tau*(1.+tau*fx)/(1.-tau2F2) ;
//            this->push_GPS_(in, dxdt, out) ;
//        }
        else
        {
            std::cout << "Performing Euler in GPS..." << std::endl ;
            this->eta_ = dt ;
            this->push_GPS_(in, dxdt, out) ;
        }
    }

} ;



template<
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer = boostode::initially_resizer
        >
class GPS4 : public GPSABC<GPS4<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
                           4, State, Value, Deriv, Time, Algebra, Operations, Resizer
                          >
{

public :
    typedef boostode::explicit_stepper_base<
          GPS4<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
          4, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                           > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    GPS4(const algebra_type & algebra=algebra_type()) :
        GPSABC<GPS4<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
               4, State, Value, Deriv, Time, Algebra, Operations, Resizer>(algebra)
    {
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        time_type tau(dt/2.) ;
        time_type tau2(tau*tau) ;

        value_type x2(DotTraits<state_type, state_type>::get(in, in)) ;
        value_type f2(DotTraits<deriv_type, deriv_type>::get(dxdt, dxdt)) ;
        value_type tau2F2(tau2*f2) ;

        // if time step is not small enough we perform an euler
        if(sqrt(x2)>sqrt(tau2F2))
        {
            value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
            value_type x_norm(sqrt(x2)) ;
            value_type a(tau/x_norm) ;
            value_type b(tau2/3./x2) ;
            value_type a2(a*a) ;
            value_type b2(b*b) ;
            value_type c( (1.+b*f2)*(1.+b*f2) - a2*f2 ) ;
            this->eta_ = 2*a2*fx/c + a*x_norm * (1+c+f2*(a2+2.*b+b2*f2)) / (c*(1.+b*f2)) ;
            this->push_GPS_(in, dxdt, out) ;
        }
//        else if(sqrt(x2)<1.e-12)
//        {
//            value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
//            this->eta_ = 2*tau*(1.+tau*fx)/(1.-tau2F2) ;
//            this->push_GPS_(in, dxdt, out) ;
//        }
        else
        {
            std::cout << "Performing Euler in GPS..." << std::endl ;
            this->eta_ = dt ;
            this->push_GPS_(in, dxdt, out) ;
        }
    }

} ;



template<
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer = boostode::initially_resizer
        >
class GPS6 : public GPSABC<GPS6<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
                           6, State, Value, Deriv, Time, Algebra, Operations, Resizer
                          >
{

public :
    typedef boostode::explicit_stepper_base<
          GPS6<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
          6, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                           > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    GPS6(const algebra_type & algebra=algebra_type()) :
        GPSABC<GPS6<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
               6, State, Value, Deriv, Time, Algebra, Operations, Resizer>(algebra)
    {
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        time_type tau(dt/2.) ;
        time_type tau2(tau*tau) ;

        value_type x2(DotTraits<state_type, state_type>::get(in, in)) ;
        value_type f2(DotTraits<deriv_type, deriv_type>::get(dxdt, dxdt)) ;
        value_type tau2F2(tau2*f2) ;

        // if time step is not small enough we perform an euler
        if(sqrt(x2)>sqrt(tau2F2))
        {
            value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
            value_type x_norm(sqrt(x2)) ;
            value_type a((15.*tau*x2+tau*tau2*f2)/15./x_norm/x2) ;
            value_type b(2.*tau2 / 5. / x2) ;
            value_type a2(a*a) ;
            value_type b2(b*b) ;
            value_type c( (1.+b*f2)*(1.+b*f2) - a2*f2 ) ;
            this->eta_ = 2*a2*fx/c + a*x_norm * (1+c+f2*(a2+2.*b+b2*f2)) / (c*(1.+b*f2)) ;
//            if( sqrt(tau2F2) <= sqrt(x2)/6.)
//            {
//                std::cout << "Lower bound of 6th order scheme not respected." << std::endl ;
//            }
            this->push_GPS_(in, dxdt, out) ;
        }
//        else if(sqrt(x2)<1.e-12)
//        {
//            value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
//            this->eta_ = 2*tau*(1.+tau*fx)/(1.-tau2F2) ;
//            this->push_GPS_(in, dxdt, out) ;
//        }
        else
        {
            std::cout << "Performing Euler in GPS..." << std::endl ;
            this->eta_ = dt ;
            this->push_GPS_(in, dxdt, out) ;
        }
    }

} ;

template<
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer = boostode::initially_resizer
        >
class GPS1 : public GPSABC<GPS1<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
                           1, State, Value, Deriv, Time, Algebra, Operations, Resizer
                          >
{

public :
    typedef boostode::explicit_stepper_base<
          GPS1<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
          1, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                           > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    GPS1(const algebra_type & algebra=algebra_type()) :
        GPSABC<GPS1<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
               1, State, Value, Deriv, Time, Algebra, Operations, Resizer>(algebra)
    {
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        value_type x2(DotTraits<state_type, state_type>::get(in, in)) ;
        value_type f2(DotTraits<deriv_type, deriv_type>::get(dxdt, dxdt)) ;

        if( ( x2<1.e-12 ) || ( f2 < 1.e-12 ) )
        {
            std::cout << "Performing Euler in GPS..." << std::endl ;
            this->eta_ = dt ;
            this->push_GPS_(in, dxdt, out) ;
            return ;
        }

        value_type fx(DotTraits<deriv_type, state_type>::get(dxdt, in)) ;
        value_type f_norm(sqrt(f2)) ;
        value_type x_norm(sqrt(x2)) ;
        time_type alphaN(std::cosh(dt*f_norm/x_norm)) ;
        time_type betaN(std::sinh(dt*f_norm/x_norm)) ;
        this->eta_ = ( (alphaN - 1. )*fx + betaN*x_norm*f_norm ) / f2;
        this->push_GPS_(in, dxdt, out) ;
    }

} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_GPS_hpp
