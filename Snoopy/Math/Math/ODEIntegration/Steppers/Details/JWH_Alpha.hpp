#pragma once

#include <type_traits>
#include <fstream>

#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>

#include <boost/numeric/odeint/stepper/stepper_categories.hpp>

#include <boost/numeric/odeint/stepper/base/algebra_stepper_base.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/Solvers/Newton.hpp"
#include "Math/Solvers/SolverParameters.hpp"
#include "Math/FiniteDifference/FiniteDifference.hpp"


namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace JWHA {

template <typename T, class Enable=void>
struct MTypeTraits ;

template <typename T>
struct MTypeTraits<T,
                   typename std::enable_if<
                      BV::Math::ODEIntegration::Details::IsEigen<T, Eigen::EigenBase>::value
                                          >::type>
{
    typedef Eigen::MatrixXd mtype ;
    typedef Eigen::VectorXd vtype ;
} ;

template <typename System, typename TType=double>
class KFDWrapper
{
private:
    System & system_ ;
    TType t_ ;
public:
    KFDWrapper(System & system, const TType & t) :
        system_(system), t_(t)
    {
    }

    // FIXME finite difference functor has the following signature...
    // See if we need another one.
    Eigen::VectorXd get(const Eigen::Ref<const Eigen::VectorXd> & x)
    {
        Eigen::VectorXd res(Eigen::VectorXd::Zero(x.size())) ;
        system_.posVariation(x, res, t_) ;
        return res ;
    }
} ;

template <typename System, typename TType=double>
class JacFDWrapper
{
private:
    System & system_ ;
    TType t_ ;
public:
    JacFDWrapper(System & system, const TType & t) :
        system_(system), t_(t)
    {
    }

    // FIXME finite difference functor has the following signature...
    // See if we need another one.
    Eigen::VectorXd get(const Eigen::Ref<const Eigen::VectorXd> & x)
    {
        Eigen::VectorXd res(Eigen::VectorXd::Zero(x.size())) ;
        system_(x, res, t_) ;
        return res ;
    }
} ;

template <typename System, typename TType=double>
class CFDWrapper
{
private:
    System & system_ ;
    TType t_ ;
public:
    CFDWrapper(System & system, const TType & t) :
        system_(system), t_(t)
    {
    }

    // FIXME finite difference functor has the following signature...
    // See if we need another one.
    Eigen::VectorXd get(const Eigen::Ref<const Eigen::VectorXd> & x)
    {
        Eigen::VectorXd res(Eigen::VectorXd::Zero(x.size())) ;
        system_.velVariation(x, res, t_) ;
        return res ;
    }
} ;

template <typename System, typename StateIn, typename Time,
          typename MType, typename AlphaType, typename GammaType>
class ResidualsFunction
{
private:
    using State = typename BV::Math::ODEIntegration::Details::GetType<StateIn>::type ;
    using ThisType = ResidualsFunction<System, StateIn, Time,
                                                MType, AlphaType, GammaType> ;
    System & system_ ;
    MType M_ ;
    MType C_ ;
    MType K_ ;
    AlphaType alphaM_ ;
    AlphaType alphaF_ ;
    GammaType gamma_ ;
    State dN_ ; // position
    State ddN_ ; // der position
    State vN_ ; // velocity
    State dVN_ ; // acceleration
    State FN_ ; // acceleration
    State vNP1_ ;
    State dVNP1_ ;
    State FNP1_ ;
    Time dt_ ;
    State X_ ;
    State dX_ ;
    std::size_t nUnknowns_ ;
    bool KInitialized_ ;
    bool CInitialized_ ;

public:
    ResidualsFunction(System & system, const MType & M,
                      const AlphaType & alphaM,
                      const AlphaType & alphaF, const GammaType & gamma,
                      const State & dN, const State & ddN,
                      const State & vN, const State & dVN,
                      const State & FN, const Time & dt,
                      const State & X, const State & dX) :
        system_(system), M_(M), alphaM_(alphaM), alphaF_(alphaF),
        gamma_(gamma), dN_(dN), ddN_(ddN), vN_(vN), dVN_(dVN), FN_(FN), dt_(dt),
        nUnknowns_(X.size()), KInitialized_(false), CInitialized_(false)
    {
        FNP1_ = State::Zero(nUnknowns_) ;
        vNP1_ = State::Zero(nUnknowns_) ;
        dVNP1_ = State::Zero(nUnknowns_) ;
        K_ = MType::Zero(nUnknowns_, nUnknowns_) ;
        C_ = MType::Zero(nUnknowns_, nUnknowns_) ;
    }

    template <typename StateT>
    void updateState(const StateT & in)
    {
        vNP1_ = alphaM_ / (alphaF_*gamma_*dt_) * in
                   + (gamma_ - alphaM_) / (gamma_*alphaF_)*ddN_
                   + (alphaF_ - 1.) / alphaF_ * vN_ ;
        dX_ = vNP1_ ; // velocity
        dVNP1_ = alphaM_ / (alphaF_ * gamma_ * gamma_ * dt_ * dt_) * in
                    - 1. / (alphaF_ * gamma_ * dt_) * vN_
                    + (gamma_ - 1.) / gamma_ * dVN_
                    + (gamma_ - alphaM_) / (alphaF_ * gamma_ * gamma_ * dt_) * ddN_ ;
        X_ = dN_ + in ;
    }

    template <typename StateT, typename REType>
    void operator()(const StateT & in, REType & residuals, const Time & t)
    {
        updateState(in) ;
        //setK(in, t) ;
        //setC(in, t) ;
        system_(X_, dX_, FNP1_, t) ;
        //std::cout << "in: " << in.transpose() << std::endl ;
        //std::cout << "X: " << X_.transpose() << std::endl ;
        //std::cout << "FNP1: " << FNP1_.transpose() << std::endl ;
        State FI(alphaF_ * FNP1_ + (1. - alphaF_) * FN_) ;
        residuals = -FI
             + M_ * (alphaM_ * dVNP1_ + (1. - alphaM_) * dVN_) ;
             //+ C_ * (alphaF_ * vNP1_ + (1. - alphaF_) * vN_)
             //+ K_ * (alphaF_ * (dN_+in) + (1. - alphaF_) * dN_) ;
    }

    template <typename StateT>
    void setK(const StateT & in, const Time & t)
    {
        if (KInitialized_)
        {
            return ;
        }
        // Compute the stiffness matrix via finite difference
        // We assume the state has been updated
        namespace FD = BV::Math::FiniteDifference ;
        typedef FD::FiniteDifference<FD::FDScheme::CENTRAL,
                                     FD::IthDerivative::FIRST, 2,
                                     FD::FDContext::JACOBIAN_CALCULATION> FDType ;
        // FIXME this is the total jacobian calculation...
        typedef JacFDWrapper<ThisType, Time> WrapperJac ;
        WrapperJac wrapperJac(*this, t) ;
        std::function<Eigen::VectorXd(const Eigen::Ref<const Eigen::VectorXd> &)> fJac(
                        std::bind(&WrapperJac::get, wrapperJac, std::placeholders::_1)
                                                                                    ) ;
        K_ = FDType::get(in, fJac, 1.e-8) ;
        //typedef KFDWrapper<ThisType, Time> WrapperK ;
        //WrapperK wrapperK(*this, t) ;
        //std::function<Eigen::VectorXd(const Eigen::Ref<const Eigen::VectorXd> &)> fK(
        //                std::bind(&WrapperK::get, wrapperK, std::placeholders::_1)
        //                                                                            ) ;
        //updateState(in) ;
        //K_ = FDType::get(X_, fK, 1.e-8) ;
        KInitialized_ = true ;
    }

    template <typename StateT>
    void setC(const StateT & in, const Time & t)
    {
        if (CInitialized_)
        {
            return ;
        }
        // Compute the stiffness matrix via finite difference
        // We assume the state has been updated
        namespace FD = BV::Math::FiniteDifference ;
        typedef FD::FiniteDifference<FD::FDScheme::CENTRAL,
                                     FD::IthDerivative::FIRST, 2,
                                     FD::FDContext::JACOBIAN_CALCULATION> FDType ;
        typedef CFDWrapper<ThisType, Time> WrapperC ;
        WrapperC wrapperC(*this, t) ;
        std::function<Eigen::VectorXd(const Eigen::Ref<const Eigen::VectorXd> &)> fC(
                        std::bind(&WrapperC::get, wrapperC, std::placeholders::_1)
                                                                                    ) ;
        updateState(in) ;
        C_ = FDType::get(dX_, fC, 1.e-8) ;
        CInitialized_ = true ;
    }

    template <typename StateT>
    const MType & getK(const StateT & in, const Time & t)
    {
        if (!KInitialized_)
        {
            setK(in, t) ;
        }
        return K_ ;
    }

    template <typename StateT>
    const MType & getC(const StateT & in, const Time & t)
    {
        if (!CInitialized_)
        {
            setC(in, t) ;
        }
        return C_ ;
    }

    template <typename PosT, typename ResT>
    void posVariation(const PosT & pos, ResT & res, const Time & t)
    {
        X_ = pos ;
        system_(X_, dX_, FNP1_, t) ;
        State FI(alphaF_ * FNP1_ + (1. - alphaF_) * FN_) ;
        res = -FI
             + M_ * (alphaM_ * dVNP1_ + (1. - alphaM_) * dVN_) ;
    }

    template <typename VelT, typename ResT>
    void velVariation(const VelT & vel, ResT & res, const Time & t)
    {
        dX_ = vel ;
        system_(X_, dX_, FNP1_, t) ;
        res = FNP1_ ;
    }

    const State & getVNP1() const
    {
        return vNP1_ ;
    }

    const State & getDVNP1() const
    {
        return dVNP1_ ;
    }

    const State & getFNP1() const
    {
        return FNP1_ ;
    }
} ;

template <typename Time, typename MType, typename AlphaType, typename GammaType,
          typename RType>
class JacobianFunction
{
private:
    MType M_ ;
    MType C_ ;
    MType K_ ;
    AlphaType alphaM_ ;
    AlphaType alphaF_ ;
    GammaType gamma_ ;
    Time dt_ ;
    RType & residuals_ ;

public:
    typedef MType JType ;

    template <typename State>
    JType getJInitialized(const State & state) const
    {
        return JType::Zero(state.size(), state.size()) ;
    }

    JacobianFunction(const MType & M, const AlphaType & alphaM,
                     const AlphaType & alphaF, const GammaType & gamma,
                     const Time & dt, RType & residuals) :
        M_(M), alphaM_(alphaM), alphaF_(alphaF),
        gamma_(gamma), dt_(dt), residuals_(residuals)
    {
        K_ = MType::Zero(M.rows(), M.cols()) ;
        C_ = MType::Zero(M.rows(), M.cols()) ;
    }

    template <typename State, typename JacType>
    void operator()(const State & in, JacType & J, const Time & t)
    {
        residuals_.updateState(in) ;
        K_ = residuals_.getK(in, t) ;
        //C_ = residuals_.getC(in, t) ;

        J = alphaM_ * alphaM_ / (alphaF_ * gamma_ * gamma_ * dt_ * dt_) * M_
            //+ alphaM_ / (gamma_ * dt_) * C_
            + alphaF_ * K_ ;
    }
} ;

} // End of namespace JWHA

template<
    class State ,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
        >
class JWH_Alpha : public boostode::algebra_stepper_base<Algebra, Operations>
{
public:

    typedef JWH_Alpha<State, Value, Deriv, Time,
                      Algebra, Operations, Resizer> internal_stepper_base_type ;

    typedef State state_type ;
    typedef Value value_type ;
    typedef Deriv deriv_type ;
    typedef Time time_type ;
    typedef Resizer resizer_type ;
    typedef boostode::stepper_tag stepper_category ;
    typedef boostode::algebra_stepper_base<Algebra, Operations> algebra_stepper_base_type ;
    typedef typename algebra_stepper_base_type::algebra_type algebra_type ;
    typedef typename algebra_stepper_base_type::operations_type operations_type ;
    typedef unsigned short order_type ;

    typedef boostode::state_wrapper<state_type> wrapped_state_type ;
    typedef boostode::state_wrapper<deriv_type> wrapped_deriv_type ;

    JWH_Alpha(const algebra_type &algebra=algebra_type()) :
        algebra_stepper_base_type(algebra), initialized_(false)
    {
        // FIXME hardcoded !
        BV::Math::Solvers::SolverParameters params ;
        params.set("maxIterations", static_cast<unsigned>(40)) ;
        params.set("absoluteError", 1.e-2) ; // FIXME
        params.set("verbose", false) ;
        params.set("jacobianDelta", 1.e-8) ;
        params.set("computeJOnce", true) ; // FIXME change this ?
        solver_.setParameters(params) ;
        rhoInf_ = 0.6 ; // FIXME !
        alphaM_ = (3. - rhoInf_) / (2. * (1. + rhoInf_)) ;
        alphaF_ = 1. / (1. + rhoInf_) ;
        gamma_ = 1./2. + alphaM_ - alphaF_ ;
    }

    template <class System, class StateInOut>
    void do_step(System system, StateInOut & x, time_type t, time_type dt)
    {
        do_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt)
    {
        do_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out,
                 time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_and_jac ;
        typedef typename std::remove_reference<typename system_and_jac::first_type>::type system_type ;
        system_and_jac & sysAndJac = system ;
        system_type & sys = sysAndJac.first ;

        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        do_step_impl(sys, in, t, out, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out,
                 time_type dt)
    {
        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        do_step_impl(system, in, t, out, dt) ;
    }

    template <class StateIn>
    void adjust_size(const StateIn & x)
    {
        resize_impl(x) ;
    }

    order_type order(void) const
    {
        return 2 ;
    }

    unsigned get_n_iterations() const
    {
        return solver_.getNIterations() ;
    }
    bool has_converged() const
    {
        return solver_.hasConverged() ;
    }

private:

    template <class StateIn>
    bool resize_impl(const StateIn & x)
    {
        return true ;
    }

    template <class System, class StateIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, time_type t,
                      StateOut & out, time_type dt)
    {
        typedef typename std::remove_reference<System>::type SystemType ;
        SystemType & sys = system ;
        std::cout << "t: " << t << " dt: " << dt << std::endl ;
        std::size_t nUnknowns(in.size()) ;
        using MatType = typename JWHA::MTypeTraits<StateIn>::mtype ;
        using VType = typename JWHA::MTypeTraits<StateIn>::vtype ;
        if (!initialized_)
        {
            dN_ = VType::Zero(nUnknowns) ;
            ddN_ = VType::Zero(nUnknowns) ;
            vN_ = VType::Zero(nUnknowns) ;
            dVN_ = VType::Zero(nUnknowns) ;
            FN_ = VType::Zero(nUnknowns) ;
            sav_ = VType::Zero(nUnknowns) ;
            fTmp_ = std::make_shared<std::ofstream>("acceleration.dat") ;
            initialized_ = true ;
        }
        dt_ = dt ;
        out = in ;
        // Create the Newton solver functions callback
        VType X(VType::Zero(nUnknowns)) ;
        VType dX(VType::Zero(nUnknowns)) ;
        MatType M(MatType::Zero(nUnknowns, nUnknowns)) ;
        MatType C(MatType::Zero(nUnknowns, nUnknowns)) ;
        MatType K(MatType::Zero(nUnknowns, nUnknowns)) ;
        sys(in, X, dX, M, t) ;
        using REType = JWHA::ResidualsFunction<SystemType, StateIn, time_type,
                                               MatType, Value, Value> ;
        using JacType = JWHA::JacobianFunction<time_type, MatType, Value,
                                               Value, REType> ;
        REType fRE(sys, M, alphaM_, alphaF_, gamma_,
                   X, dX, dX, dVN_, FN_, dt_, X, dX) ;
        JacType fKE(M, alphaM_, alphaF_, gamma_, dt_, fRE) ;
        // FIXME generic slice of StateIn...
        using StateInT = typename BV::Math::ODEIntegration::Details::GetType<StateIn>::type ;
        //StateInT inTmp(StateInT::Zero(nUnknowns)) ;
        StateInT outTmp(StateInT::Zero(nUnknowns)) ;
        solver_.solve(std::make_pair(std::ref(fRE), std::ref(fKE)),
                      sav_,
                      outTmp, // FIXME solver interface not correct..
                      t) ;
        if (solver_.hasConverged())
        {
            X += outTmp ;
            State ddNP1(1. / (gamma_*dt_) * outTmp
                       + (gamma_ - 1.) / gamma_ * ddN_) ;
            sys(X, ddNP1, out, false, false) ;
            // First assign velocity
            dN_ = X ;
            ddN_ = ddNP1 ;
            vN_ = fRE.getVNP1() ;
            dVN_ = fRE.getDVNP1() ;
            Eigen::IOFormat eigenFmt(12, Eigen::DontAlignCols, "\t", "\n", "", "", "", "") ;
            *(fTmp_.get()) << t << " " << dVN_.transpose().format(eigenFmt) << std::endl ;
            FN_ = fRE.getFNP1() ;
            sav_ = outTmp ;
        }
    }

    template <class System , class StateInOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_v1(System system, StateInOut & x, time_type t, time_type dt)
    {
        m_resizer.adjust_size(x, boostode::detail::bind(
                  &internal_stepper_base_type::template resize_impl<StateInOut>,
                  boostode::detail::ref(*this), boostode::detail::_1
                                                       )) ;
        do_step_impl(system, x, t, x, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step_v1(System system, StateInOut & x, time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_and_jac ;
        typedef typename std::remove_reference<typename system_and_jac::first_type>::type system_type ;
        system_and_jac & sysAndJac = system ;
        system_type & sys = sysAndJac.first ;

        m_resizer.adjust_size(x, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        do_step_impl(sys, x, t, x, dt) ;
    }

    resizer_type m_resizer;

protected:

    BV::Math::Solvers::Newton<value_type> solver_ ;
    Value rhoInf_ ;
    Value alphaM_ ;
    Value alphaF_ ;
    Value gamma_ ;
    State dN_ ; // position
    State ddN_ ; // der position
    State vN_ ; // velocity
    State dVN_ ; // acceleration
    State FN_ ; // acceleration
    Time dt_ ;
    State sav_ ;
    bool initialized_ ;
    std::shared_ptr<std::ofstream> fTmp_ ;
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
