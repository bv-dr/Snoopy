#ifndef BV_Math_ODEIntegration_Steppers_Details_Fehlberg56_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Fehlberg56_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Fehlberg56_a1 : boost::array<Value, 1>
{
    Fehlberg56_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(6) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_a2 : boost::array<Value, 2>
{
    Fehlberg56_a2(void)
    {
        (*this)[0] = static_cast<Value>(4.) / static_cast<Value>(75.) ;
        (*this)[1] = static_cast<Value>(16.) / static_cast<Value>(75.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_a3 : boost::array<Value, 3>
{
    Fehlberg56_a3(void)
    {
        (*this)[0] = static_cast<Value>(5.) / static_cast<Value>(6.) ;
        (*this)[1] = static_cast<Value>(-16.) / static_cast<Value>(6.) ;
        (*this)[2] = static_cast<Value>(15.) / static_cast<Value>(6.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_a4 : boost::array<Value, 4>
{
    Fehlberg56_a4(void)
    {
        (*this)[0] = static_cast<Value>(-40.) / static_cast<Value>(25.) ;
        (*this)[1] = static_cast<Value>(144.) / static_cast<Value>(25.) ;
        (*this)[2] = static_cast<Value>(-100.) / static_cast<Value>(25.) ;
        (*this)[3] = static_cast<Value>(16.) / static_cast<Value>(25.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_a5 : boost::array<Value, 5>
{
    Fehlberg56_a5(void)
    {
        (*this)[0] = static_cast<Value>(722.) / static_cast<Value>(640.) ;
        (*this)[1] = static_cast<Value>(-2304.) / static_cast<Value>(640.) ;
        (*this)[2] = static_cast<Value>(2035.) / static_cast<Value>(640.) ;
        (*this)[3] = static_cast<Value>(-88.) / static_cast<Value>(640.) ;
        (*this)[4] = static_cast<Value>(275.) / static_cast<Value>(640.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_a6 : boost::array<Value, 6>
{
    Fehlberg56_a6(void)
    {
        (*this)[0] = static_cast<Value>(-22.) / static_cast<Value>(1280.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(55.) / static_cast<Value>(1280.) ;
        (*this)[3] = static_cast<Value>(-88.) / static_cast<Value>(1280.) ;
        (*this)[4] = static_cast<Value>(55.) / static_cast<Value>(1280.) ;
        (*this)[5] = static_cast<Value>(0.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_a7 : boost::array<Value, 7>
{
    Fehlberg56_a7(void)
    {
        (*this)[0] = static_cast<Value>(186.) / static_cast<Value>(1280.) ;
        (*this)[1] = static_cast<Value>(-4608.) / static_cast<Value>(1280.) ;
        (*this)[2] = static_cast<Value>(4015.) / static_cast<Value>(1280.) ;
        (*this)[3] = static_cast<Value>(-88.) / static_cast<Value>(1280.) ;
        (*this)[4] = static_cast<Value>(495.) / static_cast<Value>(1280.) ;
        (*this)[5] = static_cast<Value>(0.) ;
        (*this)[6] = static_cast<Value>(1.) ;
    }
} ;

// 5th order coefficients
template <typename Value=double>
struct Fehlberg56_b : boost::array<Value, 8>
{
    Fehlberg56_b(void)
    {
        (*this)[0] = static_cast<Value>(682.) / static_cast<Value>(8448.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(3375.) / static_cast<Value>(8448.) ;
        (*this)[3] = static_cast<Value>(2376.) / static_cast<Value>(8448.) ;
        (*this)[4] = static_cast<Value>(1375.) / static_cast<Value>(8448.) ;
        (*this)[5] = static_cast<Value>(640.) / static_cast<Value>(8448.) ;
        (*this)[6] = static_cast<Value>(0.) ;
        (*this)[7] = static_cast<Value>(0.) ;
    }
} ;

// These values correspond to the difference of the 5th order and 4th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Fehlberg56_db : boost::array<Value, 8>
{
    Fehlberg56_db(void)
    {
        (*this)[0] = static_cast<Value>(-5.) / static_cast<Value>(66.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-5.) / static_cast<Value>(66.) ;
        (*this)[6] = static_cast<Value>(5.) / static_cast<Value>(66.) ;
        (*this)[7] = static_cast<Value>(5.) / static_cast<Value>(66.) ;
    }
} ;

template <typename Value=double>
struct Fehlberg56_c : boost::array<Value, 8>
{
    Fehlberg56_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[2] = static_cast<Value>(4) / static_cast<Value>(15) ;
        (*this)[3] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[4] = static_cast<Value>(8) / static_cast<Value>(10) ;
        (*this)[5] = static_cast<Value>(1) ;
        (*this)[6] = static_cast<Value>(0) ;
        (*this)[7] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Fehlberg56 :
            public boostode::explicit_error_generic_rk<8, 5, 5, 6, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<8, 5, 5, 6, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Fehlberg56(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Fehlberg56_a1<Value>(),
                                                     Fehlberg56_a2<Value>(),
                                                     Fehlberg56_a3<Value>(),
                                                     Fehlberg56_a4<Value>(),
                                                     Fehlberg56_a5<Value>(),
                                                     Fehlberg56_a6<Value>(),
                                                     Fehlberg56_a7<Value>()),
                          Fehlberg56_b<Value>(), Fehlberg56_db<Value>(),
                          Fehlberg56_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Fehlberg56_hpp
