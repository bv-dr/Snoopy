#pragma once

#include <type_traits>
#include <cmath>
#include <Eigen/Dense>

#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>

#include <boost/numeric/odeint/stepper/stepper_categories.hpp>

#include <boost/numeric/odeint/stepper/base/algebra_stepper_base.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/Solvers/Newton.hpp"
#include "Math/Solvers/SolverParameters.hpp"
#include "Math/FiniteDifference/FiniteDifference.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace GA {

template <typename T, class Enable=void>
struct MTypeTraits ;

template <typename T>
struct MTypeTraits<T,
                   typename std::enable_if<
                      BV::Math::ODEIntegration::Details::IsEigen<T, Eigen::EigenBase>::value
                                          >::type>
{
    typedef Eigen::MatrixXd mtype ;
    typedef Eigen::VectorXd vtype ;
} ;

}// End of namespace GA

//namespace GA {
//
//template <typename Algo, typename System, typename TType=double>
//class KFDWrapper
//{
//private:
//    Algo & algo_ ;
//    System & system_ ;
//    TType t_ ;
//public:
//    KFDWrapper(Algo & algo, System & system, const TType & t) :
//        algo_(algo), system_(system), t_(t)
//    {
//    }
//
//    // FIXME finite difference functor has the following signature...
//    // See if we need another one.
//    Eigen::VectorXd get(const Eigen::Ref<const Eigen::VectorXd> & x)
//    {
//        Eigen::VectorXd res(Eigen::VectorXd::Zero(x.size())) ;
//        algo_.posVariation(system_, x, res, t_) ;
//        return res ;
//    }
//} ;
//
//template <typename Algo, typename System, typename TType=double>
//class GFDWrapper
//{
//private:
//    Algo & algo_ ;
//    System & system_ ;
//    TType t_ ;
//public:
//    GFDWrapper(Algo & algo, System & system, const TType & t) :
//        algo_(algo), system_(system), t_(t)
//    {
//    }
//
//    // FIXME finite difference functor has the following signature...
//    // See if we need another one.
//    Eigen::VectorXd get(const Eigen::Ref<const Eigen::VectorXd> & x)
//    {
//        Eigen::VectorXd res(Eigen::VectorXd::Zero(x.size())) ;
//        algo_.velVariation(system_, x, res, t_) ;
//        return res ;
//    }
//} ;
//
//} // End of namespace GA

// Implementation based on:
// "Convergence of the generalized-alpha scheme for constrained mechanical systems"
// Martin Arnold, Olivier Bruls
template<
    class State ,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
        >
class GeneralizedAlpha : public boostode::algebra_stepper_base<Algebra, Operations>
{
public:

    typedef GeneralizedAlpha<State, Value, Deriv, Time,
                             Algebra, Operations, Resizer> internal_stepper_base_type ;

    enum GAType {GENERALIZED_ALPHA, NEWMARK, HHT} ;
    typedef State state_type ;
    typedef Value value_type ;
    typedef Deriv deriv_type ;
    typedef Time time_type ;
    typedef Resizer resizer_type ;
    typedef boostode::stepper_tag stepper_category ;
    typedef boostode::algebra_stepper_base<Algebra, Operations> algebra_stepper_base_type ;
    typedef typename algebra_stepper_base_type::algebra_type algebra_type ;
    typedef typename algebra_stepper_base_type::operations_type operations_type ;
    typedef unsigned short order_type ;

    typedef boostode::state_wrapper<state_type> wrapped_state_type ;
    typedef boostode::state_wrapper<deriv_type> wrapped_deriv_type ;

    GeneralizedAlpha(const algebra_type &algebra=algebra_type()) :
        algebra_stepper_base_type(algebra), initialized_(false),
        nIterations_(0), hasConverged_(false)
    {
        GAType type = GAType::GENERALIZED_ALPHA ;
        if (type == GAType::GENERALIZED_ALPHA)
        {
            rhoInf_ = 0.8 ; // FIXME !
            alphaM_ = (2. * rhoInf_ - 1.) / (1. + rhoInf_);
            alphaF_ = rhoInf_ / (1. + rhoInf_) ;
            gamma_ = 0.5 - alphaM_ + alphaF_ ;
            beta_ = 0.25 * std::pow(gamma_ + 0.5, 2) ;
        }
        else if (type == GAType::NEWMARK)
        {
            alphaM_ = 0. ;
            alphaF_ = 0. ;
            gamma_ = 0.5 - alphaM_ + alphaF_ ;
            beta_ = 0.25 * std::pow(gamma_ + 0.5, 2) ;

        }
        else if (type == GAType::HHT)
        {
            alphaM_ = 0. ;
            alphaF_ = 0.05 ; //1./3. ;
            beta_ = std::pow(1. + alphaF_, 2) / 4 ;
            gamma_ = 0.5 + alphaF_ ;
        }
        //std::cout << "rhoInf: " << rhoInf_ << std::endl ;
        //std::cout << "alphaM: " << alphaM_ << std::endl ;
        //std::cout << "alphaF: " << alphaF_ << std::endl ;
        //std::cout << "gamma: " << gamma_ << std::endl ;
        //std::cout << "beta: " << beta_ << std::endl ;
    }

    template <class System, class StateInOut>
    void do_step(System system, StateInOut & x, time_type t, time_type dt)
    {
        do_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt)
    {
        do_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out,
                 time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_and_jac ;
        typedef typename std::remove_reference<typename system_and_jac::first_type>::type system_type ;
        system_and_jac & sysAndJac = system ;
        system_type & sys = sysAndJac.first ;

        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        do_step_impl(sys, in, t, out, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t, StateOut & out,
                 time_type dt)
    {
        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        do_step_impl(system, in, t, out, dt) ;
    }

    template <class StateIn>
    void adjust_size(const StateIn & x)
    {
        resize_impl(x) ;
    }

    order_type order(void) const
    {
        return 2 ;
    }

    unsigned get_n_iterations() const
    {
        return nIterations_ ;
    }
    bool has_converged() const
    {
        return hasConverged_ ;
    }

    //template <typename System, typename PosT, typename ResT>
    //void posVariation(System & sys, const PosT & pos, ResT & res, const Time & t)
    //{
    //    // Compute the loads
    //    sys(pos, qpNP1_, FNP1_, t) ;
    //    res = M_ * qppNP1_ - FNP1_ ; // FIXME
    //}

    //template <typename System, typename VelT, typename ResT>
    //void velVariation(System & sys, const VelT & vel, ResT & res, const Time & t)
    //{
    //    // Compute the loads
    //    sys(qNP1_, vel, FNP1_, t) ;
    //    res = -FNP1_ ;
    //}

private:

    template <class StateIn>
    bool resize_impl(const StateIn & x)
    {
        return true ;
    }

    template <class System, class StateIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, time_type t,
                      StateOut & out, time_type dt)
    {
        typedef typename std::remove_reference<System>::type SystemType ;
        SystemType & sys = system ;
        // std::cout << "t: " << t << " dt: " << dt << " " ;
        std::size_t nUnknowns(in.size()/2) ;
        using MatType = typename GA::MTypeTraits<StateIn>::mtype ;
        using VType = typename GA::MTypeTraits<StateIn>::vtype ;
        if (!initialized_)
        {
            qN_ = VType::Zero(nUnknowns) ;
            qpN_ = VType::Zero(nUnknowns) ;
            qppN_ = VType::Zero(nUnknowns) ;
            aN_ = VType::Zero(nUnknowns) ;
            qNP1_ = VType::Zero(nUnknowns) ;
            qpNP1_ = VType::Zero(nUnknowns) ;
            aNP1_ = VType::Zero(nUnknowns) ;
            FNP1_ = VType::Zero(nUnknowns) ;
            dQ_ = VType::Zero(nUnknowns) ;
            M_ = MatType::Zero(nUnknowns, nUnknowns) ;
            C_ = MatType::Zero(nUnknowns, nUnknowns) ;
            K_ = MatType::Zero(nUnknowns, nUnknowns) ;
            R_ = VType::Zero(nUnknowns) ;
            J_ = MatType::Zero(nUnknowns, nUnknowns) ;
            // FIXME hardcoded !
            //BV::Math::Solvers::SolverParameters params ;
            //params.set("maxIterations", static_cast<unsigned>(20)) ;
            //params.set("absoluteError", 1.e-3) ; // FIXME
            //params.set("verbose", false) ;
            //params.set("jacobianDelta", 1.e-8) ;
            //params.set("computeJOnce", true) ; // FIXME change this ?
            //solver_.setParameters(params) ;
            initialized_ = true ;
        }
        out = in ;
        sys(in, qN_, qpN_, M_, t) ;
        // Prediction
        aNP1_ = 1. / (1.- alphaM_) * (alphaF_ * qppN_ - alphaM_ * aN_) ;
        qpNP1_ = qpN_ + dt * (1. - gamma_) * aN_ + dt * gamma_ * aNP1_;
        qNP1_ = qN_ + dt * qpN_ + dt * dt * (0.5 - beta_) * aN_ + dt * dt * beta_ * aNP1_ ;
        qppNP1_ = VType::Zero(nUnknowns) ;
        betap_ = (1.-alphaM_)/(dt*dt*beta_*(1.-alphaF_)) ;
        gammap_ = gamma_ / (dt * beta_) ;
        //bool JComputed(false) ;
        nIterations_ = 0 ;
        hasConverged_ = false ;
        Value scaleFactor(beta_ * dt * dt) ;
        while (nIterations_<20) // FIXME
        {
            // Compute residuals
            sys(qNP1_, qpNP1_, M_, t) ;
            sys(qNP1_, qpNP1_, qppNP1_, FNP1_, t+dt, true) ;
            R_ = scaleFactor * (M_ * qppNP1_ + - FNP1_) ;
            //std::cout << "i: " << nIterations_ << std::endl;
            //std::cout << "M: " << std::endl ;
            //std::cout << M_ << std::endl ;
            //std::cout << "qN: " << qN_.transpose() << std::endl ;
            //std::cout << "qNP1: " << qNP1_.transpose() << std::endl ;
            //std::cout << "F: " << FNP1_.transpose() << std::endl ;
            //std::cout << "R: " << R_.transpose() << std::endl ;
            double eps(scaleFactor * 1.e-6) ;
            //if ((R_.array().abs() < eps).all()) // FIXME
            double Rnorm(R_.norm()) ;
            double Fnorm(FNP1_.norm()) ;
            if (std::isfinite(Rnorm) && std::isfinite(Fnorm)
                && (R_.norm() <= (FNP1_.norm() * eps)))
            {
                hasConverged_ = true ;
                break ;
            }
            //if (nIterations_ % 2 == 0)
            //{
            //    // We try to recompute J to get closer to the solution
            //    JComputed = false ;
            //}
            //if (!JComputed) // FIXME only once...
            //{
                sys(t+dt, qNP1_, qpNP1_, qppNP1_, K_) ;
                sys(t+dt, qNP1_, qpNP1_, qppNP1_, C_, true) ;
                //setK_(system, t+dt) ;
                //setC_(system, t+dt) ;
                J_ = scaleFactor * (betap_ * M_ + gammap_ * C_ + K_) ;
                //std::cout << "K:" << std::endl ;
                //std::cout << K_ << std::endl ;
                //std::cout << "C:" << std::endl ;
                //std::cout << C_ << std::endl ;
            //    JComputed = true ;
            //}
            //Eigen::ColPivHouseholderQR<MatType> qrDecomposition(J_) ;
            //qrDecomposition.setThreshold(1.e-6) ;
            Eigen::HouseholderQR<MatType> qrDecomposition(J_) ;
            dQ_ = qrDecomposition.solve(-R_) ;
            //std::cout << "J_:" << std::endl ;
            //std::cout << J_ << std::endl ;
            //std::cout << "qNP1 before: "   << qNP1_.transpose() << std::endl ;
            //std::cout << "qpNP1 before: "  << qpNP1_.transpose() << std::endl ;
            //std::cout << "qppNP1 before: " << qppNP1_.transpose() << std::endl ;
            //std::cout << "dQ: " << dQ_.transpose() << std::endl ;
            // Update
            qNP1_ += dQ_ ;
            qpNP1_ += gammap_ * dQ_ ;
            qppNP1_ += betap_ * dQ_ ;
            //std::cout << "qNP1 after: "   << qNP1_.transpose() << std::endl ;
            //std::cout << "qpNP1 after: "  << qpNP1_.transpose() << std::endl ;
            //std::cout << "qppNP1 after: " << qppNP1_.transpose() << std::endl ;
            ++nIterations_ ;
        }
        if (hasConverged_)
        {
            std::cout << "nIterations: " << nIterations_ << std::endl ;
            qN_ = qNP1_ ;
            qpN_ = qpNP1_ ;
            qppN_ = qppNP1_ ;
            aN_ = aNP1_ + (1. - alphaF_)/(1. - alphaM_) * qppNP1_ ;
            sys(qN_, qpN_, qppN_, out, false, false) ;
        }
        else
        {
            std::cout << "Solution not found after " << nIterations_ << " iterations" << std::endl ;
        }
    }

    template <class System , class StateInOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_v1(System system, StateInOut & x, time_type t, time_type dt)
    {
        m_resizer.adjust_size(x, boostode::detail::bind(
                  &internal_stepper_base_type::template resize_impl<StateInOut>,
                  boostode::detail::ref(*this), boostode::detail::_1
                                                       )) ;
        do_step_impl(system, x, t, x, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step_v1(System system, StateInOut & x, time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_and_jac ;
        typedef typename std::remove_reference<typename system_and_jac::first_type>::type system_type ;
        system_and_jac & sysAndJac = system ;
        system_type & sys = sysAndJac.first ;

        m_resizer.adjust_size(x, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        do_step_impl(sys, x, t, x, dt) ;
    }

    //template <typename System>
    //void setK_(System system, const Time & t)
    //{
    //    typedef typename std::remove_reference<System>::type SystemType ;
    //    SystemType & sys = system ;
    //    //// Compute the stiffness matrix via finite difference
    //    //// We assume the state has been updated
    //    //namespace FD = BV::Math::FiniteDifference ;
    //    //typedef FD::FiniteDifference<FD::FDScheme::CENTRAL,
    //    //                             FD::IthDerivative::FIRST, 2,
    //    //                             FD::FDContext::JACOBIAN_CALCULATION> FDType ;
    //    //typedef GA::KFDWrapper<internal_stepper_base_type, SystemType, Time> WrapperK ;
    //    //WrapperK wrapperK(*this, sys, t) ;
    //    //std::function<Eigen::VectorXd(const Eigen::Ref<const Eigen::VectorXd> &)> fK(
    //    //                std::bind(&WrapperK::get, wrapperK, std::placeholders::_1)
    //    //                                                                            ) ;
    //    //K_ = FDType::get(qNP1_, fK, 1.e-6) ;
    //    // FIXME manual calculation
    //    Value h(1.e-6) ;
    //    Eigen::Index nUnknowns(qNP1_.size()) ;
    //    K_.setZero() ;
    //    // FIXME not dependent on qppNP1_...
    //    // This should be the jacobian of: M_ * qppNP1_ - FNP1_
    //    for (Eigen::Index i=0; i<nUnknowns; ++i)
    //    {
    //        qNP1_(i) += 0.5 * h ;
    //        sys(qNP1_, qpNP1_, qppNP1_, FNP1_, t) ;
    //        K_.col(i) += -FNP1_ ;
    //        qNP1_(i) -= h ;
    //        sys(qNP1_, qpNP1_, qppNP1_, FNP1_, t) ;
    //        K_.col(i) -= -FNP1_ ;
    //        qNP1_(i) += 0.5 * h ;
    //        K_.col(i) /= h ;
    //    }
    //}

    //template <typename System>
    //void setC_(System system, const Time & t)
    //{
    //    typedef typename std::remove_reference<System>::type SystemType ;
    //    SystemType & sys = system ;
    //    //// Compute the stiffness matrix via finite difference
    //    //// We assume the state has been updated
    //    //namespace FD = BV::Math::FiniteDifference ;
    //    //typedef FD::FiniteDifference<FD::FDScheme::CENTRAL,
    //    //                             FD::IthDerivative::FIRST, 2,
    //    //                             FD::FDContext::JACOBIAN_CALCULATION> FDType ;
    //    //typedef GA::GFDWrapper<internal_stepper_base_type, SystemType, Time> WrapperG ;
    //    //WrapperG wrapperG(*this, sys, t) ;
    //    //std::function<Eigen::VectorXd(const Eigen::Ref<const Eigen::VectorXd> &)> fG(
    //    //                std::bind(&WrapperG::get, wrapperG, std::placeholders::_1)
    //    //                                                                            ) ;
    //    //C_ = FDType::get(qpNP1_, fG, 1.e-6) ;
    //    // FIXME manual calculation
    //    Value h(1.e-8) ;
    //    Eigen::Index nUnknowns(qpNP1_.size()) ;
    //    C_.setZero() ;
    //    for (Eigen::Index i=0; i<nUnknowns; ++i)
    //    {
    //        qpNP1_(i) += 0.5 * h ;
    //        sys(qNP1_, qpNP1_, qppNP1_, FNP1_, t) ;
    //        C_.col(i) += -FNP1_ ;
    //        qpNP1_(i) -= h ;
    //        sys(qNP1_, qpNP1_, qppNP1_, FNP1_, t) ;
    //        C_.col(i) -= -FNP1_ ;
    //        qpNP1_(i) += 0.5 * h ;
    //        C_.col(i) /= h ;
    //    }
    //}

    resizer_type m_resizer;

protected:

    Value rhoInf_ ;
    Value alphaM_ ;
    Value alphaF_ ;
    Value beta_ ;
    Value gamma_ ;
    State qN_ ; // position
    State qpN_ ; // velocity
    State qppN_ ; // acceleration
    State aN_ ;
    State qNP1_ ; // position
    State qpNP1_ ; // velocity
    State qppNP1_ ; // acceleration
    State aNP1_ ;
    State FNP1_ ;
    typename GA::MTypeTraits<State>::mtype M_ ;
    typename GA::MTypeTraits<State>::mtype C_ ;
    typename GA::MTypeTraits<State>::mtype K_ ;
    State R_ ;
    typename GA::MTypeTraits<State>::mtype J_ ;
    State dQ_ ;
    bool initialized_ ;
    Value gammap_ ;
    Value betap_ ;
    std::size_t nIterations_ ;
    bool hasConverged_ ;
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
