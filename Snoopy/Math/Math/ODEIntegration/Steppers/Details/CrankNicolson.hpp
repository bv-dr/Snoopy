#ifndef BV_Math_ODEIntegration_Steppers_Details_CrankNicolson_hpp
#define BV_Math_ODEIntegration_Steppers_Details_CrankNicolson_hpp

#include <type_traits>
#include <utility>
#include <array>

#include <Eigen/Dense>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/push_back.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/range_c.hpp>
#include <boost/mpl/copy.hpp>
#include <boost/mpl/size_t.hpp>

#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/iterator.hpp>
#include <boost/fusion/mpl.hpp>
#include <boost/fusion/sequence.hpp>

#include <boost/numeric/odeint/algebra/range_algebra.hpp>
#include <boost/numeric/odeint/algebra/default_operations.hpp>
#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/ImplicitGenericRK.hpp"
#include "Math/ODEIntegration/Steppers/Details/ImplicitStepperBase.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template <class State, class DState, class DerFunc, class Time>
class SolverFunctionCN
{
private:
    const State & xN_ ;
    const DState & dxNdt_ ;
    DerFunc & func_ ;
    const Time & dt_ ;

public:
    SolverFunctionCN(const State & state, const DState & dState,
                     DerFunc & func, const Time & dt) :
        xN_(state), dxNdt_(dState), func_(func), dt_(dt)
    {
    }

    template <class StateIn, class Deriv>
    void operator()(const StateIn & state, Deriv & dxdt, const Time & t)
    {
        func_(state, dxdt, t+dt_) ;
        dxdt += dxNdt_ ;
        dxdt *= 0.5 * dt_ ;
        dxdt += xN_ - state ;
    }
} ;

template <class State, class JFunc, class Time, class Value>
class SolverDFunctionCN
{
private:
    typedef JTypeTraits<State, Value, JFunc, 1> Traits ;
    JFunc & func_ ;
    const Time & dt_ ;

public:

    typedef typename Traits::JType JType ;

    template <typename StateIn>
    JType getJInitialized(const StateIn & state) const
    {
        return Traits::getJInitialized(func_, state) ;
    }

    SolverDFunctionCN(JFunc & func, const Time & dt) :
        func_(func), dt_(dt)
    {
    }

    template <class StateIn, class JTypeIn>
    void operator()(const StateIn & state, JTypeIn & J, const Time & t)
    {
        func_(state, J, t) ; // FIXME t or t+dt ?
        J *= 0.5 * dt_ ;
        Traits::subtractIdentity(J) ;
    }
} ;

template<
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
        >
class CrankNicolson :
    public implicit_stepper_base<
               CrankNicolson<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
               2, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                >
{
public :
    typedef implicit_stepper_base<
               CrankNicolson<State, Value, Deriv, Time, Algebra, Operations, Resizer>,
               2, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                 > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type;
    typedef typename stepper_base_type::value_type value_type;
    typedef typename stepper_base_type::deriv_type deriv_type;
    typedef typename stepper_base_type::time_type time_type;
    typedef typename stepper_base_type::algebra_type algebra_type;
    typedef typename stepper_base_type::operations_type operations_type;
    typedef typename stepper_base_type::resizer_type resizer_type;

    typedef typename stepper_base_type::stepper_type stepper_type;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type;

    typedef TypeTraitsIRK<State, value_type, 1> Traits ;

    // FIXME it would also have been possible to implement a Wilson-theta
    // scheme but the constructor would not have been standard
    // as we would need to provide the theta...
    CrankNicolson(const algebra_type &algebra = algebra_type()) :
        stepper_base_type(algebra)
    {
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        //typedef typename std::remove_reference<System>::type system_type ;
        //typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;
        //typedef typename std::remove_reference<typename system_type::second_type>::type jacobi_func_type ;
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;
        typedef typename std::remove_reference<typename system_type::second_type>::type jacobi_func_type ;
        system_type & sys = system ;
        deriv_func_type & deriv_func = sys.first ;
        jacobi_func_type & jacobi_func = sys.second ;

        m_resizer.adjust_size(in, boostode::detail::bind(
                            &stepper_type::template resize_impl<state_type>,
                            boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;

        // Solve
        typedef SolverFunctionCN<StateIn, DerivIn, deriv_func_type,
                                 time_type> SolverFuncType ;
        typedef SolverDFunctionCN<StateIn, jacobi_func_type, time_type,
                                  value_type> SolverDFuncType ;
        SolverFuncType func(in, dxdt, deriv_func, dt) ;
        SolverDFuncType dFunc(jacobi_func, dt) ;

        State xNP1(in) ;
        this->solver_.solve(std::make_pair(func, dFunc), in, xNP1, t) ;
        // Finish the CN scheme
        // xNP1 = xN + 0.5*dt*(f(xNP1) + f(xN))
        DerivIn dxNP1dt(dxdt) ;
        deriv_func(xNP1, dxNP1dt, t+dt) ;
        out = in + 0.5 * dt * (dxNP1dt + dxdt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        // Jacobian is evaluated via a finite difference scheme
        // FIXME factorise the type traits and below wrapper
        typedef BV::Math::Solvers::Details::FDSystem<System, StateIn, value_type> FDSys ;
        FDSys fd(system, 1.e-8) ; // FIXME
        do_step_impl(std::make_pair(std::ref(system), std::ref(fd)),
                     in, dxdt, t, out, dt) ;
    }

    template <class StateType>
    void adjust_size(const StateType & x)
    {
        resize_impl(x) ;
        stepper_base_type::adjust_size(x) ;
    }

private:

    template <class StateIn>
    bool resize_impl(const StateIn & x)
    {
        //bool resized = false ;
        // FIXME
        return true ;
    }

    resizer_type m_resizer;

} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_CrankNicolson_hpp
