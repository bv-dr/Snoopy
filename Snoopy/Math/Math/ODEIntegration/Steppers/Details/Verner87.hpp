#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner87_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner87_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

#include "Math/ODEIntegration/Steppers/Details/Verner78.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

// 8th order coefficients
template <typename Value=double>
struct Verner87_b : boost::array<Value, 13>
{
    Verner87_b(void)
    {
        (*this)[0] = static_cast<Value>(31.) / static_cast<Value>(720.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(0.) ;
        (*this)[3] = static_cast<Value>(0.) ;
        (*this)[4] = static_cast<Value>(0.) ;
        (*this)[5] = static_cast<Value>(16.) / static_cast<Value>(75.) ;
        (*this)[6] = static_cast<Value>(16807.) / static_cast<Value>(79200.) ;
        (*this)[7] = static_cast<Value>(16807.) / static_cast<Value>(79200.) ;
        (*this)[8] = static_cast<Value>(243.) / static_cast<Value>(1760.) ;
        (*this)[9] = static_cast<Value>(0.) ;
        (*this)[10] = static_cast<Value>(0.) ;
        (*this)[11] = static_cast<Value>(437400.) / static_cast<Value>(3168000.) ;
        (*this)[12] = static_cast<Value>(136400.) / static_cast<Value>(3168000.) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner87 :
            public boostode::explicit_error_generic_rk<13, 8, 8, 7, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<13, 8, 8, 7, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner87(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner78_a1<Value>(),
                                                     Verner78_a2<Value>(),
                                                     Verner78_a3<Value>(),
                                                     Verner78_a4<Value>(),
                                                     Verner78_a5<Value>(),
                                                     Verner78_a6<Value>(),
                                                     Verner78_a7<Value>(),
                                                     Verner78_a8<Value>(),
                                                     Verner78_a9<Value>(),
                                                     Verner78_a10<Value>(),
                                                     Verner78_a11<Value>(),
                                                     Verner78_a12<Value>()),
                          Verner87_b<Value>(), Verner78_db<Value>(),
                          Verner78_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner87_hpp
