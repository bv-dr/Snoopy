#ifndef BV_Math_ODEIntegration_Steppers_Details_StepAdjuster_hpp
#define BV_Math_ODEIntegration_Steppers_Details_StepAdjuster_hpp

#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/stepper/base/explicit_error_stepper_base.hpp>

#include "Math/ODEIntegration/Details.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template <typename Value, typename Time>
class StepAdjuster2
{
public:
    typedef Time time_type ;
    typedef Value value_type ;

    StepAdjuster2(const time_type maxDt=static_cast<time_type>(0)) :
        maxDt_(maxDt)
    {
    }

    time_type decrease_step(time_type dt, const value_type error,
                            const int error_order) const
    {
        // returns the decreased time step
        dt /= static_cast<time_type>(2) ;
        if(maxDt_ != static_cast<time_type >(0))
        {
            // limit to maximal stepsize even when decreasing
            dt = boostode::detail::min_abs(dt, maxDt_);
        }
        return dt ;
    }

    time_type increase_step(time_type dt, value_type error,
                            const int stepper_order) const
    {
        // returns the increased time step
        BOOST_USING_STD_MAX() ;
        // adjust the size if dt is smaller than maxDt_ (provided maxDt is not zero)
        if(error < 0.5)
        {
            // error should be > 0
            error = max BOOST_PREVENT_MACRO_SUBSTITUTION (
                    static_cast<value_type>(pow(static_cast<value_type>(2.),
                                                -static_cast<value_type>(stepper_order))),
                    error
                                                         ) ;
            //time_type dtOld = dt ;
            //error too small - increase dt
            dt *= static_cast<value_type>(2) ;
            if(maxDt_ != static_cast<time_type >(0))
            {
                // limit to maximal stepsize
                dt = boostode::detail::min_abs(dt, maxDt_) ;
            }
        }
        return dt ;
    }

    bool check_step_size_limit(const time_type dt)
    {
        if (maxDt_ != static_cast<time_type >(0))
        {
            return boostode::detail::less_eq_with_sign(dt, maxDt_, dt) ;
        }
        return true;
    }

    time_type get_max_dt() { return maxDt_ ; }

private:
    time_type maxDt_ ;
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_StepAdjuster_hpp
