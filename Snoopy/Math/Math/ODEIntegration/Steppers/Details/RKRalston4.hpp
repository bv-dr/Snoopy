#ifndef BV_Math_ODEIntegration_Steppers_Details_RKRalston4_hpp
#define BV_Math_ODEIntegration_Steppers_Details_RKRalston4_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct RKRalston4_a1 : boost::array<Value, 1>
{
    RKRalston4_a1(void)
    {
        (*this)[0] = static_cast<Value>(2) / static_cast<Value>(5) ;
    }
} ;

template <typename Value=double>
struct RKRalston4_a2 : boost::array<Value, 2>
{
    RKRalston4_a2(void)
    {
        const Value sqrt5(static_cast<Value>(std::sqrt(5.))) ;
        (*this)[0] = static_cast<Value>(-2889.+1428.*sqrt5) / static_cast<Value>(1024.) ;
        (*this)[1] = static_cast<Value>(3785. - 1620. * sqrt5) / static_cast<Value>(1024.) ;
    }
} ;

template <typename Value=double>
struct RKRalston4_a3 : boost::array<Value, 3>
{
    RKRalston4_a3(void)
    {
        const Value sqrt5(static_cast<Value>(std::sqrt(5.))) ;
        (*this)[0] = static_cast<Value>(-3365. + 2094. * sqrt5) / static_cast<Value>(6040.) ;
        (*this)[1] = static_cast<Value>(-975. - 3046. * sqrt5) / static_cast<Value>(2552.) ;
        (*this)[2] = static_cast<Value>(467040. + 203968. * sqrt5) / static_cast<Value>(240845) ;
    }
} ;

template <typename Value=double>
struct RKRalston4_b : boost::array<Value, 4>
{
    RKRalston4_b(void)
    {
        const Value sqrt5(static_cast<Value>(std::sqrt(5.))) ;
        (*this)[0] = static_cast<Value>(263.+24.*sqrt5) / static_cast<Value>(1812.) ;
        (*this)[1] = static_cast<Value>(125.-1000.*sqrt5) / static_cast<Value>(3828.) ;
        (*this)[2] = static_cast<Value>(1024.*(3346.+1623.*sqrt5)) / static_cast<Value>(5924787.) ;
        (*this)[3] = static_cast<Value>(30.-4.*sqrt5) / static_cast<Value>(123.) ;
    }
} ;

template <typename Value=double>
struct RKRalston4_c : boost::array<Value, 4>
{
    RKRalston4_c(void)
    {
        const Value sqrt5(static_cast<Value>(std::sqrt(5.))) ;
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(2) / static_cast<Value>(5) ;
        (*this)[2] = static_cast<Value>(14.-3.*sqrt5) / static_cast<Value>(16.) ;
        (*this)[3] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class RKRalston4 :
            public boostode::explicit_generic_rk<4, 4, State, Value, Deriv,
                                                 Time, Algebra, Operations,
                                                 Resizer>
{
public:
    typedef boostode::explicit_generic_rk<4, 4, State, Value, Deriv,
                                          Time, Algebra, Operations,
                                          Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    RKRalston4(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(RKRalston4_a1<Value>(),
                                                     RKRalston4_a2<Value>(),
                                                     RKRalston4_a3<Value>()),
                          RKRalston4_b<Value>(),
                          RKRalston4_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_RKRalston4_hpp
