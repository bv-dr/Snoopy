#pragma once

#include <type_traits>

#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>

#include <boost/numeric/odeint/stepper/stepper_categories.hpp>

#include <boost/numeric/odeint/stepper/base/algebra_stepper_base.hpp>

#include "Math/ODEIntegration/Details.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template<
    class Stepper,
    class Resizer=boostode::initially_resizer
        >
class ControlledNLImplicitStepper
{

public:

    typedef Stepper stepper_type ;
    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef Resizer resizer_type ;

    struct error_checker_type
    {
        value_type aTol ;
        value_type rTol ;
        value_type scaleYError ;
        value_type scaleDYError ;
        error_checker_type(value_type aTol_=1.e-6,
                           value_type rTol_=1.e-6,
                           value_type scaleYError_=1.,
                           value_type scaleDYError_=0.) :
            aTol(aTol_), rTol(rTol_), scaleYError(scaleYError_), scaleDYError(scaleDYError_)
        {
        }
    } ;
    struct step_adjuster_type
    {
        time_type maxStepSize_ ;
        unsigned maxIterations_ ;
        step_adjuster_type(time_type maxStepSize=200., unsigned maxIterations=10) :
            maxStepSize_(maxStepSize), maxIterations_(maxIterations)
        {
        }
        bool check_step_size_limit(time_type dt)
        {
            return dt < maxStepSize_ + 1.e-10 ;
        }
        time_type get_max_dt()
        {
            return maxStepSize_ ;
        }
        time_type decrease_step(time_type dt, unsigned nIterations)
        {
            // FIXME what ratio should we use ?
            return dt / 1.5 ;
        }
        time_type increase_step(time_type dt, unsigned nIterations)
        {
            // FIXME what ratio should we use ?
            return dt * 1.2 ;
        }
        unsigned get_max_iterations()
        {
            return maxIterations_ ;
        }
    } ;

    typedef boostode::explicit_controlled_stepper_tag stepper_category; //FIXME add new tag ?

    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;

    typedef ControlledNLImplicitStepper<Stepper, Resizer> controlled_stepper_type ;

    ControlledNLImplicitStepper(
            const error_checker_type & error_checker=error_checker_type(),
            const step_adjuster_type & step_adjuster=step_adjuster_type(),
            const stepper_type & stepper=stepper_type()
                       ) :
         m_stepper(stepper), m_error_checker(error_checker),
         m_step_adjuster(step_adjuster), nSuccessfulIterations_(0)
    {
    }

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return try_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step(System system, const StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return try_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateIn, class StateOut>
    boostode::controlled_step_result try_step(System system, const StateIn & in,
                                              time_type & t,
                                              StateOut & out, time_type & dt)
    {
        if (!m_step_adjuster.check_step_size_limit(dt))
        {
            // given dt was above step size limit - adjust and return fail;
            dt = m_step_adjuster.get_max_dt() ;
            return boostode::fail ;
        }

        m_xerr_resizer.adjust_size(in, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_xerr_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                             )) ;

        // do one step with error calculation
        try {
            m_stepper.do_step(system, in, t, out, dt) ;
        } catch (...)
        {
             // Newton solver may fail violently...
        }

        unsigned nIterations(m_stepper.get_n_iterations()) ;
        unsigned maxIterations(m_step_adjuster.get_max_iterations()) ;
        bool hasConverged(m_stepper.has_converged()) ;
        if ((!hasConverged) && (nIterations < maxIterations))
        {
            nIterations = maxIterations * 2 ;
        }

        if ((nIterations > maxIterations) || (!hasConverged))
        {
            // error too big, decrease step size and reject this step
            dt = m_step_adjuster.decrease_step(dt, nIterations) ;
            nSuccessfulIterations_ = 0 ;
            return boostode::fail ;
        }
        else
        {
            // otherwise, increase step size and accept
            t += dt;
            ++nSuccessfulIterations_ ;
            if (nSuccessfulIterations_ > 20) // FIXME parameter
            {
                dt = m_step_adjuster.increase_step(dt, nIterations) ;
                nSuccessfulIterations_ = 0 ;
            }
            return boostode::success ;
        }
    }

    /**
     * \brief Adjust the size of all temporaries in the stepper manually.
     * \param x A state from which the size of the temporaries to be resized is deduced.
     */
    template< class StateType >
    void adjust_size( const StateType &x )
    {
        resize_m_xerr_impl( x );
        resize_m_xnew_impl( x );
        m_stepper.adjust_size( x );
    }

    /**
     * \brief Returns the instance of the underlying stepper.
     * \returns The instance of the underlying stepper.
     */
    stepper_type& stepper( void )
    {
        return m_stepper;
    }

    /**
     * \brief Returns the instance of the underlying stepper.
     * \returns The instance of the underlying stepper.
     */
    const stepper_type& stepper( void ) const
    {
        return m_stepper;
    }

private:


    template <class System, class StateInOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step_v1(System system, StateInOut & x,
                                                 time_type & t, time_type & dt)
    {
        return try_step(system, x, t, x, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step_v1(System system, StateInOut & x,
                                                 time_type & t, time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_and_jac ;
        typedef typename std::remove_reference<typename system_and_jac::first_type>::type system_type ;
        system_and_jac & sysAndJac = system ;
        system_type & sys = sysAndJac.first ;

        return try_step(sys, x, t, x, dt) ;
    }

    template< class StateIn >
    bool resize_m_xerr_impl( const StateIn &x )
    {
        return adjust_size_by_resizeability( m_xerr , x , typename boostode::is_resizeable<state_type>::type() );
    }

    template< class StateIn >
    bool resize_m_xnew_impl( const StateIn &x )
    {
        return adjust_size_by_resizeability( m_xnew , x , typename boostode::is_resizeable<state_type>::type() );
    }

    stepper_type m_stepper;
    error_checker_type m_error_checker;
    step_adjuster_type m_step_adjuster;

    resizer_type m_xerr_resizer;
    resizer_type m_xnew_resizer;

    wrapped_state_type m_xerr;
    wrapped_state_type m_xnew;
    std::size_t nSuccessfulIterations_ ;
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
