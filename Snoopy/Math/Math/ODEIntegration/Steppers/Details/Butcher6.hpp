#ifndef BV_Math_ODEIntegration_Steppers_Details_Butcher6_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Butcher6_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Butcher6_a1 : boost::array<Value, 1>
{
    Butcher6_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(3) ;
    }
} ;

template <typename Value=double>
struct Butcher6_a2 : boost::array<Value, 2>
{
    Butcher6_a2(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(2) / static_cast<Value>(3) ;
    }
} ;

template <typename Value=double>
struct Butcher6_a3 : boost::array<Value, 3>
{
    Butcher6_a3(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(12) ;
        (*this)[1] = static_cast<Value>(4) / static_cast<Value>(12) ;
        (*this)[2] = static_cast<Value>(-1) / static_cast<Value>(12) ;
    }
} ;

template <typename Value=double>
struct Butcher6_a4 : boost::array<Value, 4>
{
    Butcher6_a4(void)
    {
        (*this)[0] = static_cast<Value>(-1) / static_cast<Value>(16) ;
        (*this)[1] = static_cast<Value>(18) / static_cast<Value>(16) ;
        (*this)[2] = static_cast<Value>(-3) / static_cast<Value>(16) ;
        (*this)[3] = static_cast<Value>(-6) / static_cast<Value>(16) ;
    }
} ;

template <typename Value=double>
struct Butcher6_a5 : boost::array<Value, 5>
{
    Butcher6_a5(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(9) / static_cast<Value>(8) ;
        (*this)[2] = static_cast<Value>(-3) / static_cast<Value>(8) ;
        (*this)[3] = static_cast<Value>(-6) / static_cast<Value>(8) ;
        (*this)[4] = static_cast<Value>(4) / static_cast<Value>(8) ;
    }
} ;

template <typename Value=double>
struct Butcher6_a6 : boost::array<Value, 6>
{
    Butcher6_a6(void)
    {
        (*this)[0] = static_cast<Value>(9) / static_cast<Value>(44) ;
        (*this)[1] = static_cast<Value>(-36) / static_cast<Value>(44) ;
        (*this)[2] = static_cast<Value>(63) / static_cast<Value>(44) ;
        (*this)[3] = static_cast<Value>(72) / static_cast<Value>(44) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(-64) / static_cast<Value>(44) ;
    }
} ;

template <typename Value=double>
struct Butcher6_b : boost::array<Value, 7>
{
    Butcher6_b(void)
    {
        (*this)[0] = static_cast<Value>(11) / static_cast<Value>(120) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(81) / static_cast<Value>(120) ;
        (*this)[3] = static_cast<Value>(81) / static_cast<Value>(120) ;
        (*this)[4] = static_cast<Value>(-32) / static_cast<Value>(120) ;
        (*this)[5] = static_cast<Value>(-32) / static_cast<Value>(120) ;
        (*this)[6] = static_cast<Value>(11) / static_cast<Value>(120) ;
    }
} ;

template <typename Value=double>
struct Butcher6_c : boost::array<Value, 7>
{
    Butcher6_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(3) ;
        (*this)[2] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(3) ;
        (*this)[4] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[5] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[6] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Butcher6 :
            public boostode::explicit_generic_rk<7, 6, State, Value, Deriv,
                                                 Time, Algebra, Operations,
                                                 Resizer>
{
public:
    typedef boostode::explicit_generic_rk<7, 6, State, Value, Deriv,
                                          Time, Algebra, Operations,
                                          Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Butcher6(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Butcher6_a1<Value>(),
                                                     Butcher6_a2<Value>(),
                                                     Butcher6_a3<Value>(),
                                                     Butcher6_a4<Value>(),
                                                     Butcher6_a5<Value>(),
                                                     Butcher6_a6<Value>()),
                          Butcher6_b<Value>(),
                          Butcher6_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Butcher6_hpp
