#ifndef BV_Math_ODEIntegration_Steppers_Details_ImplicitErrorStepperBase_hpp
#define BV_Math_ODEIntegration_Steppers_Details_ImplicitErrorStepperBase_hpp

#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_same.hpp>


#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>
#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>

#include <boost/numeric/odeint/stepper/stepper_categories.hpp>

#include <boost/numeric/odeint/stepper/base/algebra_stepper_base.hpp>

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template<
    class Stepper,
    unsigned short Order,
    unsigned short StepperOrder,
    unsigned short ErrorOrder,
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer
        >
class ImplicitErrorStepperBase :
            public boostode::algebra_stepper_base<Algebra, Operations>
{
public:

    typedef boostode::algebra_stepper_base<Algebra, Operations> algebra_stepper_base_type ;
    typedef typename algebra_stepper_base_type::algebra_type algebra_type ;


    typedef State state_type ;
    typedef Value value_type ;
    typedef Deriv deriv_type ;
    typedef Time time_type ;
    typedef Resizer resizer_type ;
    typedef Stepper stepper_type ;
    typedef boostode::explicit_error_stepper_tag stepper_category ; // FIXME
    typedef boostode::state_wrapper<state_type> wrapped_state_type ;
    typedef boostode::state_wrapper<deriv_type> wrapped_deriv_type ;
    typedef ImplicitErrorStepperBase<Stepper, Order, StepperOrder, ErrorOrder,
                                     State, Value, Deriv, Time, Algebra,
                                     Operations, Resizer> internal_stepper_base_type ;

    typedef unsigned short order_type ;
    static const order_type order_value = Order ;
    static const order_type stepper_order_value = StepperOrder ;
    static const order_type error_order_value = ErrorOrder ;


    ImplicitErrorStepperBase(const algebra_type & algebra=algebra_type()) :
        algebra_stepper_base_type(algebra)
    {
    }

    order_type order(void) const
    {
        return order_value ;
    }

    order_type stepper_order(void) const
    {
        return stepper_order_value ;
    }

    order_type error_order(void) const
    {
        return error_order_value ;
    }

    template <class System, class StateInOut>
    void do_step(System system, StateInOut &x, time_type t, time_type dt)
    {
        do_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut>
    void do_step(System system, const StateInOut & x, time_type t, time_type dt)
    {
        do_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, StateInOut & x, const DerivIn & dxdt, time_type t,
            time_type dt)
    {
        this->stepper().do_step_impl(system, x, dxdt, t, x, dt) ;
    }

    template <class System, class StateInOut, class DerivIn>
    void do_step_dxdt_impl(System system, StateInOut & x, const DerivIn & dxdt,
                           time_type t, time_type dt)
    {
        this->stepper().do_step_impl(system, x, dxdt, t, x, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                      typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<StateIn, time_type>, void>::type
    do_step(System system, const StateIn & in, time_type t, StateOut & out,
            time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;

        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        system_type & sys(system) ;
        deriv_func_type & derFunc(sys.first) ;
        derFunc(in, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, in, m_dxdt.m_v, t, out, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                      typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<StateIn, time_type>, void>::type
    do_step(System system, const StateIn & in, time_type t, StateOut & out,
            time_type dt)
    {
        typename std::remove_reference<System>::type & sys = system ;
        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        sys(in, m_dxdt.m_v,t) ;
        this->stepper().do_step_impl(system, in, m_dxdt.m_v, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, const StateIn & in, const DerivIn & dxdt,
            time_type t, StateOut & out, time_type dt)
    {
        this->stepper().do_step_impl(system, in, dxdt, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    void do_step_dxdt_impl(System system, const StateIn & in,
                           const DerivIn & dxdt, time_type t, StateOut & out,
                           time_type dt)
    {
        this->stepper().do_step_impl(system, in, dxdt, t, out, dt) ;
    }

    template <class System, class StateInOut, class Err>
    void do_step(System system, StateInOut & x, time_type t, time_type dt,
                 Err & xerr)
    {
        do_step_v5(system, x, t, dt, xerr) ;
    }

    template <class System, class StateInOut, class Err>
    void do_step(System system, const StateInOut & x, time_type t,
                 time_type dt, Err & xerr)
    {
        do_step_v5(system, x, t, dt, xerr) ;
    }


    template <class System, class StateInOut, class DerivIn, class Err>
    typename boost::disable_if<boost::is_same<DerivIn, time_type>, void>::type
    do_step(System system, StateInOut & x, const DerivIn & dxdt,
            time_type t, time_type dt, Err & xerr)
    {
        this->stepper().do_step_impl(system, x, dxdt, t, x, dt, xerr) ;
    }

    template <class System, class StateIn, class StateOut, class Err,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t,
                 StateOut & out, time_type dt, Err & xerr)
    {
        typename std::remove_reference<System>::type & sys = system ;
        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        sys(in, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, in, m_dxdt.m_v, t, out, dt, xerr) ;
    }

    template <class System, class StateIn, class StateOut, class Err,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step(System system, const StateIn & in, time_type t,
                 StateOut & out, time_type dt, Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;

        m_resizer.adjust_size(in, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;
        system_type & sys(system) ;
        deriv_func_type & derFunc(sys.first) ;
        derFunc(in, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, in, m_dxdt.m_v, t, out, dt, xerr) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut, class Err>
    void do_step(System system, const StateIn & in, const DerivIn & dxdt,
                 time_type t, StateOut & out, time_type dt, Err & xerr)
    {
        this->stepper().do_step_impl(system, in, dxdt, t, out, dt, xerr) ;
    }

    template <class StateIn>
    void adjust_size(const StateIn & x)
    {
        resize_impl(x) ;
    }



private:

    template <class System , class StateInOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_v1(System system, StateInOut & x, time_type t, time_type dt)
    {
        typename std::remove_reference<System>::type & sys = system ;
        m_resizer.adjust_size(x, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                       )) ;
        sys(x, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, x, m_dxdt.m_v, t, x, dt) ;
    }

    template <class System , class StateInOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step_v1(System system, StateInOut & x, time_type t, time_type dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;

        m_resizer.adjust_size(x, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                       )) ;
        system_type & sys(system) ;
        deriv_func_type & derFunc(sys.first) ;
        derFunc(x, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, x, m_dxdt.m_v, t, x, dt) ;
    }

    template <class System , class StateInOut , class Err,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_v5(System system, StateInOut & x, time_type t, time_type dt,
                    Err & xerr)
    {
        typename std::remove_reference<System>::type & sys = system ;
        m_resizer.adjust_size(x, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                       )) ;
        sys(x, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, x, m_dxdt.m_v, t, x, dt, xerr) ;
    }

    template <class System , class StateInOut , class Err,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    void do_step_v5(System system, StateInOut & x, time_type t, time_type dt,
                    Err & xerr)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;

        m_resizer.adjust_size(x, boostode::detail::bind(
                    &internal_stepper_base_type::template resize_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                       )) ;
        system_type & sys(system) ;
        deriv_func_type & derFunc(sys.first) ;
        derFunc(x, m_dxdt.m_v, t) ;
        this->stepper().do_step_impl(system, x, m_dxdt.m_v, t, x, dt, xerr) ;
    }

    template< class StateIn >
    bool resize_impl( const StateIn &x )
    {
        return adjust_size_by_resizeability( m_dxdt , x , typename boostode::is_resizeable<deriv_type>::type() );
    }

    stepper_type& stepper( void )
    {
        return *static_cast< stepper_type* >( this );
    }

    const stepper_type& stepper( void ) const
    {
        return *static_cast< const stepper_type* >( this );
    }


    resizer_type m_resizer;

protected:

    wrapped_deriv_type m_dxdt;
};

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_ImplicitErrorStepperBase_hpp
