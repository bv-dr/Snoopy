#ifndef BV_Math_ODEIntegration_Steppers_Details_ImplicitGenericRK_hpp
#define BV_Math_ODEIntegration_Steppers_Details_ImplicitGenericRK_hpp

#include <type_traits>
#include <utility>
#include <array>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/push_back.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/range_c.hpp>
#include <boost/mpl/copy.hpp>
#include <boost/mpl/size_t.hpp>

#include <boost/fusion/algorithm.hpp>
#include <boost/fusion/iterator.hpp>
#include <boost/fusion/mpl.hpp>
#include <boost/fusion/sequence.hpp>

#include <boost/numeric/odeint/algebra/range_algebra.hpp>
#include <boost/numeric/odeint/algebra/default_operations.hpp>
#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/ImplicitStepperBase.hpp"
#include "Math/Solvers/Details.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template <typename State, typename Value, std::size_t StageCount,
          class Enable=void>
struct TypeTraitsIRK ;

template <typename State, typename Value>
struct TypeTraitsIRK<State, Value, 1,
                     typename std::enable_if<
                              std::is_scalar<State>::value
                                            >::type>
{
    typedef State AType ;
    typedef State StagesType ;
    typedef State & SubStagesRefType ;
    typedef const State & ConstSubStagesRefType ;

    static std::size_t size(const State & state)
    {
        return 1 ;
    }

    static void setA(AType & A, std::size_t i, std::size_t j, const Value & val)
    {
        A = val ;
    }

    static const Value & getA(const AType & A, std::size_t i, std::size_t j)
    {
        return A ;
    }

    static StagesType getStagesInitialized(const State & state)
    {
        return state ;
    }

    template <typename Stages>
    static State & getStage(Stages & stages, std::size_t i)
    {
        return stages ;
    }

    template <typename Stages>
    static const State & getStage(const Stages & stages, std::size_t i)
    {
        return stages ;
    }

    template <typename StateIn>
    static void reset(StateIn & t)
    {
        t = static_cast<StateIn>(0) ;
    }

    static StagesType getStagesZero(const std::size_t size)
    {
        return static_cast<StagesType>(0) ;
    }

} ;

template <typename State, typename Value, std::size_t StageCount>
struct TypeTraitsIRK<State, Value, StageCount,
                     typename std::enable_if<
                              std::is_scalar<State>::value
                                            >::type>
{
    typedef Eigen::Matrix<Value, StageCount, StageCount> AType ;
    typedef Eigen::Matrix<Value, StageCount, 1> StagesType ;
    typedef State & SubStagesRefType ;
    typedef const State & ConstSubStagesRefType ;

    static std::size_t size(const State & state)
    {
        return 1 ;
    }

    static void setA(AType & A, std::size_t i, std::size_t j, const Value & val)
    {
        A(i, j) = val ;
    }

    static const Value & getA(const AType & A, std::size_t i, std::size_t j)
    {
        return A(i, j) ;
    }

    static StagesType getStagesInitialized(const State & state)
    {
        StagesType stages ;
        for (std::size_t i=0; i<StageCount; ++i)
        {
            stages(i) = state ;
        }
        return stages ;
    }

    template <typename Stages>
    static State & getStage(Stages & stages, std::size_t i)
    {
        return stages(i) ;
    }

    template <typename Stages>
    static const State & getStage(const Stages & stages, std::size_t i)
    {
        return stages(i) ;
    }

    template <typename StateIn>
    static void reset(StateIn & t)
    {
        t = static_cast<StateIn>(0) ;
    }

    static StagesType getStagesZero(const std::size_t size)
    {
        return StagesType::Zero(StageCount) ;
    }
} ;

template <typename State, typename Value, std::size_t StageCount>
struct TypeTraitsIRK<State, Value, StageCount,
                     typename std::enable_if<
                              ODEIntegration::Details::IsEigen<State, Eigen::EigenBase>::value
                              && State::RowsAtCompileTime != -1 // FIXME cols
                                            >::type>
{
    static constexpr int nStateVals = State::RowsAtCompileTime ;
    typedef Eigen::Matrix<Value, StageCount, StageCount> AType ;
    typedef Eigen::Matrix<Value, StageCount*nStateVals, 1> StagesType ;
    typedef typename Eigen::DenseBase<StagesType>::SegmentReturnType SubStagesRefType ;
    typedef typename Eigen::DenseBase<StagesType>::ConstSegmentReturnType ConstSubStagesRefType ;

    static std::size_t size(const State & state)
    {
        return static_cast<std::size_t>(nStateVals) ;
    }

    static void setA(AType & A, std::size_t i, std::size_t j, const Value & val)
    {
        A(i, j) = val ;
    }

    static const Value & getA(const AType & A, std::size_t i, std::size_t j)
    {
        return A(i, j) ;
    }

    static StagesType getStagesInitialized(const State & state)
    {
        StagesType stages ;
        for (std::size_t i=0; i<StageCount; ++i)
        {
            stages.segment(i*nStateVals, nStateVals) = state ;
        }
        return stages ;
    }

    template <typename Stages>
    static SubStagesRefType getStage(Stages & stages, std::size_t i)
    {
        return stages.segment(i*nStateVals, nStateVals) ;
    }

    template <typename Stages>
    static ConstSubStagesRefType getStage(const Stages & stages, std::size_t i)
    {
        return stages.segment(i*nStateVals, nStateVals) ;
    }

    template <typename StateIn>
    static void reset(StateIn & t)
    {
        t = StateIn::Zero(nStateVals) ;
    }

    static StagesType getStagesZero(const std::size_t size)
    {
        return StagesType::Zero(size) ;
    }
} ;

template <typename State, typename Value, std::size_t StageCount>
struct TypeTraitsIRK<State, Value, StageCount,
                     typename std::enable_if<
                              ODEIntegration::Details::IsEigen<State, Eigen::EigenBase>::value
                              && State::RowsAtCompileTime == -1 // FIXME cols
                                            >::type>
{
    typedef Eigen::Matrix<Value, StageCount, StageCount> AType ;
    typedef Eigen::Matrix<Value, -1, 1> StagesType ;
    typedef typename Eigen::DenseBase<StagesType>::SegmentReturnType SubStagesRefType ;
    typedef typename Eigen::DenseBase<StagesType>::ConstSegmentReturnType ConstSubStagesRefType ;

    static std::size_t size(const State & state)
    {
        return static_cast<std::size_t>(state.size()) ;
    }

    static void setA(AType & A, std::size_t i, std::size_t j, const Value & val)
    {
        A(i, j) = val ;
    }

    static const Value & getA(const AType & A, std::size_t i, std::size_t j)
    {
        return A(i, j) ;
    }

    static StagesType getStagesInitialized(const State & state)
    {
        std::size_t sizeState(size(state)) ;
        std::size_t nVals(sizeState*StageCount) ;
        StagesType stages(nVals) ;
        for (std::size_t i=0; i<StageCount; ++i)
        {
            stages.segment(i*sizeState, sizeState) = state ;
        }
        return stages ;
    }

    template <typename Stages>
    static SubStagesRefType getStage(Stages & stages, std::size_t i)
    {
        std::size_t nVals(stages.size()/StageCount) ;
        return stages.segment(i*nVals, nVals) ;
    }

    template <typename Stages>
    static ConstSubStagesRefType getStage(const Stages & stages, std::size_t i)
    {
        std::size_t nVals(stages.size()/StageCount) ;
        return stages.segment(i*nVals, nVals) ;
    }

    template <typename StateIn>
    static void reset(StateIn & t)
    {
        t = StateIn::Zero(t.size()) ;
    }

    static StagesType getStagesZero(const std::size_t size)
    {
        return StagesType::Zero(size) ;
    }
} ;

template <class State, class DerFunc, class Time,
          class Value, std::size_t StageCount, class CoefCType>
class SolverFunction
{
private:
    typedef TypeTraitsIRK<State, Value, StageCount> Traits ;
    typedef typename Traits::AType AType ;
    typedef typename Traits::StagesType StagesType ;
    StagesType stages_ ;
    DerFunc & func_ ;
    const Time & dt_ ;
    const AType & A_ ;
    const CoefCType & c_ ;
    std::size_t stagesSize_ ;

public:
    SolverFunction(const State & state, DerFunc & func, const Time & dt,
                   const AType & A, const CoefCType & c) :
        stages_(Traits::getStagesInitialized(state)),
        func_(func), dt_(dt), A_(A), c_(c),
        stagesSize_(Traits::size(state)*StageCount)
    {
    }

    const StagesType & getStages(void) const
    {
        return stages_ ;
    }

    template <class StateIn, class Deriv>
    void operator()(const StateIn & state, Deriv & dxdt, const Time & t)
    {
        // Compute the stages
        for (std::size_t i=0; i<StageCount; ++i)
        {
            typename Traits::SubStagesRefType dxdtRef(Traits::getStage(dxdt, i)) ;
            func_(Traits::getStage(state, i), dxdtRef, t+c_[i]*dt_) ;
        }
        // Modify the function
        typename Traits::StagesType sum(Traits::getStagesZero(stagesSize_)) ;
        for (std::size_t i=0; i<StageCount; ++i)
        {
            typename Traits::SubStagesRefType sumStage(Traits::getStage(sum, i)) ;
            for (std::size_t j=0; j<StageCount; ++j)
            {
                sumStage += Traits::getA(A_, i, j) * Traits::getStage(dxdt, j) ;
            }
        }
        dxdt = dt_ * sum ;
        dxdt += stages_ - state ;
    }
} ;

template <typename State, typename Value, typename JFunc,
          std::size_t StageCount, class Enable=void>
struct JTypeTraits ;

template <typename State, typename Value, typename JFunc>
struct JTypeTraits<State, Value, JFunc, 1,
                   typename std::enable_if<
                              std::is_scalar<State>::value
                                          >::type>
{
    typedef State FuncJType ;
    typedef State JType ;
    typedef State IdentityType ;

    template <typename AType, typename JTypeIn, typename AkronJTypeIn>
    static void kroneckerProduct(const AType & A, const JTypeIn & J,
                                 AkronJTypeIn & res)
    {
        res = A * J ; // J is of State type
    }

    template <typename StateIn>
    static void subtractIdentity(StateIn & t)
    {
        t -= static_cast<StateIn>(1) ;
    }

    template <typename StateIn>
    static FuncJType getFuncJInitialized(const JFunc & func, const StateIn & state)
    {
        return static_cast<StateIn>(0) ;
    }

    template <typename StateIn>
    static JType getJInitialized(const JFunc & func, const StateIn & state)
    {
        return static_cast<StateIn>(0) ;
    }
} ;

template <typename State, typename Value, typename JFunc, std::size_t StageCount>
struct JTypeTraits<State, Value, JFunc, StageCount,
                   typename std::enable_if<
                              std::is_scalar<State>::value
                                          >::type>
{
    typedef State FuncJType ;
    typedef Eigen::Matrix<Value, StageCount, StageCount> JType ;
    typedef typename Eigen::DiagonalMatrix<Value, StageCount> IdentityType ;

    template <typename AType, typename JTypeIn, typename AkronJTypeIn>
    static void kroneckerProduct(const AType & A, const JTypeIn & J,
                                 AkronJTypeIn & res)
    {
        res = A * J ;
    }

    template <typename T>
    static void subtractIdentity(T & t)
    {
        IdentityType I(StageCount) ;
        I.setIdentity() ;
        t -= I ;
    }

    template <typename StateIn>
    static FuncJType getFuncJInitialized(const JFunc & func, const StateIn & state)
    {
        return static_cast<StateIn>(0) ;
    }

    template <typename StateIn>
    static JType getJInitialized(const JFunc & func, const StateIn & state)
    {
        return JType::Zero(StageCount, StageCount) ;
    }
} ;

template <typename State, typename Value, typename JFunc, std::size_t StageCount>
struct JTypeTraits<State, Value, JFunc, StageCount,
                   typename std::enable_if<
                            ODEIntegration::Details::IsEigen<State, Eigen::EigenBase>::value
                            && ODEIntegration::Details::IsEigen<typename JFunc::JType, Eigen::DenseBase>::value
                            && State::RowsAtCompileTime != -1 // FIXME cols
                                          >::type>
{
    static constexpr int nStateVals = State::RowsAtCompileTime ;
    typedef typename JFunc::JType FuncJType ;
    typedef Eigen::Matrix<Value, StageCount*nStateVals, StageCount*nStateVals> JType ;
    typedef typename Eigen::DiagonalMatrix<Value, StageCount*nStateVals> IdentityType ;

    template <typename AType, typename JTypeIn, typename AkronJTypeIn>
    static void kroneckerProduct(const AType & A, const JTypeIn & J,
                                 AkronJTypeIn & res)
    {
        for (std::size_t i=0; i<StageCount; ++i)
        {
            for (std::size_t j=0; j<StageCount; ++j)
            {
                res.block(i*nStateVals, j*nStateVals,
                          nStateVals, nStateVals) = A(i, j) * J ;
            }
        }
    }

    template <typename T>
    static void subtractIdentity(T & t)
    {
        IdentityType I(StageCount*nStateVals) ;
        I.setIdentity() ;
        t -=  I ;
    }

    template <typename StateIn>
    static FuncJType getFuncJInitialized(const JFunc & func, const StateIn & state)
    {
        return FuncJType(nStateVals, nStateVals) ;
    }

    template <typename StateIn>
    static JType getJInitialized(const JFunc & func, const StateIn & state)
    {
        return JType::Zero(StageCount*nStateVals, StageCount*nStateVals) ;
    }
} ;

template <typename State, typename Value, typename JFunc, std::size_t StageCount>
struct JTypeTraits<State, Value, JFunc, StageCount,
                   typename std::enable_if<
                            ODEIntegration::Details::IsEigen<State, Eigen::EigenBase>::value
                            && ODEIntegration::Details::IsEigen<typename JFunc::JType, Eigen::SparseMatrixBase>::value
                            && State::RowsAtCompileTime != -1 // FIXME cols
                                          >::type>
{
    static constexpr int nStateVals = State::RowsAtCompileTime ;
    typedef typename JFunc::JType FuncJType ;
    typedef Eigen::SparseMatrix<Value> JType ;
    typedef typename Eigen::DiagonalMatrix<Value, StageCount*nStateVals> IdentityType ;

    template <typename AType, typename JTypeIn, typename AkronJTypeIn>
    static void kroneckerProduct(const AType & A, const JTypeIn & J,
                                 AkronJTypeIn & res)
    {
        // FIXME make sure res is of SparseMatrix type...
        // FIXME make sure AkronJType is column major !!
        for (int k=0; k<res.outerSize(); ++k)
        {
            for (typename JTypeIn::InnerIterator it(res, k); it; ++it)
            {
                int i(it.row()), j(it.col()) ;
                int iSC = i/nStateVals ;
                int jSC = j/nStateVals ;
                int iJ = i - iSC*nStateVals ;
                int jJ = j - jSC*nStateVals ;
                res.coeffRef(i, j) = A(iSC, jSC) * J.coeffRef(iJ, jJ) ;
            }
        }
    }

    template <typename T>
    static void subtractIdentity(T & t)
    {
        IdentityType I(StageCount*nStateVals) ;
        I.setIdentity() ;
        t -=  I ;
    }

    template <typename StateIn>
    static FuncJType getFuncJInitialized(const JFunc & func, const StateIn & state)
    {
        return func.getJInitialized(state) ;
    }

    template <typename StateIn>
    static JType getJInitialized(const JFunc & func, const StateIn & state)
    {
        JType J(StageCount*nStateVals, StageCount*nStateVals) ;
        // FIXME state is probably state in Newton hence StageCount*nStateVals !
        FuncJType FuncJ(func.getJInitialized(state.segment(nStateVals))) ;
        // Get the non zero coefs
        std::vector<std::pair<int, int> > indices ;
        for (int k=0; k<FuncJ.outerSize(); ++k)
        {
            for (typename JType::InnerIterator it(FuncJ, k); it; ++it)
            {
                indices.push_back(std::make_pair(it.row(), it.col())) ;
            }
        }
        // Initialize the correct sparse coefficients to 0 !
        // J here will be the result of the Kronecker product A * J
        typedef Eigen::Triplet<Value> Triplet ;
        std::vector<Triplet> triplets ;
        for (std::size_t i=0; i<StageCount; ++i)
        {
            for (std::size_t j=0; j<StageCount; ++j)
            {
                for (const std::pair<int, int> & inds : indices)
                {
                    triplets.push_back(Triplet(std::get<0>(inds)+i*StageCount,
                                               std::get<1>(inds)+j*StageCount,
                                               FuncJ.coeff(std::get<0>(inds),
                                                           std::get<1>(inds)))) ;
                }
            }
        }
        J.setFromTriplets(triplets.begin(), triplets.end()) ;
        return J ;
    }
} ;

template <typename State, typename Value, typename JFunc, std::size_t StageCount>
struct JTypeTraits<State, Value, JFunc, StageCount,
                   typename std::enable_if<
                            ODEIntegration::Details::IsEigen<State, Eigen::EigenBase>::value
                            && ODEIntegration::Details::IsEigen<typename JFunc::JType, Eigen::DenseBase>::value
                            && State::RowsAtCompileTime == -1 // FIXME cols
                                          >::type>
{
    typedef typename JFunc::JType FuncJType ;
    typedef Eigen::Matrix<Value, -1, -1> JType ;
    typedef typename Eigen::DiagonalMatrix<Value, Eigen::Dynamic> IdentityType ;

    template <typename AType, typename JTypeIn, typename AkronJTypeIn>
    static void kroneckerProduct(const AType & A, const JTypeIn & J,
                                 AkronJTypeIn & res)
    {
        std::size_t nVals(J.rows()) ; // Square matrix
        for (std::size_t i=0; i<StageCount; ++i)
        {
            for (std::size_t j=0; j<StageCount; ++j)
            {
                res.block(i*nVals, j*nVals, nVals, nVals) = A(i, j) * J ;
            }
        }
    }

    template <typename T>
    static void subtractIdentity(T & t)
    {
        IdentityType I(t.rows()) ;
        I.setIdentity() ;
        t -= I ;
    }

    template <typename StateIn>
    static FuncJType getFuncJInitialized(const JFunc & func, const StateIn & state)
    {
        std::size_t nVals(state.size()) ;
        return FuncJType(nVals, nVals) ;
    }

    template <typename StateIn>
    static JType getJInitialized(const JFunc & func, const StateIn & state)
    {
        std::size_t nVals(state.size()) ;
        return JType::Zero(StageCount*nVals, StageCount*nVals) ;
    }

} ;

template <typename State, typename Value, typename JFunc, std::size_t StageCount>
struct JTypeTraits<State, Value, JFunc, StageCount,
                   typename std::enable_if<
                            ODEIntegration::Details::IsEigen<State, Eigen::EigenBase>::value
                            && ODEIntegration::Details::IsEigen<typename JFunc::JType, Eigen::SparseMatrixBase>::value
                            && State::RowsAtCompileTime == -1 // FIXME cols
                                          >::type>
{
    typedef typename JFunc::JType FuncJType ;
    typedef Eigen::SparseMatrix<Value> JType ;
    typedef typename Eigen::DiagonalMatrix<Value, Eigen::Dynamic> IdentityType ;

    template <typename AType, typename JTypeIn, typename AkronJTypeIn>
    static void kroneckerProduct(const AType & A, const JTypeIn & J,
                                 AkronJTypeIn & res)
    {
        // FIXME make sure res is of SparseMatrix type...
        // FIXME make sure AkronJType is column major !!
        int nStateVals(J.rows()) ;
        for (int k=0; k<res.outerSize(); ++k)
        {
            for (typename JTypeIn::InnerIterator it(res, k); it; ++it)
            {
                int i(it.row()), j(it.col()) ;
                int iSC = i/nStateVals ;
                int jSC = j/nStateVals ;
                int iJ = i - iSC*nStateVals ;
                int jJ = j - jSC*nStateVals ;
                it.valueRef() = A(iSC, jSC) * J.coeff(iJ, jJ) ;
            }
        }
    }

    template <typename T>
    static void subtractIdentity(T & t)
    {
        // FIXME below commented doesn't work...
        //IdentityType I(t.rows()) ;
        //I.setIdentity() ;
        //t -= I ;
        // We have to iterate on the coefs
        for (int k=0; k<t.outerSize(); ++k)
        {
            for (typename T::InnerIterator it(t, k); it; ++it)
            {
                if (it.row() == it.col())
                {
                    it.valueRef() -= 1. ;
                }
            }
        }
    }

    template <typename StateIn>
    static FuncJType getFuncJInitialized(const JFunc & func, const StateIn & state)
    {
        return func.getJInitialized(state) ;
    }

    template <typename StateIn>
    static JType getJInitialized(const JFunc & func, const StateIn & state)
    {
        int nStateVals(state.size()/StageCount) ;
        JType J(StageCount*nStateVals, StageCount*nStateVals) ;
        // FIXME state is probably state in Newton hence StageCount*nStateVals !
        FuncJType FuncJ(func.getJInitialized(state.head(nStateVals))) ;
        // Get the non zero coefs
        std::vector<std::pair<int, int> > indices ;
        for (int k=0; k<FuncJ.outerSize(); ++k)
        {
            for (typename JType::InnerIterator it(FuncJ, k); it; ++it)
            {
                indices.push_back(std::make_pair(it.row(), it.col())) ;
            }
        }
        // Initialize the correct sparse coefficients to 0 !
        // J here will be the result of the Kronecker product A * J
        typedef Eigen::Triplet<Value> Triplet ;
        std::vector<Triplet> triplets ;
        for (std::size_t i=0; i<StageCount; ++i)
        {
            for (std::size_t j=0; j<StageCount; ++j)
            {
                for (const std::pair<int, int> & inds : indices)
                {
                    triplets.push_back(Triplet(std::get<0>(inds)+i*StageCount,
                                               std::get<1>(inds)+j*StageCount,
                                               FuncJ.coeff(std::get<0>(inds),
                                                           std::get<1>(inds)))) ;
                }
            }
        }
        J.setFromTriplets(triplets.begin(), triplets.end()) ;
        return J ;
    }

} ;

template <class State, class JFunc, class Time,
          class Value, std::size_t StageCount>
class SolverDFunction
{
public:
    //using FuncJType = typename JFunc::JType ;
    typedef typename JFunc::JType FuncJType ;
    typedef JTypeTraits<State, Value, JFunc, StageCount> JTraits ;
    typedef typename JTraits::JType JType ;

    template <typename StateIn>
    JType getJInitialized(const StateIn & state) const
    {
        return JTraits::getJInitialized(func_, state) ;
    }

private:
    typedef TypeTraitsIRK<State, Value, StageCount> Traits ;
    typedef typename Traits::AType AType ;
    JFunc & func_ ;
    const Time & dt_ ;
    const AType & A_ ;
    FuncJType J_ ;

public:

    SolverDFunction(const State & state, JFunc & func, const Time & dt,
                    const AType & A) :
        func_(func), dt_(dt), A_(A), J_(JTraits::getFuncJInitialized(func, state))
    {
    }

    template <class StateIn, class JTypeIn>
    void operator()(const StateIn & state, JTypeIn & J, const Time & t)
    {
        // FIXME should be called only once per step or this is wrong...
        func_(Traits::getStage(state, 0), J_, t) ;
        JTraits::kroneckerProduct(A_, J_, J) ;
        J *= dt_ ;
        JTraits::subtractIdentity(J) ;
    }
} ;

template<
    std::size_t StageCount,
    std::size_t Order,
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
        >
class implicit_generic_rk :
    public implicit_stepper_base<
               implicit_generic_rk<StageCount, Order, State, Value, Deriv,
                                   Time, Algebra, Operations, Resizer>,
               Order, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                >
{
public :
    typedef implicit_stepper_base<
               implicit_generic_rk<StageCount, Order, State, Value, Deriv, Time,
                                   Algebra, Operations, Resizer>,
               Order, State, Value, Deriv, Time, Algebra, Operations, Resizer
                                 > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type;
    typedef typename stepper_base_type::value_type value_type;
    typedef typename stepper_base_type::deriv_type deriv_type;
    typedef typename stepper_base_type::time_type time_type;
    typedef typename stepper_base_type::algebra_type algebra_type;
    typedef typename stepper_base_type::operations_type operations_type;
    typedef typename stepper_base_type::resizer_type resizer_type;

    typedef typename stepper_base_type::stepper_type stepper_type;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type;

    typedef TypeTraitsIRK<State, value_type, StageCount> Traits ;
    typedef typename Traits::AType AType ;

    typedef boost::mpl::range_c<size_t, 0, StageCount> stage_indices ;

    typedef typename boost::fusion::result_of::as_vector<
                                  typename boost::mpl::copy<
                                      stage_indices , boost::mpl::inserter<
                                                  boost::mpl::vector<>,
                                                  boost::mpl::push_back<
                                            boost::mpl::_1,
                                            std::array<value_type, StageCount>
                                                                       >
                                                                          >
                                                           >::type
                                                        >::type coef_a_type ;

    typedef typename std::array<value_type, StageCount> coef_b_type ;
    typedef typename std::array<value_type, StageCount> coef_c_type ;

    static const size_t stage_count = StageCount ;

    struct InsertA
    {
        AType & A_ ;
        const coef_a_type & a_ ;

        InsertA(AType & A, const coef_a_type & a) : A_(A), a_(a)
        {
        }

        template <typename Index>
        void operator()(Index)
        {
            const std::array<value_type, StageCount> & vals(
                                          boost::fusion::at<Index>(a_)
                                                           ) ;
            for (std::size_t i=0; i<StageCount; ++i)
            {
                Traits::setA(A_, Index::value, i, vals[i]) ;
            }
        }

    } ;

    implicit_generic_rk(const coef_a_type & a,
                        const coef_b_type & b,
                        const coef_c_type & c,
                        const algebra_type &algebra = algebra_type()) :
        stepper_base_type(algebra), b_(b), c_(c)
    {
        typedef boost::mpl::range_c<size_t, 0, StageCount> indices ;
        boost::mpl::for_each<indices>(InsertA(A_, a)) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        //typedef typename std::remove_reference<System>::type system_type ;
        //typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;
        //typedef typename std::remove_reference<typename system_type::second_type>::type jacobi_func_type ;
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;
        typedef typename std::remove_reference<typename system_type::second_type>::type jacobi_func_type ;
        system_type & sys = system ;
        deriv_func_type & deriv_func = sys.first ;
        jacobi_func_type & jacobi_func = sys.second ;

        m_resizer.adjust_size(in, boostode::detail::bind(
                            &stepper_type::template resize_impl<state_type>,
                            boostode::detail::ref(*this), boostode::detail::_1
                                                        )) ;

        // Solve
        typedef SolverFunction<StateIn, deriv_func_type, time_type,
                               value_type, StageCount, coef_c_type> SolverFuncType ;
        typedef SolverDFunction<StateIn, jacobi_func_type, time_type,
                                value_type, StageCount> SolverDFuncType ;
        SolverFuncType func(in, deriv_func, dt, A_, c_) ;
        SolverDFuncType dFunc(in, jacobi_func, dt, A_) ;

        typename Traits::StagesType inStages(func.getStages()) ;
        typename Traits::StagesType resStages(inStages) ;
        this->solver_.solve(std::make_pair(func, dFunc), inStages, resStages, t) ;
        // Finish the RK scheme
        out = in ;
        State tmp(out) ; // To have correct size, initialize with out
        for (std::size_t i=0; i<StageCount; ++i)
        {
            deriv_func(Traits::getStage(resStages, i), tmp, t+c_[i]*dt) ;
            out += dt * b_[i] * tmp ;
        }
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              typename std::enable_if<
                  !ODEIntegration::Details::IsPair<
                        typename ODEIntegration::Details::GetType<System>::type
                                                  >::value, int
                                     >::type=0>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        // Jacobian is evaluated via a finite difference scheme
        // FIXME factorise the type traits and below wrapper
        typedef BV::Math::Solvers::Details::FDSystem<System, StateIn, value_type> FDSys ;
        FDSys fd(system, 1.e-8) ; // FIXME
        do_step_impl(std::make_pair(std::ref(system), std::ref(fd)),
                     in, dxdt, t, out, dt) ;
    }

    template <class StateType>
    void adjust_size(const StateType & x)
    {
        resize_impl(x) ;
        stepper_base_type::adjust_size(x) ;
    }

private:

    template <class StateIn>
    bool resize_impl(const StateIn & x)
    {
        //bool resized = false ;
        // FIXME
        return true ;
    }

    coef_b_type b_ ;
    coef_c_type c_ ;
    AType A_ ;

    resizer_type m_resizer;

} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_ImplicitGenericRK_hpp
