#ifndef BV_Math_ODEIntegration_Steppers_Details_TwoStepsErrorStepperImplicit_hpp
#define BV_Math_ODEIntegration_Steppers_Details_TwoStepsErrorStepperImplicit_hpp

#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/ImplicitErrorStepperBase.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template <
    class Stepper,
    unsigned short StepperOrder
         >
class TwoStepsErrorStepperImplicit : public ImplicitErrorStepperBase<
                                 TwoStepsErrorStepperImplicit<Stepper, StepperOrder>,
                                 StepperOrder, StepperOrder, StepperOrder,
                                 typename Stepper::state_type,
                                 typename Stepper::value_type,
                                 typename Stepper::deriv_type,
                                 typename Stepper::time_type,
                                 typename Stepper::algebra_type,
                                 typename Stepper::operations_type,
                                 typename Stepper::resizer_type
                                                            >
{
public :
    typedef ImplicitErrorStepperBase<
        TwoStepsErrorStepperImplicit<Stepper, StepperOrder>,
        StepperOrder, StepperOrder, StepperOrder,
        typename Stepper::state_type,
        typename Stepper::value_type,
        typename Stepper::deriv_type,
        typename Stepper::time_type,
        typename Stepper::algebra_type,
        typename Stepper::operations_type,
        typename Stepper::resizer_type
                                    > stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;


    TwoStepsErrorStepperImplicit(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(algebra), stepper_(algebra),
        errorScale_(static_cast<value_type>(
                          8.*0.5/(std::pow(2., stepper_.order()) - 1.)
                                           )) // FIXME GSL uses an error safety factor of 8.
    {
    }

    template <class System, class StateIn, class DerivIn, class StateOut,
              class Err>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt, Err &xerr)
    {
        // Perform one large step
        stepper_.do_step(system, in, dxdt, t, yNP1_0_.m_v, dt) ;
        // Perform two half steps
        const time_type dt05(dt * static_cast<time_type>(0.5)) ;
        stepper_.do_step(system, in, dxdt, t, yNP05_.m_v, dt05) ;
        stepper_.do_step(system, yNP05_.m_v, t+dt05, out, dt05) ;
        //error estimate = errorScale_ * (out - yNP1_0_)
        stepper_base_type::m_algebra.for_each3(
                    xerr, out, yNP1_0_.m_v,
                    typename operations_type::template scale_sum2<
                                                      value_type, value_type
                                                                 >(1., -1.)
                                              ) ;
        stepper_base_type::m_algebra.for_each2(
          xerr, xerr,
          typename operations_type::template scale_sum1<value_type>(errorScale_)
                                              ) ;

    }



    template<class System, class StateIn, class DerivIn, class StateOut>
    void do_step_impl(System system, const StateIn & in, const DerivIn & dxdt,
                      time_type t, StateOut & out, time_type dt)
    {
        stepper_.do_step(system, in, dxdt, t, out, dt) ;
    }

    /**
     * \brief Adjust the size of all temporaries in the stepper manually.
     * \param x A state from which the size of the temporaries to be resized is deduced.
     */
    template< class StateIn >
    void adjust_size( const StateIn &x )
    {
        resize_impl( x );
        stepper_base_type::adjust_size( x );
    }

private:

    template< class StateIn >
    bool resize_impl( const StateIn &x )
    {
        bool resized = false;
        resized |= boostode::adjust_size_by_resizeability(yNP1_0_, x, typename boostode::is_resizeable<state_type>::type());
        resized |= boostode::adjust_size_by_resizeability(yNP05_, x, typename boostode::is_resizeable<deriv_type>::type());
        return resized;
    }


    wrapped_state_type yNP1_0_, yNP05_ ;
    resizer_type resizer_ ;
    Stepper stepper_ ;
    value_type errorScale_ ;

};




} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_TwoStepsErrorStepperImplicit_hpp
