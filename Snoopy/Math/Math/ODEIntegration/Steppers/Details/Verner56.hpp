#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner56_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner56_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Verner56_a1 : boost::array<Value, 1>
{
    Verner56_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(18) ;
    }
} ;

template <typename Value=double>
struct Verner56_a2 : boost::array<Value, 2>
{
    Verner56_a2(void)
    {
        (*this)[0] = static_cast<Value>(-1.) / static_cast<Value>(12.) ;
        (*this)[1] = static_cast<Value>(3.) / static_cast<Value>(12.) ;
    }
} ;

template <typename Value=double>
struct Verner56_a3 : boost::array<Value, 3>
{
    Verner56_a3(void)
    {
        (*this)[0] = static_cast<Value>(-2.) / static_cast<Value>(81.) ;
        (*this)[1] = static_cast<Value>(12.) / static_cast<Value>(81.) ;
        (*this)[2] = static_cast<Value>(8.) / static_cast<Value>(81.) ;
    }
} ;

template <typename Value=double>
struct Verner56_a4 : boost::array<Value, 4>
{
    Verner56_a4(void)
    {
        (*this)[0] = static_cast<Value>(40.) / static_cast<Value>(33.) ;
        (*this)[1] = static_cast<Value>(-12.) / static_cast<Value>(33.) ;
        (*this)[2] = static_cast<Value>(-168.) / static_cast<Value>(33.) ;
        (*this)[3] = static_cast<Value>(162.) / static_cast<Value>(33.) ;
    }
} ;

template <typename Value=double>
struct Verner56_a5 : boost::array<Value, 5>
{
    Verner56_a5(void)
    {
        (*this)[0] = static_cast<Value>(-8856.) / static_cast<Value>(1752.) ;
        (*this)[1] = static_cast<Value>(1728.) / static_cast<Value>(1752.) ;
        (*this)[2] = static_cast<Value>(43040.) / static_cast<Value>(1752.) ;
        (*this)[3] = static_cast<Value>(-36855.) / static_cast<Value>(1752.) ;
        (*this)[4] = static_cast<Value>(2695.) / static_cast<Value>(1752.) ;
    }
} ;

template <typename Value=double>
struct Verner56_a6 : boost::array<Value, 6>
{
    Verner56_a6(void)
    {
        (*this)[0] = static_cast<Value>(-8716.) / static_cast<Value>(891.) ;
        (*this)[1] = static_cast<Value>(1968.) / static_cast<Value>(891.) ;
        (*this)[2] = static_cast<Value>(39520.) / static_cast<Value>(891.) ;
        (*this)[3] = static_cast<Value>(-33696.) / static_cast<Value>(891.) ;
        (*this)[4] = static_cast<Value>(1716.) / static_cast<Value>(891.) ;
        (*this)[5] = static_cast<Value>(0.) ;
    }
} ;

template <typename Value=double>
struct Verner56_a7 : boost::array<Value, 7>
{
    Verner56_a7(void)
    {
        (*this)[0] = static_cast<Value>(117585.) / static_cast<Value>(9984.) ;
        (*this)[1] = static_cast<Value>(-22464.) / static_cast<Value>(9984.) ;
        (*this)[2] = static_cast<Value>(-540032.) / static_cast<Value>(9984.) ;
        (*this)[3] = static_cast<Value>(466830.) / static_cast<Value>(9984.) ;
        (*this)[4] = static_cast<Value>(-14014.) / static_cast<Value>(9984.) ;
        (*this)[5] = static_cast<Value>(0.) ;
        (*this)[6] = static_cast<Value>(2079.) / static_cast<Value>(9984.) ;
    }
} ;

// 5th order coefficients
template <typename Value=double>
struct Verner56_b : boost::array<Value, 8>
{
    Verner56_b(void)
    {
        (*this)[0] = static_cast<Value>(210.) / static_cast<Value>(5600.) ;
        (*this)[1] = static_cast<Value>(0.) ;
        (*this)[2] = static_cast<Value>(896.) / static_cast<Value>(5600.) ;
        (*this)[3] = static_cast<Value>(1215.) / static_cast<Value>(5600.) ;
        (*this)[4] = static_cast<Value>(2695.) / static_cast<Value>(5600.) ;
        (*this)[5] = static_cast<Value>(584.) / static_cast<Value>(5600.) ;
        (*this)[6] = static_cast<Value>(0.) ;
        (*this)[7] = static_cast<Value>(0.) ;
    }
} ;

// These values correspond to the difference of the 6th order and 5th order
// coefficients in Butcher tableau
template <typename Value=double>
struct Verner56_db : boost::array<Value, 8>
{
    Verner56_db(void)
    {
        (*this)[0] = static_cast<Value>(15015.) / static_cast<Value>(291200.) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(-118272.) / static_cast<Value>(291200.) ;
        (*this)[3] = static_cast<Value>(115830.) / static_cast<Value>(291200.) ;
        (*this)[4] = static_cast<Value>(-30030.) / static_cast<Value>(291200.) ;
        (*this)[5] = static_cast<Value>(-30368.) / static_cast<Value>(291200.) ;
        (*this)[6] = static_cast<Value>(31185.) / static_cast<Value>(291200.) ;
        (*this)[7] = static_cast<Value>(16640.) / static_cast<Value>(291200.) ;
    }
} ;

template <typename Value=double>
struct Verner56_c : boost::array<Value, 8>
{
    Verner56_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(18) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[3] = static_cast<Value>(2) / static_cast<Value>(9) ;
        (*this)[4] = static_cast<Value>(2) / static_cast<Value>(3) ;
        (*this)[5] = static_cast<Value>(1) ;
        (*this)[6] = static_cast<Value>(8) / static_cast<Value>(9) ;
        (*this)[7] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner56 :
            public boostode::explicit_error_generic_rk<8, 5, 5, 6, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<8, 5, 5, 6, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner56(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner56_a1<Value>(),
                                                     Verner56_a2<Value>(),
                                                     Verner56_a3<Value>(),
                                                     Verner56_a4<Value>(),
                                                     Verner56_a5<Value>(),
                                                     Verner56_a6<Value>(),
                                                     Verner56_a7<Value>()),
                          Verner56_b<Value>(), Verner56_db<Value>(),
                          Verner56_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner56_hpp
