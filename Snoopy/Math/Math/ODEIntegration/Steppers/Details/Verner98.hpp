#ifndef BV_Math_ODEIntegration_Steppers_Details_Verner98_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Verner98_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_error_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

#include "Math/ODEIntegration/Steppers/Details/Verner89.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

// 9th order coefficients
template <typename Value=double>
struct Verner98_b : boost::array<Value, 16>
{
    Verner98_b(void)
    {
        (*this)[0] = static_cast<Value>(23) / static_cast<Value>(525) ;
        (*this)[1] = static_cast<Value>(0) ;
        (*this)[2] = static_cast<Value>(0) ;
        (*this)[3] = static_cast<Value>(0) ;
        (*this)[4] = static_cast<Value>(0) ;
        (*this)[5] = static_cast<Value>(0) ;
        (*this)[6] = static_cast<Value>(0) ;
        (*this)[7] = static_cast<Value>(171) / static_cast<Value>(1400) ;
        (*this)[8] = static_cast<Value>(86) / static_cast<Value>(525) ;
        (*this)[9] = static_cast<Value>(93) / static_cast<Value>(280) ;
        (*this)[10] = static_cast<Value>(-2048) / static_cast<Value>(6825) ;
        (*this)[11] = static_cast<Value>(-3) / static_cast<Value>(18200) ;
        (*this)[12] = static_cast<Value>(39) / static_cast<Value>(175) ;
        (*this)[13] = static_cast<Value>(0) ;
        (*this)[14] = static_cast<Value>(39312) / static_cast<Value>(109200) ;
        (*this)[15] = static_cast<Value>(6058) / static_cast<Value>(109200) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Verner98 :
            public boostode::explicit_error_generic_rk<16, 9, 9, 8, State, Value,
                                                       Deriv, Time, Algebra,
                                                       Operations, Resizer>
{
public:
    typedef boostode::explicit_error_generic_rk<16, 9, 9, 8, State, Value, Deriv,
                                                Time, Algebra, Operations,
                                                Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Verner98(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Verner89_a1<Value>(),
                                                     Verner89_a2<Value>(),
                                                     Verner89_a3<Value>(),
                                                     Verner89_a4<Value>(),
                                                     Verner89_a5<Value>(),
                                                     Verner89_a6<Value>(),
                                                     Verner89_a7<Value>(),
                                                     Verner89_a8<Value>(),
                                                     Verner89_a9<Value>(),
                                                     Verner89_a10<Value>(),
                                                     Verner89_a11<Value>(),
                                                     Verner89_a12<Value>(),
                                                     Verner89_a13<Value>(),
                                                     Verner89_a14<Value>(),
                                                     Verner89_a15<Value>()),
                          Verner98_b<Value>(), Verner89_db<Value>(),
                          Verner89_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Verner98_hpp
