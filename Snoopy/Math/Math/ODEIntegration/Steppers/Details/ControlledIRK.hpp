#ifndef BV_Math_ODEIntegration_Steppers_Details_ControlledIRK_hpp
#define BV_Math_ODEIntegration_Steppers_Details_ControlledIRK_hpp

#include <cmath>

#include <boost/config.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_same.hpp>

#include <boost/numeric/odeint/util/bind.hpp>
#include <boost/numeric/odeint/util/unwrap_reference.hpp>
#include <boost/numeric/odeint/util/copy.hpp>

#include <boost/numeric/odeint/util/state_wrapper.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/detail/less_with_sign.hpp>

#include <boost/numeric/odeint/algebra/range_algebra.hpp>
#include <boost/numeric/odeint/algebra/default_operations.hpp>
#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>

#include <boost/numeric/odeint/stepper/controlled_step_result.hpp>
#include <boost/numeric/odeint/stepper/stepper_categories.hpp>
#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>


#include "Math/ODEIntegration/Details.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

template<
    class ErrorStepper,
    class ErrorChecker=boostode::default_error_checker<typename ErrorStepper::value_type,
                                                       typename ErrorStepper::algebra_type,
                                                       typename ErrorStepper::operations_type>,
    class StepAdjuster=boostode::default_step_adjuster<typename ErrorStepper::value_type,
                                                       typename ErrorStepper::time_type>,
    class Resizer=typename ErrorStepper::resizer_type,
    class ErrorStepperCategory=typename ErrorStepper::stepper_category
        >
class ControlledIRK
{

public:

    typedef ErrorStepper stepper_type ;
    typedef typename stepper_type::state_type state_type ;
    typedef typename stepper_type::value_type value_type ;
    typedef typename stepper_type::deriv_type deriv_type ;
    typedef typename stepper_type::time_type time_type ;
    typedef typename stepper_type::algebra_type algebra_type ;
    typedef typename stepper_type::operations_type operations_type ;
    typedef Resizer resizer_type ;
    typedef ErrorChecker error_checker_type ;
    typedef StepAdjuster step_adjuster_type ;
    typedef boostode::explicit_controlled_stepper_tag stepper_category; //FIXME add new tag ?

    typedef typename stepper_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_type::wrapped_deriv_type wrapped_deriv_type ;

    typedef ControlledIRK<ErrorStepper, ErrorChecker, StepAdjuster,
                          Resizer , boostode::explicit_error_stepper_tag> controlled_stepper_type ;

    ControlledIRK(
            const error_checker_type & error_checker=error_checker_type(),
            const step_adjuster_type & step_adjuster=step_adjuster_type(),
            const stepper_type & stepper=stepper_type()
                 ) :
         m_stepper(stepper), m_error_checker(error_checker),
         m_step_adjuster(step_adjuster)
    {
    }

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return try_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut>
    boostode::controlled_step_result try_step(System system, const StateInOut & x,
                                              time_type & t, time_type & dt)
    {
        return try_step_v1(system, x, t, dt) ;
    }

    template <class System, class StateInOut, class DerivIn>
    boostode::controlled_step_result try_step(System system, StateInOut & x,
                                              const DerivIn & dxdt, time_type & t,
                                              time_type & dt)
    {
        m_xnew_resizer.adjust_size(x, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_xnew_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                            )) ;
        boostode::controlled_step_result res = try_step(system, x, dxdt, t, m_xnew.m_v, dt) ;
        if (res == boostode::success)
        {
            boost::numeric::odeint::copy(m_xnew.m_v, x) ;
        }
        return res ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<StateIn, time_type>, boostode::controlled_step_result >::type
    try_step(System system, const StateIn & in, time_type & t, StateOut & out,
             time_type & dt)
    {
        typename std::remove_reference< System >::type & sys = system ;
        m_dxdt_resizer.adjust_size(in, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_dxdt_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                             )) ;
        sys(in, m_dxdt.m_v, t) ;
        return try_step(system, in, m_dxdt.m_v, t, out, dt) ;
    }

    template <class System, class StateIn, class StateOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    typename boost::disable_if<boost::is_same<StateIn, time_type>, boostode::controlled_step_result >::type
    try_step(System system, const StateIn & in, time_type & t, StateOut & out,
             time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;

        m_dxdt_resizer.adjust_size(in, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_dxdt_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                             )) ;
        system_type & sys(system) ;
        deriv_func_type & derFunc(sys.first) ;
        derFunc(in, m_dxdt.m_v, t) ;
        return try_step(system, in, m_dxdt.m_v, t, out, dt) ;
    }

    template <class System, class StateIn, class DerivIn, class StateOut>
    boostode::controlled_step_result try_step(System system, const StateIn & in,
                                              const DerivIn & dxdt, time_type & t,
                                              StateOut & out, time_type & dt)
    {
        if (!m_step_adjuster.check_step_size_limit(dt))
        {
            // given dt was above step size limit - adjust and return fail;
            dt = m_step_adjuster.get_max_dt() ;
            return boostode::fail ;
        }

        m_xerr_resizer.adjust_size(in, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_xerr_impl<StateIn>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                             )) ;

        // do one step with error calculation
        m_stepper.do_step(system, in, dxdt, t, out, dt, m_xerr.m_v) ;

        value_type max_rel_err = m_error_checker.error(m_stepper.algebra(), in,
                                                       dxdt, m_xerr.m_v, dt) ;

        if (max_rel_err > 1.0)
        {
            // error too big, decrease step size and reject this step
            dt = m_step_adjuster.decrease_step(dt, max_rel_err, m_stepper.error_order()) ;
            return boostode::fail ;
        }
        else
        {
            // otherwise, increase step size and accept
            t += dt;
            dt = m_step_adjuster.increase_step(dt, max_rel_err, m_stepper.stepper_order()) ;
            return boostode::success ;
        }
    }

    /**
     * \brief Adjust the size of all temporaries in the stepper manually.
     * \param x A state from which the size of the temporaries to be resized is deduced.
     */
    template< class StateType >
    void adjust_size( const StateType &x )
    {
        resize_m_xerr_impl( x );
        resize_m_dxdt_impl( x );
        resize_m_xnew_impl( x );
        m_stepper.adjust_size( x );
    }

    /**
     * \brief Returns the instance of the underlying stepper.
     * \returns The instance of the underlying stepper.
     */
    stepper_type& stepper( void )
    {
        return m_stepper;
    }

    /**
     * \brief Returns the instance of the underlying stepper.
     * \returns The instance of the underlying stepper.
     */
    const stepper_type& stepper( void ) const
    {
        return m_stepper;
    }

private:


    template <class System, class StateInOut,
              typename std::enable_if<
                 !ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                 >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step_v1(System system, StateInOut & x,
                                                 time_type & t, time_type & dt)
    {
        typename std::remove_reference<System>::type & sys = system ;
        m_dxdt_resizer.adjust_size(x, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_dxdt_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                            )) ;
        sys(x, m_dxdt.m_v, t) ;
        return try_step(system, x, m_dxdt.m_v, t, dt) ;
    }

    template <class System, class StateInOut,
              typename std::enable_if<
                 ODEIntegration::Details::IsPair<
                     typename ODEIntegration::Details::GetType<System>::type
                                                >::value, int
                                     >::type=0>
    boostode::controlled_step_result try_step_v1(System system, StateInOut & x,
                                                 time_type & t, time_type & dt)
    {
        typedef typename std::remove_reference<System>::type system_type ;
        typedef typename std::remove_reference<typename system_type::first_type>::type deriv_func_type ;

        m_dxdt_resizer.adjust_size(x, boostode::detail::bind(
                    &controlled_stepper_type::template resize_m_dxdt_impl<StateInOut>,
                    boostode::detail::ref(*this), boostode::detail::_1
                                                            )) ;
        system_type & sys(system) ;
        deriv_func_type & derFunc(sys.first) ;
        derFunc(x, m_dxdt.m_v, t) ;
        return try_step(system, x, m_dxdt.m_v, t, dt) ;
    }

    template< class StateIn >
    bool resize_m_xerr_impl( const StateIn &x )
    {
        return adjust_size_by_resizeability( m_xerr , x , typename boostode::is_resizeable<state_type>::type() );
    }

    template< class StateIn >
    bool resize_m_dxdt_impl( const StateIn &x )
    {
        return adjust_size_by_resizeability( m_dxdt , x , typename boostode::is_resizeable<deriv_type>::type() );
    }

    template< class StateIn >
    bool resize_m_xnew_impl( const StateIn &x )
    {
        return adjust_size_by_resizeability( m_xnew , x , typename boostode::is_resizeable<state_type>::type() );
    }

    stepper_type m_stepper;
    error_checker_type m_error_checker;
    step_adjuster_type m_step_adjuster;

    resizer_type m_dxdt_resizer;
    resizer_type m_xerr_resizer;
    resizer_type m_xnew_resizer;

    wrapped_deriv_type m_dxdt;
    wrapped_state_type m_xerr;
    wrapped_state_type m_xnew;
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_ControlledIRK_hpp
