#ifndef BV_Math_ODEIntegration_Steppers_Details_Gill4_hpp
#define BV_Math_ODEIntegration_Steppers_Details_Gill4_hpp

#include <cmath>
#include <boost/array.hpp>
#include <boost/numeric/odeint/stepper/explicit_generic_rk.hpp>
#include <boost/fusion/container/vector.hpp>

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {
namespace Details {

namespace boostode = boost::numeric::odeint ;

// Generic implementation
// FIXME see if it is worth implementing the non-generic one...

template <typename Value=double>
struct Gill4_a1 : boost::array<Value, 1>
{
    Gill4_a1(void)
    {
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(2) ;
    }
} ;

template <typename Value=double>
struct Gill4_a2 : boost::array<Value, 2>
{
    Gill4_a2(void)
    {
        const Value sqrt2(static_cast<Value>(std::sqrt(2.))) ;
        (*this)[0] = static_cast<Value>(sqrt2-1.) / static_cast<Value>(2.) ;
        (*this)[1] = static_cast<Value>(2.-sqrt2) / static_cast<Value>(2.) ;
    }
} ;

template <typename Value=double>
struct Gill4_a3 : boost::array<Value, 3>
{
    Gill4_a3(void)
    {
        const Value sqrt2(static_cast<Value>(std::sqrt(2.))) ;
        (*this)[0] = static_cast<Value>(0.) ;
        (*this)[1] = static_cast<Value>(-1.) / static_cast<Value>(sqrt2) ;
        (*this)[2] = static_cast<Value>(2.+sqrt2) / static_cast<Value>(2.) ;
    }
} ;

template <typename Value=double>
struct Gill4_b : boost::array<Value, 4>
{
    Gill4_b(void)
    {
        const Value sqrt2(static_cast<Value>(std::sqrt(2.))) ;
        (*this)[0] = static_cast<Value>(1) / static_cast<Value>(6) ;
        (*this)[1] = static_cast<Value>(2.-sqrt2) / static_cast<Value>(6.) ;
        (*this)[2] = static_cast<Value>(2.+sqrt2) / static_cast<Value>(6.) ;
        (*this)[3] = static_cast<Value>(1) / static_cast<Value>(6) ;
    }
} ;

template <typename Value=double>
struct Gill4_c : boost::array<Value, 4>
{
    Gill4_c(void)
    {
        (*this)[0] = static_cast<Value>(0) ;
        (*this)[1] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[2] = static_cast<Value>(1) / static_cast<Value>(2) ;
        (*this)[3] = static_cast<Value>(1) ;
    }
} ;

template <
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
class Gill4 :
            public boostode::explicit_generic_rk<4, 4, State, Value, Deriv,
                                                 Time, Algebra, Operations,
                                                 Resizer>
{
public:
    typedef boostode::explicit_generic_rk<4, 4, State, Value, Deriv,
                                          Time, Algebra, Operations,
                                          Resizer> stepper_base_type ;

    typedef typename stepper_base_type::state_type state_type ;
    typedef typename stepper_base_type::value_type value_type ;
    typedef typename stepper_base_type::deriv_type deriv_type ;
    typedef typename stepper_base_type::time_type time_type ;
    typedef typename stepper_base_type::algebra_type algebra_type ;
    typedef typename stepper_base_type::operations_type operations_type ;
    typedef typename stepper_base_type::resizer_type resizer_type ;
    typedef typename stepper_base_type::wrapped_state_type wrapped_state_type ;
    typedef typename stepper_base_type::wrapped_deriv_type wrapped_deriv_type ;
    typedef typename stepper_base_type::stepper_type stepper_type ;

    Gill4(const algebra_type & algebra=algebra_type()) :
        stepper_base_type(boost::fusion::make_vector(Gill4_a1<Value>(),
                                                     Gill4_a2<Value>(),
                                                     Gill4_a3<Value>()),
                          Gill4_b<Value>(),
                          Gill4_c<Value>(), algebra)
    {
    }
} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Details_Gill4_hpp
