#ifndef BV_Math_ODEIntegration_Steppers_EmbeddedRungeKutta_hpp
#define BV_Math_ODEIntegration_Steppers_EmbeddedRungeKutta_hpp

#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_cash_karp54_classic.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_fehlberg78.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg34.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg43.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg45.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg54.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg56.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg65.hpp"
#include "Math/ODEIntegration/Steppers/Details/Fehlberg78.hpp"
#include "Math/ODEIntegration/Steppers/Details/PrinceDormand78.hpp"
#include "Math/ODEIntegration/Steppers/Details/PrinceDormand87.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner56.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner65.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner67.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner76.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner78.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner87.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner89.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner98.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

#define BV_ODEINTEGRATION_DEFINE_RK_STEPPER(Scheme, ClassName)                 \
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer                                                          \
             >                                                                 \
    struct StepperFactory<Scheme,                                              \
                          State, Value, Deriv, Time, Algebra,                  \
                          Operations, Resizer>                                 \
    {                                                                          \
        typedef Details::ExplicitWrapper<                                      \
                   ClassName<State, Value, Deriv, Time, Algebra,               \
                          Operations, Resizer>                                 \
                                        > StepperType ;                        \
    } ;

#define BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(Scheme, ClassName)        \
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer,                                                         \
        class ErrorChecker,                                                    \
        class StepAdjuster                                                     \
             >                                                                 \
    struct AdaptiveStepperFactory<Scheme,                                      \
                                  State, Value, Deriv, Time, Algebra,          \
                                  Operations, Resizer,                         \
                                  ErrorChecker, StepAdjuster>                  \
    {                                                                          \
        typedef Details::ExplicitControlledWrapper<                            \
                     boostode::controlled_runge_kutta<                         \
                             ClassName<State, Value, Deriv, Time, Algebra,     \
                                       Operations, Resizer>,                   \
                             ErrorChecker, StepAdjuster                        \
                                                     >                         \
                                             > StepperType ;                   \
    } ;


namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_CASH_KARP54, boostode::runge_kutta_cash_karp54_classic)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_CASH_KARP54, boostode::runge_kutta_cash_karp54_classic)

BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_DOPRI5, boostode::runge_kutta_dopri5)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_DOPRI5, boostode::runge_kutta_dopri5)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_DOPRI78, Details::PrinceDormand78)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_DOPRI78, Details::PrinceDormand78)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_DOPRI87, Details::PrinceDormand87)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_DOPRI87, Details::PrinceDormand87)

BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG34, Details::Fehlberg34)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG34, Details::Fehlberg34)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG43, Details::Fehlberg43)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG43, Details::Fehlberg43)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG45, Details::Fehlberg45)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG45, Details::Fehlberg45)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG54, Details::Fehlberg54)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG54, Details::Fehlberg54)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG56, Details::Fehlberg56)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG56, Details::Fehlberg56)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG65, Details::Fehlberg65)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG65, Details::Fehlberg65)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG78, Details::Fehlberg78)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG78, Details::Fehlberg78)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_FEHLBERG87, boostode::runge_kutta_fehlberg78)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_FEHLBERG87, boostode::runge_kutta_fehlberg78)

BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER56, Details::Verner56)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER56, Details::Verner56)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER65, Details::Verner65)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER65, Details::Verner65)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER67, Details::Verner67)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER67, Details::Verner67)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER76, Details::Verner76)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER76, Details::Verner76)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER78, Details::Verner78)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER78, Details::Verner78)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER87, Details::Verner87)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER87, Details::Verner87)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER89, Details::Verner89)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER89, Details::Verner89)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::ERK_VERNER98, Details::Verner98)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::ERK_VERNER98, Details::Verner98)

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#undef BV_ODEINTEGRATION_DEFINE_RK_STEPPER
#undef BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER

#endif // BV_Math_ODEIntegration_Steppers_RungeKutta_hpp
