#ifndef BV_Math_ODEIntegration_Steppers_Adams_hpp
#define BV_Math_ODEIntegration_Steppers_Adams_hpp

#include <boost/numeric/odeint/stepper/adams_bashforth.hpp>
#include <boost/numeric/odeint/stepper/adams_moulton.hpp>
#include <boost/numeric/odeint/stepper/adams_bashforth_moulton.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

// FIXME initialisation scheme !

#define BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(Scheme, ClassName, Order)       \
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer                                                          \
             >                                                                 \
    struct StepperFactory<Scheme,                                              \
                          State, Value, Deriv, Time, Algebra,                  \
                          Operations, Resizer>                                 \
    {                                                                          \
        typedef Details::ExplicitWrapper<                                      \
                   ClassName<Order, State, Value, Deriv, Time, Algebra,        \
                             Operations, Resizer>                              \
                                        > StepperType ;                        \
    } ;

#define BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(Scheme, ClassName, Order) \
    template <                                                                    \
        class State,                                                              \
        class Value,                                                              \
        class Deriv,                                                              \
        class Time,                                                               \
        class Algebra,                                                            \
        class Operations,                                                         \
        class Resizer,                                                            \
        class ErrorChecker,                                                       \
        class StepAdjuster                                                        \
             >                                                                    \
    struct AdaptiveStepperFactory<Scheme,                                         \
                                  State, Value, Deriv, Time, Algebra,             \
                                  Operations, Resizer,                            \
                                  ErrorChecker, StepAdjuster>                     \
    {                                                                             \
        typedef Details::ExplicitWrapper<                                         \
                   ClassName<Order, State, Value, Deriv, Time, Algebra,           \
                             Operations, Resizer>                                 \
                                        > StepperType ;                           \
    } ;                                                                           \
    template <>                                                                   \
    struct HasAdaptiveStep<Scheme>                                                \
    {                                                                             \
        static constexpr bool value = false ;                                     \
    } ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_1, boostode::adams_bashforth, 1)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_2, boostode::adams_bashforth, 2)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_3, boostode::adams_bashforth, 3)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_4, boostode::adams_bashforth, 4)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_5, boostode::adams_bashforth, 5)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_6, boostode::adams_bashforth, 6)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_7, boostode::adams_bashforth, 7)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_8, boostode::adams_bashforth, 8)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_1, boostode::adams_bashforth_moulton, 1)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_2, boostode::adams_bashforth_moulton, 2)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_3, boostode::adams_bashforth_moulton, 3)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_4, boostode::adams_bashforth_moulton, 4)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_5, boostode::adams_bashforth_moulton, 5)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_6, boostode::adams_bashforth_moulton, 6)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_7, boostode::adams_bashforth_moulton, 7)
BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_8, boostode::adams_bashforth_moulton, 8)

// FIXME adams steppers are not adaptive but we have to define them
// anyway for the factory to work...
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_1, boostode::adams_bashforth, 1)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_2, boostode::adams_bashforth, 2)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_3, boostode::adams_bashforth, 3)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_4, boostode::adams_bashforth, 4)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_5, boostode::adams_bashforth, 5)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_6, boostode::adams_bashforth, 6)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_7, boostode::adams_bashforth, 7)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_8, boostode::adams_bashforth, 8)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_1, boostode::adams_bashforth_moulton, 1)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_2, boostode::adams_bashforth_moulton, 2)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_3, boostode::adams_bashforth_moulton, 3)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_4, boostode::adams_bashforth_moulton, 4)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_5, boostode::adams_bashforth_moulton, 5)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_6, boostode::adams_bashforth_moulton, 6)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_7, boostode::adams_bashforth_moulton, 7)
BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER(StepperScheme::ADAMS_BASHFORTH_MOULTON_8, boostode::adams_bashforth_moulton, 8)

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#undef BV_ODEINTEGRATION_DEFINE_ADAMS_STEPPER
#undef BV_ODEINTEGRATION_DEFINE_ADAMS_ADAPTIVE_STEPPER

#endif // BV_Math_ODEIntegration_Steppers_Adams_hpp
