#ifndef BV_Math_ODEIntegration_Steppers_RungeKutta_hpp
#define BV_Math_ODEIntegration_Steppers_RungeKutta_hpp

#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>
#include <boost/numeric/odeint/stepper/euler.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta4_classic.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Butcher6.hpp"
#include "Math/ODEIntegration/Steppers/Details/Gill4.hpp"
#include "Math/ODEIntegration/Steppers/Details/Heun.hpp"
#include "Math/ODEIntegration/Steppers/Details/Nystrom5.hpp"
#include "Math/ODEIntegration/Steppers/Details/RK2.hpp"
#include "Math/ODEIntegration/Steppers/Details/RK3.hpp"
#include "Math/ODEIntegration/Steppers/Details/RK3_8.hpp"
#include "Math/ODEIntegration/Steppers/Details/RKRalston2.hpp"
#include "Math/ODEIntegration/Steppers/Details/RKRalston4.hpp"
#include "Math/ODEIntegration/Steppers/Details/TwoStepsErrorStepper.hpp"
#include "Math/ODEIntegration/Steppers/Details/Verner8.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

#define BV_ODEINTEGRATION_DEFINE_RK_STEPPER(Scheme, ClassName)                 \
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer                                                          \
             >                                                                 \
    struct StepperFactory<Scheme,                                              \
                          State, Value, Deriv, Time, Algebra,                  \
                          Operations, Resizer>                                 \
    {                                                                          \
        typedef Details::ExplicitWrapper<                                      \
             ClassName<State, Value, Deriv, Time, Algebra,                     \
                          Operations, Resizer>                                 \
                                        >StepperType ;                         \
    } ;

#define BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(Scheme, ClassName, Order) \
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer,                                                         \
        class ErrorChecker,                                                    \
        class StepAdjuster                                                     \
             >                                                                 \
    struct AdaptiveStepperFactory<Scheme,                                      \
                                  State, Value, Deriv, Time, Algebra,          \
                                  Operations, Resizer,                         \
                                  ErrorChecker, StepAdjuster>                  \
    {                                                                          \
        typedef Details::ExplicitControlledWrapper<                            \
                    boostode::controlled_runge_kutta<                          \
                        Details::TwoStepsErrorStepper<                         \
                             ClassName<State, Value, Deriv, Time, Algebra,     \
                                       Operations, Resizer>, Order             \
                                                     >,                        \
                        ErrorChecker, StepAdjuster                             \
                                                    >                          \
                                                  > StepperType ;              \
    } ;


namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::EULER, boostode::euler)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::EULER, boostode::euler, 1)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::HEUN, Details::Heun)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::HEUN, Details::Heun, 2)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::RK2, Details::RK2)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::RK2, Details::RK2, 2)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::RK_RALSTON_2, Details::RKRalston2)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::RK_RALSTON_2, Details::RKRalston2, 2)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::RK3, Details::RK3)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::RK3, Details::RK3, 3)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::RK4, boostode::runge_kutta4_classic)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::RK4, boostode::runge_kutta4_classic, 4)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::RK_RALSTON_4, Details::RKRalston4)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::RK_RALSTON_4, Details::RKRalston4, 4)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::RK3_8, Details::RK3_8)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::RK3_8, Details::RK3_8, 4)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::GILL4, Details::Gill4)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::GILL4, Details::Gill4, 4)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::NYSTROM5, Details::Nystrom5)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::NYSTROM5, Details::Nystrom5, 5)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::BUTCHER6, Details::Butcher6)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::BUTCHER6, Details::Butcher6, 6)
BV_ODEINTEGRATION_DEFINE_RK_STEPPER(StepperScheme::VERNER8, Details::Verner8)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER(StepperScheme::VERNER8, Details::Verner8, 8)

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#undef BV_ODEINTEGRATION_DEFINE_RK_STEPPER
#undef BV_ODEINTEGRATION_DEFINE_ADAPTIVE_RK_STEPPER

#endif // BV_Math_ODEIntegration_Steppers_RungeKutta_hpp
