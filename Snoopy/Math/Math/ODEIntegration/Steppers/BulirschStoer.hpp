#ifndef BV_Math_ODEIntegration_Steppers_BulirschStoer_hpp
#define BV_Math_ODEIntegration_Steppers_BulirschStoer_hpp

#include <boost/numeric/odeint/stepper/bulirsch_stoer.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer,
    class ErrorChecker,
    class StepAdjuster
         >
struct AdaptiveStepperFactory<StepperScheme::BULIRSCH_STOER,
                              State, Value, Deriv, Time, Algebra,
                              Operations, Resizer,
                              ErrorChecker, StepAdjuster>
{
    typedef Details::ExplicitBSWrapper<
                boost::numeric::odeint::bulirsch_stoer<State, Value, Deriv,
                                                   Time, Algebra, Operations,
                                                   Resizer>
                                      > StepperType ;
} ;

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_BulirschStoer_hpp
