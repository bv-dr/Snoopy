#ifndef BV_Math_ODEIntegration_Steppers_Types_hpp
#define BV_Math_ODEIntegration_Steppers_Types_hpp

#include <iostream>

#define PROCESS_VAL(p) case(p): s = #p ; break ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

// TODO Rosenbrock 4 !

enum StepperScheme {
              EULER,                          // 0
              HEUN,                           // 1
              RK2,                            // 2
              RK_RALSTON_2,                   // 3
              RK3,                            // 4
              RK4,                            // 5
              RK_RALSTON_4,                   // 6
              RK3_8,                          // 7
              GILL4,                          // 8
              NYSTROM5,                       // 9
              BUTCHER6,                       // 10
              VERNER8,                        // 11
              ERK_CASH_KARP54,                // 12
              ERK_DOPRI5,                     // 13
              ERK_DOPRI78,                    // 14
              ERK_DOPRI87,                    // 15
              ERK_FEHLBERG34,                 // 16
              ERK_FEHLBERG43,                 // 17
              ERK_FEHLBERG45,                 // 18
              ERK_FEHLBERG54,                 // 19
              ERK_FEHLBERG56,                 // 20
              ERK_FEHLBERG65,                 // 21
              ERK_FEHLBERG78,                 // 22
              ERK_FEHLBERG87,                 // 23
              ERK_VERNER56,                   // 24
              ERK_VERNER65,                   // 25
              ERK_VERNER67,                   // 26
              ERK_VERNER76,                   // 27
              ERK_VERNER78,                   // 28
              ERK_VERNER87,                   // 29
              ERK_VERNER89,                   // 30
              ERK_VERNER98,                   // 31
              ADAMS_BASHFORTH_1,              // 32
              ADAMS_BASHFORTH_2,              // 33
              ADAMS_BASHFORTH_3,              // 34
              ADAMS_BASHFORTH_4,              // 35
              ADAMS_BASHFORTH_5,              // 36
              ADAMS_BASHFORTH_6,              // 37
              ADAMS_BASHFORTH_7,              // 38
              ADAMS_BASHFORTH_8,              // 39
              ADAMS_BASHFORTH_MOULTON_1,      // 40
              ADAMS_BASHFORTH_MOULTON_2,      // 41
              ADAMS_BASHFORTH_MOULTON_3,      // 42
              ADAMS_BASHFORTH_MOULTON_4,      // 43
              ADAMS_BASHFORTH_MOULTON_5,      // 44
              ADAMS_BASHFORTH_MOULTON_6,      // 45
              ADAMS_BASHFORTH_MOULTON_7,      // 46
              ADAMS_BASHFORTH_MOULTON_8,      // 47
              HPCG,                           // 48
              IMPLICIT_EULER,                 // 49
              IMPLICIT_RK2,                   // 50
              IMPLICIT_RK4,                   // 51
              CRANK_NICOLSON,                 // 52
              GENERALIZED_ALPHA,              // 53
              BULIRSCH_STOER,                 // 54
              GPS1,                           // 55
              GPS2,                           // 56
              GPS4,                           // 57
              GPS6,                           // 58
                   } ;

inline std::ostream & operator<<(std::ostream & out, const StepperScheme value)
{
    const char* s = 0;
    switch(value){
        PROCESS_VAL(EULER) ;
        PROCESS_VAL(HEUN) ;
        PROCESS_VAL(RK2) ;
        PROCESS_VAL(RK_RALSTON_2) ;
        PROCESS_VAL(RK3) ;
        PROCESS_VAL(RK4) ;
        PROCESS_VAL(RK_RALSTON_4) ;
        PROCESS_VAL(RK3_8) ;
        PROCESS_VAL(GILL4) ;
        PROCESS_VAL(NYSTROM5) ;
        PROCESS_VAL(BUTCHER6) ;
        PROCESS_VAL(VERNER8) ;
        PROCESS_VAL(ERK_CASH_KARP54) ;
        PROCESS_VAL(ERK_DOPRI5) ;
        PROCESS_VAL(ERK_DOPRI78) ;
        PROCESS_VAL(ERK_DOPRI87) ;
        PROCESS_VAL(ERK_FEHLBERG34) ;
        PROCESS_VAL(ERK_FEHLBERG43) ;
        PROCESS_VAL(ERK_FEHLBERG45) ;
        PROCESS_VAL(ERK_FEHLBERG54) ;
        PROCESS_VAL(ERK_FEHLBERG56) ;
        PROCESS_VAL(ERK_FEHLBERG65) ;
        PROCESS_VAL(ERK_FEHLBERG78) ;
        PROCESS_VAL(ERK_FEHLBERG87) ;
        PROCESS_VAL(ERK_VERNER56) ;
        PROCESS_VAL(ERK_VERNER65) ;
        PROCESS_VAL(ERK_VERNER67) ;
        PROCESS_VAL(ERK_VERNER76) ;
        PROCESS_VAL(ERK_VERNER78) ;
        PROCESS_VAL(ERK_VERNER87) ;
        PROCESS_VAL(ERK_VERNER89) ;
        PROCESS_VAL(ERK_VERNER98) ;
        PROCESS_VAL(ADAMS_BASHFORTH_1) ;
        PROCESS_VAL(ADAMS_BASHFORTH_2) ;
        PROCESS_VAL(ADAMS_BASHFORTH_3) ;
        PROCESS_VAL(ADAMS_BASHFORTH_4) ;
        PROCESS_VAL(ADAMS_BASHFORTH_5) ;
        PROCESS_VAL(ADAMS_BASHFORTH_6) ;
        PROCESS_VAL(ADAMS_BASHFORTH_7) ;
        PROCESS_VAL(ADAMS_BASHFORTH_8) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_1) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_2) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_3) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_4) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_5) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_6) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_7) ;
        PROCESS_VAL(ADAMS_BASHFORTH_MOULTON_8) ;
        PROCESS_VAL(HPCG) ;
        PROCESS_VAL(IMPLICIT_EULER) ;
        PROCESS_VAL(IMPLICIT_RK2) ;
        PROCESS_VAL(IMPLICIT_RK4) ;
        PROCESS_VAL(CRANK_NICOLSON) ;
        PROCESS_VAL(GENERALIZED_ALPHA) ;
        PROCESS_VAL(BULIRSCH_STOER) ;
        PROCESS_VAL(GPS1) ;
        PROCESS_VAL(GPS2) ;
        PROCESS_VAL(GPS4) ;
        PROCESS_VAL(GPS6) ;
    }

    return out << s;
}

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#undef PROCESS_VAL

#endif // BV_Math_ODEIntegration_Steppers_Types_hpp
