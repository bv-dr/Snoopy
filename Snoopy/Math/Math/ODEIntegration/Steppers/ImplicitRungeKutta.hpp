#ifndef BV_Math_ODEIntegration_Steppers_ImplicitRungeKutta_hpp
#define BV_Math_ODEIntegration_Steppers_ImplicitRungeKutta_hpp

#include <boost/numeric/odeint/stepper/controlled_runge_kutta.hpp>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/ControlledIRK.hpp"
#include "Math/ODEIntegration/Steppers/Details/CrankNicolson.hpp"
#include "Math/ODEIntegration/Steppers/Details/EulerImplicit.hpp"
#include "Math/ODEIntegration/Steppers/Details/RK2Implicit.hpp"
#include "Math/ODEIntegration/Steppers/Details/RK4Implicit.hpp"
#include "Math/ODEIntegration/Steppers/Details/TwoStepsErrorStepperImplicit.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

#define BV_ODEINTEGRATION_DEFINE_IRK_STEPPER(Scheme, ClassName)                \
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer                                                          \
             >                                                                 \
    struct StepperFactory<Scheme,                                              \
                          State, Value, Deriv, Time, Algebra,                  \
                          Operations, Resizer>                                 \
    {                                                                          \
        typedef ClassName<State, Value, Deriv, Time, Algebra,                  \
                          Operations, Resizer> StepperType ;                   \
    } ;

#define BV_ODEINTEGRATION_DEFINE_ADAPTIVE_IRK_STEPPER(Scheme, ClassName, Order)\
    template <                                                                 \
        class State,                                                           \
        class Value,                                                           \
        class Deriv,                                                           \
        class Time,                                                            \
        class Algebra,                                                         \
        class Operations,                                                      \
        class Resizer,                                                         \
        class ErrorChecker,                                                    \
        class StepAdjuster                                                     \
             >                                                                 \
    struct AdaptiveStepperFactory<Scheme,                                      \
                                  State, Value, Deriv, Time, Algebra,          \
                                  Operations, Resizer,                         \
                                  ErrorChecker, StepAdjuster>                  \
    {                                                                          \
        typedef Details::ControlledIRK<                                        \
                    Details::TwoStepsErrorStepperImplicit<                     \
                             ClassName<State, Value, Deriv, Time, Algebra,     \
                                       Operations, Resizer>, Order             \
                                                         >,                    \
                    ErrorChecker, StepAdjuster                                 \
                                                > StepperType ;                \
    } ;


namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

BV_ODEINTEGRATION_DEFINE_IRK_STEPPER(StepperScheme::IMPLICIT_EULER, Details::EulerImplicit)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_IRK_STEPPER(StepperScheme::IMPLICIT_EULER, Details::EulerImplicit, 1)
BV_ODEINTEGRATION_DEFINE_IRK_STEPPER(StepperScheme::IMPLICIT_RK2, Details::RK2Implicit)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_IRK_STEPPER(StepperScheme::IMPLICIT_RK2, Details::RK2Implicit, 2)
BV_ODEINTEGRATION_DEFINE_IRK_STEPPER(StepperScheme::IMPLICIT_RK4, Details::RK4Implicit)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_IRK_STEPPER(StepperScheme::IMPLICIT_RK4, Details::RK4Implicit, 4)
BV_ODEINTEGRATION_DEFINE_IRK_STEPPER(StepperScheme::CRANK_NICOLSON, Details::CrankNicolson)
BV_ODEINTEGRATION_DEFINE_ADAPTIVE_IRK_STEPPER(StepperScheme::CRANK_NICOLSON, Details::CrankNicolson, 2)

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#undef BV_ODEINTEGRATION_DEFINE_IRK_STEPPER
#undef BV_ODEINTEGRATION_DEFINE_ADAPTIVE_IRK_STEPPER

#endif // BV_Math_ODEIntegration_Steppers_ImplicitRungeKutta_hpp
