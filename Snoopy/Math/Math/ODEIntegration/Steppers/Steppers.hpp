#ifndef BV_Math_ODEIntegration_Steppers_Steppers_hpp
#define BV_Math_ODEIntegration_Steppers_Steppers_hpp

#include <boost/numeric/odeint/algebra/algebra_dispatcher.hpp>
#include <boost/numeric/odeint/algebra/operations_dispatcher.hpp>
#include <boost/numeric/odeint/util/resizer.hpp>
#include <boost/numeric/odeint/util/is_resizeable.hpp>
#include <tuple>
#include <type_traits>

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Adams.hpp"
#include "Math/ODEIntegration/Steppers/BulirschStoer.hpp"
#include "Math/ODEIntegration/Steppers/EmbeddedRungeKutta.hpp"
#include "Math/ODEIntegration/Steppers/GPS.hpp"
#include "Math/ODEIntegration/Steppers/HPCG.hpp"
#include "Math/ODEIntegration/Steppers/ImplicitRungeKutta.hpp"
#include "Math/ODEIntegration/Steppers/RungeKutta.hpp"
#include "Math/ODEIntegration/Steppers/GeneralizedAlpha.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

namespace boostode = boost::numeric::odeint ;

template <
    int Scheme,
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer
         >
struct Stepper
{
    typedef StepperFactory<Scheme, State, Value, Deriv, Time, Algebra,
                        Operations, Resizer> Factory ;
    typedef typename Factory::StepperType Type ;

    static Type get(void)
    {
        return Type() ;
    }

    static Type get(const ODEIntegrationParameters<Value, Time> & params)
    {
        return Type() ;
    }
} ;

template <
    int Scheme,
    class State,
    class Value=double,
    class Deriv=State,
    class Time=Value,
    class Algebra=typename boostode::algebra_dispatcher<State>::algebra_type,
    class Operations=typename boostode::operations_dispatcher<State>::operations_type,
    class Resizer=boostode::initially_resizer,
    class ErrorChecker=boostode::default_error_checker<Value, Algebra, Operations>,
    class StepAdjuster=boostode::default_step_adjuster<Value, Time>,
    class Enable=void
         >
struct AdaptiveStepper
{
    typedef AdaptiveStepperFactory<Scheme, State, Value, Deriv, Time, Algebra,
                                   Operations, Resizer,
                                   ErrorChecker, StepAdjuster> Factory ;
    typedef typename Factory::StepperType Type ;

    static Type get(Value aTol=1.e-6, Value rTol=1.e-6,
                    Value scaleYError=1., Value scaleDYError=0.,
                    Time maxStepSize=200)
    {
        return Type(typename Type::error_checker_type(aTol, rTol, scaleYError,
                                                      scaleDYError),
                    typename Type::step_adjuster_type(maxStepSize)) ;
    }

    static Type get(const ODEIntegrationParameters<Value, Time> & params)
    {
        return Type(typename Type::error_checker_type(params.getATol(),
                                                      params.getRTol(),
                                                      params.getScaleYError(),
                                                      params.getScaleDYError()),
                    typename Type::step_adjuster_type(params.getMaxStepSize())) ;
    }

} ;

template <int Scheme, class State, class Value, class Deriv, class Time,
          class Algebra, class Operations, class Resizer,
          class ErrorChecker, class StepAdjuster>
struct AdaptiveStepper<Scheme, State, Value, Deriv, Time, Algebra,
                       Operations, Resizer, ErrorChecker, StepAdjuster,
                       typename std::enable_if<!HasAdaptiveStep<Scheme>::value>::type>
{
    typedef AdaptiveStepperFactory<Scheme, State, Value, Deriv, Time, Algebra,
                                   Operations, Resizer,
                                   ErrorChecker, StepAdjuster> Factory ;
    typedef typename Factory::StepperType Type ;

    static Type get(void)
    {
        return Type() ;
    }

    static Type get(const ODEIntegrationParameters<Value, Time> & params)
    {
        return Type() ;
    }

} ;


} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Steppers_Steppers_hpp
