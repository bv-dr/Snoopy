#pragma once


#include "Math/ODEIntegration/Steppers/Details/GeneralizedAlpha.hpp"

#include "Math/ODEIntegration/Details.hpp"
#include "Math/ODEIntegration/Steppers/Details/Wrapper.hpp"
#include "Math/ODEIntegration/Steppers/Details/ControlledNLImplicitStepper.hpp"
#include "Math/ODEIntegration/Steppers/StepperFactory.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace boostode = boost::numeric::odeint ;

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Steppers {

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer
         >
struct StepperFactory<StepperScheme::GENERALIZED_ALPHA,
                      State, Value, Deriv, Time, Algebra,
                      Operations, Resizer>
{
    typedef Details::GeneralizedAlpha<State, Value, Deriv, Time, Algebra,
                                      Operations, Resizer> StepperType ;
} ;

template <
    class State,
    class Value,
    class Deriv,
    class Time,
    class Algebra,
    class Operations,
    class Resizer,
    class ErrorChecker,
    class StepAdjuster
         >
struct AdaptiveStepperFactory<StepperScheme::GENERALIZED_ALPHA,
                              State, Value, Deriv, Time, Algebra,
                              Operations, Resizer,
                              ErrorChecker, StepAdjuster>
{
    typedef Details::ControlledNLImplicitStepper<
                   Details::GeneralizedAlpha<State, Value, Deriv, Time, Algebra,
                                             Operations, Resizer>
                                                > StepperType ;

} ;

} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV




