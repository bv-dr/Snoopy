#pragma once

/**
 * PURPOSE:
 *
 *  Polynomial Regression aims to fit a non-linear relationship to a set of
 *  points. It approximates this by solving a series of linear equations using
 *  a least-squares approach.
 *
 *  We can model the expected value y as an nth degree polynomial, yielding
 *  the general polynomial regression model:
 *
 *  y = a0 + a1 * x + a2 * x^2 + ... + an * x^n
 * @author Chris Engelsma
 */

#include <Eigen/Dense>

#include "Tools/BVException.hpp"

namespace BV {
namespace Math {

Eigen::VectorXd PolynomialRegression(
    const Eigen::Ref<const Eigen::VectorXd> & x,
    const Eigen::Ref<const Eigen::VectorXd> & y, size_t order)
{
    const size_t N = x.cols() ;
    if (x.cols() != y.cols())
    {
        throw BV::Tools::Exceptions::BVException(
            "x and y must have the same size") ;
    }
    if (N == 0 || y.cols() == 0)
    {
        throw BV::Tools::Exceptions::BVException(
            "x and y sizes cannot be null") ;
    }
    int n = order ;
    int np1 = n + 1 ;
    int np2 = n + 2 ;
    int tnp1 = 2 * n + 1 ;
    double tmp ;
    // X = vector that stores values of sigma(xi^2n)
    Eigen::VectorXd X(tnp1) ;
    for (int i = 0; i < tnp1; ++i)
    {
        X(i) = 0 ;
        for (size_t j = 0; j < N; ++j)
        {
            X(i) += std::pow(x(j), i) ;
        }
    }
    // a = vector to store final coefficients.
    Eigen::VectorXd a(np1) ;

    // B = normal augmented matrix that stores the equations.
    Eigen::MatrixXd B(np1, np2) ;

    for (int i = 0; i <= n; ++i)
        for (int j = 0; j <= n; ++j)
            B(i, j) = X(i + j) ;

    // Y = vector to store values of sigma(xi^n * yi)
    Eigen::VectorXd Y(np1) ;
    for (int i = 0; i < np1; ++i)
    {
        Y(i) = 0. ;
        for (size_t j = 0; j < N; ++j)
        {
            Y(i) += std::pow(x(j), i) * y(j) ;
        }
    }

    // Load values of Y as last column of B
    for (int i = 0; i <= n; ++i)
        B(i, np1) = Y(i) ;

    n += 1 ;
    int nm1 = n - 1 ;

    // Pivotisation of the B matrix.
    for (int i = 0; i < n; ++i)
        for (int k = i + 1; k < n; ++k)
            if (B(i, i) < B(k, i))
            {
                for (int j = 0; j <= n; ++j)
                {
                    tmp = B(i, j) ;
                    B(i, j) = B(k, j) ;
                    B(k, j) = tmp ;
                }
            }

    // Performs the Gaussian elimination.
    // (1) Make all elements below the pivot equals to zero
    //     or eliminate the variable.
    for (int i = 0; i < nm1; ++i)
    {
        for (int k = i + 1; k < n; ++k)
        {
            double t = B(k, i) / B(i, i) ;
            for (int j = 0; j <= n; ++j)
            {
                B(k, j) -= t * B(i, j) ;         // (1)
            }
        }
    }
    // Back substitution.
    // (1) Set the variable as the rhs of last equation
    // (2) Subtract all lhs values except the target coefficient.
    // (3) Divide rhs by coefficient of variable being calculated.
    for (int i = nm1; i >= 0; --i)
    {
        a(i) = B(i, n) ;                  // (1)
        for (int j = 0; j < n; ++j)
        {
            if (j != i)
            {
                a(i) -= B(i, j) * a(j) ;  // (2)
            }
        }
        a(i) /= B(i, i) ;                 // (3)
    }
    return a ;
}
} // End of namespace Math
} // End of namespace BV
