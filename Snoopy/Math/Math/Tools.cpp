#include "Math/Tools.hpp"

#include <math.h>


#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

namespace BV {
namespace Math {

double Degrees(const double & value)
{
    return value * 180. / M_PI ;
}

void Degrees(Eigen::Ref<Eigen::ArrayXd> angles)
{
    angles *= 180./M_PI ;
}

double Radians(const double & value)
{
    return value * M_PI / 180. ;
}

void Radians(Eigen::Ref<Eigen::ArrayXd> angles)
{
    angles *= M_PI/180. ;
}

bool IsClose(const double & value, const double & reference,
             const double & epsilon)
{
    return std::abs(value - reference) < epsilon ;
}

bool IsNull(const double & value, const double & epsilon)
{
    return (value < epsilon) && (value > -epsilon) ;
}

double Sign(const double & x)
{
    if (x >= 0.)
    {
        return 1. ;
    }
    if (x < 0.)
    {
        return -1. ;
    }
    return 0. ;
}

//void ConvertMoment3D(BV::Tools::Vector6d & load,
//                     const BV::Geometry::Point & applicationPoint,
//                     const BV::Geometry::Point & targetPoint)
//{
//    double x0(applicationPoint.x()), y0(applicationPoint.y()), z0(
//        applicationPoint.z()) ;
//    double x1(targetPoint.x()), y1(targetPoint.y()), z1(targetPoint.z()) ;
//    load(3) += (y0 - y1) * load(2) - (z0 - z1) * load(1) ;
//    load(4) += (z0 - z1) * load(0) - (x0 - x1) * load(2) ;
//    load(5) += (x0 - x1) * load(1) - (y0 - y1) * load(0) ;
//}

double WrapAngle0_2PI(const double & angle)
{
    double twoPi(2.0 * M_PI) ;
    return angle - twoPi * floor(angle / twoPi) ;
}

//In place version
void WrapAngle0_2PI(Eigen::Ref<Eigen::ArrayXd> angles)
{
    using IndexType = Eigen::ArrayXd::Index ;
    const IndexType nAngles(angles.size()) ;
    for (IndexType i=0; i< nAngles; ++i)
    {
        angles(i) = WrapAngle0_2PI(angles(i)) ;
    }
}


Eigen::ArrayXd getWrapAngle0_2PI(const Eigen::Ref<Eigen::ArrayXd> & angles)
{
    double twoPi(2.0 * M_PI);
    Eigen::ArrayXd wrappedAngles = Eigen::ArrayXd( angles.size() );
    for (auto i = 0; i < angles.size(); ++i)
    {
        wrappedAngles(i) = angles(i) - twoPi * floor(angles(i) / twoPi);
    }
    return wrappedAngles;
}

double WrapAngleMPI_PI(const double & angle)
{
    double twoPi(2.0 * M_PI) ;
    double angle0_2PI(angle - twoPi * floor(angle / twoPi)) ;
    if (angle0_2PI > M_PI)
    {
        return angle0_2PI - twoPi ;
    }
    return angle0_2PI ;
}

Eigen::MatrixXd::Index GetRank(const Eigen::MatrixXd & mat, const double & epsilon)
{
    Eigen::FullPivLU<Eigen::MatrixXd> lu(mat) ;
    lu.setThreshold(epsilon) ;
    return lu.rank() ;
}

} // End of namespace Math
} // End of namespace BV
