#include "movmax.hpp"
#include <deque>
#include <vector>

//template<typename T>
//std::vector<T> BV::Math::movmax(std::vector<T> data, int k)
Eigen::ArrayXd BV::Math::movmax(Eigen::ArrayXd data, int k, int location)
{
    int ioffset;
    if (location < 0 )
    {
        ioffset = 0;
    }
    else if (location == 0)
    {
        ioffset = std::ceil(k / 2);
    }
    else
    {
        ioffset = k;
    }
        
    int n = data.size();
    Eigen::ArrayXd B(n);
    std::deque<int> Q;

    for (int i = 0; i < k; i++) {
        while (!Q.empty() && data[i] >= data[Q.back()])
            Q.pop_back();
        Q.push_back(i);
    }
    for (int i = k; i < n; i++) {
        B[i - k + ioffset] = data[Q.front()];
        while (!Q.empty() && data[i] >= data[Q.back()])
            Q.pop_back();
        while (!Q.empty() && Q.front() <= i - k)
            Q.pop_front();
        Q.push_back(i);
    }
    B[n - k + ioffset] = data[Q.front()];


    // handle bounds.
    for (int i = 0; i < ioffset; i++) 
    {
        B[i] = data.segment(0, ioffset+i).maxCoeff();
    }
    for (int i = n - k + 1 + ioffset; i < n; i++)
    {
        B[i] = data.segment(i-k+ioffset, n-i+k-ioffset).maxCoeff();
    }
    return B;
};

//template std::vector<double> BV::Math::movmax(std::vector<double> , int);
//template std::vector<int> BV::Math::movmax(std::vector<int>, int);