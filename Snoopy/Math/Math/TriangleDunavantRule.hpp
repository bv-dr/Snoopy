#ifndef BV_Math_TriangleDunavantRule_hpp
#define BV_Math_TriangleDunavantRule_hpp

#include "MathToolsExport.hpp"

namespace BV {
namespace Math {

MATHTOOLS_API int dunavant_degree(int rule) ;
MATHTOOLS_API int dunavant_order_num(int rule) ;
MATHTOOLS_API void dunavant_rule(int rule, int order_num, double xy[],
                                 double w[]) ;
MATHTOOLS_API int dunavant_rule_num() ;
MATHTOOLS_API int *dunavant_suborder(int rule, int suborder_num) ;
MATHTOOLS_API int dunavant_suborder_num(int rule) ;
MATHTOOLS_API void dunavant_subrule(int rule, int suborder_num,
                                    double suborder_xyz[],
                                    double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_01(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_02(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_03(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_04(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_05(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_06(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_07(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_08(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_09(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_10(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_11(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_12(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_13(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_14(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_15(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_16(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_17(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_18(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_19(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API void dunavant_subrule_20(int suborder_num, double suborder_xyz[],
                                      double suborder_w[]) ;
MATHTOOLS_API int i4_max(int i1, int i2) ;
MATHTOOLS_API int i4_min(int i1, int i2) ;
MATHTOOLS_API int i4_modp(int i, int j) ;
MATHTOOLS_API int i4_wrap(int ival, int ilo, int ihi) ;
MATHTOOLS_API double r8_huge() ;
MATHTOOLS_API int r8_nint(double x) ;
MATHTOOLS_API void reference_to_physical_t3(double t[], int n, double ref[],
                                            double phy[]) ;
MATHTOOLS_API int s_len_trim(char *s) ;
MATHTOOLS_API void timestamp() ;
MATHTOOLS_API double triangle_area(double t[2 * 3]) ;

}
}
#endif