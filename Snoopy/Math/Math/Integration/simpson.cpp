#include "simpson.hpp"



double BV::Math::Integration::simps_odd_(const Eigen::ArrayXd& y, const Eigen::ArrayXd& x)
{
    if (y.size() % 2 != 0)
    {
        // Account for possibly different spacings.
        //auto h = x.segment(1, n - 1) - x.segment(0, n - 1);
    }
    else
    {
        throw "Size should be odd";
    }

    return 0.;
}

double BV::Math::Integration::simps(const Eigen::ArrayXd& y, const Eigen::ArrayXd& x)
{
    if (y.size() % 2 != 0)
    {
        return simps_odd_(y, x);
    }
    else
    {
        // Handle the even case (average firstSpacing / lastSpacing)
        throw "Not implemented yet"; // FIXME
    }

    return 0.;
}

double BV::Math::Integration::trapz(const Eigen::ArrayXd& y, const Eigen::ArrayXd& x)
{
	auto n = y.size();
	auto vect = (y.segment(1, n - 1) + y.segment(0, n - 1)) * (x.segment(1, n - 1) - x.segment(0, n - 1));
	return 0.5*vect.sum() ;
}

double BV::Math::Integration::trapz(const Eigen::Ref<const Eigen::ArrayXd> &f, const double &dx)
{
    /*
     * Integration: trapezoid method
     * for N points from 0 to N-1 we have N-2 intervals
     * \int_a^b f(x) = \sum_{i=0}^{N-2} (f(x_i) +f(x_{i+1}))/2 dx =
     *               = \sum_{i=0}^{N-2} f(x_i) /2 dx +\sum_{i=0}^{N-2} f(x_{i+1}) /2 dx =
     *               = (1/2 f(x_0) +\sum_{i=1}^{N-2} f(x_i)/2 +\sum_{i=1}^{N-2} f(x_i)/2 +1/2 f(x_{N-1}))dx =
     *               = [1/2 f(x_0)                      (1)
     *                 +\sum_{i=1}^{N-2} f(x_i)         (2)
     *                 +1/2 f(x_{N-1})]dx               (3)
     */
    double res(0.0);
    size_t N(f.size());
    res += 0.5 *f(0);
    for(size_t i = 1; i < N -1; ++i)
        res += f(i);
    res += 0.5 *f(N-1);
    return res *dx;
}

double BV::Math::Integration::trapz(const Eigen::Ref<const Eigen::ArrayXXd> &f_xy, const double &dx, const double &dy)
{
    /*
     * Integration: trapezoid method
     * for N points from 0 to N-1 we have N-2 intervals
     * \int_a^b f(x) = (1/2 f(x_0) +\sum_{i=1}^{N-2} f(x_i) +1/2 f(x_{N-1}))dx
     * \int_a^b\int_c^d f(x,y) dx dy =
     *               = \int_a^b [1/2 f(x_0, y) +\sum_{i=1}^{N-2} f(x_i, y) + 1/2 f(x_{N-1}, y) ] dx dy =
     *               = [1/4 f(x_0, y_0)                                     (1)
     *                 +1/2 \sum_{i=1}^{N-2} f(x_i, y_0)                    (2)
     *                 +1/4 f(x_{N-1}, y_0)                                 (3)
     *                 +1/2 \sum_{j=1}^{M-2} f(x_0, y_j)                    (4)
     *                 +\sum_{i=1}^{N-2}\sum_{j=1}^{M-2} f(x_i, y_j)        (5)
     *                 +1/2 \sum_{j=1}^{M-2} f(x_{N-1}, y_j)                (6)
     *                 +1/4 f(x_0, y_{M-1})                                 (7)
     *                 +1/2 \sum_{i=1}^{N-2} f(x_i, y_{M-1})                (8)
     *                 +1/4 f(x_{N-1}, y_{M-1})] dx dy                      (9)
     */
    size_t i(0);
    size_t j(0);
    size_t N(f_xy.rows());
    size_t M(f_xy.cols());
    double res(0.0);
    res += 0.25 *f_xy(0, 0);                            // (1)
    for (j = 1; j < M -1; ++j)                          // (4)
        res += 0.5 *f_xy(0, j);
    res += 0.25 *f_xy(0, M -1);                         // (7)
    for (i = 1; i < N -1; ++i) {
        res += 0.5 *f_xy(i, 0);                         // (2)
        for(j = 1; j < M -1; ++j)                       // (5)
            res += f_xy(i, j);
        res += 0.5 *f_xy(i, M-1);                       // (8)
    }
    res += 0.25 *f_xy(N-1, 0);                          // (3)
    for (j = 1; j < M -1; ++j)                          // (6)
        res += 0.5 *f_xy(N-1, j);
    res += 0.25 *f_xy(N-1, M-1);                        // (9)

    return res *dx *dy;
}
