#pragma once
#include <math.h>
#include "IntegrationExport.hpp"
#include <Eigen/Dense>


namespace BV
{
    namespace Math
    {
        namespace Integration
        {

            
            INTEGRATION_API double simps(const Eigen::ArrayXd& y, const Eigen::ArrayXd& x);

            INTEGRATION_API double trapz(const Eigen::ArrayXd& y, const Eigen::ArrayXd& x);

            INTEGRATION_API double simps_odd_(const Eigen::ArrayXd& y, const Eigen::ArrayXd& x);

            /**
             * trapezoidal method for one variable function with equally distributed points
             * @param[in] f   : 1D array of $f(x_i)$
             * @param[in] dx  : $\Delta x$
             * @return $\int f(x) dx$
             */
            INTEGRATION_API double trapz(const Eigen::Ref<const Eigen::ArrayXd> &f, const double &dx);
            /**
             * trapezoidal method for two variable function with equally distributed points
             * @param[in] f_xy  : 2D array of $f(x_i,y_j)$
             * @param[in] dx    : $\Delta x$
             * @param[in] dy    : $\Delta y$
             * @return $\int\int f(x,y) dx dy$
             */
            INTEGRATION_API double trapz(const Eigen::Ref<const Eigen::ArrayXXd> &f_xy, const double &dx, const double &dy);

        }
    }
}
