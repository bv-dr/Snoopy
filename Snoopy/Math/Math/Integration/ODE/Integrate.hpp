#pragma once

#include "IntegrationExport.hpp"

#include "Tools/BVException.hpp"
#include "Math/Integration/ODE/Integrable.hpp"
#include "Math/Integration/ODE/Steppers/StepperABC.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {

struct IntegratorStopException : public BV::Tools::Exceptions::BVException
{
    IntegratorStopException(std::string message) : BVException(message)
    {
    }
} ;

std::shared_ptr<Steppers::StepperABC> INTEGRATION_API GetStepper(
                                      const Steppers::StepperType stepperType,
                                      IntegrableABC * integrable=nullptr
                                                                ) ;

void INTEGRATION_API Integrate(const Steppers::StepperType stepperType,
                               IntegrableABC & integrable,
                               double xStart, double xEnd, double xStep,
                               Steppers::ObserverABC & observer,
                               bool updateStateEachStep=false) ;

void INTEGRATION_API Integrate(std::shared_ptr<Steppers::StepperABC> p_stepper,
                               double xStart, double xEnd, double xStep,
                               Steppers::ObserverABC & observer,
                               bool updateStateEachStep=false) ;


} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV

