#include "Math/Integration/ODE/Integrable.hpp"
#include "Math/Tools.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {

std::size_t IntegrableABC::getNStateDerivative() const
{
    return getNState() ;
}

void IntegrableABC::setupState(const double t,
                               Eigen::Ref<Eigen::VectorXd> y,
                               Eigen::Ref<Eigen::VectorXd> dy)
{
}

void IntegrableABC::setRotatorsStateIndices(StateIndexer & StateIndexer,
                                            std::size_t iStartState,
                                            std::size_t iStartStateDer)
{
}

void IntegrableABC::setRotatorsPosVelIndices(StateIndexer & StateIndexer,
                                             std::size_t iStartPos,
                                             std::size_t iStartVel)
{
}

void IntegrableABC::provideState(const double t, Eigen::Ref<Eigen::VectorXd> y)
{
}

void IntegrableABC::provideStateDerivative(const double t,
                                          Eigen::Ref<Eigen::VectorXd> dy)
{
}

void IntegrableABC::updateState(const double t,
                                const Eigen::Ref<const Eigen::VectorXd> & y)
{
}

void IntegrableABC::updateStateDerivative(const double t,
                                          const Eigen::Ref<const Eigen::VectorXd> & dy)
{
}

std::size_t IntegrableABC::getNStatePos() const
{
    throw IntegrableException("Second order system function not implemented: getNStateX") ;
}

std::size_t IntegrableABC::getNStateVel() const
{
    return getNStatePos() ;
}

std::size_t IntegrableABC::getNStateAcc() const
{
    return getNStatePos() ;
}

void IntegrableABC::setupState(const double t,
                               Eigen::Ref<Eigen::VectorXd> x,
                               Eigen::Ref<Eigen::VectorXd> v,
                               Eigen::Ref<Eigen::VectorXd> a)
{
}

void IntegrableABC::provideState(const double t,
                                Eigen::Ref<Eigen::VectorXd> x,
                                Eigen::Ref<Eigen::VectorXd> v,
                                Eigen::Ref<Eigen::VectorXd> a)
{
    throw IntegrableException("Second order system function not implemented: provideStatePos") ;
}

void IntegrableABC::updateState(const double t,
                                const Eigen::Ref<const Eigen::VectorXd> & x,
                                const Eigen::Ref<const Eigen::VectorXd> & v,
                                const Eigen::Ref<const Eigen::VectorXd> & a)
{
}


void IntegrableABC::computeDynamicEquationComponents(const double t,
                                                     const Eigen::Ref<const Eigen::VectorXd> & x,
                                                     const Eigen::Ref<const Eigen::VectorXd> & v,
                                                     const Eigen::Ref<const Eigen::VectorXd> & a,
                                                     Eigen::Ref<Eigen::VectorXd> FInt,
                                                     Eigen::Ref<Eigen::VectorXd> FExt,
                                                     Eigen::Ref<Eigen::VectorXd> FDyn,
                                                     Eigen::Ref<Eigen::MatrixXd> M,
                                                     bool computeFInt,
                                                     bool computeFExt,
                                                     bool computeFDyn)
{
    throw IntegrableException("Second order system function not implemented: computeDynamicEquationComponents") ;
}

void IntegrableABC::computeTangentMatrices(const double t,
                                           const Eigen::Ref<const Eigen::VectorXd> & x,
                                           const Eigen::Ref<const Eigen::VectorXd> & v,
                                           const Eigen::Ref<const Eigen::VectorXd> & a,
                                           const Eigen::Ref<const Eigen::VectorXd> & FInt0,
                                           const Eigen::Ref<const Eigen::VectorXd> & FExt0,
                                           const Eigen::Ref<const Eigen::VectorXd> & FDyn0,
                                           double cInt, double cExt, double cDyn,
                                           Eigen::Ref<Eigen::MatrixXd> K,
                                           Eigen::Ref<Eigen::MatrixXd> C,
                                           bool KDyn,
                                           bool KExt,
                                           bool CDyn,
                                           bool CInt)
{
    throw IntegrableException("Second order system function not implemented: computeTangentMatrices") ;
}

} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
