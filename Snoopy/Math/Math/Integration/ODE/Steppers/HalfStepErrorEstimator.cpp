#include "Math/Integration/ODE/Steppers/HalfStepErrorEstimator.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

HalfStepErrorEstimator::HalfStepErrorEstimator() :
    p_stepper_(nullptr), order_(2)
{
}

void HalfStepErrorEstimator::setStepper(FirstOrderStepperABC * p_stepper,
                                        std::size_t nState,
                                        std::size_t nStateDer,
                                        std::size_t order)
{
    p_stepper_ = p_stepper ;
    y1_ = State(nState) ;
    y1_2_ = State(nState) ;
    y2_ = State(nState) ;
    tmpDy_ = StateDerivative(nStateDer) ;
    tmpDy2_ = StateDerivative(nStateDer) ;
    relError_ = State(nState) ;
    order_ = order ;
}

bool HalfStepErrorEstimator::estimate(
                  const double t, double & dt,
                  const State & oldState,
                  const StateDerivative & oldStateDer,
                  State & newState,
                  StateDerivative & newStateDer,
                  const double tolerance
                                     )
{
    p_stepper_->doStep(t, dt, oldState, oldStateDer, y1_, tmpDy_) ;
    p_stepper_->doStep(t, dt/2., oldState, oldStateDer, y1_2_, tmpDy2_) ;
    p_stepper_->doStep(t+dt/2., dt/2., y1_2_, tmpDy2_, y2_, tmpDy_) ;
    relError_ = y2_ - y1_ ;
    double error(relError_.normInf()) ;
    dt *= 0.9 * std::min(std::max(std::pow(tolerance/error, 1./static_cast<double>(order_)), 0.3), 2.) ;
    if (error < tolerance)
    {
        newState = y2_ + relError_ ;
        return true ;
    }
    return false ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
