#include <iostream>
#include <Eigen/Sparse>
// These commented includes are necessary to support external solvers
//#include <Eigen/PaStiXSupport>
//#include <Eigen/UmfPackSupport>
//#include <Eigen/SPQRSupport>
#ifdef EIGEN_USE_MKL_ALL
#include <Eigen/PardisoSupport>
#endif
#include <iostream>
#include <Eigen/Dense>
#include <unsupported/Eigen/src/IterativeSolvers/Scaling.h>
#include <unsupported/Eigen/MatrixFunctions>

#include "Math/Integration/ODE/Steppers/HHT.hpp"
#include "Geometry/sx3Manipulation.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

namespace Details {

bool SolveSystem(const Eigen::Ref<const Eigen::MatrixXd> & A,
                 const Eigen::Ref<const Eigen::VectorXd> & b,
                 Eigen::Ref<Eigen::VectorXd> x,
                 SolverType solverType)
{
    //Eigen::JacobiSVD<Eigen::MatrixXd> svd(A);
    //double cond = svd.singularValues()(0) 
    //                / svd.singularValues()(svd.singularValues().size()-1);
    //std::cout << "cond: " << cond << std::endl ;
    //std::cout << "J:" << std::endl ;
    //std::cout << A << std::endl ;
    try
    {
        if (solverType == SolverType::DENSE)
        {
            //x = A.completeOrthogonalDecomposition().solve(b) ;
            //x = A.colPivHouseholderQr().solve(b) ;
            x = A.householderQr().solve(b) ;
            //x = A.partialPivLu().solve(b) ;
        }
        else if (solverType == SolverType::SPARSE)
        {
            //Eigen::SparseView<Eigen::Ref<const Eigen::MatrixXd> > sparseA(A.sparseView()) ;
            Eigen::SparseMatrix<double> sparseA(A.sparseView()) ;
#ifdef EIGEN_USE_MKL_ALL // We use MKL Pardiso if available
            Eigen::PardisoLU<Eigen::SparseMatrix<double> > solver ;
            solver.pardisoParameterArray()[59] = 1 ; // out-of-core feature
#else // Eigen LU otherwise
            Eigen::SparseLU<Eigen::SparseMatrix<double> > solver ;
            solver.analyzePattern(sparseA) ;
            solver.factorize(sparseA) ;
#endif
            solver.compute(sparseA) ;
            sparseA.makeCompressed() ;
            x = solver.solve(b) ;
        }
        else if (solverType == SolverType::SCALED_SPARSE)
        {
            Eigen::SparseMatrix<double> sparseA(A.sparseView()) ;
            Eigen::IterScaling<Eigen::SparseMatrix<double> > scal ;
            scal.computeRef(sparseA) ;
            //Eigen::JacobiSVD<Eigen::MatrixXd> svdScal(sparseA);
            //double condScal = svdScal.singularValues()(0) 
            //                / svdScal.singularValues()(svdScal.singularValues().size()-1);
            //std::cout << "cond scaled: " << condScal << std::endl ;
            Eigen::VectorXd bTmp(scal.LeftScaling().cwiseProduct(b)) ;
            /*
             Different solvers were tested with Opera on complete FOWT with full dynamics enabled
             CPU: Intel(R) Xeon(R) CPU E5-2630 v4 @ 2.20GHz
             OS: linux
             Commit: TODO
             The simulation duration was set to 1 second, with a constant time step of 2.e-2s
             The tested solvers work on square matrices with no other specific singularity.

             Eigen built-in direct solvers

              Eigen::SparseQR<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>  > solver ; // 1587.467 s, mean it: 5
              Eigen::SparseLU<Eigen::SparseMatrix<double> > solver ; // 78.350 s, mean it: 5

             Eigen built-in iterative solvers

              Eigen::LeastSquaresConjugateGradient<Eigen::SparseMatrix<double> > solver ; // No convergence
              Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::IncompleteLUT<double> > solver ; // 77.2 s, mean it: 5
            
             External libraries with Eigen support

             PaStiX package, CeCILL-C license
              Eigen::PastixLU<Eigen::SparseMatrix<double> > solver ; // 92.059 s, mean it: 5

             SuiteSparse, GPL license
              Eigen::UmfPackLU<Eigen::SparseMatrix<double> > solver ; // 77.785 s, mean it: 5

             solver.analyzePattern(sparseA) ;
             solver.factorize(sparseA) ;

             SuiteSparse, GPL license
              Eigen::SPQR<Eigen::SparseMatrix<double> > solver ; // 81.403 s, mean it: 5

             MKL, commercial license
              Eigen::PardisoLU<Eigen::SparseMatrix<double> > solver ; // 76.360 s, mean it: 5
              solver.pardisoParameterArray()[59] = 1 ; // out-of-core feature
            */

            
#ifdef EIGEN_USE_MKL_ALL // We use MKL Pardiso if available
            Eigen::PardisoLU<Eigen::SparseMatrix<double> > solver ;
            solver.pardisoParameterArray()[59] = 1 ; // out-of-core feature
#else // Eigen LU otherwise
            Eigen::SparseLU<Eigen::SparseMatrix<double> > solver ;
            solver.analyzePattern(sparseA) ;
            solver.factorize(sparseA) ;
#endif

            solver.compute(sparseA) ;
            sparseA.makeCompressed() ;
            x = solver.solve(bTmp) ;
            x = scal.RightScaling().cwiseProduct(x) ;
        }
    }
    catch (std::exception & e)
    {
        std::cout << e.what() << std::endl ;
        return false ;
    }
    return true ;
}
} // End of namespace Details

HHT::HHT() :
    SecondOrderStepperABC(), firstTry_(true)
{
    setDefaultParams_() ;
}

HHT::HHT(IntegrableABC & integrable) :
    SecondOrderStepperABC(integrable),
    firstTry_(true)
{
    setDefaultParams_() ;
}

void HHT::setDefaultParams_()
{
    setMode(HHT_MODE::POSITION) ;
    setAlpha(-0.05) ;
    setIncrementTolerances(1.e-6, 1.e-6) ;
    setResidualTolerance(1.e-2) ;
    setLoadsTolerance(1.e-2) ;
    setMaxIterations(20) ;
    setMaxNIterationsBeforeStepIncrease(5) ;
    setNSuccessfulStepsBeforeStepIncrease(30) ;
    setErrorThresholdBeforeStepIncrease(0.5) ;
    setNIterationsThresholdBeforeStepDecrease(10) ;
    lastNIterations_ = 0 ;
    nSuccessfulSteps_ = 0 ;
    Rnorm_ = 0. ;
    error_ = 2. ;
    predictionMode_ = HHTPredictionMode::CONSTANT_DISPLACEMENT ;
    solverType_ = SolverType::SCALED_SPARSE ;
}

void HHT::setMode(HHT_MODE mode)
{
    mode_ = mode ;
}

void HHT::setAlpha(double alpha)
{
    if (alpha < -1./3.)
    {
        alpha = -1./3. ;
    }
    else if (alpha > 0.)
    {
        alpha = 0. ;
    }
    alpha_ = alpha ; // Between (-1/3 and 0) 0 -> no dissipation
    gamma_ = (1. - 2. * alpha_) / 2. ;
    beta_ = std::pow(1. - alpha_, 2) / 4. ;
}

void HHT::setIncrementTolerances(const double absTol, const double relTol)
{
    incrementAbsTol_ = absTol ;
    incrementRelTol_ = relTol ;
}

void HHT::setResidualTolerance(const double absTol)
{
    residualAbsTol_ = absTol ;
}

void HHT::setLoadsTolerance(const double relTol)
{
    loadsRelTol_ = relTol ;
}

void HHT::setMaxIterations(std::size_t maxIterations)
{
    maxIterations_ = maxIterations ;
}

void HHT::setMaxNIterationsBeforeStepIncrease(std::size_t val)
{
    maxNIterationsBeforeStepIncrease_ = val ;
}

void HHT::setNSuccessfulStepsBeforeStepIncrease(std::size_t val)
{
    nSuccessfulStepsBeforeStepIncrease_ = val ;
}

void HHT::setErrorThresholdBeforeStepIncrease(double val)
{
    errorThresholdBeforeStepIncrease_ = val ;
}

void HHT::setNIterationsThresholdBeforeStepDecrease(std::size_t val)
{
    nIterationsThresholdBeforeStepDecrease_ = val ;
}

void HHT::setPredictionMode(HHTPredictionMode predictionMode)
{
    predictionMode_ = predictionMode ;
}

void HHT::setSolverType(SolverType solverType)
{
    solverType_ = solverType ;
}

void HHT::setup(const double t)
{
    SecondOrderStepperABC::setup(t) ;
    std::size_t nV(p_integrable_->getNStateVel()) ;
    std::size_t nA(p_integrable_->getNStateAcc()) ;
    iterationCorrection_ = Eigen::VectorXd::Zero(nA) ;
    stepCorrection_ = Eigen::VectorXd::Zero(nA) ;
    oldDisplacementStepCorrection_ = Eigen::VectorXd::Zero(nA) ;
    oldAccelerationStepCorrection_ = Eigen::VectorXd::Zero(nA) ;
    tmpDisplacementStepCorrection_ = Eigen::VectorXd::Zero(nA) ;
    xAsRotationVector_ = Eigen::VectorXd::Zero(nA) ;
    errorWeights_ = Eigen::VectorXd::Zero(nA) ;
    xNew_ = x_ ;
    vNew_ = Eigen::VectorXd::Zero(nV) ;
    aNew_ = Eigen::VectorXd::Zero(nA) ;
    R_ = Eigen::VectorXd::Zero(nA) ;
    Rold_ = Eigen::VectorXd::Zero(nA) ;
    dFdX_ = Eigen::MatrixXd::Zero(nA, nA) ;
    dFdV_ = Eigen::MatrixXd::Zero(nA, nA) ;
    Bt_ = Eigen::MatrixXd::Identity(nA, nA) ;
    spinStateIndexer_ = stateIndexer_.getStateIndexerUsingRotVec() ;
    FInt_ = Eigen::VectorXd::Zero(nA) ;
    FExt_ = Eigen::VectorXd::Zero(nA) ;
    FDyn_ = Eigen::VectorXd::Zero(nA) ;
    FIntOld_ = Eigen::VectorXd::Zero(nA) ;
    FExtOld_ = Eigen::VectorXd::Zero(nA) ;
    FDynOld_ = Eigen::VectorXd::Zero(nA) ;
    M_ = Eigen::MatrixXd::Zero(nA, nA) ;
    error_ = 2. ;
    firstTry_ = true ;
    FOldInitialized_ = false ;
}

bool HHT::doStep(const double t, const double dt,
                 const Eigen::Ref<const Eigen::VectorXd> & oldX,
                 const Eigen::Ref<const Eigen::VectorXd> & oldV,
                 const Eigen::Ref<const Eigen::VectorXd> & oldA,
                 Eigen::Ref<Eigen::VectorXd> newX,
                 Eigen::Ref<Eigen::VectorXd> newV,
                 Eigen::Ref<Eigen::VectorXd> newA)
{
    double h(dt) ;

    bool converged(false) ;
    setupStep_(h, oldX, oldV, oldA, newX, newV, newA) ;

    for (std::size_t it=0; it<maxIterations_; ++it)
    {
        converged = increment_(h, oldX, oldV, oldA, newX, newV, newA) ;
        firstIteration_ = false ;

        // convergence trend flag
        //if ((Rold_.norm() < R_.norm()) && (R_.norm() > 1./absTol_))
        //{
        //    if (error_ < 1.)
        //    {
        //        // We don't want the error to be lower than 1. or
        //        // the time step won't be decreased
        //        error_ = 1. ;
        //    }
        //    return false ;
        //}

        if (converged)
        {
            nIterations_ = it+1 ;
            lastNIterations_ = nIterations_ ;
            ++nSuccessfulSteps_ ;
            FIntOld_ = FInt_ ;
            FExtOld_ = FExt_ ;
            FDynOld_ = FDyn_ ;
            if (mode_ == HHT_MODE::ACCELERATION)
            {
                oldAccelerationStepCorrection_ = stepCorrection_ ;
                oldDisplacementStepCorrection_ = tmpDisplacementStepCorrection_ ;
            }
            else if (mode_ == HHT_MODE::POSITION)
            {
                oldDisplacementStepCorrection_ = stepCorrection_ ;
                oldAccelerationStepCorrection_ = newA - oldA ;
            }
            errorMessage_ = "" ;
            //std::cout << "t: " << t << " dt: " << dt << " it: " << nIterations_ << std::endl ;
            return true ;
        }
    }
    errorMessage_ = "Maximum number of iterations reached" ;
    return false ;
}

void HHT::advance(const double step, bool updateStateEachStep)
{
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, x_, v_, a_) ;
    }
    bool converged(doStep(currentTime_, step, x_, v_, a_, xNew_, vNew_, aNew_)) ;

    currentTime_ += step ;
    if (!converged)
    {
        throw StepperException("HHT did not converge: " + errorMessage_) ;
    }
    else
    {
        x_ = xNew_ ;
        v_ = vNew_ ;
        a_ = aNew_ ;
    }
    p_integrable_->updateState(currentTime_, x_, v_, a_) ;
}

bool HHT::tryStep(double & dt, bool updateStateEachStep)
{
    if (updateStateEachStep && firstTry_)
    {
        p_integrable_->provideState(currentTime_, x_, v_, a_) ;
        firstTry_ = false ;
    }
    bool converged(false) ;
    try
    {
        converged = doStep(currentTime_, dt, x_, v_, a_, xNew_, vNew_, aNew_) ;
    }
    catch(const std::exception& e)
    {
        errorMessage_ = e.what() ;
        std::cout << "ERROR: " << errorMessage_ << std::endl ;
        converged = false ;
    }
    
    if (!converged)
    {
        // Decrease dt
        if ((error_ < 1.) || (!std::isfinite(error_)))
        {
            dt *= 0.5 ;
        }
        else
        {
            dt *= std::max(0.9 * std::pow(error_, -1.), 1./5.) ;
        }
        nSuccessfulSteps_ = 0 ;
        return false ;
    }
    currentTime_ += dt ;
    x_ = xNew_ ;
    v_ = vNew_ ;
    a_ = aNew_ ;
    p_integrable_->updateState(currentTime_, x_, v_, a_) ;
    firstTry_ = true ;
    if (lastNIterations_ > nIterationsThresholdBeforeStepDecrease_)
    {
        // Decrease step a little as it has troubles to converge
        dt *= 0.9 ;
    }
    else if ((lastNIterations_ < maxNIterationsBeforeStepIncrease_)
             && (nSuccessfulSteps_ > nSuccessfulStepsBeforeStepIncrease_)
             && (error_ < errorThresholdBeforeStepIncrease_))
    {
        // Increase dt
        //dt *= 1.2 ;
        error_ = std::max(std::pow(5., -2), error_) ;
        dt *= 0.9 * std::pow(error_, -1./2.) ;
        nSuccessfulSteps_ = 0 ; // Reset counter as we don't want to increase too often
    }
    return true ;
}

void HHT::orientationsToRotationVector_(const Eigen::Ref<const Eigen::VectorXd> & pos,
                                        Eigen::Ref<Eigen::VectorXd> posConverted)
{
    const std::map<std::string, std::map<Eigen::Index, Eigen::Index> > & correspondances(spinStateIndexer_.getStateCorrespondingIndices()) ;
    for (auto const & [ind, indexer] : spinStateIndexer_.getIndices())
    {
        if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::ROTATION_VECTOR)
        {
            if (correspondances.at("rotToQuat").find(ind) != correspondances.at("rotToQuat").end())
            {
                // Convert quaternion to rotation vector
                Eigen::Index quatInd(correspondances.at("rotToQuat").at(ind)) ;
                BV::Geometry::Rotation::Quaternion q(pos.segment<4>(quatInd)) ;
                posConverted.segment<3>(ind) = q.toRotationVector().unknowns() ;
            }
        }
    }
}

void HHT::displacementIncrement_(double h,
                                 const Eigen::Ref<const Eigen::VectorXd> & oldX,
                                 const Eigen::Ref<const Eigen::VectorXd> & oldV,
                                 const Eigen::Ref<const Eigen::VectorXd> & oldA,
                                 Eigen::Ref<Eigen::VectorXd> & newX,
                                 Eigen::Ref<Eigen::VectorXd> & newV,
                                 Eigen::Ref<Eigen::VectorXd> & newA,
                                 const Eigen::Ref<const Eigen::VectorXd> & iterationCorrection,
                                 Eigen::Ref<Eigen::VectorXd> stepCorrection)
{
    const std::map<std::string, std::map<Eigen::Index, Eigen::Index> > & correspondances(spinStateIndexer_.getStateCorrespondingIndices()) ;
    Eigen::Matrix3d lambda ;
    for (auto const & [ind, indexer] : spinStateIndexer_.getIndices())
    {
        if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::ROTATION_VECTOR)
        {
            BV::Geometry::Rotation::RotationVector rv(iterationCorrection.segment<3>(ind)) ;
            // Get lambda i-1
            setLambda_(stepCorrection.segment<3>(ind), lambda) ;
            lambda = rv.getMatrix() * lambda ;
            // Update step correction
            stepCorrection.segment<3>(ind) = BV::Geometry::so3ToR3(lambda.log()) ;
            // Then update velocities and accelerations
            updateVelAcc_(h, newV.segment<3>(ind), newA.segment<3>(ind),
                          oldV.segment<3>(ind), oldA.segment<3>(ind),
                          stepCorrection.segment<3>(ind)) ;

            newV.segment<3>(ind) = lambda * newV.segment<3>(ind) ;
            newA.segment<3>(ind) = lambda * newA.segment<3>(ind) ;
            // Finally deal with orientations
            if (correspondances.at("rotToQuat").find(ind) != correspondances.at("rotToQuat").end())
            {
                // Update quaternion using rotation vector
                Eigen::Index quatInd(correspondances.at("rotToQuat").at(ind)) ;
                BV::Geometry::Rotation::Quaternion q(oldX.segment<4>(quatInd)) ;
                Eigen::Vector3d wVals(stepCorrection.segment<3>(ind)) ;
                newX.segment<4>(quatInd) = (BV::Geometry::Rotation::Quaternion(BV::Math::Details::QFromRotV(wVals)) * q).unknowns() ;
            }
            else
            {
                Eigen::Index indInState(correspondances.at("rotToRot").at(ind)) ;
                BV::Geometry::Rotation::RotationVector rt = BV::Geometry::Rotation::RotationVector(oldX.segment<3>(indInState)) ;
                BV::Geometry::Rotation::RotationMatrix newR = BV::Geometry::Rotation::RotationMatrix(lambda*rt.toRotationMatrix().getMatrix()) ;
                newX.segment<3>(indInState) = newR.toRotationVector().unknowns() ;
            }
        }
        else // No rotation state
        {
            Eigen::Index extentInSpinState(indexer.extentInStateDer) ;
            Eigen::Index indInState(correspondances.at("otherToOther").at(ind)) ;
            Eigen::Index extent(indexer.extentInState) ;
            // Add iteration correction to step correction
            stepCorrection.segment(ind, extentInSpinState) += iterationCorrection.segment(ind, extentInSpinState) ;
            updateVelAcc_(h, newV.segment(ind, extent), newA.segment(ind, extent),
                          oldV.segment(ind, extent), oldA.segment(ind, extent),
                          stepCorrection.segment(ind, extentInSpinState)) ;
            newX.segment(indInState, extent) = oldX.segment(indInState, extent) + stepCorrection.segment(ind, extent) ;
        }
    }
}

void HHT::accelerationIncrement_(double h,
                                 const Eigen::Ref<const Eigen::VectorXd> & oldX,
                                 const Eigen::Ref<const Eigen::VectorXd> & oldV,
                                 const Eigen::Ref<const Eigen::VectorXd> & oldA,
                                 Eigen::Ref<Eigen::VectorXd> & newX,
                                 Eigen::Ref<Eigen::VectorXd> & newV,
                                 Eigen::Ref<Eigen::VectorXd> & newA,
                                 const Eigen::Ref<const Eigen::VectorXd> & iterationCorrection,
                                 Eigen::Ref<Eigen::VectorXd> stepCorrection)
{
    const std::map<std::string, std::map<Eigen::Index, Eigen::Index> > & correspondances(spinStateIndexer_.getStateCorrespondingIndices()) ;
    Eigen::Matrix3d lambda ;
    stepCorrection += iterationCorrection ;
    newA = oldA + stepCorrection ;
    for (auto const & [ind, indexer] : spinStateIndexer_.getIndices())
    {
        // Add iteration correction to step correction
        if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::ROTATION_VECTOR)
        {
            // First compute orientation according to acceleration increment
            // Compute Gamma
            double aNormSquared(std::pow(newA.segment<3>(ind).norm(), 2)) ;
            Eigen::Matrix3d aSkew(BV::Geometry::R3Toso3(newA.segment<3>(ind))) ;
            double coef(std::pow(h, 4) * beta_ * beta_) ;
            double coef1(coef * aNormSquared) ;
            Eigen::Matrix3d Gamma(1. / (1. + coef1)
                                  * ((1. + coef1) * Eigen::Matrix3d::Identity()
                                     + h * h * beta_ * aSkew
                                     + coef * aSkew * aSkew)) ;
            Eigen::Vector3d posIncrement(Gamma * (h * oldV.segment<3>(ind)
                                                  + h * h * (0.5 - beta_) * oldA.segment<3>(ind)
                                                  + h * h * beta_ * newA.segment<3>(ind))) ;
            tmpDisplacementStepCorrection_.segment<3>(ind) = posIncrement ;
            // Then compute velocity
            // Get lambda i
            setLambda_(posIncrement, lambda) ;
            newV.segment<3>(ind) = lambda * (oldV.segment<3>(ind)
                                             + h * (1. - gamma_) * oldA.segment<3>(ind))
                                            + h * gamma_ * newA.segment<3>(ind) ;

            // Finally deal with orientations
            if (correspondances.at("rotToQuat").find(ind) != correspondances.at("rotToQuat").end())
            {
                // Update quaternion using rotation vector
                Eigen::Index quatInd(correspondances.at("rotToQuat").at(ind)) ;
                BV::Geometry::Rotation::Quaternion q(oldX.segment<4>(quatInd)) ;
                newX.segment<4>(quatInd) = (BV::Geometry::Rotation::Quaternion(BV::Math::Details::QFromRotV(posIncrement)) * q).unknowns() ;
            }
            else
            {
                Eigen::Index indInState(correspondances.at("rotToRot").at(ind)) ;
                BV::Geometry::Rotation::RotationVector rt = BV::Geometry::Rotation::RotationVector(oldX.segment<3>(indInState)) ;
                BV::Geometry::Rotation::RotationMatrix newR = BV::Geometry::Rotation::RotationMatrix(lambda*rt.toRotationMatrix().getMatrix()) ;
                newX.segment<3>(indInState) = newR.toRotationVector().unknowns() ;
            }
        }
        else // No rotation state
        {
            Eigen::Index extent(indexer.extentInState) ;
            Eigen::Index indInState(correspondances.at("otherToOther").at(ind)) ;
            newX.segment(indInState, extent) = oldX.segment(indInState, extent)
                                        + h * oldV.segment(ind, extent)
                                        + h * h * ((0.5 - beta_) * oldA.segment(ind, extent)
                                                   + beta_ * newA.segment(ind, extent)) ;
            tmpDisplacementStepCorrection_.segment(ind, extent) = newX.segment(indInState, extent)
                                                                 - oldX.segment(indInState, extent) ;
            newV.segment(ind, extent) = oldV.segment(ind, extent)
                                        + h * ((1 - gamma_) * oldA.segment(ind, extent)
                                               + gamma_ * newA.segment(ind, extent)) ;
        }
    }
}

void HHT::setupStep_(const double h,
                     const Eigen::Ref<const Eigen::VectorXd> & oldX,
                     const Eigen::Ref<const Eigen::VectorXd> & oldV,
                     const Eigen::Ref<const Eigen::VectorXd> & oldA,
                     Eigen::Ref<Eigen::VectorXd> newX,
                     Eigen::Ref<Eigen::VectorXd> newV,
                     Eigen::Ref<Eigen::VectorXd> newA)
{
    // Step correction is the delta between last converged step and current one
    stepCorrection_.setZero() ;
    // Prediction
    if (predictionMode_ == HHTPredictionMode::CONSTANT_ACCELERATION)
    {
        accelerationIncrement_(h, oldX, oldV, oldA, newX, newV, newA,
                               oldAccelerationStepCorrection_, stepCorrection_) ;
        if (mode_ == HHT_MODE::POSITION)
        {
            // We need to convert stepCorrection_ in displacement correction
            stepCorrection_ = tmpDisplacementStepCorrection_ ;
        }
    }
    else if (predictionMode_ == HHTPredictionMode::CONSTANT_POSITION)
    {
        displacementIncrement_(h, oldX, oldV, oldA, newX, newV, newA,
                               stepCorrection_, stepCorrection_) ;
    }
    else if (predictionMode_ == HHTPredictionMode::CONSTANT_DISPLACEMENT)
    {
        displacementIncrement_(h, oldX, oldV, oldA, newX, newV, newA,
                               oldDisplacementStepCorrection_, stepCorrection_) ;
        if (mode_ == HHT_MODE::ACCELERATION)
        {
            // We need to convert stepCorrection_ in displacement correction
            tmpDisplacementStepCorrection_ = stepCorrection_ ;
            stepCorrection_ = newA - oldA ;
        }
    }
    Rold_.setZero() ;
    if (!FOldInitialized_)
    {
        p_integrable_->updateState(currentTime_, oldX, oldV, oldA) ;
        p_integrable_->computeDynamicEquationComponents(
                                        currentTime_, oldX, oldV, oldA,
                                        FIntOld_, FExtOld_, FDynOld_, M_
                                                       ) ;
        FOldInitialized_ = true ;
    }
    Rold_ = -alpha_ * (FIntOld_ - FExtOld_) ;
    if (mode_ == HHT_MODE::ACCELERATION)
    {
        calcErrorWeights_(oldA, incrementRelTol_, incrementAbsTol_, errorWeights_) ;
    }
    else
    {
        orientationsToRotationVector_(oldX, xAsRotationVector_) ;
        calcErrorWeights_(xAsRotationVector_, incrementRelTol_, incrementAbsTol_, errorWeights_) ;
    }
    error_ = 2. ;
    firstIteration_ = true ;
}

void HHT::setTsInvT_(const Eigen::Ref<const Eigen::Vector3d> & rotVec,
                     Eigen::Ref<Eigen::Matrix3d> mat)
{
    double angleSquared(rotVec.dot(rotVec)) ;
    if (BV::Math::IsClose(angleSquared, 0.))
    {
        mat = Eigen::Matrix3d::Identity() ;
        return ;
    }
    double angle(std::sqrt(angleSquared)) ;
    double halfAngle(angle / 2.) ;
    double coef(halfAngle / std::tan(halfAngle)) ;
    mat = coef * Eigen::Matrix3d::Identity()
          + (1. - coef) * rotVec * rotVec.transpose() / angleSquared
          + 0.5 * BV::Geometry::R3Toso3(rotVec) ;
}

void HHT::updateBtp_()
{
    for (auto const & [ind, indexer] : spinStateIndexer_.getIndices())
    {
        if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::ROTATION_VECTOR)
        {
            setTsInvT_(stepCorrection_.segment<3>(ind), Bt_.block<3, 3>(ind, ind)) ;
        }
    }
}

void HHT::setTsT_(const Eigen::Ref<const Eigen::Vector3d> & rotVec,
                  Eigen::Ref<Eigen::Matrix3d> mat)
{
    double angleSquared(rotVec.dot(rotVec)) ;
    if (BV::Math::IsClose(angleSquared, 0.))
    {
        mat = Eigen::Matrix3d::Identity() ;
        return ;
    }
    double angle(std::sqrt(angleSquared)) ;
    Eigen::Matrix3d rotVecSkew(BV::Geometry::R3Toso3(rotVec)) ;
    mat = Eigen::Matrix3d::Identity()
            - (1. - std::cos(angle)) / angleSquared * rotVecSkew
            - (angle - std::sin(angle)) / std::pow(angle, 3) * rotVecSkew * rotVecSkew ;
}

void HHT::updateBta_()
{
    for (auto const & [ind, indexer] : spinStateIndexer_.getIndices())
    {
        if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::ROTATION_VECTOR)
        {
            setTsT_(tmpDisplacementStepCorrection_.segment<3>(ind), Bt_.block<3, 3>(ind, ind)) ;
        }
    }
}

void HHT::setLambda_(const Eigen::Ref<const Eigen::Vector3d> & rotV,
                     Eigen::Ref<Eigen::Matrix3d> lambda)
{
    lambda = BV::Geometry::R3Toso3(rotV).exp() ;
}

void HHT::updateVelAcc_(double h,
                        Eigen::Ref<Eigen::VectorXd> newV,
                        Eigen::Ref<Eigen::VectorXd> newA,
                        const Eigen::Ref<const Eigen::VectorXd> & oldV,
                        const Eigen::Ref<const Eigen::VectorXd> & oldA,
                        const Eigen::Ref<const Eigen::VectorXd> & stepCorrection)
{
    newV = gamma_ / (h * beta_) * stepCorrection
            + (beta_ - gamma_) / beta_ * oldV
            + h * (beta_ - 0.5 * gamma_) / beta_ * oldA ;
    newA = stepCorrection / (h * h * beta_)
            - oldV / (h * beta_)
            - (0.5 - beta_) / beta_ * oldA ;
}

bool HHT::increment_(const double h,
                     const Eigen::Ref<const Eigen::VectorXd> & oldX,
                     const Eigen::Ref<const Eigen::VectorXd> & oldV,
                     const Eigen::Ref<const Eigen::VectorXd> & oldA,
                     Eigen::Ref<Eigen::VectorXd> newX,
                     Eigen::Ref<Eigen::VectorXd> newV,
                     Eigen::Ref<Eigen::VectorXd> newA)
{
    double tNew(currentTime_+h) ;
    p_integrable_->updateState(tNew, newX, newV, newA) ;

    R_ = Rold_ ;

    // Update residual
    p_integrable_->computeDynamicEquationComponents(tNew, newX, newV, newA,
                                                    FInt_, FExt_, FDyn_, M_) ;
    normLoads_ = FExt_.norm() ;
    R_ += (1. + alpha_) * (FInt_ - FExt_) ;
    R_ += FDyn_ ; // M * a + gyroscopic...

    if (checkConvergence_(oldX, oldV, oldA, newX, newV, newA))
    {
        return true ;
    }

    // Compute the tangent matrices
    p_integrable_->computeTangentMatrices(tNew, newX, newV, newA,
                                            FInt_, FExt_, FDyn_,
                                            1.+alpha_, -1.-alpha_, 1.,
                                            dFdX_, dFdV_,
                                            false, true, false, true) ;
        

    if (mode_ == HHT_MODE::ACCELERATION)
    {
        updateBta_() ;
        Details::SolveSystem(M_
                             + (1. + alpha_) * gamma_ * h * dFdV_
                             + (1. + alpha_) * h * h * beta_ * dFdX_ * Bt_,
                             -R_,
                             iterationCorrection_, 
                             solverType_) ;
        
        // Update system state. step correction is the total correction from last
        // converged step.
        // We have to deal with rotations in a specific way so we have to loop
        accelerationIncrement_(h, oldX, oldV, oldA, newX, newV, newA,
                               iterationCorrection_, stepCorrection_) ;
    }
    else if (mode_ == HHT_MODE::POSITION)
    {
        // Compute the Bt matrix linking spatial spin to rotation variation
        updateBtp_() ;

        Details::SolveSystem(1./(h * h * beta_) * M_ * Bt_
                             + (1. + alpha_) * gamma_ / (h * beta_) * dFdV_ * Bt_
                             + (1. + alpha_) * dFdX_,
                             -R_,
                             iterationCorrection_, 
                             solverType_) ;
        
        // Update system state. step correction is the total correction from last
        // converged step.
        // We have to deal with rotations in a specific way so we have to loop
        displacementIncrement_(h, oldX, oldV, oldA, newX, newV, newA,
                               iterationCorrection_, stepCorrection_) ;
    }
    else
    {
        throw StepperException("HHT stepper mode not implemented") ;
    }
    return false ;
}

void HHT::calcErrorWeights_(const Eigen::VectorXd & x, double rtol, double atol,
                            Eigen::VectorXd & ewt)
{
    ewt = (rtol * x.cwiseAbs() + Eigen::VectorXd::Constant(x.rows(), atol)).cwiseInverse() ;
}

double HHT::wrmsNorm_(const Eigen::VectorXd & values,
                      const Eigen::VectorXd & weights)
{
    return Eigen::numext::sqrt(values.cwiseProduct(weights).cwiseAbs2().sum() / values.size()) ;
}

bool HHT::checkConvergence_(const Eigen::Ref<const Eigen::VectorXd> & oldX,
                            const Eigen::Ref<const Eigen::VectorXd> & oldV,
                            const Eigen::Ref<const Eigen::VectorXd> & oldA,
                            const Eigen::Ref<const Eigen::VectorXd> & newX,
                            const Eigen::Ref<const Eigen::VectorXd> & newV,
                            const Eigen::Ref<const Eigen::VectorXd> & newA)
{
    if ((!R_.allFinite()) || (!iterationCorrection_.allFinite()))
    {
        throw StepperException("HHT did not converge: non-finite values") ;
    }
    Rnorm_ = R_.norm() ;
    if (!firstIteration_)
    {
        error_ = wrmsNorm_(iterationCorrection_, errorWeights_) ;
    }
    //std::cout << "norm: " << Rnorm_ <<  " norm loads: " << normLoads_ << " error: " << error_ << std::endl ;
    absConvergenceCriteria_ = Rnorm_ < residualAbsTol_  ;
    relConvergenceCriteria_ = Rnorm_ < (normLoads_ * loadsRelTol_) ;

    if (absConvergenceCriteria_ || relConvergenceCriteria_ || (error_ < 1.))
    {
        return true ;
    }
    return false ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
