#include "Math/Integration/ODE/Steppers/RK2.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

RK2::RK2(IntegrableABC & integrable) :
    FirstOrderStepperABC(integrable),
    firstTry_(true)
{
}

RK2::RK2() :
    FirstOrderStepperABC(),
    firstTry_(true)
{
}

void RK2::setup(const double t)
{
    FirstOrderStepperABC::setup(t) ;
    std::size_t nY(p_integrable_->getNState()) ;
    std::size_t nDY(p_integrable_->getNStateDerivative()) ;
    k2_ = StateDerivative(nDY) ;
    yNew_ = State(nY) ;
    dyNew_ = StateDerivative(nDY) ;
    errorEstimator_.setStepper(this, nY, nDY, 2) ;
    firstTry_ = true ;
}

void RK2::doStep(const double t, const double dt,
                 const State & oldState,
                 const StateDerivative & oldStateDer,
                 State & newState,
                 StateDerivative & newStateDer)
{
    newState = oldState + 0.5*dt*oldStateDer ;
    p_integrable_->computeStateDerivative(t+0.5*dt, newState, k2_) ;
    newState = oldState + dt*k2_ ;
    p_integrable_->computeStateDerivative(t+dt, newState, newStateDer) ;
}

void RK2::advance(const double dt, bool updateStateEachStep)
{
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
    }
    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;
    y_ = yNew_ ;
    dy_ = dyNew_ ;
    currentTime_ += dt ;
    p_integrable_->updateState(currentTime_, y_) ;
    p_integrable_->updateStateDerivative(currentTime_, dy_) ;
}

bool RK2::tryStep(double & dt, bool updateStateEachStep)
{
    if (updateStateEachStep && firstTry_)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
        firstTry_ = false ;
    }
    double currentDt(dt) ; // Error estimate may change dt
    bool isOK(errorEstimator_.estimate(currentTime_, dt, y_, dy_, yNew_, dyNew_,
                                       relTol_)) ;
    if (isOK)
    {
        y_ = yNew_ ;
        dy_ = yNew_ ;
        currentTime_ += currentDt ;
        p_integrable_->updateState(currentTime_, y_) ;
        p_integrable_->updateStateDerivative(currentTime_, dy_) ;
        firstTry_ = true ;
    }
    return isOK ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
