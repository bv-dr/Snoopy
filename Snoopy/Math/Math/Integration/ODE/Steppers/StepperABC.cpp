#include "Math/Integration/ODE/Steppers/StepperABC.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

ObserverABC::ObserverABC() : hasFinished_(false), nSteps_(0),
    raisedException_(false)
{
}

void ObserverABC::simulationInformation(double xStart, double xEnd, double initStep)
{
    // Default does nothing
}

bool ObserverABC::hasFinished() const
{
    return hasFinished_ ;
}

void ObserverABC::setHasFinished(const bool & val)
{
    hasFinished_ = val ;
}

std::size_t ObserverABC::nIntegrationSteps() const
{
    return nSteps_ ;
}

void ObserverABC::setNIntegrationSteps(const std::size_t & nSteps)
{
    nSteps_ = nSteps ;
}

bool ObserverABC::hasRaisedException() const
{
    return raisedException_ ;
}

const std::string & ObserverABC::getException() const
{
    return errorMessage_ ;
}

void ObserverABC::setException(const std::exception & e)
{
    errorMessage_ = e.what() ;
    raisedException_ = true ;
}

void ObserverABC::addStep(const double & step)
{
    steps_.push_back(step) ;
}

void ObserverABC::addStepSize(const double & stepSize)
{
    stepSize_.push_back(stepSize) ;
}

void ObserverABC::addNIterations(const std::size_t & nIterations)
{
    iterations_.push_back(nIterations) ;
}

const std::vector<double> & ObserverABC::getSteps() const
{
    return steps_ ;
}

const std::vector<double> & ObserverABC::getStepsSize() const
{
    return stepSize_ ;
}

const std::vector<std::size_t> & ObserverABC::getNIterations() const
{
    return iterations_ ;
}

void ObserverABC::setLastState(const Eigen::Ref<const Eigen::VectorXd> & state)
{
    lastState_ = state ;
}

void ObserverABC::setLastStateDerivative(const Eigen::Ref<const Eigen::VectorXd> & stateDer)
{
    lastStateDerivative_ = stateDer ;
}

void ObserverABC::setLastPos(const Eigen::Ref<const Eigen::VectorXd> & pos)
{
    lastPos_ = pos ;
}

void ObserverABC::setLastVel(const Eigen::Ref<const Eigen::VectorXd> & vel)
{
    lastVel_ = vel ;
}

void ObserverABC::setLastAcc(const Eigen::Ref<const Eigen::VectorXd> & acc)
{
    lastAcc_ = acc ;
}

const Eigen::VectorXd & ObserverABC::getLastState() const
{
    return lastState_ ;
}

const Eigen::VectorXd & ObserverABC::getLastStateDerivative() const
{
    return lastStateDerivative_ ;
}

const Eigen::VectorXd & ObserverABC::getLastPos() const
{
    return lastPos_ ;
}

const Eigen::VectorXd & ObserverABC::getLastVel() const
{
    return lastVel_ ;
}

const Eigen::VectorXd & ObserverABC::getLastAcc() const
{
    return lastAcc_ ;
}


bool ObserverABC::isLastStateFirstOrder() const
{
    if (lastState_.rows() > 0)
    {
        return true ;
    }
    return false ;
}

bool ObserverABC::isLastStateSecondOrder() const
{
    if (lastPos_.rows() > 0)
    {
        return true ;
    }
    return false ;
}


StepperABC::StepperABC(IntegrableABC & integrable) :
    p_integrable_(&integrable), currentTime_(0.),
    useAdaptiveStep_(false),
    minStep_(1.e-8),
    maxStep_(100.),
    absTol_(1.e-10),
    relTol_(1.e-6),
    scaleYError_(1.),
    scaleDYError_(1.),
    maxIterations_(20)
{
}

StepperABC::StepperABC() :
    p_integrable_(nullptr), currentTime_(0.),
    useAdaptiveStep_(false),
    minStep_(1.e-8),
    maxStep_(100.),
    absTol_(1.e-8),
    relTol_(1.e-8),
    scaleYError_(1.),
    scaleDYError_(0.),
    maxIterations_(20),
    nIterations_(1)
{
}

void StepperABC::setIntegrable(IntegrableABC & integrable)
{
    p_integrable_ = &integrable ;
}

void StepperABC::setAdaptiveStepParameters(const bool useAdaptiveStep,
                                           const double minStep,
                                           const double maxStep,
                                           const double absTol,
                                           const double relTol,
                                           const double scaleYError,
                                           const double scaleDYError,
                                           const double maxIterations)
{
    useAdaptiveStep_ = useAdaptiveStep ;
    minStep_ = minStep ;
    maxStep_ = maxStep ;
    absTol_ = absTol ;
    relTol_ = relTol ;
    scaleYError_ = scaleYError ;
    scaleDYError_ = scaleDYError ;
    maxIterations_ = maxIterations ;
}

bool StepperABC::useAdaptiveStep() const
{
    return useAdaptiveStep_ ;
}

void StepperABC::adjustStep(double & step) const
{
    if (step < minStep_)
    {
        step = minStep_ ;
        // minimum step reached
        throw StepperException("Minimum time step reached") ;
    }
    else if (step > maxStep_)
    {
        step = maxStep_ ;
    }
}

void StepperABC::failCheck() const
{
}

void StepperABC::failCheck(std::size_t it) const
{
    failCheck() ;
    if (it > maxIterations_)
    {
        throw StepperException("Maximum number of step iterations reached") ;
    }
}

double StepperABC::getTime()
{
    return currentTime_ ;
}

std::size_t StepperABC::getNIterations() const
{
    return nIterations_ ;
}

FirstOrderStepperABC::FirstOrderStepperABC(IntegrableABC & integrable) :
    StepperABC(integrable)
{
}

FirstOrderStepperABC::FirstOrderStepperABC() :
    StepperABC()
{
}

const State & FirstOrderStepperABC::getY() const
{
    return y_ ;
}

const StateDerivative & FirstOrderStepperABC::getDY() const
{
    return dy_ ;
}

void FirstOrderStepperABC::setup(const double t)
{
    y_.resize(p_integrable_->getNState()) ;
    dy_.resize(p_integrable_->getNStateDerivative()) ;
    p_integrable_->setupState(t, y_, dy_) ;
    stateIndexer_.clear() ;
    stateIndexer_.setStateSize(y_.size()) ;
    p_integrable_->setRotatorsStateIndices(stateIndexer_, 0, 0) ;
    y_.setIndexer(stateIndexer_) ;
    currentTime_ = t ;
}

void FirstOrderStepperABC::failCheck() const
{
    if (!y_.allFinite())
    {
        throw StepperException("State vector contains incorrect values") ;
    }
}

void FirstOrderStepperABC::observe(ObserverABC & observer) const
{
    observer.state(currentTime_, y_) ;
    observer.stateDerivative(currentTime_, dy_) ;
}

void FirstOrderStepperABC::observeLastStep(ObserverABC & observer) const
{
    observer.setLastState(y_) ;
    observer.setLastStateDerivative(dy_) ;
}

SecondOrderStepperABC::SecondOrderStepperABC(IntegrableABC & integrable) :
    StepperABC(integrable)
{
}

SecondOrderStepperABC::SecondOrderStepperABC() :
    StepperABC()
{
}

void SecondOrderStepperABC::setup(const double t)
{
    x_.resize(p_integrable_->getNStatePos()) ;
    v_.resize(p_integrable_->getNStateVel()) ;
    a_.resize(p_integrable_->getNStateAcc()) ;
    x_.setZero() ;
    v_.setZero() ;
    a_.setZero() ;
    p_integrable_->setupState(t, x_, v_, a_) ;
    stateIndexer_.clear() ;
    stateIndexer_.setStateSize(x_.size()) ;
    p_integrable_->setRotatorsPosVelIndices(stateIndexer_, 0, 0) ;
    currentTime_ = t ;
}

void SecondOrderStepperABC::failCheck() const
{
    if ((!x_.allFinite()) || (!v_.allFinite()))
    {
        throw StepperException("State vectors contain incorrect values") ;
    }
}

void SecondOrderStepperABC::observe(ObserverABC & observer) const
{
    observer.posVel(currentTime_, x_, v_) ;
    observer.acc(currentTime_, a_) ;
}

void SecondOrderStepperABC::observeLastStep(ObserverABC & observer) const
{
    observer.setLastPos(x_) ;
    observer.setLastVel(v_) ;
    observer.setLastAcc(a_) ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
