#pragma once

#include "IntegrationExport.hpp"

#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Core>

#include "Math/Integration/ODE/Steppers/StepperABC.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

// TODO adaptive step !

enum class HHT_MODE {ACCELERATION, POSITION} ;

enum class HHTPredictionMode {CONSTANT_ACCELERATION, CONSTANT_POSITION,
                              CONSTANT_DISPLACEMENT} ;

enum class SolverType {DENSE, SPARSE, SCALED_SPARSE} ;

class INTEGRATION_API HHT : public SecondOrderStepperABC
{
private:
    // Parameters
    HHT_MODE mode_ ;
    double alpha_ ;
    double gamma_ ;
    double beta_ ;
    double incrementAbsTol_ ;
    double incrementRelTol_ ;
    double residualAbsTol_ ;
    double loadsRelTol_ ;
    std::size_t maxIterations_ ;
    std::size_t lastNIterations_ ;
    std::size_t nSuccessfulSteps_ ;
    std::size_t maxNIterationsBeforeStepIncrease_ ;
    std::size_t nSuccessfulStepsBeforeStepIncrease_ ;
    double errorThresholdBeforeStepIncrease_ ;
    std::size_t nIterationsThresholdBeforeStepDecrease_ ;
    HHTPredictionMode predictionMode_ ;
    SolverType solverType_ ;

    // Internal state
    Eigen::VectorXd xNew_ ;
    Eigen::VectorXd vNew_ ;
    Eigen::VectorXd aNew_ ;
    Eigen::VectorXd iterationCorrection_ ;
    Eigen::VectorXd stepCorrection_ ;
    Eigen::VectorXd oldDisplacementStepCorrection_ ;
    Eigen::VectorXd oldAccelerationStepCorrection_ ;
    Eigen::VectorXd tmpDisplacementStepCorrection_ ;
    Eigen::VectorXd xAsRotationVector_ ;
    Eigen::VectorXd errorWeights_ ;
    Eigen::VectorXd R_ ;
    Eigen::VectorXd Rold_ ;
    Eigen::MatrixXd dFdX_ ;
    Eigen::MatrixXd dFdV_ ;
    Eigen::VectorXd variablesAtI_ ;
    Eigen::MatrixXd Bt_ ;
    Eigen::VectorXd FInt_ ;
    Eigen::VectorXd FExt_ ;
    Eigen::VectorXd FDyn_ ;
    Eigen::VectorXd FIntOld_ ;
    Eigen::VectorXd FExtOld_ ;
    Eigen::VectorXd FDynOld_ ;
    Eigen::MatrixXd M_ ;
    double normLoads_ ;
    double Rnorm_ ;
    bool absConvergenceCriteria_ ;
    bool relConvergenceCriteria_ ;
    bool firstTry_ ;
    bool FOldInitialized_ ;
    std::string errorMessage_ ;
    BV::Math::StateIndexer spinStateIndexer_ ;
    double error_ ;
    bool firstIteration_ ;

    void setupStep_(const double h,
                    const Eigen::Ref<const Eigen::VectorXd> & oldX,
                    const Eigen::Ref<const Eigen::VectorXd> & oldV,
                    const Eigen::Ref<const Eigen::VectorXd> & oldA,
                    Eigen::Ref<Eigen::VectorXd> newX,
                    Eigen::Ref<Eigen::VectorXd> newV,
                    Eigen::Ref<Eigen::VectorXd> newA) ;

    void setTsInvT_(const Eigen::Ref<const Eigen::Vector3d> & rotVec,
                    Eigen::Ref<Eigen::Matrix3d> mat) ;

    void setTsT_(const Eigen::Ref<const Eigen::Vector3d> & rotVec,
                 Eigen::Ref<Eigen::Matrix3d> mat) ;

    void updateBtp_() ;
    void updateBta_() ;

    void setLambda_(const Eigen::Ref<const Eigen::Vector3d> & rotV,
                    Eigen::Ref<Eigen::Matrix3d> lambda) ;

    void updateVelAcc_(double h,
                       Eigen::Ref<Eigen::VectorXd> newV,
                       Eigen::Ref<Eigen::VectorXd> newA,
                       const Eigen::Ref<const Eigen::VectorXd> & oldV,
                       const Eigen::Ref<const Eigen::VectorXd> & oldA,
                       const Eigen::Ref<const Eigen::VectorXd> & stepCorrection) ;

    void orientationsToRotationVector_(const Eigen::Ref<const Eigen::VectorXd> & pos,
                                       Eigen::Ref<Eigen::VectorXd> posConverted) ;

    void displacementIncrement_(double h,
                                const Eigen::Ref<const Eigen::VectorXd> & oldX,
                                const Eigen::Ref<const Eigen::VectorXd> & oldV,
                                const Eigen::Ref<const Eigen::VectorXd> & oldA,
                                Eigen::Ref<Eigen::VectorXd> & newX,
                                Eigen::Ref<Eigen::VectorXd> & newV,
                                Eigen::Ref<Eigen::VectorXd> & newA,
                                const Eigen::Ref<const Eigen::VectorXd> & iterationCorrection,
                                Eigen::Ref<Eigen::VectorXd> stepCorrection) ;

    void accelerationIncrement_(double h,
                                const Eigen::Ref<const Eigen::VectorXd> & oldX,
                                const Eigen::Ref<const Eigen::VectorXd> & oldV,
                                const Eigen::Ref<const Eigen::VectorXd> & oldA,
                                Eigen::Ref<Eigen::VectorXd> & newX,
                                Eigen::Ref<Eigen::VectorXd> & newV,
                                Eigen::Ref<Eigen::VectorXd> & newA,
                                const Eigen::Ref<const Eigen::VectorXd> & iterationCorrection,
                                Eigen::Ref<Eigen::VectorXd> stepCorrection) ;

    bool increment_(const double h,
                    const Eigen::Ref<const Eigen::VectorXd> & oldX,
                    const Eigen::Ref<const Eigen::VectorXd> & oldV,
                    const Eigen::Ref<const Eigen::VectorXd> & oldA,
                    Eigen::Ref<Eigen::VectorXd> newX,
                    Eigen::Ref<Eigen::VectorXd> newV,
                    Eigen::Ref<Eigen::VectorXd> newA) ;

    void calcErrorWeights_(const Eigen::VectorXd & x, double rtol, double atol,
                           Eigen::VectorXd & ewt) ;

    double wrmsNorm_(const Eigen::VectorXd & values,
                     const Eigen::VectorXd & weights) ;

    bool checkConvergence_(const Eigen::Ref<const Eigen::VectorXd> & oldX,
                           const Eigen::Ref<const Eigen::VectorXd> & oldV,
                           const Eigen::Ref<const Eigen::VectorXd> & oldA,
                           const Eigen::Ref<const Eigen::VectorXd> & newX,
                           const Eigen::Ref<const Eigen::VectorXd> & newV,
                           const Eigen::Ref<const Eigen::VectorXd> & newA) ;


    void setDefaultParams_() ;

public:

    HHT(IntegrableABC & integrable) ;
    HHT() ;

    virtual ~HHT() {}

    void setMode(HHT_MODE mode) ;

    void setAlpha(double alpha) ;

    void setIncrementTolerances(const double absTol, const double relTol) ;

    void setResidualTolerance(const double absTol) ;

    void setLoadsTolerance(const double relTol) ;

    void setMaxIterations(std::size_t maxIterations) ;

    void setMaxNIterationsBeforeStepIncrease(std::size_t val) ;

    void setNSuccessfulStepsBeforeStepIncrease(std::size_t val) ;

    void setErrorThresholdBeforeStepIncrease(double val) ;

    void setNIterationsThresholdBeforeStepDecrease(std::size_t val) ;

    void setPredictionMode(HHTPredictionMode predictionMode) ;

    void setSolverType(SolverType solverType) ;

    void setup(const double t) override ;

    void advance(const double step, bool updateStateEachStep=false) override ;

    bool doStep(const double t, const double dt,
                const Eigen::Ref<const Eigen::VectorXd> & oldX,
                const Eigen::Ref<const Eigen::VectorXd> & oldV,
                const Eigen::Ref<const Eigen::VectorXd> & oldA,
                Eigen::Ref<Eigen::VectorXd> newX,
                Eigen::Ref<Eigen::VectorXd> newV,
                Eigen::Ref<Eigen::VectorXd> newA) override ;

    bool tryStep(double & dt, bool updateStateEachStep=false) override ;

} ;

} // End of namespace Details
} // End of namespace Steppers
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
