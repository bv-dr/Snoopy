#include "Math/Integration/ODE/Steppers/Dopri5.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

Dopri5::Dopri5(IntegrableABC & integrable) :
    FirstOrderStepperABC(integrable),
    a2_(1./ 5.),
    a3_(3./ 10.),
    a4_(4./ 5.),
    a5_(8./ 9.),
    b21_(1./5.),
    b31_(3./40.),
    b32_(9./40.),
    b41_(44./45.),
    b42_(-56./15.),
    b43_(32./9.),
    b51_(19372./6561.),
    b52_(-25360./2187.),
    b53_(64448./6561.),
    b54_(-212./729.),
    b61_(9017./3168.),
    b62_(-355./33.),
    b63_(46732./5247.),
    b64_(49./176.),
    b65_(-5103./18656.),
    c1_(35./384.),
    c3_(500./1113.),
    c4_(125./192.),
    c5_(-2187./6784.),
    c6_(11./84.),
    dc1_(5179./57600.),
    dc3_(7571./16695.),
    dc4_(393./640.),
    dc5_(-92097./339200.),
    dc6_(187./2100.),
    dc7_(1./40.),
    firstTry_(true)
{
}

Dopri5::Dopri5() :
    FirstOrderStepperABC(),
    a2_(1./ 5.),
    a3_(3./ 10.),
    a4_(4./ 5.),
    a5_(8./ 9.),
    b21_(1./5.),
    b31_(3./40.),
    b32_(9./40.),
    b41_(44./45.),
    b42_(-56./15.),
    b43_(32./9.),
    b51_(19372./6561.),
    b52_(-25360./2187.),
    b53_(64448./6561.),
    b54_(-212./729.),
    b61_(9017./3168.),
    b62_(-355./33.),
    b63_(46732./5247.),
    b64_(49./176.),
    b65_(-5103./18656.),
    c1_(35./384.),
    c3_(500./1113.),
    c4_(125./192.),
    c5_(-2187./6784.),
    c6_(11./84.),
    dc1_(5179./57600.),
    dc3_(7571./16695.),
    dc4_(393./640.),
    dc5_(-92097./339200.),
    dc6_(187./2100.),
    dc7_(1./40.),
    firstTry_(true)
{
}

void Dopri5::setup(const double t)
{
    FirstOrderStepperABC::setup(t) ;
    std::size_t nY(p_integrable_->getNState()) ;
    std::size_t nDY(p_integrable_->getNStateDerivative()) ;
    k2_ = StateDerivative(nDY) ;
    k3_ = StateDerivative(nDY) ;
    k4_ = StateDerivative(nDY) ;
    k5_ = StateDerivative(nDY) ;
    k6_ = StateDerivative(nDY) ;
    yNew_ = State(nY) ;
    yNew_.setIndexer(stateIndexer_) ;
    yNew2_ = State(nY) ;
    yNew2_.setIndexer(stateIndexer_) ;
    dyNew_ = StateDerivative(nDY) ;
    error_ = State(nY) ;
    error_.setIndexer(stateIndexer_) ;
    relError_ = State(nY) ;
    relError_.setIndexer(stateIndexer_) ;
    tmpError_ = State(nY) ;
    tmpError_.setIndexer(stateIndexer_) ;
    firstTry_ = true ;
}

void Dopri5::doStep(const double t, const double dt,
                    const State & oldState,
                    const StateDerivative & oldStateDer,
                    State & newState,
                    StateDerivative & newStateDer)
{
    newState = oldState + dt*b21_*oldStateDer ;
    p_integrable_->computeStateDerivative(t+dt*a2_, newState, k2_) ;

    newState = oldState + dt*(b31_*oldStateDer + b32_*k2_) ;
    p_integrable_->computeStateDerivative(t+dt*a3_, newState, k3_) ;

    newState = oldState + dt*(b41_*oldStateDer + b42_*k2_ + b43_*k3_) ;
    p_integrable_->computeStateDerivative(t+dt*a4_, newState, k4_) ;

    newState = oldState + dt*(b51_*oldStateDer + b52_*k2_ + b53_*k3_ + b54_*k4_) ;
    p_integrable_->computeStateDerivative(t+dt*a5_, newState, k5_) ;

    newState = oldState + dt*(b61_*oldStateDer + b62_*k2_ + b63_*k3_ + b64_*k4_ + b65_*k5_) ;
    p_integrable_->computeStateDerivative(t+dt, newState, k6_) ;

    newState = oldState + dt*(c1_*oldStateDer + c3_*k3_ + c4_*k4_ + c5_*k5_ + c6_*k6_) ;
    p_integrable_->computeStateDerivative(t+dt, newState, newStateDer) ;
}

void Dopri5::advance(const double dt, bool updateStateEachStep)
{
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
    }

    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;

    y_ = yNew_ ;
    dy_ = dyNew_ ;
    currentTime_ += dt ;
    // We need to update state for results to be output correctly
    p_integrable_->updateState(currentTime_, y_) ;
    p_integrable_->updateStateDerivative(currentTime_, dy_) ;
}

bool Dopri5::tryStep(double & dt, bool updateStateEachStep)
{
    if (updateStateEachStep && firstTry_)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
        firstTry_ = false ;
    }

    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;

    yNew2_ = y_ + dt * (dc1_*dy_ +  k3_*dc3_ + k4_*dc4_ + k5_*dc5_ + k6_*dc6_ + dyNew_*dc7_) ;
    tmpError_ = scaleYError_ * y_ + scaleDYError_ * dt * dy_ ;
    error_ = Eigen::VectorXd::Constant(y_.size(), absTol_) + tmpError_ * relTol_ ;
    relError_ = State((yNew_-yNew2_).array().cwiseQuotient(error_.array()).matrix()) ;
    relError_.setIndexer(stateIndexer_) ;
    double error(relError_.normInf()) ;
    if (error <= 1.)
    {
        // FIXME should we add the error to be more precise?
        y_ = yNew_ ;
        dy_ = dyNew_ ;
        currentTime_ += dt ;
        p_integrable_->updateState(currentTime_, y_) ;
        p_integrable_->updateStateDerivative(currentTime_, dy_) ;
        increaseStep_(dt, error) ;
        firstTry_ = true ;
        return true ;
    }
    decreaseStep_(dt, error) ;
    return false ;
}

void Dopri5::increaseStep_(double & dt, double error) const
{
    double errorOrder(4.) ;
    if (error < 0.5)
    {
        error = std::max(std::pow(5., -errorOrder), error) ;
        dt *= 9./10. * std::pow(error, (-1/errorOrder)) ;
    }
}

void Dopri5::decreaseStep_(double & dt, const double error) const
{
    double errorOrder(4) ;
    dt *= std::max(9./10. * std::pow(error, -1./(errorOrder-1.)), 1./5.) ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
