#pragma once

#include <Eigen/Dense>

#include "Math/Integration/ODE/Steppers/StepperABC.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

class HalfStepErrorEstimator
{
private:
    FirstOrderStepperABC * p_stepper_ ;
    std::size_t order_ ;
    State y1_ ;
    State y1_2_ ;
    State y2_ ;
    StateDerivative tmpDy_ ;
    StateDerivative tmpDy2_ ;
    State relError_ ;
public:
    HalfStepErrorEstimator() ;

    void setStepper(FirstOrderStepperABC * p_stepper,
                    std::size_t nState, std::size_t nStateDer,
                    std::size_t order) ;

    bool estimate(const double t, double & dt,
                  const State & oldState,
                  const StateDerivative & oldStateDer,
                  State & newState,
                  StateDerivative & newStateDer,
                  const double tolerance) ;
} ;

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
