#pragma once

#include "IntegrationExport.hpp"

#include <memory>

#include "Tools/BVException.hpp"
#include "Math/Integration/ODE/Integrable.hpp"
#include "Math/State.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

enum class StepperType {EULER, RK2, RK4, HPCG, ERK_DOPRI5, HHT} ;

struct StepperException : public BV::Tools::Exceptions::BVException
{
    StepperException(std::string message) : BVException(message)
    {
    }
} ;

class INTEGRATION_API ObserverABC
{
private:
    bool hasFinished_ ;
    std::size_t nSteps_ ;
    bool raisedException_ ;
    std::string errorMessage_ ;
    std::vector<double> steps_ ;
    std::vector<double> stepSize_ ;
    std::vector<std::size_t> iterations_ ;
    Eigen::VectorXd lastState_ ;
    Eigen::VectorXd lastStateDerivative_ ;
    Eigen::VectorXd lastPos_ ;
    Eigen::VectorXd lastVel_ ;
    Eigen::VectorXd lastAcc_ ;

public:
    ObserverABC() ;
    virtual ~ObserverABC() {}

    virtual void simulationInformation(double xStart, double xEnd,
                                       double initStep) ;

    virtual void state(double t,
                       const Eigen::Ref<const Eigen::VectorXd> & state) = 0 ;

    virtual void stateDerivative(double t,
                                 const Eigen::Ref<const Eigen::VectorXd> & der) = 0 ;

    virtual void posVel(double t,
                        const Eigen::Ref<const Eigen::VectorXd> & pos,
                        const Eigen::Ref<const Eigen::VectorXd> & vel) = 0 ;

    virtual void acc(double t,
                     const Eigen::Ref<const Eigen::VectorXd> & acc) = 0 ;

    void addStep(const double & step) ;

    void addStepSize(const double & stepSize) ;

    void addNIterations(const std::size_t & nIterations) ;

    const std::vector<double> & getSteps() const ;
    const std::vector<double> & getStepsSize() const ;
    const std::vector<std::size_t> & getNIterations() const ;

    void setLastState(const Eigen::Ref<const Eigen::VectorXd> & state) ;
    void setLastStateDerivative(const Eigen::Ref<const Eigen::VectorXd> & stateDer) ;
    void setLastPos(const Eigen::Ref<const Eigen::VectorXd> & pos) ;
    void setLastVel(const Eigen::Ref<const Eigen::VectorXd> & vel) ;
    void setLastAcc(const Eigen::Ref<const Eigen::VectorXd> & acc) ;

    const Eigen::VectorXd & getLastState() const ;
    const Eigen::VectorXd & getLastStateDerivative() const ;
    const Eigen::VectorXd & getLastPos() const ;
    const Eigen::VectorXd & getLastVel() const ;
    const Eigen::VectorXd & getLastAcc() const ;

    bool hasFinished() const ;

    void setHasFinished(const bool & val) ;

    std::size_t nIntegrationSteps() const ;

    void setNIntegrationSteps(const std::size_t & nSteps) ;

    bool hasRaisedException() const ;

    const std::string & getException() const ;

    void setException(const std::exception & e) ;

    bool isLastStateFirstOrder() const ;
    bool isLastStateSecondOrder() const ;
} ;

// TODO dense output ?
class INTEGRATION_API StepperABC
{
protected:
    IntegrableABC * p_integrable_ ;
    double currentTime_ ;
    // Adaptive step parameters
    bool useAdaptiveStep_ ;
    double minStep_ ;
    double maxStep_ ;
    double absTol_ ;
    double relTol_ ;
    double scaleYError_ ;
    double scaleDYError_ ;
    std::size_t maxIterations_ ;
    std::size_t nIterations_ ;
    StateIndexer stateIndexer_ ;

public:
    StepperABC(IntegrableABC & integrable) ;
    StepperABC() ;

    virtual ~StepperABC() {}

    virtual void setIntegrable(IntegrableABC & integrable) ;

    virtual void setAdaptiveStepParameters(const bool useAdaptiveStep,
                                           const double minStep,
                                           const double maxStep,
                                           const double absTol,
                                           const double relTol,
                                           const double scaleYError,
                                           const double scaleDYError,
                                           const double maxIterations) ;

    bool useAdaptiveStep() const ;

    void adjustStep(double & step) const ;

    virtual void failCheck() const ;

    void failCheck(std::size_t it) const ;

    virtual void setup(const double t) = 0 ;

    virtual void advance(const double dt, bool updateStateEachStep=false) = 0 ;

    virtual bool tryStep(double & dt,
                         bool updateStateEachStep=false) = 0 ;

    double getTime() ;

    virtual void observe(ObserverABC & observer) const = 0 ;

    virtual void observeLastStep(ObserverABC & observer) const = 0 ;

    virtual std::size_t getNIterations() const ;

} ;

// TODO First and second order base classes? Explicit/Implicit?

class INTEGRATION_API FirstOrderStepperABC : public StepperABC
{
protected:
    State y_ ;
    StateDerivative dy_ ;

    ~FirstOrderStepperABC() {}

public:
    FirstOrderStepperABC(IntegrableABC & integrable) ;
    FirstOrderStepperABC() ;

    const State & getY() const ;
    const StateDerivative & getDY() const ;

    void setup(const double t) override ; // FIXME add stepper parameters ?

    virtual void doStep(const double t, const double dt,
                        const State & oldState,
                        const StateDerivative & oldStateDer,
                        State & newState,
                        StateDerivative & newStateDer) = 0 ;

    void failCheck() const override ;

    virtual void observe(ObserverABC & observer) const override ;

    virtual void observeLastStep(ObserverABC & observer) const override ;
} ;

class INTEGRATION_API SecondOrderStepperABC : public StepperABC
{
protected:
    Eigen::VectorXd x_ ;
    Eigen::VectorXd v_ ;
    Eigen::VectorXd a_ ;

    ~SecondOrderStepperABC() {}

public:
    SecondOrderStepperABC(IntegrableABC & integrable) ;
    SecondOrderStepperABC() ;

    void setup(const double t) override ;

    virtual bool doStep(const double t, const double dt,
                        const Eigen::Ref<const Eigen::VectorXd> & oldX,
                        const Eigen::Ref<const Eigen::VectorXd> & oldV,
                        const Eigen::Ref<const Eigen::VectorXd> & oldA,
                        Eigen::Ref<Eigen::VectorXd> newX,
                        Eigen::Ref<Eigen::VectorXd> newV,
                        Eigen::Ref<Eigen::VectorXd> newA) = 0 ;

    void failCheck() const override ;

    virtual void observe(ObserverABC & observer) const override ;

    virtual void observeLastStep(ObserverABC & observer) const override ;
} ;

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
