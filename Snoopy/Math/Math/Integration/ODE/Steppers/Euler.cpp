#include "Math/Integration/ODE/Steppers/Euler.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

Euler::Euler(IntegrableABC & integrable) :
    FirstOrderStepperABC(integrable),
    firstTry_(true)
{
}

Euler::Euler() :
    FirstOrderStepperABC()
{
}

void Euler::setup(const double t)
{
    FirstOrderStepperABC::setup(t) ;
    std::size_t nY(p_integrable_->getNState()) ;
    std::size_t nDY(p_integrable_->getNStateDerivative()) ;
    yNew_ =  State(nY) ;
    dyNew_ = StateDerivative(nDY) ;
    errorEstimator_.setStepper(this, nY, nDY, 1) ;
    firstTry_ = true ;
}

void Euler::doStep(const double t, const double dt,
                   const State & oldState,
                   const StateDerivative & oldStateDer,
                   State & newState,
                   StateDerivative & newStateDer)
{
    newState = oldState + dt*oldStateDer ;
    p_integrable_->computeStateDerivative(t+dt, newState, newStateDer) ;
}

void Euler::advance(const double dt, bool updateStateEachStep)
{
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
    }
    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;
    y_ = yNew_ ;
    dy_ = dyNew_ ;
    currentTime_ += dt ;
    p_integrable_->updateState(currentTime_, y_) ;
    p_integrable_->updateStateDerivative(currentTime_, dy_) ;
}

bool Euler::tryStep(double & dt, bool updateStateEachStep)
{
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
        firstTry_ = false ;
    }
    double currentDt(dt) ; // Error estimate may change dt
    bool isOK(errorEstimator_.estimate(currentTime_, dt, y_, dy_, yNew_, dyNew_, relTol_)) ;
    if (isOK)
    {
        y_ = yNew_ ;
        dy_ = dyNew_ ;
        currentTime_ += currentDt ;
        p_integrable_->updateState(currentTime_, y_) ;
        p_integrable_->updateStateDerivative(currentTime_, dy_) ;
        firstTry_ = true ;
    }
    return isOK ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
