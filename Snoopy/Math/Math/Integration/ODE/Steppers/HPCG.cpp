#include "Math/Integration/ODE/Steppers/HPCG.hpp"
#include "Math/Tools.hpp"
#include "Geometry/Rotation/Quaternion.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

HPCG::HPCG() :
    FirstOrderStepperABC(),
    nStepsHistory_(0),
    rk4_(),
    iStep_(0),
    errorInitialized_(false),
    firstTry_(true),
    updateStateEachStep_(false)
{
}

HPCG::HPCG(IntegrableABC & integrable) :
    FirstOrderStepperABC(integrable),
    nStepsHistory_(0),
    rk4_(integrable),
    iStep_(0),
    errorInitialized_(false),
    firstTry_(true),
    updateStateEachStep_(false)
{
}

void HPCG::setIntegrable(IntegrableABC & integrable)
{
    FirstOrderStepperABC::setIntegrable(integrable) ;
    rk4_.setIntegrable(integrable) ;
}

void HPCG::setup(const double t)
{
    FirstOrderStepperABC::setup(t) ;
    std::size_t nY(p_integrable_->getNState()) ;
    std::size_t nDY(p_integrable_->getNStateDerivative()) ;
    nStepsHistory_ = 0 ;
    yHistory_ = StepsHistory<State>(7) ;
    dYHistory_ = StepsHistory<StateDerivative>(7) ;
    rk4_.setup(t) ;
    statePrediction_ = State(nY) ;
    statePrediction_.setIndexer(stateIndexer_) ;
    stateModification_ = State(nY) ;
    stateModification_.setIndexer(stateIndexer_) ;
    stateCorrection_ = State(nY) ;
    stateCorrection_.setIndexer(stateIndexer_) ;
    error_ = State(nY) ;
    error_.setIndexer(stateIndexer_) ;
    yNew_ = State(nY) ;
    yNew_.setIndexer(stateIndexer_) ;
    yTmp_ = State(nY) ;
    yTmp_.setIndexer(stateIndexer_) ;
    dyNew_ = StateDerivative(nDY) ;
    errorInitialized_ = false ;
    firstTry_ = true ;
}

void HPCG::doStep(const double t, const double dt,
                  const State & oldState,
                  const StateDerivative & oldStateDer,
                  State & newState,
                  StateDerivative & newStateDer)
{
    // We need 4 history steps before starting the HPCG
    // first one is initial condition, then we need 3 steps with RK4
    // to start the HPCG
    if (nStepsHistory_ < 3)
    {
        rk4_.advance(dt, updateStateEachStep_) ;
        newState = rk4_.getY() ;
        newStateDer = rk4_.getDY() ;
        iStep_ = 3 ; // Initialization for adaptive HPCG
    }
    else
    {
        // Prediction
        statePrediction_ = yHistory_.getStep(3)
                           + 4.*dt/3. * (2. * dYHistory_.getStep(0)
                                         - dYHistory_.getStep(1)
                                         + 2. * dYHistory_.getStep(2)) ;
        // Modification
        if (errorInitialized_)
        {
            stateModification_ = statePrediction_ - 112./121. * error_ ;
        }
        else
        {
            stateModification_ = statePrediction_ ;
            errorInitialized_ = true ;
        }
        // Correction
        p_integrable_->computeStateDerivative(currentTime_+dt,
                                              stateModification_,
                                              newStateDer) ;
        stateCorrection_ = (oldState + 1./8.* (-yHistory_.getStep(2) + oldState))
                           + 3.*dt/8. * (newStateDer
                                         - dYHistory_.getStep(1)
                                         + 2. * dYHistory_.getStep(0)) ;
        error_ = statePrediction_ - stateCorrection_ ;

        // Final estimate
        newState = stateCorrection_ + 9./121. * error_ ;
        p_integrable_->computeStateDerivative(currentTime_+dt, newState, newStateDer) ;
    }
}

void HPCG::advance(const double dt, bool updateStateEachStep)
{
    updateStateEachStep_ = updateStateEachStep ;
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
    }
    yHistory_.addStep(y_) ;
    dYHistory_.addStep(dy_) ;
    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;
    yHistory_.readyForNextStep() ;
    dYHistory_.readyForNextStep() ;
    if (nStepsHistory_< 7)
    {
        ++nStepsHistory_ ;
    }
    y_ = yNew_ ;
    dy_ = dyNew_ ;
    currentTime_ += dt ;
    p_integrable_->updateState(currentTime_, y_) ;
    p_integrable_->updateStateDerivative(currentTime_, dy_) ;
}

bool HPCG::tryStep(double & dt, bool updateStateEachStep)
{
    if (updateStateEachStep && firstTry_)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case state has changed we have to compute its derivative again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
        firstTry_ = false ;
    }
    // TODO original HPCG performed a dichotomy before starting to
    // make sure the time step was correct
    yHistory_.addStep(y_) ;
    dYHistory_.addStep(dy_) ;
    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;
    //double delt(error_.cwiseAbs().sum()) ; // FIXME originally we summed all elements
    double delt(error_.normInf()) ;
    if ((nStepsHistory_ > 3) && (delt >= relTol_))
    {
        decreaseStep_(dt, delt) ;
        return false ;
    }
    ++iStep_ ;
    if (nStepsHistory_< 7)
    {
        ++nStepsHistory_ ;
    }
    y_ = yNew_ ;
    dy_ = dyNew_ ;
    currentTime_ += dt ;
    p_integrable_->updateState(currentTime_, y_) ;
    p_integrable_->updateStateDerivative(currentTime_, dy_) ;
    firstTry_ = true ;
    if (delt < relTol_*0.2) // FIXME 0.2 arbitrary
    {
        increaseStep_(dt, delt) ;
    }
    yHistory_.readyForNextStep() ;
    dYHistory_.readyForNextStep() ;
    return true ;
}

void HPCG::decreaseStep_(double & dt, const double error)
{
    if (nStepsHistory_ < 4)
    {
        return ;
    }
    dt *= 0.5 ;
    // FIXME not factorizing the factor as rotations don't like extrapolation
    // and because operators priorities we would do some...
    yNew_ = 0.00390625 * 80. * yHistory_.getStep(0)
            + 0.00390625 * 135. * yHistory_.getStep(1)
            + 0.00390625 * 40. * yHistory_.getStep(2)
            + 0.00390625 * yHistory_.getStep(3)
            - dt * 0.1171875 * (dYHistory_.getStep(0)
                                - 6. * dYHistory_.getStep(1)
                                - dYHistory_.getStep(2)) ;
    yTmp_ = 0.00390625 * 12. * yHistory_.getStep(0)
            + 0.00390625 * 135. * yHistory_.getStep(1)
            + 0.00390625 * 108. * yHistory_.getStep(2)
            + 0.00390625 * yHistory_.getStep(3)
            - dt * 0.0234375 * (dYHistory_.getStep(0)
                                + 18. * dYHistory_.getStep(1)
                                - 9. * dYHistory_.getStep(2)) ;
    yHistory_.setStep(3, yTmp_) ;
    yHistory_.setStep(2, yHistory_.getStep(1)) ;
    dYHistory_.setStep(2, dYHistory_.getStep(1)) ;
    p_integrable_->computeStateDerivative(currentTime_-3*dt, yNew_, dyNew_) ;
    yHistory_.setStep(1, yNew_) ;
    dYHistory_.setStep(1, dyNew_) ;
    yNew_ = yHistory_.getStep(3) ;
    p_integrable_->computeStateDerivative(currentTime_-5*dt, yNew_, dyNew_) ;
    error_ = 8.962963 * (yHistory_.getStep(0) - yNew_)
             - dt * 3.361111 * (dYHistory_.getStep(0)
                                + 3. * (dYHistory_.getStep(1)
                                        + dYHistory_.getStep(2))
                                + dyNew_) ;
    dYHistory_.setStep(3, dyNew_) ;
    iStep_ = 0 ;
}

void HPCG::increaseStep_(double & dt, const double error)
{
    if ((nStepsHistory_ >= 6)
        && (iStep_ >= 4)
        && ((iStep_ % 2) == 0))
    {
        dt *= 2. ;
        yHistory_.setStep(0, yHistory_.getStep(1)) ;
        yHistory_.setStep(1, yHistory_.getStep(3)) ;
        yHistory_.setStep(2, yHistory_.getStep(5)) ;
        dYHistory_.setStep(0, dYHistory_.getStep(1)) ;
        dYHistory_.setStep(1, dYHistory_.getStep(3)) ;
        dYHistory_.setStep(2, dYHistory_.getStep(5)) ;
        error_ = 8.962963 * (y_ - yHistory_.getStep(3))
                 - dt * 3.361111 * (dy_
                                    + 3. * (dYHistory_.getStep(1)
                                            + dYHistory_.getStep(2))
                                    + dYHistory_.getStep(3)) ;
        iStep_ = 0 ;
    }
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
