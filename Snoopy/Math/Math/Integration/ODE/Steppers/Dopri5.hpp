#pragma once

#include "IntegrationExport.hpp"

#include "Math/Integration/ODE/Steppers/StepperABC.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

class INTEGRATION_API Dopri5 : public FirstOrderStepperABC
{
private:
    double a2_ ;
    double a3_ ;
    double a4_ ;
    double a5_ ;
    double b21_ ;
    double b31_ ;
    double b32_ ;
    double b41_ ;
    double b42_ ;
    double b43_ ;
    double b51_ ;
    double b52_ ;
    double b53_ ;
    double b54_ ;
    double b61_ ;
    double b62_ ;
    double b63_ ;
    double b64_ ;
    double b65_ ;
    double c1_ ;
    double c3_ ;
    double c4_ ;
    double c5_ ;
    double c6_ ;
    double dc1_ ;
    double dc3_ ;
    double dc4_ ;
    double dc5_ ;
    double dc6_ ;
    double dc7_ ;
    StateDerivative k2_ ;
    StateDerivative k3_ ;
    StateDerivative k4_ ;
    StateDerivative k5_ ;
    StateDerivative k6_ ;

    State yNew_ ;
    State yNew2_ ;
    StateDerivative dyNew_ ;
    State error_ ;
    State relError_ ;
    State tmpError_ ;
    bool firstTry_ ;

    void decreaseStep_(double & dt, const double error) const ;
    void increaseStep_(double & dt, double error) const ;
public:
    Dopri5(IntegrableABC & integrable) ;
    Dopri5() ;

    void setup(const double t) override ;

    void doStep(const double t, const double dt,
                const State & oldState,
                const StateDerivative & oldStateDer,
                State & newState,
                StateDerivative & newStateDer) override ;

    void advance(const double dt, bool updateStateEachStep=false) override ;

    bool tryStep(double & dt, bool updateStateEachStep=false) override ;

} ;

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
