#include "Math/Integration/ODE/Steppers/RK4.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

RK4::RK4(IntegrableABC & integrable) :
    FirstOrderStepperABC(integrable),
    firstTry_(true)
{
}

RK4::RK4() :
    FirstOrderStepperABC(),
    firstTry_(true)
{
}

void RK4::setup(const double t)
{
    FirstOrderStepperABC::setup(t) ;
    std::size_t nY(p_integrable_->getNState()) ;
    std::size_t nDY(p_integrable_->getNStateDerivative()) ;
    k2_ = StateDerivative(nDY) ;
    k3_ = StateDerivative(nDY) ;
    k4_ = StateDerivative(nDY) ;
    yNew_ = State(nY) ;
    yNew_.setIndexer(stateIndexer_) ;
    dyNew_ = StateDerivative(nDY) ;
    errorEstimator_.setStepper(this, nY, nDY, 4) ;
    firstTry_ = true ;
}

void RK4::doStep(const double t, const double dt,
                 const State & oldState,
                 const StateDerivative & oldStateDer,
                 State & newState,
                 StateDerivative & newStateDer)
{
    newState = oldState + 0.5 * dt * oldStateDer ;
    p_integrable_->computeStateDerivative(t+0.5*dt, newState, k2_) ;
    newState = oldState + 0.5 * dt * k2_ ;
    p_integrable_->computeStateDerivative(t+0.5*dt, newState, k3_) ;
    newState = oldState + dt * k3_ ;
    p_integrable_->computeStateDerivative(t+dt, newState, k4_) ;
    newState = oldState + dt/6.*(oldStateDer + 2.*k2_ + 2.*k3_ + k4_) ;
    p_integrable_->computeStateDerivative(t+dt, newState, newStateDer) ;
}

void RK4::advance(const double dt, bool updateStateEachStep)
{
    if (updateStateEachStep)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case y_ has changed we should compute dy_ again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
    }
    doStep(currentTime_, dt, y_, dy_, yNew_, dyNew_) ;
    y_ = yNew_ ;
    dy_ = dyNew_ ;
    currentTime_ += dt ;
    p_integrable_->updateState(currentTime_, y_) ;
    p_integrable_->updateStateDerivative(currentTime_, dy_) ;
}

bool RK4::tryStep(double & dt, bool updateStateEachStep)
{
    if (updateStateEachStep && firstTry_)
    {
        p_integrable_->provideState(currentTime_, y_) ;
        // In case y_ has changed we should compute dy_ again...
        p_integrable_->computeStateDerivative(currentTime_, y_, dy_) ;
        firstTry_ = false ;
    }
    double currentDt(dt) ; // Error estimate may change dt
    bool isOK(errorEstimator_.estimate(currentTime_, dt, y_, dy_, yNew_, dyNew_,
                                       relTol_)) ;
    if (isOK)
    {
        y_ = yNew_ ;
        dy_ = dyNew_ ;
        currentTime_ += currentDt ;
        p_integrable_->updateState(currentTime_, y_) ;
        p_integrable_->updateStateDerivative(currentTime_, dy_) ;
        firstTry_ = true ;
    }
    return isOK ;
}

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
