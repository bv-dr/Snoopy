#pragma once

#include "IntegrationExport.hpp"

#include <vector>

#include "Math/Integration/ODE/Steppers/StepperABC.hpp"
#include "Math/Integration/ODE/Steppers/RK4.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

template <typename HistoryType>
class INTEGRATION_API StepsHistory
{
private:
    std::size_t bufferSize_ ;
    std::vector<HistoryType> history_ ;
    std::size_t currentStepIndex_ ;
    std::size_t lastValidStep_ ;
public:
    StepsHistory() : StepsHistory(0)
    {
    }

    StepsHistory(std::size_t bufferSize) :
        bufferSize_(bufferSize),
        history_(bufferSize),
        currentStepIndex_(0),
        lastValidStep_(0)
    {
    }

    void addStep(const HistoryType & values)
    {
        history_[currentStepIndex_] = values ;
    }

    void resetToLastValidStep()
    {
        currentStepIndex_ = lastValidStep_ ;
    }

    void readyForNextStep()
    {
        lastValidStep_ = currentStepIndex_ ;
        ++currentStepIndex_ ;
        if (currentStepIndex_ > (bufferSize_-1))
        {
            currentStepIndex_ = 0 ;
        }
    }

    const HistoryType & getStep(std::size_t iPrevious) const
    {
        // iPrevious means 0 is last added step, 1 is step before and so on...
        // FIXME we don't check iPrevious value is in buffer size because
        // this function should be used only internally
        int iStep(currentStepIndex_-iPrevious) ;
        if (iStep < 0)
        {
            iStep += bufferSize_ ;
        }
        return history_[iStep] ;
    }

    void setStep(std::size_t iPrevious, const HistoryType & values)
    {
        // iPrevious means 0 is last added step, 1 is step before and so on...
        // FIXME we don't check iPrevious value is in buffer size because
        // this function should be used only internally
        int iStep(currentStepIndex_-iPrevious) ;
        if (iStep < 0)
        {
            iStep += bufferSize_ ;
        }
        history_[iStep] = values ;
    }

} ;

class INTEGRATION_API HPCG : public FirstOrderStepperABC
{
private:
    std::size_t nStepsHistory_ ;
    StepsHistory<State> yHistory_ ;
    StepsHistory<StateDerivative> dYHistory_ ;
    RK4 rk4_ ;
    State statePrediction_ ;
    State stateModification_ ;
    State stateCorrection_ ;
    State error_ ;
    State yNew_ ;
    State yTmp_ ;
    StateDerivative dyNew_ ;
    std::size_t iStep_ ;
    bool errorInitialized_ ;
    bool firstTry_ ;
    bool updateStateEachStep_ ;

    void decreaseStep_(double & dt, const double error) ;
    void increaseStep_(double & dt, const double error) ;

public:
    HPCG(IntegrableABC & integrable) ;
    HPCG() ;

    void setIntegrable(IntegrableABC & integrable) override ;

    void setup(const double t) override ;

    void doStep(const double t, const double dt,
                const State & oldState,
                const StateDerivative & oldStateDer,
                State & newState,
                StateDerivative & newStateDer) override ;

    void advance(const double dt, bool updateStateEachStep=false) override ;

    bool tryStep(double & dt, bool updateStateEachStep=false) override ;

} ;

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
