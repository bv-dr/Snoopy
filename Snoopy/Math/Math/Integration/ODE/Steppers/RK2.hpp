#pragma once

#include "IntegrationExport.hpp"

#include "Math/Integration/ODE/Steppers/StepperABC.hpp"
#include "Math/Integration/ODE/Steppers/HalfStepErrorEstimator.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {
namespace Steppers {

class INTEGRATION_API RK2 : public FirstOrderStepperABC
{
private:
    StateDerivative k2_ ;
    State yNew_ ;
    StateDerivative dyNew_ ;
    HalfStepErrorEstimator errorEstimator_ ;
    bool firstTry_ ;

public:
    RK2(IntegrableABC & integrable) ;
    RK2() ;

    void setup(const double t) override ;

    void doStep(const double t, const double dt,
                const State & oldState,
                const StateDerivative & oldStateDer,
                State & newState,
                StateDerivative & newStateDer) override ;

    void advance(const double dt, bool updateStateEachStep=false) override ;

    bool tryStep(double & dt, bool updateStateEachStep=false) override ;

} ;

} // End of namespace Steppers
} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
