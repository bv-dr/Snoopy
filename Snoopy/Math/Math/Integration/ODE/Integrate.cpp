#include "Math/Integration/ODE/Integrate.hpp"
#include "Math/Integration/ODE/Steppers/StepperABC.hpp"
#include "Math/Integration/ODE/Steppers/Euler.hpp"
#include "Math/Integration/ODE/Steppers/RK2.hpp"
#include "Math/Integration/ODE/Steppers/RK4.hpp"
#include "Math/Integration/ODE/Steppers/HPCG.hpp"
#include "Math/Integration/ODE/Steppers/Dopri5.hpp"
#include "Math/Integration/ODE/Steppers/HHT.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {

// TODO time adaptation

std::shared_ptr<Steppers::StepperABC> GetStepper(
                            const Steppers::StepperType stepperType,
                            IntegrableABC * integrable
                                                )
{
    std::shared_ptr<Steppers::StepperABC> p_stepper ;
    if (stepperType == Steppers::StepperType::EULER)
    {
        p_stepper = std::make_shared<Steppers::Euler>() ;
    }
    else if (stepperType == Steppers::StepperType::RK2)
    {
        p_stepper = std::make_shared<Steppers::RK2>() ;
    }
    else if (stepperType == Steppers::StepperType::RK4)
    {
        p_stepper = std::make_shared<Steppers::RK4>() ;
    }
    else if (stepperType == Steppers::StepperType::HPCG)
    {
        p_stepper = std::make_shared<Steppers::HPCG>() ;
    }
    else if (stepperType == Steppers::StepperType::ERK_DOPRI5)
    {
        p_stepper = std::make_shared<Steppers::Dopri5>() ;
    }
    else if (stepperType == Steppers::StepperType::HHT)
    {
        p_stepper = std::make_shared<Steppers::HHT>() ;
    }
    else
    {
        throw BV::Tools::Exceptions::BVException("Unknown stepper type") ;
    }
    if (integrable != nullptr)
    {
        p_stepper->setIntegrable(*integrable) ;
    }
    return p_stepper ;
}

void Integrate(const Steppers::StepperType stepperType,
               IntegrableABC & integrable,
               double xStart, double xEnd, double xStep,
               Steppers::ObserverABC & observer,
               bool updateStateEachStep)
{
    Integrate(GetStepper(stepperType, &integrable),
              xStart, xEnd, xStep, observer, updateStateEachStep) ;
}

void Integrate(std::shared_ptr<Steppers::StepperABC> p_stepper,
               double xStart, double xEnd, double xStep,
               Steppers::ObserverABC & observer,
               bool updateStateEachStep)
{
    p_stepper->setup(xStart) ;
    p_stepper->observe(observer) ;
    observer.addStepSize(0.) ;
    observer.addStep(xStart) ;
    observer.addNIterations(0) ;
    unsigned nSteps(0) ;
    double currentX(xStart) ;
    if (!p_stepper->useAdaptiveStep())
    {
        while (currentX < xEnd-1.e-8)
        {
            if (currentX+xStep > xEnd)
            {
                xStep = xEnd - currentX ;
            }
            observer.addStepSize(xStep) ;
            try
            {
                p_stepper->advance(xStep, updateStateEachStep) ;
                p_stepper->failCheck() ;
                p_stepper->observe(observer) ;
            }
            catch (std::exception & e)
            {
                std::cout << "Error: " << e.what() << std::endl ;
                observer.setHasFinished(false) ;
                observer.setNIntegrationSteps(nSteps) ;
                observer.setException(e) ;
                p_stepper->observeLastStep(observer) ;
                return ;
            }
            currentX = p_stepper->getTime() ;
            ++nSteps ;
            observer.addStep(currentX) ;
            observer.addNIterations(p_stepper->getNIterations()) ;
        }
    }
    else
    {
        while (currentX < xEnd-1.e-8)
        {
            if (currentX+xStep > xEnd)
            {
                xStep = xEnd - currentX ;
            }
            bool stepOK(false) ;
            double stepSize(xStep) ;
            try
            {
                std::size_t it(0) ;
                do
                {
                    stepSize = xStep ;
                    stepOK = p_stepper->tryStep(xStep, updateStateEachStep) ;
                    p_stepper->adjustStep(xStep) ;
                    p_stepper->failCheck(++it) ;
                } while (!stepOK) ;
                p_stepper->observe(observer) ;
            }
            catch (std::exception & e)
            {
                std::cout << "Error: " << e.what() << std::endl ;
                observer.setHasFinished(false) ;
                observer.setNIntegrationSteps(nSteps) ;
                observer.setException(e) ;
                p_stepper->observeLastStep(observer) ;
                return ;
            }
            currentX = p_stepper->getTime() ;
            ++nSteps ;
            observer.addStep(currentX) ;
            observer.addStepSize(stepSize) ;
            observer.addNIterations(p_stepper->getNIterations()) ;
        }
    }
    observer.setHasFinished(true) ;
    observer.setNIntegrationSteps(nSteps) ;
    p_stepper->observeLastStep(observer) ;
}

} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV

