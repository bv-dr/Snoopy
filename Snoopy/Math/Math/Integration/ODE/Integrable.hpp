#pragma once

#include "IntegrationExport.hpp"

#include <Eigen/Dense>
#include <iostream>

#include "Tools/BVException.hpp"
#include "Math/State.hpp"

namespace BV {
namespace Math {
namespace Integration {
namespace ODE {

struct IntegrableException : public BV::Tools::Exceptions::BVException
{
    using BV::Tools::Exceptions::BVException::BVException ;
} ;

class INTEGRATION_API IntegrableABC
{
public:
    virtual ~IntegrableABC() {}

    // TODO find a better naming or way to distinguish what the state is...

    // First order system part
    virtual std::size_t getNState() const = 0 ;

    virtual std::size_t getNStateDerivative() const ;

    virtual void setupState(const double t,
                            Eigen::Ref<Eigen::VectorXd> y,
                            Eigen::Ref<Eigen::VectorXd> dy) ;

    virtual void setRotatorsStateIndices(StateIndexer & stateIndexer,
                                         std::size_t iStartState,
                                         std::size_t iStartStateDer) ;

    virtual void setRotatorsPosVelIndices(StateIndexer & stateIndexer,
                                          std::size_t iStartPos,
                                          std::size_t iStartVel) ;

    virtual void provideState(const double t, Eigen::Ref<Eigen::VectorXd> y) ;

    virtual void provideStateDerivative(const double t,
                                        Eigen::Ref<Eigen::VectorXd> dy) ;

    virtual void updateState(const double t,
                             const Eigen::Ref<const Eigen::VectorXd> & y) ;

    virtual void updateStateDerivative(const double t,
                                       const Eigen::Ref<const Eigen::VectorXd> & dy) ;

    // First order explicit steppers solve function
    virtual void computeStateDerivative(const double t,
                                        const Eigen::Ref<const Eigen::VectorXd> & y,
                                        Eigen::Ref<Eigen::VectorXd> dy) = 0 ;

    // Second order system part
    virtual std::size_t getNStatePos() const ;

    virtual std::size_t getNStateVel() const ;

    virtual std::size_t getNStateAcc() const ;

    virtual void setupState(const double t,
                            Eigen::Ref<Eigen::VectorXd> x,
                            Eigen::Ref<Eigen::VectorXd> v,
                            Eigen::Ref<Eigen::VectorXd> a) ;

    virtual void provideState(const double t,
                              Eigen::Ref<Eigen::VectorXd> x,
                              Eigen::Ref<Eigen::VectorXd> v,
                              Eigen::Ref<Eigen::VectorXd> a) ;

    virtual void updateState(const double t,
                             const Eigen::Ref<const Eigen::VectorXd> & x,
                             const Eigen::Ref<const Eigen::VectorXd> & v,
                             const Eigen::Ref<const Eigen::VectorXd> & a) ;

    virtual void computeDynamicEquationComponents(const double t,
                                                  const Eigen::Ref<const Eigen::VectorXd> & x,
                                                  const Eigen::Ref<const Eigen::VectorXd> & v,
                                                  const Eigen::Ref<const Eigen::VectorXd> & a,
                                                  Eigen::Ref<Eigen::VectorXd> FInt, // Including structural damping
                                                  Eigen::Ref<Eigen::VectorXd> FExt,
                                                  Eigen::Ref<Eigen::VectorXd> FDyn, // (M+Ma) * a + gyroscopic + ...
                                                  Eigen::Ref<Eigen::MatrixXd> M,
                                                  bool computeFInt=true,
                                                  bool computeFExt=true,
                                                  bool computeFDyn=true) ;

    virtual void computeTangentMatrices(const double t,
                                        const Eigen::Ref<const Eigen::VectorXd> & x,
                                        const Eigen::Ref<const Eigen::VectorXd> & v,
                                        const Eigen::Ref<const Eigen::VectorXd> & a,
                                        const Eigen::Ref<const Eigen::VectorXd> & FInt0,
                                        const Eigen::Ref<const Eigen::VectorXd> & FExt0,
                                        const Eigen::Ref<const Eigen::VectorXd> & FDyn0,
                                        double cInt, double cExt, double cDyn,
                                        Eigen::Ref<Eigen::MatrixXd> K, // Var of residual according to pos
                                        Eigen::Ref<Eigen::MatrixXd> C, // Var of residual according to vel
                                        bool KDyn=false, // No dynamic loads computation for pos variation
                                        bool KExt=true, // No external loads computation for pos variation
                                        bool CDyn=false, // No dynamic loads computation for vel variation
                                        bool CInt=false) ; // No internal loads computation for vel variation

} ;

} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace BV
