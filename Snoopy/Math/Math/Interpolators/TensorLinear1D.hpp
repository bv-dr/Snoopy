#pragma once
#include <Eigen/Dense>

namespace BV {
namespace TimeDomain {

/**
 * Template function which calculates the coefficients for the Linear interpolation
 * fox x in [x_n-1, x_n)
 *  a_n = (f_n -f_{n-1}) / (x_n -x_{n-1})
 *  b_n = (f_{n-1} x_n -f_n x_{n-1}) / (x_n -x_{n-1})
 *
 * @param data Tensor from which the linear coefficients must be obtained
 * @param x Array of x_n nodes, which corresponds to the dimension index 'col'
 * @param col dimension index (0-based) of the data which is used to be interpolated
 *
 * return Rank_+1 Tensor with coefficients a_n, b_n to calculate interpolated value on interval
 * Note: although, the coefficients are given for the interval, number of which is less by 1 than number of nodes,
 *  the output still has same dimensions for the first Rank_ dimensions, and 2 for the last
 *
 *  Let index_n = (i_0, i_1, ..., i_{col-1}, n, i_{col+1}, ..., i_{Rank_-1})
 *  and data.dimension() == (N_0, N_1, ..., N_{col-1}, N_{col}, N_{col+1}, ..., N_{Rank_-1}), then
 *  result.dimension() == (N_0, N_1, ..., N_{col-1}, N_{col}, N_{col+1}, ..., N_{Rank_-1}, 2), but
 *  result[index_{N_{col}-1}, *] are not used.
 *
 *  For x in [x_{n}, x_{n+1}] the approximation is calculated as
 *      f(...., x, ....) = result[index_n, 0] + x *result[index_n, 1]
 *   for n = 0, 1, 2, ..., N_{col}-2
 *
 *  Test
 *  In order to visualize the data in gnuplot, use
 *  ind=0; splot "test.dat" i ind u 2:3:4 w l, "test_app_x.dat" i ind u 2:3:4 w l, "test_app_y.dat" i ind u 2:3:4 w l
 *  where ind = 0, 1, 2 (N0-1)
 *  test function is sin(x_j +i*pi/(N0-1))*cos(y_k)
 *
    Eigen::Index N0(3), N1(20), N2(5);
    Eigen::Tensor<double, 3> test(N0, N1, N2);
    Eigen::ArrayXd x(test.dimension(1));
    Eigen::ArrayXd y(test.dimension(2));
    // x vector
    for(Eigen::Index i = 0; i < test.dimension(1); i++)
        x[i] = i*2.*M_PI/(N1-1);
    // y vector
    for(Eigen::Index i = 0; i < test.dimension(2); i++)
        y[i] = i*2.*M_PI/(N2-1);
    // tensor
    for(Eigen::Index i = 0; i < N0; i++)
        for(Eigen::Index j = 0; j < N1; j++)
            for(Eigen::Index k = 0; k < N2; k++)
                test(i, j, k) = std::sin(x[j] +i*M_PI/(N0-1)) *std::cos(y[k]);  // sin(x +phi_0)*cos(k*2*M_PI/(N2-1))

    // Input data visualisation:
    std::ofstream out_base("test.dat");
    for(Eigen::Index i = 0; i < N0; i++)
    {
        for(Eigen::Index j = 0; j < N1; j++)
        {
            for(Eigen::Index k = 0; k < N2; k++)
                out_base << i << "\t" << x[j] << "\t" << k*2.*M_PI/(N2-1) << "\t"
                    << "\t" << test(i, j, k) << "\n";
            out_base << "\n";
        }
        out_base << "\n";
    }
    out_base.close();

    Eigen::Tensor<double, 4> r = GetLinearCoeffs(test, x, 1);

    // output approximations:
    Eigen::Index M(10); // M points per interval
    std::ofstream out_approx("test_app.dat");
    for(Eigen::Index i = 0; i < N0; i++)
    {
        for(Eigen::Index j = 0; j < N1-1; j++)
        {
            for(Eigen::Index jj = 0; jj < M; jj++)
            {
                double x0(x[j] +jj*(x[j+1]-x[j])/(M-1));
                for(Eigen::Index k = 0; k < N2; k++)
                    out_approx << i << "\t" << x0 << "\t" << y[k] << "\t"
                        << "\t" << r(0, i, j, k) +r(1, i, j, k) *x0 << "\n";
                out_approx << "\n";
            }
        }
        out_approx << "\n";
    }
    out_approx.close();

    r = GetLinearCoeffs(test, y, 2);

    // output approximations:
    out_approx.open("test_app_y.dat");
    for(Eigen::Index i = 0; i < N0; i++)
    {
        for(Eigen::Index j = 0; j < N1; j++)
        {
            for(Eigen::Index k = 0; k < N2-1; k++)
            {
                for(Eigen::Index jj = 0; jj < M; jj++)
                {
                    double y0(y[k] +jj*(y[k+1]-y[k])/(M-1));
                    out_approx << i << "\t" << x[j] << "\t" << y0 << "\t"
                        << "\t" << r(0, i, j, k) +r(1, i, j, k) *y0 << "\n";
                }
            }
            out_approx << "\n";
        }
        out_approx << "\n";
    }
    out_approx.close();
 */
template <int Rank_, typename T>
Eigen::Tensor<T, Rank_ +1> GetLinearCoeffs(const Eigen::Tensor<T, Rank_> & data,
        const Eigen::ArrayXd & x,
        const int & col = 0)
{
    // dimsData : dimensions of the data array
    // dimsData.size() == Rank_
    //
    // dimsRes  : dimensions of the result array
    const typename Eigen::Tensor<T, Rank_>::Dimensions &dimsData(data.dimensions());
    typename Eigen::Tensor<T, Rank_ +1>::Dimensions dimsRes ;
    for(Eigen::Index i = 0; i < Rank_; i++)
        dimsRes[i+1] = dimsData[i];
    dimsRes[0] = 2;
    Eigen::Tensor<T, Rank_ +1> res(dimsRes);
    // Raw data
    const typename Eigen::Tensor<T, Rank_>::Scalar* rawData = data.data();
    typename Eigen::Tensor<T, Rank_>::Scalar* rawRes = res.data();

    // Precalculation of 1/(x[n+1] -x[n])
    Eigen::ArrayXd x1(x.size());
    for(Eigen::Index n = 0; n < x.size() -1; n++)
        x1[n] = 1.0/(x[n+1] -x[n]);
    x1[x.size() -1] = 0.0;

    Eigen::ArrayXi index = Eigen::ArrayXi::Zero(Rank_);         // Array of indices
    Eigen::ArrayXi P(Rank_+1);                                  // Array of dimensions products. P[Rank_] == data.size()
    P[0] = 1;
    for(Eigen::Index r = 1; r < Rank_+1; r ++)
        P[r] = P[r-1] *dimsData[r-1];

    for(Eigen::Index i = 0; i < P[Rank_]/dimsData[col]; i ++)
    {
        // calculating the index d of element in rawData as function of 'index' array
        //      NOTATION:
        //          index_n = [ i_0, i_1, ..., i_{col-1}, n, i_{col+1}, ..., i_{Rank_-1} ]
        //          d = d( i_0, i_1, ..., i_{col-1}, 0, i_{col +1}, ..., i_{Rank_-1} )
        // res_data[d] = data(index_0)
        Eigen::Index d(0);
        for(Eigen::Index r = 0; r < Rank_; r++)
        {
            d += index[r] *P[r];
        }
        // At this stage we have:
        // d0 = d( i_0, i_1, ..., i_{col-1}, 0, i_{col +1}, ..., i_{Rank_-1} )
        // dn = d( i_0, i_1, ..., i_{col-1}, n, i_{col +1}, ..., i_{Rank_-1} ) = d0 +P[col] *n
        // ***** worker block *****
        //  on interval [x[n], x[n+1])
        //      res[0, index_n] = (data[index_n] x[n+1] -data[index_{n+1}] x[n]) / (x[n+1] -x[n])       = b[n]
        //      res[1, index_n] = (data[index_{n+1}] -data[index_{n}]) / (x[n+1] -x[n])                 = a[n]
        //      f(x) = a[n] x +b[n]
        Eigen::Index dn0(d);
        Eigen::Index rdn0(dn0 +dn0);
        Eigen::Index rdn1(rdn0 +1);
        for(Eigen::Index ic = 0; ic < dimsData[col]-1; ic++)
        {
            //dn0 = d +P[col] *ic;  dn0  = d(   i_0, i_1, ..., i_{col -1}, ic, i_{col +1}, ..., i_{Rank_-1} )
            //rdn0 = 2 d;           rdn0 = d(0, i_0, i_1, ..., i_{col -1}, ic, i_{col +1}, ..., i_{Rank_-1} ) => rawRes[rdn1] = res(0, i_0, i_1, ..., i_{col -1}, ic, i_{col +1}, ..., i_{Rank_ -1})
            //rdn1 = rdn0 +1;       rdn1 = d(1, i_0, i_1, ..., i_{col -1}, ic, i_{col +1}, ..., i_{Rank_-1} ) => rawRes[rdn1] = res(1, i_0, i_1, ..., i_{col -1}, ic, i_{col +1}, ..., i_{Rank_ -1})
            Eigen::Index dn10(dn0 +P[col]);
            // b_n
            // rawRes[rdn0] = res(0, index_ic)
            rawRes[rdn0] = (rawData[dn0] *x[ic+1] -rawData[dn10] *x[ic]) *x1[ic];
            // a_n
            // rawRes[rdn1] = res(1, index_ic)
            rawRes[rdn1] = (rawData[dn10] -rawData[dn0]) *x1[ic];
            // Next step
            dn0 = dn10;
            rdn0 = dn10 +dn10;
            rdn1 = rdn0 +1;
        }
        // ***** end of worker block *****
        // Next index
        for(Eigen::Index r = 0; r < Rank_; r++)
        {
            if (r == col) continue;     // scip col's elements
            index[r] += 1;              // next element
            if (index[r] == dimsData[r])   // upper boundary
                index[r] = 0;               // if reached upper boundary => reset index to 0
            else
                break;                      // if it is not the upper boundary => index is increased => break r-loop
        }

    }
    return res;
}

}
}
