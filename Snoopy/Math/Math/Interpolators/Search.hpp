#ifndef BV_Math_Interpolators_Search_hpp
#define BV_Math_Interpolators_Search_hpp

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <type_traits>
#include <string>
#include <array>

#include "Tools/BVException.hpp"
#include "Math/Tools.hpp"
#include "Math/Interpolators/Details.hpp"

namespace BV {
namespace Math {
namespace Interpolators {

using namespace std ;

namespace Details {

/*!
 * \brief An exception that is raised whenever the value seek is out of the
 *     axis range.
 */
struct OutOfBoundsException : public BV::Tools::Exceptions::BVException
{
    OutOfBoundsException(const std::string & message) :
        BV::Tools::Exceptions::BVException(message)
    {
    }
} ;

template <typename T>
auto ToString(T val) -> decltype(std::to_string(val), std::string())
{
    return std::to_string(val) ;
}

template <typename T>
auto ToString(T val) -> decltype(std::to_string(val[0]), std::string())
{
    return std::to_string(val[0]) ;
}

/*!
 * \brief Returns the lower and upper indices of value in axis container.
 *     Given a lower index provided, the upper index is looked for so
 *     that axis[lower] < value < axis[upper].
 *     Note that axis is supposed sorted increasingly.
 * \param axis: the axis into which to look for value lower and upper indices.
 * \param value: the value which lower and upper indices in axis should be found.
 * \param lower: the lower index so that axis[lower] < value.
 * \param epsilon: the double comparison delta, defaults to 1.e-8
 * \return the lower and upper indices corresponding to the values in axis
 *     that surround provided value.
 */
template <typename AxisType, typename IndexType>
std::array<IndexType, 2> GetLowerAndUpper(
                            const AxisType & axis,
                            const typename Axis<AxisType>::ScalarType & value,
                            const IndexType & lower,
                            typename Axis<AxisType>::ScalarType epsilon=1.e-8,
                            bool throwOnWrongValue=false
                                              )
{
    IndexType ind(0) ;
    if (Details::Axis<AxisType>::get(axis, lower) <= value)
    {
        while (((lower+ind) < (Details::Axis<AxisType>::size(axis)-1))
               && (abs(Details::Axis<AxisType>::get(axis, lower)
                       - Details::Axis<AxisType>::get(axis, lower+ind)) < epsilon))
        {
            ind += 1 ;
        }
        if (throwOnWrongValue && ((Axis<AxisType>::get(axis, lower+ind)+epsilon) < value))
        {
            throw Details::OutOfBoundsException("value not in axis range: "
                                                + ToString(value)
                                                + " > "
                                                + ToString(
                                                Axis<AxisType>::get(axis, lower+ind)
                                                          )) ;
        }
    }
    std::array<IndexType, 2> inds ;
    inds[0] = lower ;
    inds[1] = lower+ind ;
    return inds ;
}

/*!
 * \brief Given a lower index, this function seeks for the upper index and
 *     returns the index which corresponding value in axis is closest from
 *     provided value. Lower and upper indices surround the value so that:
 *     axis[lower] < value < axis[upper].
 *     Note that axis is supposed sorted increasingly.
 * \param axis: the axis containing the set of value to which to compare value.
 * \param value: the value for which to seek the closest index in axis.
 * \param lower: the lower index so that axis[lower] < value
 * \return the index corresponding to the closest value to provided value
 *     in axis.
 */
template <typename AxisType, typename IndexType>
IndexType GetClosest(const AxisType & axis,
                    const typename Axis<AxisType>::ScalarType & value,
                    const IndexType & lower)
{
    typedef typename Axis<AxisType>::ScalarType ScalarType ;
    std::array<IndexType, 2> inds(GetLowerAndUpper(axis, value, lower)) ;
    ScalarType diff1(value - Axis<AxisType>::get(axis, inds[0])) ;
    ScalarType diff2(Axis<AxisType>::get(axis, inds[1]) - value) ;
    if (diff1 < diff2)
    {
        return inds[0] ;
    }
    return inds[1] ;
}

} // End of namespace Details

template <typename AxisType>
typename Details::Axis<AxisType>::IndexType GetLower(const AxisType & axis,
                              const typename Details::Axis<AxisType>::ScalarType & value,
                              typename Details::Axis<AxisType>::ScalarType epsilon=1.e-8,
                              bool throwOnWrongValue=false)
{
    using IndexType = typename Details::Axis<AxisType>::IndexType ;
    IndexType lowerIndex(0) ;
    IndexType maxIndex(Details::Axis<AxisType>::size(axis)-1) ;
    IndexType higherIndex(maxIndex) ;
    IndexType middleIndex((higherIndex + lowerIndex) / 2) ;
    while ((higherIndex - lowerIndex) > 1)
    {
        if (abs(Details::Axis<AxisType>::get(axis, middleIndex) - value) < epsilon)
        {
            return middleIndex ;
        }
        if (Details::Axis<AxisType>::get(axis, middleIndex) < value)
        {
            lowerIndex = middleIndex ;
        }
        else
        {
            higherIndex = middleIndex ;
        }
        middleIndex = (higherIndex + lowerIndex) / 2 ;
    }
    if ((lowerIndex == 0) && (value < Details::Axis<AxisType>::get(axis, 0)))
    {
        if (throwOnWrongValue)
        {
            throw Details::OutOfBoundsException("value not in axis range: "
                                                + Details::ToString(value)
                                                + " < "
                                                + Details::ToString(
                                            Details::Axis<AxisType>::get(axis, 0)
                                                                   )) ;
        }
        --higherIndex ;
    }
    if ((higherIndex == maxIndex)
        && (value > Details::Axis<AxisType>::get(axis, maxIndex)))
    {
        if (throwOnWrongValue)
        {
            throw Details::OutOfBoundsException("value not in axis range: "
                                                + Details::ToString(value)
                                                + " > "
                                                + Details::ToString(
                                            Details::Axis<AxisType>::get(axis, maxIndex)
                                                                   )) ;
        }
        // If we have to extrapolate, we chose to set the indices to the same
        // value, i.e. the last index
        ++lowerIndex ;
    }
    return lowerIndex ;
}

/*!
    * \brief Returns the lower and upper indices corresponding to the values
    *     surrounding provided value in provided axis.
    * \param axis: the axis containing values with which to compare provided
    *     value.
    *     Note that axis is supposed sorted increasingly.
    * \param value: the value for which to seek its neighbours in axis.
    * \return the lower and upper indices corresponding the values surrounding
    *     provided value in axis.
    */
template <typename AxisType>
std::array<typename Details::Axis<AxisType>::IndexType, 2> GetLowerAndUpper(
            const AxisType & axis,
            const typename Details::Axis<AxisType>::ScalarType & value
                                                        )
{
    typename Details::Axis<AxisType>::IndexType lower(GetLower(axis, value)) ;
    return Details::GetLowerAndUpper(axis, value, lower) ;
}

/*!
    * \brief This function seeks for the lower and upper indices corresponding
    *     to values in axis surrounding provided value, and
    *     returns the one which value is the closest one.
    * \param axis: the axis containing the set of value to which to compare value.
    *     Note that axis is supposed sorted increasingly.
    * \param value: the value for which to seek the closest index in axis.
    * \return the index corresponding to the closest value to provided value
    *     in axis.
    */
template <typename AxisType>
typename Details::Axis<AxisType>::IndexType GetClosest(
            const AxisType & axis,
            const typename Details::Axis<AxisType>::ScalarType & value
                            )
{
    typename Details::Axis<AxisType>::IndexType lower(GetLower(axis, value)) ;
    return Details::GetClosest(axis, value, lower) ;
}

// We tried to use Eigen specific methods below which took longer than the bissection method above...
/*!
 * \brief specialization of the Indices class for the Eigen and Tensor types.
 *     This specialization is useful as those types have faster search
 *     algorithms than the bissection method of original class.
 */
//template <typename AxisType>
//struct Indices<AxisType,
//               typename std::enable_if<Details::IsEigen<AxisType>::value>::type>
//{
//    using IndexType = typename Details::Axis<AxisType>::IndexType ;
//
//    /*!
//     * \brief Returns the index in axis of the value before provided value.
//     * \param axis, the axis in which to find the lower value.
//     * \param value, the value for which to find a lower value in axis.
//     * \return the index of the lower value in axis vector.
//     */
//    static IndexType getLower(
//                const AxisType & axis,
//                const typename Details::Axis<AxisType>::ScalarType & value,
//                bool throwOnWrongValue=false
//                            )
//    {
//        typedef typename Details::GetType<typename Eigen::internal::eval<AxisType>::type>::type OpType ;
//        typename Details::Axis<AxisType>::CopyType diff(
//             (axis - OpType::Constant(axis.size(), value)).cwiseAbs()
//                                                       ) ;
//        IndexType ind ;
//        diff.minCoeff(&ind) ; // minimum distance
//        if (Details::Axis<AxisType>::get(axis, ind) > value) // FIXME eps ?
//        {
//            if (ind == 0)
//            {
//                if (throwOnWrongValue)
//                {
//                    throw Details::OutOfBoundsException(
//                                              "value not in axis range: "
//                                              +std::to_string(value)
//                                              + " < "
//                                              + std::to_string(
//                                           Details::Axis<AxisType>::get(axis, 0)
//                                                              )
//                                                       ) ;
//                }
//                return ind ;
//            }
//            return ind-1 ;
//        }
//        return ind ;
//    }
//
//    /*!
//     * \brief Returns the lower and upper indices corresponding to the values
//     *     surrounding provided value in provided axis.
//     * \param axis: the axis containing values with which to compare provided
//     *     value.
//     *     Note that axis is supposed sorted increasingly.
//     * \param value: the value for which to seek its neighbours in axis.
//     * \return the lower and upper indices corresponding the values surrounding
//     *     provided value in axis.
//     */
//    static std::array<IndexType, 2> getLowerAndUpper(
//                const AxisType & axis,
//                const typename Details::Axis<AxisType>::ScalarType & value
//                                                         )
//    {
//        IndexType lower(getLower(axis, value)) ;
//        return Details::GetLowerAndUpper(axis, value, lower) ;
//    }
//
//    /*!
//     * \brief This function seeks for the lower and upper indices corresponding
//     *     to values in axis surrounding provided value, and
//     *     returns the one which value is the closest one.
//     * \param axis: the axis containing the set of value to which to compare value.
//     *     Note that axis is supposed sorted increasingly.
//     * \param value: the value for which to seek the closest index in axis.
//     * \return the index corresponding to the closest value to provided value
//     *     in axis.
//     */
//    static IndexType getClosest(
//                const AxisType & axis,
//                const typename Details::Axis<AxisType>::ScalarType & value
//                              )
//    {
//        IndexType lower(getLower(axis, value)) ;
//        return Details::GetClosest(axis, value, lower) ;
//    }
//
//} ;

} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Interpolators_Search_hpp
