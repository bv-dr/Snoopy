#ifndef BV_Math_Interpolators_Sort_hpp
#define BV_Math_Interpolators_Sort_hpp

#include <vector>
#include <tuple>

#include "Math/Interpolators/Details.hpp"

namespace BV {
namespace Math {
namespace Interpolators {

namespace Details {

template <typename ValueType>
void SortAscendingOrder(std::vector<std::tuple<ValueType, unsigned> > & vect)
{
    std::sort(vect.begin(), vect.end(),
              [](const std::tuple<ValueType, unsigned> & v1,
                 const std::tuple<ValueType, unsigned> & v2) -> bool
              {
                  return std::get<0>(v1) < std::get<0>(v2) ;
              }) ;
}

} // End of namespace Details

/*!
 * \brief Sorts the provided axis and data in ascending order.
 * \param axis, the axis to be sorted.
 * \param data, the data that will be sorted accordingly to axis.
 * \param sortedAxis, the sorted axis.
 * \param sortedData, the sorted data.
 */
template <typename AxisType, typename DataType, std::size_t N>
void SortAxisAscendingOrder(const AxisType & axis,
                            const DataType & data,
                            Details::Axis<AxisType>::CopyType & sortedAxis,
                            Details::Data<N, DataType>::CopyType & sortedData)
{
    // FIXME naive implementation, find a better way !
    typedef typename Details::Axis<AxisType>::ScalarType ScalarType ;
    std::vector<std::tuple<ScalarType, unsigned> > rows ;
    unsigned nValues(Details::Axis<AxisType>::size(axis)) ;
    for (unsigned iRow=0; iRow<nValues; ++iRow)
    {
        std::vector<ScalarType> row ;
        rows.push_back(std::make_tuple(
                    Details::Axis<AxisType>::get(axis, iRow), iRow
                                      )) ;
    }
    SortAscendingOrder_(rows) ;
    for (unsigned iRow=0; iRow<nValues; ++iRow)
    {
        Details::Axis<AxisType>::set(sortedAxis, iRow, std::get<0>(rows[iRow])) ;
        Details::Data<N, DataType>::set(
                  sortedData, iRow,
                  Details::Data<N, DataType>::get(data, std::get<1>(rows[iRow]))
                                       ) ;
    }
}

} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Interpolators_Sort_hpp
