#ifndef BV_Math_Interpolators_Linear_hpp
#define BV_Math_Interpolators_Linear_hpp

#include <map>
#include <vector>
#include <type_traits>
#include <iostream>

#include "Math/Tools.hpp"
#include "Math/Interpolators/Details.hpp"
#include "Math/Interpolators/Search.hpp"

namespace BV {
namespace Math {
namespace Interpolators {

enum ExtrapolationType {
    EXCEPTION,
    BOUNDARY,
    ZERO,
    EXTRAPOLATE
};

/*!
 * \brief Specialization of the Interpolator class for 1D interpolation with
 *     a linear interpolation scheme
 */
template <>
struct Interpolator<1, InterpScheme::LINEAR>
{
    /*!
     * \brief Performs a linear interpolation between two values.
     * \param x0: the value in interpolation axis corresponding to the first value.
     * \param x1: the value in interpolation axis corresponding to the second value.
     * \param y0: the value in data corresponding to x0 in axis.
     * \param y1: the value in data corresponding to x1 in axis.
     * \param x: the value comprised between x0 and x1 in axis for which to
     *     linearly interpolate between y0 and y1.
     * \param result: the result of the linear interpolation.
     *
     * The linear interpolation is performed using following formula:
     * \f[
     *     \frac{y_1 - y_0}{x_1 - x_0} * (x - x_0) + y_0
     * \f]
     */
    template <typename ScalarType, typename DataInnerType,
              typename DataInnerType2>
    static void set(const ScalarType & x0, const ScalarType & x1,
                    const DataInnerType & y0, const DataInnerType & y1,
                    const ScalarType & x,
                    DataInnerType2 & result)
    {
        // Linear interpolation shall not be performed with two equal points
        if (abs(x0 - x1) < 1.e-8)
        {
            result = y0 ;
            return ;
        }
        const ScalarType deltaX(x1-x0) ;
        const typename Details::Data<1, DataInnerType>::CopyType deltaY(y1-y0) ;

        result = deltaY*((x-x0)/deltaX) + y0 ;
    }

    template <typename ScalarType, typename DataInnerType,
              typename DataInnerType2>
    static void set(const ScalarType & x0, const ScalarType & x1,
                    const DataInnerType & y0, const DataInnerType & y1,
                    const ScalarType & x,
                    DataInnerType2 && result)
    {
        // Copy should be safe as it is a kind of pointer holder to data
        DataInnerType2 res(result) ;
        set(x0, x1, y0, y1, x, res) ;
    }

    /*!
     * \brief delegates to the similar set method for the calculation and
     *     returns the result.
     */
    template <typename ScalarType, typename DataInnerType>
    static typename Details::Data<1, DataInnerType>::CopyType get(
                             const ScalarType & x0, const ScalarType & x1,
                             const DataInnerType & y0, const DataInnerType & y1,
                             const ScalarType & x
                                                                 )
    {
        typedef typename Details::Data<1, DataInnerType>::CopyType ResultType ;
        ResultType result(Details::Data<1, DataInnerType>::getResult(x, y0)) ;
        set(x0, x1, y0, y1, x, result) ;
        return result ;
    }

    /*!
     * \brief Performs a linear interpolation of provided data according to
     *     the axis coordinate value which should lie in provided axis range.
     * \param axis: the axis of the linear interpolation into which provided
     *     value lies.
     *     Note that axis is supposed sorted increasingly.
     *     There is no assumption on a fixed step or not.
     * \param data: the data to interpolate for corresponding value in axis.
     * \param value: the value for which to interpolate data.
     *     Note that value should lie in axis range, no extrapolation is
     *     performed. An exception will be raised in case it is not the case.
     * \return the interpolated data.
     */
    template <typename AxisType, typename DataType, typename InnerType>
    static void set(const AxisType & axis,
                    const DataType & data,
                    const typename Details::Axis<AxisType>::ScalarType & value,
                    InnerType & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        using IndexType = typename Details::Axis<AxisType>::IndexType ;
        std::array<IndexType, 2> inds(GetLowerAndUpper(axis, value)) ;
        if ((inds[0] == inds[1]) // Same indices in axis
            && !(abs(Details::Axis<AxisType>::get(axis, inds[1]) - value) < 1.e-8)) // and not the last axis value
        {
            if (extrapType == ExtrapolationType::BOUNDARY)
            {
                result = Details::Data<1, DataType>::get(data, inds[0]) ;
                return ;
            }
            else if (extrapType == ExtrapolationType::EXCEPTION)
            {
                throw Details::OutOfBoundsException("Interpolation error : value not in axis range: "
                                                    + Details::ToString(value)) ;
            }
            else if (extrapType == ExtrapolationType::ZERO)
            {
                result = Details::Data<1, DataType>::getResult(value, data, true) ;
                return ;
            }
            else if (extrapType == ExtrapolationType::EXTRAPOLATE)
            {
                if (inds[0] == 0)
                {
                    inds[1] += 1 ; // FIXME what if same value at upper index...
                }
                else
                {
                    inds[0] -= 1 ; // FIXME what if same value at lower index...
                }
            }
        }

        set(Details::Axis<AxisType>::get(axis, inds[0]),
            Details::Axis<AxisType>::get(axis, inds[1]),
            Details::Data<1, DataType>::get(data, inds[0]),
            Details::Data<1, DataType>::get(data, inds[1]),
            value, result) ;
    }

    template <typename AxisType, typename AxisIndex, typename DataType, typename InnerType>
    static void set(const AxisType & axis,
                    const AxisIndex & axisIndex,
                    const DataType & data,
                    const typename Details::Axis<AxisType>::ScalarType & value,
                    InnerType & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        using IndexType = typename Details::Axis<AxisType>::IndexType ;
        std::array<IndexType, 2> inds(GetLowerAndUpper(axis, value)) ;
        if ((inds[0] == inds[1]) // Same indices in axis
            && !(abs(Details::Axis<AxisType>::get(axis, inds[1]) - value) < 1.e-8)) // and not the last axis value
        {
            if (extrapType == ExtrapolationType::BOUNDARY)
            {
                result = Details::Data<1, DataType>::get(data, inds[0], axisIndex) ;
                return ;
            }
            else if (extrapType == ExtrapolationType::EXCEPTION)
            {
                throw Details::OutOfBoundsException("Interpolation error : value not in axis range: "
                                                    + Details::ToString(value)) ;
            }
            else if (extrapType == ExtrapolationType::ZERO)
            {
                result = Details::Data<1, DataType>::getResult(value, data, true) ;
                return ;
            }
            else if (extrapType == ExtrapolationType::EXTRAPOLATE)
            {
                if (inds[0] == 0)
                {
                    inds[1] += 1 ; // FIXME what if same value at upper index...
                }
                else
                {
                    inds[0] -= 1 ; // FIXME what if same value at lower index...
                }
            }
        }

        set(Details::Axis<AxisType>::get(axis, inds[0]),
            Details::Axis<AxisType>::get(axis, inds[1]),
            Details::Data<1, DataType>::get(data, inds[0], axisIndex),
            Details::Data<1, DataType>::get(data, inds[1], axisIndex),
            value, result) ;
    }

    template <typename AxisType, typename DataType, typename InnerType>
    static void set(const AxisType & axis,
                    const DataType & data,
                    const typename Details::Axis<AxisType>::ScalarType & value,
                    InnerType && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        InnerType res(result) ;
        set(axis, data, value, res, extrapType) ;
    }

    template <typename AxisType, typename AxisIndex, typename DataType, typename InnerType>
    static void set(const AxisType & axis,
                    const AxisIndex & axisIndex,
                    const DataType & data,
                    const typename Details::Axis<AxisType>::ScalarType & value,
                    InnerType && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        InnerType res(result) ;
        set(axis, axisIndex, data, value, res, extrapType) ;
    }

    /*!
     * \brief delegates to the similar get method for the calculation and
     *     returns the result.
     */
    template <typename AxisType, typename DataType>
    static typename Details::Data<1, DataType>::InnerType get(
                       const AxisType & axis,
                       const DataType & data,
                       const typename Details::Axis<AxisType>::ScalarType & value,
                       ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                             )
    {
        typedef typename Details::Data<1, DataType>::InnerType ResultType ;
        ResultType result(Details::Data<1, DataType>::getResult(value, data)) ;
        set(axis, data, value, result, extrapType) ;
        return result ;
    }

    template <typename AxisType, typename AxisIndex, typename DataType>
    static typename Details::Data<1, DataType>::InnerType get(
                       const AxisType & axis,
                       const AxisIndex & axisIndex,
                       const DataType & data,
                       const typename Details::Axis<AxisType>::ScalarType & value,
                       ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                             )
    {
        typedef typename Details::Data<1, DataType>::InnerType ResultType ;
        ResultType result(Details::Data<1, DataType>::getResult(value, data)) ;
        set(axis, axisIndex, data, value, result, extrapType) ;
        return result ;
    }
    /*!
     * \brief Performs a linear interpolation of provided data according to
     *     the axis coordinate values which should lie in provided axis range.
     * \param axis: the axis of the linear interpolation into which provided
     *     value lies.
     *     Note that axis is supposed sorted increasingly.
     *     There is no assumption on a fixed step or not.
     * \param data: the data to interpolate for corresponding value in axis.
     * \param values: the values for which to interpolate data.
     *     Note that all values should lie in axis range, no extrapolation is
     *     performed. An exception will be raised in case it is not the case.
     * \return the interpolated data.
     */
    template <typename AxisType, typename DataType, typename AxisType2,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType2>::value, int>::type=0>
    static void set(const AxisType & axis, const DataType & data,
                    const AxisType2 & values, DataType2 & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        using SizeType = typename Details::Axis<AxisType2>::SizeType ;
        SizeType nValues(Details::Axis<AxisType2>::size(values)) ;
        //typedef typename Details::Data<1, DataType2>::InnerTypeRef InnerTypeRef ;
        for (SizeType ind=0; ind<nValues; ++ind)
        {
            //InnerTypeRef inner(Details::Data<1, DataType2>::get(result, ind)) ;
            set(axis, data, Details::Axis<AxisType2>::get(values, ind), //inner) ;
                Details::Data<1, DataType2>::get(result, ind), extrapType) ;
        }
    }

    template <typename AxisType, typename AxisIndex, typename DataType,
              typename AxisType2, typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType2>::value, int>::type=0>
    static void set(const AxisType & axis, const AxisIndex & axisIndex,
                    const DataType & data,
                    const AxisType2 & values, DataType2 & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        using SizeType = typename Details::Axis<AxisType2>::SizeType ;
        SizeType nValues(Details::Axis<AxisType2>::size(values)) ;
        //typedef typename Details::Data<1, DataType2>::InnerTypeRef InnerTypeRef ;
        for (SizeType ind=0; ind<nValues; ++ind)
        {
            //InnerTypeRef inner(Details::Data<1, DataType2>::get(result, ind)) ;
            set(axis, axisIndex, data, Details::Axis<AxisType2>::get(values, ind), //inner) ;
                Details::Data<1, DataType2>::get(result, ind, axisIndex),
                extrapType) ;
        }
    }

    template <typename AxisType, typename DataType, typename AxisType2,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType2>::value, int>::type=0>
    static void set(const AxisType & axis, const DataType & data,
                    const AxisType2 & values, DataType2 && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        DataType2 res(result) ;
        set(axis, data, values, res, extrapType) ;
    }

    template <typename AxisType, typename AxisIndex, typename DataType,
              typename AxisType2, typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType2>::value, int>::type=0>
    static void set(const AxisType & axis, const AxisIndex & axisIndex,
                    const DataType & data,
                    const AxisType2 & values, DataType2 && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        DataType2 res(result) ;
        set(axis, axisIndex, data, values, res, extrapType) ;
    }

    template <typename AxisType, typename DataType, typename AxisType2,
              typename std::enable_if<!Details::IsScalar<AxisType2>::value, int>::type=0>
    static typename Details::Data<1, DataType>::CopyType get(
                                                   const AxisType & axis,
                                                   const DataType & data,
                                                   const AxisType2 & values,
                                                   ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        typename Details::Data<1, DataType>::CopyType result(
                      Details::Data<1, DataType>::getResult(values, data)
                                                            ) ;
        set(axis, data, values, result, extrapType) ;
        return result ;
    }

    template <typename AxisType, typename AxisIndex, typename DataType, typename AxisType2,
              typename std::enable_if<!Details::IsScalar<AxisType2>::value, int>::type=0>
    static typename Details::Data<1, DataType>::CopyType get(
                                                   const AxisType & axis,
                                                   const AxisIndex & axisIndex,
                                                   const DataType & data,
                                                   const AxisType2 & values,
                                                   ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        typename Details::Data<1, DataType>::CopyType result(
                      Details::Data<1, DataType>::getResult(values, data, axisIndex)
                                                            ) ;
        set(axis, axisIndex, data, values, result, extrapType) ;
        return result ;
    }

    /*!
     * \brief Special overload for a map set of values.
     *     std::map may have the axis and data in the same container.
     *     Furthermore, search algorithms in map are optimized.
     * \param data: the map with keys corresponding to the interpolation axis
     *     and values corresponding to the corresponding data.
     *     Note that DataType may not be a scalar.
     * \param value: the value for which to interpolate data.
     * \return the interpolated data.
     */
    template <typename ScalarType, typename DataType, typename DataType2>
    static void set(const std::map<ScalarType, DataType> & data,
                    const ScalarType & value, DataType2 & result)
    {
        typedef typename std::map<ScalarType, DataType>::const_iterator Iterator ;
        Iterator it(data.upper_bound(value)) ;
        if (it == data.end())
        {
            result = (--it)->second ;
            return ;
        }
        if (it == data.begin())
        {
            result = it->second ;
            return ;
        }
        Iterator itm1(it) ;
        --itm1 ;
        typename Details::Data<1,DataType>::CopyType delta(
                              (value - itm1->first) / (it->first - itm1->first)
                                                          ) ;
        result = delta * it->second + (1.-delta) * itm1->second ;
    }

    //template <typename ScalarType, typename DataType>
    //static typename Details::Data<1, DataType>::CopyType get(
    //                              const std::map<ScalarType, DataType> & data,
    //                              const ScalarType & value
    //                                                        )
    //{
    //    typename Details::Data<1, DataType>::CopyType result(
    //                                Details::Data<1, DataType>::getInitialised(
    //                                   Details::Data<1, DataType>::get(data, 0)
    //                                                                          )
    //                                                        ) ;
    //    set(data, value, result) ;
    //    return result ;
    //}

    /*!
     * \brief Same as the get method for std::map but with a vector of values
     *     to interpolate.
     * \param data: the map of axis/data into which to interpolate.
     * \param values: the vector of values to interpolate in data.
     * \return the vector of interpolated data.
     */
    template <typename ScalarType, typename DataType, typename DataType2>
    static void set(const std::map<ScalarType, DataType> & data,
                    const std::vector<ScalarType> & values,
                    DataType2 & result)
    {
        // FIXME here we assume DataType2 is of std::vector type
        std::size_t size(values.size()) ;
        for (std::size_t i=0; i<size; ++i)
        {
            set(data, values[i], result[i]) ;
        }
    }

    template <typename ScalarType, typename DataType>
    static std::vector<typename Details::Data<1, DataType>::CopyType> get(
                                   const std::map<ScalarType, DataType> & data,
                                   const std::vector<ScalarType> & values
                                                                         )
    {
        std::size_t size(values.size()) ;
        std::vector<typename Details::Data<1, DataType>::CopyType> result(size) ;
        set(data, values, result) ;
        return result ;
    }

} ;

/*!
 * \brief Specialization of the Interpolator class for 2D interpolation with
 *     a linear interpolation scheme
 */
template <>
struct Interpolator<2, InterpScheme::LINEAR>
{
    template <typename ScalarType, typename DataInnerType,
              typename DataInnerType2>
    static void set(const ScalarType & axis0_0, const ScalarType & axis0_1,
                    const ScalarType & axis1_0, const ScalarType & axis1_1,
                    const DataInnerType & data0_0, const DataInnerType & data0_1,
                    const DataInnerType & data1_0, const DataInnerType & data1_1,
                    const ScalarType & value1, const ScalarType & value2,
                    DataInnerType2 & result)
    {
        ScalarType ratio1(1.), ratio2(1.) ; // upper value by default
        if (!(abs(axis0_1 - axis0_0) < 1.e-8))
        {
            ratio1 = (value1 - axis0_0)/(axis0_1 - axis0_0) ;
        }
        if (!(abs(axis1_1 - axis1_0) < 1.e-8))
        {
            ratio2 = (value2 - axis1_0)/(axis1_1 - axis1_0) ;
        }
        ScalarType r1r2(ratio1*ratio2) ;
        result = (1 - ratio1 - ratio2 + r1r2) * data0_0 +
                              (ratio2 - r1r2) * data0_1 +
                              (ratio1 - r1r2) * data1_0 +
                                         r1r2 * data1_1 ;
    }

    template <typename ScalarType, typename DataInnerType,
              typename DataInnerType2>
    static void set(const ScalarType & axis0_0, const ScalarType & axis0_1,
                    const ScalarType & axis1_0, const ScalarType & axis1_1,
                    const DataInnerType & data0_0, const DataInnerType & data0_1,
                    const DataInnerType & data1_0, const DataInnerType & data1_1,
                    const ScalarType & value1, const ScalarType & value2,
                    DataInnerType2 && result)
    {
        DataInnerType2 res(result) ;
        set(axis0_0, axis0_1, axis1_0, axis1_1,
            data0_0, data0_1, data1_0, data1_1,
            value1, value2, res) ;
    }

    template <typename ScalarType, typename DataInnerType>
    static typename Details::Data<2, DataInnerType>::CopyType get(
                   const ScalarType & axis0_0, const ScalarType & axis0_1,
                   const ScalarType & axis1_0, const ScalarType & axis1_1,
                   const DataInnerType & data0_0, const DataInnerType & data0_1,
                   const DataInnerType & data1_0, const DataInnerType & data1_1,
                   const ScalarType & value1, const ScalarType & value2
                                                                 )
    {
        typedef typename Details::Data<2, DataInnerType>::CopyType ResultType ;
        ResultType result(Details::Data<2, DataInnerType>::getResult(value1, value2, data0_0)) ;
        set(axis0_0, axis0_1, axis1_0, axis1_1,
            data0_0, data0_1, data1_0, data1_1,
            value1, value2, result) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2,
              typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    DataInnerType & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        using IndexType = typename Details::Axis<AxisType1>::IndexType ;
        std::array<IndexType, 2> inds0(GetLowerAndUpper(axis1, value1)) ;
        std::array<IndexType, 2> inds1(GetLowerAndUpper(axis2, value2)) ;
        if ((inds0[0] == inds0[1]) // Same indices in axis
            && !(abs(Details::Axis<AxisType1>::get(axis1, inds0[1]) - value1) < 1.e-8)) // and not the last axis value
        {
            // Boundary is dealt by default
            if (extrapType == ExtrapolationType::EXCEPTION)
            {
                throw Details::OutOfBoundsException("Interpolation error : value not in axis range: "
                                                    + Details::ToString(value1)) ;
            }
            else if (extrapType == ExtrapolationType::ZERO)
            {
                result = Details::Data<2, DataType>::getResult(value1, value2, data, true) ;
                return ;
            }
            else if (extrapType == ExtrapolationType::EXTRAPOLATE)
            {
                if (inds0[0] == 0)
                {
                    inds0[1] += 1 ; // FIXME what if same value at upper index...
                }
                else
                {
                    inds0[0] -= 1 ; // FIXME what if same value at lower index...
                }
            }
        }
        if ((inds1[0] == inds1[1]) // Same indices in axis
            && !(abs(Details::Axis<AxisType2>::get(axis2, inds1[1]) - value2) < 1.e-8)) // and not the last axis value
        {
            // Boundary is dealt by default
            if (extrapType == ExtrapolationType::EXCEPTION)
            {
                throw Details::OutOfBoundsException("Interpolation error : value not in axis range: "
                                                    + Details::ToString(value2)) ;
            }
            else if (extrapType == ExtrapolationType::ZERO)
            {
                result = Details::Data<2, DataType>::getResult(value1, value2, data, true) ;
                return;
            }
            else if (extrapType == ExtrapolationType::EXTRAPOLATE)
            {
                if (inds1[0] == 0)
                {
                    inds1[1] += 1 ; // FIXME what if same value at upper index...
                }
                else
                {
                    inds1[0] -= 1 ; // FIXME what if same value at lower index...
                }
            }
        }
        set(Details::Axis<AxisType1>::get(axis1, inds0[0]),
            Details::Axis<AxisType1>::get(axis1, inds0[1]),
            Details::Axis<AxisType2>::get(axis2, inds1[0]),
            Details::Axis<AxisType2>::get(axis2, inds1[1]),
            Details::Data<2, DataType>::get(data, inds0[0], inds1[0]),
            Details::Data<2, DataType>::get(data, inds0[0], inds1[1]),
            Details::Data<2, DataType>::get(data, inds0[1], inds1[0]),
            Details::Data<2, DataType>::get(data, inds0[1], inds1[1]),
            value1, value2, result) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
              typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    DataInnerType & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        using IndexType = typename Details::Axis<AxisType1>::IndexType ;
        std::array<IndexType, 2> inds0(GetLowerAndUpper(axis1, value1)) ;
        std::array<IndexType, 2> inds1(GetLowerAndUpper(axis2, value2)) ;
        if ((inds0[0] == inds0[1]) // Same indices in axis
            && !(abs(Details::Axis<AxisType1>::get(axis1, inds0[1]) - value1) < 1.e-8)) // and not the last axis value
        {
            // Boundary is dealt by default
            if (extrapType == ExtrapolationType::EXCEPTION)
            {
                throw Details::OutOfBoundsException("Interpolation error : value not in axis range: "
                                                    + Details::ToString(value1)) ;
            }
            else if (extrapType == ExtrapolationType::ZERO)
            {
                result = Details::Data<2, DataType>::getResult(value1, value2, axisIndex1, axisIndex2, data, true) ;
                return;
            }
            else if (extrapType == ExtrapolationType::EXTRAPOLATE)
            {
                if (inds0[0] == 0)
                {
                    inds0[1] += 1 ; // FIXME what if same value at upper index...
                }
                else
                {
                    inds0[0] -= 1 ; // FIXME what if same value at lower index...
                }
            }
        }
        if ((inds1[0] == inds1[1]) // Same indices in axis
            && !(abs(Details::Axis<AxisType2>::get(axis2, inds1[1]) - value2) < 1.e-8)) // and not the last axis value
        {
            // Boundary is dealt by default
            if (extrapType == ExtrapolationType::EXCEPTION)
            {
                throw Details::OutOfBoundsException("Interpolation error : value not in axis range: "
                                                    + Details::ToString(value2)) ;
            }
            else if (extrapType == ExtrapolationType::ZERO)
            {
                result = Details::Data<2, DataType>::getResult(value1, value2, axisIndex1, axisIndex2, data, true) ;
                return;
            }
            else if (extrapType == ExtrapolationType::EXTRAPOLATE)
            {
                if (inds1[0] == 0)
                {
                    inds1[1] += 1 ; // FIXME what if same value at upper index...
                }
                else
                {
                    inds1[0] -= 1 ; // FIXME what if same value at lower index...
                }
            }
        }
        set(Details::Axis<AxisType1>::get(axis1, inds0[0]),
            Details::Axis<AxisType1>::get(axis1, inds0[1]),
            Details::Axis<AxisType2>::get(axis2, inds1[0]),
            Details::Axis<AxisType2>::get(axis2, inds1[1]),
            Details::Data<2, DataType>::get(data, inds0[0], inds1[0], axisIndex1, axisIndex2),
            Details::Data<2, DataType>::get(data, inds0[0], inds1[1], axisIndex1, axisIndex2),
            Details::Data<2, DataType>::get(data, inds0[1], inds1[0], axisIndex1, axisIndex2),
            Details::Data<2, DataType>::get(data, inds0[1], inds1[1], axisIndex1, axisIndex2),
            value1, value2, result) ;
    }

    template <typename AxisType1, typename AxisType2, typename DataType,
              typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    DataInnerType && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        DataInnerType res(result) ;
        set(axis1, axis2, data, value1, value2, res, extrapType) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
              typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    DataInnerType && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        DataInnerType res(result) ;
        set(axis1, axis2, axisIndex1, axisIndex2, data, value1, value2, res, extrapType) ;
    }

    template <typename AxisType1, typename AxisType2, typename DataType>
    static typename Details::Data<2, DataType>::InnerType get(
                     const AxisType1 & axis1,
                     const AxisType2 & axis2,
                     const DataType & data,
                     const typename Details::Axis<AxisType1>::ScalarType & value1,
                     const typename Details::Axis<AxisType2>::ScalarType & value2,
                     ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                             )
    {
        typedef typename Details::Data<2, DataType>::InnerType ResultType ;
        ResultType result(Details::Data<2, DataType>::getResult(value1, value2, data)) ;
        set(axis1, axis2, data, value1, value2, result, extrapType) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
              typename DataType>
    static typename Details::Data<2, DataType>::InnerType get(
                     const AxisType1 & axis1,
                     const AxisType2 & axis2,
                     const AxisIndex & axisIndex1,
                     const AxisIndex & axisIndex2,
                     const DataType & data,
                     const typename Details::Axis<AxisType1>::ScalarType & value1,
                     const typename Details::Axis<AxisType2>::ScalarType & value2,
                     ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                             )
    {
        typedef typename Details::Data<2, DataType>::InnerType ResultType ;
        ResultType result(Details::Data<2, DataType>::getResult(value1, value2, axisIndex1, axisIndex2, data)) ;
        set(axis1, axis2, axisIndex1, axisIndex2, data, value1, value2, result, extrapType) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename DataType,
              typename AxisType3, typename AxisType4,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType3>::value
                                      && !Details::IsScalar<AxisType4>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const DataType & data,
                    const AxisType3 & values1, const AxisType4 & values2,
                    DataType2 & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        using SizeType = typename Details::Axis<AxisType3>::SizeType ;
        SizeType nValues1(Details::Axis<AxisType3>::size(values1)) ;
        SizeType nValues2(Details::Axis<AxisType4>::size(values2)) ;
        for (SizeType i=0; i<nValues1; ++i)
        {
            for (SizeType j=0; j<nValues2; ++j)
            {
                //typedef typename Details::Data<2, DataType2>::InnerTypeRef InnerTypeRef ;
                //InnerTypeRef inner(Details::Data<2, DataType2>::get(result, i, j)) ;
                set(axis1, axis2, data,
                    Details::Axis<AxisType3>::get(values1, i),
                    Details::Axis<AxisType4>::get(values2, j),
                    Details::Data<2, DataType2>::get(result, i, j),
                    extrapType) ;//inner) ;
            }
        }
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
              typename DataType, typename AxisType3, typename AxisType4,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType3>::value
                                      && !Details::IsScalar<AxisType4>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const AxisIndex & axisIndex1, const AxisIndex & axisIndex2,
                    const DataType & data,
                    const AxisType3 & values1, const AxisType4 & values2,
                    DataType2 & result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        using SizeType = typename Details::Axis<AxisType3>::SizeType ;
        SizeType nValues1(Details::Axis<AxisType3>::size(values1)) ;
        SizeType nValues2(Details::Axis<AxisType4>::size(values2)) ;
        for (SizeType i=0; i<nValues1; ++i)
        {
            for (SizeType j=0; j<nValues2; ++j)
            {
                //typedef typename Details::Data<2, DataType2>::InnerTypeRef InnerTypeRef ;
                //InnerTypeRef inner(Details::Data<2, DataType2>::get(result, i, j)) ;
                set(axis1, axis2, axisIndex1, axisIndex2, data,
                    Details::Axis<AxisType3>::get(values1, i),
                    Details::Axis<AxisType4>::get(values2, j),
                    Details::Data<2, DataType2>::get(result, i, j,
                                                     axisIndex1, axisIndex2),
                    extrapType) ;//inner) ;
            }
        }
    }

    template <typename AxisType1, typename AxisType2,
              typename DataType, typename AxisType3, typename AxisType4,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType3>::value
                                      && !Details::IsScalar<AxisType4>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const DataType & data,
                    const AxisType3 & values1, const AxisType4 & values2,
                    DataType2 && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        DataType2 res(result) ;
        set(axis1, axis2, data, values1, values2, res, extrapType) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
              typename DataType, typename AxisType3, typename AxisType4,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType3>::value
                                      && !Details::IsScalar<AxisType4>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const AxisIndex & axisIndex1, const AxisIndex & axisIndex2,
                    const DataType & data,
                    const AxisType3 & values1, const AxisType4 & values2,
                    DataType2 && result,
                    ExtrapolationType extrapType=ExtrapolationType::BOUNDARY)
    {
        DataType2 res(result) ;
        set(axis1, axis2, axisIndex1, axisIndex2, data, values1, values2, res,
            extrapType) ;
    }

    template <typename AxisType1, typename AxisType2, typename DataType,
              typename AxisType3, typename AxisType4,
              typename std::enable_if<!Details::IsScalar<AxisType3>::value
                                      && !Details::IsScalar<AxisType4>::value, int>::type=0>
    static typename Details::Data<2, DataType>::CopyType get(
                            const AxisType1 & axis1, const AxisType2 & axis2,
                            const DataType & data,
                            const AxisType3 & values1, const AxisType4 & values2,
                            ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        typename Details::Data<2, DataType>::CopyType result(
                  Details::Data<2, DataType>::getResult(values1, values2, data)
                                                            ) ;
        set(axis1, axis2, data, values1, values2, result, extrapType) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
              typename DataType, typename AxisType3, typename AxisType4,
              typename std::enable_if<!Details::IsScalar<AxisType3>::value
                                      && !Details::IsScalar<AxisType4>::value, int>::type=0>
    static typename Details::Data<2, DataType>::CopyType get(
                            const AxisType1 & axis1, const AxisType2 & axis2,
                            const AxisIndex & axisIndex1, const AxisIndex & axisIndex2,
                            const DataType & data,
                            const AxisType3 & values1, const AxisType4 & values2,
                            ExtrapolationType extrapType=ExtrapolationType::BOUNDARY
                                                            )
    {
        typename Details::Data<2, DataType>::CopyType result(
                  Details::Data<2, DataType>::getResult(values1, values2,
                                                        axisIndex1, axisIndex2,
                                                        data)
                                                            ) ;
        set(axis1, axis2, axisIndex1, axisIndex2, data, values1, values2, result,
            extrapType) ;
        return result ;
    }

} ;

template <>
struct Interpolator<3, InterpScheme::LINEAR>
{
    template <typename ScalarType, typename DataInnerType,
              typename DataInnerType2>
    static void set(const ScalarType & axis0_0,
                    const ScalarType & axis0_1,
                    const ScalarType & axis1_0,
                    const ScalarType & axis1_1,
                    const ScalarType & axis2_0,
                    const ScalarType & axis2_1,
                    const DataInnerType & data0_0_0,
                    const DataInnerType & data0_0_1,
                    const DataInnerType & data0_1_0,
                    const DataInnerType & data0_1_1,
                    const DataInnerType & data1_0_0,
                    const DataInnerType & data1_0_1,
                    const DataInnerType & data1_1_0,
                    const DataInnerType & data1_1_1,
                    const ScalarType & value1,
                    const ScalarType & value2,
                    const ScalarType & value3,
                    DataInnerType2 & result)
    {
        ScalarType ratio1(1.), ratio2(1.), ratio3(1.) ; // upper value by default
        if (!(abs(axis0_1 - axis0_0) < 1.e-8))
        {
            ratio1 = (value1 - axis0_0)/(axis0_1 - axis0_0) ;
        }
        if (!(abs(axis1_1 - axis1_0) < 1.e-8))
        {
            ratio2 = (value2 - axis1_0)/(axis1_1 - axis1_0) ;
        }
        if (!(abs(axis2_1 - axis2_0) < 1.e-8))
        {
            ratio3 = (value3 - axis2_0)/(axis2_1 - axis2_0) ;
        }
        ScalarType oppRatio1(1 - ratio1) ;
        ScalarType oppRatio2(1 - ratio2) ;
        ScalarType oppRatio3(1 - ratio3) ;
        result = (oppRatio1 * oppRatio2 * oppRatio3 ) * data0_0_0 +
                 (oppRatio1 * oppRatio2 *    ratio3 ) * data0_0_1 +
                 (oppRatio1 *    ratio2 * oppRatio3 ) * data0_1_0 +
                 (oppRatio1 *    ratio2 *    ratio3 ) * data0_1_1 +
                 (   ratio1 * oppRatio2 * oppRatio3 ) * data1_0_0 +
                 (   ratio1 * oppRatio2 *    ratio3 ) * data1_0_1 +
                 (   ratio1 *    ratio2 * oppRatio3 ) * data1_1_0 +
                 (   ratio1 *    ratio2 *    ratio3 ) * data1_1_1 ;
    }

    template <typename ScalarType, typename DataInnerType,
              typename DataInnerType2>
    static void set(const ScalarType & axis0_0,
                    const ScalarType & axis0_1,
                    const ScalarType & axis1_0,
                    const ScalarType & axis1_1,
                    const ScalarType & axis2_0,
                    const ScalarType & axis2_1,
                    const DataInnerType & data0_0_0,
                    const DataInnerType & data0_0_1,
                    const DataInnerType & data0_1_0,
                    const DataInnerType & data0_1_1,
                    const DataInnerType & data1_0_0,
                    const DataInnerType & data1_0_1,
                    const DataInnerType & data1_1_0,
                    const DataInnerType & data1_1_1,
                    const ScalarType & value1,
                    const ScalarType & value2,
                    const ScalarType & value3,
                    DataInnerType2 && result)
    {
        DataInnerType2 res(result) ;
        set(axis0_0, axis0_1, axis1_0, axis1_1,
            axis2_0, axis2_1,
            data0_0_0, data0_0_1, data0_1_0, data0_1_1,
            data1_0_0, data1_0_1, data1_1_0, data1_1_1,
            value1, value2, value3, res) ;
    }

    template <typename ScalarType, typename DataInnerType>
    static typename Details::Data<3, DataInnerType>::CopyType get(
                                             const ScalarType & axis0_0,
                                             const ScalarType & axis0_1,
                                             const ScalarType & axis1_0,
                                             const ScalarType & axis1_1,
                                             const ScalarType & axis2_0,
                                             const ScalarType & axis2_1,
                                             const DataInnerType & data0_0_0,
                                             const DataInnerType & data0_0_1,
                                             const DataInnerType & data0_1_0,
                                             const DataInnerType & data0_1_1,
                                             const DataInnerType & data1_0_0,
                                             const DataInnerType & data1_0_1,
                                             const DataInnerType & data1_1_0,
                                             const DataInnerType & data1_1_1,
                                             const ScalarType & value1,
                                             const ScalarType & value2,
                                             const ScalarType & value3
                                                                 )
    {
        typedef typename Details::Data<3, DataInnerType>::CopyType ResultType ;
        ResultType result(Details::Data<3, DataInnerType>::getResult(value1, value2, value3, data0_0_0)) ;
        set(axis0_0, axis0_1, axis1_0, axis1_1,
            axis2_0, axis2_1,
            data0_0_0, data0_0_1, data0_1_0, data0_1_1,
            data1_0_0, data1_0_1, data1_1_0, data1_1_1,
            value1, value2, value3, result) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const AxisType3 & axis3,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    const typename Details::Axis<AxisType3>::ScalarType & value3,
                    DataInnerType & result)
    {
        using IndexType = typename Details::Axis<AxisType1>::IndexType ;
        std::array<IndexType, 2> inds0(GetLowerAndUpper(axis1, value1)) ;
        std::array<IndexType, 2> inds1(GetLowerAndUpper(axis2, value2)) ;
        std::array<IndexType, 2> inds2(GetLowerAndUpper(axis3, value3)) ;
        set(Details::Axis<AxisType1>::get(axis1, inds0[0]),
            Details::Axis<AxisType1>::get(axis1, inds0[1]),
            Details::Axis<AxisType2>::get(axis2, inds1[0]),
            Details::Axis<AxisType2>::get(axis2, inds1[1]),
            Details::Axis<AxisType3>::get(axis3, inds2[0]),
            Details::Axis<AxisType3>::get(axis3, inds2[1]),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[0], inds2[0]),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[0], inds2[1]),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[1], inds2[0]),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[1], inds2[1]),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[0], inds2[0]),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[0], inds2[1]),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[1], inds2[0]),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[1], inds2[1]),
            value1, value2, value3, result) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex, typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const AxisType3 & axis3,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2,
                    const AxisIndex & axisIndex3,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    const typename Details::Axis<AxisType3>::ScalarType & value3,
                    DataInnerType & result)
    {
        using IndexType = typename Details::Axis<AxisType1>::IndexType ;
        std::array<IndexType, 2> inds0(GetLowerAndUpper(axis1, value1)) ;
        std::array<IndexType, 2> inds1(GetLowerAndUpper(axis2, value2)) ;
        std::array<IndexType, 2> inds2(GetLowerAndUpper(axis3, value3)) ;
        set(Details::Axis<AxisType1>::get(axis1, inds0[0]),
            Details::Axis<AxisType1>::get(axis1, inds0[1]),
            Details::Axis<AxisType2>::get(axis2, inds1[0]),
            Details::Axis<AxisType2>::get(axis2, inds1[1]),
            Details::Axis<AxisType3>::get(axis3, inds2[0]),
            Details::Axis<AxisType3>::get(axis3, inds2[1]),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[0], inds2[0],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[0], inds2[1],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[1], inds2[0],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[0], inds1[1], inds2[1],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[0], inds2[0],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[0], inds2[1],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[1], inds2[0],
                                            axisIndex1, axisIndex2, axisIndex3),
            Details::Data<3, DataType>::get(data, inds0[1], inds1[1], inds2[1],
                                            axisIndex1, axisIndex2, axisIndex3),
            value1, value2, value3, result) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const AxisType3 & axis3,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    const typename Details::Axis<AxisType3>::ScalarType & value3,
                    DataInnerType && result)
    {
        DataInnerType res(result) ;
        set(axis1, axis2, axis3, data, value1, value2, value3, res) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex, typename DataType, typename DataInnerType>
    static void set(const AxisType1 & axis1,
                    const AxisType2 & axis2,
                    const AxisType3 & axis3,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2,
                    const AxisIndex & axisIndex3,
                    const DataType & data,
                    const typename Details::Axis<AxisType1>::ScalarType & value1,
                    const typename Details::Axis<AxisType2>::ScalarType & value2,
                    const typename Details::Axis<AxisType3>::ScalarType & value3,
                    DataInnerType && result)
    {
        DataInnerType res(result) ;
        set(axis1, axis2, axis3, axisIndex1, axisIndex2, axisIndex3,
            data, value1, value2, value3, res) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename DataType>
    static typename Details::Data<3, DataType>::InnerType get(
                   const AxisType1 & axis1,
                   const AxisType2 & axis2,
                   const AxisType3 & axis3,
                   const DataType & data,
                   const typename Details::Axis<AxisType1>::ScalarType & value1,
                   const typename Details::Axis<AxisType2>::ScalarType & value2,
                   const typename Details::Axis<AxisType3>::ScalarType & value3
                                                             )
    {
        typedef typename Details::Data<3, DataType>::InnerType ResultType ;
        ResultType result(Details::Data<3, DataType>::getResult(value1, value2, value3, data)) ;
        set(axis1, axis2, axis3, data, value1, value2, value3, result) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex, typename DataType>
    static typename Details::Data<3, DataType>::InnerType get(
                   const AxisType1 & axis1,
                   const AxisType2 & axis2,
                   const AxisType3 & axis3,
                   const AxisIndex & axisIndex1,
                   const AxisIndex & axisIndex2,
                   const AxisIndex & axisIndex3,
                   const DataType & data,
                   const typename Details::Axis<AxisType1>::ScalarType & value1,
                   const typename Details::Axis<AxisType2>::ScalarType & value2,
                   const typename Details::Axis<AxisType3>::ScalarType & value3
                                                             )
    {
        typedef typename Details::Data<3, DataType>::InnerType ResultType ;
        ResultType result(Details::Data<3, DataType>::getResult(value1, value2, value3,
                                                                axisIndex1, axisIndex2, axisIndex3,
                                                                data)) ;
        set(axis1, axis2, axis3, axisIndex1, axisIndex2, axisIndex3,
            data, value1, value2, value3, result) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename DataType,
              typename AxisType4, typename AxisType5, typename AxisType6,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType4>::value
                                      && !Details::IsScalar<AxisType5>::value
                                      && !Details::IsScalar<AxisType6>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const AxisType3 & axis3, const DataType & data,
                    const AxisType4 & values1, const AxisType5 & values2,
                    const AxisType6 & values3, DataType2 & result)
    {
        using SizeType = typename Details::Axis<AxisType4>::SizeType ;
        SizeType nValues1(Details::Axis<AxisType4>::size(values1)) ;
        SizeType nValues2(Details::Axis<AxisType5>::size(values2)) ;
        SizeType nValues3(Details::Axis<AxisType6>::size(values3)) ;
        for (SizeType i=0; i<nValues1; ++i)
        {
            for (SizeType j=0; j<nValues2; ++j)
            {
                for (SizeType k=0; k<nValues3; ++k)
                {
                    //typedef typename Details::Data<3, DataType2>::InnerTypeRef InnerTypeRef ;
                    //InnerTypeRef inner(Details::Data<3, DataType2>::get(result, i, j, k)) ;
                    set(axis1, axis2, axis3, data,
                        Details::Axis<AxisType4>::get(values1, i),
                        Details::Axis<AxisType5>::get(values2, j),
                        Details::Axis<AxisType6>::get(values3, k),
                        Details::Data<3, DataType2>::get(result, i, j, k)) ;//inner) ;
                }
            }
        }
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex, typename DataType,
              typename AxisType4, typename AxisType5, typename AxisType6,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType4>::value
                                      && !Details::IsScalar<AxisType5>::value
                                      && !Details::IsScalar<AxisType6>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const AxisType3 & axis3, const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2, const AxisIndex & axisIndex3,
                    const DataType & data,
                    const AxisType4 & values1, const AxisType5 & values2,
                    const AxisType6 & values3, DataType2 & result)
    {
        using SizeType = typename Details::Axis<AxisType4>::SizeType ;
        SizeType nValues1(Details::Axis<AxisType4>::size(values1)) ;
        SizeType nValues2(Details::Axis<AxisType5>::size(values2)) ;
        SizeType nValues3(Details::Axis<AxisType6>::size(values3)) ;
        for (SizeType i=0; i<nValues1; ++i)
        {
            for (SizeType j=0; j<nValues2; ++j)
            {
                for (SizeType k=0; k<nValues3; ++k)
                {
                    //typedef typename Details::Data<3, DataType2>::InnerTypeRef InnerTypeRef ;
                    //InnerTypeRef inner(Details::Data<3, DataType2>::get(result, i, j, k)) ;
                    set(axis1, axis2, axis3, axisIndex1, axisIndex2, axisIndex3,
                        data,
                        Details::Axis<AxisType4>::get(values1, i),
                        Details::Axis<AxisType5>::get(values2, j),
                        Details::Axis<AxisType6>::get(values3, k),
                        Details::Data<3, DataType2>::get(result, i, j, k,
                                                         axisIndex1, axisIndex2,
                                                         axisIndex3)) ;//inner) ;
                }
            }
        }
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename DataType,
              typename AxisType4, typename AxisType5, typename AxisType6,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType4>::value
                                      && !Details::IsScalar<AxisType5>::value
                                      && !Details::IsScalar<AxisType6>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const AxisType3 & axis3, const DataType & data,
                    const AxisType4 & values1, const AxisType5 & values2,
                    const AxisType6 & values3, DataType2 && result)
    {
        DataType2 res(result) ;
        set(axis1, axis2, axis3, data, values1, values2, values3, res) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex, typename DataType,
              typename AxisType4, typename AxisType5, typename AxisType6,
              typename DataType2,
              typename std::enable_if<!Details::IsScalar<AxisType4>::value
                                      && !Details::IsScalar<AxisType5>::value
                                      && !Details::IsScalar<AxisType6>::value, int>::type=0>
    static void set(const AxisType1 & axis1, const AxisType2 & axis2,
                    const AxisType3 & axis3, const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2, const AxisIndex & axisIndex3,
                    const DataType & data,
                    const AxisType4 & values1, const AxisType5 & values2,
                    const AxisType6 & values3, DataType2 && result)
    {
        DataType2 res(result) ;
        set(axis1, axis2, axis3, axisIndex1, axisIndex2, axisIndex3,
            data, values1, values2, values3, res) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename DataType,
              typename AxisType4, typename AxisType5, typename AxisType6,
              typename std::enable_if<!Details::IsScalar<AxisType4>::value
                                      && !Details::IsScalar<AxisType5>::value
                                      && !Details::IsScalar<AxisType6>::value, int>::type=0>
    static typename Details::Data<3, DataType>::CopyType get(
                           const AxisType1 & axis1, const AxisType2 & axis2,
                           const AxisType3 & axis3, const DataType & data,
                           const AxisType4 & values1, const AxisType5 & values2,
                           const AxisType6 & values3
                                                            )
    {
        typename Details::Data<3, DataType>::CopyType result(
          Details::Data<3, DataType>::getResult(values1, values2, values3, data)
                                                            ) ;
        set(axis1, axis2, axis3, data, values1, values2, values3, result) ;
        return result ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
              typename AxisIndex, typename DataType,
              typename AxisType4, typename AxisType5, typename AxisType6,
              typename std::enable_if<!Details::IsScalar<AxisType4>::value
                                      && !Details::IsScalar<AxisType5>::value
                                      && !Details::IsScalar<AxisType6>::value, int>::type=0>
    static typename Details::Data<3, DataType>::CopyType get(
                           const AxisType1 & axis1, const AxisType2 & axis2,
                           const AxisType3 & axis3, const AxisIndex & axisIndex1,
                           const AxisIndex & axisIndex2, const AxisIndex & axisIndex3,
                           const DataType & data,
                           const AxisType4 & values1, const AxisType5 & values2,
                           const AxisType6 & values3
                                                            )
    {
        typename Details::Data<3, DataType>::CopyType result(
          Details::Data<3, DataType>::getResult(values1, values2, values3,
                                                axisIndex1, axisIndex2,
                                                axisIndex3, data)
                                                            ) ;
        set(axis1, axis2, axis3, axisIndex1, axisIndex2, axisIndex3,
            data, values1, values2, values3, result) ;
        return result ;
    }

} ;


typedef Interpolator<1, InterpScheme::LINEAR> Linear1D ;
typedef Interpolator<2, InterpScheme::LINEAR> Linear2D ;
typedef Interpolator<3, InterpScheme::LINEAR> Linear3D ;

} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Interpolators_Linear_hpp
