#pragma once

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <autodiff/forward/real.hpp>

#include <vector>
#include <type_traits>

namespace BV {
namespace Math {
namespace Interpolators {

/*!
 * \enum Enumeration specifying the different interpolation schemes
 */
enum InterpScheme {LINEAR} ;

/*!
 * \class Interpolator
 * \brief Interpolation class that should be specialized for the different
 *     interpolation schemes.
 * \tparam N: the order of the interpolation
 * \tparam Scheme: the scheme used for the interpolation
 *     Use enumeration InterpScheme to choose the scheme.
 */
template <std::size_t N, int Scheme>
struct Interpolator ;

template <typename ValueType>
inline std::vector<ValueType> operator+(const std::vector<ValueType> & v1,
                                        const std::vector<ValueType> & v2)
{
    std::size_t size(v1.size()) ;
    std::vector<ValueType> tmp(size) ;
    for (std::size_t i=0; i<size; ++i)
    {
        tmp[i] = v1[i] + v2[i] ;
    }
    return tmp ;
}

template <typename ValueType>
inline std::vector<ValueType> operator-(const std::vector<ValueType> & v1,
                                        const std::vector<ValueType> & v2)
{
    std::size_t size(v1.size()) ;
    std::vector<ValueType> tmp(size) ;
    for (std::size_t i=0; i<size; ++i)
    {
        tmp[i] = v1[i] - v2[i] ;
    }
    return tmp ;
}

template <typename ValueType>
inline std::vector<ValueType> operator*(const std::vector<ValueType> & v,
                                        const ValueType & val)
{
    std::vector<ValueType> tmp(v) ;
    for (std::size_t i=0; i<v.size(); ++i)
    {
        tmp[i] *= val ;
    }
    return tmp ;
}

template <typename ValueType>
inline std::vector<ValueType> operator/(const std::vector<ValueType> & v,
                                        const ValueType & val)
{
    std::vector<ValueType> tmp(v) ;
    for (std::size_t i=0; i<v.size(); ++i)
    {
        tmp[i] /= val ;
    }
    return tmp ;
}

//template <typename Derived, typename ValueType>
//inline Eigen::Tensor<typename Derived::Scalar, Derived::NumDimensions>
//operator*(const Eigen::TensorBase<Derived> & t, const ValueType & val)
//{
//    Eigen::Tensor<ValueType, Derived::NumDimensions> constTmp(t) ;
//    constTmp.setConstant(val) ;
//    return t * constTmp ;
//}
//
//template <typename Derived, typename ValueType>
//inline Eigen::Tensor<typename Derived::Scalar, Derived::NumDimensions>
//operator*(Eigen::TensorBase<Derived> && t, const ValueType & val)
//{
//    Eigen::Tensor<ValueType, Derived::NumDimensions> constTmp(t) ;
//    constTmp.setConstant(val) ;
//    return t * constTmp ;
//}

namespace Details {

namespace TypeCheck {

template <typename T>
std::true_type IsEigen(const Eigen::EigenBase<T>*) ;
std::false_type IsEigen(...) ;

template <typename T>
std::true_type IsTensor(const Eigen::TensorBase<T>*) ;
std::false_type IsTensor(...) ;

} // End of namespace TypeCheck

template <typename T>
using GetType = std::remove_cv<typename std::remove_reference<T>::type> ;

template <typename T>
struct IsEigen : decltype(TypeCheck::IsEigen(std::declval<typename GetType<T>::type*>()))
{
} ;

template <typename T>
struct IsTensor : decltype(TypeCheck::IsTensor(std::declval<typename GetType<T>::type*>()))
{
} ;

template <typename T>
struct IsScalar : std::is_scalar<T>
{
} ;

template <>
struct IsScalar<autodiff::real1st> : std::true_type
{
} ;

// Specialization of Axis for different types

template <typename AxisType_, class Enable=void>
struct Axis ;

template <typename ScalarType_>
struct Axis<ScalarType_,
            typename std::enable_if<IsScalar<ScalarType_>::value>::type>
{
public:
    typedef ScalarType_ ScalarType ;
    typedef ScalarType_ Type ;
    typedef std::size_t SizeType ;
    typedef std::size_t IndexType ;
    typedef ScalarType_ CopyType ;

    static SizeType size(const Type & axis)
    {
        return 1 ;
    }

    // FIXME depending on Eigen type we cannot return a const ScalarType &
    template <typename IndexType_>
    static ScalarType get(const Type & axis, const IndexType_ & ind)
    {
        return axis ;
    }
} ;


template <typename Derived>
struct Axis<Derived,
            typename std::enable_if<IsEigen<Derived>::value &&
                                    ((Derived::RowsAtCompileTime==1)
                                     || (Derived::ColsAtCompileTime==1))>::type>
{
public:
    typedef typename Derived::Scalar ScalarType ;
    typedef Derived Type ;
    typedef typename Derived::Index SizeType ;
    typedef typename Derived::Index IndexType ;
    typedef typename GetType<typename Eigen::internal::eval<Derived>::type>::type CopyType ;

    static SizeType size(const Type & axis)
    {
        return axis.size() ;
    }

    // FIXME depending on Eigen type we cannot return a const ScalarType &
    template <typename IndexType_>
    static ScalarType get(const Type & axis, const IndexType_ & ind)
    {
        return axis(ind) ;
    }

} ;

template <typename ScalarType_>
struct Axis<std::vector<ScalarType_>, void>
{
public:
    typedef ScalarType_ ScalarType ;
    typedef std::vector<ScalarType> Type ;
    typedef std::vector<ScalarType> CopyType ;
    typedef typename std::vector<ScalarType_>::size_type SizeType ;
    typedef typename std::vector<ScalarType_>::size_type IndexType ;

    static SizeType size(const Type & axis)
    {
        return axis.size() ;
    }

    template <typename IndexType_>
    static const ScalarType & get(const Type & axis, const IndexType_ & ind)
    {
        return axis[ind] ;
    }

} ;

template <typename Derived>
struct Axis<Derived,
            typename std::enable_if<IsTensor<Derived>::value &&
                                    Derived::NumDimensions==1>::type>
{
public:
    //using Derived = Eigen::Tensor<T, 1, Order> ;
    typedef typename Derived::Scalar ScalarType ;
    typedef Derived Type ;
    typedef Eigen::Tensor<ScalarType, 1> CopyType ;
    typedef typename Derived::Index SizeType ;
    typedef typename Derived::Index IndexType ;

    static SizeType size(const Type & axis)
    {
        return axis.size() ;
    }

    template <typename IndexType_>
    static const ScalarType & get(const Type & axis, const IndexType_ & ind)
    {
        return axis(ind) ;
    }
} ;

// Specialization of data for different types

template <std::size_t N, typename DataType, class Enable=void>
struct Data ;

template <typename ScalarType_>
struct Data<1, ScalarType_,
            typename std::enable_if<IsScalar<ScalarType_>::value>::type>
{
    typedef ScalarType_ ScalarType ;
    typedef ScalarType InnerType ;
    typedef ScalarType & InnerTypeRef ;
    typedef const ScalarType & InnerTypeConstRef ;
    typedef ScalarType Type ;
    typedef ScalarType CopyType ;

    template <typename AxisType>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero=false)
    {
        return CopyType(0.) ;
    }

    template <typename AxisType, typename AxisIndex>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex,
                              bool setZero = false)
    {
        return CopyType(0.) ;
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind)
    {
        return data ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind)
    {
        return result ;
    }

    template <typename AxisIndex, typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind,
                                 const AxisIndex & axisIndex)
    {
        return data ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        return result ;
    }

} ;

// Specialisation for Eigen Vector type
template <typename Derived>
struct Data<1, Derived,
            typename std::enable_if<IsEigen<Derived>::value &&
                                    ((Derived::RowsAtCompileTime==1)
                                     || (Derived::ColsAtCompileTime==1))>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    typedef ScalarType InnerType ;
    typedef ScalarType & InnerTypeRef ;
    typedef const ScalarType & InnerTypeConstRef ;
    typedef Derived Type ;
    typedef typename GetType<typename Eigen::internal::eval<Derived>::type>::type CopyType ;

    template <typename AxisType,
              typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        if (setZero)
        {
            return CopyType(CopyType::Zero(Axis<AxisType>::size(axis))) ;
        }
        return CopyType(Axis<AxisType>::size(axis)) ;
    }

    template <typename AxisType,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
        static ScalarType getResult(const AxisType & axis, const Type & data,
                                    bool setZero = false)
    {
        return ScalarType(0) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex, bool setZero = false)
    {
        if (setZero)
        {
            return CopyType(CopyType::Zero(Axis<AxisType>::size(axis))) ;
        }
        return CopyType(Axis<AxisType>::size(axis)) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static ScalarType getResult(const AxisType & axis, const Type & data,
                                const AxisIndex & axisIndex, bool setZero = false)
    {
        return ScalarType(0) ;
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind)
    {
        return data(ind) ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind)
    {
        return result(ind) ;
    }

    template <typename IndexType_, typename AxisIndex>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind,
                                 const AxisIndex & axisIndex)
    {
        return data(ind) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        return result(ind) ;
    }

} ;

// Specialisation for Eigen Matrix type
template <typename Derived>
struct Data<1, Derived,
            typename std::enable_if<IsEigen<Derived>::value &&
                                    ((Derived::RowsAtCompileTime!=1)
                                     && (Derived::ColsAtCompileTime!=1))>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    // FIXME this inner type is wrong !
    // It should not be a matrix (what about Array)
    // And it may be a row or column vector depending on the interpolation axis...
    // Shall we choose ?
    typedef Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> InnerType ;
    //typedef Eigen::Matrix<ScalarType, Derived::ColsAtCompileTime, 1> InnerType ;
    //typedef Eigen::Matrix<ScalarType, 1, Derived::ColsAtCompileTime> InnerType ;
    typedef Derived Type ;
    typedef typename GetType<typename Eigen::internal::eval<Derived>::type>::type CopyType ;
    //typedef Eigen::Matrix<ScalarType, Derived::RowsAtCompileTime,
    //                      Derived::ColsAtCompileTime> CopyType ;
    typedef typename Derived::ConstRowXpr InnerTypeConstRef ;
    typedef typename Derived::RowXpr InnerTypeRef ;

    template <typename AxisType,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        if (setZero)
        {
            return CopyType(CopyType::Zero(Axis<AxisType>::size(axis), data.cols())) ;
        }
        return CopyType(Axis<AxisType>::size(axis), data.cols()) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex,
                              bool setZero = false)
    {
        if (axisIndex == 0)
        {
            if (setZero)
            {
                return CopyType(CopyType::Zero(Axis<AxisType>::size(axis), data.cols())) ;
            }
            return CopyType(Axis<AxisType>::size(axis), data.cols()) ;
        }
        if (setZero)
        {
            return CopyType(CopyType::Zero(data.rows(), Axis<AxisType>::size(axis))) ;
        }
        return CopyType(data.rows(), Axis<AxisType>::size(axis)) ;
    }

    template <typename AxisType,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static InnerType getResult(const AxisType & axis, const Type & data,
                               bool setZero = false)
    {
        // FIXME wrong implementation, could be vector or array...
        if (setZero)
        {
            return InnerType(InnerType::Zero(data.cols())) ;
        }
        return InnerType(data.cols()) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static InnerType getResult(const AxisType & axis, const Type & data,
                               const AxisIndex & axisIndex, bool setZero = false)
    {
        // FIXME wrong implementation, could be row of column vector or array...
        if (axisIndex == 0)
        {
            if (setZero)
            {
                return InnerType(InnerType::Zero(data.cols())) ;
            }
            return InnerType(data.cols()) ;
        }
        if (setZero)
        {
            return InnerType(InnerType::Zero(data.rows())) ;
        }
        return InnerType(data.rows()) ;
    }

    template <typename IndexType_>
    static auto get(const Type & data, const IndexType_ & ind)
    {
        return data.row(ind) ;
    }

    template <typename ResultType, typename IndexType_>
    static auto get(ResultType & result, const IndexType_ & ind)
    {
        return result.row(ind) ;
    }

    template <typename IndexType_, typename AxisIndex>
    static auto get(const Type & data, const IndexType_ & ind,
                                 const AxisIndex & axisIndex)
    {
        if (axisIndex == 0)
        {
            return data.row(ind) ;
        }
        return data.col(ind) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static auto get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        if (axisIndex == 0)
        {
            return result.row(ind) ;
        }
        return result.col(ind) ;
    }

} ;

template <typename ScalarType_>
struct Data<1, std::vector<ScalarType_>,
            typename std::enable_if<IsScalar<ScalarType_>::value>::type>
{
    typedef ScalarType_ ScalarType ;
    typedef ScalarType InnerType ;
    typedef ScalarType & InnerTypeRef ;
    typedef const ScalarType & InnerTypeConstRef ;
    typedef std::vector<ScalarType> Type ;
    typedef std::vector<ScalarType> CopyType ;

    template <typename AxisType,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        if (setZero)
        {
            return CopyType(Axis<AxisType>::size(axis), 0.) ;
        }
        return CopyType(Axis<AxisType>::size(axis)) ;
    }

    template <typename AxisType,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static ScalarType getResult(const AxisType & axis, const Type & data,
                                bool setZero = false)
    {
        return ScalarType(0) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex, bool setZero = false)
    {
        if (setZero)
        {
            return CopyType(Axis<AxisType>::size(axis), 0.) ;
        }
        return CopyType(Axis<AxisType>::size(axis)) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static ScalarType getResult(const AxisType & axis, const Type & data,
                                const AxisIndex & axisIndex, bool setZero = false)
    {
        return ScalarType(0) ;
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind)
    {
        return data[ind] ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind)
    {
        return result[ind] ;
    }

    template <typename IndexType_, typename AxisIndex>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind,
                                 const AxisIndex & axisIndex)
    {
        return data[ind] ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        return result[ind] ;
    }

} ;

// Specialisation for a std::vector of Eigen Vector type
template <typename EigenType_>
struct Data<1, std::vector<EigenType_>,
            typename std::enable_if<IsEigen<EigenType_>::value &&
                                    ((EigenType_::RowsAtCompileTime==1)
                                     || (EigenType_::ColsAtCompileTime==1))>::type>
{
    typedef typename EigenType_::Scalar ScalarType ;
    typedef typename GetType<typename Eigen::internal::eval<EigenType_>::type>::type InnerType ;
    typedef typename GetType<typename Eigen::internal::eval<EigenType_>::type>::type & InnerTypeRef ;
    typedef const typename GetType<typename Eigen::internal::eval<EigenType_>::type>::type & InnerTypeConstRef ;
    typedef std::vector<EigenType_> Type ;
    typedef std::vector<typename GetType<typename Eigen::internal::eval<EigenType_>::type>::type> CopyType ;

    template <typename AxisType,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        std::vector<InnerType> res ;
        Eigen::Index nValues(data[0].size()) ;
        std::size_t nAxis(Axis<AxisType>::size(axis)) ;
        if (setZero)
        {
            for (std::size_t iRes=0; iRes<nAxis; ++iRes)
            {
                res.push_back(InnerType::Zero(nValues)) ;
            }
            return res ;
        }
        for (std::size_t iRes=0; iRes<nAxis; ++iRes)
        {
            res.push_back(InnerType(nValues)) ;
        }
        return res ;
    }

    template <typename AxisType,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static InnerType getResult(const AxisType & axis, const Type & data,
                                bool setZero = false)
    {
        Eigen::Index nValues(data[0].size()) ;
        if (setZero)
        {
            return InnerType::Zero(nValues) ;
        }
        return InnerType(nValues) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex, bool setZero = false)
    {
        std::vector<InnerType> res ;
        Eigen::Index nValues(data[0].size()) ;
        std::size_t nAxis(Axis<AxisType>::size(axis)) ;
        if (setZero)
        {
            for (std::size_t iRes=0; iRes<nAxis; ++iRes)
            {
                res.push_back(InnerType::Zero(nValues)) ;
            }
            return res ;
        }
        for (std::size_t iRes=0; iRes<nAxis; ++iRes)
        {
            res.push_back(InnerType(nValues)) ;
        }
        return res ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static ScalarType getResult(const AxisType & axis, const Type & data,
                                const AxisIndex & axisIndex, bool setZero = false)
    {
        Eigen::Index nValues(data[0].size()) ;
        if (setZero)
        {
            return InnerType::Zero(nValues) ;
        }
        return InnerType(nValues) ;
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind)
    {
        return data[ind] ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind)
    {
        return result[ind] ;
    }

    template <typename IndexType_, typename AxisIndex>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind,
                                 const AxisIndex & axisIndex)
    {
        return data[ind] ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        return result[ind] ;
    }

} ;

template <typename ScalarType_>
struct Data<1, std::vector<std::vector<ScalarType_> >, void>
{
    typedef ScalarType_ ScalarType ;
    typedef std::vector<ScalarType> InnerType ;
    typedef std::vector<ScalarType> & InnerTypeRef ;
    typedef const std::vector<ScalarType> & InnerTypeConstRef ;
    typedef std::vector<std::vector<ScalarType> > Type ;
    typedef std::vector<std::vector<ScalarType> > CopyType ;

    template <typename AxisType,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        using SizeType = typename Axis<AxisType>::SizeType ;
        SizeType size(Axis<AxisType>::size(axis)) ;
        CopyType tmp(size) ;
        for (SizeType i=0; i<size; ++i)
        {
            InnerType inner(data[0].size(), 0.) ;
            tmp[i] = inner ;
        }
        return tmp ;
    }

    template <typename AxisType,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static InnerType getResult(const AxisType & axis, const Type & data,
                               bool setZero = false)
    {
        InnerType inner(data[0].size(), 0.) ;
        return inner;
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & ind)
    {
        return data[ind] ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & ind)
    {
        return result[ind] ;
    }

    // Cannot interpolate along 2nd axis !!
} ;

template <typename Derived>
struct Data<1, Derived, typename std::enable_if<IsTensor<Derived>::value &&
                                                Derived::NumDimensions==1>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    typedef ScalarType InnerType ;
    typedef ScalarType & InnerTypeRef ;
    typedef const ScalarType & InnerTypeConstRef ;
    typedef Derived Type ;
    typedef Eigen::Tensor<ScalarType, 1> CopyType ;

    template <typename AxisType,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        typename CopyType::Dimensions shape(ref.dimensions()) ;
        shape[0] = Axis<AxisType>::size(axis) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static InnerType getResult(const AxisType & axis, const Type & data,
                               bool setZero = false)
    {
        return InnerType(0) ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex, bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        typename CopyType::Dimensions shape(ref.dimensions()) ;
        shape[0] = Axis<AxisType>::size(axis) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    static InnerType getResult(const AxisType & axis, const Type & data,
                               const AxisIndex & axisIndex, bool setZero = false)
    {
        return InnerType(0) ;
    }

    template <typename IndexType_>
    static auto get(const Type & data, const IndexType_ & ind)
    {
        return data(ind) ;
    }

    template <typename ResultType, typename IndexType_>
    static auto get(ResultType & result, const IndexType_ & ind)
    {
        return result(ind) ;
    }

    template <typename AxisIndex, typename IndexType_>
    static auto get(const Type & data, const IndexType_ & ind,
                    const AxisIndex & axisIndex)
    {
        return data(ind) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static auto get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        return result(ind) ;
    }

} ;

template <typename Derived>
struct Data<1, Derived, typename std::enable_if<IsTensor<Derived>::value &&
                                                Derived::NumDimensions>=2>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    enum {
        Rank = Derived::NumDimensions
    } ;
    typedef Eigen::Tensor<ScalarType, Rank-1> InnerType ;
    typedef Eigen::TensorRef<Eigen::Tensor<ScalarType, Rank-1> > InnerTypeRef ;
    typedef Eigen::TensorRef<const Eigen::Tensor<ScalarType, Rank-1> > InnerTypeConstRef ;
    typedef Derived Type ;
    typedef Eigen::Tensor<ScalarType, Rank> CopyType ;

    template <typename AxisType>//,
        //typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        typename CopyType::Dimensions shape(ref.dimensions()) ;
        shape[0] = Axis<AxisType>::size(axis) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    //template <typename AxisType,
    //    typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
    //    static InnerType getResult(const AxisType & axis, const Type & data,
    //                               bool setZero = false)
    //{
    //    const Eigen::TensorRef<const CopyType> & ref(data) ;
    //    Eigen::DSizes<typename InnerType::Index, Rank-1> newShape ;
    //    for (typename Derived::Index i=1; i<Derived::NumDimensions; ++i)
    //    {
    //        newShape[i-1] = ref.dimension(i) ;
    //    }
    //    InnerType res(newShape) ;
    //    if (setZero)
    //    {
    //        res.setZero() ;
    //    }
    //    return res ;
    //}

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType>::value, int>::type = 0>
    static CopyType getResult(const AxisType & axis, const Type & data,
                              const AxisIndex & axisIndex,
                              bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        typename CopyType::Dimensions shape(ref.dimensions()) ;
        //data.eval() ;
        //shape = ref.dimensions() ;
        shape[axisIndex] = Axis<AxisType>::size(axis) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType>::value, int>::type = 0>
        static InnerType getResult(const AxisType & axis, const Type & data,
                                   const AxisIndex & axisIndex, bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        Eigen::DSizes<typename InnerType::Index, Rank - 1> newShape ;
        typename Derived::Index iNew(0) ;
        for (typename Derived::Index i = 0; i < Derived::NumDimensions; ++i)
        {
            if (i == axisIndex)
            {
                continue ;
            }
            newShape[iNew] = ref.dimension(i) ;
            ++iNew ;
        }
        InnerType res(newShape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename IndexType_>
    static auto get(const Type & data, const IndexType_ & ind)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        return ref.chip(ind, 0) ;
    }

    template <typename ResultType, typename IndexType_>
    static auto get(ResultType & result, const IndexType_ & ind)
    {
        return result.chip(ind, 0) ;
    }

    template <typename IndexType_, typename AxisIndex>
    static auto get(const Type & data, const IndexType_ & ind,
                    const AxisIndex & axisIndex)
    {
        Eigen::DSizes<typename Type::Index, Rank> offsets ;
        Eigen::DSizes<typename Type::Index, Rank> extents ;
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        typename CopyType::Dimensions dims(ref.dimensions()) ;
        for (AxisIndex i=0; i<Rank; ++i)
        {
            if (i == axisIndex)
            {
                offsets[i] = ind ;
                extents[i] = 1 ;
                continue ;
            }
            offsets[i] = 0 ;
            extents[i] = dims[i] ;
        }
        return data.slice(offsets, extents) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static auto get(ResultType & result, const IndexType_ & ind,
                            const AxisIndex & axisIndex)
    {
        Eigen::DSizes<typename ResultType::Index, Rank> offsets ;
        Eigen::DSizes<typename ResultType::Index, Rank> extents ;
        typename Type::Dimensions dims(result.dimensions()) ;
        for (AxisIndex i=0; i<Rank; ++i)
        {
            if (i == axisIndex)
            {
                offsets[i] = ind ;
                extents[i] = 1 ;
                continue ;
            }
            offsets[i] = 0 ;
            extents[i] = dims[i] ;
        }
        return result.slice(offsets, extents) ;
    }

} ;

template <typename Derived>
struct Data<2, Derived,
            typename std::enable_if<IsEigen<Derived>::value &&
                                    ((Derived::RowsAtCompileTime!=1)
                                     && (Derived::ColsAtCompileTime!=1))>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    typedef ScalarType InnerType ;
    typedef ScalarType & InnerTypeRef ;
    typedef const ScalarType & InnerTypeConstRef ;
    typedef Derived Type ;
    typedef typename GetType<typename Eigen::internal::eval<Derived>::type>::type CopyType ;
    //typedef Eigen::Matrix<ScalarType, Derived::RowsAtCompileTime,
    //                      Derived::ColsAtCompileTime> CopyType ;

    template <typename AxisType1, typename AxisType2,
              typename std::enable_if<!IsScalar<AxisType1>::value
                                      && !IsScalar<AxisType2>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const Type & data, bool setZero = false)
    {
        if (setZero)
        {
            return CopyType(CopyType::Zero(Axis<AxisType1>::size(axis1),
                Axis<AxisType2>::size(axis2))) ;
        }
        return CopyType(Axis<AxisType1>::size(axis1),
                        Axis<AxisType2>::size(axis2)) ;
    }

    template <typename AxisType1, typename AxisType2,
        typename std::enable_if<IsScalar<AxisType1>::value
                                && IsScalar<AxisType2>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const Type & data, bool setZero = false)
    {
        return InnerType(0) ;
    }

    // FIXME return type of getResult if one of the axis is a scalar and not the other
    // is not easy to determine because of the row/col dimensionality and because
    // of the type of Eigen object to return Matrix/Array...

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType1>::value
        && !IsScalar<AxisType2>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const AxisIndex & axisIndex1,
                              const AxisIndex & axisIndex2,
                              const Type & data, bool setZero = false)
    {
        // FIXME assume this axis indices are in the correct order...
        if (axisIndex1 < axisIndex2)
        {
            if (setZero)
            {
                return CopyType(CopyType::Zero(Axis<AxisType1>::size(axis1),
                    Axis<AxisType2>::size(axis2))) ;
            }
            return CopyType(Axis<AxisType1>::size(axis1),
                            Axis<AxisType2>::size(axis2)) ;
        }
        return CopyType(Axis<AxisType2>::size(axis2),
                        Axis<AxisType1>::size(axis1)) ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType1>::value
        && IsScalar<AxisType2>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const AxisIndex & axisIndex1,
                               const AxisIndex & axisIndex2,
                               const Type & data, bool setZero = false)
    {
        return InnerType(0) ;
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & i,
                                 const IndexType_ & j)
    {
        return data(i, j) ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & i,
                            const IndexType_ & j)
    {
        return result(i, j) ;
    }

    template <typename IndexType_, typename AxisIndex>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & i,
                                 const IndexType_ & j,
                                 const AxisIndex & axisIndex1,
                                 const AxisIndex & axisIndex2)
    {
        return data(i, j) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static InnerTypeRef get(ResultType & result, const IndexType_ & i,
                            const IndexType_ & j,
                            const AxisIndex & axisIndex1,
                            const AxisIndex & axisIndex2)
    {
        return result(i, j) ;
    }
} ;

template <typename ScalarType_>
struct Data<2, std::vector<std::vector<ScalarType_> >, void>
{
    typedef ScalarType_ ScalarType ;
    typedef ScalarType InnerType ;
    typedef ScalarType & InnerTypeRef ;
    typedef const ScalarType & InnerTypeConstRef ;
    typedef std::vector<std::vector<ScalarType> > Type ;
    typedef std::vector<std::vector<ScalarType> > CopyType ;

    template <typename AxisType1, typename AxisType2,
        typename std::enable_if<!IsScalar<AxisType1>::value
        && !IsScalar<AxisType2>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const Type & data, bool setZero = false)
    {
        std::size_t size(Axis<AxisType1>::size(axis1)) ;
        std::size_t size2(Axis<AxisType2>::size(axis2)) ;
        CopyType tmp(size) ;
        for (std::size_t i=0; i<size; ++i)
        {
            std::vector<ScalarType> inner(size2, 0.) ;
            tmp[i] = inner ;
        }
        return tmp ;
    }

    template <typename AxisType1, typename AxisType2,
        typename std::enable_if<IsScalar<AxisType1>::value
        && IsScalar<AxisType2>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const Type & data, bool setZero = false)
    {
        return InnerType(0);
    }

    template <typename IndexType_>
    static InnerTypeConstRef get(const Type & data,
                                 const IndexType_ & i,
                                 const IndexType_ & j)
    {
        return data[i][j] ;
    }

    template <typename ResultType, typename IndexType_>
    static InnerTypeRef get(ResultType & result, const IndexType_ & i,
                            const IndexType_ & j)
    {
        return result[i][j] ;
    }

    template <typename IndexType_, typename AxisIndex>
    static InnerTypeConstRef get(const Type & data, const IndexType_ & i,
                                 const IndexType_ & j,
                                 const AxisIndex & axisIndex1,
                                 const AxisIndex & axisIndex2)
    {
        return data[i][j] ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static InnerTypeRef get(ResultType & result, const IndexType_ & i,
                            const IndexType_ & j,
                            const AxisIndex & axisIndex1,
                            const AxisIndex & axisIndex2)
    {
        return result[i][j] ;
    }
} ;

template <typename Derived>
struct Data<2, Derived,
            typename std::enable_if<IsTensor<Derived>::value &&
                                    //Derived::NumDimensions>=3>::type>
                                    Derived::NumDimensions>=2>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    enum {
        Rank = Derived::NumDimensions
    } ;
    typedef Eigen::Tensor<ScalarType, Rank-2> InnerType ;
    typedef Eigen::TensorRef<Eigen::Tensor<ScalarType, Rank-2> > InnerTypeRef ;
    typedef Eigen::TensorRef<const Eigen::Tensor<ScalarType, Rank-2> > InnerTypeConstRef ;
    typedef Derived Type ;
    typedef Eigen::Tensor<ScalarType, Rank> CopyType ;

    template <typename AxisType1, typename AxisType2,
        typename std::enable_if<!IsScalar<AxisType1>::value
        && !IsScalar<AxisType2>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const Type & data, bool setZero = false)
    {
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[0] = Axis<AxisType1>::size(axis1) ;
        shape[1] = Axis<AxisType2>::size(axis2) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2,
        typename std::enable_if<IsScalar<AxisType1>::value
        && IsScalar<AxisType2>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const Type & data, bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        Eigen::DSizes<typename InnerType::Index, Rank - 2> newShape ;
        for (typename Derived::Index i = 2; i < Derived::NumDimensions; ++i)
        {
            newShape[i - 2] = ref.dimension(i) ;
        }
        InnerType res(newShape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType1>::value
        && !IsScalar<AxisType2>::value, int>::type = 0>
    static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                              const AxisIndex & axisIndex1,
                              const AxisIndex & axisIndex2,
                              const Type & data, bool setZero = false)
    {
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[axisIndex1] = Axis<AxisType1>::size(axis1) ;
        shape[axisIndex2] = Axis<AxisType2>::size(axis2) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisIndex,
        typename std::enable_if<IsScalar<AxisType1>::value
        && IsScalar<AxisType2>::value, int>::type = 0>
    static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                               const AxisIndex & axisIndex1,
                               const AxisIndex & axisIndex2,
                               const Type & data, bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        Eigen::DSizes<typename InnerType::Index, Rank - 2> newShape ;
        typename Derived::Index iNew(0) ;
        for (typename Derived::Index i = 0; i < Derived::NumDimensions; ++i)
        {
            if ((i == axisIndex1) || (i == axisIndex2))
            {
                continue ;
            }
            newShape[iNew] = ref.dimension(i) ;
            ++iNew;
        }
        InnerType res(newShape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename IndexType_>
    static auto get(const Type & data, const IndexType_ & i, const IndexType_ & j)
    {
        return data.chip(i, 0).chip(j, 0) ;
    }

    template <typename ResultType, typename IndexType_>
    static auto get(ResultType & result, const IndexType_ & i, const IndexType_ & j)
    {
        return result.chip(i, 0).chip(j, 0) ;
    }

    template <typename AxisIndex, typename IndexType_>
    static auto get(const Type & data, const IndexType_ & i,
                    const IndexType_ & j,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2)
    {
        if (axisIndex1 < axisIndex2)
        {
            return data.chip(i, axisIndex1).chip(j, axisIndex2 - 1) ;
        }
        return data.chip(j, axisIndex2).chip(i, axisIndex1 - 1) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static auto get(ResultType & result, const IndexType_ & i,
                    const IndexType_ & j,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2)
    {
        if (axisIndex1 < axisIndex2)
        {
            return result.chip(i, axisIndex1).chip(j, axisIndex2 - 1) ;
        }
        return result.chip(j, axisIndex2).chip(i, axisIndex1 - 1) ;
    }
} ;

template <typename Derived>
struct Data<3, Derived,
    typename std::enable_if<IsTensor<Derived>::value &&
                            Derived::NumDimensions >= 3>::type>
{
    typedef typename Derived::Scalar ScalarType ;
    enum {
        Rank = Derived::NumDimensions
    } ;
    typedef Eigen::Tensor<ScalarType, Rank - 3> InnerType ;
    typedef Eigen::TensorRef<Eigen::Tensor<ScalarType, Rank - 3> > InnerTypeRef ;
    typedef Eigen::TensorRef<const Eigen::Tensor<ScalarType, Rank - 3> > InnerTypeConstRef ;
    typedef Derived Type ;
    typedef Eigen::Tensor<ScalarType, Rank> CopyType ;

    template <typename AxisType1, typename AxisType2, typename AxisType3,
        typename std::enable_if<!IsScalar<AxisType1>::value
        && !IsScalar<AxisType2>::value
        && !IsScalar<AxisType3>::value, int>::type = 0>
        static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                                  const AxisType3 & axis3, const Type & data,
                                  bool setZero = false)
    {
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[0] = Axis<AxisType1>::size(axis1) ;
        shape[1] = Axis<AxisType2>::size(axis2) ;
        shape[2] = Axis<AxisType3>::size(axis3) ;
        CopyType res(shape);
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
        typename std::enable_if<IsScalar<AxisType1>::value
        && IsScalar<AxisType2>::value
        && IsScalar<AxisType3>::value, int>::type = 0>
        static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                                   const AxisType3 & axis3, const Type & data,
                                   bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        Eigen::DSizes<typename InnerType::Index, Rank - 3> newShape ;
        for (typename Derived::Index i = 3; i < Derived::NumDimensions; ++i)
        {
            newShape[i - 3] = ref.dimension(i) ;
        }
        InnerType res(newShape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
        typename AxisIndex,
        typename std::enable_if<!IsScalar<AxisType1>::value
        && !IsScalar<AxisType2>::value
        && !IsScalar<AxisType3>::value, int>::type = 0>
        static CopyType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                                  const AxisType3 & axis3,
                                  const AxisIndex & axisIndex1,
                                  const AxisIndex & axisIndex2,
                                  const AxisIndex & axisIndex3,
                                  const Type & data, bool setZero = false)
    {
        typename Type::Dimensions shape(data.dimensions()) ;
        shape[axisIndex1] = Axis<AxisType1>::size(axis1) ;
        shape[axisIndex2] = Axis<AxisType2>::size(axis2) ;
        shape[axisIndex3] = Axis<AxisType3>::size(axis3) ;
        CopyType res(shape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename AxisType1, typename AxisType2, typename AxisType3,
    typename AxisIndex,
    typename std::enable_if<IsScalar<AxisType1>::value
    && IsScalar<AxisType2>::value
    && IsScalar<AxisType3>::value, int>::type = 0>
        static InnerType getResult(const AxisType1 & axis1, const AxisType2 & axis2,
                                   const AxisType3 & axis3,
                                   const AxisIndex & axisIndex1,
                                   const AxisIndex & axisIndex2,
                                   const AxisIndex & axisIndex3,
                                   const Type & data, bool setZero = false)
    {
        const Eigen::TensorRef<const CopyType> & ref(data) ;
        Eigen::DSizes<typename InnerType::Index, Rank - 3> newShape ;
        typename Derived::Index iNew(0) ;
        for (typename Derived::Index i = 0; i < Derived::NumDimensions; ++i)
        {
            if ((i == axisIndex1) || (i == axisIndex2) || (i == axisIndex3))
            {
                continue ;
            }
            newShape[iNew] = ref.dimension(i) ;
            ++iNew;
        }
        InnerType res(newShape) ;
        if (setZero)
        {
            res.setZero() ;
        }
        return res ;
    }

    template <typename DataType, typename IndexType_>
    static auto get(const DataType & data, const IndexType_ & i,
                    const IndexType_ & j, const IndexType_ & k)
    {
        return data.chip(i, 0).chip(j, 0).chip(k, 0) ;
    }

    template <typename ResultType, typename IndexType_>
    static auto get(ResultType & result, const IndexType_ & i,
                    const IndexType_ & j, const IndexType_ & k)
    {
        return result.chip(i, 0).chip(j, 0).chip(k, 0) ;
    }

    template <typename IndexType_, typename AxisIndex>
    static auto get(const Type & data, const IndexType_ & i,
                    const IndexType_ & j, const IndexType_ & k,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2,
                    const AxisIndex & axisIndex3)
    {
        Eigen::DSizes<typename Type::Index, Rank> offsets ;
        Eigen::DSizes<typename Type::Index, Rank> extents ;
        typename Type::Dimensions dims(data.dimensions()) ;
        for (AxisIndex ind = 0; ind < Rank; ++ind)
        {
            if (ind == axisIndex1)
            {
                offsets[ind] = i ;
                extents[ind] = 1 ;
                continue ;
            }
            else if (ind == axisIndex2)
            {
                offsets[ind] = j ;
                extents[ind] = 1 ;
                continue ;
            }
            else if (ind == axisIndex3)
            {
                offsets[ind] = k ;
                extents[ind] = 1 ;
                continue ;
            }
            offsets[ind] = 0 ;
            extents[ind] = dims[ind] ;
        }
        return data.slice(offsets, extents) ;
    }

    template <typename ResultType, typename IndexType_, typename AxisIndex>
    static auto get(ResultType & result, const IndexType_ & i,
                    const IndexType_ & j, const IndexType_ & k,
                    const AxisIndex & axisIndex1,
                    const AxisIndex & axisIndex2,
                    const AxisIndex & axisIndex3)
    {
        Eigen::DSizes<typename ResultType::Index, Rank> offsets ;
        Eigen::DSizes<typename ResultType::Index, Rank> extents ;
        typename Type::Dimensions dims(result.dimensions()) ;
        for (AxisIndex ind = 0; ind < Rank; ++ind)
        {
            if (ind == axisIndex1)
            {
                offsets[ind] = i ;
                extents[ind] = 1 ;
                continue ;
            }
            else if (ind == axisIndex2)
            {
                offsets[ind] = j ;
                extents[ind] = 1 ;
                continue ;
            }
            else if (ind == axisIndex3)
            {
                offsets[ind] = k ;
                extents[ind] = 1 ;
                continue ;
            }
            offsets[ind] = 0 ;
            extents[ind] = dims[ind] ;
        }
        return result.slice(offsets, extents) ;
    }
} ;

} // End of namespace Details

} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV
