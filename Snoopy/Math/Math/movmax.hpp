#include <Eigen/Dense>
#include <vector>
#include "MathToolsExport.hpp"
namespace BV {
namespace Math {

// template<typename T>
// std::vector<T> movmax(std::vector<T> data, int k);

	MATHTOOLS_API Eigen::ArrayXd movmax(Eigen::ArrayXd data, int k, int location = 0);

}
}