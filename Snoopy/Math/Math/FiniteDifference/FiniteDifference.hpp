#ifndef BV_Math_FiniteDifference_FiniteDifference_hpp
#define BV_Math_FiniteDifference_FiniteDifference_hpp

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <cmath>
#include <functional>
#include <map>
#include <type_traits>

#include "Math/FiniteDifference/Details.hpp"
#include "Math/FiniteDifference/Typedefs.hpp"

namespace BV {
namespace Math {
namespace FiniteDifference {

/*!
 * \class FiniteDifference Math/FiniteDifference/FiniteDifference.hpp
 * \brief Performs finite differences on a set of input.
 *     Different use cases are considered, depending on the context.
 *     More information on the different types is provided in the documentation
 *     of the specialized structures.
 *
 * \tparam Scheme: the scheme to use for the finite difference (central,
 *     forward, backward). An enumeration is provided in Typedefs.hpp for
 *     clarification on the indices to use. See FDScheme.
 * \tparam IDer: the ith derivative to compute. For the moment only the first
 *     and second derivatives are implemented. An enumeration is provided
 *     in Typedefs.hpp for clarification. See IthDerivative.
 * \tparam Order: the order of the finite difference scheme. Implemented
 *     orders are in the range 1 to 8. Note that only even orders are
 *     implemented for the central finite difference.
 * \tparam Context: the context in which to calculate the finite difference.
 *     Please refer to the specialized classes for more information.
 *     An enumeration is provided in Typedefs.hpp for clarification on the
 *     indices used. See FDContext.
 */
template <int Scheme, int IDer, int Order, int Context=FDContext::FIXED_SIZE_IO>
struct FiniteDifference ;

/*!
 * \brief Specialization of the FiniteDifference structure in case of
 *     the first derivative for fixed size input/output types.
 *     Typically this applies to BV::Math::Functions types. However user defined
 *     functors should also work, provided these functors define an
 *     OutputType typedef and a call operator.
 * \tparam Scheme: the scheme to use for the finite difference. Use the
 *     enumeration FDScheme.
 * \tparam Order: the order of the finite difference.
 */
template <int Scheme, int Order>
struct FiniteDifference<Scheme, IthDerivative::FIRST, Order,
                        FDContext::FIXED_SIZE_IO>
{
    /*!
     * \brief Computes the finite difference for fixed size input/output.
     *     Template parameters should be automatically deduced by the compiler.
     * \param x: the values for which to evaluate the function.
     *     This type should be a scalar or vector of known size at compile time.
     * \param f: a functor providing a call operator that evaluates in x.
     *     The functor should also provide a typedef named OutputType, which
     *     provides the type of the function output. Note that the dimensions
     *     of the output should be known at compile time.
     * \param h: the deltas to apply on the x input to compute the finite
     *     difference.
     *
     * Example for a central finite difference of order 2:
     *
     * \code{.cpp}
     *
     *     #include <Eigen/Dense>
     *
     *     #include "Math/FiniteDifference/FiniteDifference.hpp"
     *     #include "Math/FiniteDifference/Typedefs.hpp"
     *
     *     typedef FiniteDifference<FDScheme::CENTRAL,
     *                              IthDerivative::FIRST, 2,
     *                              FDContext::FIXED_SIZE_IO> Type ;
     *     using BV::Math::Functions::Analytical ;
     *     // Here we evaluate a function of R2 to R3
     *     Analytical<2, 3> func({"2.*x**2+3.*y+4.", "3.*x**2", "2.*y**2"},
     *                           {"x", "y"}) ;
     *     Eigen::Vector2d input ;
     *     input << 3., 4. ;
     *     Eigen::Vector2d h ;
     *     h << 1.e-6, 1.e-6 ;
     *     Eigen::Matrix<double, 3, 2> res(Type::get(input, func, h)) ;
     *
     * \endcode
     *
     * This example computes the following central finite differences for
     * each of the 3 functions:
     * \f[ \frac{df_n}{dx}(x, y) = \frac{f_n(x+h, y) - f_n(x-h, y)}{2.*h} \f]
     * \f[ \frac{df_n}{dy}(x, y) = \frac{f_n(x, y+h) - f_n(x, y-h)}{2.*h} \f]
     * With \f[ f_1(x, y) = 2 x^2 + 3 y + 4 \f]
     *      \f[ f_2(x, y) = 3 x^2 \f]
     *      \f[ f_3(x, y) = 2 y^2 \f]
     *
     * The finite differences then approximate the derivatives for \f$x=3\f$
     * and \f$y=4\f$:
     * \f[ \frac{df_1}{dx}(x, y) = 4 x = 12 \f]
     * \f[ \frac{df_1}{dy}(x, y) = 3\f]
     * \f[ \frac{df_2}{dx}(x, y) = 6 x = 18 \f]
     * \f[ \frac{df_2}{dy}(x, y) = 0 \f]
     * \f[ \frac{df_3}{dx}(x, y) = 0 \f]
     * \f[ \frac{df_3}{dy}(x, y) = 4 y = 16 \f]
     *
     */
    template <typename InputType, typename Functor, typename DeltaType>
    static typename Details::Traits<InputType, typename Functor::OutputType,
                                    IthDerivative::FIRST>::OutputType
        get(const InputType & x, Functor & f, const DeltaType & h)
    {
        typedef Details::Traits<InputType, typename Functor::OutputType,
                                IthDerivative::FIRST> TraitsType ;
        typename TraitsType::OutputType res(TraitsType::getOutputZero()) ;
        typename TraitsType::InputType xTmp(x) ;
        for (typename TraitsType::IndexType i=0; i<TraitsType::sizeInput(); ++i)
        {
            auto it(Details::Coefs<Scheme, IthDerivative::FIRST,
                                   Order>::coefs.begin()) ;
            auto itEnd(Details::Coefs<Scheme, IthDerivative::FIRST,
                                      Order>::coefs.end()) ;
            for (; it!=itEnd; ++it)
            {
                TraitsType::add(xTmp, i, it->first*TraitsType::get(h, i)) ;
                TraitsType::addRes(res, i, it->second * f(xTmp)) ;
                TraitsType::subtract(xTmp, i, it->first*TraitsType::get(h, i)) ;
            }
            TraitsType::divideRes(res, i, TraitsType::get(h, i)) ;
        }
        return res ;
    }

    /*!
     * \brief Overload of the get function with a default value for the deltas.
     *     Refer to the other get function documentation for more details.
     */
    template <typename InputType, typename Functor,
              typename std::enable_if<!Details::IsScalar<InputType>::value,
                                      int>::type=0>
    static typename Details::Traits<InputType, typename Functor::OutputType,
                                    IthDerivative::FIRST>::OutputType
        get(const InputType & x, Functor & f, const double & h=1.e-6)
    {
        typedef Details::Traits<InputType, typename Functor::OutputType,
                                IthDerivative::FIRST> TraitsType ;
        return get(x, f, TraitsType::getInputConstant(h)) ;
    }

} ;

/*!
 * \brief Specialization of the FiniteDifference structure in case of
 *     a Jacobian calculation.
 *     Typically this applies to functions of N->N.
 * \tparam Scheme: the scheme to use for the finite difference. Use the
 *     enumeration FDScheme.
 * \tparam Order: the order of the finite difference.
 */
template <int Scheme, int Order>
struct FiniteDifference<Scheme, IthDerivative::FIRST, Order,
                        FDContext::JACOBIAN_CALCULATION>
{
    /*!
     * \brief Calculation of the Jacobian of f (N->N function).
     * \param x: the function input vector of size N
     * \param f: the function which evaluates at x.
     *     This function should return a vector of size N.
     * \param h: the deltas used to calculate the finite differences.
     *
     * Example:
     *
     * \code{.cpp}
     *
     *     #include <functional>
     *     #include <Eigen/Dense>
     *
     *     #include "Math/FiniteDifference/FiniteDifference.hpp"
     *
     *     typedef FiniteDifference<FDScheme::FORWARD,
     *                              IthDerivative::FIRST, 2,
     *                              FDContext::JACOBIAN_CALCULATION> Type ;
     *     struct Functor
     *     {
     *         Eigen::VectorXd operator()(const Eigen::VectorXd & in)
     *         {
     *             return get(in) ;
     *         }

     *         Eigen::VectorXd eval(const Eigen::VectorXd & in)
     *         {
     *             return get(in) ;
     *         }

     *         static Eigen::VectorXd get(const Eigen::VectorXd & in)
     *         {
     *             return 2. * in ;
     *         }
     *     } ;
     *     Eigen::VectorXd input(5) ;
     *     input << 1., 2., 3., 4., 5. ;
     *     Eigen::VectorXd h(5) ;
     *     h << 1.e-6, 1.e-6, 1.e-6, 1.e-6, 1.e-6 ;
     *     Functor func ;
     *     // First way to evaluate the Jacobian with this functor
     *     Eigen::MatrixXd jac(Type::get(input, func, h)) ;
     *     // Another way to evaluate the Jacobian with this functor
     *     Eigen::MatrixXd jac2(Type::get(input, Functor::get, h)) ;
     *     // Last way to evaluate the Jacobian with this functor
     *     std::function<Eigen::VectorXd(const Eigen::VectorXd &)> f(
     *                std::bind(&Functor::eval, func, std::placeholders::_1)
     *                                                              ) ;
     *     Eigen::MatrixXd jac3(Type::get(input, f)) ;
     *
     * \endcode
     *
     * The return type is of the following form:
     * Given \f[ x = [x_1, x_2, ..., x_n] \f]
     * and \f[ f(x_1, x_2, ..., x_n) = [f_1, f_2, ..., f_n] \f]
     *
     * We obtain an approximation of:
     *
     * \f[
     *     J = \begin{bmatrix}
     *          \frac{\partial f_1}{\partial x_1} & \frac{\partial f_1}{\partial x_2} & ... & \frac{\partial f_1}{\partial x_n} \\
     *          \frac{\partial f_2}{\partial x_1} & \frac{\partial f_2}{\partial x_2} & ... & \frac{\partial f_2}{\partial x_n} \\
     *          ... & ... & ... & ... \\
     *          \frac{\partial f_n}{\partial x_1} & \frac{\partial f_n}{\partial x_2} & ... & \frac{\partial f_n}{\partial x_n} \\
     *                 \end{bmatrix}
     * \f]
     *
     *
     */
    template <typename Derived, typename Functor, typename Derived2>
    static typename Details::JacobianTypeGetter<IthDerivative::FIRST, Derived,
                                                Derived::RowsAtCompileTime,
                                                Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & x, Functor & f,
            const Eigen::MatrixBase<Derived2> & h)
    {
        using SizeType = typename Eigen::MatrixBase<Derived>::Index ;
        SizeType size(x.size()) ;
        typedef typename Details::JacobianTypeGetter<
                                     IthDerivative::FIRST, Derived,
                                     Derived::RowsAtCompileTime,
                                     Derived::ColsAtCompileTime
                                                    >::Type MatType ;
        MatType res(MatType::Zero(size, size)) ;
        typename Details::InputTypeGetter<Derived,
                                          Derived::RowsAtCompileTime,
                                          Derived::ColsAtCompileTime>::Type xTmp(x) ;
        for (SizeType i=0; i<size; ++i)
        {
            auto it(Details::Coefs<Scheme, IthDerivative::FIRST,
                                   Order>::coefs.begin()) ;
            auto itEnd(Details::Coefs<Scheme, IthDerivative::FIRST,
                                      Order>::coefs.end()) ;
            for (; it!=itEnd; ++it)
            {
                xTmp(i) += it->first * h(i) ;
                res.col(i) += it->second * f(xTmp) ;
                xTmp(i) -= it->first * h(i) ;
            }
            res.col(i) /= h(i) ;
        }
        return res ;
    }

    /*!
     * \brief Overload of get with a default value for the deltas.
     *     Please refer to the get method documentation for more details.
     */
    template <typename Derived, typename Functor>
    static typename Details::JacobianTypeGetter<IthDerivative::FIRST, Derived,
                                                Derived::RowsAtCompileTime,
                                                Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & x, Functor & f,
            const typename Derived::Scalar & h=1.e-6)
    {
        return get(x, f, Derived::Constant(x.size(), h)) ;

    }

    // Special overload for scalar types
    template <typename Scalar, typename Functor>
    static Scalar get(const Scalar & x, Functor & f, const Scalar & h)
    {
        Scalar res(static_cast<Scalar>(0)) ;
        Scalar xTmp(x) ;
        auto it(Details::Coefs<Scheme, IthDerivative::FIRST,
                               Order>::coefs.begin()) ;
        auto itEnd(Details::Coefs<Scheme, IthDerivative::FIRST,
                                  Order>::coefs.end()) ;
        for (; it!=itEnd; ++it)
        {
            xTmp += it->first * h ;
            res += it->second * f(xTmp) ;
            xTmp -= it->first * h ;
        }
        res /= h ;
        return res ;
    }
} ;

/*!
 * \brief Specialization of the central FiniteDifference structure in case of
 *     a discrete set of data first derivative.
 * \tparam Order: the order of the central finite difference.
 */
template <int Order>
struct FiniteDifference<FDScheme::CENTRAL, IthDerivative::FIRST, Order,
                        FDContext::DISCRETE>
{
    /*!
     * \brief Calculation of the finite difference of the provided
     *     discrete set of data.
     * \param fx: the data for which to calculate the finite difference.
     * \param h: the delta to apply for the finite difference calculation.
     *
     * Example:
     *
     * \code{.cpp}
     *     #include <Eigen/Dense>
     *
     *     #include "Math/FiniteDifference/FiniteDifference.hpp"
     *
     *     typedef FiniteDifference<FDScheme::CENTRAL, IthDerivative::FIRST, 2,
     *                              FDContext::DISCRETE> Type ;
     *     Eigen::MatrixXd fx(10, 2) ;
     *     fx << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.,
     *           11., 12., 13., 14., 15., 16., 17., 18., 19., 20. ;
     *     Eigen::MatrixXd res(Type::get(fx, 2.)) ;
     *     // res now contains 1. on each of its components
     *
     * \endcode
     *
     * Above example computed the following:
     *
     * The first value was computed using a forward FD scheme:
     * \f[ df_1 = \frac{fx_2 - fx_1}{h} \f]
     * The inside values were computed using central FD scheme:
     * \f[ df_2 = \frac{fx_3 - fx_1}{2 h} \f]
     * \f[ ... \f]
     * \f[ df_{n-1} = \frac{fx_n - fx_{n-2}}{2 h} \f]
     * The last node was computed using backward FD scheme:
     * \f[ df_n = \frac{fx_n - fx_{n-1}}{h} \f]
     *
     */
    template <typename Derived>
    static typename Details::InputTypeGetter<Derived,
                                             Derived::RowsAtCompileTime,
                                             Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & fx, const double & h=1.e-6)
    {
        using IndexType = typename Eigen::MatrixBase<Derived>::Index ;
        IndexType nRows(fx.rows()) ;
        if (nRows < (static_cast<IndexType>(Order)*2+1))
        {
            throw Details::OrderToHighException(
                        "Not enough values ("
                        +std::to_string(nRows)
                        +") provided for central finite difference of order "
                        +std::to_string(Order)
                                               ) ;
        }
        typename Eigen::MatrixBase<Derived>::Index nCols(fx.cols()) ;
        typedef typename Details::InputTypeGetter<
                               Derived, Derived::RowsAtCompileTime,
                               Derived::ColsAtCompileTime
                                                 >::Type ResType ;
        ResType res(ResType::Zero(nRows, nCols)) ;
        IndexType startInd(static_cast<IndexType>(Order)/2) ;
        IndexType endInd(nRows - static_cast<IndexType>(Order)/2) ;
        IndexType nInside(endInd-startInd) ;
        // Inside values with central finite difference
        auto it(Details::Coefs<FDScheme::CENTRAL, IthDerivative::FIRST,
                               Order>::coefs.begin()) ;
        auto itEnd(Details::Coefs<FDScheme::CENTRAL, IthDerivative::FIRST,
                                  Order>::coefs.end()) ;
        for (; it!=itEnd; ++it)
        {
            res.block(startInd, 0, nInside, nCols) +=
                  it->second * fx.block(startInd+it->first, 0, nInside, nCols) ;
        }
        // Boundary values
        // Forward difference for start
        auto itForward(Details::Coefs<FDScheme::FORWARD, IthDerivative::FIRST,
                                      Order>::coefs.begin()) ;
        auto itEndForward(Details::Coefs<FDScheme::FORWARD,
                                         IthDerivative::FIRST,
                                         Order>::coefs.end()) ;
        for (; itForward!=itEndForward; ++itForward)
        {
            res.block(0, 0, startInd, nCols) +=
                         itForward->second * fx.block(itForward->first,
                                                      0, startInd, nCols) ;
        }
        auto itBackward(Details::Coefs<FDScheme::BACKWARD, IthDerivative::FIRST,
                                       Order>::coefs.begin()) ;
        auto itEndBackward(Details::Coefs<FDScheme::BACKWARD,
                                          IthDerivative::FIRST,
                                          Order>::coefs.end()) ;
        for (; itBackward!=itEndBackward; ++itBackward)
        {
            res.block(endInd, 0, startInd, nCols) +=
                      itBackward->second * fx.block(endInd+itBackward->first,
                                                    0, startInd, nCols) ;
        }
        res /= h ;
        return res ;
    }

} ;

/*!
 * \brief Same as FiniteDifference<FDScheme::CENTRAL, IthDerivative::FIRST,
 *                                 Order, FDContext::DISCRETE>
 *     but using a forward finite difference scheme.
 */
template <int Order>
struct FiniteDifference<FDScheme::FORWARD, IthDerivative::FIRST, Order,
                        FDContext::DISCRETE>
{
    template <typename Derived>
    static typename Details::InputTypeGetter<Derived,
                                             Derived::RowsAtCompileTime,
                                             Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & fx, const double & h=1.e-6)
    {
        using IndexType = typename Eigen::MatrixBase<Derived>::Index ;
        IndexType nRows(fx.rows()) ;
        if (nRows < (static_cast<IndexType>(Order)*2))
        {
            throw Details::OrderToHighException(
                        "Not enough values ("
                        +std::to_string(nRows)
                        +") provided for forward finite difference of order "
                        +std::to_string(Order)
                                               ) ;
        }
        typename Eigen::MatrixBase<Derived>::Index nCols(fx.cols()) ;
        typedef typename Details::InputTypeGetter<
                               Derived, Derived::RowsAtCompileTime,
                               Derived::ColsAtCompileTime
                                                 >::Type ResType ;
        ResType res(ResType::Zero(nRows, nCols)) ;
        IndexType endInd(nRows - static_cast<IndexType>(Order)) ;
        IndexType nInside(endInd) ;
        IndexType nEnd(nRows-nInside) ;
        // Inside values with forward finite difference
        auto it(Details::Coefs<FDScheme::FORWARD, IthDerivative::FIRST,
                               Order>::coefs.begin()) ;
        auto itEnd(Details::Coefs<FDScheme::FORWARD, IthDerivative::FIRST,
                                  Order>::coefs.end()) ;
        for (; it!=itEnd; ++it)
        {
            res.block(0, 0, nInside, nCols) +=
                        it->second * fx.block(it->first, 0, nInside, nCols) ;
        }
        // End boundary values
        auto itBackward(Details::Coefs<FDScheme::BACKWARD, IthDerivative::FIRST,
                                       Order>::coefs.begin()) ;
        auto itEndBackward(Details::Coefs<FDScheme::BACKWARD,
                                          IthDerivative::FIRST,
                                          Order>::coefs.end()) ;
        for (; itBackward!=itEndBackward; ++itBackward)
        {
            res.block(endInd, 0, nEnd, nCols) +=
                    itBackward->second * fx.block(endInd+itBackward->first, 0,
                                                  nEnd, nCols) ;
        }
        res /= h ;
        return res ;
    }

} ;

/*!
 * \brief Same as FiniteDifference<FDScheme::CENTRAL, IthDerivative::FIRST,
 *                                 Order, FDContext::DISCRETE>
 *     but using a backward finite difference scheme.
 */
template <int Order>
struct FiniteDifference<FDScheme::BACKWARD, IthDerivative::FIRST, Order,
                        FDContext::DISCRETE>
{
    template <typename Derived>
    static typename Details::InputTypeGetter<Derived,
                                             Derived::RowsAtCompileTime,
                                             Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & fx, const double & h=1.e-6)
    {
        using IndexType = typename Eigen::MatrixBase<Derived>::Index ;
        IndexType nRows(fx.rows()) ;
        if (nRows < (static_cast<IndexType>(Order)*2))
        {
            throw Details::OrderToHighException(
                        "Not enough values ("
                        +std::to_string(nRows)
                        +") provided for backward finite difference of order "
                        +std::to_string(Order)
                                               ) ;
        }
        IndexType nCols(fx.cols()) ;
        typedef typename Details::InputTypeGetter<
                               Derived, Derived::RowsAtCompileTime,
                               Derived::ColsAtCompileTime
                                                 >::Type ResType ;
        ResType res(ResType::Zero(nRows, nCols)) ;
        IndexType startInd(static_cast<IndexType>(Order)) ;
        IndexType nInside(nRows-startInd) ;
        // Inside values with backward finite difference
        auto it(Details::Coefs<FDScheme::BACKWARD, IthDerivative::FIRST,
                               Order>::coefs.begin()) ;
        auto itEnd(Details::Coefs<FDScheme::BACKWARD, IthDerivative::FIRST,
                                  Order>::coefs.end()) ;
        for (; it!=itEnd; ++it)
        {
            res.block(startInd, 0, nInside, nCols) +=
                  it->second * fx.block(startInd+it->first, 0, nInside, nCols) ;
        }
        // Start boundary values
        auto itForward(Details::Coefs<FDScheme::FORWARD, IthDerivative::FIRST,
                                      Order>::coefs.begin()) ;
        auto itEndForward(Details::Coefs<FDScheme::FORWARD,
                                         IthDerivative::FIRST,
                                         Order>::coefs.end()) ;
        for (; itForward!=itEndForward; ++itForward)
        {
            res.block(0, 0, startInd, nCols) +=
                          itForward->second * fx.block(itForward->first,
                                                       0, startInd, nCols) ;
        }
        res /= h ;
        return res ;
    }

} ;

/*!
 * \brief Specialization of the FiniteDifference structure in case of
 *     the second derivative for fixed size input/output types.
 *     Typically this applies to BV::Math::Functions types. However user defined
 *     functors should also work, provided these functors define an
 *     OutputType typedef and a call operator.
 * \tparam Scheme: the scheme to use for the finite difference. Use the
 *     enumeration FDScheme.
 * \tparam Order: the order of the finite difference.
 */
template <int Scheme, int Order>
struct FiniteDifference<Scheme, IthDerivative::SECOND, Order,
                        FDContext::FIXED_SIZE_IO>
{

    /*!
     * \brief Computes the finite difference for fixed size input/output.
     *     Template parameters should be automatically deduced by the compiler.
     * \param x: the value for which to evaluate the function.
     *     This type should be a scalar.
     * \param f: a functor providing a call operator that evaluates in x.
     *     The functor should also provide a typedef named OutputType, which
     *     provides the type of the function output. Note that the dimensions
     *     of the output should be known at compile time.
     * \param h: the deltas to apply on the x input to compute the finite
     *     difference.
     *
     * Example for a central finite difference of order 2:
     *
     * \code{.cpp}
     *
     *     #include <Eigen/Dense>
     *     #include <unsupported/Eigen/CXX11/Tensor>
     *
     *     #include "Math/FiniteDifference/FiniteDifference.hpp"
     *
     *     typedef FiniteDifference<FDScheme::CENTRAL,
     *                              IthDerivative::SECOND, 2,
     *                              FDContext::FIXED_SIZE_IO> Type ;
     *     using BV::Math::Functions::Analytical ;
     *     // Here we evaluate a function of R2 to R3
     *     Analytical<1, 1> func("2.*x**2+3.*y+4.", "x") ;
     *     double res(Type::get(3., func, 1.e-4)) ; // res = 4
     *
     * \endcode
     *
     * This example computes the following central finite difference:
     * \f[ \frac{d^2f}{dx^2}(x) = \frac{f(x-h) - 2 f(x) + f(x+h)}{h^2} \f]
     *
     * The finite differences then approximate the derivatives for \f$x=3\f$:
     * \f[ \frac{d^2f}{dx^2}(x) = 4 \f]
     *
     */
    template <typename DataType, typename Functor,
              typename std::enable_if<Details::IsScalar<DataType>::value,
                                      int>::type=0>
    static typename Details::Traits<DataType, typename Functor::OutputType,
                                    IthDerivative::SECOND>::OutputType
        get(const DataType & x, Functor & f, const DataType & h=1.e-4)
    {
        typedef Details::Traits<DataType, typename Functor::OutputType,
                                IthDerivative::SECOND> TraitsType ;
        typename TraitsType::OutputType res(TraitsType::getOutputZero()) ;
        DataType xTmp(x) ;
        auto it(Details::Coefs<Scheme, IthDerivative::SECOND,
                               Order>::coefs.begin()) ;
        auto itEnd(Details::Coefs<Scheme, IthDerivative::SECOND,
                                  Order>::coefs.end()) ;
        for (; it!=itEnd; ++it)
        {
            xTmp += it->first * h ;
            res += it->second * f(xTmp) ;
            xTmp -= it->first * h ;
        }
        res /= (h * h) ;
        return res ;
    }

    typedef FiniteDifference<Scheme, IthDerivative::FIRST, Order,
                             FDContext::FIXED_SIZE_IO> FD1Type ;

    /*!
     * \brief Computes the finite difference for fixed size input/output.
     *     Template parameters should be automatically deduced by the compiler.
     * \param x: the values for which to evaluate the function.
     *     This type should be a vector of known size at compile time.
     * \param f: a functor providing a call operator that evaluates in x.
     *     The functor should also provide a typedef named OutputType, which
     *     provides the type of the function output. Note that the dimensions
     *     of the output should be known at compile time.
     * \param h: the deltas to apply on the x input to compute the finite
     *     difference.
     *
     * Example for a central finite difference of order 2:
     *
     * \code{.cpp}
     *
     *     #include <Eigen/Dense>
     *     #include <unsupported/Eigen/CXX11/Tensor>
     *
     *     #include "Math/FiniteDifference/FiniteDifference.hpp"
     *
     *     typedef FiniteDifference<FDScheme::CENTRAL,
     *                              IthDerivative::SECOND, 2,
     *                              FDContext::FIXED_SIZE_IO> Type ;
     *     using BV::Math::Functions::Analytical ;
     *     // Here we evaluate a function of R2 to R3
     *     Analytical<2, 3> func({"2.*x**2+3.*y+4.", "3.*x**2", "2.*y**2"},
     *                           {"x", "y"}) ;
     *     Eigen::Vector2d input ;
     *     input << 3., 4. ;
     *     Eigen::Vector2d h ;
     *     h << 1.e-4, 1.e-4 ;
     *     Eigen::Tensor<double, 3> res(Type::get(input, func, h)) ;
     *
     * \endcode
     *
     * This example computes the following central finite differences for
     * each of the 3 functions and each of the 2 variables:
     * \f[
     *     \frac{\partial^2f_1}{\partial x_1 \partial x_2}(x_1, x_2) =
     *         \frac{\frac{\partial f_1(x_1-h, x_2)}{dx_2}
     *               - \frac{\partial f_1(x_1+h, x_2)}{dx_2}}{2*h}
     * \f]
     *
     */
    template <typename InputType, typename Functor, typename DeltaType>
    static typename Details::Traits<InputType, typename Functor::OutputType,
                                    IthDerivative::SECOND>::OutputType
        get(const InputType & x, Functor & f, const DeltaType & h)
    {
        typedef Details::Traits<InputType, typename Functor::OutputType,
                                IthDerivative::SECOND> TraitsType ;
        typename TraitsType::OutputType res(TraitsType::getOutputZero()) ;
        typename TraitsType::InputType xTmp(x) ;
        for (typename TraitsType::IndexType i=0; i<TraitsType::sizeInput(); ++i)
        {
            auto it(Details::Coefs<Scheme, IthDerivative::FIRST,
                                   Order>::coefs.begin()) ;
            auto itEnd(Details::Coefs<Scheme, IthDerivative::FIRST,
                                      Order>::coefs.end()) ;
            for (; it!=itEnd; ++it)
            {
                TraitsType::add(xTmp, i, it->first*TraitsType::get(h, i)) ;
                TraitsType::addRes(res, i, it->second*FD1Type::get(xTmp, f, h)) ;
                TraitsType::subtract(xTmp, i, it->first*TraitsType::get(h, i)) ;
            }
            TraitsType::divideRes(res, i, TraitsType::get(h, i)) ;
        }
        return res ;
    }

    /*!
     * \brief Overload of get with a default value for the deltas.
     */
    template <typename InputType, typename Functor,
              typename std::enable_if<!Details::IsScalar<InputType>::value,
                                      int>::type=0>
    static typename Details::Traits<InputType, typename Functor::OutputType,
                                    IthDerivative::SECOND>::OutputType
        get(const InputType & x, Functor & f, const double & h=1.e-3)
    {
        typedef Details::Traits<InputType, typename Functor::OutputType,
                                IthDerivative::SECOND> TraitsType ;
        return get(x, f, TraitsType::getInputConstant(h)) ;
    }

} ;

/*!
 * \brief Calculation of the Hessian matrix of a system of equation using
 *     finite difference schemes.
 *     Typically this applies to functions of N->N.
 * \tparam Scheme: scheme used for the calculation of the finite difference.
 * \tparam Order: order of the finite difference calculation.
 */
template <int Scheme, int Order>
struct FiniteDifference<Scheme, IthDerivative::SECOND, Order,
                        FDContext::JACOBIAN_CALCULATION>
{
    typedef FiniteDifference<Scheme, IthDerivative::FIRST, Order,
                             FDContext::JACOBIAN_CALCULATION> FD1Type ;

    // FIXME f return type same size as x ?
    template <typename Derived, typename Functor, typename Derived2>
    static typename Details::JacobianTypeGetter<IthDerivative::SECOND, Derived,
                                                Derived::RowsAtCompileTime,
                                                Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & x, Functor & f,
            const Eigen::MatrixBase<Derived2> & h)
    {
        using SizeType = typename Eigen::MatrixBase<Derived>::Index ;
        SizeType size(x.size()) ;
        typedef typename Details::JacobianTypeGetter<
                                     IthDerivative::SECOND, Derived,
                                     Derived::RowsAtCompileTime,
                                     Derived::ColsAtCompileTime
                                                    >::Type HessianType ;
        HessianType res(size, size, size) ;
        res.setZero() ;
        typename Details::InputTypeGetter<Derived,
                                          Derived::RowsAtCompileTime,
                                          Derived::ColsAtCompileTime>::Type xTmp(x) ;
        typename HessianType::Dimensions dims(res.dimensions()) ;
        SizeType N(dims[0]), M(dims[1]) ;
        using Scalar = typename Derived::Scalar ;
        for (SizeType i=0; i<size; ++i)
        {
            auto it(Details::Coefs<Scheme, IthDerivative::FIRST,
                                   Order>::coefs.begin()) ;
            auto itEnd(Details::Coefs<Scheme, IthDerivative::FIRST,
                                      Order>::coefs.end()) ;
            for (; it!=itEnd; ++it)
            {
                xTmp(i) += it->first * h(i) ;
                Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> pTmp(
                                           it->second * FD1Type::get(xTmp, f, h)
                                                                          ) ;
                for (SizeType l=0; l<N; ++l)
                {
                    for (SizeType m=0; m<M; ++m)
                    {
                        res(l, i, m) += pTmp(l, m) ;
                    }
                }
                xTmp(i) -= it->first * h(i) ;
            }
            Eigen::DSizes<SizeType, 3> offsets {0, i, 0} ;
            Eigen::DSizes<SizeType, 3> extents {N, 1, M} ;
            res.slice(offsets, extents) = res.slice(offsets, extents) / h(i) ;
        }
        return res ;
    }

    template <typename Derived, typename Functor>
    static typename Details::JacobianTypeGetter<IthDerivative::SECOND, Derived,
                                                Derived::RowsAtCompileTime,
                                                Derived::ColsAtCompileTime>::Type
        get(const Eigen::MatrixBase<Derived> & x, Functor & f,
            const typename Derived::Scalar & h=1.e-4)
    {
        return get(x, f, Derived::Constant(x.size(), h)) ;
    }

} ;

} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_FiniteDifference_FiniteDifference_hpp
