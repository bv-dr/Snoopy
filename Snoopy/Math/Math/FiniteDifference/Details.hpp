#ifndef BV_Math_FiniteDifference_Details_hpp
#define BV_Math_FiniteDifference_Details_hpp

#include "FiniteDifferenceExport.hpp"
#include <functional>
#include <map>
#include <cmath>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <autodiff/forward/real.hpp>

#include "Math/FiniteDifference/Typedefs.hpp"


namespace BV {
namespace Math {
namespace FiniteDifference {

namespace Details {

template <typename T>
struct IsScalar : std::is_scalar<T>
{
} ;

template <>
struct IsScalar<autodiff::real1st> : std::true_type
{
} ;

struct FINITEDIFFERENCE_API OrderToHighException : std::exception
{
protected:
    std::string message_ ;
public:
    OrderToHighException(std::string message) : message_(message)
    {
    }

    ~OrderToHighException() throw()
    {
    }

    const char *what() const throw()
    {
        return message_.c_str() ;
    }

    std::string getMessage() const
    {
        return message_ ;
    }
} ;

template <int Scheme, int IDer, int Order>
struct FINITEDIFFERENCE_API Coefs
{
    static const std::map<int, double> coefs ;
} ;

// Explicit specialization here to avoid warning on coefs definition in cpp file
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::FIRST, 2>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::FIRST, 4>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::FIRST, 6>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::FIRST, 8>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::SECOND, 2>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::SECOND, 4>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::SECOND, 6>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::CENTRAL, IthDerivative::SECOND, 8>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::FIRST, 1>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::FIRST, 2>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::FIRST, 3>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::FIRST, 4>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::FIRST, 5>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::FIRST, 6>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::SECOND, 1>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::SECOND, 2>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::SECOND, 3>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::SECOND, 4>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::SECOND, 5>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::FORWARD, IthDerivative::SECOND, 6>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::FIRST, 1>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::FIRST, 2>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::FIRST, 3>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::FIRST, 4>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::FIRST, 5>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::FIRST, 6>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::SECOND, 1>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::SECOND, 2>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::SECOND, 3>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::SECOND, 4>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::SECOND, 5>
{
    static const std::map<int, double> coefs ;
} ;
template <>
struct FINITEDIFFERENCE_API Coefs<FDScheme::BACKWARD, IthDerivative::SECOND, 6>
{
    static const std::map<int, double> coefs ;
} ;


template <Eigen::Index NRows, Eigen::Index NCols>
constexpr Eigen::Index VectorDimGetter(void)
{
    return ((NRows != 1) ? NRows : NCols) ;
}

template <typename InputType, typename FOutputType, int IDer,
          typename Enable=void>
struct Traits ;

template <typename T1, typename T2>
struct FINITEDIFFERENCE_API Traits<T1, T2, IthDerivative::FIRST,
              typename std::enable_if<
                  !IsScalar<T1>::value && !IsScalar<T2>::value
                                     >::type>
{
	using IndexType = Eigen::Index ;
    static constexpr IndexType N = VectorDimGetter<T1::RowsAtCompileTime,
                                                   T1::ColsAtCompileTime>() ;
    static constexpr IndexType M = VectorDimGetter<T2::RowsAtCompileTime,
                                                   T2::ColsAtCompileTime>() ;

    typedef typename T2::Scalar DataType ;
    typedef Eigen::Matrix<DataType, M, N> OutputType ;
    typedef Eigen::Matrix<DataType, N, 1> InputType ;
    typedef Eigen::Matrix<DataType, M, 1> FOutputType ;

    static InputType getInputConstant(const double & val)
    {
        return InputType::Constant(val) ;
    }

    static OutputType getOutputZero(void)
    {
        return OutputType::Zero() ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return N ;
    }

    template <typename InputTypeLike>
    static const DataType & get(const InputTypeLike & input, IndexType i)
    {
        return input(i) ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input(i) += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input(i) -= val ;
    }


    static void addRes(OutputType & res, IndexType i, const FOutputType & out)
    {
        res.col(i) += out ;
    }


    static void divideRes(OutputType & res, IndexType i, const DataType & val)
    {
        res.col(i) /= val ;
    }
} ;

template <typename T1, typename T2>
struct FINITEDIFFERENCE_API Traits<T1, T2, IthDerivative::FIRST,
              typename std::enable_if<
                  IsScalar<T1>::value && !IsScalar<T2>::value
                                     >::type>
{
	using IndexType = Eigen::Index ;
    static constexpr IndexType M = VectorDimGetter<T2::RowsAtCompileTime,
                                                   T2::ColsAtCompileTime>() ;

    typedef typename T2::Scalar DataType ;
    typedef Eigen::Matrix<DataType, M, 1> OutputType ;
    typedef DataType InputType ;
    typedef Eigen::Matrix<DataType, M, 1> FOutputType ;

    static InputType getInputConstant(const double & val)
    {
        return val ;
    }

    static OutputType getOutputZero(void)
    {
        return OutputType::Zero() ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return 1 ;
    }

    template <typename InputTypeLike>
    static const double & get(const InputTypeLike & input, IndexType i)
    {
        return input ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const double & val)
    {
        input += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const double & val)
    {
        input -= val ;
    }


    static void addRes(OutputType & res, IndexType i, const FOutputType & out)
    {
        res += out ;
    }


    static void divideRes(OutputType & res, IndexType i, const double & val)
    {
        res /= val ;
    }
} ;

template <typename T1, typename T2>
struct FINITEDIFFERENCE_API Traits<T1, T2, IthDerivative::FIRST,
              typename std::enable_if<
                  !IsScalar<T1>::value && IsScalar<T2>::value
                                     >::type>
{
	using IndexType = Eigen::Index ;
    static constexpr IndexType N = VectorDimGetter<T1::RowsAtCompileTime,
                                                   T1::ColsAtCompileTime>() ;

    typedef T2 DataType ;
    typedef Eigen::Matrix<DataType, N, 1> OutputType ;
    typedef Eigen::Matrix<DataType, N, 1> InputType ;
    typedef double FOutputType ;

    static InputType getInputConstant(const double & val)
    {
        return InputType::Constant(val) ;
    }

    static OutputType getOutputZero(void)
    {
        return OutputType::Zero() ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return N ;
    }

    template <typename InputTypeLike>
    static const double & get(const InputTypeLike & input, IndexType i)
    {
        return input(i) ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const double & val)
    {
        input(i) += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const double & val)
    {
        input(i) -= val ;
    }


    static void addRes(OutputType & res, IndexType i, const FOutputType & out)
    {
        res(i) += out ;
    }


    static void divideRes(OutputType & res, IndexType i, const double & val)
    {
        res(i) /= val ;
    }
} ;

template <typename T1, typename T2, int IDer>
struct FINITEDIFFERENCE_API Traits<T1, T2, IDer,
              typename std::enable_if<
                  IsScalar<T1>::value && IsScalar<T2>::value
                                     >::type>
{
    typedef T2 DataType ;
    typedef T2 OutputType ;
    typedef T1 InputType ;
    typedef T2 FOutputType ;
    using IndexType = Eigen::Index ;

    static InputType getInputConstant(const DataType & val)
    {
        return val ;
    }

    static OutputType getOutputZero(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return 1 ;
    }

    template <typename InputTypeLike>
    static const DataType & get(const InputTypeLike & input, IndexType i)
    {
        return input ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input -= val ;
    }


    static void addRes(OutputType & res, IndexType i, const FOutputType & out)
    {
        res += out ;
    }


    static void divideRes(OutputType & res, IndexType i, const DataType & val)
    {
        res /= val ;
    }
} ;

template <typename T1, typename T2>
struct FINITEDIFFERENCE_API Traits<T1, T2, IthDerivative::SECOND,
              typename std::enable_if<
                  !IsScalar<T1>::value && !IsScalar<T2>::value
                                     >::type>
{
    typedef typename T1::Scalar DataType ;
    typedef Eigen::Tensor<DataType, 3> OutputType ;
    using IndexType = Eigen::Index ;
    static constexpr IndexType N = VectorDimGetter<T1::RowsAtCompileTime,
                                                   T1::ColsAtCompileTime>() ;
    static constexpr IndexType M = VectorDimGetter<T2::RowsAtCompileTime,
                                                   T2::ColsAtCompileTime>() ;
    typedef Eigen::Matrix<DataType, N, 1> InputType ;
    typedef Eigen::Matrix<DataType, M, 1> FOutputType ;
    typedef Eigen::Matrix<DataType, M, N> FDerOutputType ;

    static InputType getInputConstant(const double & val)
    {
        return InputType::Constant(val) ;
    }

    static OutputType getOutputZero(void)
    {
        OutputType res(M, N, N) ;
        res.setZero() ;
        return res ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return N ;
    }

    static constexpr IndexType sizeOutput(void)
    {
        return M ;
    }

    template <typename InputTypeLike>
    static const DataType & get(const InputTypeLike & input, IndexType i)
    {
        return input(i) ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input(i) += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input(i) -= val ;
    }


    static void addRes(OutputType & res, IndexType j, const FDerOutputType & out)
    {
        for (IndexType i=0; i<M; ++i)
        {
            for (IndexType k=0; k<N; ++k)
            {
                res(i, j, k) += out(i, k) ;
            }
        }
    }

    static void divideRes(OutputType & res, IndexType i, const DataType & val)
    {
        Eigen::DSizes<IndexType, 3> offsets {0, i, 0} ;
        Eigen::DSizes<IndexType, 3> extents {M, 1, N} ;
        res.slice(offsets, extents) = res.slice(offsets, extents) / val ;
    }
} ;

template <typename T1, typename T2>
struct FINITEDIFFERENCE_API Traits<T1, T2, IthDerivative::SECOND,
              typename std::enable_if<
                  IsScalar<T1>::value && !IsScalar<T2>::value
                                     >::type>
{
    typedef typename T2::Scalar DataType ;
    using IndexType = Eigen::Index ;
    static constexpr IndexType M = VectorDimGetter<T2::RowsAtCompileTime,
                                             T2::ColsAtCompileTime>() ;
    typedef Eigen::Matrix<DataType, M, 1> OutputType ;
    typedef double InputType ;
    typedef Eigen::Matrix<DataType, M, 1> FOutputType ;

    static InputType getInputConstant(const DataType & val)
    {
        return val ;
    }

    static OutputType getOutputZero(void)
    {
        return OutputType::Zero() ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return 1 ;
    }

    static constexpr IndexType sizeOutput(void)
    {
        return M ;
    }

    template <typename InputTypeLike>
    static const DataType & get(const InputTypeLike & input, IndexType i)
    {
        return input ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input -= val ;
    }


    static void addRes(OutputType & res, IndexType i, const FOutputType & out)
    {
        res += out ;
    }


    static void divideRes(OutputType & res, IndexType i, const DataType & val)
    {
        res /= val ;
    }
} ;

template <typename T1, typename T2>
struct FINITEDIFFERENCE_API Traits<T1, T2, IthDerivative::SECOND,
              typename std::enable_if<
                  !IsScalar<T1>::value && IsScalar<T2>::value
                                     >::type>
{
    typedef T2 DataType ;
    using IndexType = Eigen::Index ;
    static constexpr IndexType N = VectorDimGetter<T1::RowsAtCompileTime,
                                             T1::ColsAtCompileTime>() ;
    typedef Eigen::Matrix<DataType, N, N> OutputType ;
    typedef Eigen::Matrix<DataType, N, 1> InputType ;
    typedef Eigen::Matrix<DataType, N, 1> DerOutputType ;
    typedef double FOutputType ;

    static InputType getInputConstant(const DataType & val)
    {
        return InputType::Constant(val) ;
    }

    static OutputType getOutputZero(void)
    {
        return OutputType::Zero() ;
    }

    static constexpr IndexType sizeInput(void)
    {
        return N ;
    }

    static constexpr IndexType sizeOutput(void)
    {
        return 1 ;
    }

    template <typename InputTypeLike>
    static const DataType & get(const InputTypeLike & input, IndexType i)
    {
        return input(i) ;
    }

    template <typename InputTypeLike>
    static void add(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input(i) += val ;
    }

    template <typename InputTypeLike>
    static void subtract(InputTypeLike & input, IndexType i, const DataType & val)
    {
        input(i) -= val ;
    }


    static void addRes(OutputType & res, IndexType i, const DerOutputType & out)
    {
        res.row(i) += out ;
    }


    static void divideRes(OutputType & res, IndexType i, const DataType val)
    {
        res.row(i) /= val ;
    }
} ;

template <int IDer, typename Derived, Eigen::Index NRows, Eigen::Index NCols>
struct JacobianTypeGetter ;

template <typename Derived, Eigen::Index NRows>
struct JacobianTypeGetter<IthDerivative::FIRST, Derived, NRows, 1>
{
    typedef Eigen::Matrix<typename Derived::Scalar, NRows, NRows> Type ;
} ;

template <typename Derived, Eigen::Index NCols>
struct JacobianTypeGetter<IthDerivative::FIRST, Derived, 1, NCols>
{
    typedef Eigen::Matrix<typename Derived::Scalar, NCols, NCols> Type ;
} ;

template <typename Derived>
struct JacobianTypeGetter<IthDerivative::FIRST, Derived, Eigen::Dynamic, 1>
{
    typedef Eigen::Matrix<typename Derived::Scalar, Eigen::Dynamic,
                          Eigen::Dynamic> Type ;
} ;

template <typename Derived>
struct JacobianTypeGetter<IthDerivative::FIRST, Derived, 1, Eigen::Dynamic>
{
    typedef Eigen::Matrix<typename Derived::Scalar, Eigen::Dynamic,
                          Eigen::Dynamic> Type ;
} ;

template <typename Derived, Eigen::Index NRows, Eigen::Index NCols>
struct JacobianTypeGetter<IthDerivative::SECOND, Derived, NRows, NCols>
{
    typedef Eigen::Tensor<typename Derived::Scalar, 3> Type ;
} ;

template <typename Derived, Eigen::Index NRows, Eigen::Index NCols>
struct InputTypeGetter
{
    typedef Eigen::Matrix<typename Derived::Scalar, NRows, NCols> Type ;
} ;

} // End of namespace Details

} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_FiniteDifference_Details_hpp
