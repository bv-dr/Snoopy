#include "Math/FiniteDifference/Details.hpp"

namespace BV {
namespace Math {
namespace FiniteDifference {

namespace Details {

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::FIRST,
                                  2>::coefs = {{-1, -0.5},
                                               { 1,  0.5}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::FIRST,
                                  4>::coefs = {{-2,  1./12.},
                                               {-1, -2./3. },
                                               { 1,  2./3. },
                                               { 2, -1./12.}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::FIRST,
                                  6>::coefs = {{-3, -1./60.},
                                               {-2,  3./20.},
                                               {-1, -3./4. },
                                               { 1,  3./4. },
                                               { 2, -3./20.},
                                               { 3,  1./60.}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::FIRST,
                                  8>::coefs = {{-4,  1./280.},
                                               {-2, -4./105.},
                                               {-2,  1./5.  },
                                               {-1, -4./5.  },
                                               { 1,  4./5.  },
                                               { 2, -1./5.  },
                                               { 2,  4./105.},
                                               { 3, -1./280.}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::SECOND,
                                  2>::coefs = {{-1, 1.},
                                               { 0, -2.},
                                               { 1, 1.}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::SECOND,
                                  4>::coefs = {{-2, -1./12.},
                                               {-1,  4./3. },
                                               { 0, -5./2. },
                                               { 1,  4./3. },
                                               { 2, -1./12.}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::SECOND,
                                  6>::coefs = {{-3,  1. /90.},
                                               {-2, -3. /20.},
                                               {-1,  3. /2. },
                                               { 0, -49./18.},
                                               { 1,  3. /2. },
                                               { 2, -3. /20.},
                                               { 3,  1. /90.}} ;

const std::map<int, double> Coefs<FDScheme::CENTRAL,
                                  IthDerivative::SECOND,
                                  8>::coefs = {{-4, -1.  /560.},
                                               {-3,  8.  /315.},
                                               {-2, -1.  /5.  },
                                               {-1,  8.  /5.  },
                                               { 0, -205./72. },
                                               { 1,  8.  /5.  },
                                               { 2, -1.  /5.  },
                                               { 3,  8.  /315.},
                                               { 4, -1.  /560.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::FIRST,
                                  1>::coefs = {{0, -1.},
                                               {1,  1.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::FIRST,
                                  2>::coefs = {{0, -3./2.},
                                               {1,  2.   },
                                               {2, -1./2.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::FIRST,
                                  3>::coefs = {{0, -11./6.},
                                               {1,  3.    },
                                               {2, -3. /2.},
                                               {3,  1. /3.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::FIRST,
                                  4>::coefs = {{0, -25./12.},
                                               {1,  4.     },
                                               {2, -3.     },
                                               {3,  4. /3. },
                                               {4, -1. /4. }} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::FIRST,
                                  5>::coefs = {{0, -137./60.},
                                               {1,  5.      },
                                               {2, -5.      },
                                               {3,  10. /3. },
                                               {4, -5.  /4. },
                                               {5,  1.  /5. }} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::FIRST,
                                  6>::coefs = {{0, -49./60.},
                                               {1,  6.     },
                                               {2, -15./2. },
                                               {3,  20./3.},
                                               {4, -15./4.},
                                               {5,  5. /5.},
                                               {6, -1. /6.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::SECOND,
                                  1>::coefs = {{0,  1.},
                                               {1, -2.},
                                               {2,  1.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::SECOND,
                                  2>::coefs = {{0,  2.},
                                               {1, -5.},
                                               {2,  4.},
                                               {3, -1.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::SECOND,
                                  3>::coefs = {{0,  35./12.},
                                               {1, -26./3. },
                                               {2,  19./2. },
                                               {3, -14./3. },
                                               {4,  11./12.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::SECOND,
                                  4>::coefs = {{0,  15. /4. },
                                               {1, -77. /6. },
                                               {2,  107./6. },
                                               {3, -13.     },
                                               {4,  61. /12.},
                                               {5, -5.  /6. }} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::SECOND,
                                  5>::coefs = {{0,  203./45. },
                                               {1, -87. /5.  },
                                               {2,  117./4.  },
                                               {3, -254./9.  },
                                               {4,  33. /2.  },
                                               {5, -27. /5.  },
                                               {6,  137./180.}} ;

const std::map<int, double> Coefs<FDScheme::FORWARD,
                                  IthDerivative::SECOND,
                                  6>::coefs = {{0,  469. /90. },
                                               {1, -223. /10. },
                                               {2,  879. /20. },
                                               {3, -949. /18. },
                                               {4,  41.       },
                                               {5, -201. /10. },
                                               {6,  1019./180.},
                                               {7, -7.   /10. }} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::FIRST,
                                  1>::coefs = {{ 0,  1.},
                                               {-1, -1.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::FIRST,
                                  2>::coefs = {{ 0,  3./2.},
                                               {-1, -2.   },
                                               {-2,  1./2.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::FIRST,
                                  3>::coefs = {{ 0,  11./6.},
                                               {-1, -3.    },
                                               {-2,  3. /2.},
                                               {-3, -1. /3.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::FIRST,
                                  4>::coefs = {{ 0,  25./12.},
                                               {-1, -4.     },
                                               {-2,  3.     },
                                               {-3, -4. /3. },
                                               {-4,  1. /4. }} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::FIRST,
                                  5>::coefs = {{ 0,  137./60.},
                                               {-1, -5.      },
                                               {-2,  5.      },
                                               {-3, -10. /3. },
                                               {-4,  5.  /4. },
                                               {-5, -1.  /5. }} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::FIRST,
                                  6>::coefs = {{ 0,  49./60.},
                                               {-1, -6.     },
                                               {-2,  15./2. },
                                               {-3, -20./3.},
                                               {-4,  15./4.},
                                               {-5, -5. /5.},
                                               {-6,  1. /6.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::SECOND,
                                  1>::coefs = {{ 0,  1.},
                                               {-1, -2.},
                                               {-2,  1.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::SECOND,
                                  2>::coefs = {{ 0,  2.},
                                               {-1, -5.},
                                               {-2,  4.},
                                               {-3, -1.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::SECOND,
                                  3>::coefs = {{ 0,  35./12.},
                                               {-1, -26./3. },
                                               {-2,  19./2. },
                                               {-3, -14./3. },
                                               {-4,  11./12.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::SECOND,
                                  4>::coefs = {{ 0,  15. /4. },
                                               {-1, -77. /6. },
                                               {-2,  107./6. },
                                               {-3, -13.     },
                                               {-4,  61. /12.},
                                               {-5, -5.  /6. }} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::SECOND,
                                  5>::coefs = {{ 0,  203./45. },
                                               {-1, -87. /5.  },
                                               {-2,  117./4.  },
                                               {-3, -254./9.  },
                                               {-4,  33. /2.  },
                                               {-5, -27. /5.  },
                                               {-6,  137./180.}} ;

const std::map<int, double> Coefs<FDScheme::BACKWARD,
                                  IthDerivative::SECOND,
                                  6>::coefs = {{ 0,  469. /90. },
                                               {-1, -223. /10. },
                                               {-2,  879. /20. },
                                               {-3, -949. /18. },
                                               {-4,  41.       },
                                               {-5, -201. /10. },
                                               {-6,  1019./180.},
                                               {-7, -7.   /10. }} ;

} // End of namespace Details

} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV
