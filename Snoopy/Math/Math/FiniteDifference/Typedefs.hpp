#ifndef BV_Math_FiniteDifference_Typedefs_hpp
#define BV_Math_FiniteDifference_Typedefs_hpp


namespace BV {
namespace Math {
namespace FiniteDifference {

enum FDScheme {CENTRAL, FORWARD, BACKWARD} ;
enum IthDerivative {FIRST=1, SECOND=2} ;
enum FDContext {FIXED_SIZE_IO, JACOBIAN_CALCULATION, DISCRETE} ;

} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_FiniteDifference_Typedefs_hpp
