#pragma once

#include <Eigen/Dense>
#include <iostream>
#include <map>
#include <iterator>
#include <utility>

#include "Math/Tools.hpp"
#include "Geometry/Rotation/Quaternion.hpp"
#include "Geometry/Rotation/RotationVector.hpp"
#include "Geometry/Rotation/AxisAndAngle.hpp"

namespace BV {
namespace Math {

namespace Details {

inline Eigen::Vector4d QFromRotV(const Eigen::Ref<const Eigen::Vector3d> & w)
{
    Eigen::Vector4d qVals ;
    double squaredNorm(w.dot(w)) ;
    double k ;
    if (squaredNorm > 1.e-20)
    {
        double norm(std::sqrt(squaredNorm)) ;
        double halfNorm(0.5 * norm) ;
        k = std::sin(halfNorm) / norm ;
        qVals(0) = std::cos(halfNorm) ;
    }
    else
    {
        k = 0.5 ;
        qVals(0) = 1. ;
    }
    qVals(1) = w(0) * k ;
    qVals(2) = w(1) * k ;
    qVals(3) = w(2) * k ;
    return qVals ;
}

inline Eigen::Vector3d QToRotV(const Eigen::Ref<const Eigen::Vector4d> & q)
{
    Eigen::Vector3d w ;
    double sinSquared(q(1) * q(1) + q(2) * q(2) + q(3) * q(3)) ;
    double k(0.) ;
    if (sinSquared > 1.e-20)
    {
        double sinTheta(std::sqrt(sinSquared)) ;
        k = 2. * std::atan2(sinTheta, q(0)) / sinTheta ;
    }
    else
    {
        k = 2. ;
    }
    w(0) = q(1) * k ;
    w(1) = q(2) * k ;
    w(2) = q(3) * k ;
    return w ;
}

template <typename RotatorType>
void AddRotations(const Eigen::Ref<const Eigen::VectorXd> & v1,
                  const Eigen::Ref<const Eigen::VectorXd> & v2,
                  Eigen::Ref<Eigen::VectorXd> res)
{
    RotatorType r1(v1) ;
    RotatorType r2(v2) ;
    res = (r1 + r2).unknowns() ;
}

template <typename RotatorType>
void SubRotations(const Eigen::Ref<const Eigen::VectorXd> & v1,
                  const Eigen::Ref<const Eigen::VectorXd> & v2,
                  Eigen::Ref<Eigen::VectorXd> res)
{
    RotatorType r1(v1) ;
    RotatorType r2(v2) ;
    res = (r1 - r2).unknowns() ;
}

template <typename RotatorType>
void OppositeRotation(const Eigen::Ref<const Eigen::VectorXd> & v,
                      Eigen::Ref<Eigen::VectorXd> res)
{
    RotatorType q(v) ;
    q.inverse() ;
    res = q.unknowns() ;
}

inline
void QuaternionPlusRotVec(const Eigen::Ref<const Eigen::VectorXd> & vq,
                          const Eigen::Ref<const Eigen::VectorXd> & vrv,
                          Eigen::Ref<Eigen::VectorXd> res)
{
    BV::Geometry::Rotation::Quaternion q(vq) ;
    Eigen::Vector3d wVals(vrv) ;
    res = (BV::Geometry::Rotation::Quaternion(Details::QFromRotV(wVals)) * q).unknowns() ;
}

inline
void QuaternionCoefMultiplication(const Eigen::Ref<const Eigen::VectorXd> & vq,
                                  const double & coef,
                                  Eigen::Ref<Eigen::VectorXd> res)
{
    BV::Geometry::Rotation::Quaternion qInterp, q0 ;
    BV::Geometry::Rotation::Quaternion q(vq) ;
    if ((coef > 1.) || (coef < 0.))
    {
        // Extrapolation
        BV::Geometry::Rotation::AxisAndAngle aa(q) ;
        aa.angle() = aa.angle() * coef ;
        qInterp = aa.toQuaternion() ;
    }
    else
    {
        qInterp = q0.slerp(coef, q) ;
    }
    res = qInterp.unknowns() ;
}

//template <int RotatorType>
//class RotatorSize_ ;
//
//template <>
//class RotatorSize_<Geometry::RotatorTypeEnum::QUATERNION>
//{
//    typedef Geometry::Rotation::Quaternion RotType ;
//    static const Eigen::Index nParameters =  4 ;
//    static const Eigen::Index nDerParameters = 3 ;
//} ;
//
//template <>
//class RotatorSize_<Geometry::RotatorTypeEnum::ROTATION_VECTOR>
//{
//    typedef Geometry::Rotation::RotationVector RotType ;
//    static const Eigen::Index nParameters =  3 ;
//    static const Eigen::Index nDerParameters = 3 ;
//} ;

struct Indexer
{
    Eigen::Index indexInState ;
    Eigen::Index extentInState ;
    Eigen::Index indexInStateDer ;
    Eigen::Index extentInStateDer ;
    bool standardOperation ;
    Geometry::RotatorTypeEnum rotationType ;
    std::function<void(const Eigen::Ref<const Eigen::VectorXd> &,
                       const Eigen::Ref<const Eigen::VectorXd> &,
                       Eigen::Ref<Eigen::VectorXd>)> addOperation ;
    std::function<void(const Eigen::Ref<const Eigen::VectorXd> &,
                       const Eigen::Ref<const Eigen::VectorXd> &,
                       Eigen::Ref<Eigen::VectorXd>)> subOperation ;
    std::function<void(const Eigen::Ref<const Eigen::VectorXd> &,
                       Eigen::Ref<Eigen::VectorXd>)> oppositeOperation ;
} ;

} // End of namespace details

class StateIndexer
{
private:
    Eigen::Index stateSize_ ;
    // First index is Rotator position in State
    // second is omega index in StateDerivative
    std::map<Geometry::RotatorTypeEnum, std::map<Eigen::Index, Eigen::Index> > rotatorsIndices_ ;
    mutable bool refreshNeeded_ ;
    mutable std::map<Eigen::Index, Details::Indexer> indices_ ;
    std::map<std::string, std::map<Eigen::Index, Eigen::Index> > correspondances_ ;

    void resetIndices_() const
    {
        if (stateSize_ == 0)
        {
            std::cout << "incorrect state size in state indexer !" << std::endl ;
            return ;
        }
        // FIXME at the moment we only deal with quaternions and rotation vectors
        // Maybe we should find a way to deal with all rotations
        std::map<Geometry::RotatorTypeEnum, std::pair<Eigen::Index, Eigen::Index> > rotatorTypeToVarSize ;
        rotatorTypeToVarSize[Geometry::RotatorTypeEnum::QUATERNION] = {4, 3} ;
        rotatorTypeToVarSize[Geometry::RotatorTypeEnum::ROTATION_VECTOR] = {3, 3} ;
        indices_.clear() ;
        std::map<Eigen::Index, Details::Indexer> rotIndices ;
        // c++ maps are sorted
        // First deal with the rotators indices
        for (auto const & [rotType, indices] : rotatorsIndices_)
        {
            for (auto const & [iState, iStateDer] : indices)
            {
                Details::Indexer indexer ;
                indexer.indexInState = iState ;
                indexer.indexInStateDer = iStateDer ;
                indexer.extentInState = rotatorTypeToVarSize[rotType].first ;
                indexer.extentInStateDer = rotatorTypeToVarSize[rotType].second ;
                indexer.standardOperation = false ;
                indexer.rotationType = rotType ;
                if (rotType == Geometry::RotatorTypeEnum::QUATERNION)
                {
                    indexer.addOperation = Details::AddRotations<Geometry::Rotation::Quaternion> ;
                    indexer.subOperation = Details::SubRotations<Geometry::Rotation::Quaternion> ;
                    indexer.oppositeOperation = Details::OppositeRotation<Geometry::Rotation::Quaternion> ;
                }
                else if (rotType == Geometry::RotatorTypeEnum::ROTATION_VECTOR)
                {
                    indexer.addOperation = Details::AddRotations<Geometry::Rotation::RotationVector> ;
                    indexer.subOperation = Details::SubRotations<Geometry::Rotation::RotationVector> ;
                    indexer.oppositeOperation = Details::OppositeRotation<Geometry::Rotation::RotationVector> ;
                }
                rotIndices[iState] = indexer ;
            }
        }
        // Then fill in the gaps
        auto AddIndexer = [&](Eigen::Index iState, Eigen::Index iStateDer, Eigen::Index extent)
        {
                Details::Indexer indexer ;
                indexer.indexInState = iState ;
                indexer.indexInStateDer = iStateDer ;
                indexer.extentInState = extent ;
                indexer.extentInStateDer = extent ;
                indexer.standardOperation = true ;
                indexer.rotationType = Geometry::RotatorTypeEnum::UNDEFINED_ROTATION ;
                indices_[iState] = indexer ;
        } ;
        if (rotIndices.size())
        {
            std::map<Eigen::Index, Details::Indexer>::const_iterator it(rotIndices.begin()) ;
            Eigen::Index lastIndState(0), lastIndStateDer(0), extentState(0) ;
            for(; it!=rotIndices.end(); ++it)
            {
                extentState = it->first - lastIndState ;
                if (extentState > 0)
                {
                    AddIndexer(lastIndState, lastIndStateDer, extentState) ;
                    lastIndState += extentState ;
                    lastIndStateDer += extentState ;
                }
                lastIndState += it->second.extentInState ;
                lastIndStateDer += it->second.extentInStateDer ;
                indices_[it->first] = it->second ;
            }
            if (lastIndState < (stateSize_-1))
            {
                AddIndexer(lastIndState, lastIndStateDer, stateSize_-lastIndState) ;
            }
        }
        else
        {
            AddIndexer(0, 0, stateSize_) ;
        }
        refreshNeeded_ = false ;
    }

public:
    StateIndexer(Eigen::Index stateSize) :
        stateSize_(stateSize), refreshNeeded_(true)
    {
    }

    StateIndexer() : stateSize_(0), refreshNeeded_(true)
    {
    }

    StateIndexer(Eigen::Index stateSize,
                 std::map<Eigen::Index, Details::Indexer> indices,
                 std::map<std::string, std::map<Eigen::Index, Eigen::Index> > correspondances) :
        stateSize_(stateSize), refreshNeeded_(false),
        indices_(indices), correspondances_(correspondances)
    {
    }

    void setStateSize(Eigen::Index stateSize)
    {
        stateSize_ = stateSize ;
    }

    void clear()
    {
        rotatorsIndices_.clear() ;
        indices_.clear() ;
        refreshNeeded_ = true ;
    }

    void addRotatorIndices(BV::Geometry::RotatorTypeEnum rotatorType,
                           Eigen::Index indexInState,
                           Eigen::Index indexInStateDer)
    {
        if (!rotatorsIndices_.count(rotatorType))
        {
            rotatorsIndices_[rotatorType] = {} ;
        }
        rotatorsIndices_.at(rotatorType)[indexInState] = indexInStateDer ;
        refreshNeeded_ = true ;
    }

    const std::map<Eigen::Index, Details::Indexer> & getIndices() const
    {
        if (refreshNeeded_)
        {
            resetIndices_() ;
        }
        return indices_ ;
    }

    StateIndexer getStateIndexerUsingRotVec() const
    {
        std::map<Eigen::Index, Details::Indexer> newIndices ;
        std::map<std::string, std::map<Eigen::Index, Eigen::Index> > correspondances ;
        correspondances["rotToQuat"] = {} ;
        correspondances["rotToRot"] = {} ;
        correspondances["otherToOther"] = {} ;
        Eigen::Index nQuats(0) ;
        for (auto const & [index, indexer] : getIndices())
        {
            Eigen::Index newInd(index - nQuats) ;
            Details::Indexer newIndexer(indexer) ;
            newIndexer.indexInState = newInd ;
            if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::QUATERNION)
            {
                newIndexer.extentInState = 3 ;
                newIndexer.rotationType = BV::Geometry::RotatorTypeEnum:: ROTATION_VECTOR ;
                correspondances.at("rotToQuat")[newInd] = index ;
                ++nQuats ;
            }
            else if (indexer.rotationType == BV::Geometry::RotatorTypeEnum::ROTATION_VECTOR)
            {
                correspondances.at("rotToRot")[newInd] = index ;
            }
            else
            {
                correspondances.at("otherToOther")[newInd] = index ;
            }
            newIndices[newInd] = newIndexer ;
        }
        return StateIndexer(stateSize_ - nQuats, newIndices, correspondances) ;
    }

    const std::map<std::string, std::map<Eigen::Index, Eigen::Index> > & getStateCorrespondingIndices() const
    {
        return correspondances_ ;
    }

} ;

// This class has to be header only because of Windows DLL export and Eigen matrix inheritance
// https://stackoverflow.com/questions/58630573/why-cant-i-inherit-from-eigenmatrix

class State : public Eigen::VectorXd // FIXME maybe ArrayXd would be a better choice
{
private:
    // We have to deal with rotations operations in a correct way
    StateIndexer stateIndexer_ ;

public:
    typedef Eigen::VectorXd Base ;

    State() : Eigen::VectorXd()
    {
    }

    State(Eigen::Index size) :
        Eigen::VectorXd(Eigen::VectorXd::Zero(size)),
        stateIndexer_(size)
    {
    }


    State(const State & other) :
        Eigen::VectorXd(other),
        stateIndexer_(other.stateIndexer_)
    {
    }

    State(const Eigen::Ref<const Eigen::VectorXd> & vec) :
        Eigen::VectorXd(vec),
        stateIndexer_(vec.size())
    {
    }

    template <typename OtherDerived>
    State & operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->Eigen::VectorXd::operator=(other) ;
        return *this ;
    }

    State & operator=(const State & other)
    {
        this->Eigen::VectorXd::operator=(other) ;
        this->stateIndexer_ = other.stateIndexer_ ;
        return *this ;
    }

    void setIndexer(const StateIndexer & stateIndexer)
    {
        stateIndexer_ = stateIndexer ;
    }

    const std::map<Eigen::Index, Details::Indexer> & getIndices() const
    {
        return stateIndexer_.getIndices() ;
    }

    State operator+(const State & other) const
    {
        State res(*this) ;
        for (auto const & [index, indexer] : stateIndexer_.getIndices())
        {
            Eigen::Index ind(indexer.indexInState), extent(indexer.extentInState) ;
            if (indexer.standardOperation)
            {
                res.segment(ind, extent) += other.segment(ind, extent) ;
            }
            else
            {
                indexer.addOperation(res.segment(ind, extent),
                                     other.segment(ind, extent),
                                     res.segment(ind, extent)) ;
            }
        }
        return res ;
    }

    State operator-(const State & other) const
    {
        State res(*this) ;
        for (auto const & [index, indexer] : stateIndexer_.getIndices())
        {
            Eigen::Index ind(indexer.indexInState), extent(indexer.extentInState) ;
            if (indexer.standardOperation)
            {
                res.segment(ind, extent) -= other.segment(ind, extent) ;
            }
            else
            {
                indexer.subOperation(this->segment(ind, extent),
                                     other.segment(ind, extent),
                                     res.segment(ind, extent)) ;
            }
        }
        return res ;
    }

    State operator-() const
    {
        State res(*this) ;
        for (auto const & [index, indexer] : stateIndexer_.getIndices())
        {
            Eigen::Index ind(indexer.indexInState), extent(indexer.extentInState) ;
            if (indexer.standardOperation)
            {
                res.segment(ind, extent) = -this->segment(ind, extent) ;
            }
            else
            {
                indexer.oppositeOperation(this->segment(ind, extent),
                                          res.segment(ind, extent)) ;
            }
        }
        return res ;
    }

    double normInf() const
    {
        // We don't want to deal with the quaternions here as they would
        // return identity rotation (1, 0, 0, 0) which would mislead
        // steppers because of the "1" variable
        // FIXME don't really know what to do...
        State copy(*this) ;
        for (auto const & [index, indexer] : stateIndexer_.getIndices())
        {
            if (indexer.rotationType == Geometry::RotatorTypeEnum::QUATERNION)
            {
                copy(index) -= 1. ;
            }
        }
        return copy.Eigen::VectorXd::lpNorm<Eigen::Infinity>() ;
    }

    Eigen::VectorXd error(const State & other) const
    {
        State res(*this) ;
        res = res.Eigen::VectorXd::operator-=(other) ;
        // We don't want to deal with the quaternions here as they would
        // return identity rotation (1, 0, 0, 0) which would mislead
        // steppers because of the "1" variable
        return res ;
    }

    //Eigen::VectorXd toRotV() const
    //{
    //    if (!(iQuats_.size()))
    //    {
    //        return *this ;
    //    }
    //    Eigen::Index thisSize(this->size()) ;
    //    Eigen::Index rotVSize(thisSize-iQuats_.size()) ;
    //    Eigen::VectorXd rotV(rotVSize) ;
    //    std::map<Eigen::Index, Eigen::Index>::const_iterator it(iQuats_.begin()) ;
    //    Eigen::Index lastIndY(0), lastIndDY(0) ;
    //    Eigen::Index extentY(it->first - lastIndY) ;
    //    Eigen::Index extentDY(it->second - lastIndDY) ;
    //    while (lastIndDY < rotVSize-1)
    //    {
    //        rotV.segment(lastIndDY, extentDY) = this->segment(lastIndY, extentY) ;
    //        lastIndDY = lastIndDY + extentDY ;
    //        lastIndY = lastIndY + extentY ;
    //        if (it != iQuats_.end())
    //        {
    //            rotV.segment(lastIndDY, 3) = Details::QToRotV(this->segment(lastIndY, 4)) ;
    //            lastIndDY += 3 ;
    //            lastIndY += 4 ;
    //            ++it ;
    //        }
    //        if (it != iQuats_.end())
    //        {
    //            extentY = it->first - lastIndY ;
    //            extentDY = it->second - lastIndDY ;
    //        }
    //        else
    //        {
    //            extentY = thisSize - lastIndY ;
    //            extentDY = rotVSize - lastIndDY ;
    //        }
    //    }
    //    return rotV ;
    //}

    //void fromRotV(const Eigen::Ref<const Eigen::VectorXd> & rotV)
    //{
    //    if (!(iQuats_.size()))
    //    {
    //        this->Eigen::VectorXd::operator=(rotV) ;
    //        return ;
    //    }
    //    // Here we assume this is correctly initialized
    //    Eigen::Index thisSize(this->size()) ;
    //    Eigen::Index rotVSize(rotV.size()) ;
    //    std::map<Eigen::Index, Eigen::Index>::const_iterator it(iQuats_.begin()) ;
    //    Eigen::Index lastIndY(0), lastIndDY(0) ;
    //    Eigen::Index extentY(it->first - lastIndY) ;
    //    Eigen::Index extentDY(it->second - lastIndDY) ;
    //    while (lastIndDY < rotVSize-1)
    //    {
    //        this->segment(lastIndY, extentY) = rotV.segment(lastIndDY, extentDY) ;
    //        lastIndDY = lastIndDY + extentDY ;
    //        lastIndY = lastIndY + extentY ;
    //        if (it != iQuats_.end())
    //        {
    //            this->segment(lastIndY, 4) = Details::QFromRotV(rotV.segment(lastIndDY, 3)) ;
    //            lastIndDY += 3 ;
    //            lastIndY += 4 ;
    //            ++it ;
    //        }
    //        if (it != iQuats_.end())
    //        {
    //            extentY = it->first - lastIndY ;
    //            extentDY = it->second - lastIndDY ;
    //        }
    //        else
    //        {
    //            extentY = thisSize - lastIndY ;
    //            extentDY = rotVSize - lastIndDY ;
    //        }
    //    }
    //}

} ;

class StateDerivative : public Eigen::VectorXd // FIXME maybe ArrayXd would be a better choice
{
public:
    typedef Eigen::VectorXd Base ;

    StateDerivative() : Eigen::VectorXd()
    {
    }

    StateDerivative(Eigen::Index size) : Eigen::VectorXd(size)
    {
    }

    StateDerivative(const State & other) : Eigen::VectorXd(other)
    {
    }

    StateDerivative(const Eigen::Ref<const Eigen::VectorXd> & vec) :
        Eigen::VectorXd(vec)
    {
    }

    template <typename OtherDerived>
    StateDerivative & operator=(const Eigen::MatrixBase<OtherDerived>& other)
    {
        this->Eigen::VectorXd::operator=(other) ;
        return *this ;
    }

    StateDerivative operator-() const
    {
        StateDerivative res(*this) ;
        res = res.Eigen::VectorXd::operator-() ;
        return res ;
    }

    StateDerivative operator+(const StateDerivative & der) const
    {
        StateDerivative res(*this) ;
        res = res.Eigen::VectorXd::operator+=(der) ;
        return res ;
    }

    StateDerivative operator+(const Eigen::Ref<const Eigen::VectorXd> & vec) const
    {
        StateDerivative res(*this) ;
        res = res.Eigen::VectorXd::operator+=(vec) ;
        return res ;
    }

    StateDerivative operator-(const StateDerivative & der) const
    {
        StateDerivative res(*this) ;
        res = res.Eigen::VectorXd::operator-=(der) ;
        return res ;
    }

    StateDerivative operator-(const Eigen::Ref<const Eigen::VectorXd> & vec) const
    {
        StateDerivative res(*this) ;
        res = res.Eigen::VectorXd::operator-=(vec) ;
        return res ;
    }

} ;

inline State operator+(const State & y, const StateDerivative & dy)
{
    State res(y) ;
    auto indices(y.getIndices()) ;
    for (auto const & [index, indexer] : indices)
    {
        Eigen::Index indY(indexer.indexInState), extentY(indexer.extentInState) ;
        Eigen::Index indDy(indexer.indexInStateDer), extentDy(indexer.extentInStateDer) ;
        if (indexer.rotationType == Geometry::RotatorTypeEnum::QUATERNION)
        {
            Details::QuaternionPlusRotVec(y.segment(indY, extentY), dy.segment(indDy, extentDy),
                                          res.segment(indY, extentY)) ;
        }
        else
        {
            res.segment(indY, extentY) += dy.segment(indDy, extentDy) ;
        }
    }
    return res ;
}

inline State operator+(const StateDerivative & dy, const State & y)
{
    return y + dy ;
}

inline State operator-(const State & y, const StateDerivative & dy)
{
    State res(y) ;
    auto indices(y.getIndices()) ;
    for (auto const & [index, indexer] : indices)
    {
        Eigen::Index indY(indexer.indexInState), extentY(indexer.extentInState) ;
        Eigen::Index indDy(indexer.indexInStateDer), extentDy(indexer.extentInStateDer) ;
        if (indexer.rotationType == Geometry::RotatorTypeEnum::QUATERNION)
        {
            Details::QuaternionPlusRotVec(y.segment(indY, extentY), -dy.segment(indDy, extentDy),
                                          res.segment(indY, extentY)) ;
        }
        else
        {
            res.segment(indY, extentY) -= dy.segment(indDy, extentDy) ;
        }
    }
    return res ;
}

inline State operator-(const StateDerivative & dy, const State & y)
{
    return y - dy ;
}

inline void SetDiffAsStateDerivative(const State & y1, const State & y2,
                                     StateDerivative & dy)
{
    std::cout << "TODO SetDiffAsStateDerivative" << std::endl ;
    // TODO !
    //// FIXME here we assume states have the same quaternion indices
    //auto iQuats(y1.getQuaternionIndices()) ;
    //State yDiff(y1 - y2) ;
    //if (!(iQuats.size()))
    //{
    //    dy = yDiff ;
    //    return ;
    //}
    //std::map<Eigen::Index, Eigen::Index>::const_iterator it(iQuats.begin()) ;
    //Eigen::Index lastIndY(0), lastIndDY(0) ;
    //Eigen::Index extentY(it->first - lastIndY) ;
    //Eigen::Index extentDY(it->second - lastIndDY) ;
    //while (lastIndDY < dy.size()-1)
    //{
    //    dy.segment(lastIndDY, extentDY) = yDiff.segment(lastIndY, extentY) ;
    //    lastIndDY = lastIndDY + extentDY ;
    //    lastIndY = lastIndY + extentY ;
    //    if (it != iQuats.end())
    //    {
    //        dy.segment(lastIndDY, 3) = Details::QToRotV(yDiff.segment(lastIndY, 4)) ;
    //        lastIndDY += 3 ;
    //        lastIndY += 4 ;
    //        ++it ;
    //    }
    //    if (it != iQuats.end())
    //    {
    //        extentY = it->first - lastIndY ;
    //        extentDY = it->second - lastIndDY ;
    //    }
    //    else
    //    {
    //        extentY = yDiff.size() - lastIndY ;
    //        extentDY = dy.size() - lastIndDY ;
    //    }
    //}
}

inline State operator*(const State & y, double coef)
{
    State res(y) ;
    if (BV::Math::IsClose(coef, 1.))
    {
        return res ;
    }
    auto indices(y.getIndices()) ;
    for (auto const & [index, indexer] : indices)
    {
        Eigen::Index indY(indexer.indexInState), extentY(indexer.extentInState) ;
        if (indexer.rotationType == Geometry::RotatorTypeEnum::QUATERNION)
        {
            Details::QuaternionCoefMultiplication(y.segment(indY, extentY), coef,
                                                  res.segment(indY, extentY)) ;
        }
        else
        {
            res.segment(indY, extentY) *= coef ;
        }
    }
    return res ;
}

inline State operator*(double coef, const State & y)
{
    return y * coef ;
}

inline StateDerivative operator*(const StateDerivative & dy, double coef)
{
    StateDerivative res(dy) ;
    res.Eigen::VectorXd::operator*=(coef) ;
    return res ;
}

inline StateDerivative operator*(double coef, const StateDerivative & dy)
{
    return dy * coef ;
}

} // End of namespace Math
} // End of namespace BV
