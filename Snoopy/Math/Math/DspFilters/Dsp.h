/*******************************************************************************

"A Collection of Useful C++ Classes for Digital Signal Processing"
 By Vincent Falco

Official project location:
http://code.google.com/p/dspfilterscpp/

See Documentation.cpp for contact information, notes, and bibliography.

--------------------------------------------------------------------------------

License: MIT License (http://www.opensource.org/licenses/mit-license.php)
Copyright (c) 2009 by Vincent Falco

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*******************************************************************************/

#ifndef DSPFILTERS_DSP_H
#define DSPFILTERS_DSP_H

//
// Include this file in your application to get everything
//

#include "Math/DspFilters/Bessel.h"
#include "Math/DspFilters/Biquad.h"
#include "Math/DspFilters/Butterworth.h"
#include "Math/DspFilters/Cascade.h"
#include "Math/DspFilters/ChebyshevI.h"
#include "Math/DspFilters/ChebyshevII.h"
#include "Math/DspFilters/Common.h"
#include "Math/DspFilters/Custom.h"
#include "Math/DspFilters/Elliptic.h"
#include "Math/DspFilters/Filter.h"
#include "Math/DspFilters/Legendre.h"
#include "Math/DspFilters/PoleFilter.h"
#include "Math/DspFilters/RBJ.h"
#include "Math/DspFilters/SmoothedFilter.h"
#include "Math/DspFilters/State.h"
#include "Math/DspFilters/Utilities.h"

#endif
