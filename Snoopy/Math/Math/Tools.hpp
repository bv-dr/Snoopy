#pragma once

#include "MathToolsExport.hpp"

#include <vector>
#include <set>
#include <Eigen/Dense>

#include "Tools/BVException.hpp"
#include "Tools/EigenTypedefs.hpp"

namespace BV {
namespace Math {

MATHTOOLS_API double Degrees(const double & value) ;
MATHTOOLS_API void Degrees(Eigen::Ref<Eigen::ArrayXd> angles) ;
MATHTOOLS_API double Radians(const double & value) ;
MATHTOOLS_API void Radians(Eigen::Ref<Eigen::ArrayXd> angles) ;
MATHTOOLS_API bool IsClose(const double & value, const double & reference,
             const double & epsilon = 1.e-8) ;
MATHTOOLS_API bool IsNull(const double & value, const double & epsilon = 1.e-8) ;
MATHTOOLS_API double Sign(const double & x) ;
//void ConvertMoment3D(BV::Tools::Vector6d & load,
//                     const BV::Geometry::Point & applicationPoint,
//                     const BV::Geometry::Point & targetPoint) ;
MATHTOOLS_API double WrapAngle0_2PI(const double & angle) ;
MATHTOOLS_API void WrapAngle0_2PI(Eigen::Ref<Eigen::ArrayXd> angles) ;
MATHTOOLS_API Eigen::ArrayXd getWrapAngle0_2PI(const Eigen::Ref<Eigen::ArrayXd> & angles);
MATHTOOLS_API double WrapAngleMPI_PI(const double & angle) ;

MATHTOOLS_API Eigen::MatrixXd::Index GetRank(const Eigen::MatrixXd & mat, const double & epsilon) ;

template<typename Derived>
inline bool IsFinite(const Eigen::MatrixBase<Derived> & x)
{
    return ((x - x).array() == (x - x).array()).all() ;
}
inline bool IsFinite(const double & x)
{
    return std::isfinite(x) ;
}

template<typename Derived>
inline bool IsNaN(const Eigen::MatrixBase<Derived> & x)
{
    return ((x.array() == x.array())).all();
}
inline bool IsNaN(const double & x)
{
    return std::isnan(x) ;
}

/*!
 \brief Switchs variables values
 \param[in] a First variable reference
 \param[in] b Second variable reference
 */
template <typename T>
void switch_x1_x2(T & a, T & b)
{
    const T temp(b) ;
    b = a ;
    a = temp ;
}

template <typename T1, typename T2>
inline Eigen::VectorXd prod(const T1 & val1, const T2 & val2)
{
    return val1.cwiseProduct(val2) ;
}

template <typename T1, typename T2>
inline Eigen::VectorXd div(const T1 & val1, const T2 & val2)
{
    return val1.cwiseQuotient(val2) ;
}

template <typename T=std::size_t>
class CombinListGenerator
{
private:
    T icurrent_ ;
    T jcurrent_ ;
    T imax_ ;
    T jmax_ ;
    T nRank_ ;
    std::vector<std::vector<T> > priorityCases_ ;
public:
    CombinListGenerator(T nCoords, T nRank) :
        icurrent_(0), jcurrent_(0), imax_(std::pow(2, nCoords) - 1),
        jmax_(nCoords - 1), nRank_(nRank)
    {
    }

    std::vector<T> operator()()
    {
        while (icurrent_ <= imax_)
        {
            std::vector<T> tuple ;
            tuple.clear() ;
            tuple.reserve(nRank_) ;
            jcurrent_ = 0 ;
            while (jcurrent_ <= jmax_)
            {
                if ((icurrent_ >> jcurrent_) & (1 == 1))
                {
                    tuple.push_back(jcurrent_) ;
                }
                ++jcurrent_ ;
            }
            if (tuple.size() == nRank_)
            {
                return tuple ;
            }
            ++icurrent_ ;
        }
        throw BV::Tools::Exceptions::BVException(
            "Combin list: should not pass here") ;
    }
    operator bool()
    {
        if (icurrent_ > 0)
        {
            ++icurrent_ ;
        }
        return (icurrent_ < imax_) ;
    }
} ;

template<typename DerivedA, typename DerivedB>
inline bool allcloseRTol(const Eigen::DenseBase<DerivedA>& a,
                  const Eigen::DenseBase<DerivedB>& b,
                  const typename DerivedA::RealScalar& rtol
                      = Eigen::NumTraits<typename DerivedA::RealScalar>::dummy_precision(),
                  const typename DerivedA::RealScalar& atol
                      = Eigen::NumTraits<typename DerivedA::RealScalar>::epsilon())
{
  return ((a.derived() - b.derived()).array().abs()
          <= (atol + rtol * b.derived().array().abs())).all();
}

template<typename DerivedA, typename DerivedB>
inline bool allclose(const Eigen::DenseBase<DerivedA>& a,
              const Eigen::DenseBase<DerivedB>& b,
              const typename DerivedA::RealScalar& tol
                  = Eigen::NumTraits<typename DerivedA::RealScalar>::dummy_precision())
{
  return ((a.derived() - b.derived()).array().abs() <= tol).all();
}

MATHTOOLS_API Eigen::ArrayXd inline getUnique(const Eigen::ArrayXd & a, const double & tolerance = 1.e-8) {
    auto comp = [tolerance](const double &a, const double &b) {return a < b && std::abs(a-b) >= tolerance;};
    std::set<double, decltype(comp)> s(comp);
    Eigen::Index n(a.size());
    for(Eigen::Index i = 0; i < n; ++i) s.insert(a(i));
    std::vector<double> v(s.begin(), s.end());
    return Eigen::Map<Eigen::ArrayXd>(v.data(), v.size());
}

} // End of namespace Math
} // End of namespace BV
