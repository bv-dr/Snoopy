#ifndef _PASCALTRIANGLE_HPP_
#define _PASCALTRIANGLE_HPP_

#include "MathToolsExport.hpp"
#include <map>
#include <vector>

namespace BV {
namespace Math {
class MATHTOOLS_API PascalTriangle
{
    std::map<unsigned, std::vector<unsigned> > pascalTriangles_ ;
    // singleton
    static PascalTriangle pascalTriangle_ ;
    PascalTriangle()
    {
    }

public:

    /*!
     \brief Returns the Pascal triangle
     \param[in] n Order of the desired triangle
     \returns Pascal triangle at order n
     */
    static std::vector<unsigned> getPascalTriangle(const unsigned& n) ;
} ;
}
}

#endif
