#include "Math/PascalTriangle.hpp"

using namespace BV::Math ;

PascalTriangle PascalTriangle::pascalTriangle_ = PascalTriangle() ;

std::vector<unsigned> PascalTriangle::getPascalTriangle(const unsigned& n)
{
    if (pascalTriangle_.pascalTriangles_.find(n)
        == pascalTriangle_.pascalTriangles_.end())
    {
        const unsigned n_terms = n * (n + 1) / 2 ;
        std::vector<int unsigned> pascalTriangle ;
        pascalTriangle.reserve(n_terms) ;
        const int one = 1 ;
        for (unsigned i = 0; i < n; i++)
        {
            pascalTriangle.push_back(one) ;
        }
        for (unsigned j = 1, i = n; j < n; j++)
        {
            const unsigned n_l = n - j ;
            unsigned val = 1 ;
            for (unsigned k = 0; k < n_l; k++, i++)
            {
                pascalTriangle.push_back(val) ;
                const unsigned index = i - n_l ;
                val += pascalTriangle[index] ;
            }
        }
        pascalTriangle_.pascalTriangles_[n] = pascalTriangle ;
    }
    return pascalTriangle_.pascalTriangles_.find(n)->second ;
}
