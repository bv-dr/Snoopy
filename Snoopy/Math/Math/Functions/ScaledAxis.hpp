#pragma once

#include "Math/Functions/ABC.hpp"

namespace BV {
namespace Math {
namespace Functions {

template <std::size_t N, std::size_t M, typename DataType=double, std::size_t IAxis=0>
class ScaledAxis : public ABC<N, M, DataType>
{
public:

    typedef typename ABC<N, M, DataType>::IndexType IndexType ;
    typedef typename ABC<N, M, DataType>::InputType InputType ;
    typedef typename ABC<N, M, DataType>::OutputType OutputType ;
    typedef typename ABC<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, M, DataType>::Der2OutputType Der2OutputType ;

private:
    std::shared_ptr<ABC<N, M, DataType> > fct_ ;
    DataType scale_ ;

public:

    ScaledAxis(std::shared_ptr<ABC<N, M, DataType> > fct) :
        ABC<N, M, DataType>(),
        fct_(fct), scale_(1.)
    {
    }

    using ABC<N, M, DataType>::eval ;
    using ABC<N, M, DataType>::operator() ;
    using ABC<N, M, DataType>::dEval ;
    using ABC<N, M, DataType>::dEval2 ;

    virtual const OutputType & eval(const InputType & values) const
    {
        InputType vals(values) ;
        Details::Traits<N, M, DataType>::multiplyInputAtIndex(vals, IAxis, scale_) ;
        return fct_->eval(vals) ;
    }

    virtual const DerOutputType & dEval(const InputType & values) const
    {
        InputType vals(values) ;
        Details::Traits<N, M, DataType>::multiplyInputAtIndex(vals, IAxis, scale_) ;
        return fct_->dEval(vals) ;
    }

    virtual const Der2OutputType & dEval2(const InputType & values) const
    {
        InputType vals(values) ;
        Details::Traits<N, M, DataType>::multiplyInputAtIndex(vals, IAxis, scale_) ;
        return fct_->dEval2(vals) ;
    }

    void setScaleFactor(const DataType & val)
    {
        scale_ = val ;
    }

    DataType getScaleFactor() const
    {
        return scale_ ;
    }

} ;

} // End of namespace Fcts
} // End of namespace Math
} // End of namespace BV