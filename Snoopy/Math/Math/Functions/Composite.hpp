#ifndef BV_Math_Functions_Composite_hpp
#define BV_Math_Functions_Composite_hpp

#include <Eigen/Dense>
#include <tuple>
#include <utility>

#include "Tools/BVException.hpp"
#include "Math/Functions/ABC.hpp"
#include "Math/Functions/Details.hpp"

namespace BV {
namespace Math {
namespace Functions {

struct InvalidCompositeFunctionException :
        public BV::Tools::Exceptions::BVException
{
    InvalidCompositeFunctionException(const std::string & message) :
        BV::Tools::Exceptions::BVException(message)
    {
    }
} ;

template <std::size_t NPrevious, typename Func, typename ...FunctionsTypes>
struct NEvaluator
{
    static constexpr std::size_t N = NEvaluator<NPrevious+Func::startDimension, FunctionsTypes...>::N ;
} ;

template <std::size_t NPrevious, typename Func>
struct NEvaluator<NPrevious, Func>
{
    static constexpr std::size_t N = NPrevious + Func::startDimension ;
} ;

template <std::size_t MPrevious, typename Func, typename ...FunctionsTypes>
struct MEvaluator
{
    static constexpr std::size_t M = MEvaluator<MPrevious+Func::endDimension, FunctionsTypes...>::M ;
} ;

template <std::size_t MPrevious, typename Func>
struct MEvaluator<MPrevious, Func>
{
    static constexpr std::size_t M = MPrevious + Func::endDimension ;
} ;

template <typename Func, typename ...FunctionsTypes>
struct DataTypeEvaluator
{
    typedef typename DataTypeEvaluator<FunctionsTypes...>::Type Type ;
} ;

template <typename Func>
struct DataTypeEvaluator<Func>
{
    typedef typename Func::DataType Type ;
} ;

template <class ...FunctionsTypes>
class Composite : public ABC<NEvaluator<0, FunctionsTypes...>::N,
                             MEvaluator<0, FunctionsTypes...>::M,
                             typename DataTypeEvaluator<FunctionsTypes...>::Type>
{
private:
    std::tuple<FunctionsTypes...> functions_ ;
    std::size_t nFunctions_ ;

    template <std::size_t I=0, typename InputType, typename... Funcs>
    typename std::enable_if<I == sizeof...(Funcs), void>::type
        eval_(const std::tuple<Funcs...> &, const InputType &,
              std::size_t &, std::size_t &) const
    {
    }

    template <std::size_t I=0, typename InputType, typename... Funcs>
    typename std::enable_if<I < sizeof...(Funcs), void>::type
        eval_(const std::tuple<Funcs...>& funcs, const InputType & values,
              std::size_t & NSum, std::size_t & MSum) const
    {
        std::size_t NFunc(std::get<I>(funcs).getStartDimension()) ;
        std::size_t MFunc(std::get<I>(funcs).getEndDimension()) ;
        this->output_.segment(MSum, MFunc) =
                std::get<I>(funcs).eval(values.segment(NSum, NFunc)) ;
        NSum += NFunc ;
        MSum += MFunc ;
        eval_<I + 1, InputType, Funcs...>(funcs, values, NSum, MSum) ;
    }

    template <std::size_t I=0, typename InputType, typename... Funcs>
    typename std::enable_if<I == sizeof...(Funcs), void>::type
        dEval_(const std::tuple<Funcs...> &, const InputType &,
               std::size_t &, std::size_t &) const
    {
    }

    template <std::size_t I=0, typename InputType, typename... Funcs>
    typename std::enable_if<I < sizeof...(Funcs), void>::type
        dEval_(const std::tuple<Funcs...>& funcs, const InputType & values,
               std::size_t & NSum, std::size_t & MSum) const
    {
        std::size_t NFunc(std::get<I>(funcs).getStartDimension()) ;
        std::size_t MFunc(std::get<I>(funcs).getEndDimension()) ;
        this->der_.block(MSum, NSum, MFunc, NFunc) =
                std::get<I>(funcs).dEval(values.segment(NSum, NFunc)) ;
        NSum += NFunc ;
        MSum += MFunc ;
        dEval_<I + 1, InputType, Funcs...>(funcs, values, NSum, MSum) ;
    }

    template <std::size_t NFunc, std::size_t MFunc, typename FuncDer2OutputType>
    typename std::enable_if<((NFunc>1) && (MFunc>1)), void>::type
        setDer2_(const FuncDer2OutputType & fDer2,
                 const std::size_t & MSum, const std::size_t & NSum) const
    {
        Eigen::DSizes<Eigen::Index, 3> offsets {static_cast<Eigen::Index>(MSum),
        	                                    static_cast<Eigen::Index>(NSum),
												static_cast<Eigen::Index>(NSum)} ;
        Eigen::DSizes<Eigen::Index, 3> extents {static_cast<Eigen::Index>(MFunc),
        	                                    static_cast<Eigen::Index>(NFunc),
												static_cast<Eigen::Index>(NFunc)} ;
        this->der2_.slice(offsets, extents) = fDer2 ;
    }

    template <std::size_t NFunc, std::size_t MFunc, typename FuncDer2OutputType>
    typename std::enable_if<((NFunc==1) && (MFunc>1)), void>::type
        setDer2_(const FuncDer2OutputType & fDer2,
                 const std::size_t & MSum, const std::size_t & NSum) const
    {
        //Eigen::array<unsigned, 3> offsets = {MSum, NSum, NSum} ;
        //Eigen::array<unsigned, 3> extents = {MFunc, 1, 1} ;
        //this->der2_.slice(offsets, extents) = fDer2 ;
        for (std::size_t i=MSum; i<MFunc; ++i)
        {
            this->der2_(i, static_cast<Eigen::Index>(NSum), static_cast<Eigen::Index>(NSum)) = fDer2(i) ;
        }
    }

    template <std::size_t NFunc, std::size_t MFunc, typename FuncDer2OutputType>
    typename std::enable_if<((NFunc>1) && (MFunc==1)), void>::type
        setDer2_(const FuncDer2OutputType & fDer2,
                 const std::size_t & MSum, const std::size_t & NSum) const
    {
        //Eigen::array<unsigned, 3> offsets = {MSum, NSum, NSum} ;
        //Eigen::array<unsigned, 3> extents = {1, NFunc, NFunc} ;
        //this->der2_.slice(offsets, extents) = fDer2 ;
        for (std::size_t i=NSum; i<NFunc; ++i)
        {
            for (std::size_t j=NSum; i<NFunc; ++i)
            {
                this->der2_(static_cast<Eigen::Index>(MSum),
                		    static_cast<Eigen::Index>(i),
							static_cast<Eigen::Index>(j)) = fDer2(i, j) ;
            }
        }
    }

    template <std::size_t NFunc, std::size_t MFunc, typename FuncDer2OutputType>
    typename std::enable_if<((NFunc==1) && (MFunc==1)), void>::type
        setDer2_(const FuncDer2OutputType & fDer2,
                 const std::size_t & MSum, const std::size_t & NSum) const
    {
        this->der2_(MSum, NSum, NSum) = fDer2 ;
    }


    template <std::size_t I=0, typename InputType, typename... Funcs>
    typename std::enable_if<I == sizeof...(Funcs), void>::type
        dEval2_(const std::tuple<Funcs...> &, const InputType &,
                std::size_t &, std::size_t &) const
    {
    }

    template<std::size_t I=0, typename InputType, typename... Funcs>
    typename std::enable_if<I < sizeof...(Funcs), void>::type
        dEval2_(const std::tuple<Funcs...>& funcs, const InputType & values,
                std::size_t & NSum, std::size_t & MSum) const
    {
        typedef typename std::tuple_element<I, typename std::decay<decltype(funcs)>::type>::type FuncType ;
        constexpr std::size_t NFunc(FuncType::startDimension) ;
        constexpr std::size_t MFunc(FuncType::endDimension) ;
        typename FuncType::Der2OutputType der2(
                        std::get<I>(funcs).dEval2(values.segment(NSum, NFunc))
                                              ) ;
        setDer2_<NFunc, MFunc>(der2, NSum, MSum) ;
        NSum += NFunc ;
        MSum += MFunc ;
        dEval2_<I + 1, InputType, Funcs...>(funcs, values, NSum, MSum) ;
    }

public:
    static constexpr std::size_t N = NEvaluator<0, FunctionsTypes...>::N ;
    static constexpr std::size_t M = MEvaluator<0, FunctionsTypes...>::M ;
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename DataTypeEvaluator<FunctionsTypes...>::Type DataType ;
    typedef typename ABC<N, M, DataType>::InputType InputType ;
    typedef typename ABC<N, M, DataType>::OutputType OutputType ;
    typedef typename ABC<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, M, DataType>::Der2OutputType Der2OutputType ;
    
    Composite(FunctionsTypes...functions) :
        ABC<N, M, DataType>(),
        functions_(std::move(functions)...),
        nFunctions_(sizeof...(FunctionsTypes))
    {
    }

    using ABC<N, M, DataType>::eval ;
    using ABC<N, M, DataType>::operator() ;
    using ABC<N, M, DataType>::dEval ;
    using ABC<N, M, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        std::size_t iN(0) ;
        std::size_t iM(0) ;
        eval_(functions_, values, iN, iM) ;
        return this->output_ ;
    }

    const DerOutputType & dEval(const InputType & values) const
    {
        Details::Traits<N, M, DataType>::setDerZero(this->der_) ;
        std::size_t iN(0) ;
        std::size_t iM(0) ;
        dEval_(functions_, values, iN, iM) ;
        return this->der_ ;
    }

    const Der2OutputType & dEval2(const InputType & values) const
    {
        Details::Traits<N, M, DataType>::setDer2Zero(this->der2_) ;
        std::size_t iN(0) ;
        std::size_t iM(0) ;
        dEval2_(functions_, values, iN, iM) ;
        return this->der2_ ;
    }
} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Composite_hpp
