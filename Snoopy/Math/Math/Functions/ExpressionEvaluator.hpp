#ifndef BV_Math_Functions_ExpressionEvaluator_hpp
#define BV_Math_Functions_ExpressionEvaluator_hpp

#include <boost/spirit/version.hpp>
#if !defined(SPIRIT_VERSION) || SPIRIT_VERSION < 0x2010
#error "At least Spirit version 2.1 required"
#endif
#include <boost/math/constants/constants.hpp>
#include <boost/phoenix.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/adapted.hpp>

#include "Tools/BVException.hpp"
#include "Math/Functions/ABC.hpp"

namespace BV {
namespace Math {
namespace Functions {

namespace qi    = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phx   = boost::phoenix;

namespace { // anonymous

struct LazyPow_
{
    template <typename X, typename Y>
    struct result
    {
        typedef X type ;
    } ;

    template <typename X, typename Y>
    X operator()(X x, Y y) const
    {
        return std::pow(x, y) ;
    }
} ;

struct LazyUfunc_
{
    template <typename F, typename A1>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1>
    A1 operator()(F f, A1 a1) const
    {
        return f(a1) ;
    }
} ;

struct LazyUSharedFunc_
{
    template <typename F, typename A1>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1>
    A1 operator()(F f, A1 a1) const
    {
        return f->eval(a1) ;
    }
} ;

struct LazyBfunc_
{
    template <typename F, typename A1, typename A2>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1, typename A2>
    A1 operator()(F f, A1 a1, A2 a2) const
    {
        return f(a1, a2) ;
    }
} ;

struct LazyBSharedFunc_
{
    template <typename F, typename A1, typename A2>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1, typename A2>
    A1 operator()(F f, A1 a1, A2 a2) const
    {
        return f->eval(a1, a2) ;
    }
} ;

struct LazyTSharedFunc_
{
    template <typename F, typename A1, typename A2, typename A3>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1, typename A2, typename A3>
    A1 operator()(F f, A1 a1, A2 a2, A3 a3) const
    {
        return f->eval(a1, a2, a3) ;
    }
} ;

struct LazyQSharedFunc_
{
    template <typename F, typename A1, typename A2, typename A3, typename A4>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1, typename A2, typename A3, typename A4>
    A1 operator()(F f, A1 a1, A2 a2, A3 a3, A4 a4) const
    {
        return f->eval(a1, a2, a3, a4) ;
    }
} ;

struct LazyFSharedFunc_
{
    template <typename F, typename A1, typename A2, typename A3, typename A4, typename A5>
    struct result
    {
        typedef A1 type ;
    } ;

    template <typename F, typename A1, typename A2, typename A3, typename A4, typename A5>
    A1 operator()(F f, A1 a1, A2 a2, A3 a3, A4 a4, A5 a5) const
    {
        return f->eval(a1, a2, a3, a4, a5) ;
    }
} ;

template <typename T>
T MaxByValue(const T a, const T b)
{
    return std::max(a, b) ;
}

template <typename T>
T MinByValue(const T a, const T b)
{
    return std::min(a, b) ;
}

} // end namespace anonymous

// Pass functions to boost
template <typename T, typename Iterator>
class ExpressionParser : public qi::grammar<Iterator, T(), ascii::space_type>
{
private:
    boost::spirit::qi::rule<
            Iterator, T(), boost::spirit::ascii::space_type
                           > expression_, term_, factor_, primary_ ;
    bool isInitialised_ ;

    // symbol table for constants like "pi"
    struct Constant_ : boost::spirit::qi::symbols<
                typename std::iterator_traits<Iterator>::value_type, T
                                                 >
    {
        Constant_(void)
        {
            this->add
                ("digits",   std::numeric_limits<T>::digits   )
                ("digits10", std::numeric_limits<T>::digits10 )
                ("e" ,       boost::math::constants::e<T>()   )
                ("epsilon",  std::numeric_limits<T>::epsilon())
                ("pi",       boost::math::constants::pi<T>()  )
            ;
        }
    } constant_ ;

    struct Variable_ : boost::spirit::qi::symbols<
                typename std::iterator_traits<Iterator>::value_type, T
                                                 >
    {
        void addVariable(const std::string & name)
        {
            this->add(name) ;
        }
        void addVariable(const std::string & name, const T & value)
        {
            this->add(name, value) ;
        }
        void setVariable(const std::string & name, const T & value)
        {
            this->at(name) = value ;
        }
        T & getVariable(const std::string & name)
        {
            return this->at(name) ;
        }
    } variable_ ;

    // symbol table for unary functions like "abs"
    struct Ufunc_ : boost::spirit::qi::symbols<
                typename std::iterator_traits<Iterator>::value_type, T (*)(T)
                                              >
    {
        Ufunc_(void)
        {
            this->add
                ("abs"  , static_cast<T (*)(T)>(&std::abs  ))
                ("acos" , static_cast<T (*)(T)>(&std::acos ))
                ("asin" , static_cast<T (*)(T)>(&std::asin ))
                ("atan" , static_cast<T (*)(T)>(&std::atan ))
                ("ceil" , static_cast<T (*)(T)>(&std::ceil ))
                ("cos"  , static_cast<T (*)(T)>(&std::cos  ))
                ("cosh" , static_cast<T (*)(T)>(&std::cosh ))
                ("exp"  , static_cast<T (*)(T)>(&std::exp  ))
                ("floor", static_cast<T (*)(T)>(&std::floor))
                ("log10", static_cast<T (*)(T)>(&std::log10))
                ("log"  , static_cast<T (*)(T)>(&std::log  ))
                ("sin"  , static_cast<T (*)(T)>(&std::sin  ))
                ("sinh" , static_cast<T (*)(T)>(&std::sinh ))
                ("sqrt" , static_cast<T (*)(T)>(&std::sqrt ))
                ("tan"  , static_cast<T (*)(T)>(&std::tan  ))
                ("tanh" , static_cast<T (*)(T)>(&std::tanh ))
            ;
        }
    } ufunc_ ;

    // symbol table for binary functions like "pow"
    struct Bfunc_ : boost::spirit::qi::symbols<
                typename std::iterator_traits<Iterator>::value_type, T (*)(T, T)
                                              >
    {
        Bfunc_(void)
        {
            this->add
                ("atan2", static_cast<T (*)(T, T)>(&std::atan2))
                ("max"  , static_cast<T (*)(T, T)>(&MaxByValue))
                ("min"  , static_cast<T (*)(T, T)>(&MinByValue))
                ("pow"  , static_cast<T (*)(T, T)>(&std::pow  ))
            ;
        }
    } bfunc_ ;

    struct UFunctor_ : boost::spirit::qi::symbols<
                          typename std::iterator_traits<Iterator>::value_type,
                          std::shared_ptr<ABC<1, 1, T> >
                                                 >
    {
        void addFunctor(const std::string & name,
                        std::shared_ptr<ABC<1, 1, T> > func)
        {
            this->add(name, func) ;
        }
    } uFunctor_ ;

    struct BFunctor_ : boost::spirit::qi::symbols<
                          typename std::iterator_traits<Iterator>::value_type,
                          std::shared_ptr<ABC<2, 1, T> >
                                                 >
    {
        void addFunctor(const std::string & name,
                        std::shared_ptr<ABC<2, 1, T> > func)
        {
            this->add(name, func) ;
        }
    } bFunctor_ ;

    struct TFunctor_ : boost::spirit::qi::symbols<
                          typename std::iterator_traits<Iterator>::value_type,
                          std::shared_ptr<ABC<3, 1, T> >
                                                 >
    {
        void addFunctor(const std::string & name,
                        std::shared_ptr<ABC<3, 1, T> > func)
        {
            this->add(name, func) ;
        }
    } tFunctor_ ;

    struct QFunctor_ : boost::spirit::qi::symbols<
                          typename std::iterator_traits<Iterator>::value_type,
                          std::shared_ptr<ABC<4, 1, T> >
                                                  >
    {
        void addFunctor(const std::string & name,
                        std::shared_ptr<ABC<4, 1, T> > func)
        {
            this->add(name, func) ;
        }
    } qFunctor_ ;

    struct FFunctor_ : boost::spirit::qi::symbols<
                          typename std::iterator_traits<Iterator>::value_type,
                          std::shared_ptr<ABC<5, 1, T> >
                                                 >
    {
        void addFunctor(const std::string & name,
                        std::shared_ptr<ABC<5, 1, T> > func)
        {
            this->add(name, func) ;
        }
    } fFunctor_ ;

public:
    void initialise(void)
    {
        if (isInitialised_)
        {
            return ;
        }
        using boost::spirit::qi::real_parser ;
        using boost::spirit::qi::real_policies ;
        using boost::spirit::qi::lexeme ;
        using boost::spirit::qi::char_ ;
        using boost::spirit::qi::as_string ;
        using boost::spirit::qi::alpha ;
        real_parser<T ,real_policies<T> > real ;

        using boost::spirit::qi::_1 ;
        using boost::spirit::qi::_2 ;
        using boost::spirit::qi::_3 ;
        using boost::spirit::qi::_4 ;
        using boost::spirit::qi::_5 ;
        using boost::spirit::qi::_6 ;
        using boost::spirit::qi::no_case ;
        using boost::spirit::qi::_val ;

        boost::phoenix::function<LazyPow_>   LazyPow ;
        boost::phoenix::function<LazyUfunc_> LazyUfunc ;
        boost::phoenix::function<LazyUSharedFunc_> LazyUSharedFunc ;
        boost::phoenix::function<LazyBfunc_> LazyBfunc ;
        boost::phoenix::function<LazyBSharedFunc_> LazyBSharedFunc ;
        boost::phoenix::function<LazyTSharedFunc_> LazyTSharedFunc ;
        boost::phoenix::function<LazyQSharedFunc_> LazyQSharedFunc ;
        boost::phoenix::function<LazyFSharedFunc_> LazyFSharedFunc ;

        expression_ =
            term_                   [_val =  _1]
            >> *(  ('+' >> term_    [_val += _1])
                |  ('-' >> term_    [_val -= _1])
                )
            ;

        term_ =
            factor_                 [_val =  _1]
            >> *(  ('*' >> factor_  [_val *= _1])
                |  ('/' >> factor_  [_val /= _1])
                )
            ;

        factor_ =
            primary_                [_val =  _1]
            >> *(  ("**" >> factor_ [_val = LazyPow(_val, _1)])
                )
            ;

        primary_ =
            real                   [_val =  _1]
            |   '(' >> expression_  [_val =  _1] >> ')'
            |   ('-' >> primary_    [_val = -_1])
            |   ('+' >> primary_    [_val =  _1])
            |   (no_case[ufunc_] >> '(' >> expression_ >> ')')
                                   [_val = LazyUfunc(_1, _2)]
            |   (uFunctor_ >> '(' >> expression_ >> ')')
                                   [_val = LazyUSharedFunc(_1, _2)]
            |   (no_case[bfunc_] >> '(' >> expression_ >> ','
                                        >> expression_ >> ')')
                                   [_val = LazyBfunc(_1, _2, _3)]
            |   (bFunctor_ >> '(' >> expression_ >> ','
                                  >> expression_ >> ')')
                                   [_val = LazyBSharedFunc(_1, _2, _3)]
            |   (tFunctor_ >> '(' >> expression_ >> ','
                                  >> expression_ >> ','
                                  >> expression_ >> ')')
                                   [_val = LazyTSharedFunc(_1, _2, _3, _4)]
            |   (qFunctor_ >> '(' >> expression_ >> ','
                                  >> expression_ >> ','
                                  >> expression_ >> ','
                                  >> expression_ >> ')')
                                   [_val = LazyQSharedFunc(_1, _2, _3, _4, _5)]
            |   (fFunctor_ >> '(' >> expression_ >> ','
                                  >> expression_ >> ','
                                  >> expression_ >> ','
                                  >> expression_ >> ','
                                  >> expression_ >> ')')
                                   [_val = LazyFSharedFunc(_1, _2, _3, _4, _5, _6)]
            |   no_case[constant_]  [_val =  _1]
            |   variable_           [_val =  _1]
            ;
        isInitialised_ = true ;
    }

    ExpressionParser(void) :
        ExpressionParser::base_type(expression_), isInitialised_(false)
    {
    }

    //template <typename ... Args>
    //ExpressionParser(const Args &... variables) :
    //    ExpressionParser::base_type(expression_), isInitialised_(false)
    //{
    //    // FIXME check the type of Args ?
    //    // If we don't it can be a string or a vector of strings...
    //    //std::vector<std::string> vars = {static_cast<std::string>(variables)...} ;
    //    std::vector<std::string> vars = {variables...} ;
    //    // initialise the variables
    //    for (const std::string & var : vars)
    //    {
    //         variable_.addVariable(var) ;
    //    }
    //}

    ExpressionParser(const ExpressionParser<T, Iterator> & other) :
        ExpressionParser::base_type(expression_), isInitialised_(false)
    {
        variable_ = other.variable_ ;
        uFunctor_ = other.uFunctor_ ;
        bFunctor_ = other.bFunctor_ ;
        tFunctor_ = other.tFunctor_ ;
        qFunctor_ = other.qFunctor_ ;
        fFunctor_ = other.fFunctor_ ;
    }

    ExpressionParser<T, Iterator> & operator=(
                         const ExpressionParser<T, Iterator> & other
                                             )
    {
        variable_ = other.variable_ ;
        uFunctor_ = other.uFunctor_ ;
        bFunctor_ = other.bFunctor_ ;
        tFunctor_ = other.tFunctor_ ;
        qFunctor_ = other.qFunctor_ ;
        fFunctor_ = other.fFunctor_ ;
        isInitialised_ = false ;
        return *this ;
    }

    void addFunctor(const std::string & name, std::shared_ptr<ABC<1, 1, T> > func)
    {
        uFunctor_.addFunctor(name, func) ;
        isInitialised_ = false ;
    }

    void addFunctor(const std::string & name, std::shared_ptr<ABC<2, 1, T> > func)
    {
        bFunctor_.addFunctor(name, func) ;
        isInitialised_ = false ;
    }

    void addFunctor(const std::string & name, std::shared_ptr<ABC<3, 1, T> > func)
    {
        tFunctor_.addFunctor(name, func) ;
        isInitialised_ = false ;
    }

    void addFunctor(const std::string & name, std::shared_ptr<ABC<4, 1, T> > func)
    {
        qFunctor_.addFunctor(name, func) ;
        isInitialised_ = false ;
    }

    void addFunctor(const std::string & name, std::shared_ptr<ABC<5, 1, T> > func)
    {
        fFunctor_.addFunctor(name, func) ;
        isInitialised_ = false ;
    }

    void addVariable(const std::string & var)
    {
        variable_.addVariable(var) ;
        isInitialised_ = false ;
    }

    void setVariable(const std::string & var, const T & value)
    {
        variable_.setVariable(var, value) ;
    }

    T & getVariable(const std::string & var)
    {
        return variable_.getVariable(var) ;
    }
} ;

template <typename T, typename Iterator>
bool parse(Iterator & iter,
           Iterator end,
           const ExpressionParser<T, Iterator> & parser,
           T &result)
{
    return boost::spirit::qi::phrase_parse(
                iter, end, parser, boost::spirit::ascii::space, result
                                          ) ;
}

template <typename T>
class ExpressionEvaluator
{
private:
    std::string expression_ ;
    ExpressionParser<T, std::string::const_iterator> parser_ ;

public:
    ExpressionEvaluator(void)
    {
    }

    ExpressionEvaluator(const std::string & expression) :
        expression_(expression), parser_()
    {
    }

    //template <typename ...Args>
    //ExpressionEvaluator(const std::string & expression,
    //                    const Args &... variables) :
    //    expression_(expression), parser_(variables...)
    //{
    //}

    ExpressionEvaluator(const ExpressionEvaluator<T> & other) :
        expression_(other.expression_), parser_(other.parser_)
    {
    }

    ExpressionEvaluator<T> & operator=(const ExpressionEvaluator<T> & other)
    {
        if (this != &other)
        {
            expression_ = other.expression_ ;
            parser_ = other.parser_ ;
        }
        return *this ;
    }

    template <typename F>
    void addFunctor(const std::string & name, F func)
    {
        parser_.addFunctor(name, func) ;
    }

    void addVariable(const std::string & var)
    {
        parser_.addVariable(var) ;
    }

    void setVariable(const std::string & var, const T & val)
    {
        parser_.setVariable(var, val) ;
    }

    T evaluate(void)
    {
        double result ;
        std::string::const_iterator iter(expression_.begin()) ;
        std::string::const_iterator end(expression_.end()) ;
        // FIXME there may be a way not to parse each time...
        parser_.initialise() ;
        if (!parse(iter, end, parser_, result))
        {
            throw BV::Tools::Exceptions::BVException(
                                     "Expression evaluation failed..."
                                                    ) ;
        }
        return result ;
    }

   T derivative(T & x, const T & h=T(0.00000001))
   {
      T x_init = x ;
      x = x_init + T(2) * h ;
      T y0 = evaluate() ;
      x = x_init + h ;
      T y1 = evaluate() ;
      x = x_init - h ;
      T y2 = evaluate() ;
      x = x_init - T(2) * h ;
      T y3 = evaluate() ;
      x = x_init ;

      return (-y0 + T(8) * (y1 - y2) + y3) / (T(12) * h) ;
   }

   T secondDerivative(T & x, const T & h=T(0.00001))
   {
      T y = evaluate() ;
      T x_init = x ;
      x = x_init + T(2) * h ;
      T y0 = evaluate() ;
      x = x_init + h ;
      T y1 = evaluate() ;
      x = x_init - h ;
      T y2 = evaluate() ;
      x = x_init - T(2) * h ;
      T y3 = evaluate() ;
      x = x_init ;

      return (-y0 + T(16) * (y1 + y2) - T(30) * y - y3) / (T(12) * h * h) ;
   }

   T thirdDerivative(T & x, const T & h=T(0.0001))
   {
      T x_init = x ;
      x = x_init + T(2) * h ;
      T y0 = evaluate() ;
      x = x_init + h ;
      T y1 = evaluate() ;
      x = x_init - h ;
      T y2 = evaluate() ;
      x = x_init - T(2) * h ;
      T y3 = evaluate() ;
      x = x_init ;

      return (y0 + T(2) * (y2 - y1) - y3) / (T(2) * h * h * h) ;
   }

   T derivative(const std::string & varName, const T& h=T(0.00000001))
   {
       // FIXME perform some check if varName exists for example...
       T & x = parser_.getVariable(varName) ;
       T x_original = x ;
       T result = derivative(x, h) ;
       x = x_original ;

       return result ;
   }

   T secondDerivative(const std::string& varName, const T & h=T(0.00001))
   {
       // FIXME perform some check if varName exists for example...
       T & x = parser_.getVariable(varName) ;
       T x_original = x ;
       T result = secondDerivative(x, h) ;
       x = x_original ;

       return result ;
   }

   T thirdDerivative(const std::string & varName, const T & h=T(0.0001))
   {
       // FIXME perform some check if varName exists for example...
       T & x = parser_.getVariable(varName) ;
       T x_original = x ;
       T result = secondDerivative(x, h) ;
       x = x_original ;

       return result ;
   }

} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_ExpressionEvaluator_hpp

