#ifndef BV_Math_Functions_Polynomial_hpp
#define BV_Math_Functions_Polynomial_hpp

#include <Eigen/Dense>
#include <array>
#include <initializer_list>

#include "Math/Functions/ABC.hpp"

namespace BV {
namespace Math {
namespace Functions {
/*!
 * \class Polynomial Math/Functions/Polynomial.hpp Functions::Polynomial
 * \brief Class that allows the evaluation of M polynomial expressions,
 *     along with their derivatives.
 */
template <std::size_t M, typename DataType=double>
class Polynomial : public ABC<1, M, DataType>
{
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<1, M, DataType>::InputType InputType ;
    typedef typename ABC<1, M, DataType>::OutputType OutputType ;
    typedef typename ABC<1, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<1, M, DataType>::Der2OutputType Der2OutputType ;
    
    typedef Eigen::Matrix<DataType, Eigen::Dynamic, 1> CoefsType ;

    /*!
     * \brief Constructor
     * \param coefs: an array of M vectors of coefficients for each of
     *     the polynoms.
     *
     * Example for the polynoms \f$y = 2 + 3 x + 4 x^2\f$ and \f$y=1 + 6 x\f$:
     * \code{.cpp}
     *     Eigen::Vector3d coefs1 ;
     *     coefs1 << 2., 3., 4. ;
     *     Eigen::Vector2d coefs2 ;
     *     coefs2 << 1., 6. ;
     *     std::array<typename Polynomial<2>::CoefsType, 2> coefs ;
     *     coefs[0] = coefs1 ;
     *     coefs[1] = coefs2 ;
     *     Polynomial<2> func(coefs) ;
     * \endcode
     *
     */
    Polynomial(const std::array<CoefsType, M> & coefs) :
        ABC<1, M, DataType>(),
        coefs_(coefs)
    {
        for (std::size_t i=0; i<M; ++i)
        {
            // Check if the tail of the coefs array is zero.
            // If so, readapt the coefs and the corresponding order
            std::size_t size(coefs_[i].size()) ;
            for (std::size_t j=0; j<size; ++j)
            {
                if (coefs_[i].tail(size-j).isZero())
                {
                    // Copy the coefs in a new vector with correct size
                    CoefsType newCoefs(coefs_[i].head(j+1)) ;
                    coefs_[i] = newCoefs ;
                    break ;
                }
            }
            orders_[i] = coefs_[i].size() - 1 ;
        }
    }

    /*!
     * \brief Constructor
     * \param coefs: variadic arguments corresponding to the M vectors of
     *     coefficients for each of the polynoms.
     *
     * Example for the polynoms \f$y = 2 + 3 x + 4 x^2\f$ and \f$y=1 + 6 x\f$:
     * \code{.cpp}
     *     Eigen::Vector3d coefs1 ;
     *     coefs1 << 2., 3., 4. ;
     *     Eigen::Vector2d coefs2 ;
     *     coefs2 << 1., 6. ;
     *     Polynomial<2> func(coefs1, coefs2) ;
     * \endcode
     *
     */
    template <typename ...CoefsTypes>
    Polynomial(CoefsTypes... coefs) :
        Polynomial(getCoefs_({coefs...}))
    {
    }

    using ABC<1, M, DataType>::eval ;
    using ABC<1, M, DataType>::operator() ;
    using ABC<1, M, DataType>::dEval ;
    using ABC<1, M, DataType>::dEval2 ;

    /*!
     * \brief Evaluation of the polynoms in x.
     * \param x: the value for which to evaluate the polynoms.
     *
     * The evaluation uses Horner method:
     *
     * \f$ y = a_0 + x ( a_1 + x (a_2 + ... + x (a_{n-1} + a_n x)))\f$
     *
     * Example:
     * \code{.cpp}
     *     Eigen::Vector3d coefs1 ;
     *     coefs1 << 2., 3., 4. ;
     *     Eigen::Vector2d coefs2 ;
     *     coefs2 << 1., 6. ;
     *     Polynomial<2> func(coefs1, coefs2) ;
     *     Eigen::Vector2d res(func.eval(1.)) ;
     *     // res now holds:
     *     // res(0) = 9.
     *     // res(1) = 7.
     * \endcode
     *
     */
    const OutputType & eval(const InputType & x) const
    {
        for (std::size_t i=0; i<M; ++i)
        {
            const CoefsType & coefs(coefs_[i]) ;
            std::size_t order(orders_[i]) ;
            // Evaluation using Horner method
            this->output_(i) = coefs(order) ;
            for (std::size_t iCoef=order; iCoef>0; --iCoef)
            {
                this->output_(i) *= x ;
                this->output_(i) += coefs(iCoef-1) ;
            }
        }
        return this->output_ ;
    }

    /*!
     * \brief Evaluation of the polynoms first derivative in x.
     * \param x: the value for which to evaluate the polynoms derivative.
     *
     * This function delegates to the dEval_ function. Please refer to the
     * documentation of this function for more information.
     *
     * Example:
     * \code{.cpp}
     *     Eigen::Vector3d coefs1 ;
     *     coefs1 << 2., 3., 4. ;
     *     Eigen::Vector2d coefs2 ;
     *     coefs2 << 1., 6. ;
     *     Polynomial<2> func(coefs1, coefs2) ;
     *     Eigen::Vector2d der(func.dEval(1.)) ;
     *     // der now holds:
     *     // der(0) = 11.
     *     // der(1) = 6.
     * \endcode
     *
     */
    const DerOutputType & dEval(const InputType & x) const
    {
        if (!firstDerivatives_.size())
        {
            firstDerivatives_.push_back(Polynomial<M, DataType>(getDerCoefs_(1))) ;
        }
        return firstDerivatives_[0].eval(x) ;
        //return dEval_(1, x, this->der_) ;
    }

    /*!
     * \brief Evaluation of the polynoms second derivative in x.
     * \param x: the value for which to evaluate the polynoms derivative.
     *
     * This function delegates to the dEval_ function. Please refer to the
     * documentation of this function for more information.
     *
     * Example:
     * \code{.cpp}
     *     Eigen::Vector3d coefs1 ;
     *     coefs1 << 2., 3., 4. ;
     *     Eigen::Vector2d coefs2 ;
     *     coefs2 << 1., 6. ;
     *     Polynomial<2> func(coefs1, coefs2) ;
     *     Eigen::Vector2d der2(func.dEval2(1.)) ;
     *     // der2 now holds:
     *     // der2(0) = 8.
     *     // der2(1) = 0.
     * \endcode
     *
     */
    const Der2OutputType & dEval2(const InputType & x) const
    {
        if (!secondDerivatives_.size())
        {
            secondDerivatives_.push_back(Polynomial<M, DataType>(getDerCoefs_(2))) ;
        }
        return secondDerivatives_[0].eval(x) ;
        //return dEval_(2, x, this->der2_) ;
    }

    const Polynomial<M, DataType> & getPolynomialDer(void) const
    {
        if (!firstDerivatives_.size())
        {
            firstDerivatives_.push_back(Polynomial<M, DataType>(getDerCoefs_(1))) ;
        }
        return firstDerivatives_[0] ;
    }

    const Polynomial<M, DataType> & getPolynomialDer2(void) const
    {
        if (!secondDerivatives_.size())
        {
            secondDerivatives_.push_back(Polynomial<M, DataType>(getDerCoefs_(2))) ;
        }
        return secondDerivatives_[0] ;
    }

private:
    /*! Coefficients of the polynom: \f$(a, b, c)\f$ for \f$y = a + b x + c x^2\f$ */
    std::array<CoefsType, M> coefs_ ;
    /*! Order of the polynom: \f$2\f$ for \f$y = a + b x + c x^2\f$ */
    std::array<std::size_t, M> orders_ ;
    mutable std::vector<Polynomial<M, DataType> > firstDerivatives_ ;
    mutable std::vector<Polynomial<M, DataType> > secondDerivatives_ ;

    /*!
     * \brief Computes the factorial of provided number.
     * \param i: the number for which the factorial should be computed.
     * \return the factorial \f$!i\f$
     */
    std::size_t factorial_(const std::size_t & i) const
    {
        if (i < 2)
        {
            return 1 ;
        }
        return i * factorial_(i-1) ;
    }

    /*!
     * \brief Evaluation of the polynoms derivative in x.
     * \param k: the order of the derivative (1 for 1st der, 2 for 2nd der, ...)
     * \param x: the value for which to evaluate the polynoms derivative.
     *
     * The derivative is calculated using following formulation for the
     * second derivative:
     *
     * \f$ y = !2 a_2 + x (\frac{!3}{!1} a_3 + x( \frac{!4}{!2} a_4 + ...+
     *         x( \frac{!(n-1)}{!(n-3)} a_{n-1} + \frac{!n}{!(n-2)}a_n x)))\f$
     *
     */
    template <typename Outp>
    const Outp & dEval_(const std::size_t & k, const InputType & x,
                        Outp & outp) const
    {
        for (std::size_t i=0; i<M; ++i)
        {
            const CoefsType & coefs(coefs_[i]) ;
            std::size_t order(orders_[i]) ;
            if (k > order)
            {
                outp(i) = 0. ;
                continue ;
            }
            outp(i) = coefs(order) * factorial_(order) / factorial_(order-k) ;
            for (std::size_t iCoef=order-1; iCoef>k-1; --iCoef)
            {
                outp(i) *= x ;
                outp(i) += factorial_(iCoef) / factorial_(iCoef-k) * coefs(iCoef) ;
            }
        }
        return outp ;
    }

    std::array<CoefsType, M> getDerCoefs_(const std::size_t & k) const
    {
        std::array<CoefsType, M> res ;
        for (std::size_t i=0; i<M; ++i)
        {
            const CoefsType & coefs(coefs_[i]) ;
            std::size_t size(coefs.size()) ;
            std::size_t order(orders_[i]) ;
            if (k > order)
            {
                CoefsType tmp(1) ;
                tmp(0) = 0. ;
                res[i] = tmp ;
                continue ;
            }
            CoefsType tmp(size - k) ;
            std::size_t ind(size-k-1) ;
            tmp(ind) = coefs(order) * factorial_(order) / factorial_(order-k) ;
            --ind ;
            for (std::size_t iCoef=order-1; iCoef>k-1; --iCoef, --ind)
            {
                tmp(ind) = factorial_(iCoef) / factorial_(iCoef-k) * coefs(iCoef) ;
            }
            res[i] = tmp ;
        }
        return res ;
    }

    /*!
     * \brief Utility function to map variadic arguments to the coefs type.
     */
    std::array<CoefsType, M> getCoefs_(std::initializer_list<CoefsType> coefs) const
    {
        std::array<CoefsType, M> tmp ;
        auto itVals(coefs.begin()) ;
        auto endVals(coefs.end()) ;
        std::size_t i(0) ;
        for (;itVals!=endVals;++itVals, ++i)
        {
            tmp[i] = *itVals ;
        }
        return tmp ;
    }

} ;

/*!
 * \brief specialisation for M=1 as the output type is not iterable.
 */
template <typename DataType>
class Polynomial<1, DataType> : public ABC<1, 1, DataType>
{
public:
    typedef typename ABC<1, 1, DataType>::InputType InputType ;
    typedef typename ABC<1, 1, DataType>::OutputType OutputType ;
    typedef typename ABC<1, 1, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<1, 1, DataType>::Der2OutputType Der2OutputType ;

    typedef Eigen::Matrix<DataType, Eigen::Dynamic, 1> CoefsType ;

    Polynomial(const CoefsType & coefs) :
        ABC<1, 1, DataType>(),
        coefs_(coefs)
    {
        // Check if the tail of the coefs array is zero.
        // If so, readapt the coefs and the corresponding order
        std::size_t size(coefs_.size()) ;
        for (std::size_t j=0; j<size; ++j)
        {
            if (coefs_.tail(size-j).isZero())
            {
                // Copy the coefs in a new vector with correct size
                CoefsType newCoefs(coefs_.head(j+1)) ;
                coefs_ = newCoefs ;
                break ;
            }
        }
        order_ = coefs_.size() - 1 ;
    }

    using ABC<1, 1, DataType>::eval ;
    using ABC<1, 1, DataType>::operator() ;
    using ABC<1, 1, DataType>::dEval ;
    using ABC<1, 1, DataType>::dEval2 ;

    const OutputType & eval(const InputType & x) const
    {
        const CoefsType & coefs(coefs_) ;
        std::size_t order(order_) ;
        // Evaluation using Horner method
        this->output_ = coefs(order) ;
        for (std::size_t iCoef=order; iCoef>0; --iCoef)
        {
            this->output_ *= x ;
            this->output_ += coefs(iCoef-1) ;
        }
        return this->output_ ;
    }

    const DerOutputType & dEval(const InputType & x) const
    {
        if (!firstDerivative_.size())
        {
            firstDerivative_.push_back(Polynomial<1, DataType>(getDerCoefs_(1))) ;
        }
        return firstDerivative_[0].eval(x) ;
        //return dEval_(1, x, this->der_) ;
    }

    const Der2OutputType & dEval2(const InputType & x) const
    {
        if (!secondDerivative_.size())
        {
            secondDerivative_.push_back(Polynomial<1, DataType>(getDerCoefs_(2))) ;
        }
        return secondDerivative_[0].eval(x) ;
        //return dEval_(2, x, this->der2_) ;
    }

    const Polynomial<1, DataType> & getPolynomialDer(void) const
    {
        if (!firstDerivative_.size())
        {
            firstDerivative_.push_back(Polynomial<1, DataType>(getDerCoefs_(1))) ;
        }
        return firstDerivative_[0] ;
    }

    const Polynomial<1, DataType> & getPolynomialDer2(void) const
    {
        if (!secondDerivative_.size())
        {
            secondDerivative_.push_back(Polynomial<1, DataType>(getDerCoefs_(2))) ;
        }
        return secondDerivative_[0] ;
    }

    std::size_t getOrder(void) const
    {
        return order_ ;
    }

    const CoefsType & getCoefficients(void) const
    {
        return coefs_ ;
    }

private:
    CoefsType coefs_ ;
    std::size_t order_ ;
    mutable std::vector<Polynomial<1, DataType> > firstDerivative_ ;
    mutable std::vector<Polynomial<1, DataType> > secondDerivative_ ;

    std::size_t factorial_(const std::size_t & i) const
    {
        if (i < 2)
        {
            return 1 ;
        }
        return i * factorial_(i-1) ;
    }

    template <typename Outp>
    const Outp & dEval_(const std::size_t & k, const InputType & x,
                        Outp & outp) const
    {
        const CoefsType & coefs(coefs_) ;
        std::size_t order(order_) ;
        if (k > order)
        {
            outp = 0. ;
            return outp ;
        }
        outp = coefs(order) * factorial_(order) / factorial_(order-k) ;
        for (std::size_t iCoef=order-1; iCoef>k-1; --iCoef)
        {
            outp *= x ;
            outp += factorial_(iCoef) / factorial_(iCoef-k) * coefs(iCoef) ;
        }
        return outp ;
    }

    CoefsType getDerCoefs_(const std::size_t & k) const
    {
        std::size_t size(coefs_.size()) ;
        if (k > order_)
        {
            CoefsType tmp(1) ;
            tmp(0) = 0. ;
            return tmp ;
        }
        CoefsType tmp(size - k) ;
        std::size_t ind(size-k-1) ;
        tmp(ind) = coefs_(order_) * factorial_(order_) / factorial_(order_-k) ;
        --ind ;
        for (std::size_t iCoef=order_-1; iCoef>k-1; --iCoef, --ind)
        {
            tmp(ind) = factorial_(iCoef) / factorial_(iCoef-k) * coefs_(iCoef) ;
        }
        return tmp ;
    }

} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Polynomial_hpp
