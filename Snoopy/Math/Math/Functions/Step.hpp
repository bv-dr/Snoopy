#pragma once

#include <Eigen/Dense>
#include <initializer_list>

#include "Math/Functions/ABC.hpp"

namespace BV {
namespace Math {
namespace Functions {

template <std::size_t N=1, std::size_t M=1, typename DataType=double>
class Step : public ABC<N, M, DataType>
{
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<N, M, DataType>::IndexType IndexType ;
    typedef typename ABC<N, M, DataType>::InputType InputType ;
    typedef typename ABC<N, M, DataType>::OutputType OutputType ;
    typedef typename ABC<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, M, DataType>::Der2OutputType Der2OutputType ;

    Step(const DataType & startValue, const OutputType & values) :
        ABC<N, M, DataType>()
    {
        this->startValue_ = startValue ;
        this->output_ = values ;
        Details::Traits<N, M, DataType>::setDerZero(this->der_) ;
        Details::Traits<N, M, DataType>::setDer2Zero(this->der2_) ;
    }

    using ABC<N, M, DataType>::eval ;
    using ABC<N, M, DataType>::operator() ;
    using ABC<N, M, DataType>::dEval ;
    using ABC<N, M, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        if (values < startValue_)
        {
            return this->der_ ;  // Zero !
        }
        return this->output_ ;
    }

    const DerOutputType & dEval(const InputType & values) const
    {
        return this->der_ ;
    }

    const Der2OutputType & dEval2(const InputType & values) const
    {
        return this->der2_ ;
    }
private:
    double startValue_ ;
} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
