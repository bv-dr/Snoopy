#ifndef BV_Math_Functions_Uniform_hpp
#define BV_Math_Functions_Uniform_hpp

#include <Eigen/Dense>
#include <initializer_list>

#include "Math/Functions/ABC.hpp"

namespace BV {
namespace Math {
namespace Functions {

/*!
 * \class Uniform Math/Functions/Uniform.hpp Functions::Uniform
 * \brief Provides the possibility to write M uniform functions that will
 *     be evaluated at runtime for the N input values.
 *     Examples are provided in the documentation of the individual functions.
 * \tparam N, the start dimension
 * \tparam M, the end dimension
 * \tparam DataType, the type of values treated by the uniform function.
 */
template <std::size_t N, std::size_t M, typename DataType=double>
class Uniform : public ABC<N, M, DataType>
{
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<N, M, DataType>::IndexType IndexType ;
    typedef typename ABC<N, M, DataType>::InputType InputType ;
    typedef typename ABC<N, M, DataType>::OutputType OutputType ;
    typedef typename ABC<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, M, DataType>::Der2OutputType Der2OutputType ;
    
    /*!
     * \brief Constructor
     * \param values: uniform values of each of the M functions.
     *     values are of type OutputType defined in the typedefs.
     *
     * Example:
     * \code{.cpp}
     *     Eigen::Vector2d values(Eigen::Vector2d::Ones()) ;
     *     Uniform<4, 2> func(values) ;
     * \endcode
     *
     */
    Uniform(const OutputType & values) :
        ABC<N, M, DataType>()
    {
        this->output_ = values ;
        Details::Traits<N, M, DataType>::setDerZero(this->der_) ;
        Details::Traits<N, M, DataType>::setDer2Zero(this->der2_) ;
    }

    /*!
     * \brief Constructor with variadic arguments
     * \param values: the values provides as a succession of DataType values.
     *
     * Example:
     * \code{.cpp}
     *     Uniform<4, 2> func(1., 2.) ;
     * \endcode
     *
     */
    template <typename ...DataTypes>
    Uniform(DataTypes... values) :
        Uniform(getOutput_({values...}))
    {
        static_assert(sizeof...(values) == M,
                      "Wrong number of uniform values provided") ;
    }

    using ABC<N, M, DataType>::eval ;
    using ABC<N, M, DataType>::operator() ;
    using ABC<N, M, DataType>::dEval ;
    using ABC<N, M, DataType>::dEval2 ;

    /*!
     * \brief Evaluation of the uniform functions at values.
     * \param values: In case of uniform functions, these values don't matter.
     * \return the uniform values provided at initialisation.
     *     The return type is defined in the typedefs
     *
     * Example:
     * \code{.cpp}
     *     Uniform<4, 2> func(1., 2.) ;
     *     Eigen::Vector4d values ;
     *     values << 12., 13., 14., 15. ;
     *     Eigen::Vector2d res(func.eval(values)) ;
     *     // Above call to eval can also be done as follow:
     *     // Eigen::Vector2d res(func.eval(12., 13., 14., 15.)) ;
     *     // res has now the following values:
     *     // res(0) = 1.
     *     // res(1) = 2.
     * \endcode
     *
     */
    const OutputType & eval(const InputType & values) const
    {
        return this->output_ ;
    }

    /*!
     * \brief Evaluation of the uniform functions derivative at values.
     * \param values: In case of uniform functions, these values don't matter.
     * \return a zero initialized return type.
     *     The return type is defined in the typedefs.
     *
     * Example:
     * \code{.cpp}
     *     Uniform<4, 2> func(1., 2.) ;
     *     Eigen::Vector4d values ;
     *     values << 12., 13., 14., 15. ;
     *     Eigen::Matrix<double, 2, 4> der(func.dEval(values)) ;
     *     // Above call to dEval can also be done as follow:
     *     // Eigen::Matrix<double, 2, 4> der(func.dEval(12., 13., 14., 15.)) ;
     *     // der is now a matrix full of zeros.
     * \endcode
     *
     */
    const DerOutputType & dEval(const InputType & values) const
    {
        return this->der_ ;
    }

    /*!
     * \brief Evaluation of the uniform functions second derivative at values.
     * \param values: In case of uniform functions, these values don't matter.
     * \return a zero initialized return type.
     *     The return type is defined in the typedefs.
     *
     * Example:
     * \code{.cpp}
     *     Uniform<4, 2> func(1., 2.) ;
     *     Eigen::Vector4d values ;
     *     values << 12., 13., 14., 15. ;
     *     Eigen::Tensor<double, 3> der2(func.dEval2(values)) ;
     *     // Above call to dEval2 can also be done as follow:
     *     // Eigen::Tensor<double, 3> der2(func.dEval(12., 13.,
     *                                                           14., 15.)) ;
     *     // der2 is now an array of shape (2, 4, 4) full of zeros.
     * \endcode
     *
     */
    const Der2OutputType & dEval2(const InputType & values) const
    {
        return this->der2_ ;
    }
private:
    /*!
     * \brief Utility function to map variadic arguments values into the
     *     output type specified by the typedef.
     */
    OutputType getOutput_(std::initializer_list<DataType> values) const
    {
        OutputType tmp ;
        auto itVals(values.begin()) ;
        auto endVals(values.end()) ;
        IndexType i(0) ;
        for (;itVals!=endVals;++itVals, ++i)
        {
            tmp(i) = *itVals ;
        }
        return tmp ;
    }
} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Uniform_hpp
