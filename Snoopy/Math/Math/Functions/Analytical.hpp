#ifndef BV_Math_Functions_Analytical_hpp
#define BV_Math_Functions_Analytical_hpp

#include "Math/Functions/ABC.hpp"
#include "Math/Functions/ExpressionEvaluator.hpp"

namespace BV {
namespace Math {
namespace Functions {

/*!
 * \class Analytical Math/Functions/Analytical.hpp Functions::Analytical
 * \brief Provides the possibility to write M analytical functions that will
 *     be evaluated at runtime for the N input values.
 *     Many features are available as:
 *         - use std functions (trigonometry, pow, sqrt, ...)
 *         - use of user defined variables names
 *         - embed other analytical functions.
 *     Examples are provided in the documentation of the individual functions.
 * \tparam N, the start dimension
 * \tparam M, the end dimension
 * \tparam DataType, the type of values treated by the analytical function.
 */
template <std::size_t N, std::size_t M, typename DataType=double>
class Analytical : public ABC<N, M, DataType>
{
private:
    /*! The N variables which the expression evaluator should parse */
    std::array<std::string, N> variables_ ;
    /*! The M evaluators which parse the different analytical expressions */
    mutable std::array<ExpressionEvaluator<DataType>, M> evaluators_ ;
    /*! The M first derivatives.
     *  Each first derivative should contain N analytical expressions.
     */
    mutable std::vector<std::array<Analytical<N, N, DataType>, M> > firstDerivatives_ ;
public:

    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<N, M, DataType>::InputType InputType ;
    typedef typename ABC<N, M, DataType>::OutputType OutputType ;
    typedef typename ABC<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, M, DataType>::Der2OutputType Der2OutputType ;
    /*! The first derivative of each M expression type */
    typedef Analytical<N, N, DataType> AnalDerType ;

    Analytical(void) {} // FIXME this is only added for python wrapping to work...

    /*!
     * \brief Constructor
     * \param formulae: an array of M formulae that should be evaluated for
     *     the N variables provided. Each of the formulas can contain from
     *     0 to N of the variables, a reference to another Analytical object
     *     that will have to be referenced later on.
     * \param variables: an array of N variables names that are used in
     *     provided formulae.
     *
     * Example:
     * \code{.cpp}
     *     std::array<std::string, 2> variables {"x", "y"} ;
     *     Analytical<2, 3> func({"2*x+f(y)", "4.*y+g(x, y)", "2.*sin(pi*y)"},
     *                           variables) ;
     *     // f and g should be referenced now. Use addFunctor function.
     * \endcode
     *
     */
    Analytical(const std::array<std::string, M> & formulae,
               const std::array<std::string, N> & variables) :
        ABC<N, M, DataType>(),
        variables_(variables)
    {
        for (std::size_t i=0; i<M; ++i)
        {
            ExpressionEvaluator<DataType> evaluator(formulae[i]) ;
            for (std::size_t j=0; j<N; ++j)
            {
                evaluator.addVariable(variables[j]) ;
            }
            evaluators_[i] = evaluator ;
        }
    }

    /*!
     * \brief Constructor with the first derivatives provided.
     * \param formulae: an array of M formulae that should be evaluated for
     *     the N variables provided. Each of the formulas can contain from
     *     0 to N of the variables, a reference to another Analytical object
     *     that will have to be referenced later on.
     * \param variables: an array of N variables names that are used in
     *     provided formulae.
     * \param firstDerivatives: an array of M Analytical<N, N, DataType>
     *     functions which define the analytical first derivatives of
     *     each of the M expressions according to each of the N variables.
     *     Note that if functors are included in the expressions, their
     *     first derivative should also be included.
     *     Note that if first derivative is also provided to these first
     *     derivatives, then it provides the second derivative to this.
     *
     * Example:
     * \code{.cpp}
     *     std::array<std::string, 2> variables {"x", "y"} ;
     *     std::array<std::string, 3> formulae {"2*x+f(y)",
     *                                          "4.*y+3.*x",
     *                                          "2.*sin(pi*y)"} ;
     *     Analytical<2, 2> df1({"2.", "6."}, variables) ; // 6. is f(y) derivative
     *     Analytical<2, 2> df2({"3.", "4."}, variables) ;
     *     Analytical<2, 2> df3({"0.", "2.*pi*cos(pi*y)"}, variables) ;
     *     Analytical<2, 3> func(formulae, variables, {df1, df2, df3}) ;
     *     // f should be referenced now. Use addFunctor function.
     * \endcode
     *
     */
    Analytical(const std::array<std::string, M> & formulae,
               const std::array<std::string, N> & variables,
               std::array<Analytical<N, N, DataType>, M> firstDerivatives) :
        Analytical(formulae, variables)
    {
        firstDerivatives_.push_back(firstDerivatives) ;
    }

    /*!
     * \brief Provides the possibility to refer to another Analytical function
     *    that is used by one or more of the M expressions.
     * \param name, the name of the functor used in the expressions.
     * \param func, the functor itself. Note that this functor should generally
     *     be of type Analytical<X, 1, DataType> with X <= N.
     *     Also note that the same variable names should be used.
     *     Additional note, a functor can itself include functors.
     *
     * Example:
     * \code{.cpp}
     *     std::array<std::string, 2> variables {"x", "y"} ;
     *     std::array<std::string, 3> formulae {"2*x+f(y)",
     *                                          "4.*y+g(x, y)",
     *                                          "2.*sin(pi*y)"}
     *     std::shared_ptr<ABC<1, 1> > p_func(
     *                  std::make_shared<Analytical<1, 1> >("4.*y+10.", "y")
     *                                       ) ;
     *     std::shared_ptr<ABC<2, 1> > p_func2(
     *                  std::make_shared<Analytical<2, 1> >("y+sin(pi*x)",
     *                                                      variables)
     *                                        ) ;
     *     Analytical<2, 3> func(formulae, variables) ;
     *     func.addFunctor("f", p_func) ;
     *     func.addFunctor("g", p_func2) ;
     * \endcode
     *
     */
    template <typename F>
    void addFunctor(const std::string & name, F func)
    {
        for (std::size_t i=0; i<M; ++i)
        {
            evaluators_[i].addFunctor(name, func) ;
        }
        for (std::size_t i=0; i<firstDerivatives_.size(); ++i)
        {
            for (std::size_t j=0; j<M; ++j)
            {
                firstDerivatives_[i][j].addFunctor(name, func) ;
            }
        }
    }

    using ABC<N, M, DataType>::eval ;
    using ABC<N, M, DataType>::operator() ;
    using ABC<N, M, DataType>::dEval ;
    using ABC<N, M, DataType>::dEval2 ;

    /*!
     * \brief Function used to evaluate the analytical functions.
     * \param values, the values for which to evaluate the function.
     *     The type of values depend on the start dimension N of the function
     *     and is defined by the OutputType typedef.
     * \return the result of the evaluation.
     *
     * Each of the variables in the analytical expressions provided at
     * construction are replaced by corresponding value in values, the
     * expressions are parsed and the result returned.
     *
     * Example:
     * \code{.cpp}
     *     std::array<std::string, 2> variables {"x", "y"} ;
     *     Analytical<2, 3> func({"2*x+f(y)", "4.*y+g(x, y)", "2.*sin(pi*y)"},
     *                           variables) ;
     *     std::shared_ptr<ABC<1, 1> > p_func(
     *                  std::make_shared<Analytical<1, 1> >("4.*y+10.", "y")
     *                                       ) ;
     *     std::shared_ptr<ABC<2, 1> > p_func2(
     *                  std::make_shared<Analytical<2, 1> >("y+sin(pi*x)",
     *                                                      variables)
     *                                        ) ;
     *     func.addFunctor("f", p_func) ;
     *     func.addFunctor("g", p_func2) ;
     *     Eigen::Vector2d values ;
     *     values << 2., 3. ;
     *     Eigen::Vector3d res(func.eval(values)) ;
     *     // Note that the call to eval can also be done as follows:
     *     // Eigen::Vector3d res(func.eval(2., 3.)) ;
     *     // res now holds the following results:
     *     // res(0) = 2.*2.+4.*3.+10.
     *     // res(1) = 4.*3.+3.+sin(pi*2.)
     *     // res(2) = 2.*sin(pi*3.)
     * \endcode
     *
     */
    const OutputType & eval(const InputType & values) const
    {
        for (std::size_t i=0; i<M; ++i)
        {
            for (std::size_t j=0; j<N; ++j)
            {
                evaluators_[i].setVariable(variables_[j], values(j)) ;
            }
            this->output_(i) = evaluators_[i].evaluate() ;
        }
        return this->output_ ;
    }

    /*!
     * \brief Function used to evaluate the first derivative of the functions.
     * \param values, the values for which to evaluate the derivatives.
     *     The type of values depend on the start dimension N of the function
     *     and is defined by the OutputType typedef.
     *
     * Note that if no analytical first derivatives were provided, the
     * derivatives are approximated using a finite difference scheme.
     * This could result in a loss of precision compare to analytical
     * exact derivative.
     *
     * Example:
     * \code{.cpp}
     *     std::array<std::string, 2> variables {"x", "y"} ;
     *     std::array<std::string, 3> formulae {"2*x+f(y)",
     *                                          "4.*y+3.*x",
     *                                          "2.*sin(pi*y)"} ;
     *     Analytical<2, 2> df1({"2.", "6."}, variables) ; // 6. is f(y) derivative
     *     Analytical<2, 2> df2({"3.", "4."}, variables) ;
     *     Analytical<2, 2> df3({"0.", "2.*pi*cos(pi*y)"}, variables) ;
     *     Analytical<2, 3> func(formulae, variables, {df1, df2, df3}) ;
     *     std::shared_ptr<ABC<1, 1> > p_func(
     *                  std::make_shared<Analytical<1, 1> >("6.*y+10.", "y")
     *                                       ) ;
     *     func.addFunctor("f", p_func) ;
     *     Eigen::Matrix2d values ;
     *     values << 1., 2. ;
     *     Eigen::Matrix<double, 3, 2> der(func.dEval(values)) ;
     *     // Note that dEval can also be called using following syntax:
     *     // Eigen::Matrix<double, 3, 2> der(func.dEval(1., 2.)) ;
     *     // der now holds the following values:
     *     // der.row(0) = 2., 6.
     *     // der.row(1) = 3., 4.
     *     // der.row(2) = 0., 2.*pi*cos(pi*2.)
     * \endcode
     *
     */
    const DerOutputType & dEval(const InputType & values) const
    {
        // The result is a MxN matrix
        if (firstDerivatives_.size())
        {
            std::array<AnalDerType, M> & ders(firstDerivatives_[0]) ;
            for (std::size_t i=0; i<M; ++i)
            {
                AnalDerType & der(ders[i]) ;
                // res should contain N values ordered as follows:
                // [df1/dv1, df1/dv2, df1/dv3, ...]
                this->der_.row(i) = der.eval(values) ;
            }
            return this->der_ ;
        }
        return ABC<N, M, DataType>::dEval(values) ;
    }

    /*!
     * \brief Function used to evaluate the second derivative of the functions.
     * \param values, the values for which to evaluate the derivatives.
     *     The type of values depend on the start dimension N of the function
     *     and is defined by the OutputType typedef.
     *
     * Note that if no analytical first derivatives were provided , an exception
     * is thrown as the numeric calculation of partial second derivatives is
     * not implemented yet.
     *
     * Example:
     * \code{.cpp}
     *     std::array<std::string, 2> variables {"x", "y"} ;
     *     std::array<std::string, 3> formulae {"2*x+f(y)",
     *                                          "4.*y+3.*x",
     *                                          "2.*sin(pi*y)"} ;
     *     Analytical<2, 2> df1({"2.", "6."}, variables) ; // 6. is f(y) derivative
     *     Analytical<2, 2> df2({"3.", "4."}, variables) ;
     *     Analytical<2, 2> df3({"0.", "2.*pi*cos(pi*y)"}, variables) ;
     *     Analytical<2, 3> func(formulae, variables, {df1, df2, df3}) ;
     *     std::shared_ptr<ABC<1, 1> > p_func(
     *                  std::make_shared<Analytical<1, 1> >("6.*y+10.", "y")
     *                                       ) ;
     *     func.addFunctor("f", p_func) ;
     *     Eigen::Matrix2d values ;
     *     values << 1., 2. ;
     *     Eigen::Tensor<double, 3> der2(func.dEval2(values)) ;
     *     // Note that dEval2 can also be called using following syntax:
     *     // Eigen::Tensor<double, 3> der2(func.dEval2(1., 2.)) ;
     *     // der now holds the following values:
     *     // der.row(0) = 0., 0.
     *     // der.row(1) = 0., 0.
     *     // der.row(2) = 0., -2.*pi**2*sin(pi*2.)
     * \endcode
     *
     */
    const Der2OutputType & dEval2(const InputType & values) const
    {
        // The result is an MxNxN array
        if (firstDerivatives_.size())
        {
            std::array<AnalDerType, M> & ders(firstDerivatives_[0]) ;
            for (std::size_t i=0; i<M; ++i)
            {
                AnalDerType & der(ders[i]) ;
                Eigen::Matrix<DataType, N, N> dTmp(der.dEval(values)) ;
                this->der2_.chip(i, 0) = Eigen::TensorMap<Eigen::Tensor<DataType, 2> >(dTmp.data(), N, N) ;
            }
            return this->der2_ ;
        }
        return ABC<N, M, DataType>::dEval2(values) ;
    }

} ;


/*!
 * \brief Partial specialization for N=1 and M=1.
 *
 * This is necessary as the storage types for the evaluators differ,
 * in addition to the calculation algorithms.
 *
 */
template <typename DataType>
class Analytical<1, 1, DataType> : public ABC<1, 1, DataType>
{
private:
    std::string variable_ ;
    mutable ExpressionEvaluator<DataType> evaluator_ ;
    mutable std::vector<Analytical<1, 1, DataType> > firstDerivative_ ;
public:

    typedef typename ABC<1, 1, DataType>::InputType InputType ;
    typedef typename ABC<1, 1, DataType>::OutputType OutputType ;
    typedef typename ABC<1, 1, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<1, 1, DataType>::Der2OutputType Der2OutputType ;
    typedef Analytical<1, 1, DataType> AnalDerType ;

    Analytical(void) {} // FIXME this is only added for python wrapping to work...

    Analytical(const std::string & formula,
               const std::string & variable) :
        ABC<1, 1, DataType>(),
        variable_(variable), evaluator_(formula)
    {
        evaluator_.addVariable(variable_) ;
    }

    Analytical(const std::string & formula,
               const std::string & variable,
               Analytical<1, 1, DataType> firstDerivative) :
        Analytical(formula, variable)
    {
        firstDerivative_.push_back(firstDerivative) ;
    }

    Analytical(const std::array<std::string, 1> & formula,
               const std::array<std::string, 1> & variable) :
        Analytical(formula[0], variable[0])
    {
    }

    Analytical(const std::array<std::string, 1> & formula,
               const std::array<std::string, 1> & variable,
               std::array<Analytical<1, 1, DataType>, 1> firstDerivative) :
        Analytical(formula[0], variable[0], firstDerivative[0])
    {
    }

    template <typename F>
    void addFunctor(const std::string & name, F func)
    {
        evaluator_.addFunctor(name, func) ;
        for (std::size_t i=0; i<firstDerivative_.size(); ++i)
        {
            firstDerivative_[i].addFunctor(name, func) ;
        }
    }

    const OutputType & eval(const InputType & value) const
    {
        evaluator_.setVariable(variable_, value) ;
        this->output_ = evaluator_.evaluate() ;
        return this->output_ ;
    }

    const OutputType & operator()(const InputType & value) const
    {
        return eval(value) ;
    }

    const DerOutputType & dEval(const InputType & value) const
    {
        if (firstDerivative_.size())
        {
            AnalDerType & der(firstDerivative_[0]) ;
            this->der_ = der.eval(value) ;
            // FIXME res should contain M*N values ordered as follows:
            // [df/dv1, df/dv2, df/dv3, ...]
            return this->der_ ;
        }
        return ABC<1, 1, DataType>::dEval(value) ;
    }

    const Der2OutputType & dEval2(const InputType & value) const
    {
        if (firstDerivative_.size())
        {
            AnalDerType & der(firstDerivative_[0]) ;
            this->der2_ = der.dEval(value) ;
            // FIXME res should contain (N, N) values
            return this->der2_ ;
        }
        return ABC<1, 1, DataType>::dEval2(value) ;
    }

} ;

/*!
 * \brief Partial specialization for M=1.
 *
 * This is necessary as the storage types for the evaluators differ,
 * in addition to the calculation algorithms.
 *
 */
template <std::size_t N, typename DataType>
class Analytical<N, 1, DataType> : public ABC<N, 1, DataType>
{
private:
    std::array<std::string, N> variables_ ;
    mutable ExpressionEvaluator<DataType> evaluator_ ;
    mutable std::vector<Analytical<N, N, DataType> > firstDerivatives_ ;
public:

    typedef typename ABC<N, 1, DataType>::InputType InputType ;
    typedef typename ABC<N, 1, DataType>::OutputType OutputType ;
    typedef typename ABC<N, 1, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, 1, DataType>::Der2OutputType Der2OutputType ;
    typedef Analytical<N, N, DataType> AnalDerType ;

    Analytical(void) {} // FIXME this is only added for python wrapping to work...

    Analytical(const std::string & formula,
               const std::array<std::string, N> & variables) :
        ABC<N, 1, DataType>(),
        variables_(variables), evaluator_(formula)
    {
        for (std::size_t j=0; j<N; ++j)
        {
            evaluator_.addVariable(variables[j]) ;
        }
    }

    Analytical(const std::string & formula,
               const std::array<std::string, N> & variables,
               Analytical<N, N, DataType> firstDerivatives) :
        Analytical(formula, variables)
    {
        firstDerivatives_.push_back(firstDerivatives) ;
    }

    Analytical(const std::array<std::string, 1> & formula,
               const std::array<std::string, N> & variables) :
        Analytical(formula[0], variables)
    {
    }

    Analytical(const std::array<std::string, 1> & formula,
               const std::array<std::string, N> & variables,
               std::array<Analytical<N, N, DataType>, 1> firstDerivatives) :
        Analytical(formula[0], variables, firstDerivatives[0])
    {
    }

    template <typename F>
    void addFunctor(const std::string & name, F func)
    {
        evaluator_.addFunctor(name, func) ;
        std::size_t n(firstDerivatives_.size()) ;
        for (std::size_t i=0; i<n; ++i)
        {
            firstDerivatives_[i].addFunctor(name, func) ;
        }
    }

    using ABC<N, 1, DataType>::eval ;
    using ABC<N, 1, DataType>::operator() ;
    using ABC<N, 1, DataType>::dEval ;
    using ABC<N, 1, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        for (std::size_t j=0; j<N; ++j)
        {
            evaluator_.setVariable(variables_[j], values(j)) ;
        }
        this->output_ = evaluator_.evaluate() ;
        return this->output_ ;
    }

    const DerOutputType & dEval(const InputType & values) const
    {
        if (firstDerivatives_.size())
        {
            AnalDerType & der(firstDerivatives_[0]) ;
            this->der_ = der.eval(values) ;
            // FIXME res should contain M*N values ordered as follows:
            // [df/dv1, df/dv2, df/dv3, ...]
            return this->der_ ;
        }
        return ABC<N, 1, DataType>::dEval(values) ;
    }

    const Der2OutputType & dEval2(const InputType & values) const
    {
        if (firstDerivatives_.size())
        {
            AnalDerType & der(firstDerivatives_[0]) ;
            this->der2_ = der.dEval(values) ;
            // FIXME res should contain (N, N) values
            return this->der2_ ;
        }
        return ABC<N, 1, DataType>::dEval2(values) ;
    }

} ;

/*!
 * \brief Partial specialization for N=1.
 *
 * This is necessary as the storage types for the evaluators differ,
 * in addition to the calculation algorithms.
 *
 */
template <std::size_t M, typename DataType>
class Analytical<1, M, DataType> : public ABC<1, M, DataType>
{
private:
    std::string variable_ ;
    mutable std::array<ExpressionEvaluator<DataType>, M> evaluators_ ;
    mutable std::vector<std::array<Analytical<1, 1, DataType>, M> > firstDerivatives_ ;
public:
    //enum {startDimension = N, endDimension = M} ;

    typedef typename ABC<1, M, DataType>::InputType InputType ;
    typedef typename ABC<1, M, DataType>::OutputType OutputType ;
    typedef typename ABC<1, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<1, M, DataType>::Der2OutputType Der2OutputType ;
    typedef Analytical<1, 1, DataType> AnalDerType ;

    Analytical(void) {} // FIXME this is only added for python wrapping to work...

    Analytical(const std::array<std::string, M> & formulas,
               const std::string & variable) :
        ABC<1, M, DataType>(),
        variable_(variable)
    {
        for (std::size_t i=0; i<M; ++i)
        {
            ExpressionEvaluator<DataType> evaluator(formulas[i]) ;
            evaluator.addVariable(variable) ;
            evaluators_[i] = evaluator ;
        }
    }

    Analytical(const std::array<std::string, M> & formulas,
               const std::array<std::string, 1> & variables) :
        Analytical(formulas, variables[0])
    {
    }

    Analytical(const std::array<std::string, M> & formulas,
               const std::string & variable,
               std::array<Analytical<1, 1, DataType>, M> firstDerivatives) :
        Analytical(formulas, variable)
    {
        firstDerivatives_.push_back(firstDerivatives) ;
    }

    Analytical(const std::array<std::string, M> & formulas,
               const std::array<std::string, 1> & variables,
               std::array<Analytical<1, 1, DataType>, M> firstDerivatives) :
        Analytical(formulas, variables[0], firstDerivatives)
    {
    }

    template <typename F>
    void addFunctor(const std::string & name, F func)
    {
        for (std::size_t i=0; i<M; ++i)
        {
            evaluators_[i].addFunctor(name, func) ;
        }
        std::size_t n(firstDerivatives_.size()) ;
        for (std::size_t i=0; i<n; ++i)
        {
            for (std::size_t j=0; j<M; ++j)
            {
                firstDerivatives_[i][j].addFunctor(name, func) ;
            }
        }
    }

    using ABC<1, M, DataType>::eval ;
    using ABC<1, M, DataType>::operator() ;
    using ABC<1, M, DataType>::dEval ;
    using ABC<1, M, DataType>::dEval2 ;

    const OutputType & eval(const InputType & value) const
    {
        for (std::size_t i=0; i<M; ++i)
        {
            evaluators_[i].setVariable(variable_, value) ;
            this->output_(i) = evaluators_[i].evaluate() ;
        }
        return this->output_ ;
    }

    const DerOutputType & dEval(const InputType & value) const
    {
        if (firstDerivatives_.size())
        {
            std::array<AnalDerType, M> & ders(firstDerivatives_[0]) ;
            for (std::size_t i=0; i<M; ++i)
            {
                AnalDerType & der(ders[i]) ;
                this->der_(i) = der.eval(value) ;
            }
            return this->der_ ;
        }
        return ABC<1, M, DataType>::dEval(value) ;
    }

    const Der2OutputType & dEval2(const InputType & value) const
    {
        if (firstDerivatives_.size())
        {
            std::array<AnalDerType, M> & ders(firstDerivatives_[0]) ;
            for (std::size_t i=0; i<M; ++i)
            {
                AnalDerType & der(ders[i]) ;
                this->der2_(i) = der.dEval(value) ;
            }
            return this->der2_ ;
        }
        return ABC<1, M, DataType>::dEval2(value) ;
    }

} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Analytical_hpp
