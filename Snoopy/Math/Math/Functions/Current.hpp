#pragma once

#include "Math/Functions/ABC.hpp"


namespace BV {
namespace Math {
namespace Functions {
/*!
 * \class Current Math/Functions/Current.hpp Functions::Current
 * \brief Class that evaluates tide and wind generated currents velocity.
 * V_{c, tide}(z) = V_{c, tide}(0)*((d+z)/d)^alpha  for z <= 0
 * 
 */
template <typename DataType=double>
class Current : public ABC<4, 3, DataType>
{
private:
    double seabedTideCurrent_ ;
    double deltaTideCurrent_ ;
    double zSeabed_ ;
    double invPowerLawExponent_ ;
    double surfaceWindInducedCurrent_ ;
    double windInfluenceWaterdepthLimit_ ;
    double tideCurrentHeading_ ;
    double windInducedCurrentHeading_ ;

public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<4, 3, DataType>::InputType InputType ;
    typedef typename ABC<4, 3, DataType>::OutputType OutputType ;
    typedef typename ABC<4, 3, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<4, 3, DataType>::Der2OutputType Der2OutputType ;
    

    /*!
     * \brief Constructor
     * \param : 
     */
    Current(double surfaceTideCurrent, double seabedTideCurrent, double tideCurrentHeading,
            double waterdepth, double powerLawExponent, double surfaceWindInducedCurrent,
            double windInducedCurrentHeading, double windInfluenceWaterdepthLimit) :
        ABC<4, 3, DataType>(),
        seabedTideCurrent_(seabedTideCurrent),
        deltaTideCurrent_(surfaceTideCurrent - seabedTideCurrent),
        zSeabed_(-std::abs(waterdepth)),
        invPowerLawExponent_(1./powerLawExponent),
        surfaceWindInducedCurrent_(surfaceWindInducedCurrent),
        windInfluenceWaterdepthLimit_(-std::abs(windInfluenceWaterdepthLimit)),
        tideCurrentHeading_(tideCurrentHeading),
        windInducedCurrentHeading_(windInducedCurrentHeading)
    {
    }

    using ABC<4, 3, DataType>::eval ;
    using ABC<4, 3, DataType>::operator() ;
    using ABC<4, 3, DataType>::dEval ;
    using ABC<4, 3, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        // values: t, x, y, z
        const double z(values(3)) ;
        if (z > 0)
        {
            this->output_(0) = 0. ;
            this->output_(1) = 0. ;
            return this->output_ ;
        }
        double VcT = seabedTideCurrent_ ;
        if (z > zSeabed_)
        {
            const double K = (zSeabed_ - z) / zSeabed_ ;
            VcT += deltaTideCurrent_*std::pow(K, invPowerLawExponent_) ;
        }
        this->output_(0) = VcT * cos(tideCurrentHeading_) ;
        this->output_(1) = VcT * sin(tideCurrentHeading_) ;
        if (z > windInfluenceWaterdepthLimit_)
        {
            double VcC = surfaceWindInducedCurrent_ * (windInfluenceWaterdepthLimit_ - z) / windInfluenceWaterdepthLimit_ ;
            this->output_(0) += VcC * cos(windInducedCurrentHeading_) ;
            this->output_(1) += VcC * sin(windInducedCurrentHeading_) ;
        }
        return this->output_ ;
    }

    const OutputType & operator()(const InputType & value) const
    {
        return eval(value) ;
    }

    const DerOutputType & dEval(const InputType & value) const
    {
        return ABC<4, 3, DataType>::dEval(value) ;
    }

    const Der2OutputType & dEval2(const InputType & value) const
    {
        return ABC<4, 3, DataType>::dEval2(value) ;
    }
} ;
}
}
}
