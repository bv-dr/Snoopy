#ifndef BV_Math_Functions_Details_hpp
#define BV_Math_Functions_Details_hpp

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <initializer_list>
#include <type_traits>

namespace BV {
namespace Math {
namespace Functions {

namespace Details {

template<bool...>
struct BoolPack ;

template<bool... bs>
using AllTrue = std::is_same<BoolPack<bs..., true>, BoolPack<true, bs...> > ;

template<class R, class... Ts>
using AreAllConvertible = AllTrue<std::is_convertible<Ts, R>::value...> ;

/*!
 * \brief Traits class used to deduce I/O types used by functions.
 */
template <std::size_t N, std::size_t M, typename DataType>
struct Traits
{
    typedef Eigen::Matrix<DataType, N, 1> InputType ;
    typedef typename Eigen::Matrix<DataType, N, 1>::Index IndexType ;
    typedef Eigen::Matrix<DataType, M, 1> OutputType ;
    typedef Eigen::Matrix<DataType, M, N> DerOutputType ;
    typedef Eigen::Tensor<DataType, 3> Der2OutputType ;

    template <typename Derived>
    static void setInput(InputType & input,
                         const Eigen::MatrixBase<Derived> & other)
    {
        input = other ;
    }

    static InputType getInputTypeConstant(const DataType & val)
    {
        return InputType::Constant(val) ;
    }

    static InputType initInput(void)
    {
        return InputType::Zero() ;
    }

    static void multiplyInputAtIndex(InputType & input,
                                     IndexType i,
                                     const DataType & factor)
    {
        input(i) *= factor ;
    }

    static OutputType initOutput(void)
    {
        return OutputType::Zero() ;
    }

    static DerOutputType initDer(void)
    {
        return DerOutputType::Zero() ;
    }

    static Der2OutputType initDer2(void)
    {
        return Der2OutputType(static_cast<IndexType>(M),
        		              static_cast<IndexType>(N),
							  static_cast<IndexType>(N)) ;
    }

    static OutputType getOutputTypeZero(void)
    {
        return OutputType::Zero() ;
    }

    static void setDerZero(DerOutputType & der)
    {
        der = DerOutputType::Zero() ;
    }

    static void setDer2Zero(Der2OutputType & der2)
    {
        der2.setZero() ;
    }
} ;

/*!
 * \brief Traits class specialization for M=1
 */
template <std::size_t N, typename DataType>
struct Traits<N, 1, DataType>
{
    typedef Eigen::Matrix<DataType, N, 1> InputType ;
    typedef typename Eigen::Matrix<DataType, N, 1>::Index IndexType ;
    typedef DataType OutputType ;
    typedef Eigen::Matrix<DataType, N, 1> DerOutputType ;
    typedef Eigen::Matrix<DataType, N, N> Der2OutputType ;

    template <typename Derived>
    static void setInput(InputType & input,
                         const Eigen::MatrixBase<Derived> & other)
    {
        input = other ;
    }

    static InputType getInputTypeConstant(const DataType & val)
    {
        return InputType::Constant(val) ;
    }

    static InputType initInput(void)
    {
        return InputType::Zero() ;
    }

    static void multiplyInputAtIndex(InputType & input,
                                     IndexType i,
                                     const DataType & factor)
    {
        input(i) *= factor ;
    }

    static OutputType initOutput(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static DerOutputType initDer(void)
    {
        return DerOutputType::Zero() ;
    }

    static Der2OutputType initDer2(void)
    {
        return Der2OutputType::Zero() ;
    }

    static OutputType getOutputTypeZero(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static void setDerZero(DerOutputType & der)
    {
        der = DerOutputType::Zero() ;
    }

    static void setDer2Zero(Der2OutputType & der2)
    {
        der2 = Der2OutputType::Zero() ;
    }
} ;

/*!
 * \brief Traits class specialization for N=1
 */
template <std::size_t M, typename DataType>
struct Traits<1, M, DataType>
{
    typedef DataType InputType ;
    typedef typename Eigen::Matrix<DataType, M, 1>::Index IndexType ;
    typedef Eigen::Matrix<DataType, M, 1> OutputType ;
    typedef Eigen::Matrix<DataType, M, 1> DerOutputType ;
    typedef Eigen::Matrix<DataType, M, 1> Der2OutputType ;

    template <typename Derived>
    static void setInput(InputType & input,
                         const Eigen::MatrixBase<Derived> & other)
    {
        input = other(0) ;
    }

    static InputType getInputTypeConstant(const DataType & val)
    {
        return val ;
    }

    static InputType initInput(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static void multiplyInputAtIndex(InputType & input,
                                     IndexType i,
                                     const DataType & factor)
    {
        input *= factor ;
    }

    static OutputType initOutput(void)
    {
        return OutputType::Zero() ;
    }

    static DerOutputType initDer(void)
    {
        return DerOutputType::Zero() ;
    }

    static Der2OutputType initDer2(void)
    {
        return Der2OutputType::Zero() ;
    }

    static OutputType getOutputTypeZero(void)
    {
        return OutputType::Zero() ;
    }

    static void setDerZero(DerOutputType & der)
    {
        der = DerOutputType::Zero() ;
    }

    static void setDer2Zero(Der2OutputType & der2)
    {
        der2 = Der2OutputType::Zero() ;
    }
} ;

/*!
 * \brief Traits class specialization for N=1 and M=1
 */
template <typename DataType>
struct Traits<1, 1, DataType>
{
    typedef DataType InputType ;
    typedef unsigned IndexType ;
    typedef DataType OutputType ;
    typedef DataType DerOutputType ;
    typedef DataType Der2OutputType ;

    template <typename Derived>
    static void setInput(InputType & input,
                         const Eigen::MatrixBase<Derived> & other)
    {
        input = other(0) ;
    }

    static InputType getInputTypeConstant(const DataType & val)
    {
        return val ;
    }

    static InputType initInput(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static void multiplyInputAtIndex(InputType & input,
                                     IndexType i,
                                     const DataType & factor)
    {
        input *= factor ;
    }

    static OutputType initOutput(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static DerOutputType initDer(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static Der2OutputType initDer2(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static OutputType getOutputTypeZero(void)
    {
        return static_cast<DataType>(0.) ;
    }

    static void setDerZero(DerOutputType & der)
    {
        der = static_cast<DataType>(0.) ;
    }

    static void setDer2Zero(Der2OutputType & der2)
    {
        der2 = static_cast<DataType>(0.) ;
    }
} ;

} // End of namespace Details

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Details_hpp
