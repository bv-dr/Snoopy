#ifndef BV_Math_Functions_ABC_hpp
#define BV_Math_Functions_ABC_hpp

#include "Math/FiniteDifference/FiniteDifference.hpp"
#include "Math/Functions/Details.hpp"

namespace BV {
namespace Math {
namespace Functions {

/*!
 * \class ABC Math/Functions/ABC.hpp Functions::ABC
 * \brief Abstract base class defining the interface for all functions objects.
 * \tparam N, the start field dimension
 * \tparam M, the end field dimension
 * \tparam DataType, the type of data treated by the function object.
 *     Defaults to double.
 */
template <std::size_t N, std::size_t M, typename DataType_=double>
class ABC
{
public:
    /*!
     * \brief Useful enum for providing the start and end dimensions.
     */
    enum {startDimension = N, endDimension = M} ;

    /*!
     * \brief Useful typedefs to know which types are input/output by the functions.
     */
    typedef DataType_ DataType ;
    typedef typename Details::Traits<N, M, DataType>::IndexType IndexType ;
    typedef typename Details::Traits<N, M, DataType>::InputType InputType ;
    typedef typename Details::Traits<N, M, DataType>::OutputType OutputType ;
    typedef typename Details::Traits<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename Details::Traits<N, M, DataType>::Der2OutputType Der2OutputType ;

    typedef FiniteDifference::FiniteDifference<
                          FiniteDifference::FDScheme::CENTRAL,
                          FiniteDifference::IthDerivative::FIRST,
                          2,
                          FiniteDifference::FDContext::FIXED_SIZE_IO
                                              > FDFirstDer ;
    typedef FiniteDifference::FiniteDifference<
                          FiniteDifference::FDScheme::CENTRAL,
                          FiniteDifference::IthDerivative::SECOND,
                          2,
                          FiniteDifference::FDContext::FIXED_SIZE_IO
                                              > FDSecondDer ;
    /*!
     * \brief Constructor.
     *     Initializes the output types.
     */
    ABC(void) :
        input_(Details::Traits<N, M, DataType>::initInput()),
        output_(Details::Traits<N, M, DataType>::initOutput()),
        der_(Details::Traits<N, M, DataType>::initDer()),
        der2_(Details::Traits<N, M, DataType>::initDer2())
    {
    }

    /*!
     * \brief Virtual destructor. Does nothing.
     */
    virtual ~ABC(void)
    {
    }

    constexpr std::size_t getStartDimension(void) const
    {
        return N ;
    }

    constexpr std::size_t getEndDimension(void) const
    {
        return M ;
    }

    /*!
     * \brief Function used to evaluate the function object.
     *     This function must be overloaded by the derived classes.
     * \param values, the values for which to evaluate the function.
     *     The type of values depend on the start dimension N of the function.
     *
     */
    virtual const OutputType & eval(const InputType & values) const = 0 ;

    /*!
     * \brief Function used to evaluate the first derivative of the function.
     *     This function has a default implementation which performs
     *     a central finite difference of order 2.
     *     Derived classes can overload this if there is a better implementation.
     * \param values, the values for which to evaluate the function.
     *     The type of values depend on the start dimension N of the function.
     */
    virtual const DerOutputType & dEval(const InputType & values) const
    {
        this->der_ = FDFirstDer::get(
                    values, *this,
                    Details::Traits<N, M, DataType>::getInputTypeConstant(1.e-6)
                                    ) ;
        return this->der_ ;
    }

    /*!
     * \brief Function used to evaluate the second derivative of the function.
     *     This function has a default implementation which performs
     *     a central finite difference of order 2.
     *     Derived classes can overload this if there is a better implementation.
     * \param values, the values for which to evaluate the function.
     *     The type of values depend on the start dimension N of the function.
     */
    virtual const Der2OutputType & dEval2(const InputType & values) const
    {
        this->der2_ = FDSecondDer::get(
                    values, *this,
                    Details::Traits<N, M, DataType>::getInputTypeConstant(1.e-4)
                                     ) ;
        return this->der2_ ;
    }

    /*!
     * \brief Function used to evaluate the function object.
     *     This functions redirects to pure virtual function eval after
     *     formatting the arguments provided.
     * \param values, the values for which to evaluate the function.
     *     The number of provided values should be N, the start dimension
     *     of the function.
     * \return the result of the function evaluation at values.
     *     The return type is OutputType, typedef'ed above
     */
    template <typename ...DataTypes,
              typename = typename std::enable_if<sizeof...(DataTypes)==N
                                                 && N!=1
                                                 && Details::AreAllConvertible<DataType, DataTypes...>::value>::type>
    const OutputType & eval(DataTypes... values) const
    {
        //static_assert(sizeof...(values) == N, "Wrong number of values provided") ;
        setInput_({values...}) ;
        return eval(input_) ;
    }

    template <typename Derived>
    const OutputType & eval(const Eigen::MatrixBase<Derived> & input) const
    {
        // We copy the eigen like type into a correct input type
        Details::Traits<N, M, DataType>::setInput(input_, input) ;
        return eval(input_) ;
    }

    /*!
     * \brief Function used to evaluate the function object.
     *     This functions redirects to pure virtual function eval.
     * \param values, the values for which to evaluate the function.
     *     InputType is defined as a function typedef.
     * \return the result of the function evaluation at values.
     *     The return type is OutputType, typedef'ed above
     */
    const OutputType & operator()(const InputType & values) const
    {
        return eval(values) ;
    }

    /*!
     * \brief Function used to evaluate the first derivative of the function.
     *     This functions redirects to pure virtual function dEval after
     *     formatting the arguments provided.
     * \param values, the values for which to evaluate the function.
     *     The number of provided values should be N, the start dimension
     *     of the function.
     * \return the first derivative of the function of type DerOutputType.
     */
    template <typename ...DataTypes,
              typename = typename std::enable_if<sizeof...(DataTypes)==N
                                                 && N!=1
                                                 && Details::AreAllConvertible<DataType, DataTypes...>::value>::type>
    const DerOutputType & dEval(DataTypes... values) const
    {
        //static_assert(sizeof...(values) == N, "Wrong number of values provided") ;
        setInput_({values...}) ;
        return dEval(input_) ;
    }

    template <typename Derived>
    const DerOutputType & dEval(const Eigen::MatrixBase<Derived> & input) const
    {
        // We copy the eigen like type into a correct input type
        Details::Traits<N, M, DataType>::setInput(input_, input) ;
        return dEval(input_) ;
    }

    /*!
     * \brief Function used to evaluate the second derivative of the function.
     *     This functions redirects to pure virtual function dEval2 after
     *     formatting the arguments provided.
     * \param values, the values for which to evaluate the function.
     *     The number of provided values should be N, the start dimension
     *     of the function.
     * \return the second derivative of the function of type Der2OutputType.
     */
    template <typename ...DataTypes,
              typename = typename std::enable_if<sizeof...(DataTypes)==N
                                                 && N!=1
                                                 && Details::AreAllConvertible<DataType, DataTypes...>::value>::type>
    const Der2OutputType & dEval2(DataTypes... values) const
    {
        //static_assert(sizeof...(values) == N, "Wrong number of values provided") ;
        setInput_({values...}) ;
        return dEval2(input_) ;
    }

    template <typename Derived>
    const Der2OutputType & dEval2(const Eigen::MatrixBase<Derived> & input) const
    {
        // We copy the eigen like type into a correct input type
        Details::Traits<N, M, DataType>::setInput(input_, input) ;
        return dEval2(input_) ;
    }

protected:
    mutable InputType input_ ; /*!< input values used as a temporary container */
    mutable OutputType output_ ; /*!< output values used as a temporary container */
    mutable DerOutputType der_ ; /*!< derivatives values used as a temporary container */
    mutable Der2OutputType der2_ ; /*!< derivatives values used as a temporary container */

    /*!
     * \brief function used to convert into InputType variadic arguments.
     * \param values, the input values of size N that should be converted
     *     into InputType.
     *     input_ temporary container is used to store the values.
     */
    // Don't remove the SFINAE below: it is used to circumvent an MSVC bug
    template <typename Dummy = void,
              typename = typename std::enable_if<N!=1, Dummy>::type>
    void setInput_(std::initializer_list<DataType> values) const
    {
        auto itVals(values.begin()) ;
        auto endVals(values.end()) ;
        IndexType i(0) ;
        for (;itVals!=endVals;++itVals, ++i)
        {
            input_(i) = *itVals ;
        }
    }

} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_ABC_hpp
