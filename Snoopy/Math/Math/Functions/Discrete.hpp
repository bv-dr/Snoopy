#ifndef BV_Math_Functions_Discrete_hpp
#define BV_Math_Functions_Discrete_hpp

#include <Eigen/Dense>

#include "Math/Functions/ABC.hpp"
#include "Math/Interpolators/Interpolators.hpp"

namespace BV {
namespace Math {
namespace Functions {

template <std::size_t N, std::size_t M, int InterpScheme,
          typename DataType=double,
          typename DiscreteAxisType=Eigen::VectorXd,
          typename DiscreteDataType=Eigen::VectorXd>
class Discrete ;

template <std::size_t M, int InterpScheme, typename DataType,
          typename DiscreteAxisType, typename DiscreteDataType>
class Discrete<1, M, InterpScheme, DataType, DiscreteAxisType, DiscreteDataType> :
    public ABC<1, M, DataType>
{
private:
    std::array<DiscreteAxisType, M> axes_ ;
    std::array<DiscreteDataType, M> data_ ;
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<1, M, DataType>::InputType InputType ;
    typedef typename ABC<1, M, DataType>::OutputType OutputType ;
    typedef typename ABC<1, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<1, M, DataType>::Der2OutputType Der2OutputType ;

    Discrete(const std::array<DiscreteAxisType, M> & axes,
             const std::array<DiscreteDataType, M> & data) :
        ABC<1, M, DataType>(), axes_(axes), data_(data)
    {
    }

    using ABC<1, M, DataType>::eval ;
    using ABC<1, M, DataType>::operator() ;
    using ABC<1, M, DataType>::dEval ;
    using ABC<1, M, DataType>::dEval2 ;

    const OutputType & eval(const InputType & value) const
    {
        for (std::size_t i=0; i<M; ++i)
        {
            this->output_(i) =
                    Interpolators::Interpolator<1, InterpScheme>::get(axes_[i],
                                                                      data_[i],
                                                                      value) ;
        }
        return this->output_ ;
    }

} ;

template <int InterpScheme, typename DataType, typename DiscreteAxisType,
          typename DiscreteDataType>
class Discrete<1, 1, InterpScheme, DataType, DiscreteAxisType, DiscreteDataType> :
    public ABC<1, 1, DataType>
{
private:
    DiscreteAxisType axis_ ;
    DiscreteDataType data_ ;
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<1, 1, DataType>::InputType InputType ;
    typedef typename ABC<1, 1, DataType>::OutputType OutputType ;
    typedef typename ABC<1, 1, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<1, 1, DataType>::Der2OutputType Der2OutputType ;

    Discrete(const DiscreteAxisType & axis, const DiscreteDataType & data) :
        ABC<1, 1, DataType>(), axis_(axis), data_(data)
    {
    }

    using ABC<1, 1, DataType>::eval ;
    using ABC<1, 1, DataType>::operator() ;
    using ABC<1, 1, DataType>::dEval ;
    using ABC<1, 1, DataType>::dEval2 ;

    const OutputType & eval(const InputType & value) const
    {
        this->output_ = Interpolators::Interpolator<1, InterpScheme>::get(
                                                            axis_, data_, value
                                                                         ) ;
        return this->output_ ;
    }

    void setAxis(const Eigen::Index & i, const DataType & val)
    {
        axis_(i) = val ;
    }

} ;

template <std::size_t M, int InterpScheme, typename DataType,
          typename DiscreteAxisType, typename DiscreteDataType>
class Discrete<2, M, InterpScheme, DataType, DiscreteAxisType, DiscreteDataType> :
    public ABC<2, M, DataType>
{
private:
    std::array<DiscreteAxisType, M> axes1_ ;
    std::array<DiscreteAxisType, M> axes2_ ;
    std::array<DiscreteDataType, M> data_ ;
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<2, M, DataType>::InputType InputType ;
    typedef typename ABC<2, M, DataType>::OutputType OutputType ;
    typedef typename ABC<2, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<2, M, DataType>::Der2OutputType Der2OutputType ;

    Discrete(const std::array<DiscreteAxisType, M> & axes1,
             const std::array<DiscreteAxisType, M> & axes2,
             const std::array<DiscreteDataType, M> & data) :
        ABC<2, M, DataType>(),
        axes1_(axes1), axes2_(axes2), data_(data)
    {
    }

    using ABC<2, M, DataType>::eval ;
    using ABC<2, M, DataType>::operator() ;
    using ABC<2, M, DataType>::dEval ;
    using ABC<2, M, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        for (std::size_t i=0; i<M; ++i)
        {
            this->output_(i) =
                  Interpolators::Interpolator<2, InterpScheme>::get(axes1_[i],
                                                                    axes2_[i],
                                                                    data_[i],
                                                                    values(0),
                                                                    values(1)) ;
        }
        return this->output_ ;
    }

} ;

template <int InterpScheme, typename DataType, typename DiscreteAxisType,
          typename DiscreteDataType>
class Discrete<2, 1, InterpScheme, DataType, DiscreteAxisType, DiscreteDataType> :
    public ABC<2, 1, DataType>
{
private:
    DiscreteAxisType axis1_ ;
    DiscreteAxisType axis2_ ;
    DiscreteDataType data_ ;
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<2, 1, DataType>::InputType InputType ;
    typedef typename ABC<2, 1, DataType>::OutputType OutputType ;
    typedef typename ABC<2, 1, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<2, 1, DataType>::Der2OutputType Der2OutputType ;

    Discrete(const DiscreteAxisType & axis1, const DiscreteAxisType & axis2,
             const DiscreteDataType & data) :
        ABC<2, 1, DataType>(), axis1_(axis1), axis2_(axis2), data_(data)
    {
    }

    using ABC<2, 1, DataType>::eval ;
    using ABC<2, 1, DataType>::operator() ;
    using ABC<2, 1, DataType>::dEval ;
    using ABC<2, 1, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        this->output_ = Interpolators::Interpolator<2, InterpScheme>::get(
                                                         axis1_, axis2_, data_,
                                                         values(0), values(1)
                                                                         ) ;
        return this->output_ ;
    }
} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Discrete_hpp
