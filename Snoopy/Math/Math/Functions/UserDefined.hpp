#ifndef BV_Math_Functions_UserDefined_hpp
#define BV_Math_Functions_UserDefined_hpp

#include <functional>
#include <vector>

#include "Math/Functions/ABC.hpp"

namespace BV {
namespace Math {
namespace Functions {

namespace Details {

template <int N, typename InputType, typename OutputType, typename DataType=double>
class FunctorWrapper ;

template <typename InputType, typename OutputType, typename DataType>
class FunctorWrapper<2, InputType, OutputType, DataType>
{
    std::function<OutputType(const DataType &, const DataType &)> functor_ ;

public:
    FunctorWrapper(std::function<OutputType(const DataType &,
                                            const DataType &)> functor) :
        functor_(functor)
    {
    }

    OutputType operator()(const InputType & vals)
    {
        return functor_(vals(0), vals(1)) ;
    }
} ;

template <typename InputType, typename OutputType, typename DataType>
class FunctorWrapper<3, InputType, OutputType, DataType>
{
    std::function<OutputType(const DataType &, const DataType &,
                             const DataType &)> functor_ ;

public:
    FunctorWrapper(std::function<OutputType(const DataType &,
                                            const DataType &,
                                            const DataType &)> functor) :
        functor_(functor)
    {
    }

    OutputType operator()(const InputType & vals)
    {
        return functor_(vals(0), vals(1), vals(2)) ;
    }
} ;

template <typename InputType, typename OutputType, typename DataType>
class FunctorWrapper<4, InputType, OutputType, DataType>
{
    std::function<OutputType(const DataType &, const DataType &,
                             const DataType &, const DataType &)> functor_ ;

public:
    FunctorWrapper(std::function<OutputType(const DataType &,
                                            const DataType &,
                                            const DataType &,
                                            const DataType &)> functor) :
        functor_(functor)
    {
    }

    OutputType operator()(const InputType & vals)
    {
        return functor_(vals(0), vals(1), vals(2), vals(3)) ;
    }
} ;

template <typename InputType, typename OutputType, typename DataType>
class FunctorWrapper<5, InputType, OutputType, DataType>
{
    std::function<OutputType(const DataType &, const DataType &,
                             const DataType &, const DataType &,
                             const DataType &)> functor_ ;

public:
    FunctorWrapper(std::function<OutputType(const DataType &,
                                            const DataType &,
                                            const DataType &,
                                            const DataType &,
                                            const DataType &)> functor) :
        functor_(functor)
    {
    }

    OutputType operator()(const InputType & vals)
    {
        return functor_(vals(0), vals(1), vals(2), vals(3), vals(4)) ;
    }
} ;

} // End of namespace Details

template <std::size_t N, std::size_t M, typename DataType=double,
          bool unusedForWrap=true> // only used for python wrapping
class UserDefined : public ABC<N, M, DataType>
{
public:
    /*!
     * Useful typedefs used to get the input/output types.
     */
    typedef typename ABC<N, M, DataType>::InputType InputType ;
    typedef typename ABC<N, M, DataType>::OutputType OutputType ;
    typedef typename ABC<N, M, DataType>::DerOutputType DerOutputType ;
    typedef typename ABC<N, M, DataType>::Der2OutputType Der2OutputType ;
    
    UserDefined(std::function<OutputType(const InputType &)> functor) :
        ABC<N, M, DataType>(),
        functor_(functor)
    {
    }

    UserDefined(std::function<OutputType(const InputType &)> functor,
                std::function<DerOutputType(const InputType &)> dFunctor) :
        UserDefined(functor)
    {
        dFunctor_.push_back(dFunctor) ;
    }

    UserDefined(std::function<OutputType(const InputType &)> functor,
                std::function<DerOutputType(const InputType &)> dFunctor,
                std::function<Der2OutputType(const InputType &)> d2Functor) :
        UserDefined(functor, dFunctor)
    {
        d2Functor_.push_back(d2Functor) ;
    }

    template<std::size_t D=N, typename std::enable_if<D == 2, void>::type* = nullptr>
    UserDefined(std::function<OutputType(const DataType & , const DataType &)> functor) :
        UserDefined(Details::FunctorWrapper<N, InputType, OutputType, DataType>(functor))
    {
    }

    template<std::size_t D=N, typename std::enable_if<D == 3, void>::type* = nullptr>
    UserDefined(std::function<OutputType(const DataType &, const DataType & ,
                                         const DataType &)> functor) :
        UserDefined(Details::FunctorWrapper<N, InputType, OutputType, DataType>(functor))
    {
    }

    template<std::size_t D=N, typename std::enable_if<D == 4, void>::type* = nullptr>
    UserDefined(std::function<OutputType(const DataType &,const DataType &,
                                         const DataType & , const DataType &)> functor) :
        UserDefined(Details::FunctorWrapper<N, InputType, OutputType, DataType>(functor))
    {
    }

    template<std::size_t D=N, typename std::enable_if<D == 5, void>::type* = nullptr>
    UserDefined(std::function<OutputType(const DataType &,const DataType &,
                                         const DataType & , const DataType &,
                                         const DataType &)> functor) :
        UserDefined(Details::FunctorWrapper<N, InputType, OutputType, DataType>(functor))
    {
    }

    using ABC<N, M, DataType>::eval ;
    using ABC<N, M, DataType>::operator() ;
    using ABC<N, M, DataType>::dEval ;
    using ABC<N, M, DataType>::dEval2 ;

    const OutputType & eval(const InputType & values) const
    {
        this->output_ = functor_(values) ;
        return this->output_ ;
    }

    const DerOutputType & dEval(const InputType & values) const
    {
        if (dFunctor_.size())
        {
            this->der_ = dFunctor_[0](values) ;
            return this->der_ ;
        }
        return ABC<N, M, DataType>::dEval(values) ;
    }

    const Der2OutputType & dEval2(const InputType & values) const
    {
        if (d2Functor_.size())
        {
            this->der2_ = d2Functor_[0](values) ;
            return this->der2_ ;
        }
        return ABC<N, M, DataType>::dEval2(values) ;
    }

private:
    std::function<OutputType(const InputType &)> functor_ ;
    std::vector<std::function<DerOutputType(const InputType &)> > dFunctor_ ;
    std::vector<std::function<Der2OutputType(const InputType &)> > d2Functor_ ;
} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_UserDefined_hpp
