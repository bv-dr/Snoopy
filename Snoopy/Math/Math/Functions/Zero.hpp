#ifndef BV_Math_Functions_Zero_hpp
#define BV_Math_Functions_Zero_hpp

#include <Eigen/Dense>

#include "Math/Functions/ABC.hpp"
#include "Math/Functions/Uniform.hpp"

namespace BV {
namespace Math {
namespace Functions {

/*!
 * \class Zero Math/Functions/Zero.hpp Functions::Zero
 * \brief Provides the possibility to write M zero functions that will
 *     be evaluated at runtime for the N input values.
 * \tparam N, the start dimension
 * \tparam M, the end dimension
 * \tparam DataType, the type of values treated by the zero functions.
 */
template <std::size_t N, std::size_t M, typename DataType=double>
class Zero : public Uniform<N, M, DataType>
{
public:
    
    /*!
     * \brief Constructor
     *
     * Example:
     * \code{.cpp}
     *     Zero<4, 2> func ;
     * \endcode
     *
     */
    Zero(void) :
        Uniform<N, M, DataType>(
                    Details::Traits<N, M, DataType>::getOutputTypeZero()
                               )
    {
    }

} ;

} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Zero_hpp
