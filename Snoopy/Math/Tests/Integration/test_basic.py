import numpy as np
from scipy.integrate import trapezoid
from Snoopy import Math

x = [0,2,3,4]
y = [0,1,1,3]

def test_trapz() :
    assert ( np.isclose( Math.Integration.trapz( y, x) , trapezoid(y, x ) ))
    print ("trapz ok")

def test_simps() :
    pass
    #assert ( np.isclose( Math.Integration.simps( y, x) , simps(y, x ) ))



if __name__ == "__main__" :
    test_trapz()
