#include "Tests/Solvers/LinearFunction.hpp"

#include "Math/Solvers/Newton.hpp"
#include "Math/Solvers/BV_Brent.hpp"
#include "Math/Solvers/Newton.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace Tests {

namespace Details {

double LinFunc::operator()(const double & x) const
{
    return 2.*x + 1. ;
}

void LinFunc::operator()(const double & x, double & y, const double & t) const
{
    y = this->operator ()(x) ;
}

double LinDFunc::operator()(const double & x) const
{
    return 2. ;
}
void LinDFunc::operator()(const double & x, double & dy, const double & t) const
{
    dy = this->operator ()(x) ;
}

Eigen::Vector2d LinFunc2::operator()(const Eigen::Vector2d & x) const
{
    Eigen::Vector2d y ;
    y(0) = 2. * x(0) + 4. * x(1) + 3. ;
    y(1) = 2. * x(0) - 3. * x(1) - 1. ;
    return y ;
}

void LinFunc2::operator()(const Eigen::Vector2d & x, Eigen::Vector2d & y, const double & t) const
{
    y = this->operator ()(x) ;
}


Eigen::Matrix2d LinDFunc2::operator()(const Eigen::Vector2d & x) const
{
    Eigen::Matrix2d dy ;
    dy(0, 0) = 5. + 4. * x(1) ;
    dy(0, 1) = 2. * x(0) + 7. ;
    dy(1, 0) = 1. - 3. * x(1) ;
    dy(1, 1) = 2. * x(0) - 4. ;
    return dy ;
}

void LinDFunc2::operator()(const Eigen::Vector2d & x, Eigen::Matrix2d & dy, const double & t) const
{
    dy = this->operator ()(x) ;
}

} // End of namespace Details

void testLinearFunctionScalar(void)
{
    Details::testLinearFunctionScalar_<Newton<double> >(SolverParameters()) ;

    SolverParameters brentParams ;
    brentParams.set("a", -2.) ;
    brentParams.set("b", 1.) ;
    brentParams.set("fct_tolerance", 1.e-8) ;
    Details::testLinearFunctionScalar_<BV_Brent<double> >(brentParams) ;
}

void testLinearFunctionMultiVariate(void)

{

    Details::testLinearFunctionMultivariate_<Newton<double> >(SolverParameters()) ;
}

} // End of namespace Tests
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV
