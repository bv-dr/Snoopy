#ifndef BV_Math_Solvers_Tests_LinearFunction_hpp
#define BV_Math_Solvers_Tests_LinearFunction_hpp

#include <Eigen/Dense>
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <utility>

#include "Math/Solvers/SolverParameters.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace Tests {

namespace Details {

//void testLinearFunctionNewton(void) ;


struct LinFunc
{
    double operator()(const double & x) const ;
    void operator()(const double & x, double & y, const double & t) const ;
} ;

struct LinDFunc
{
    typedef double JType ;

    template <typename StateIn>
    static JType getJInitialized(const StateIn & state)
    {
        return 0. ;
    }

    double operator()(const double & x) const ;
    void operator()(const double & x, double & dy, const double & t) const ;
} ;

struct LinFunc2
{
    Eigen::Vector2d operator()(const Eigen::Vector2d & x) const ;
    void operator()(const Eigen::Vector2d & x, Eigen::Vector2d & y, const double & t) const ;

} ;

struct LinDFunc2
{
    typedef Eigen::Matrix2d JType ;

    template <typename StateIn>
    static JType getJInitialized(const StateIn & state)
    {
        return Eigen::Matrix2d::Zero() ;
    }

    Eigen::Matrix2d operator()(const Eigen::Vector2d & x) const ;
    void operator()(const Eigen::Vector2d & x, Eigen::Matrix2d & dy, const double & t) const ;
} ;

template <class Solver>
void testLinearFunctionScalar_(const SolverParameters & params)
{
    Solver solver(params) ;
    {
        // Test one variable function with jacobian
        double res(0.) ;
        solver.solve(std::make_pair(LinFunc(), LinDFunc()), 0., res, 0.) ;
        BOOST_CHECK_CLOSE(res, -0.5, 1.e-8) ;
        // Test one variable function no jacobian
        solver.solve(LinFunc(), 0., res, 0.) ;
        BOOST_CHECK_CLOSE(res, -0.5, 1.e-8) ;
    }
}

template <class Solver>
void testLinearFunctionMultivariate_(const SolverParameters & params)
{
    Solver solver(params) ;
    {
        // Test multivariable function
        Eigen::Vector2d res(Eigen::Vector2d::Zero()) ;
        Eigen::Vector2d in(Eigen::Vector2d::Zero()) ;
        solver.solve(std::make_pair(LinFunc2(), LinDFunc2()), in, res, 0.) ;
        Eigen::Matrix2d Ainv ;
        double detAInv(1./(-2.*3 - 2.*4.)) ;
        Ainv(0, 0) = -detAInv * 3. ;
        Ainv(0, 1) = -detAInv * 4. ;
        Ainv(1, 0) = -detAInv * 2. ;
        Ainv(1, 1) = detAInv * 2. ;
        Eigen::Vector2d comp ;
        comp(0) = -Ainv(0, 0) * 3. + Ainv(0, 1) ;
        comp(1) = -Ainv(1, 0) * 3. + Ainv(1, 1) ;
        BOOST_CHECK_SMALL(res(0)-comp(0), 1.e-5) ;
        BOOST_CHECK_SMALL(res(1)-comp(1), 1.e-5) ;
        // Test multivarible function no jacobian
        solver.solve(LinFunc2(), in, res, 0.) ;
        BOOST_CHECK_SMALL(res(0)-comp(0), 1.e-5) ;
        BOOST_CHECK_SMALL(res(1)-comp(1), 1.e-5) ;
    }
}

} // End of namespace Details

void testLinearFunctionScalar(void) ;
void testLinearFunctionMultiVariate(void) ;

} // End of namespace Tests
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_Tests_LinearFunction_hpp
