#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

#include "Tests/Solvers/LinearFunction.hpp"
#include "Tests/Solvers/NonLinearFunction.hpp"

using namespace boost::unit_test ;

#define BV_ADD_SOLVERS_TEST( testName )                  \
    SolversTestSuite->add(BOOST_TEST_CASE( &(            \
                      BV::Math::Solvers::Tests::testName \
                                            ) ) ) ;

test_suite * init_unit_test_suite(int argc, char* argv[])
{
    test_suite * SolversTestSuite = BOOST_TEST_SUITE("SolversTestSuite") ;

    BV_ADD_SOLVERS_TEST(testLinearFunctionScalar) ;
    BV_ADD_SOLVERS_TEST(testLinearFunctionMultiVariate) ;
     BV_ADD_SOLVERS_TEST(testNonLinearFunctionScalar) ;

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV solvers test suite" ;
    framework::master_test_suite().add(SolversTestSuite) ;

  return 0 ;
}

#undef BV_ADD_SOLVERS_TEST
