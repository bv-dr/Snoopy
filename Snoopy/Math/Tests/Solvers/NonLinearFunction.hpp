#ifndef BV_Math_Solvers_Tests_NonLinearFunction_hpp
#define BV_Math_Solvers_Tests_NonLinearFunction_hpp

#include <Eigen/Dense>
#include <boost/test/unit_test.hpp>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <utility>
#include <cmath>

#include "Math/Solvers/SolverParameters.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace Tests {

namespace Details {
struct NonLinFunc
{
    double operator()(const double & x) const ;
    void operator()(const double & x, double & y, const double & t) const ;
} ;

struct NonLinDFunc
{
    typedef double JType ;

    template <typename StateIn>
    static JType getJInitialized(const StateIn & state)
    {
        return 0. ;
    }

    double operator()(const double & x) const ;
    void operator()(const double & x, double & dy, const double & t) const ;
} ;

template <class Solver>
void testNonLinearFunctionScalarSol1_(const SolverParameters & params)
{
    Solver solver(params) ;
    {
        // Test one variable function with jacobian
        // Start closer to positive solution
        double res(0.) ;
        solver.solve(std::make_pair(Details::NonLinFunc(), Details::NonLinDFunc()), 0.1, res, 0.) ;
        BOOST_CHECK_CLOSE(res, std::sqrt(0.5), 1.e-8) ;
    }
    {
        // Test one variable function without jacobian
        // Start closer to positive solution
        double res(0.) ;
        solver.solve(Details::NonLinFunc(), 0.1, res, 0.) ;
        BOOST_CHECK_CLOSE(res, std::sqrt(0.5), 1.e-8) ;
    }
}

template <class Solver>
void testNonLinearFunctionScalarSol2_(const SolverParameters & params)
{
    Solver solver(params) ;

    {
        // Test one variable function with jacobian
        // Start closer to negative solution
        double res(0.) ;
        solver.solve(std::make_pair(Details::NonLinFunc(), Details::NonLinDFunc()), -0.1, res, 0.) ;
        BOOST_CHECK_CLOSE(res, -std::sqrt(0.5), 1.e-8) ;
    }
    {
        // Test one variable function without jacobian
        // Start closer to negative solution
        double res(0.) ;
        solver.solve(Details::NonLinFunc(), -0.1, res, 0.) ;
        BOOST_CHECK_CLOSE(res, -std::sqrt(0.5), 1.e-8) ;
    }
}

} // End of namespace Details

void testNonLinearFunctionScalar(void) ;

} // End of namespace Tests
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Solvers_Tests_NonLinearFunction_hpp
