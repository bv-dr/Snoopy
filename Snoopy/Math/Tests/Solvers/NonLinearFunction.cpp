#include "Tests/Solvers/NonLinearFunction.hpp"

#include "Math/Solvers/Newton.hpp"
#include "Math/Solvers/BV_Brent.hpp"
#include "Math/Solvers/Newton.hpp"

namespace BV {
namespace Math {
namespace Solvers {
namespace Tests {

namespace Details {
double NonLinFunc::operator()(const double & x) const
{
    return 2.*std::pow(x, 2) - 1. ;
}

void NonLinFunc::operator()(const double & x, double & y, const double & t) const
{
    y = this->operator ()(x) ;
}

double NonLinDFunc::operator()(const double & x) const
{
    return 4. * x ;
}

void NonLinDFunc::operator()(const double & x, double & dy, const double & t) const
{
    dy = this->operator ()(x) ;
}

} // End of namespace Details

void testNonLinearFunctionScalar(void)
{
    SolverParameters newtonParams ;
    Details::testNonLinearFunctionScalarSol1_<Newton<double> >(newtonParams) ;
    Details::testNonLinearFunctionScalarSol2_<Newton<double> >(newtonParams) ;

    SolverParameters brentParamsSol1 ;
    brentParamsSol1.set("a", 0.) ;
    brentParamsSol1.set("b", 1.) ;
    brentParamsSol1.set("fct_tolerance", 1.e-8) ;
    Details::testNonLinearFunctionScalarSol1_<BV_Brent<double> >(brentParamsSol1) ;

    SolverParameters brentParamsSol2 ;
    brentParamsSol2.set("a", -1.) ;
    brentParamsSol2.set("b", 0.) ;
    brentParamsSol2.set("fct_tolerance", 1.e-8) ;
    Details::testNonLinearFunctionScalarSol2_<BV_Brent<double> >(brentParamsSol2) ;
}

} // End of namespace Tests
} // End of namespace Solvers
} // End of namespace Math
} // End of namespace BV
