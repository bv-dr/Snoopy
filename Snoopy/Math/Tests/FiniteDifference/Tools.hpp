#ifndef BV_Math_FiniteDifference_Tests_Tools_hpp
#define BV_Math_FiniteDifference_Tests_Tools_hpp

#include <Eigen/Dense>
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

#include <unsupported/Eigen/CXX11/Tensor>

namespace BV {
namespace Math {
namespace FiniteDifference {
namespace Tests {

template <typename Derived, typename Derived2>
void CheckEigenType(const Eigen::MatrixBase<Derived> & m1,
                    const Eigen::MatrixBase<Derived2> & m2,
                    double eps=1.e-8)
{
    for (unsigned iRow=0; iRow<m1.rows(); ++iRow)
    {
        for (unsigned iCol=0; iCol<m1.cols(); ++iCol)
        {
            BOOST_CHECK_SMALL(m1(iRow, iCol) - m2(iRow, iCol), eps) ;
        }
    }
}

template <int Rank>
struct TensorChecker
{
    template <typename DataType, int Order>
    static void check(
               const Eigen::Tensor<DataType,Rank,Order> & ar1,
               const Eigen::Tensor<DataType,Rank,Order> & ar2,
               double eps=1.e-8
                     )
    {
        for (int i=0; i<ar1.dimensions()[0]; ++i)
        {
            /* Original code which started from Eigen 3.4 has a problem with
             * chip function as soon as the Tensor has Rank == 1
             * Eigen::Tensor<int, 3> a; a.setValues( {1, 2, 3} ); a.chip(0,0);
             * crashes (not the case in version 3.3)
             *
            Eigen::Tensor<DataType, Rank-1, Order> a1(ar1.chip(i, 0)) ;
            Eigen::Tensor<DataType, Rank-1, Order> a2(ar2.chip(i, 0)) ;
            TensorChecker<Rank-1>::check(a1, a2, eps) ;
             *
             * The following code is a fix of the above code to be able to work with Eigen >=3.4
             * 07/11/2023
             */
            /* Start of fix (std >= c++17)*/
            if constexpr (Rank > 1)
            {
            // Because of "if constexpr" the below code is compiled for Rank > 1
                Eigen::Tensor<DataType, Rank-1, Order> a1(ar1.chip(i, 0)) ;
                Eigen::Tensor<DataType, Rank-1, Order> a2(ar2.chip(i, 0)) ;
                TensorChecker<Rank-1>::check(a1, a2, eps) ;
            }
            else
            {
            // This code is compiled for Rank == 1 because for Rank == 0 there is
            // a specialization written later on
                Eigen::Tensor<DataType, Rank-1, Order> a1; a1.setValues(ar1(i)) ;
                Eigen::Tensor<DataType, Rank-1, Order> a2; a2.setValues(ar2(i)) ;
                TensorChecker<Rank-1>::check(a1, a2, eps) ;
            }
            /* End of fix */
        }
    }
} ;

template <>
struct TensorChecker<0>
{
    template <typename DataType, int Order>
    static void check(
               const Eigen::Tensor<DataType,0,Order> & ar1,
               const Eigen::Tensor<DataType,0,Order> & ar2,
               double eps=1.e-8
                     )
    {
        BOOST_CHECK_SMALL(ar1(0) - ar2(0), eps) ;
    }
} ;


} // End of namespace Tests
} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_FiniteDifference_Tests_Tools_hpp
