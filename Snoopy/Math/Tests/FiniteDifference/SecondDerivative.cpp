#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <functional>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "Tests/FiniteDifference/FirstDerivative.hpp"
#include "Tests/FiniteDifference/Tools.hpp"
#include "Math/FiniteDifference/FiniteDifference.hpp"
#include "Math/Functions/Analytical.hpp"

namespace BV {
namespace Math {
namespace FiniteDifference {
namespace Tests {

void FD2_Function(void)
{
    using BV::Math::Functions::Analytical ;
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::SECOND, 2,
                             FDContext::FIXED_SIZE_IO> TypeCO2 ;
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::SECOND, 4,
                             FDContext::FIXED_SIZE_IO> TypeCO4 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::SECOND, 2,
                             FDContext::FIXED_SIZE_IO> TypeFO2 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::SECOND, 4,
                             FDContext::FIXED_SIZE_IO> TypeFO4 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::SECOND, 2,
                             FDContext::FIXED_SIZE_IO> TypeBO2 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::SECOND, 4,
                             FDContext::FIXED_SIZE_IO> TypeBO4 ;
    {
        Analytical<1, 1> func("2.*x**2+3.*x+4.", "x") ;
        double resCO2(TypeCO2::get(3., func, 1.e-5)) ;
        double resCO4(TypeCO4::get(3., func, 1.e-3)) ;
        double resFO2(TypeFO2::get(3., func, 1.e-3)) ;
        double resFO4(TypeFO4::get(3., func, 1.e-3)) ;
        double resBO2(TypeBO2::get(3., func, 1.e-3)) ;
        double resBO4(TypeBO4::get(3., func, 1.e-3)) ;
        BOOST_CHECK_CLOSE(resCO2, 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(resCO4, 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(resFO2, 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(resFO4, 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(resBO2, 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(resBO4, 4., 1.e-5) ;
    }
    {
        Analytical<1, 2> func({"2.*x**2+3.*x+4.", "2.*x**3"}, "x") ;
        Eigen::Vector2d resCO2(TypeCO2::get(3., func, 1.e-4)) ;
        Eigen::Vector2d resCO4(TypeCO4::get(3., func, 1.e-3)) ;
        Eigen::Vector2d resFO2(TypeFO2::get(3., func, 1.e-3)) ;
        Eigen::Vector2d resFO4(TypeFO4::get(3., func, 1.e-3)) ;
        Eigen::Vector2d resBO2(TypeBO2::get(3., func, 1.e-3)) ;
        Eigen::Vector2d resBO4(TypeBO4::get(3., func, 1.e-3)) ;
        Eigen::Vector2d comp ;
        comp(0) = 4. ;
        comp(1) = 36. ;
        CheckEigenType(resCO2, comp, 1.e-5) ;
        CheckEigenType(resCO4, comp, 1.e-5) ;
        CheckEigenType(resFO2, comp, 1.e-5) ;
        CheckEigenType(resFO4, comp, 1.e-5) ;
        CheckEigenType(resBO2, comp, 1.e-5) ;
        CheckEigenType(resBO4, comp, 1.e-5) ;
    }
    {
        Analytical<2, 1> func("2.*x**2+3.*y**2+4.*x*y**2", {"x", "y"}) ;
        Eigen::Vector2d input ;
        input << 3., 4. ;
        Eigen::Vector2d h ;
        h << 1.e-3, 1.e-3 ;
        Eigen::Matrix2d resCO2(TypeCO2::get(input, func, h)) ;
        Eigen::Matrix2d resCO2Bis(TypeCO2::get(input, func)) ;
        Eigen::Matrix2d resCO4(TypeCO4::get(input, func, h)) ;
        Eigen::Matrix2d resFO2(TypeFO2::get(input, func, h)) ;
        Eigen::Matrix2d resFO2Bis(TypeFO2::get(input, func)) ;
        Eigen::Matrix2d resFO4(TypeFO4::get(input, func, h)) ;
        Eigen::Matrix2d resBO2(TypeBO2::get(input, func, h)) ;
        Eigen::Matrix2d resBO2Bis(TypeBO2::get(input, func)) ;
        Eigen::Matrix2d resBO4(TypeBO4::get(input, func, h)) ;
        Eigen::Matrix2d comp ;
        comp(0, 0) = 4. ;
        comp(0, 1) = 32. ;
        comp(1, 0) = 32. ;
        comp(1, 1) = 30. ;
        CheckEigenType(resCO2, comp, 1.e-4) ;
        CheckEigenType(resCO2Bis, comp, 1.e-4) ;
        CheckEigenType(resCO4, comp, 1.e-4) ;
        CheckEigenType(resFO2, comp, 1.e-4) ;
        CheckEigenType(resFO2Bis, comp, 1.e-4) ;
        CheckEigenType(resFO4, comp, 1.e-4) ;
        CheckEigenType(resBO2, comp, 1.e-4) ;
        CheckEigenType(resBO2Bis, comp, 1.e-4) ;
        CheckEigenType(resBO4, comp, 1.e-4) ;
    }
    {
        Analytical<2, 1> func("2.*x**2+3.*y**2+4.*x*y**2", {"x", "y"}) ;
        Eigen::Matrix2d input ;
        input << 3., 4., 4., 3. ;
        Eigen::Vector2d h ;
        h << 1.e-3, 1.e-3 ;
        Eigen::Matrix2d resCO2(TypeCO2::get(input.row(0), func, h)) ;
        Eigen::Matrix2d resCO2Bis(TypeCO2::get(input.col(0), func)) ;
        Eigen::Matrix2d resCO4(TypeCO4::get(input.row(0), func, h)) ;
        Eigen::Matrix2d resFO2(TypeFO2::get(input.row(0), func, h)) ;
        Eigen::Matrix2d resFO2Bis(TypeFO2::get(input.col(0), func)) ;
        Eigen::Matrix2d resFO4(TypeFO4::get(input.row(0), func, h)) ;
        Eigen::Matrix2d resBO2(TypeBO2::get(input.row(0), func, h)) ;
        Eigen::Matrix2d resBO2Bis(TypeBO2::get(input.col(0), func)) ;
        Eigen::Matrix2d resBO4(TypeBO4::get(input.row(0), func, h)) ;
        Eigen::Matrix2d comp ;
        comp(0, 0) = 4. ;
        comp(0, 1) = 32. ;
        comp(1, 0) = 32. ;
        comp(1, 1) = 30. ;
        CheckEigenType(resCO2, comp, 1.e-4) ;
        CheckEigenType(resCO2Bis, comp, 1.e-4) ;
        CheckEigenType(resCO4, comp, 1.e-4) ;
        CheckEigenType(resFO2, comp, 1.e-4) ;
        CheckEigenType(resFO2Bis, comp, 1.e-4) ;
        CheckEigenType(resFO4, comp, 1.e-4) ;
        CheckEigenType(resBO2, comp, 1.e-4) ;
        CheckEigenType(resBO2Bis, comp, 1.e-4) ;
        CheckEigenType(resBO4, comp, 1.e-4) ;
    }
    {
        Analytical<2, 3> func({"2.*x**2+3.*y+4.", "3.*x**2", "2.*y**2"},
                              {"x", "y"}) ;
        Eigen::Vector2d input ;
        input << 3., 4. ;
        Eigen::Tensor<double, 3> resCO2(TypeCO2::get(input, func)) ;
        Eigen::Tensor<double, 3> resCO4(TypeCO4::get(input, func)) ;
        Eigen::Tensor<double, 3> resFO2(TypeCO2::get(input, func)) ;
        Eigen::Tensor<double, 3> resFO4(TypeCO4::get(input, func)) ;
        Eigen::Tensor<double, 3> resBO2(TypeCO2::get(input, func)) ;
        Eigen::Tensor<double, 3> resBO4(TypeCO4::get(input, func)) ;

        Eigen::Tensor<double, 3> comp(3, 2, 2) ;
        comp(0, 0, 0) = 4. ;
        comp(0, 0, 1) = 0. ;
        comp(0, 1, 0) = 0. ;
        comp(0, 1, 1) = 0. ;
        comp(1, 0, 0) = 6. ;
        comp(1, 0, 1) = 0. ;
        comp(1, 1, 0) = 0. ;
        comp(1, 1, 1) = 0. ;
        comp(2, 0, 0) = 0. ;
        comp(2, 0, 1) = 0. ;
        comp(2, 1, 0) = 0. ;
        comp(2, 1, 1) = 4. ;

        TensorChecker<3>::check(resCO2, comp, 1.e-4) ;
        TensorChecker<3>::check(resCO4, comp, 1.e-4) ;
        TensorChecker<3>::check(resFO2, comp, 1.e-4) ;
        TensorChecker<3>::check(resFO4, comp, 1.e-4) ;
        TensorChecker<3>::check(resBO2, comp, 1.e-4) ;
        TensorChecker<3>::check(resBO4, comp, 1.e-4) ;
    }
}

void FD2_Hessian(void)
{
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::SECOND, 2,
                             FDContext::JACOBIAN_CALCULATION> TypeCO2 ;
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::SECOND, 4,
                             FDContext::JACOBIAN_CALCULATION> TypeCO4 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::SECOND, 2,
                             FDContext::JACOBIAN_CALCULATION> TypeFO2 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::SECOND, 4,
                             FDContext::JACOBIAN_CALCULATION> TypeFO4 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::SECOND, 2,
                             FDContext::JACOBIAN_CALCULATION> TypeBO2 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::SECOND, 4,
                             FDContext::JACOBIAN_CALCULATION> TypeBO4 ;
    struct Functor
    {
        Eigen::VectorXd eval(const Eigen::VectorXd & in)
        {
            return get(in) ;
        }

        Eigen::VectorXd operator()(const Eigen::VectorXd & in)
        {
            return get(in) ;
        }

        static Eigen::VectorXd get(const Eigen::VectorXd & in)
        {
            // f_i = x_i^2 - x_{i+1}^2
            Eigen::VectorXd::Index size(in.size()) ;
            Eigen::VectorXd tmp(in.cwiseProduct(in)) ;
            tmp.head(size-1) -= in.tail(size-1).cwiseProduct(in.tail(size-1)) ;
            return tmp ;
        }
    } ;
    {
        Eigen::VectorXd input(5) ;
        input << 1., 2., 3., 4., 5. ;
        Functor func ;
        Eigen::Tensor<double, 3> hesCO2(TypeCO2::get(input, func)) ;
        Eigen::Tensor<double, 3> hesCO4(TypeCO4::get(input, func)) ;
        Eigen::Tensor<double, 3> hesFO2(TypeFO2::get(input, func)) ;
        Eigen::Tensor<double, 3> hesFO4(TypeFO4::get(input, func)) ;
        Eigen::Tensor<double, 3> hesBO2(TypeBO2::get(input, func)) ;
        Eigen::Tensor<double, 3> hesBO4(TypeBO4::get(input, func)) ;
        Eigen::Tensor<double, 3> comp(5, 5, 5) ;
        comp.setZero() ;
        for (unsigned i=0; i<5; ++i)
        {
            comp(i, i, i) = 2. ;
            if (i<4)
            {
                comp(i, i+1, i+1) = -2. ;
            }
        }
        TensorChecker<3>::check(hesCO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesCO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Vector4d input ;
        input << 1., 2., 3., 4. ;
        Functor func ;
        Eigen::Tensor<double, 3> hesCO2(TypeCO2::get(input, func)) ;
        Eigen::Tensor<double, 3> hesCO4(TypeCO4::get(input, func)) ;
        Eigen::Tensor<double, 3> hesFO2(TypeFO2::get(input, func)) ;
        Eigen::Tensor<double, 3> hesFO4(TypeFO4::get(input, func)) ;
        Eigen::Tensor<double, 3> hesBO2(TypeBO2::get(input, func)) ;
        Eigen::Tensor<double, 3> hesBO4(TypeBO4::get(input, func)) ;
        Eigen::Tensor<double, 3> comp(4, 4, 4) ;
        comp.setZero() ;
        for (unsigned i=0; i<4; ++i)
        {
            comp(i, i, i) = 2. ;
            if (i<3)
            {
                comp(i, i+1, i+1) = -2. ;
            }
        }
        TensorChecker<3>::check(hesCO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesCO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Matrix4d input ;
        input << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12.,
                 13., 14., 15., 16. ;
        Functor func ;
        Eigen::Tensor<double, 3> hesCO2(TypeCO2::get(input.col(0), func)) ;
        Eigen::Tensor<double, 3> hesCO4(TypeCO4::get(input.row(0), func)) ;
        Eigen::Tensor<double, 3> hesFO2(TypeFO2::get(input.col(0), func)) ;
        Eigen::Tensor<double, 3> hesFO4(TypeFO4::get(input.row(0), func)) ;
        Eigen::Tensor<double, 3> hesBO2(TypeBO2::get(input.col(0), func)) ;
        Eigen::Tensor<double, 3> hesBO4(TypeBO4::get(input.row(0), func)) ;
        Eigen::Tensor<double, 3> comp(4, 4, 4) ;
        comp.setZero() ;
        for (unsigned i=0; i<4; ++i)
        {
            comp(i, i, i) = 2. ;
            if (i<3)
            {
                comp(i, i+1, i+1) = -2. ;
            }
        }
        TensorChecker<3>::check(hesCO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesCO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Vector4d input ;
        input << 1., 2., 3., 4. ;
        Eigen::Tensor<double, 3> hesCO2(TypeCO2::get(input, Functor::get)) ;
        Eigen::Tensor<double, 3> hesCO4(TypeCO4::get(input, Functor::get)) ;
        Eigen::Tensor<double, 3> hesFO2(TypeFO2::get(input, Functor::get)) ;
        Eigen::Tensor<double, 3> hesFO4(TypeFO4::get(input, Functor::get)) ;
        Eigen::Tensor<double, 3> hesBO2(TypeBO2::get(input, Functor::get)) ;
        Eigen::Tensor<double, 3> hesBO4(TypeBO4::get(input, Functor::get)) ;
        Eigen::Tensor<double, 3> comp(4, 4, 4) ;
        comp.setZero() ;
        for (unsigned i=0; i<4; ++i)
        {
            comp(i, i, i) = 2. ;
            if (i<3)
            {
                comp(i, i+1, i+1) = -2. ;
            }
        }
        TensorChecker<3>::check(hesCO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesCO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Vector4d input ;
        input << 1., 2., 3., 4. ;
        Functor func ;
        std::function<Eigen::Vector4d(const Eigen::Vector4d &)> f(
                          std::bind(&Functor::eval, func, std::placeholders::_1)
                                                                 ) ;
        Eigen::Tensor<double, 3> hesCO2(TypeCO2::get(input, f)) ;
        Eigen::Tensor<double, 3> hesCO4(TypeCO4::get(input, f)) ;
        Eigen::Tensor<double, 3> hesFO2(TypeFO2::get(input, f)) ;
        Eigen::Tensor<double, 3> hesFO4(TypeFO4::get(input, f)) ;
        Eigen::Tensor<double, 3> hesBO2(TypeBO2::get(input, f)) ;
        Eigen::Tensor<double, 3> hesBO4(TypeBO4::get(input, f)) ;
        Eigen::Tensor<double, 3> comp(4, 4, 4) ;
        comp.setZero() ;
        for (unsigned i=0; i<4; ++i)
        {
            comp(i, i, i) = 2. ;
            if (i<3)
            {
                comp(i, i+1, i+1) = -2. ;
            }
        }
        TensorChecker<3>::check(hesCO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesCO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesFO4, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO2, comp, 1.e-5) ;
        TensorChecker<3>::check(hesBO4, comp, 1.e-5) ;
    }
}

} // End of namespace Tests
} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV
