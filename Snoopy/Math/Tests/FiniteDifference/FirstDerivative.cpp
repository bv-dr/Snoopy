#include "Tests/FiniteDifference/FirstDerivative.hpp"

#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <functional>
#include <Eigen/Dense>

#include "Tests/FiniteDifference/Tools.hpp"
#include "Math/FiniteDifference/FiniteDifference.hpp"
#include "Math/Functions/Analytical.hpp"

namespace BV {
namespace Math {
namespace FiniteDifference {
namespace Tests {

void FD1_Function(void)
{
    using BV::Math::Functions::Analytical ;
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::FIRST, 2,
                             FDContext::FIXED_SIZE_IO> TypeCO2 ;
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::FIRST, 4,
                             FDContext::FIXED_SIZE_IO> TypeCO4 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::FIRST, 2,
                             FDContext::FIXED_SIZE_IO> TypeFO2 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::FIRST, 4,
                             FDContext::FIXED_SIZE_IO> TypeFO4 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::FIRST, 2,
                             FDContext::FIXED_SIZE_IO> TypeBO2 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::FIRST, 4,
                             FDContext::FIXED_SIZE_IO> TypeBO4 ;
    {
        Analytical<1, 1> func("2.*x**2+3.*x+4.", "x") ;
        double resCO2(TypeCO2::get(3., func, 1.e-8)) ;
        double resCO4(TypeCO4::get(3., func, 1.e-8)) ;
        double resFO2(TypeFO2::get(3., func, 1.e-8)) ;
        double resFO4(TypeFO4::get(3., func, 1.e-8)) ;
        double resBO2(TypeBO2::get(3., func, 1.e-8)) ;
        double resBO4(TypeBO4::get(3., func, 1.e-8)) ;
        BOOST_CHECK_CLOSE(resCO2, 15., 1.e-5) ;
        BOOST_CHECK_CLOSE(resCO4, 15., 1.e-5) ;
        BOOST_CHECK_CLOSE(resFO2, 15., 1.e-5) ;
        BOOST_CHECK_CLOSE(resFO4, 15., 1.e-5) ;
        BOOST_CHECK_CLOSE(resBO2, 15., 1.e-5) ;
        BOOST_CHECK_CLOSE(resBO4, 15., 1.e-5) ;
    }
    {
        Analytical<2, 1> func("2.*x**2+3.*y+4.", {"x", "y"}) ;
        Eigen::Vector2d input ;
        input << 3., 4. ;
        Eigen::Vector2d h ;
        h << 1.e-6, 1.e-6 ;
        Eigen::Vector2d resCO2(TypeCO2::get(input, func, h)) ;
        Eigen::Vector2d resCO2Bis(TypeCO2::get(input, func)) ;
        Eigen::Vector2d resCO4(TypeCO4::get(input, func, h)) ;
        Eigen::Vector2d resFO2(TypeFO2::get(input, func, h)) ;
        Eigen::Vector2d resFO2Bis(TypeFO2::get(input, func)) ;
        Eigen::Vector2d resFO4(TypeFO4::get(input, func, h)) ;
        Eigen::Vector2d resBO2(TypeBO2::get(input, func, h)) ;
        Eigen::Vector2d resBO2Bis(TypeBO2::get(input, func)) ;
        Eigen::Vector2d resBO4(TypeBO4::get(input, func, h)) ;
        Eigen::Vector2d comp ;
        comp(0) = 12. ;
        comp(1) = 3. ;
        CheckEigenType(resCO2, comp, 1.e-5) ;
        CheckEigenType(resCO2Bis, comp, 1.e-5) ;
        CheckEigenType(resCO4, comp, 1.e-5) ;
        CheckEigenType(resFO2, comp, 1.e-5) ;
        CheckEigenType(resFO2Bis, comp, 1.e-5) ;
        CheckEigenType(resFO4, comp, 1.e-5) ;
        CheckEigenType(resBO2, comp, 1.e-5) ;
        CheckEigenType(resBO2Bis, comp, 1.e-5) ;
        CheckEigenType(resBO4, comp, 1.e-5) ;
    }
    {
        Analytical<2, 1> func("2.*x**2+3.*y+4.", {"x", "y"}) ;
        Eigen::Matrix2d input ;
        input << 3., 4., 4., 3. ;
        Eigen::Vector2d h ;
        h << 1.e-6, 1.e-6 ;
        Eigen::Vector2d resCO2(TypeCO2::get(input.row(0), func, h)) ;
        Eigen::Vector2d resCO2Bis(TypeCO2::get(input.col(0), func)) ;
        Eigen::Vector2d resCO4(TypeCO4::get(input.row(0), func, h)) ;
        Eigen::Vector2d resFO2(TypeFO2::get(input.row(0), func, h)) ;
        Eigen::Vector2d resFO2Bis(TypeFO2::get(input.col(0), func)) ;
        Eigen::Vector2d resFO4(TypeFO4::get(input.row(0), func, h)) ;
        Eigen::Vector2d resBO2(TypeBO2::get(input.row(0), func, h)) ;
        Eigen::Vector2d resBO2Bis(TypeBO2::get(input.col(0), func)) ;
        Eigen::Vector2d resBO4(TypeBO4::get(input.row(0), func, h)) ;
        Eigen::Vector2d comp ;
        comp(0) = 12. ;
        comp(1) = 3. ;
        CheckEigenType(resCO2, comp, 1.e-5) ;
        CheckEigenType(resCO2Bis, comp, 1.e-5) ;
        CheckEigenType(resCO4, comp, 1.e-5) ;
        CheckEigenType(resFO2, comp, 1.e-5) ;
        CheckEigenType(resFO2Bis, comp, 1.e-5) ;
        CheckEigenType(resFO4, comp, 1.e-5) ;
        CheckEigenType(resBO2, comp, 1.e-5) ;
        CheckEigenType(resBO2Bis, comp, 1.e-5) ;
        CheckEigenType(resBO4, comp, 1.e-5) ;
    }
    {
        Analytical<2, 3> func({"2.*x**2+3.*y+4.", "3.*x**2", "2.*y**2"},
                              {"x", "y"}) ;
        Eigen::Vector2d input ;
        input << 3., 4. ;
        Eigen::Matrix<double, 3, 2> resCO2(TypeCO2::get(input, func)) ;
        Eigen::Matrix<double, 3, 2> resCO4(TypeCO4::get(input, func)) ;
        Eigen::Matrix<double, 3, 2> resFO2(TypeCO2::get(input, func)) ;
        Eigen::Matrix<double, 3, 2> resFO4(TypeCO4::get(input, func)) ;
        Eigen::Matrix<double, 3, 2> resBO2(TypeCO2::get(input, func)) ;
        Eigen::Matrix<double, 3, 2> resBO4(TypeCO4::get(input, func)) ;
        Eigen::Matrix<double, 3, 2> comp ;
        comp(0, 0) = 12. ;
        comp(0, 1) = 3. ;
        comp(1, 0) = 18. ;
        comp(1, 1) = 0. ;
        comp(2, 0) = 0. ;
        comp(2, 1) = 16. ;
        CheckEigenType(resCO2, comp, 1.e-5) ;
        CheckEigenType(resCO4, comp, 1.e-5) ;
        CheckEigenType(resFO2, comp, 1.e-5) ;
        CheckEigenType(resFO4, comp, 1.e-5) ;
        CheckEigenType(resBO2, comp, 1.e-5) ;
        CheckEigenType(resBO4, comp, 1.e-5) ;
    }
}

void FD1_Jacobian(void)
{
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::FIRST, 2,
                             FDContext::JACOBIAN_CALCULATION> TypeCO2 ;
    typedef FiniteDifference<FDScheme::CENTRAL,
                             IthDerivative::FIRST, 4,
                             FDContext::JACOBIAN_CALCULATION> TypeCO4 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::FIRST, 2,
                             FDContext::JACOBIAN_CALCULATION> TypeFO2 ;
    typedef FiniteDifference<FDScheme::FORWARD,
                             IthDerivative::FIRST, 4,
                             FDContext::JACOBIAN_CALCULATION> TypeFO4 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::FIRST, 2,
                             FDContext::JACOBIAN_CALCULATION> TypeBO2 ;
    typedef FiniteDifference<FDScheme::BACKWARD,
                             IthDerivative::FIRST, 4,
                             FDContext::JACOBIAN_CALCULATION> TypeBO4 ;
    struct Functor
    {
        Eigen::VectorXd operator()(const Eigen::VectorXd & in)
        {
            return 2. * in ;
        }

        Eigen::VectorXd eval(const Eigen::VectorXd & in)
        {
            return 2. * in ;
        }

        static Eigen::VectorXd get(const Eigen::VectorXd & in)
        {
            return 2. * in ;
        }
    } ;
    {
        Eigen::VectorXd input(5) ;
        input << 1., 2., 3., 4., 5. ;
        Functor func ;
        Eigen::MatrixXd jacCO2(TypeCO2::get(input, func)) ;
        Eigen::MatrixXd jacCO4(TypeCO4::get(input, func)) ;
        Eigen::MatrixXd jacFO2(TypeFO2::get(input, func)) ;
        Eigen::MatrixXd jacFO4(TypeFO4::get(input, func)) ;
        Eigen::MatrixXd jacBO2(TypeBO2::get(input, func)) ;
        Eigen::MatrixXd jacBO4(TypeBO4::get(input, func)) ;
        Eigen::MatrixXd comp(Eigen::MatrixXd::Identity(5, 5)) ;
        comp *= 2. ;
        CheckEigenType(jacCO2, comp, 1.e-5) ;
        CheckEigenType(jacCO4, comp, 1.e-5) ;
        CheckEigenType(jacFO2, comp, 1.e-5) ;
        CheckEigenType(jacFO4, comp, 1.e-5) ;
        CheckEigenType(jacBO2, comp, 1.e-5) ;
        CheckEigenType(jacBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Vector4d input ;
        input << 1., 2., 3., 4. ;
        Functor func ;
        Eigen::Matrix4d jacCO2(TypeCO2::get(input, func)) ;
        Eigen::Matrix4d jacCO4(TypeCO4::get(input, func)) ;
        Eigen::Matrix4d jacFO2(TypeFO2::get(input, func)) ;
        Eigen::Matrix4d jacFO4(TypeFO4::get(input, func)) ;
        Eigen::Matrix4d jacBO2(TypeBO2::get(input, func)) ;
        Eigen::Matrix4d jacBO4(TypeBO4::get(input, func)) ;
        Eigen::Matrix4d comp(Eigen::Matrix4d::Identity()) ;
        comp *= 2. ;
        CheckEigenType(jacCO2, comp, 1.e-5) ;
        CheckEigenType(jacCO4, comp, 1.e-5) ;
        CheckEigenType(jacFO2, comp, 1.e-5) ;
        CheckEigenType(jacFO4, comp, 1.e-5) ;
        CheckEigenType(jacBO2, comp, 1.e-5) ;
        CheckEigenType(jacBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Matrix4d input ;
        input << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12.,
                 13., 14., 15., 16. ;
        Functor func ;
        Eigen::Matrix4d jacCO2(TypeCO2::get(input.col(0), func)) ;
        Eigen::Matrix4d jacCO4(TypeCO4::get(input.row(0), func)) ;
        Eigen::Matrix4d jacFO2(TypeFO2::get(input.col(0), func)) ;
        Eigen::Matrix4d jacFO4(TypeFO4::get(input.row(0), func)) ;
        Eigen::Matrix4d jacBO2(TypeBO2::get(input.col(0), func)) ;
        Eigen::Matrix4d jacBO4(TypeBO4::get(input.row(0), func)) ;
        Eigen::Matrix4d comp(Eigen::Matrix4d::Identity()) ;
        comp *= 2. ;
        CheckEigenType(jacCO2, comp, 1.e-5) ;
        CheckEigenType(jacCO4, comp, 1.e-5) ;
        CheckEigenType(jacFO2, comp, 1.e-5) ;
        CheckEigenType(jacFO4, comp, 1.e-5) ;
        CheckEigenType(jacBO2, comp, 1.e-5) ;
        CheckEigenType(jacBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Vector4d input ;
        input << 1., 2., 3., 4. ;
        Eigen::Matrix4d jacCO2(TypeCO2::get(input, Functor::get)) ;
        Eigen::Matrix4d jacCO4(TypeCO4::get(input, Functor::get)) ;
        Eigen::Matrix4d jacFO2(TypeFO2::get(input, Functor::get)) ;
        Eigen::Matrix4d jacFO4(TypeFO4::get(input, Functor::get)) ;
        Eigen::Matrix4d jacBO2(TypeBO2::get(input, Functor::get)) ;
        Eigen::Matrix4d jacBO4(TypeBO4::get(input, Functor::get)) ;
        Eigen::Matrix4d comp(Eigen::Matrix4d::Identity()) ;
        comp *= 2. ;
        CheckEigenType(jacCO2, comp, 1.e-5) ;
        CheckEigenType(jacCO4, comp, 1.e-5) ;
        CheckEigenType(jacFO2, comp, 1.e-5) ;
        CheckEigenType(jacFO4, comp, 1.e-5) ;
        CheckEigenType(jacBO2, comp, 1.e-5) ;
        CheckEigenType(jacBO4, comp, 1.e-5) ;
    }
    {
        Eigen::Vector4d input ;
        input << 1., 2., 3., 4. ;
        Functor func ;
        std::function<Eigen::Vector4d(const Eigen::Vector4d &)> f(
                          std::bind(&Functor::eval, func, std::placeholders::_1)
                                                                 ) ;
        Eigen::Matrix4d jacCO2(TypeCO2::get(input, f)) ;
        Eigen::Matrix4d jacCO4(TypeCO4::get(input, f)) ;
        Eigen::Matrix4d jacFO2(TypeFO2::get(input, f)) ;
        Eigen::Matrix4d jacFO4(TypeFO4::get(input, f)) ;
        Eigen::Matrix4d jacBO2(TypeBO2::get(input, f)) ;
        Eigen::Matrix4d jacBO4(TypeBO4::get(input, f)) ;
        Eigen::Matrix4d comp(Eigen::Matrix4d::Identity()) ;
        comp *= 2. ;
        CheckEigenType(jacCO2, comp, 1.e-5) ;
        CheckEigenType(jacCO4, comp, 1.e-5) ;
        CheckEigenType(jacFO2, comp, 1.e-5) ;
        CheckEigenType(jacFO4, comp, 1.e-5) ;
        CheckEigenType(jacBO2, comp, 1.e-5) ;
        CheckEigenType(jacBO4, comp, 1.e-5) ;
    }
}

void FD1_Discrete(void)
{
    typedef FiniteDifference<FDScheme::CENTRAL, IthDerivative::FIRST, 2,
                             FDContext::DISCRETE> TypeCO2 ;
    typedef FiniteDifference<FDScheme::CENTRAL, IthDerivative::FIRST, 4,
                             FDContext::DISCRETE> TypeCO4 ;
    typedef FiniteDifference<FDScheme::FORWARD, IthDerivative::FIRST, 2,
                             FDContext::DISCRETE> TypeFO2 ;
    typedef FiniteDifference<FDScheme::FORWARD, IthDerivative::FIRST, 4,
                             FDContext::DISCRETE> TypeFO4 ;
    typedef FiniteDifference<FDScheme::BACKWARD, IthDerivative::FIRST, 2,
                             FDContext::DISCRETE> TypeBO2 ;
    typedef FiniteDifference<FDScheme::BACKWARD, IthDerivative::FIRST, 4,
                             FDContext::DISCRETE> TypeBO4 ;
    {
        Eigen::VectorXd fx(10) ;
        fx << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10. ;
        Eigen::VectorXd resCO2(TypeCO2::get(fx, 2.)) ;
        Eigen::VectorXd resCO4(TypeCO4::get(fx, 2.)) ;
        Eigen::VectorXd resFO2(TypeFO2::get(fx, 2.)) ;
        Eigen::VectorXd resFO4(TypeFO4::get(fx, 2.)) ;
        Eigen::VectorXd resBO2(TypeBO2::get(fx, 2.)) ;
        Eigen::VectorXd resBO4(TypeBO4::get(fx, 2.)) ;
        Eigen::VectorXd comp(Eigen::VectorXd::Ones(10)) ;
        comp /= 2. ;
        CheckEigenType(resCO2, comp, 1.e-5) ;
        CheckEigenType(resCO4, comp, 1.e-5) ;
        CheckEigenType(resFO2, comp, 1.e-5) ;
        CheckEigenType(resFO4, comp, 1.e-5) ;
        CheckEigenType(resBO2, comp, 1.e-5) ;
        CheckEigenType(resBO4, comp, 1.e-5) ;
    }
    {
        Eigen::MatrixXd fx(10, 2) ;
        fx << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.,
              11., 12., 13., 14., 15., 16., 17., 18., 19., 20. ;
        Eigen::MatrixXd resCO2(TypeCO2::get(fx, 2.)) ;
        Eigen::MatrixXd resCO4(TypeCO4::get(fx, 2.)) ;
        Eigen::MatrixXd resFO2(TypeFO2::get(fx, 2.)) ;
        Eigen::MatrixXd resFO4(TypeFO4::get(fx, 2.)) ;
        Eigen::MatrixXd resBO2(TypeBO2::get(fx, 2.)) ;
        Eigen::MatrixXd resBO4(TypeBO4::get(fx, 2.)) ;
        Eigen::MatrixXd comp(Eigen::MatrixXd::Ones(10, 2)) ;
        CheckEigenType(resCO2, comp, 1.e-5) ;
        CheckEigenType(resCO4, comp, 1.e-5) ;
        CheckEigenType(resFO2, comp, 1.e-5) ;
        CheckEigenType(resFO4, comp, 1.e-5) ;
        CheckEigenType(resBO2, comp, 1.e-5) ;
        CheckEigenType(resBO4, comp, 1.e-5) ;
    }
    {
        Eigen::MatrixXd fx(10, 2) ;
        fx << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10.,
              11., 12., 13., 14., 15., 16., 17., 18., 19., 20. ;
        Eigen::MatrixXd resCO2(TypeCO2::get(fx.block(0, 0, 5, 2), 2.)) ;
        Eigen::MatrixXd resCO4(TypeCO4::get(fx.block(0, 0, 9, 2), 2.)) ;
        Eigen::MatrixXd resFO2(TypeFO2::get(fx.block(0, 0, 5, 2), 2.)) ;
        Eigen::MatrixXd resFO4(TypeFO4::get(fx.block(0, 0, 8, 2), 2.)) ;
        Eigen::MatrixXd resBO2(TypeBO2::get(fx.block(0, 0, 5, 2), 2.)) ;
        Eigen::MatrixXd resBO4(TypeBO4::get(fx.block(0, 0, 8, 2), 2.)) ;
        Eigen::MatrixXd comp(Eigen::MatrixXd::Ones(10, 2)) ;
        CheckEigenType(resCO2, comp.block(0, 0, 5, 2), 1.e-5) ;
        CheckEigenType(resCO4, comp.block(0, 0, 9, 2), 1.e-5) ;
        CheckEigenType(resFO2, comp.block(0, 0, 5, 2), 1.e-5) ;
        CheckEigenType(resFO4, comp.block(0, 0, 8, 2), 1.e-5) ;
        CheckEigenType(resBO2, comp.block(0, 0, 5, 2), 1.e-5) ;
        CheckEigenType(resBO4, comp.block(0, 0, 8, 2), 1.e-5) ;
    }

}

} // End of namespace Tests
} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV
