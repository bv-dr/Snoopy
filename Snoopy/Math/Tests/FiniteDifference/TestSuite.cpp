#include <boost/test/unit_test.hpp>

#include "Tests/FiniteDifference/FirstDerivative.hpp"
#include "Tests/FiniteDifference/SecondDerivative.hpp"

using namespace boost::unit_test ;

#define BV_ADD_FINITE_DIFFERENCE_TEST( testName )                         \
    FiniteDifferenceTestSuite->add(BOOST_TEST_CASE( &(                    \
                            BV::Math::FiniteDifference::Tests::testName   \
                                                     ) ) ) ;

test_suite * init_unit_test_suite(int argc, char* argv[])
{
    test_suite * FiniteDifferenceTestSuite = BOOST_TEST_SUITE("FiniteDifferenceTestSuite") ;

    BV_ADD_FINITE_DIFFERENCE_TEST(FD1_Function)
    BV_ADD_FINITE_DIFFERENCE_TEST(FD1_Jacobian)
    BV_ADD_FINITE_DIFFERENCE_TEST(FD1_Discrete)
    BV_ADD_FINITE_DIFFERENCE_TEST(FD2_Function)
    BV_ADD_FINITE_DIFFERENCE_TEST(FD2_Hessian)

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV Finite Difference test suite" ;
    framework::master_test_suite().add(FiniteDifferenceTestSuite) ;

  return 0 ;
}

#undef BV_ADD_FINITE_DIFFERENCE_TEST
