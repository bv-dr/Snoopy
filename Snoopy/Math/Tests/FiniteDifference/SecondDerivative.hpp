#ifndef BV_Math_FiniteDifference_Tests_SecondDerivative_hpp
#define BV_Math_FiniteDifference_Tests_SecondDerivative_hpp

namespace BV {
namespace Math {
namespace FiniteDifference {
namespace Tests {

void FD2_Function(void) ;
void FD2_Hessian(void) ;

} // End of namespace Tests
} // End of namespace FiniteDifference
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_FiniteDifference_Tests_SecondDerivative_hpp
