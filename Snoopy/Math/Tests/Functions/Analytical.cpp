#include "Tests/Functions/Analytical.hpp"
#include "Math/Functions/Analytical.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include <unsupported/Eigen/CXX11/Tensor>

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

void testR1ToR1Analytical(void)
{
    // Standard formula
    std::string formula("2.*x+5.") ;
    std::string variable("x") ;
    {
        Analytical<1, 1> func(formula, variable) ;
        BOOST_CHECK_CLOSE(func.eval(2.), 9., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        BOOST_CHECK_CLOSE(func.dEval(3.), 2., 1.e-6) ; // First derivative
        BOOST_CHECK_SMALL(func.dEval2(3.), 1.e-4) ; // Second derivative
    }
    {
        // Same formula but specifying the analytical first derivative
        Analytical<1, 1> firstDer("2.", variable) ;
        Analytical<1, 1> func(formula, variable, firstDer) ;
        BOOST_CHECK_CLOSE(func.eval(2.), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(func.dEval(3.), 2., EPSILON) ; // First derivative
        BOOST_CHECK_SMALL(func.dEval2(3.), 1.e-4) ; // Second derivative FD
    }
    {
        // Same formula but with all the analytical derivatives (1st and 2nd)
        Analytical<1, 1> secondDer("0.", variable) ;
        Analytical<1, 1> firstDer("2.", variable, secondDer) ;
        Analytical<1, 1> func(formula, variable, firstDer) ;
        BOOST_CHECK_CLOSE(func.eval(2.), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(func.dEval(3.), 2., EPSILON) ; // First derivative
        BOOST_CHECK_SMALL(func.dEval2(3.), EPSILON) ; // Second derivative
    }
    {
        // Formula with another formula embedded
        std::shared_ptr<ABC<1, 1> > p_func(
                    std::make_shared<Analytical<1, 1> >(formula, variable)
                                          ) ;
        Analytical<1, 1> func2("3.*x+f(x)", variable) ;
        func2.addFunctor("f", p_func) ;
        BOOST_CHECK_CLOSE(func2.eval(2.), 15., EPSILON) ;
        BOOST_CHECK_CLOSE(func2.dEval(3.), 5., 1.e-5) ; // First derivative FD
        BOOST_CHECK_SMALL(func2.dEval2(3.), 1.e-4) ; // Second derivative FD
    }
    {
        std::shared_ptr<ABC<1, 1> > p_func(
                    std::make_shared<Analytical<1, 1> >(formula, variable)
                                          ) ;
        // Same formula with analytical derivatives
        Analytical<1, 1> secondDer("0.", variable) ;
        Analytical<1, 1> firstDer("5.", variable, secondDer) ;
        Analytical<1, 1> func2("3.*x+f(x)", variable, firstDer) ;
        func2.addFunctor("f", p_func) ;
        BOOST_CHECK_CLOSE(func2.eval(2.), 15., EPSILON) ;
        BOOST_CHECK_CLOSE(func2.dEval(3.), 5., EPSILON) ; // First derivative
        BOOST_CHECK_SMALL(func2.dEval2(3.), EPSILON) ; // Second derivative
    }
    {
        // New formula with 2 levels of embedded functions
        std::shared_ptr<ABC<1, 1> > p_func(
                    std::make_shared<Analytical<1, 1> >(formula, variable)
                                          ) ;
        std::shared_ptr<Analytical<1, 1> > p_func2(
                    std::make_shared<Analytical<1, 1> >("3.*x+f(x)", variable)
                                                  ) ;
        p_func2->addFunctor("f", p_func) ;
        Analytical<1, 1> secondDer("8.", variable) ;
        Analytical<1, 1> firstDer("5.+8.*x", variable, secondDer) ;
        Analytical<1, 1> func3("4.*x**2+g(x)", variable, firstDer) ;
        func3.addFunctor("g", p_func2) ;
        BOOST_CHECK_CLOSE(func3.eval(2.), 31., EPSILON) ;
        BOOST_CHECK_CLOSE(func3.dEval(3.), 29., EPSILON) ; // First derivative
        BOOST_CHECK_CLOSE(func3.dEval2(3.), 8., EPSILON) ; // Second derivative
    }
}

void testR2ToR1Analytical(void)
{
    // Standard formula
    std::string formula("2.*x+5.*y") ;
    std::array<std::string, 2> variables {"x", "y"} ;
    {
        Analytical<2, 1> func(formula, variables) ;
        BOOST_CHECK_CLOSE(func.eval(2., 1.), 9., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        Eigen::Vector2d der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 2., 1.e-6) ;
        BOOST_CHECK_CLOSE(der(1), 5., 1.e-5) ;
        // Second derivative is not calculated numerically for the moment,
        // we have to provide it analytically.
        //BOOST_CHECK_SMALL(func.dEval2(3.), 1.e-4) ; // Second derivative
    }
    {
        // Same formula but specifying the analytical first derivative
        Analytical<2, 2> firstDer({"2.", "5."}, variables) ;
        Analytical<2, 1> func(formula, variables, firstDer) ;
        BOOST_CHECK_CLOSE(func.eval(2., 1.), 9., EPSILON) ;
        Eigen::Vector2d der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1), 5., EPSILON) ;
    }
    {
        // Same formula but with all the analytical derivatives (1st and 2nd)
        Analytical<2, 2> d2fx({"0.", "0."}, variables) ;
        Analytical<2, 2> d2fy({"0.", "0."}, variables) ;
        Analytical<2, 2> df({"2.", "5."}, variables, {d2fx, d2fy}) ;
        Analytical<2, 1> func(formula, variables, df) ;
        BOOST_CHECK_CLOSE(func.eval(2., 1.), 9., EPSILON) ;
        Eigen::Vector2d der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1), 5., EPSILON) ;
        Eigen::Matrix2d der2(func.dEval2(3., 1.)) ; // Second derivative
        BOOST_CHECK_SMALL(der2(0), EPSILON) ;
        BOOST_CHECK_SMALL(der2(1), EPSILON) ;
        BOOST_CHECK_SMALL(der2(2), EPSILON) ;
        BOOST_CHECK_SMALL(der2(3), EPSILON) ;
    }
    {
        std::shared_ptr<ABC<1, 1> > p_func(
                    std::make_shared<Analytical<1, 1> >("3.*x+1", "x")
                                          ) ;
        std::shared_ptr<Analytical<2, 1> > p_func2(
                    std::make_shared<Analytical<2, 1> >("f(x)+y", variables)
                                                  ) ;
        p_func2->addFunctor("f", p_func) ;
        // Same formula with analytical derivatives
        Analytical<2, 2> d2fx({"0.", "0."}, variables) ;
        Analytical<2, 2> d2fy({"0.", "0."}, variables) ;
        Analytical<2, 2> firstDer({"3.", "1."}, variables, {d2fx, d2fy}) ;
        Analytical<2, 1> func("2.+g(x, y)", variables, firstDer) ;
        func.addFunctor("g", p_func2) ;
        BOOST_CHECK_CLOSE(func.eval(2., 1.), 10., EPSILON) ;
        Eigen::Vector2d der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 3., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1), 1., EPSILON) ;
        Eigen::Matrix2d der2(func.dEval2(3., 1.)) ; // Second derivative
        BOOST_CHECK_SMALL(der2(0), EPSILON) ;
        BOOST_CHECK_SMALL(der2(1), EPSILON) ;
        BOOST_CHECK_SMALL(der2(2), EPSILON) ;
        BOOST_CHECK_SMALL(der2(3), EPSILON) ;
    }
}

void testR1ToR2Analytical(void)
{
    // Standard formula
    std::array<std::string, 2> formulae {"2.*x+5.", "3.*x+4."} ;
    std::string variable("x") ;
    {
        Analytical<1, 2> func(formulae, variable) ;
        Eigen::Vector2d res(func.eval(2.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 10., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        Eigen::Vector2d der(func.dEval(3.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 2., 1.e-6) ;
        BOOST_CHECK_CLOSE(der(1), 3., 1.e-5) ;
        // Second derivative is not calculated numerically for the moment,
        // we have to provide it analytically.
        //BOOST_CHECK_SMALL(func.dEval2(3.), 1.e-4) ; // Second derivative
    }
    {
        // Same formula but specifying the analytical first derivative
        Analytical<1, 1> df1({"2."}, {variable}) ;
        Analytical<1, 1> df2({"3."}, {variable}) ;
        Analytical<1, 2> func(formulae, variable, {df1, df2}) ;
        Eigen::Vector2d res(func.eval(2.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 10., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        Eigen::Vector2d der(func.dEval(3.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1), 3., EPSILON) ;
    }
    {
        // Same formula but with all the analytical derivatives (1st and 2nd)
        Analytical<1, 1> d2f1({"0."}, {variable}) ;
        Analytical<1, 1> d2f2({"0."}, {variable}) ;
        Analytical<1, 1> df1({"2."}, {variable}, d2f1) ;
        Analytical<1, 1> df2({"3."}, {variable}, d2f2) ;
        Analytical<1, 2> func(formulae, variable, {df1, df2}) ;
        Eigen::Vector2d res(func.eval(2.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 10., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        Eigen::Vector2d der(func.dEval(3.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1), 3., EPSILON) ;
        Eigen::Vector2d der2(func.dEval2(3.)) ; // First derivative
        BOOST_CHECK_SMALL(der2(0), EPSILON) ;
        BOOST_CHECK_SMALL(der2(1), EPSILON) ;
    }
    {
        std::shared_ptr<ABC<1, 1> > p_func(
                    std::make_shared<Analytical<1, 1> >("3.*x+1", "x")
                                          ) ;
        Analytical<1, 1> d2f1({"0."}, {variable}) ;
        Analytical<1, 1> d2f2({"0."}, {variable}) ;
        Analytical<1, 1> df1({"3."}, {variable}, d2f1) ;
        Analytical<1, 1> df2({"1."}, {variable}, d2f2) ;
        Analytical<1, 2> func({"2.+g(x)", "x+12."}, {variable}, {df1, df2}) ;
        func.addFunctor("g", p_func) ;
        Eigen::Vector2d res(func.eval(2.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 14., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        Eigen::Vector2d der(func.dEval(3.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0), 3., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1), 1., EPSILON) ;
        Eigen::Vector2d der2(func.dEval2(3.)) ; // First derivative
        BOOST_CHECK_SMALL(der2(0), EPSILON) ;
        BOOST_CHECK_SMALL(der2(1), EPSILON) ;
    }
}

void testR2ToR3Analytical(void)
{
    // Standard formula
    std::array<std::string, 3> formulae {"2.*x+5.*y", "3.*x+4.*y", "4.*x+3.*y"} ;
    std::array<std::string, 2> variables {"x", "y"} ;
    {
        Analytical<2, 3> func(formulae, variables) ;
        Eigen::Vector3d res(func.eval(2., 1.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 10., EPSILON) ;
        BOOST_CHECK_CLOSE(res(2), 11., EPSILON) ;
        // Finite difference derivative by default, not very precise...
        Eigen::Matrix<double, 3, 2> der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0, 0), 2., 1.e-5) ;
        BOOST_CHECK_CLOSE(der(0, 1), 5., 1.e-5) ;
        BOOST_CHECK_CLOSE(der(1, 0), 3., 1.e-5) ;
        BOOST_CHECK_CLOSE(der(1, 1), 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(der(2, 0), 4., 1.e-5) ;
        BOOST_CHECK_CLOSE(der(2, 1), 3., 1.e-5) ;
        // Second derivative is not calculated numerically for the moment,
        // we have to provide it analytically.
        //BOOST_CHECK_SMALL(func.dEval2(3.), 1.e-4) ; // Second derivative
    }
    {
        // Same function with analytical first derivative
        Analytical<2, 2> df1({"2.", "5."}, variables) ;
        Analytical<2, 2> df2({"3.", "4."}, variables) ;
        Analytical<2, 2> df3({"4.", "3."}, variables) ;
        Analytical<2, 3> func(formulae, variables, {df1, df2, df3}) ;
        Eigen::Vector3d res(func.eval(2., 1.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 10., EPSILON) ;
        BOOST_CHECK_CLOSE(res(2), 11., EPSILON) ;
        Eigen::Matrix<double, 3, 2> der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0, 0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(0, 1), 5., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1, 0), 3., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1, 1), 4., EPSILON) ;
        BOOST_CHECK_CLOSE(der(2, 0), 4., EPSILON) ;
        BOOST_CHECK_CLOSE(der(2, 1), 3., EPSILON) ;
        Eigen::Tensor<double, 3> der2(func.dEval2(3., 1.)) ;
        BOOST_CHECK_SMALL((double)der2(0, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 1, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 1, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 1, 1), 1.e-5) ;
    }
    {
        // Same function with analytical first and second derivatives
        Analytical<2, 2> d2f1_1({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f1_2({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f2_1({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f2_2({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f3_1({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f3_2({"0.", "0."}, variables) ;
        Analytical<2, 2> df1({"2.", "5."}, variables, {d2f1_1, d2f1_2}) ;
        Analytical<2, 2> df2({"3.", "4."}, variables, {d2f2_1, d2f2_2}) ;
        Analytical<2, 2> df3({"4.", "3."}, variables, {d2f3_1, d2f3_2}) ;
        Analytical<2, 3> func(formulae, variables, {df1, df2, df3}) ;
        Eigen::Vector3d res(func.eval(2., 1.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 10., EPSILON) ;
        BOOST_CHECK_CLOSE(res(2), 11., EPSILON) ;
        Eigen::Matrix<double, 3, 2> der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0, 0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(0, 1), 5., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1, 0), 3., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1, 1), 4., EPSILON) ;
        BOOST_CHECK_CLOSE(der(2, 0), 4., EPSILON) ;
        BOOST_CHECK_CLOSE(der(2, 1), 3., EPSILON) ;
        Eigen::Tensor<double, 3> der2(func.dEval2(3., 1.)) ;
        BOOST_CHECK_SMALL((double)der2(0, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 1, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 1, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 1, 1), 1.e-5) ;
    }
    {
        // Same function with analytical first and second derivatives
        // and functors
        std::shared_ptr<ABC<1, 1> > p_func(
                    std::make_shared<Analytical<1, 1> >("3.*y+2.", "y")
                                          ) ;
        std::shared_ptr<ABC<2, 1> > p_func2(
                    std::make_shared<Analytical<2, 1> >("3.*y+4.*x", variables)
                                           ) ;
        Analytical<2, 2> d2f1_1({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f1_2({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f2_1({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f2_2({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f3_1({"0.", "0."}, variables) ;
        Analytical<2, 2> d2f3_2({"0.", "0."}, variables) ;
        Analytical<2, 2> df1({"2.", "3."}, variables, {d2f1_1, d2f1_2}) ;
        Analytical<2, 2> df2({"4.", "7."}, variables, {d2f2_1, d2f2_2}) ;
        Analytical<2, 2> df3({"0.", "0."}, variables, {d2f3_1, d2f3_2}) ;
        Analytical<2, 3> func({"2*x+f(y)", "4.*y+g(x, y)", "2."}, variables,
                              {df1, df2, df3}) ;
        func.addFunctor("f", p_func) ;
        func.addFunctor("g", p_func2) ;
        Eigen::Vector3d res(func.eval(2., 1.)) ;
        BOOST_CHECK_CLOSE(res(0), 9., EPSILON) ;
        BOOST_CHECK_CLOSE(res(1), 15., EPSILON) ;
        BOOST_CHECK_CLOSE(res(2), 2., EPSILON) ;
        Eigen::Matrix<double, 3, 2> der(func.dEval(3., 1.)) ; // First derivative
        BOOST_CHECK_CLOSE(der(0, 0), 2., EPSILON) ;
        BOOST_CHECK_CLOSE(der(0, 1), 3., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1, 0), 4., EPSILON) ;
        BOOST_CHECK_CLOSE(der(1, 1), 7., EPSILON) ;
        BOOST_CHECK_CLOSE(der(2, 0), 0., EPSILON) ;
        BOOST_CHECK_CLOSE(der(2, 1), 0., EPSILON) ;
        Eigen::Tensor<double, 3> der2(func.dEval2(3., 1.)) ;
        BOOST_CHECK_SMALL((double)der2(0, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(0, 1, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(1, 1, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 0, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 0, 1), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 1, 0), 1.e-5) ;
        BOOST_CHECK_SMALL((double)der2(2, 1, 1), 1.e-5) ;
    }
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
