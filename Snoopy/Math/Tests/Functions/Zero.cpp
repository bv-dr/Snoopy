#include "Tests/Functions/Zero.hpp"
#include "Math/Functions/Zero.hpp"

#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

void testR1ToR1Zero(void)
{
    Zero<1, 1> zero ;
    BOOST_CHECK_CLOSE(zero.eval(3.), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(zero.dEval(3.), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(zero.dEval2(3.), 0., EPSILON) ;
}

void testR2ToR1Zero(void)
{
    Zero<2, 1> zero ;
    BOOST_CHECK_CLOSE(zero.eval(3., 4.), 0., EPSILON) ;
    Eigen::Vector2d der(zero.dEval(3., 4.)) ;
    BOOST_CHECK_CLOSE(der(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1), 0., EPSILON) ;
    Eigen::Matrix2d der2(zero.dEval2(3., 4.)) ;
    BOOST_CHECK_CLOSE(der2(0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(1, 1), 0., EPSILON) ;
}

void testR1ToR2Zero(void)
{
    Zero<1, 2> zero ;
    Eigen::Vector2d res(zero.eval(1.)) ;
    BOOST_CHECK_CLOSE(res(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(res(1), 0., EPSILON) ;
    Eigen::Vector2d der(zero.dEval(1.)) ;
    BOOST_CHECK_CLOSE(der(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1), 0., EPSILON) ;
    Eigen::Vector2d der2(zero.dEval(1.)) ;
    BOOST_CHECK_CLOSE(der2(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(1), 0., EPSILON) ;
}

void testR2ToR3Zero(void)
{
    Zero<2, 3> zero ;
    Eigen::Vector3d res(zero.eval(5., 6.)) ;
    BOOST_CHECK_CLOSE(res(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(res(1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(res(2), 0., EPSILON) ;
    Eigen::Matrix<double, 3, 2> der(zero.dEval(5., 6.)) ;
    BOOST_CHECK_CLOSE(der(0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(2, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(2, 1), 0., EPSILON) ;
    Eigen::Tensor<double, 3> der2(zero.dEval2(5., 6.)) ;
    BOOST_CHECK_CLOSE((double)der2(0, 0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(0, 0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(0, 1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(0, 1, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 1, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 1, 1), 0., EPSILON) ;
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
