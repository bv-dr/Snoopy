#ifndef BV_Math_Functions_Tests_ExpressionEvaluator_hpp
#define BV_Math_Functions_Tests_ExpressionEvaluator_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void literal1(void) ;
void literal2(void) ;
void literal3(void) ;
void literal4(void) ;
void literal5(void) ;
void literal6(void) ;
void literal7(void) ;
void literal8(void) ;
void literal9(void) ;

void basicop1(void) ;
void basicop2(void) ;
void basicop3(void) ;
void basicop4(void) ;
void basicop5(void) ;

void pemdas1(void) ;
void pemdas2(void) ;
void pemdas3(void) ;
void pemdas4(void) ;

void unary1(void) ;
void unary2(void) ;
void unary3(void) ;
void unary4(void) ;

void func_abs(void) ;
void func_acos(void) ;
void func_asin(void) ;
void func_atan(void) ;
void func_ceil(void) ;
void func_cos(void) ;
void func_cosh(void) ;
void func_exp(void) ;
void func_floor(void) ;
void func_log(void) ;
void func_log10(void) ;
void func_sin(void) ;
void func_sinh(void) ;
void func_sqrt(void) ;
void func_tan(void) ;
void func_tanh(void) ;

void func_pow(void) ;
void func_max(void) ;
void func_min(void) ;
void func_atan2(void) ;

//void parse_inf(void) ;
//void parse_infinity(void) ;
//void parse_nan(void) ;

void testVariable(void) ;
void test2Variables(void) ;
void testFunction(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Tests_ExpressionEvaluator_hpp
