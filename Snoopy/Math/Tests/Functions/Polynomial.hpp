#ifndef BV_Math_Functions_Test_Polynomial_hpp
#define BV_Math_Functions_Test_Polynomial_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testR1ToR1Polynomial(void) ;
void testR1ToR2Polynomial(void) ;
void testR1ToR3Polynomial(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_Polynomial_hpp
