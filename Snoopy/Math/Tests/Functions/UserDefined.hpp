#ifndef BV_Math_Functions_Test_UserDefined_hpp
#define BV_Math_Functions_Test_UserDefined_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testUserDefined(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_UserDefined_hpp
