#include <boost/test/unit_test.hpp>

#include "Tests/Functions/Composite.hpp"
#include "Tests/Functions/Discrete.hpp"
#include "Tests/Functions/ExpressionEvaluator.hpp"
#include "Tests/Functions/Polynomial.hpp"
#include "Tests/Functions/Uniform.hpp"
#include "Tests/Functions/UserDefined.hpp"
#include "Tests/Functions/Zero.hpp"

using namespace boost::unit_test ;

#define BV_ADD_FUNCTIONS_TEST( testName )                          \
    FunctionsTestSuite->add(BOOST_TEST_CASE( &(                    \
                            BV::Math::Functions::Tests::testName   \
                                              ) ) ) ;

test_suite * init_unit_test_suite(int argc, char* argv[])
{
    test_suite * FunctionsTestSuite = BOOST_TEST_SUITE("FunctionsTestSuite") ;

    BV_ADD_FUNCTIONS_TEST(literal1)
    BV_ADD_FUNCTIONS_TEST(literal2)
    BV_ADD_FUNCTIONS_TEST(literal3)
    BV_ADD_FUNCTIONS_TEST(literal4)
    BV_ADD_FUNCTIONS_TEST(literal5)
    BV_ADD_FUNCTIONS_TEST(literal6)
    BV_ADD_FUNCTIONS_TEST(literal7)
    BV_ADD_FUNCTIONS_TEST(literal8)
    BV_ADD_FUNCTIONS_TEST(literal9)
    BV_ADD_FUNCTIONS_TEST(basicop1)
    BV_ADD_FUNCTIONS_TEST(basicop2)
    BV_ADD_FUNCTIONS_TEST(basicop3)
    BV_ADD_FUNCTIONS_TEST(basicop4)
    BV_ADD_FUNCTIONS_TEST(basicop5)
    BV_ADD_FUNCTIONS_TEST(pemdas1)
    BV_ADD_FUNCTIONS_TEST(pemdas2)
    BV_ADD_FUNCTIONS_TEST(pemdas3)
    BV_ADD_FUNCTIONS_TEST(pemdas4)
    BV_ADD_FUNCTIONS_TEST(unary1)
    BV_ADD_FUNCTIONS_TEST(unary2)
    BV_ADD_FUNCTIONS_TEST(unary3)
    BV_ADD_FUNCTIONS_TEST(unary4)
    BV_ADD_FUNCTIONS_TEST(func_abs)
    BV_ADD_FUNCTIONS_TEST(func_acos)
    BV_ADD_FUNCTIONS_TEST(func_asin)
    BV_ADD_FUNCTIONS_TEST(func_atan)
    BV_ADD_FUNCTIONS_TEST(func_ceil)
    BV_ADD_FUNCTIONS_TEST(func_cos)
    BV_ADD_FUNCTIONS_TEST(func_cosh)
    BV_ADD_FUNCTIONS_TEST(func_exp)
    BV_ADD_FUNCTIONS_TEST(func_floor)
    BV_ADD_FUNCTIONS_TEST(func_log)
    BV_ADD_FUNCTIONS_TEST(func_log10)
    BV_ADD_FUNCTIONS_TEST(func_sin)
    BV_ADD_FUNCTIONS_TEST(func_sinh)
    BV_ADD_FUNCTIONS_TEST(func_sqrt)
    BV_ADD_FUNCTIONS_TEST(func_tan)
    BV_ADD_FUNCTIONS_TEST(func_tanh)
    BV_ADD_FUNCTIONS_TEST(func_pow)
    BV_ADD_FUNCTIONS_TEST(func_max)
    BV_ADD_FUNCTIONS_TEST(func_min)
    BV_ADD_FUNCTIONS_TEST(func_atan2)
    //BV_ADD_FUNCTIONS_TEST(parse_inf)
    //BV_ADD_FUNCTIONS_TEST(parse_infinity)
    //BV_ADD_FUNCTIONS_TEST(parse_nan)
    BV_ADD_FUNCTIONS_TEST(testVariable)
    BV_ADD_FUNCTIONS_TEST(test2Variables)
    BV_ADD_FUNCTIONS_TEST(testFunction)

    //BV_ADD_FUNCTIONS_TEST(testR1ToR1Analytical)
    //BV_ADD_FUNCTIONS_TEST(testR2ToR1Analytical)
    //BV_ADD_FUNCTIONS_TEST(testR1ToR2Analytical)
    //BV_ADD_FUNCTIONS_TEST(testR2ToR3Analytical)

    BV_ADD_FUNCTIONS_TEST(testR1ToR1Uniform)
    BV_ADD_FUNCTIONS_TEST(testR2ToR1Uniform)
    BV_ADD_FUNCTIONS_TEST(testR1ToR2Uniform)
    BV_ADD_FUNCTIONS_TEST(testR2ToR3Uniform)

    BV_ADD_FUNCTIONS_TEST(testR1ToR1Zero)
    BV_ADD_FUNCTIONS_TEST(testR2ToR1Zero)
    BV_ADD_FUNCTIONS_TEST(testR1ToR2Zero)
    BV_ADD_FUNCTIONS_TEST(testR2ToR3Zero)

    BV_ADD_FUNCTIONS_TEST(testR1ToR1Polynomial)
    BV_ADD_FUNCTIONS_TEST(testR1ToR2Polynomial)
    BV_ADD_FUNCTIONS_TEST(testR1ToR3Polynomial)

    BV_ADD_FUNCTIONS_TEST(testR1ToR1Discrete)
    BV_ADD_FUNCTIONS_TEST(testR1ToR2Discrete)
    BV_ADD_FUNCTIONS_TEST(testR2ToR1Discrete)
    BV_ADD_FUNCTIONS_TEST(testR2ToR2Discrete)

    BV_ADD_FUNCTIONS_TEST(testComposite)

    BV_ADD_FUNCTIONS_TEST(testUserDefined)

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV Functions test suite" ;
    framework::master_test_suite().add(FunctionsTestSuite) ;

  return 0 ;
}

#undef BV_ADD_FUNCTIONS_TEST
