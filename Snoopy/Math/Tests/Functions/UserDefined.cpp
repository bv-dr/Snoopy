#include "Tests/Functions/UserDefined.hpp"
#include "Math/Functions/UserDefined.hpp"

#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <functional>

#include "Tests/Functions/Tools.hpp"

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

Eigen::Vector2d TestFunc(const Eigen::Vector3d & input)
{
    Eigen::Vector2d tmp ;
    tmp << 2., 3. ;
    return tmp ;
}

struct TestFunctor
{
    static Eigen::Vector2d get(const Eigen::Vector3d & input)
    {
        Eigen::Vector2d tmp ;
        tmp << 2., 3. ;
        return tmp ;
    }

    Eigen::Vector2d operator()(const Eigen::Vector3d & input)
    {
        return get(input) ;
    }

    Eigen::Vector2d eval(const Eigen::Vector3d & input)
    {
        return get(input) ;
    }

    Eigen::Matrix<double, 2, 3> dEval(const Eigen::Vector3d & input)
    {
        Eigen::Matrix<double, 2, 3> tmp ;
        tmp << 1., 2., 3., 4., 5., 6. ;
        return tmp ;
    }

    Eigen::Tensor<double, 3> dEval2(const Eigen::Vector3d & input)
    {
        Eigen::Tensor<double, 3> tmp(2, 3, 3) ;
        tmp.setConstant(2.) ;
        return tmp ;
    }

} ;

void testUserDefined(void)
{
    Eigen::Vector3d input ;
    input << 1., 2., 3. ;
    Eigen::Vector2d comp ;
    comp << 2., 3. ;
    {
        UserDefined<3, 2> userDefined(TestFunc) ;
        Eigen::Vector2d res(userDefined.eval(input)) ;
        CheckEigenType(res, comp) ;
    }
    {
        TestFunctor func ;
        UserDefined<3, 2> userDefined(func) ;
        Eigen::Vector2d res(userDefined.eval(input)) ;
        CheckEigenType(res, comp) ;
    }
    {
        UserDefined<3, 2> userDefined(&TestFunctor::get) ;
        Eigen::Vector2d res(userDefined.eval(input)) ;
        CheckEigenType(res, comp) ;
    }
    {
        TestFunctor func ;
        UserDefined<3, 2> userDefined(std::bind(&TestFunctor::eval, func,
                                                std::placeholders::_1)) ;
        Eigen::Vector2d res(userDefined.eval(input)) ;
        CheckEigenType(res, comp) ;
    }
    {
        UserDefined<3, 2> userDefined(
                    [](const Eigen::Vector3d & input) -> Eigen::Vector2d
                    {
                        Eigen::Vector2d tmp ;
                        tmp << 2., 3. ;
                        return tmp ;
                    }
                                     ) ;
        Eigen::Vector2d res(userDefined.eval(input)) ;
        CheckEigenType(res, comp) ;
    }
    {
        TestFunctor func ;
        UserDefined<3, 2> userDefined(std::bind(&TestFunctor::eval, func,
                                                std::placeholders::_1),
                                      std::bind(&TestFunctor::dEval, func,
                                                std::placeholders::_1),
                                      std::bind(&TestFunctor::dEval2, func,
                                                std::placeholders::_1)) ;
        Eigen::Vector2d res(userDefined.eval(input)) ;
        CheckEigenType(res, comp) ;
        Eigen::Matrix<double, 2, 3> der(userDefined.dEval(input)) ;
        Eigen::Matrix<double, 2, 3> compDer ;
        compDer << 1., 2., 3., 4., 5., 6. ;
        CheckEigenType(der, compDer) ;
        Eigen::Tensor<double, 3> der2(userDefined.dEval2(input)) ;
        Eigen::Tensor<double, 3> compDer2(2, 3, 3) ;
        compDer2.setConstant(2.) ;
        TensorChecker<3>::check(der2, compDer2) ;
    }
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
