#ifndef BV_Math_Functions_Test_Discrete_hpp
#define BV_Math_Functions_Test_Discrete_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testR1ToR1Discrete(void) ;
void testR1ToR2Discrete(void) ;
void testR2ToR1Discrete(void) ;
void testR2ToR2Discrete(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_Discrete_hpp
