#include "Tests/Functions/Uniform.hpp"
#include "Math/Functions/Uniform.hpp"

#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

void testR1ToR1Uniform(void)
{
    Uniform<1, 1> uniform(2.) ;
    BOOST_CHECK_CLOSE(uniform.eval(3.), 2., EPSILON) ;
    BOOST_CHECK_CLOSE(uniform.dEval(3.), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(uniform.dEval2(3.), 0., EPSILON) ;
}

void testR2ToR1Uniform(void)
{
    Uniform<2, 1> uniform(2.) ;
    BOOST_CHECK_CLOSE(uniform.eval(3., 4.), 2., EPSILON) ;
    Eigen::Vector2d der(uniform.dEval(3., 4.)) ;
    BOOST_CHECK_CLOSE(der(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1), 0., EPSILON) ;
    Eigen::Matrix2d der2(uniform.dEval2(3., 4.)) ;
    BOOST_CHECK_CLOSE(der2(0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(1, 1), 0., EPSILON) ;
}

void testR1ToR2Uniform(void)
{
    Uniform<1, 2> uniform(3., 4.) ;
    Eigen::Vector2d res(uniform.eval(1.)) ;
    BOOST_CHECK_CLOSE(res(0), 3., EPSILON) ;
    BOOST_CHECK_CLOSE(res(1), 4., EPSILON) ;
    Eigen::Vector2d der(uniform.dEval(1.)) ;
    BOOST_CHECK_CLOSE(der(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1), 0., EPSILON) ;
    Eigen::Vector2d der2(uniform.dEval(1.)) ;
    BOOST_CHECK_CLOSE(der2(0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der2(1), 0., EPSILON) ;
}

void testR2ToR3Uniform(void)
{
    Uniform<2, 3> uniform(2., 3., 4.) ;
    Eigen::Vector3d res(uniform.eval(5., 6.)) ;
    BOOST_CHECK_CLOSE(res(0), 2., EPSILON) ;
    BOOST_CHECK_CLOSE(res(1), 3., EPSILON) ;
    BOOST_CHECK_CLOSE(res(2), 4., EPSILON) ;
    Eigen::Matrix<double, 3, 2> der(uniform.dEval(5., 6.)) ;
    BOOST_CHECK_CLOSE(der(0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(2, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE(der(2, 1), 0., EPSILON) ;
    Eigen::Tensor<double, 3> der2(uniform.dEval2(5., 6.)) ;
    BOOST_CHECK_CLOSE((double)der2(0, 0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(0, 0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(0, 1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(0, 1, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(1, 1, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 0, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 0, 1), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 1, 0), 0., EPSILON) ;
    BOOST_CHECK_CLOSE((double)der2(2, 1, 1), 0., EPSILON) ;
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
