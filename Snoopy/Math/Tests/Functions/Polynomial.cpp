#include "Tests/Functions/Polynomial.hpp"
#include "Math/Functions/Polynomial.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

void testR1ToR1Polynomial(void)
{
    Eigen::Vector3d coefs ;
    coefs << 1., 2., 3. ;
    Polynomial<1> polynomial(coefs) ;
    BOOST_CHECK_CLOSE(polynomial.eval(2.), 17., EPSILON) ;
    BOOST_CHECK_CLOSE(polynomial.dEval(2.), 14., EPSILON) ;
    BOOST_CHECK_CLOSE(polynomial.dEval2(2.), 6., EPSILON) ;
}

void testR1ToR2Polynomial(void)
{
    Eigen::Vector3d coefs1 ;
    coefs1 << 1., 2., 3. ;
    Eigen::Vector2d coefs2 ;
    coefs2 << 4., 5. ;
    Polynomial<2> polynomial(coefs1, coefs2) ;
    Eigen::Vector2d res(polynomial.eval(2.)) ;
    BOOST_CHECK_CLOSE(res(0), 17., EPSILON) ;
    BOOST_CHECK_CLOSE(res(1), 14., EPSILON) ;
    Eigen::Vector2d der(polynomial.dEval(2.)) ;
    BOOST_CHECK_CLOSE(der(0), 14., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1), 5., EPSILON) ;
    Eigen::Vector2d der2(polynomial.dEval2(2.)) ;
    BOOST_CHECK_CLOSE(der2(0), 6., EPSILON) ;
    BOOST_CHECK_SMALL(der2(1), EPSILON) ;
}

void testR1ToR3Polynomial(void)
{
    Eigen::Vector3d coefs1 ;
    coefs1 << 1., 2., 3. ;
    Eigen::Vector2d coefs2 ;
    coefs2 << 4., 5. ;
    Eigen::Vector4d coefs3 ;
    coefs3 << 1., 2., 3., 4. ;
    Polynomial<3> polynomial(coefs1, coefs2, coefs3) ;
    Eigen::Vector3d res(polynomial.eval(2.)) ;
    BOOST_CHECK_CLOSE(res(0), 17., EPSILON) ;
    BOOST_CHECK_CLOSE(res(1), 14., EPSILON) ;
    BOOST_CHECK_CLOSE(res(2), 49., EPSILON) ;
    Eigen::Vector3d der(polynomial.dEval(2.)) ;
    BOOST_CHECK_CLOSE(der(0), 14., EPSILON) ;
    BOOST_CHECK_CLOSE(der(1), 5., EPSILON) ;
    BOOST_CHECK_CLOSE(der(2), 62., EPSILON) ;
    Eigen::Vector3d der2(polynomial.dEval2(2.)) ;
    BOOST_CHECK_CLOSE(der2(0), 6., EPSILON) ;
    BOOST_CHECK_SMALL(der2(1), EPSILON) ;
    BOOST_CHECK_CLOSE(der2(2), 54., EPSILON) ;
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
