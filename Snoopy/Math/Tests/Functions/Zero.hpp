#ifndef BV_Math_Functions_Test_Zero_hpp
#define BV_Math_Functions_Test_Zero_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testR1ToR1Zero(void) ;
void testR2ToR1Zero(void) ;
void testR1ToR2Zero(void) ;
void testR2ToR3Zero(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_Zero_hpp
