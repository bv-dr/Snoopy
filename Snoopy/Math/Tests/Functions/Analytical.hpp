#ifndef BV_Math_Functions_Test_Analytical_hpp
#define BV_Math_Functions_Test_Analytical_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testR1ToR1Analytical(void) ;
void testR2ToR1Analytical(void) ;
void testR1ToR2Analytical(void) ;
void testR2ToR3Analytical(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_Analytical_hpp
