#ifndef BV_Math_Functions_Test_Uniform_hpp
#define BV_Math_Functions_Test_Uniform_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testR1ToR1Uniform(void) ;
void testR2ToR1Uniform(void) ;
void testR1ToR2Uniform(void) ;
void testR2ToR3Uniform(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_Uniform_hpp
