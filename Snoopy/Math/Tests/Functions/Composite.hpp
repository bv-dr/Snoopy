#ifndef BV_Math_Functions_Test_Composite_hpp
#define BV_Math_Functions_Test_Composite_hpp

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

void testComposite(void) ;

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_Functions_Test_Composite_hpp
