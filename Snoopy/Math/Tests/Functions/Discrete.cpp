#include "Tests/Functions/Discrete.hpp"
#include "Math/Functions/Discrete.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>

#include "Math/Interpolators/Interpolators.hpp"

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

void testR1ToR1Discrete(void)
{
    Eigen::VectorXd axis(10) ;
    axis << 1., 2., 3., 4., 5., 6., 7., 8., 9., 10. ;
    Eigen::VectorXd data(10) ;
    data << 11., 12., 13., 14., 15., 16., 17., 18., 19., 20. ;
    Discrete<1, 1, Interpolators::InterpScheme::LINEAR> discrete(axis, data) ;
    BOOST_CHECK_CLOSE(discrete.eval(1.), 11., EPSILON) ;
}

void testR1ToR2Discrete(void)
{
}

void testR2ToR1Discrete(void)
{
}

void testR2ToR2Discrete(void)
{
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
