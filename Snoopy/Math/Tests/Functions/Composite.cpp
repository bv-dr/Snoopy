#include "Tests/Functions/Composite.hpp"
#include "Math/Functions/Composite.hpp"
#include <boost/test/tools/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include "Tests/Functions/Tools.hpp"
#include "Math/Functions/Analytical.hpp"
#include "Math/Functions/Uniform.hpp"

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double EPSILON = 1.e-8 ;

void testComposite(void)
{
    Analytical<2, 3> analytical({"2.*x", "3.*y", "x+y"}, {"x", "y"}) ;
    Uniform<1, 2> uniform(2., 3.) ;
    Composite<Analytical<2, 3>, Uniform<1, 2> > composite(analytical, uniform) ;
    Eigen::Matrix<double, 5, 1> res(composite.eval(1., 2., 3.)) ;
    Eigen::Matrix<double, 5, 1> comp ;
    comp << 2., 6., 3., 2., 3. ;
    CheckEigenType(res, comp) ;
    Eigen::Matrix<double, 5, 3> der(composite.dEval(1., 2., 3.)) ;
    Eigen::Matrix<double, 5, 3> compDer ;
    compDer << 2., 0., 0., 0., 3., 0., 1., 1., 0., 0., 0., 0., 0., 0., 0. ;
    CheckEigenType(der, compDer) ;
    Eigen::Tensor<double, 3> der2(composite.dEval2(1., 2., 3.)) ;
    Eigen::Tensor<double, 3> compDer2(5, 3, 3) ;
    compDer2.setZero() ;
    TensorChecker<3>::check(der2, compDer2, 1.e-6) ;
}

} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
