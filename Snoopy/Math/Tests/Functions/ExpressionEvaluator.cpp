#include "Tests/Functions/ExpressionEvaluator.hpp"
#include <boost/test/unit_test.hpp>

#include "Math/Functions/Analytical.hpp"

namespace BV {
namespace Math {
namespace Functions {
namespace Tests {

const double close_enough = std::numeric_limits<double>::epsilon() ;

#define EXPRTEST(casename, expr, expected)                     \
void casename(void)                                            \
{                                                              \
    ExpressionParser<double, std::string::const_iterator> eg ; \
    const std::string s = expr;                                \
    std::string::const_iterator iter = s.begin();              \
    std::string::const_iterator end  = s.end();                \
    double result;                                             \
    eg.initialise() ;                                          \
    BOOST_REQUIRE(parse(iter, end, eg, result));               \
    BOOST_CHECK(iter == end);                                  \
    BOOST_CHECK_CLOSE(result, (expected), close_enough);       \
}

EXPRTEST(literal1, "1.234", 1.234)
EXPRTEST(literal2, "4.2e2", 420)
EXPRTEST(literal3, "5e-01", 0.5)
EXPRTEST(literal4, "-3",    -3)
EXPRTEST(literal5, "pi",       boost::math::constants::pi<double>())
EXPRTEST(literal6, "epsilon",  std::numeric_limits<double>::epsilon())
EXPRTEST(literal7, "digits",   std::numeric_limits<double>::digits)
EXPRTEST(literal8, "digits10", std::numeric_limits<double>::digits10)
EXPRTEST(literal9, "e",        boost::math::constants::e<double>())

EXPRTEST(basicop1, " 2 +\t3\n",   5)       // Whitespace ignored
EXPRTEST(basicop2, " 2 -\t3\n",  -1)
EXPRTEST(basicop3, " 2 *\t3\n",   6)
EXPRTEST(basicop4, " 2 /\t3\n",   2./3.)   // Double division
EXPRTEST(basicop5, " 2 ** 3\n",  8)

EXPRTEST(pemdas1,  "2*3+4*5",   2*3+4*5)
EXPRTEST(pemdas2,  "2*(3+4)*5", 2*(3+4)*5)
EXPRTEST(pemdas3,  "2**3+4",    std::pow(2,3)+4)
EXPRTEST(pemdas4,  "2**2**-3",  std::pow(2.,std::pow(2.,-3.)))

EXPRTEST(unary1,   "-(2)",      -2)
EXPRTEST(unary2,   "-(-2)",      2)
EXPRTEST(unary3,   "+(-2)",     -2)
EXPRTEST(unary4,   "+(+2)",      2)

EXPRTEST(func_abs,   "abs (-1.0)", std::abs(-1.0))
EXPRTEST(func_acos,  "acos( 1.0)", std::acos(1.0))
EXPRTEST(func_asin,  "asin( 1.0)", std::asin(1.0))
EXPRTEST(func_atan,  "atan( 1.0)", std::atan(1.0))
EXPRTEST(func_ceil,  "ceil( 0.5)", std::ceil(0.5))
EXPRTEST(func_cos,   "cos ( 1.0)", std::cos(1.0))
EXPRTEST(func_cosh,  "cosh( 1.0)", std::cosh(1.0))
EXPRTEST(func_exp,   "exp ( 1.0)", std::exp(1.0))
EXPRTEST(func_floor, "floor(0.5)", std::floor(0.5))
EXPRTEST(func_log,   "log ( 1.0)", std::log(1.0))
EXPRTEST(func_log10, "log10(0.5)", std::log10(0.5))
EXPRTEST(func_sin,   "sin ( 1.0)", std::sin(1.0))
EXPRTEST(func_sinh,  "sinh( 1.0)", std::sinh(1.0))
EXPRTEST(func_sqrt,  "sqrt( 1.0)", std::sqrt(1.0))
EXPRTEST(func_tan,   "tan ( 1.0)", std::tan(1.0))
EXPRTEST(func_tanh,  "tanh( 1.0)", std::tanh(1.0))

EXPRTEST(func_pow,   "pow  (2.0, 3.0)", std::pow(2.0,3.0))
EXPRTEST(func_max,   "max  (2.0, 3.0)", std::max(2.0,3.0))
EXPRTEST(func_min,   "min  (2.0, 3.0)", std::min(2.0,3.0))
EXPRTEST(func_atan2, "atan2(2.0, 3.0)", std::atan2(2.0,3.0))

//void parse_inf (void)
//{
//    ExpressionParser<double, std::string::const_iterator> eg ;
//    const std::string s = "inf";
//    std::string::const_iterator iter = s.begin();
//    std::string::const_iterator end = s.end();
//    double result;
//    BOOST_REQUIRE(parse(iter, end, eg, result));
//    BOOST_CHECK(iter == end);
//    BOOST_CHECK((boost::math::isinf)(result));
//}
//
//void parse_infinity(void)
//{
//    ExpressionParser<double, std::string::const_iterator> eg ;
//    const std::string s = "infinity";
//    std::string::const_iterator iter = s.begin();
//    std::string::const_iterator end = s.end();
//    double result;
//    BOOST_REQUIRE(parse(iter, end, eg, result));
//    BOOST_CHECK(iter == end);
//    BOOST_CHECK((boost::math::isinf)(result));
//}
//
//void parse_nan(void)
//{
//    ExpressionParser<double, std::string::const_iterator> eg ;
//    const std::string s = "nan";
//    std::string::const_iterator iter = s.begin();
//    std::string::const_iterator end = s.end();
//    double result;
//    BOOST_REQUIRE(parse(iter, end, eg, result));
//    BOOST_CHECK(iter == end);
//    BOOST_CHECK((boost::math::isnan)(result));
//}

void testVariable(void)
{
    //std::vector<std::string> varVect ;
    //varVect.push_back("x") ;
    ExpressionParser<double, std::string::const_iterator> eg2 ;
    eg2.addVariable("x") ;
    eg2.initialise() ;

    const std::string s = "2.*x+3" ;
    double result ;
    std::string::const_iterator iter = s.begin() ;
    std::string::const_iterator end = s.end() ;
    eg2.setVariable("x", 2.) ;
    BOOST_REQUIRE(parse(iter, end, eg2, result)) ;
    BOOST_CHECK(iter == end) ;
    BOOST_CHECK_CLOSE(result, 7., close_enough) ;
    eg2.setVariable("x", 3.) ;
    iter = s.begin() ;
    end = s.end() ;
    BOOST_REQUIRE(parse(iter, end, eg2, result)) ;
    BOOST_CHECK(iter == end) ;
    BOOST_CHECK_CLOSE(result, 9., close_enough) ;
}

void test2Variables(void)
{
    const std::string s = "2.*x+y" ;
    //std::vector<std::string> varVect ;
    //varVect.push_back("x") ;
    //varVect.push_back("y") ;
    ExpressionEvaluator<double> evaluator(s) ;// , varVect) ;
    evaluator.addVariable("x") ;
    evaluator.addVariable("y") ;
    evaluator.setVariable("x", 2.) ;
    evaluator.setVariable("y", 3.) ;
    BOOST_CHECK_CLOSE(evaluator.evaluate(), 7., close_enough) ;
    evaluator.setVariable("x", 3.) ;
    BOOST_CHECK_CLOSE(evaluator.evaluate(), 9., close_enough) ;
    BOOST_CHECK_CLOSE(evaluator.derivative("x"), 2., 1.e-6) ;
}

void testFunction(void)
{
    const std::string s = "2.*x+f(x)" ;
    //std::vector<std::string> varVect ;
    //varVect.push_back("x") ;
    //std::shared_ptr<R1ToR1::ABC> myFunc(new R1ToR1::Uniform(2.)) ;
    std::shared_ptr<ABC<1, 1> > myFunc(new Analytical<1, 1, double>("2.*x", "x")) ;
    ExpressionEvaluator<double> evaluator(s) ;//, varVect) ;
    evaluator.addVariable("x") ;
    evaluator.addFunctor("f", myFunc) ;
    evaluator.setVariable("x", 2.) ;
    BOOST_CHECK_CLOSE(evaluator.evaluate(), 8., close_enough) ;
    evaluator.setVariable("x", 3.) ;
    BOOST_CHECK_CLOSE(evaluator.evaluate(), 12., close_enough) ;
    BOOST_CHECK_CLOSE(evaluator.derivative("x"), 4., 1.e-6) ;
}


} // End of namespace Tests
} // End of namespace Functions
} // End of namespace Math
} // End of namespace BV
