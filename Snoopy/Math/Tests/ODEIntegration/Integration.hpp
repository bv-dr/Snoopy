#ifndef BV_Math_ODEIntegration_Tests_Integration_hpp
#define BV_Math_ODEIntegration_Tests_Integration_hpp

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

void testIntegration(void) ;
void testIntegrationObserver(void) ;
void testIntegrationObserverStopper(void) ;

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Tests_Integration_hpp
