#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>
#include <boost/test/unit_test.hpp>

//#include "Math/ODEIntegration/Tests/RungeKutta.hpp"
//#include "Math/ODEIntegration/Tests/EmbeddedRungeKutta.hpp"
#include "Tests/ODEIntegration/Comparison.hpp"
#include "Tests/ODEIntegration/Integration.hpp"

using namespace boost::unit_test ;

#define BV_ADD_ODEINTEGRATION_TEST( testName )                  \
    ODEIntegrationTestSuite->add(BOOST_TEST_CASE( &(            \
                      BV::Math::ODEIntegration::Tests::testName \
                                                ) ) ) ;

test_suite * init_unit_test_suite(int argc, char* argv[])
{
    test_suite * ODEIntegrationTestSuite = BOOST_TEST_SUITE("ODEIntegrationTestSuite") ;

    //BV_ADD_ODEINTEGRATION_TEST(testHarmonicOscillatorRK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testAdaptiveHarmonicOscillatorRK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testDampedHarmonicOscillatorRK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testAdaptiveDampedHarmonicOscillatorRK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testHarmonicOscillatorERK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testAdaptiveHarmonicOscillatorERK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testDampedHarmonicOscillatorERK) ;
    //BV_ADD_ODEINTEGRATION_TEST(testAdaptiveDampedHarmonicOscillatorERK) ;
    BV_ADD_ODEINTEGRATION_TEST(testHarmonicOscillator) ;
    BV_ADD_ODEINTEGRATION_TEST(testDampedHarmonicOscillator) ;
    BV_ADD_ODEINTEGRATION_TEST(testSin) ;
    BV_ADD_ODEINTEGRATION_TEST(testIntegration) ;
    BV_ADD_ODEINTEGRATION_TEST(testIntegrationObserver) ;
    BV_ADD_ODEINTEGRATION_TEST(testIntegrationObserverStopper) ;

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV ODEIntegration test suite" ;
    framework::master_test_suite().add( ODEIntegrationTestSuite ) ;

  return 0 ;
}

#undef BV_ADD_ODEINTEGRATION_TEST
