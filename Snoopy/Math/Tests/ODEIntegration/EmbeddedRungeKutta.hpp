#ifndef BV_Math_ODEIntegration_Tests_EmbeddedRungeKutta_hpp
#define BV_Math_ODEIntegration_Tests_EmbeddedRungeKutta_hpp

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

void testHarmonicOscillatorERK(void) ;
void testAdaptiveHarmonicOscillatorERK(void) ;
void testDampedHarmonicOscillatorERK(void) ;
void testAdaptiveDampedHarmonicOscillatorERK(void) ;

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Tests_EmbeddedRungeKutta_hpp
