#include "Tests/ODEIntegration/Comparison.hpp"

#include <boost/filesystem.hpp>
#include <iostream>
#include <Eigen/Dense>

#include "Tests/ODEIntegration/Tools.hpp"
#include "Math/Functions/Analytical.hpp"
#include "Math/ODEIntegration/Steppers/Steppers.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

namespace Details {

using namespace BV::Math::ODEIntegration::Steppers ;

template <typename Case>
void Compare(const Case & testCase,
             const std::string & name,
             std::string outputFolder="Math/ODEIntegration/Tests/Output",
             double xStart=0., double xEnd=1000., double xStep=1.)
{
    using BV::Math::ODEIntegration::Tests::Comparator ;

    typedef typename Case::OutputType State ;

    Comparator<Case> comparator(testCase, outputFolder, name, xStart, xEnd, xStep) ;

    comparator.template addStepper<Stepper<StepperScheme::EULER, State> >("Euler") ;
    comparator.template addStepper<Stepper<StepperScheme::HEUN, State> >("Heun") ;
    comparator.template addStepper<Stepper<StepperScheme::RK2, State> >("RK2") ;
    comparator.template addStepper<Stepper<StepperScheme::RK_RALSTON_2, State> >("RK_Ralston2") ;
    comparator.template addStepper<Stepper<StepperScheme::RK3, State> >("RK3") ;
    comparator.template addStepper<Stepper<StepperScheme::RK4, State> >("RK4") ;
    comparator.template addStepper<Stepper<StepperScheme::RK_RALSTON_4, State> >("RK_Ralston4") ;
    comparator.template addStepper<Stepper<StepperScheme::RK3_8, State> >("RK3_8") ;
    comparator.template addStepper<Stepper<StepperScheme::GILL4, State> >("Gill4") ;
    comparator.template addStepper<Stepper<StepperScheme::NYSTROM5, State> >("Nystrom5") ;
    comparator.template addStepper<Stepper<StepperScheme::BUTCHER6, State> >("Butcher6") ;
    comparator.template addStepper<Stepper<StepperScheme::VERNER8, State> >("Verner8") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_CASH_KARP54, State> >("ERK_Cash_Karp54") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_DOPRI5, State> >("ERK_Dopri5") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_DOPRI78, State> >("ERK_Dopri78") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_DOPRI87, State> >("ERK_Dopri87") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG34, State> >("ERK_Fehlberg34") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG43, State> >("ERK_Fehlberg43") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG45, State> >("ERK_Fehlberg45") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG54, State> >("ERK_Fehlberg54") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG56, State> >("ERK_Fehlberg56") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG65, State> >("ERK_Fehlberg65") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG78, State> >("ERK_Fehlberg78") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_FEHLBERG87, State> >("ERK_Fehlberg87") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER56, State> >("ERK_Verner56") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER65, State> >("ERK_Verner65") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER67, State> >("ERK_Verner67") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER76, State> >("ERK_Verner76") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER78, State> >("ERK_Verner78") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER87, State> >("ERK_Verner87") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER89, State> >("ERK_Verner89") ;
    comparator.template addStepper<Stepper<StepperScheme::ERK_VERNER98, State> >("ERK_Verner98") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_1, State> >("AdamsBashforth1") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_2, State> >("AdamsBashforth2") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_3, State> >("AdamsBashforth3") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_4, State> >("AdamsBashforth4") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_5, State> >("AdamsBashforth5") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_6, State> >("AdamsBashforth6") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_7, State> >("AdamsBashforth7") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_8, State> >("AdamsBashforth8") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_1, State> >("AdamsBashforthMoulton1") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_2, State> >("AdamsBashforthMoulton2") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_3, State> >("AdamsBashforthMoulton3") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_4, State> >("AdamsBashforthMoulton4") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_5, State> >("AdamsBashforthMoulton5") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_6, State> >("AdamsBashforthMoulton6") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_7, State> >("AdamsBashforthMoulton7") ;
    comparator.template addStepper<Stepper<StepperScheme::ADAMS_BASHFORTH_MOULTON_8, State> >("AdamsBashforthMoulton8") ;
    comparator.template addStepper<Stepper<StepperScheme::HPCG, State> >("HPCG") ;
    comparator.template addStepper<Stepper<StepperScheme::IMPLICIT_EULER, State> >("IEuler") ;
    comparator.template addStepper<Stepper<StepperScheme::IMPLICIT_RK2, State> >("IRK2") ;
    comparator.template addStepper<Stepper<StepperScheme::IMPLICIT_RK4, State> >("IRK4") ;
    comparator.template addStepper<Stepper<StepperScheme::CRANK_NICOLSON, State> >("CN") ;

    //comparator.template addStepper<AdaptiveStepper<StepperScheme::EULER, State> >("EulerAdapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::HEUN, State> >("HeunAdapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::RK2, State> >("RK2Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::RK_RALSTON_2, State> >("RK_Ralston2Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::RK3, State> >("RK3Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::RK4, State> >("RK4Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::RK_RALSTON_4, State> >("RK_Ralston4Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::RK3_8, State> >("RK3_8Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::GILL4, State> >("Gill4Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::NYSTROM5, State> >("Nystrom5Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::BUTCHER6, State> >("Butcher6Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::VERNER8, State> >("Verner8Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_CASH_KARP54, State> >("ERK_Cash_Karp54Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_DOPRI5, State> >("ERK_Dopri5Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_DOPRI78, State> >("ERK_Dopri78Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_DOPRI87, State> >("ERK_Dopri87Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG34, State> >("ERK_Fehlberg34Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG43, State> >("ERK_Fehlberg43Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG45, State> >("ERK_Fehlberg45Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG54, State> >("ERK_Fehlberg54Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG56, State> >("ERK_Fehlberg56Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG65, State> >("ERK_Fehlberg65Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG78, State> >("ERK_Fehlberg78Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_FEHLBERG87, State> >("ERK_Fehlberg87Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER56, State> >("ERK_Verner56Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER65, State> >("ERK_Verner65Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER67, State> >("ERK_Verner67Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER76, State> >("ERK_Verner76Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER78, State> >("ERK_Verner78Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER87, State> >("ERK_Verner87Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER89, State> >("ERK_Verner89Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::ERK_VERNER98, State> >("ERK_Verner98Adapt") ;
    //comparator.template addStepper<AdaptiveStepper<StepperScheme::HPCG, State> >("HPCGAdapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::IMPLICIT_EULER, State> >("IEulerAdapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::IMPLICIT_RK2, State> >("IRK2Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::IMPLICIT_RK4, State> >("IRK4Adapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::CRANK_NICOLSON, State> >("CNAdapt") ;
    comparator.template addStepper<AdaptiveStepper<StepperScheme::BULIRSCH_STOER, State> >("BSAdapt") ;
    comparator.template addStepper<Stepper<StepperScheme::GPS1, State> >("GPS1") ;
    comparator.template addStepper<Stepper<StepperScheme::GPS2, State> >("GPS2") ;
    comparator.template addStepper<Stepper<StepperScheme::GPS4, State> >("GPS4") ;
    comparator.template addStepper<Stepper<StepperScheme::GPS6, State> >("GPS6") ;
}

} // End of namespace Details

void testHarmonicOscillator(void)
{
    std::cout << "Start harmonic oscillator test" << std::endl ;
    HarmonicOscillator testCase(0.01, 1., 1., 0., 0.) ;
    Details::Compare(testCase, "HarmonicOscillator_001_1_1_0_0") ;
    std::cout << "End harmonic oscillator test" << std::endl ;
}

void testDampedHarmonicOscillator(void)
{
    std::cout << "Start damped harmonic oscillator test" << std::endl ;
    DampedHarmonicOscillator testCase(0.01, 1., 1., 0.2, 0., 0.) ;
    Details::Compare(testCase, "DampedHarmonicOscillator_001_1_1_02_0_0") ;
    std::cout << "End damped harmonic oscillator test" << std::endl ;
}

void testSin(void)
{
    std::cout << "Start stiff sin test" << std::endl ;
    using BV::Math::Functions::Analytical ;
    Analytical<1, 1> sinDer("2.*cos(0.5*x)", "x") ;
    Analytical<1, 1> sinFunc("4.*sin(0.5*x)", "x", sinDer) ;
    Functional<Analytical<1, 1> > testCase(sinFunc) ;
    Details::Compare(testCase, "Sin_A4_T4pi") ;
    std::cout << "End stiff sin test" << std::endl ;
}

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
