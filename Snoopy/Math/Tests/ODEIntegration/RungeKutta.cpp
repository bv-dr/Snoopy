#include "Tests/ODEIntegration/RungeKutta.hpp"

#include <boost/filesystem.hpp>
#include <iostream>
#include <chrono>

#include "Tests/ODEIntegration/Tools.hpp"
#include "Math/ODEIntegration/Steppers/Steppers.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

namespace Details {

template <int Scheme, typename TestCase>
void TestRK(const std::string& folderName, const std::string& fileName,
            const double & xStep, TestCase & testCase)
{
    typedef Steppers::Stepper<Scheme, Eigen::VectorXd> FactoryType ;
    typedef typename FactoryType::Type Stepper ;
    Stepper stepper(FactoryType::get()) ;
    std::string log(folderName + "/" + fileName) ;
    Launcher<Stepper, TestCase> launcher(stepper, testCase, log, false) ;
    double xStart(0.) ;
    double xEnd(200.) ;
    double h(xStep) ;
    std::size_t nSteps(0) ;
    launcher(xStart, xEnd, h, nSteps) ;
}

template <int Scheme, typename TestCase>
void TestAdaptRK(const std::string& folderName, const std::string& fileName,
                 TestCase & testCase)
{
    typedef Steppers::AdaptiveStepper<Scheme, Eigen::VectorXd> FactoryType ;
    typedef typename FactoryType::Type Stepper ;
    Stepper stepper(FactoryType::get(1.e-6, 1.e-6, 1., 0., 100.)) ;
    std::string log(folderName + "/" + fileName) ;
    Launcher<Stepper, TestCase> launcher(stepper, testCase, log, false) ;
    double xStart(0.) ;
    double xEnd(200.) ;
    double h(1.) ;
    std::size_t nSteps(0) ;
    launcher(xStart, xEnd, h, nSteps) ;
}

} // End of namespace Details

void testHarmonicOscillatorRK(void)
{
    std::cout << "HO RK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directory(dir) ;
    }
    HarmonicOscillator testCase(0.01, 1., 1., 0., 0.) ;
    using Steppers::StepperScheme ;
    Details::TestRK<StepperScheme::EULER>(folderName, "logEulerHO.dat", 0.01, testCase) ;
    Details::TestRK<StepperScheme::HEUN>(folderName, "logHeunHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK2>(folderName, "logRK2HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK_RALSTON_2>(folderName, "logRalston2HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK3>(folderName, "logRK3HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK4>(folderName, "logRK4HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK_RALSTON_4>(folderName, "logRalston4HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK3_8>(folderName, "logRK38HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::GILL4>(folderName, "logGill4HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::NYSTROM5>(folderName, "logNystrom5HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::BUTCHER6>(folderName, "logButcher6HO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::VERNER8>(folderName, "logVerner8HO.dat", 1., testCase) ;
}

void testAdaptiveHarmonicOscillatorRK(void)
{
    std::cout << "HO ARK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directory(dir) ;
    }
    HarmonicOscillator testCase(0.01, 1., 1., 0., 0.) ;
    using Steppers::StepperScheme ;
    Details::TestAdaptRK<StepperScheme::EULER>(folderName, "logEulerAHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::HEUN>(folderName, "logHeunAHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK2>(folderName, "logRK2AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK_RALSTON_2>(folderName, "logRalston2AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK3>(folderName, "logRK3AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK4>(folderName, "logRK4AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK_RALSTON_4>(folderName, "logRalston4AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK3_8>(folderName, "logRK38AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::GILL4>(folderName, "logGill4AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::NYSTROM5>(folderName, "logNystrom5AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::BUTCHER6>(folderName, "logButcher6AHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::VERNER8>(folderName, "logVerner8AHO.dat", testCase) ;
}

void testDampedHarmonicOscillatorRK(void)
{
    std::cout << "DHO RK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directory(dir) ;
    }
    using Steppers::StepperScheme ;
    DampedHarmonicOscillator testCase(0.01, 1., 1., 0.2, 0., 0.) ;
    Details::TestRK<StepperScheme::EULER>(folderName, "logFEDHO.dat", 0.01, testCase) ;
    Details::TestRK<StepperScheme::HEUN>(folderName, "logHeunDHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK2>(folderName, "logRK2DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK_RALSTON_2>(folderName, "logRalston2DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK3>(folderName, "logRK3DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK4>(folderName, "logRK4DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK_RALSTON_4>(folderName, "logRalston4DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::RK3_8>(folderName, "logRK38DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::GILL4>(folderName, "logGill4DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::NYSTROM5>(folderName, "logNystrom5DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::BUTCHER6>(folderName, "logButcher6DHO.dat", 1., testCase) ;
    Details::TestRK<StepperScheme::VERNER8>(folderName, "logVerner8DHO.dat", 1., testCase) ;
}

void testAdaptiveDampedHarmonicOscillatorRK(void)
{
    std::cout << "DHO ARK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directory(dir) ;
    }
    using Steppers::StepperScheme ;
    DampedHarmonicOscillator testCase(0.01, 1., 1., 0.2, 0., 0.) ;
    Details::TestAdaptRK<StepperScheme::EULER>(folderName, "logFEADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::HEUN>(folderName, "logHeunADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK2>(folderName, "logRK2ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK_RALSTON_2>(folderName, "logRalston2ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK3>(folderName, "logRK3ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK4>(folderName, "logRK4ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK_RALSTON_4>(folderName, "logRalston4ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::RK3_8>(folderName, "logRK38ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::GILL4>(folderName, "logGill4ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::NYSTROM5>(folderName, "logNystrom5ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::BUTCHER6>(folderName, "logButcher6ADHO.dat", testCase) ;
    Details::TestAdaptRK<StepperScheme::VERNER8>(folderName, "logVerner8ADHO.dat", testCase) ;
}

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
