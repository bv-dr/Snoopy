#ifndef BV_Math_ODEIntegration_Tests_Comparison_hpp
#define BV_Math_ODEIntegration_Tests_Comparison_hpp

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

void testHarmonicOscillator(void) ;
void testDampedHarmonicOscillator(void) ;
void testSin(void) ;

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Tests_Comparison_hpp
