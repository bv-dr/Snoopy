#include "Tests/ODEIntegration/Tools.hpp"
#include "Math/Tools.hpp"
#include <cmath>
#include <iostream>


namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

HarmonicOscillator::HarmonicOscillator(const double & k,
                                       const double & l0,
                                       const double & m,
                                       const double & y0,
                                       const double & dY0) :
    k_(k), l0_(l0), m_(m), y0_(y0), dY0_(dY0),
    state_(2), der_(2), J_(2, 2), ref_(2)

{
}

HarmonicOscillator::~HarmonicOscillator(void)
{
}

HarmonicOscillator::OutputType HarmonicOscillator::getState(void)
{
    state_(0) = y0_ ;
    state_(1) = dY0_ ;
    return state_ ;
}

HarmonicOscillator::OutputType HarmonicOscillator::getReference(const double & t)
{
    double w(std::sqrt(k_/m_)) ;
    double A(l0_ - y0_) ;
    double B(dY0_/w) ;
    ref_(0) = l0_ - A * std::cos(w*t) + B * std::sin(w*t) ;
    ref_(1) = w * A * std::sin(w*t) + B * std::cos(w*t) ;
    return ref_ ;
}

DampedHarmonicOscillator::DampedHarmonicOscillator(const double & k,
                                                   const double & l0,
                                                   const double & m,
                                                   const double & b,
                                                   const double & y0,
                                                   const double & dY0) :
    HarmonicOscillator(k, l0, m, y0, dY0), b_(b)
{
}

DampedHarmonicOscillator::OutputType DampedHarmonicOscillator::getReference(const double & t)
{
    double w(std::sqrt(k_/m_)) ;
    double gamma(b_ / m_) ;
    double alpha(std::sqrt(std::abs(w*w-gamma*gamma/4.))) ;
    double A(l0_ - y0_) ;
    double B ;
    double C(std::exp(-gamma*t/2.)) ;
    double D ;
    if (gamma > (2. * w))
    {
        B = A * gamma / (2. * alpha) ;
        D = A * std::cosh(alpha*t) + B * std::sinh(alpha*t) ;
        ref_(0) = l0_ - C * D ;
        ref_(1) = -gamma/2. * C * D + C * (alpha*A*std::sinh(alpha*t) + alpha*B*std::cosh(alpha*t)) ;
    }
    else if (BV::Math::IsClose(gamma, 2. * w))
    {
        B = A * gamma / 2. ;
        D = A + B * t ;
        ref_(0) = l0_ - C * D ;
        ref_(1) = -gamma/2. * C * D + C * B ;
    }
    else
    {
        B = A * gamma / (2. * alpha) ;
        D = A * std::cos(alpha*t) + B * std::sin(alpha*t) ;
        ref_(0) = l0_ - C * D ;
        ref_(1) = -gamma/2. * C * D + C * (-alpha*A*std::sin(alpha*t)+alpha*B*std::cos(alpha*t)) ;
    }
    return ref_ ;
}

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
