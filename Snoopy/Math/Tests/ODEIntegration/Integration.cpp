#include "Tests/ODEIntegration/Integration.hpp"

#include <Eigen/Dense>

#include "Tests/ODEIntegration/Tools.hpp"
#include "Math/ODEIntegration/Integrate.hpp"
#include "Math/ODEIntegration/ODEIntegrationParameters.hpp"
#include "Math/ODEIntegration/Steppers/Types.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

void testIntegration(void)
{

    ODEIntegrationParameters<double, double> params(
                              Steppers::StepperScheme::RK4, false
                                                   ) ;
    HarmonicOscillator ho(0.01, 1., 1., 0., 0.) ;
    Eigen::Vector2d state(ho.getState()) ;
    System<HarmonicOscillator> system(ho) ;
    {
        IntegrationInformation info(Integrate(params, system, state, 0., 100., 1.)) ;
    }

}

struct DummyObserver
{
    template <typename State, typename Time>
    void operator()(const State & state, const Time & time)
    {
        return ; // Nothing done, just for call test
    }
} ;

void testIntegrationObserver(void)
{

    ODEIntegrationParameters<double, double> params(
                              Steppers::StepperScheme::RK4, true
                                                   ) ;
    HarmonicOscillator ho(0.01, 1., 1., 0., 0.) ;
    Eigen::Vector2d state(ho.getState()) ;
    System<HarmonicOscillator> system(ho) ;
    DummyObserver obs ;
    {
        IntegrationInformation info(Integrate(params, system, state, 0., 100., 1.,
                                              obs)) ;
    }
}

struct DummyStopper
{
    template <typename State, typename Time>
    bool operator()(const State & state, const Time & time)
    {
        if (time > 50.)
        {
            return true ;
        }
        return false ;
    }

} ;

void testIntegrationObserverStopper(void)
{

    ODEIntegrationParameters<double, double> params(
                              Steppers::StepperScheme::RK4, true
                                                   ) ;
    HarmonicOscillator ho(0.01, 1., 1., 0., 0.) ;
    Eigen::Vector2d state(ho.getState()) ;
    System<HarmonicOscillator> system(ho) ;
    DummyObserver obs ;
    DummyStopper stopper ;
    {
        IntegrationInformation info(Integrate(params, system, state, 0., 100., 1.,
                                              obs, stopper)) ;
    }
}

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
