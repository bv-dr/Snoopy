#ifndef BV_Math_ODEIntegration_Tests_Tools_hpp
#define BV_Math_ODEIntegration_Tests_Tools_hpp

#include "Math/Tools.hpp"

#include <boost/filesystem.hpp>
#include <Eigen/Dense>
#include <string>
#include <fstream>
#include <functional>
#include <vector>
#include <chrono>

#include <boost/test/unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include "Math/ODEIntegration/Integrate.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

using namespace boost::unit_test ;

class HarmonicOscillator
{
public:
    typedef Eigen::VectorXd OutputType ;
    typedef Eigen::VectorXd DerOutputType ;
    typedef Eigen::MatrixXd Der2OutputType ;

protected:
    double k_ ;
    double l0_ ;
    double m_ ;
    double y0_ ;
    double dY0_ ;
    OutputType state_ ; // x and v
    DerOutputType der_ ;
    Der2OutputType J_ ;
    OutputType ref_ ;

public:

    HarmonicOscillator(const double & k, const double & l0, const double & m,
                       const double & y0, const double & dY0) ;
    virtual ~HarmonicOscillator(void) ;

    virtual OutputType getState(void) ;

    virtual OutputType getReference(const double & t) ;

    template <typename State>
    DerOutputType getDerivative(const double & t, const State & y)
    {
        der_(0) = y(1) ; // v
        der_(1) = - k_ / m_ * (y(0) - l0_) ; // F
        return der_ ;
    }

    template <typename State>
    Der2OutputType getJacobian(const double & t, const State & y)
    {
        J_(0, 0) = 0. ;
        J_(0, 1) = 1. ;
        J_(1, 0) = - k_ / m_ ;
        J_(1, 1) = 0. ;
        return J_ ;
    }
} ;

class DampedHarmonicOscillator : public HarmonicOscillator
{
public:
    typedef Eigen::VectorXd OutputType ;
    typedef Eigen::VectorXd DerOutputType ;
    typedef Eigen::MatrixXd Der2OutputType ;

private:
    double b_ ;

public:
    DampedHarmonicOscillator(const double & k, const double & l0,
                             const double & m, const double & b,
                             const double & y0, const double & dY0) ;

    virtual OutputType getReference(const double & t) ;

    template <typename State>
    DerOutputType getDerivative(const double & t, const State & y)
    {
        der_(0) = y(1) ; // v
        der_(1) = - b_ / m_ * y(1) - k_ / m_ * (y(0) - l0_) ; // F
        return der_ ;
    }

    template <typename State>
    Der2OutputType getJacobian(const double & t, const State & y)
    {
        J_(0, 0) = 0. ;
        J_(0, 1) = 1. ;
        J_(1, 0) = - k_ / m_ ;
        J_(1, 1) = - b_ / m_ ;
        return J_ ;
    }
} ;

template <typename F>
class Functional
{
private:
    F func_ ;
public:
    typedef typename F::OutputType OutputType ;
    typedef typename F::DerOutputType DerOutputType ;
    typedef typename F::Der2OutputType Der2OutputType ;

    Functional(const F & func) : func_(func)
    {
    }

    OutputType getState(void)
    {
        return func_(0.) ;
    }

    DerOutputType getDerivative(const double & t, const OutputType & y)
    {
        return func_.dEval(t) ;
    }

    Der2OutputType getJacobian(const double & t, const OutputType & y)
    {
        return func_.dEval2(t) ;
    }

    OutputType getReference(const double & t)
    {
        return func_(t) ;
    }

} ;

template <typename TestCase>
class System
{
private:
    TestCase testCase_ ;

public:
    System(const TestCase & testCase) : testCase_(testCase)
    {
    }

    template <typename State, typename Deriv, typename XType>
    void operator()(const State & y, Deriv & dy, const XType & x)
    {
        dy = testCase_.getDerivative(x, y) ;
    }
} ;

template <typename TestCase>
class SystemJacobian
{
private:
    TestCase testCase_ ;

public:

    typedef typename TestCase::Der2OutputType JType ;

    template <typename StateIn>
    static JType getJInitialized(const StateIn & state)
    {
        return JType::Zero(state.size(), state.size()) ;
    }

    SystemJacobian(const TestCase & testCase) : testCase_(testCase)
    {
    }

    template <typename State, typename JType, typename XType>
    void operator()(const State & y, JType & J, const XType & x)
    {
        J = testCase_.getJacobian(x, y) ;
    }
} ;

template <typename T>
struct OutputHelper
{
    static T getAbsError(const T & ref, const T & y)
    {
        return (ref - y).cwiseAbs() ;
    }

    static T getRelError(const T & ref, const T & y)
    {
        T relError(ref - y) ;
        for (unsigned i=0; i<relError.size(); ++i)
        {
            if (!BV::Math::IsClose(ref(i), 0.))
            {
                relError(i) /= ref(i) ;
            }
        }
        return relError.cwiseAbs() ;
    }

    static double getMax(const T & y)
    {
        return y.maxCoeff() ;
    }

    static void write(std::ofstream & f, const T & y)
    {
        f << y.transpose() ;
    }

    static void boostComp(const T & ref, const T & y,
                          const double & tol)
    {
        for (unsigned i=0; i<y.size(); ++i)
        {
            BOOST_CHECK_SMALL(y(i) - ref(i), tol) ;
        }
    }
} ;

template <>
struct OutputHelper<double>
{
    static double getAbsError(const double & ref, const double & y)
    {
        return std::abs(ref - y) ;
    }

    static double getRelError(const double & ref, const double & y)
    {
        if (BV::Math::IsClose(ref, 0.))
        {
            return std::abs(ref - y) ;
        }
        return std::abs((ref - y) / ref) ;
    }

    static double getMax(const double & y)
    {
        return y ;
    }

    static void write(std::ofstream & f, const double & y)
    {
        f << y ;
    }

    static void boostComp(const double & ref, const double & y,
                          const double & tol)
    {
        BOOST_CHECK_SMALL(y - ref, tol) ;
    }
} ;


template <typename TestCase>
class Observer
{
private:
    TestCase testCase_ ;
    std::string logFileName_ ;
    bool compareMaxError_ ;
    bool useBoostComp_ ;
    double tol_ ;
    double previousX_ ;
    double maxAbsError_ ;
    double maxRelError_ ;
    std::ofstream logFile_ ;
public:
    typedef typename TestCase::OutputType OutputType ;

    Observer(const TestCase & testCase, std::string logFileName,
             bool compareMaxError=true, bool useBoostComp=false,
             const double & tol=1.e-4) :
        testCase_(testCase), logFileName_(logFileName),
        compareMaxError_(compareMaxError), useBoostComp_(useBoostComp),
        tol_(tol), previousX_(0.), maxAbsError_(0.), maxRelError_(0.)
    {
        logFile_.open(logFileName_) ;
    }

    ~Observer(void)
    {
        logFile_.close() ;
    }

    void operator()(const OutputType & y, const double & x)
    {
        typedef OutputHelper<OutputType> H ;
        OutputType ref(testCase_.getReference(x)) ;
        if (useBoostComp_)
        {
            H::boostComp(ref, y, tol_) ;
        }
        OutputType absError(H::getAbsError(ref, y)) ;
        OutputType relError(H::getRelError(ref, y)) ;
        if (compareMaxError_)
        {
            double tmpRel(H::getMax(relError)) ;
            if (tmpRel > maxRelError_)
            {
                maxRelError_ = tmpRel ;
            }
            double tmpAbs(H::getMax(absError)) ;
            if (tmpAbs > maxAbsError_)
            {
                maxAbsError_ = tmpAbs ;
            }
        }
        // Write the log file
        logFile_ << x << " " ;
        H::write(logFile_, y) ;
        logFile_ << " " ;
        H::write(logFile_, ref) ;
        logFile_ << " " ;
        H::write(logFile_, absError) ;
        logFile_ << " " ;
        H::write(logFile_, relError) ;
        logFile_ << " " << x-previousX_ << std::endl ;
        previousX_ = x ;
    }

    double getMaxRelError(void) const
    {
        return maxRelError_ ;
    }

    double getMaxAbsError(void) const
    {
        return maxAbsError_ ;
    }
} ;

template <typename Stepper, typename TestCase>
class Launcher
{
private:
    Stepper stepper_ ;
    TestCase testCase_ ;
    System<TestCase> system_ ;
    SystemJacobian<TestCase> systemJacobian_ ;
    Observer<TestCase> observer_ ;

public:
    typedef typename TestCase::OutputType OutputType ;

    Launcher(Stepper stepper, TestCase testCase,
             std::string logFileName,
             bool compareMaxError=true, bool useBoostComp=false,
             double tol=1.e-4) :
        stepper_(stepper), testCase_(testCase),
        system_(testCase), systemJacobian_(testCase),
        observer_(testCase, logFileName, compareMaxError, useBoostComp)
    {
    }

    IntegrationInformation operator()(const double & xStart, const double & xEnd,
                                      const double & xStep)
    {
        OutputType state(testCase_.getState()) ;
        return Integrate(stepper_,
                         std::make_pair(std::ref(system_),
                                        std::ref(systemJacobian_)),
                         state, xStart, xEnd, xStep,
                         std::ref(observer_)) ;
    }

    double getMaxRelError(void) const
    {
        return observer_.getMaxRelError() ;
    }

    double getMaxAbsError(void) const
    {
        return observer_.getMaxAbsError() ;
    }
} ;

template <typename TestCase>
class Comparator
{
private:
    TestCase testCase_ ;
    std::string outputFolderName_ ;
    std::string testCaseFolderName_ ;
    std::string logFileName_ ;
    double xStart_ ;
    double xEnd_ ;
    double xStep_ ;
    bool compareDuration_ ;
    bool compareMaxError_ ;
    bool compareNSteps_ ;
    bool useBoostComp_ ;
    double tol_ ;
    std::ofstream logFile_ ;
public:
    Comparator(const TestCase & testCase,
               std::string outputFolderName,
               std::string caseName,
               const double & xStart, const double & xEnd, const double & xStep,
               bool compareDuration=true, bool compareMaxError=true,
               bool compareNSteps=true, bool useBoostComp=false,
               double tol=1.e-4) :
        testCase_(testCase),
        outputFolderName_(outputFolderName),
        testCaseFolderName_(outputFolderName_ + "/" + caseName),
        logFileName_(caseName+".dat"),
        xStart_(xStart), xEnd_(xEnd), xStep_(xStep),
        compareDuration_(compareDuration), compareMaxError_(compareMaxError),
        compareNSteps_(compareNSteps), useBoostComp_(useBoostComp),
        tol_(tol)
    {
        boost::filesystem::path dirOutput(outputFolderName_) ;
        if (!boost::filesystem::exists(dirOutput))
        {
            boost::filesystem::create_directory(dirOutput) ;
        }
        boost::filesystem::path dirTestCase(testCaseFolderName_) ;
        if (!boost::filesystem::exists(dirTestCase))
        {
            boost::filesystem::create_directory(dirTestCase) ;
        }
        logFile_.open(testCaseFolderName_ + "/" + logFileName_) ;
        logFile_ << "# Stepper name | Duration (microsec) | Steps number | Max rel error | Max abs error" << std::endl ;
    }

    ~Comparator(void)
    {
        logFile_.close() ;
    }

    template <typename StepperFactory>
    void addStepper(std::string stepperName)
    {
        typedef typename StepperFactory::Type Stepper ;
        Stepper stepper(StepperFactory::get()) ;
        std::string log(testCaseFolderName_ + "/" + stepperName + ".dat") ;
        Launcher<Stepper, TestCase> launcher(
                  stepper, testCase_, log, compareMaxError_, useBoostComp_, tol_
                                            ) ;
        std::chrono::high_resolution_clock::time_point tStart(std::chrono::high_resolution_clock::now()) ;
        IntegrationInformation info(launcher(xStart_, xEnd_, xStep_)) ;
        std::chrono::high_resolution_clock::time_point tEnd(std::chrono::high_resolution_clock::now()) ;
        logFile_ << stepperName
                 << " " << std::chrono::duration_cast<std::chrono::microseconds>(tEnd-tStart).count()
                 << " " << info.nIntegrationSteps()
                 << " " << launcher.getMaxRelError()
                 << " " << launcher.getMaxAbsError()
                 << std::endl ;
    }

} ;

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Tests_Tools_hpp
