#ifndef BV_Math_ODEIntegration_Tests_RungeKutta_hpp
#define BV_Math_ODEIntegration_Tests_RungeKutta_hpp

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

void testHarmonicOscillatorRK(void) ;
void testAdaptiveHarmonicOscillatorRK(void) ;
void testDampedHarmonicOscillatorRK(void) ;
void testAdaptiveDampedHarmonicOscillatorRK(void) ;

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV

#endif // BV_Math_ODEIntegration_Tests_RungeKutta_hpp
