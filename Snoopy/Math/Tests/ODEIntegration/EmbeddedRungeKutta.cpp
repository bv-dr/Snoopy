#include "Tests/ODEIntegration/EmbeddedRungeKutta.hpp"

#include <boost/filesystem.hpp>
#include <iostream>
#include <chrono>

#include "Tests/ODEIntegration/Tools.hpp"
#include "Math/ODEIntegration/Steppers/Steppers.hpp"

namespace BV {
namespace Math {
namespace ODEIntegration {
namespace Tests {

namespace Details {

template <int Scheme, typename TestCase>
void TestERK(const std::string& folderName, const std::string& fileName,
             const double & xStep, TestCase & testCase)
{
    typedef Steppers::Stepper<Scheme, Eigen::VectorXd> FactoryType ;
    typedef typename FactoryType::Type Stepper ;
    Stepper stepper(FactoryType::get()) ;
    std::string log(folderName + "/" + fileName) ;
    Launcher<Stepper, TestCase> launcher(stepper, testCase, log, false) ;
    double xStart(0.) ;
    double xEnd(200.) ;
    double h(xStep) ;
    std::size_t nSteps(0) ;
    launcher(xStart, xEnd, h, nSteps) ;
}

template <int Scheme, typename TestCase>
void TestAdaptERK(const std::string& folderName, const std::string& fileName,
                  TestCase & testCase)
{
    typedef Steppers::AdaptiveStepper<Scheme, Eigen::VectorXd> FactoryType ;
    typedef typename FactoryType::Type Stepper ;
    Stepper stepper(FactoryType::get(1.e-6, 1.e-6, 1., 0., 100.)) ;
    std::string log(folderName + "/" + fileName) ;
    Launcher<Stepper, TestCase> launcher(stepper, testCase, log, false) ;
    double xStart(0.) ;
    double xEnd(200.) ;
    double h(1.) ;
    std::size_t nSteps(0) ;
    launcher(xStart, xEnd, h, nSteps) ;
}

} // End of namespace Details

void testHarmonicOscillatorERK(void)
{
    std::cout << "HO ERK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directories(dir) ;
    }
    HarmonicOscillator testCase(0.01, 1., 1., 0., 0.) ;
    using Steppers::StepperScheme ;
    Details::TestERK<StepperScheme::ERK_CASH_KARP54>(folderName, "logCK54HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_DOPRI5>(folderName, "logDopri5HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG34>(folderName, "logFehlberg34HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG43>(folderName, "logFehlberg43HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG45>(folderName, "logFehlberg45HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG54>(folderName, "logFehlberg54HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG56>(folderName, "logFehlberg56HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG65>(folderName, "logFehlberg65HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG78>(folderName, "logFehlberg78HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG87>(folderName, "logFehlberg87HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER56>(folderName, "logVerner56HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER65>(folderName, "logVerner65HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER67>(folderName, "logVerner67HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER76>(folderName, "logVerner76HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER78>(folderName, "logVerner78HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER87>(folderName, "logVerner87HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER89>(folderName, "logVerner89HO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER98>(folderName, "logVerner98HO.dat", 1., testCase) ;
}

void testAdaptiveHarmonicOscillatorERK(void)
{
    std::cout << "HO AERK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directories(dir) ;
    }
    HarmonicOscillator testCase(0.01, 1., 1., 0., 0.) ;
    using Steppers::StepperScheme ;
    Details::TestAdaptERK<StepperScheme::ERK_CASH_KARP54>(folderName, "logCK54AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_DOPRI5>(folderName, "logDopri5AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG34>(folderName, "logFehlberg34AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG43>(folderName, "logFehlberg43AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG45>(folderName, "logFehlberg45AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG54>(folderName, "logFehlberg54AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG56>(folderName, "logFehlberg56AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG65>(folderName, "logFehlberg65AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG78>(folderName, "logFehlberg78AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG87>(folderName, "logFehlberg87AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER56>(folderName, "logVerner56AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER65>(folderName, "logVerner65AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER67>(folderName, "logVerner67AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER76>(folderName, "logVerner76AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER78>(folderName, "logVerner78AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER87>(folderName, "logVerner87AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER89>(folderName, "logVerner89AHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER98>(folderName, "logVerner98AHO.dat", testCase) ;
}

void testDampedHarmonicOscillatorERK(void)
{
    std::cout << "DHO ERK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directories(dir) ;
    }
    using Steppers::StepperScheme ;
    DampedHarmonicOscillator testCase(0.01, 1., 1., 0.2, 0., 0.) ;
    Details::TestERK<StepperScheme::ERK_CASH_KARP54>(folderName, "logCK54DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_DOPRI5>(folderName, "logDopri5DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG34>(folderName, "logFehlberg34DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG43>(folderName, "logFehlberg43DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG45>(folderName, "logFehlberg45DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG54>(folderName, "logFehlberg54DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG56>(folderName, "logFehlberg56DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG65>(folderName, "logFehlberg65DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG78>(folderName, "logFehlberg78DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_FEHLBERG87>(folderName, "logFehlberg87DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER56>(folderName, "logVerner56DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER65>(folderName, "logVerner65DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER67>(folderName, "logVerner67DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER76>(folderName, "logVerner76DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER78>(folderName, "logVerner78DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER87>(folderName, "logVerner87DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER89>(folderName, "logVerner89DHO.dat", 1., testCase) ;
    Details::TestERK<StepperScheme::ERK_VERNER98>(folderName, "logVerner98DHO.dat", 1., testCase) ;
}

void testAdaptiveDampedHarmonicOscillatorERK(void)
{
    std::cout << "DHO AERK: " << boost::filesystem::current_path() << std::endl ;
    std::string folderName("Math/ODEIntegration/Tests/Output") ;
    boost::filesystem::path dir(folderName) ;
    if (!boost::filesystem::exists(dir))
    {
        boost::filesystem::create_directories(dir) ;
    }
    using Steppers::StepperScheme ;
    DampedHarmonicOscillator testCase(0.01, 1., 1., 0.2, 0., 0.) ;
    Details::TestAdaptERK<StepperScheme::ERK_CASH_KARP54>(folderName, "logCK54ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_DOPRI5>(folderName, "logDopri5ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG34>(folderName, "logFehlberg34ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG43>(folderName, "logFehlberg43ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG45>(folderName, "logFehlberg45ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG54>(folderName, "logFehlberg54ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG56>(folderName, "logFehlberg56ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG65>(folderName, "logFehlberg65ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG78>(folderName, "logFehlberg78ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_FEHLBERG87>(folderName, "logFehlberg87ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER56>(folderName, "logVerner56ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER65>(folderName, "logVerner65ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER67>(folderName, "logVerner67ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER76>(folderName, "logVerner76ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER78>(folderName, "logVerner78ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER87>(folderName, "logVerner87ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER89>(folderName, "logVerner89ADHO.dat", testCase) ;
    Details::TestAdaptERK<StepperScheme::ERK_VERNER98>(folderName, "logVerner98ADHO.dat", testCase) ;
}

} // End of namespace Tests
} // End of namespace ODEIntegration
} // End of namespace Math
} // End of namespace BV
