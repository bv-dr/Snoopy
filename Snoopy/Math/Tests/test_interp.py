import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
from Snoopy.Math import df_interpolate


def test_df_interpolate(display=False) : 
   x_old = np.linspace( 1 , 100 , 10)
   x_new = np.linspace( 1 , 100, 100)

   df = pd.DataFrame( index = x_old , data = {"log_data" : np.log(x_old) } )

   dfNew = df_interpolate( df , x_new, k = 1, xfunc = np.log  )
   
   assert( np.isclose( dfNew["log_data"].values , np.log(x_new) ).all() )
   
   if display : 
       fig, ax = plt.subplots()
       df.plot(marker = "o" , linestyle = "", ax=ax, label = "original")
       dfNew["log_data"].plot(ax=ax, label = "Linearly interpolated in log space", marker = "+")
       ax.plot(x_new , np.log(x_new) , label = "True")
       ax.legend()
   



if __name__ == "__main__" :


   test_df_interpolate(True)

