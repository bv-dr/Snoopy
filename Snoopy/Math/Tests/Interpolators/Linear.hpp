#pragma once

namespace BV {
namespace Math {
namespace Interpolators {
namespace Tests {

void test1DEigenVector(void) ;
void test1DEigenVectorBlock1(void) ;
void test1DEigenVectorBlock2(void) ;
void test1DEigenVectorBlock3(void) ;
void test1DEigenMatrix(void) ;
void test1DStdVector(void) ;
void test1DStdVectorStdVector(void) ;
void test1DStdMap(void) ;
void test1DArray(void) ;
void test1DArraySecondAxis(void) ;
void test2DEigen(void) ;
void test2DStdVectorStdVector(void) ;
void test2DArray(void) ;
//void test3DArray(void) ;

} // End of namespace Tests
} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV
