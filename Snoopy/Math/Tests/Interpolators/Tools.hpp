#pragma once

#include <boost/test/unit_test.hpp>

#include <Eigen/Dense>

namespace BV {
namespace Math {
namespace Interpolators {
namespace Tests {

void CheckStdVectors(const std::vector<double> & v1,
                     const std::vector<double> & v2,
                     const double epsilon=1.e-9) ;

void CheckStdVectorStdVectors(const std::vector<std::vector<double> > & v1,
                              const std::vector<std::vector<double> > & v2,
                              const double epsilon=1.e-9) ;

template <typename T1, typename T2>
void CheckArrays(const T1 & ar1, const T2 & ar2, const double epsilon=1.e-9)
{
    assert(ar1.dimensions() == ar2.dimensions()) ;
    auto it2(ar2.data()) ;
    int i(0) ;
    for (auto it1=ar1.data(); i!=ar1.size(); ++it1, ++it2, ++i)
    {
        BOOST_CHECK_SMALL((*it1)-(*it2), epsilon) ;
    }
}

} // End of namespace Tests
} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV
