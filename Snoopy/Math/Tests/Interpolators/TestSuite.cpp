#include <boost/test/unit_test.hpp>

#include "Tests/Interpolators/Linear.hpp"

using namespace boost::unit_test ;

#define BV_ADD_INTERPOLATORS_TEST( testName )                         \
    InterpolatorsTestSuite->add(BOOST_TEST_CASE( &(                   \
                            BV::Math::Interpolators::Tests::testName  \
                                                  ))) ;

test_suite * init_unit_test_suite(int argc, char* argv[])
{
    test_suite * InterpolatorsTestSuite = BOOST_TEST_SUITE("InterpolatorsTestSuite") ;

    BV_ADD_INTERPOLATORS_TEST(test1DEigenVector)
    BV_ADD_INTERPOLATORS_TEST(test1DEigenVectorBlock1)
    BV_ADD_INTERPOLATORS_TEST(test1DEigenVectorBlock2)
    BV_ADD_INTERPOLATORS_TEST(test1DEigenVectorBlock3)
    BV_ADD_INTERPOLATORS_TEST(test1DEigenMatrix)
    BV_ADD_INTERPOLATORS_TEST(test1DStdVector)
    BV_ADD_INTERPOLATORS_TEST(test1DStdVectorStdVector)
    BV_ADD_INTERPOLATORS_TEST(test1DStdMap)
    BV_ADD_INTERPOLATORS_TEST(test1DArray)
    BV_ADD_INTERPOLATORS_TEST(test1DArraySecondAxis)
    BV_ADD_INTERPOLATORS_TEST(test2DEigen)
    BV_ADD_INTERPOLATORS_TEST(test2DStdVectorStdVector)
    BV_ADD_INTERPOLATORS_TEST(test2DArray)
    //BV_ADD_INTERPOLATORS_TEST(test3DArray)

    // Registering all the suites
    framework::master_test_suite().p_name.value = "BV Interpolators test suite" ;
    framework::master_test_suite().add(InterpolatorsTestSuite) ;

  return 0 ;
}

#undef BV_ADD_INTERPOLATORS_TEST
