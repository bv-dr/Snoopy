#include "Tests/Interpolators/Linear.hpp"

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <vector>
#include <map>

#include "Math/Interpolators/Linear.hpp"
#include "Tests/Interpolators/Tools.hpp"
#include "Tools/Tests/Tools.hpp"

namespace BV {
namespace Math {
namespace Interpolators {
namespace Tests {

void test1DEigenVector(void)
{
    Eigen::VectorXd axis(10) ;
    Eigen::VectorXd data(10) ;
    Eigen::VectorXd values(10) ;
    for (auto i = 0; i < axis.size(); ++i)
    {
        axis(i) = static_cast<double>(i + 1);
        data(i) = static_cast<double>(i + 1);
        values(i) = static_cast<double>(i + 1);
    }

    Eigen::VectorXd res(Linear1D::get(axis, data, values)) ;
    BV::Tools::Tests::CheckEigenVectors(res, data) ;
    values *= 0.6 ;
    values += Eigen::VectorXd::Constant(10, 1.) ; // In order to be in axis range
    res = Linear1D::get(axis, data, values) ;
    BV::Tools::Tests::CheckEigenVectors(res, values) ;
}

void test1DEigenVectorBlock1(void)
{
    Eigen::MatrixXd tmp(Eigen::MatrixXd::Zero(10, 3)) ;
    Eigen::VectorXd data(10) ;
    Eigen::VectorXd values(10) ;
    for (unsigned i=0; i<data.size(); ++i)
    {
        tmp(i, 0) = static_cast<double>(i+1) ;
        data(i) = static_cast<double>(i+1) ;
        values(i) = static_cast<double>(i+1) ;
    }
    Eigen::VectorXd res(Linear1D::get(tmp.col(0), data, values)) ;
    BV::Tools::Tests::CheckEigenVectors(res, data) ;
    values *= 0.6 ;
    values += Eigen::VectorXd::Constant(10, 1.) ; // In order to be in axis range
    res = Linear1D::get(tmp.col(0), data, values) ;
    BV::Tools::Tests::CheckEigenVectors(res, values) ;
}

void test1DEigenVectorBlock2(void)
{
    Eigen::MatrixXd tmp(Eigen::MatrixXd::Zero(10, 3)) ;
    Eigen::VectorXd values(10) ;
    for (unsigned i=0; i<values.size(); ++i)
    {
        tmp(i, 0) = static_cast<double>(i+1) ;
        tmp(i, 1) = static_cast<double>(i+1) ;
        values(i) = static_cast<double>(i+1) ;
    }
    Eigen::VectorXd res(Linear1D::get(tmp.col(0), tmp.col(1), values)) ;
    BV::Tools::Tests::CheckEigenVectors(res, tmp.col(1)) ;
    values *= 0.5 ;
    values += Eigen::VectorXd::Constant(10, 1.) ; // In order to be in axis range
    res = Linear1D::get(tmp.col(0), tmp.col(1), values) ;
    BV::Tools::Tests::CheckEigenVectors(res, values) ;
}

void test1DEigenVectorBlock3(void)
{
    Eigen::MatrixXd tmp(Eigen::MatrixXd::Zero(10, 3)) ;
    for (unsigned i=0; i<tmp.rows(); ++i)
    {
        tmp(i, 0) = static_cast<double>(i+1) ;
        tmp(i, 1) = static_cast<double>(i+1) ;
        tmp(i, 2) = static_cast<double>(i+1) ;
    }
    Eigen::VectorXd res(Linear1D::get(tmp.col(0), tmp.col(1), tmp.col(2))) ;
    BV::Tools::Tests::CheckEigenVectors(res, tmp.col(2)) ;
    tmp.col(2) *= 0.5 ;
    tmp.col(2) += Eigen::VectorXd::Constant(10, 1.) ; // In order to be in axis range
    res = Linear1D::get(tmp.col(0), tmp.col(1), tmp.col(2)) ;
    BV::Tools::Tests::CheckEigenVectors(res, tmp.col(2)) ;
}

void test1DEigenMatrix(void)
{
    Eigen::VectorXd axis(10) ;
    Eigen::MatrixXd data(10, 3) ;
    Eigen::VectorXd values(10) ;
    for (unsigned i=0; i<axis.size(); ++i)
    {
        axis(i) = static_cast<double>(i+1) ;
        values(i) = static_cast<double>(i+1) ;
        for (unsigned j=0; j<data.cols(); ++j)
        {
            data(i, j) = static_cast<double>(i*3+j) ;
        }
    }
    Eigen::MatrixXd mat = data.block(0, 0, 10, 2);
    Eigen::MatrixXd res(Linear1D::get(axis, mat, values)) ;
    BV::Tools::Tests::CheckEigenMatrix(res, mat) ;
}

void test1DStdVector(void)
{
    std::vector<double> axis(10) ;
    std::vector<double> data(10) ;
    std::vector<double> values(10) ;
    for (unsigned i=0; i<axis.size(); ++i)
    {
        axis[i] = static_cast<double>(i+1) ;
        data[i] = static_cast<double>(i+1) ;
        values[i] = static_cast<double>(i+1) ;
    }
    std::vector<double> res(Linear1D::get(axis, data, values)) ;
    CheckStdVectors(res, data) ;
    for (unsigned i=0; i<values.size(); ++i)
    {
        values[i] *= 0.6 ;
        values[i] += 1. ;
    }
    res = Linear1D::get(axis, data, values) ;
    CheckStdVectors(res, values) ;
}

void test1DStdVectorStdVector(void)
{
    std::vector<double> axis(10) ;
    std::vector<std::vector<double> > data(10) ;
    std::vector<double> values(10) ;
    for (unsigned i=0; i<axis.size(); ++i)
    {
        axis[i] = static_cast<double>(i+1) ;
        values[i] = static_cast<double>(i+1) ;
        data[i] = std::vector<double>(3) ;
        for (unsigned j=0; j<3; ++j)
        {
            data[i][j] = static_cast<double>(i*3+j) ;
        }
    }
    std::vector<std::vector<double> > res(Linear1D::get(axis, data, values)) ;
    CheckStdVectorStdVectors(res, data) ;
}

void test1DStdMap(void)
{
    std::map<double, double> data ;
    std::vector<double> values(10) ;
    for (unsigned i=0; i<values.size(); ++i)
    {
        double val(static_cast<double>(i)) ;
        data.insert(std::pair<double, double>(val, val)) ;
        values[i] = val ;
    }
    std::vector<double> res(Linear1D::get(data, values)) ;
    CheckStdVectors(res, values) ;
    for (unsigned i=0; i<values.size(); ++i)
    {
        values[i] *= 0.5 ;
    }
    res = Linear1D::get(data, values) ;
    CheckStdVectors(res, values) ;
}

void test1DArray(void)
{
    Eigen::Tensor<double, 1> axis(10) ;
    Eigen::Tensor<double, 3> data(10, 10, 3) ;
    Eigen::Tensor<double, 1> values(10) ;
    Eigen::MatrixXd subData(10, 3) ;
    for (unsigned i=0; i<axis.size(); ++i)
    {
        axis(i) = static_cast<double>(i+1) ;
        values(i) = static_cast<double>(i+1) ;
        for (unsigned j=0; j<subData.cols(); ++j)
        {
            subData(i, j) = static_cast<double>(i*3+j) ;
        }
    }
    for (int i=0; i<data.dimensions()[0]; ++i)
    {
        data.chip(i, 0) = Eigen::TensorMap<Eigen::Tensor<double, 2> >(subData.data(), 10, 3) ;
    }
    Eigen::Tensor<double, 3> res(Linear1D::get(axis, 0, data, values)) ;
    CheckArrays(res, data) ;
}

void test1DArraySecondAxis(void)
{
    Eigen::Tensor<double, 1> axis(10) ;
    Eigen::Tensor<double, 3> data(10, 10, 3) ;
    Eigen::Tensor<double, 1> values(10) ;
    Eigen::MatrixXd subData(10, 3) ;
    for (unsigned i=0; i<axis.size(); ++i)
    {
        axis(i) = static_cast<double>(i+1) ;
        values(i) = static_cast<double>(i+1) ;
        for (unsigned j=0; j<subData.cols(); ++j)
        {
            subData(i, j) = static_cast<double>(i*3+j) ;
        }
    }
    for (int i=0; i<data.dimensions()[0]; ++i)
    {
        data.chip(i, 0) = Eigen::TensorMap<Eigen::Tensor<double, 2> >(subData.data(), 10, 3) ;
    }
    Eigen::Tensor<double, 3> res(Linear1D::get(axis, 1, data, values)) ;
    CheckArrays(res, data) ;
}

void test2DEigen(void)
{
    Eigen::VectorXd axis1(10) ;
    Eigen::VectorXd axis2(5) ;
    Eigen::MatrixXd data(10, 5) ;
    Eigen::VectorXd values1(10) ;
    Eigen::VectorXd values2(5) ;
    for (unsigned i=0; i<axis1.size(); ++i)
    {
        axis1(i) = static_cast<double>(i+1) ;
        values1(i) = static_cast<double>(i+1) ;
        if (i<5)
        {
            axis2(i) = static_cast<double>(i+1) ;
            values2(i) = static_cast<double>(i+1) ;
        }
        for (unsigned j=0; j<data.cols(); ++j)
        {
            data(i, j) = static_cast<double>(i*5+j) ;
        }
    }
    Eigen::MatrixXd res(Linear2D::get(axis1, axis2, data, values1, values2)) ;
    BV::Tools::Tests::CheckEigenMatrix(res, data) ;
}

void test2DStdVectorStdVector(void)
{
    std::vector<double> axis1(10) ;
    std::vector<double> axis2(5) ;
    std::vector<double> values1(10) ;
    std::vector<double> values2(5) ;
    std::vector<std::vector<double> > data(10) ;
    for (unsigned i=0; i<axis1.size(); ++i)
    {
        axis1[i] = static_cast<double>(i+1) ;
        values1[i] = static_cast<double>(i+1) ;
        if (i<5)
        {
            axis2[i] = static_cast<double>(i+1) ;
            values2[i] = static_cast<double>(i+1) ;
        }
        std::vector<double> tmp(5) ;
        for (unsigned j=0; j<tmp.size(); ++j)
        {
            tmp[j] = static_cast<double>(i*3+j) ;
        }
        data[i] = tmp ;
    }
    std::vector<std::vector<double> > res(Linear2D::get(axis1, axis2, data,
                                                        values1, values2)) ;
    CheckStdVectorStdVectors(res, data) ;
}

void test2DArray(void)
{
    Eigen::Tensor<double, 1> axis1(10) ;
    Eigen::Tensor<double, 1> axis2(5) ;
    Eigen::Tensor<double, 3> data(10, 5, 3) ;
    Eigen::Tensor<double, 1> values1(10) ;
    Eigen::Tensor<double, 1> values2(5) ;
    for (unsigned i=0; i<axis1.size(); ++i)
    {
        axis1(i) = static_cast<double>(i+1) ;
        values1(i) = static_cast<double>(i+1) ;
        if (i<5)
        {
            axis2(i) = static_cast<double>(i+1) ;
            values2(i) = static_cast<double>(i+1) ;
        }
        for (int j=0; j<data.dimensions()[1]; ++j)
        {
            for (int k=0; k<data.dimensions()[2]; ++k)
            {
                data(i, j, k) = static_cast<double>(i*10+j*5+k) ;
            }
        }
    }
    Eigen::Tensor<double, 3> res(Linear2D::get(axis1, axis2, data, values1, values2)) ;
    CheckArrays(res, data) ;
}

//void test3DArray(void)
//{
//    Eigen::Tensor<double, 1> axis1(10) ;
//    Eigen::Tensor<double, 1> axis2(5) ;
//    Eigen::Tensor<double, 1> axis3(3) ;
//    Eigen::Tensor<double, 4> data(10, 5, 3, 2) ;
//    Eigen::Tensor<double, 1> values1(10) ;
//    Eigen::Tensor<double, 1> values2(5) ;
//    Eigen::Tensor<double, 1> values3(3) ;
//    for (unsigned i=0; i<axis1.size(); ++i)
//    {
//        axis1(i) = static_cast<double>(i+1) ;
//        values1(i) = static_cast<double>(i+1) ;
//        if (i<5)
//        {
//            axis2(i) = static_cast<double>(i+1) ;
//            values2(i) = static_cast<double>(i+1) ;
//        }
//        if (i<3)
//        {
//            axis3(i) = static_cast<double>(i+1) ;
//            values3(i) = static_cast<double>(i+1) ;
//        }
//        for (int j=0; j<data.dimensions()[1]; ++j)
//        {
//            for (int k=0; k<data.dimensions()[2]; ++k)
//            {
//                for (int l=0; l<data.dimensions()[3]; ++l)
//                {
//                    data(i, j, k, l) = static_cast<double>(i*10+j*5+k*3+l) ;
//                }
//            }
//        }
//    }
//    Eigen::Tensor<double, 4> res(Linear3D::get(axis1, axis2, axis3, data,
//                                               values1, values2, values3)) ;
//    CheckArrays(res, data) ;
//}

} // End of namespace Tests
} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV

