#include "Tests/Interpolators/Tools.hpp"

#include <iostream>


namespace BV {
namespace Math {
namespace Interpolators {
namespace Tests {

void CheckStdVectors(const std::vector<double> & v1,
                     const std::vector<double> & v2,
                     const double epsilon)
{
    assert(v1.size() == v2.size()) ;
    for (unsigned i=0; i<v1.size(); ++i)
    {
        BOOST_CHECK_SMALL(v1[i]-v2[i], epsilon) ;
    }
}

void CheckStdVectorStdVectors(const std::vector<std::vector<double> > & v1,
                              const std::vector<std::vector<double> > & v2,
                              const double epsilon)
{
    assert(v1.size() == v2.size()) ;
    for (unsigned i=0; i<v1.size(); ++i)
    {
        for (unsigned j=0; j<v1[i].size(); ++j)
        {
            BOOST_CHECK_SMALL(v1[i][j]-v2[i][j], epsilon) ;
        }
    }

}

} // End of namespace Tests
} // End of namespace Interpolators
} // End of namespace Math
} // End of namespace BV
