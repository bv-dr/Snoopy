import numpy as np
from Snoopy.Math import FunContourGenerator


def test_iso_contour(display = False):
    x = np.arange(-10.0 , 15.0 , 0.01)
    y = np.arange(-10.0 , 10.0 , 0.01)

    def fun( x,y ) :
        return x**2 + y

    cgen = FunContourGenerator( fun, x, y )
    test = cgen(0.5)
    r = fun(*test)
    assert( np.isclose( r[~np.isnan(r)] , 0.5, rtol = 1e-3).all())

    if display :
        ax = cgen.plot( [0.5] , linewidth = 2)

        cgen.contourf(ax = ax, levels = [ 0.6 , 19 ])


if __name__ == "__main__" :

    test_iso_contour(True)
