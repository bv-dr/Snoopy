import json

from Opera.Math import Functions


def fct1Arg(a):
    return a*2


def fct2Arg(a, b):
    return a*2+b


def testSerializationR1ToR1UserDefined():
    f = Functions.R1ToR1UserDefined(fct1Arg)
    fAPI = Functions.AsFunction(f, 1, 1)
    d1 = fAPI.getSerializable()
    d2 = f.getSerializable()
    assert(d1 == d2)
    assert(f(5) == 10)
    assert(fAPI(5) == 10)
    assert(Functions.AsFunction(fAPI.getSerializable(), 1, 1)(5) == 10)
    d3 = json.loads(json.dumps(fAPI.getSerializable()))
    assert(Functions.AsFunction(d3, 1, 1)(5) == 10)


def testSerializationR2ToR1UserDefined():
    f = Functions.R2ToR1UserDefinedNArgs(fct2Arg)
    fAPI = Functions.AsFunction(f, 2, 1)
    d1 = fAPI.getSerializable()
    d2 = f.getSerializable()
    assert(d1 == d2)
    assert(f(5, 1) == 11)
    assert(fAPI(5, 1) == 11)
    assert(Functions.AsFunction(fAPI.getSerializable(), 2, 1)(5, 1) == 11)
    d3 = json.loads(json.dumps(fAPI.getSerializable()))
    assert(Functions.AsFunction(d3, 1, 1)(5, 1) == 11)


def testSerializationR2ToR1Uniform():
    f = Functions.R2ToR1Uniform(2.)
    fAPI = Functions.AsFunction(f, 2, 1)
    d1 = fAPI.getSerializable()
    d2 = f.getSerializable()
    assert(d1 == d2)
    assert(f(5, 1) == 2)
    assert(fAPI(5, 1) == 2)
    assert(Functions.AsFunction(fAPI.getSerializable(), 2, 1)(5, 1) == 2)
    d3 = json.loads(json.dumps(fAPI.getSerializable()))
    assert(Functions.AsFunction(d3, 1, 1)(5, 1) == 2.)


def testSerializationR2ToR1Analystical():
    f = Functions.R2ToR1Analytical('2*x+y', ('x', 'y'))
    fAPI = Functions.AsFunction(f, 2, 1)
    d1 = fAPI.getSerializable()
    d2 = f.getSerializable()
    assert(d1 == d2)
    assert(f(5, 1) == 11)
    assert(fAPI(5, 1) == 11)
    assert(Functions.AsFunction(fAPI.getSerializable(), 2, 1)(5, 1) == 11)
    d3 = json.loads(json.dumps(fAPI.getSerializable()))
    assert(Functions.AsFunction(d3, 1, 1)(5, 1) == 11)


def testSerializationR2ToR1AnalysticalDer():
    der2x = Functions.R2ToR2Analytical(["0.", "0."], ["x", "y"])
    der2y = Functions.R2ToR2Analytical(["0.", "0."], ["x", "y"])
    der = Functions.R2ToR2Analytical(["2.", "1."], ["x", "y"], [der2x, der2y])
    f = Functions.R2ToR1Analytical("2*x+y", ("x", "y"), der)
    fAPI = Functions.AsFunction(f, 2, 1)
    d1 = fAPI.getSerializable()
    d2 = f.getSerializable()
    assert(d1 == d2)
    assert(f(5, 1) == 11)
    assert(fAPI(5, 1) == 11)
    assert(Functions.AsFunction(fAPI.getSerializable(), 2, 1)(5, 1) == 11)
    d3 = json.loads(json.dumps(fAPI.getSerializable()))
    assert(Functions.AsFunction(d3, 1, 1)(5, 1) == 11)


def testSerializationR2ToR1AnalysticalFunctor():
    a = Functions.R2ToR1Analytical("12.*x+3.*y", ["x", "y"])
    f = Functions.R2ToR1Analytical("f(x, y)+2.*x+3.*y", ["x", "y"])
    f.addFunctor("f", a)
    fAPI = Functions.AsFunction(f, 2, 1)
    d1 = fAPI.getSerializable()
    d2 = f.getSerializable()
    assert(d1 == d2)
    assert(f(1., 2.) == 26.)
    assert(fAPI(1., 2.) == 26.)
    assert(Functions.AsFunction(fAPI.getSerializable(), 2, 1)(1., 2.) == 26.)
    d3 = json.loads(json.dumps(fAPI.getSerializable()))
    assert(Functions.AsFunction(d3, 1, 1)(1., 2.) == 26.)


if __name__ == '__main__':
    """Test part"""
    testSerializationR1ToR1UserDefined()
    testSerializationR2ToR1UserDefined()
    testSerializationR2ToR1Uniform()
    testSerializationR2ToR1Analystical()
    testSerializationR2ToR1AnalysticalDer()
    testSerializationR2ToR1AnalysticalFunctor()
