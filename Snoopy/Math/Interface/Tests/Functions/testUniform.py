#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy

from Opera.Math import Functions

def testUniformR1ToR1():
    a = Functions.R1ToR1Uniform(3.)
    assert a(2.) == 3.
    assert a.eval(2.) == 3.
    assert a.dEval(2.) == 0.
    assert a.dEval2(2.) == 0.
    
def testUniformR2ToR1():
    a = Functions.R2ToR1Uniform(3.)
    assert a(1., 2.) == 3.
    assert a.eval(1., 2.) == 3.
    assert a.eval(numpy.array((1., 2.), dtype=float)) == 3.
    assert numpy.allclose(a.dEval(1., 2.), numpy.zeros((2, 2), dtype=float))
    
def testUniformR1ToR2():
    a = Functions.R1ToR2Uniform(3., 4.)
    assert numpy.allclose(a(1.), [3., 4.])
    assert numpy.allclose(a.eval(1.), [3., 4.])
    assert numpy.allclose(a.dEval(1.), numpy.zeros((2, 2), dtype=float))
    
def testUniformR3ToR2():
    a = Functions.R3ToR2Uniform(3., 4.)
    assert numpy.allclose(a(1., 2., 3.), numpy.array((3., 4.), dtype=float))
    assert numpy.allclose(a.eval(1., 2., 3.), numpy.array((3., 4.), dtype=float))
    assert numpy.allclose(a.eval(numpy.array((1., 2., 3.), dtype=float)),
                          numpy.array((3., 4.), dtype=float))
    assert numpy.allclose(a.dEval(1., 2., 3.), numpy.zeros((2, 3), dtype=float))
