#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy

from Opera.Math import Functions

def testPolynomialR1ToR1():
    a = Functions.R1ToR1Polynomial(numpy.array((1., 2., 3.), dtype=float))
    assert abs(a(2.) - 17.) < 1.e-8
    assert abs(a.dEval(2.) - 14.) < 1.e-8
    assert abs(a.dEval2(2.) - 6.) < 1.e-8

def testPolynomialR1ToR2():
    a = Functions.R1ToR2Polynomial([numpy.array((1., 2., 3.), dtype=float),
                                      numpy.array((2., 3., 4.), dtype=float)])
    assert numpy.allclose(a(2.), numpy.array((17., 24.), dtype=float))
    assert numpy.allclose(a.dEval(2.), numpy.array((14., 19.), dtype=float))
    assert numpy.allclose(a.dEval2(2.), numpy.array((6., 8.), dtype=float))
