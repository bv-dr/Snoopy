#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy

from Opera.Math import Functions

def myFuncR1ToR1(val):
    return val + 1.

def myFuncR2ToR1(v1, v2):
    return v1 + v2 + 20.

def myFuncR2ToR1Array(vals):
    return vals.sum()

def myFuncR1ToR2(v1):
    return numpy.array((v1, v1), dtype=float)

def myFuncR3ToR2(v1, v2, v3):
    return numpy.array((v1+v2, v2+v3), dtype=float)

def myFuncR3ToR2Array(vals):
    return numpy.array((vals[0], vals[1]), dtype=float)

def testUserDefinedR1ToR1():
    a = Functions.R1ToR1UserDefined(myFuncR1ToR1)
    assert a(2.) == 3.
    assert a.eval(2.) == 3.
    
def testUserDefinedR2ToR1():
    a = Functions.R2ToR1UserDefined(myFuncR2ToR1Array)
    b = Functions.R2ToR1UserDefinedNArgs(myFuncR2ToR1)
    assert a(numpy.array((1., 2.), dtype=float)) == 3.
    assert b(1., 2.) == 23.
    assert b.eval(1., 2.) == 23.
    
def testUserDefinedR1ToR2():
    a = Functions.R1ToR2UserDefined(myFuncR1ToR2)
    assert numpy.allclose(a(1.), [1., 1.])
    assert numpy.allclose(a.eval(1.), [1., 1.])
    
def testUserDefinedR3ToR2():
    a = Functions.R3ToR2UserDefinedNArgs(myFuncR3ToR2)
    b = Functions.R3ToR2UserDefined(myFuncR3ToR2Array)
    assert numpy.allclose(a(1., 2., 3.), numpy.array((3., 5.), dtype=float))
    assert numpy.allclose(a.eval(1., 2., 3.), numpy.array((3., 5.), dtype=float))
    assert numpy.allclose(b(numpy.array((1., 2., 3.), dtype=float)),
                            numpy.array((1., 2.), dtype=float))

if __name__ == "__main__":
    testUserDefinedR1ToR1()
    testUserDefinedR1ToR2()
    testUserDefinedR2ToR1()
    testUserDefinedR3ToR2()