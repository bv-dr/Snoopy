#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy

from Opera.Math import Functions

def testAnalyticalR1ToR1():
    a = Functions.R1ToR1Analytical("12.*x+3.", "x")
    assert abs(a(2.) - 27.) < 1.e-8
    assert abs(a.dEval(2.) - 12.) < 1.e-6
    
def testAnalyticalR2ToR1():
    a = Functions.R2ToR1Analytical("12.*x+3.*y", ["x", "y"])
    assert a(1., 2.) == 18.
    assert numpy.allclose(a.dEval(1., 2.), numpy.array((12., 3.), dtype=float))
    assert numpy.allclose(a.dEval2(1., 2.), numpy.zeros((2, 2), dtype=float), 1.e-6, 1.e-6)
    
def testAnalyticalR2ToR1Der():
    der2x = Functions.R2ToR2Analytical(["0.", "0."], ["x", "y"])
    der2y = Functions.R2ToR2Analytical(["0.", "0."], ["x", "y"])
    der = Functions.R2ToR2Analytical(["12.", "3."], ["x", "y"], [der2x, der2y])
    a = Functions.R2ToR1Analytical("12.*x+3.*y", ["x", "y"], der)
    assert a(1., 2.) == 18.
    assert numpy.allclose(a.dEval(1., 2.), numpy.array((12., 3.), dtype=float))
    assert numpy.allclose(a.dEval2(1., 2.), numpy.zeros((2, 2), dtype=float))
    
def testAnalyticalR1ToR2():
    a = Functions.R1ToR2Analytical(["12.*x+3.", "x"], "x")
    assert numpy.allclose(a(2.), numpy.array((27., 2.), dtype=float))
    assert numpy.allclose(a.eval(2.), numpy.array((27., 2.), dtype=float))
    assert numpy.allclose(a.dEval(2.), numpy.array((12., 1.), dtype=float))
    
def testAnalyticalR3ToR2():
    der2F1x = Functions.R3ToR3Analytical(["0.", "0.", "0."], ["x", "y", "z"])
    der2F1y = Functions.R3ToR3Analytical(["0.", "0.", "0."], ["x", "y", "z"])
    der2F1z = Functions.R3ToR3Analytical(["0.", "0.", "0."], ["x", "y", "z"])
    der2F2x = Functions.R3ToR3Analytical(["0.", "1.", "0."], ["x", "y", "z"])
    der2F2y = Functions.R3ToR3Analytical(["1.", "0.", "0."], ["x", "y", "z"])
    der2F2z = Functions.R3ToR3Analytical(["0.", "0.", "0."], ["x", "y", "z"])
    derF1 = Functions.R3ToR3Analytical(["12.", "3.", "2."], ["x", "y", "z"],
                                        [der2F1x, der2F1y, der2F1z])
    derF2 = Functions.R3ToR3Analytical(["y", "x", "1."], ["x", "y", "z"],
                                         [der2F2x, der2F2y, der2F2z])
    a = Functions.R3ToR2Analytical(["12.*x+3.*y+2.*z", "x*y+z"],
                                     ["x", "y", "z"], [derF1, derF2])
    assert numpy.allclose(a.eval(1., 2., 3.), numpy.array((24., 5.), dtype=float))
    assert numpy.allclose(a.dEval(1., 2., 3.), numpy.array(((12., 3., 2.),
                                                            (2., 1., 1.)),
                                                           dtype=float))
    der2Comp = numpy.zeros((2, 3, 3), dtype=float)
    der2Comp[1, 0, 1] = 1.
    der2Comp[1, 1, 0] = 1.
    assert numpy.allclose(a.dEval2(1., 2., 3.), der2Comp)

def testAnalyticalR2ToR1Functor():
    a = Functions.R2ToR1Analytical("12.*x+3.*y", ["x", "y"])
    b = Functions.R2ToR1Analytical("f(x, y)+2.*x+3.*y", ["x", "y"])
    b.addFunctor("f", a)
    assert b(1., 2.) == 26.
    assert numpy.allclose(b.dEval(1., 2.), numpy.array((14., 6.), dtype=float))
    assert numpy.allclose(a.dEval2(1., 2.), numpy.zeros((2, 2), dtype=float), 1.e-6, 1.e-6)
