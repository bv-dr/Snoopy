#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy

from Opera.Math import Functions

def testDiscreteR1ToR1():
    a = Functions.R1ToR1Discrete(numpy.arange(10, dtype=float),
                                   numpy.arange(10, dtype=float)+2.)
    assert abs(a(1.5) - 3.5) < 1.e-8
    assert abs(a.dEval(1.5) - 1.) < 1.e-8
    assert a.dEval2(1.5) < 1.e-8

def testDiscreteR2ToR1():
    a = Functions.R2ToR1Discrete(numpy.arange(10, dtype=float),
                                   numpy.arange(10, dtype=float),
                                   numpy.ones((10, 10), dtype=float))
    assert abs(a(1.5, 1.5) - 1.) < 1.e-8
    assert numpy.allclose(a.dEval(1.5, 1.5), numpy.zeros(2, dtype=float))

