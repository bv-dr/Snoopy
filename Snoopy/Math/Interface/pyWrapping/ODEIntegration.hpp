#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <memory>

#include "Math/ODEIntegration/Integrate.hpp"
#include "Math/Integration/ODE/Integrate.hpp"
#include "Math/Integration/ODE/Integrable.hpp"
#include "Math/Integration/ODE/Steppers/StepperABC.hpp"
#include "Math/Integration/ODE/Steppers/Euler.hpp"
#include "Math/Integration/ODE/Steppers/RK2.hpp"
#include "Math/Integration/ODE/Steppers/RK4.hpp"
#include "Math/Integration/ODE/Steppers/HPCG.hpp"
#include "Math/Integration/ODE/Steppers/Dopri5.hpp"
#include "Math/Integration/ODE/Steppers/HHT.hpp"
#include "Math/ODEIntegration/HPCG/HPCG.hpp"

namespace BV {
namespace PythonInterface {
namespace Math {
namespace Integration {
namespace ODE {

using namespace BV::Math::Integration::ODE ;
namespace py = pybind11 ;
using namespace py::literals;

void exportModule(pybind11::module & m)
{

    py::class_<IntegrableABC, std::shared_ptr<IntegrableABC> >(m, "IntegrableABC")
        // TODO expose API
    ;

    py::class_<Steppers::ObserverABC>(m, "ObserverABC")
        // TODO expose API
    ;

    py::register_exception<Steppers::StepperException>(m, "StepperException") ;

    py::class_<Steppers::StepperABC,
               std::shared_ptr<Steppers::StepperABC> >(m, "StepperABC")
        .def("setIntegrable", &Steppers::StepperABC::setIntegrable)
        .def("setup", &Steppers::StepperABC::setup)
        .def("advance", &Steppers::StepperABC::advance)
        .def("getTime", &Steppers::StepperABC::getTime)
        .def("setAdaptiveStepParameters", &Steppers::StepperABC::setAdaptiveStepParameters)
        // TODO observe?
    ;

    py::enum_<Steppers::StepperType>(m, "StepperType")
        .value("EULER", Steppers::StepperType::EULER)
        .value("RK2", Steppers::StepperType::RK2)
        .value("RK4", Steppers::StepperType::RK4)
        .value("HPCG", Steppers::StepperType::HPCG)
        .value("ERK_DOPRI5", Steppers::StepperType::ERK_DOPRI5)
        .value("HHT", Steppers::StepperType::HHT)
    ;

    py::class_<Steppers::Euler, Steppers::StepperABC,
               std::shared_ptr<Steppers::Euler> >(m, "Euler")
        .def(py::init<>())
        .def(py::init<IntegrableABC &>())
    ;

    py::class_<Steppers::RK2, Steppers::StepperABC,
               std::shared_ptr<Steppers::RK2> >(m, "RK2")
        .def(py::init<>())
        .def(py::init<IntegrableABC &>())
    ;

    py::class_<Steppers::RK4, Steppers::StepperABC,
               std::shared_ptr<Steppers::RK4> >(m, "RK4")
        .def(py::init<>())
        .def(py::init<IntegrableABC &>())
    ;

    py::class_<Steppers::HPCG, Steppers::StepperABC,
               std::shared_ptr<Steppers::HPCG> >(m, "HPCG")
        .def(py::init<>())
        .def(py::init<IntegrableABC &>())
    ;

    py::class_<Steppers::Dopri5, Steppers::StepperABC,
               std::shared_ptr<Steppers::Dopri5> >(m, "Dopri5")
        .def(py::init<>())
        .def(py::init<IntegrableABC &>())
    ;

    py::enum_<Steppers::HHT_MODE>(m, "HHT_MODE")
        .value("POSITION", Steppers::HHT_MODE::POSITION)
        .value("ACCELERATION", Steppers::HHT_MODE::ACCELERATION)
    ;

    py::enum_<Steppers::SolverType>(m, "SolverType")
        .value("DENSE", Steppers::SolverType::DENSE)
        .value("SPARSE", Steppers::SolverType::SPARSE)
        .value("SCALED_SPARSE", Steppers::SolverType::SCALED_SPARSE)
    ;

    py::enum_<Steppers::HHTPredictionMode>(m, "HHTPredictionMode")
        .value("CONSTANT_ACCELERATION", Steppers::HHTPredictionMode::CONSTANT_ACCELERATION)
        .value("CONSTANT_POSITION", Steppers::HHTPredictionMode::CONSTANT_POSITION)
        .value("CONSTANT_DISPLACEMENT", Steppers::HHTPredictionMode::CONSTANT_DISPLACEMENT)
    ;

    py::class_<Steppers::HHT, Steppers::StepperABC,
               std::shared_ptr<Steppers::HHT> >(m, "HHT")
        .def(py::init<>())
        .def(py::init<IntegrableABC &>())
        .def("setMode", &Steppers::HHT::setMode)
        .def("setAlpha", &Steppers::HHT::setAlpha)
        .def("setIncrementTolerances", &Steppers::HHT::setIncrementTolerances)
        .def("setResidualTolerance", &Steppers::HHT::setResidualTolerance)
        .def("setLoadsTolerance", &Steppers::HHT::setLoadsTolerance)
        .def("setMaxIterations", &Steppers::HHT::setMaxIterations)
        .def("setMaxNIterationsBeforeStepIncrease", &Steppers::HHT::setMaxNIterationsBeforeStepIncrease)
        .def("setNSuccessfulStepsBeforeStepIncrease", &Steppers::HHT::setNSuccessfulStepsBeforeStepIncrease)
        .def("setErrorThresholdBeforeStepIncrease", &Steppers::HHT::setErrorThresholdBeforeStepIncrease)
        .def("setNIterationsThresholdBeforeStepDecrease", &Steppers::HHT::setNIterationsThresholdBeforeStepDecrease)
        .def("setPredictionMode", &Steppers::HHT::setPredictionMode)
        .def("setSolverType", &Steppers::HHT::setSolverType)
    ;

    m.def("GetStepper", &GetStepper, "stepperType"_a, "integrable"_a=nullptr) ;

    m.def("Integrate",
          [](const Steppers::StepperType stepperType,
             IntegrableABC & integrable,
             double xStart, double xEnd, double xStep,
             Steppers::ObserverABC & observer) -> void
          {
              return Integrate(stepperType, integrable,
                               xStart, xEnd, xStep, observer) ;
          }) ;

    m.def("Integrate",
          [](const Steppers::StepperType stepperType,
             IntegrableABC & integrable,
             double xStart, double xEnd, double xStep,
             Steppers::ObserverABC & observer,
             bool updateStateEachStep) -> void
          {
              return Integrate(stepperType, integrable,
                               xStart, xEnd, xStep, observer,
                               updateStateEachStep) ;
          }) ;

    m.def("Integrate",
          [](std::shared_ptr<Steppers::StepperABC> p_stepper,
             double xStart, double xEnd, double xStep,
             Steppers::ObserverABC & observer) -> void
          {
              return Integrate(p_stepper, xStart, xEnd, xStep, observer) ;
          }) ;

    m.def("Integrate",
          [](std::shared_ptr<Steppers::StepperABC> p_stepper,
             double xStart, double xEnd, double xStep,
             Steppers::ObserverABC & observer,
             bool updateStateEachStep) -> void
          {
              return Integrate(p_stepper, xStart, xEnd, xStep, observer,
                               updateStateEachStep) ;
          }) ;

    namespace Old = BV::Math::ODEIntegration ;

    py::enum_<Old::Steppers::StepperScheme>(m, "StepperScheme")
        .value("EULER",                     Old::Steppers::StepperScheme::EULER)
        .value("HEUN",                      Old::Steppers::StepperScheme::HEUN)
        .value("RK2",                       Old::Steppers::StepperScheme::RK2)
        .value("RK_RALSTON_2",              Old::Steppers::StepperScheme::RK_RALSTON_2)
        .value("RK3",                       Old::Steppers::StepperScheme::RK3)
        .value("RK4",                       Old::Steppers::StepperScheme::RK4)
        .value("RK_RALSTON_4",              Old::Steppers::StepperScheme::RK_RALSTON_4)
        .value("RK3_8",                     Old::Steppers::StepperScheme::RK3_8)
        .value("GILL4",                     Old::Steppers::StepperScheme::GILL4)
        .value("NYSTROM5",                  Old::Steppers::StepperScheme::NYSTROM5)
        .value("BUTCHER6",                  Old::Steppers::StepperScheme::BUTCHER6)
        .value("VERNER8",                   Old::Steppers::StepperScheme::VERNER8)
        .value("ERK_CASH_KARP54",           Old::Steppers::StepperScheme::ERK_CASH_KARP54)
        .value("ERK_DOPRI5",                Old::Steppers::StepperScheme::ERK_DOPRI5)
        .value("ERK_DOPRI78",               Old::Steppers::StepperScheme::ERK_DOPRI78)
        .value("ERK_DOPRI87",               Old::Steppers::StepperScheme::ERK_DOPRI87)
        .value("ERK_FEHLBERG34",            Old::Steppers::StepperScheme::ERK_FEHLBERG34)
        .value("ERK_FEHLBERG43",            Old::Steppers::StepperScheme::ERK_FEHLBERG43)
        .value("ERK_FEHLBERG45",            Old::Steppers::StepperScheme::ERK_FEHLBERG45)
        .value("ERK_FEHLBERG54",            Old::Steppers::StepperScheme::ERK_FEHLBERG54)
        .value("ERK_FEHLBERG56",            Old::Steppers::StepperScheme::ERK_FEHLBERG56)
        .value("ERK_FEHLBERG65",            Old::Steppers::StepperScheme::ERK_FEHLBERG65)
        .value("ERK_FEHLBERG78",            Old::Steppers::StepperScheme::ERK_FEHLBERG78)
        .value("ERK_FEHLBERG87",            Old::Steppers::StepperScheme::ERK_FEHLBERG87)
        .value("ERK_VERNER56",              Old::Steppers::StepperScheme::ERK_VERNER56)
        .value("ERK_VERNER65",              Old::Steppers::StepperScheme::ERK_VERNER65)
        .value("ERK_VERNER67",              Old::Steppers::StepperScheme::ERK_VERNER67)
        .value("ERK_VERNER76",              Old::Steppers::StepperScheme::ERK_VERNER76)
        .value("ERK_VERNER78",              Old::Steppers::StepperScheme::ERK_VERNER78)
        .value("ERK_VERNER87",              Old::Steppers::StepperScheme::ERK_VERNER87)
        .value("ERK_VERNER89",              Old::Steppers::StepperScheme::ERK_VERNER89)
        .value("ERK_VERNER98",              Old::Steppers::StepperScheme::ERK_VERNER98)
        .value("ADAMS_BASHFORTH_1",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_1)
        .value("ADAMS_BASHFORTH_2",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_2)
        .value("ADAMS_BASHFORTH_3",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_3)
        .value("ADAMS_BASHFORTH_4",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_4)
        .value("ADAMS_BASHFORTH_5",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_5)
        .value("ADAMS_BASHFORTH_6",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_6)
        .value("ADAMS_BASHFORTH_7",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_7)
        .value("ADAMS_BASHFORTH_8",         Old::Steppers::StepperScheme::ADAMS_BASHFORTH_8)
        .value("ADAMS_BASHFORTH_MOULTON_1", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_1)
        .value("ADAMS_BASHFORTH_MOULTON_2", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_2)
        .value("ADAMS_BASHFORTH_MOULTON_3", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_3)
        .value("ADAMS_BASHFORTH_MOULTON_4", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_4)
        .value("ADAMS_BASHFORTH_MOULTON_5", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_5)
        .value("ADAMS_BASHFORTH_MOULTON_6", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_6)
        .value("ADAMS_BASHFORTH_MOULTON_7", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_7)
        .value("ADAMS_BASHFORTH_MOULTON_8", Old::Steppers::StepperScheme::ADAMS_BASHFORTH_MOULTON_8)
        .value("HPCG",                      Old::Steppers::StepperScheme::HPCG)
        .value("IMPLICIT_EULER",            Old::Steppers::StepperScheme::IMPLICIT_EULER)
        .value("IMPLICIT_RK2",              Old::Steppers::StepperScheme::IMPLICIT_RK2)
        .value("IMPLICIT_RK4",              Old::Steppers::StepperScheme::IMPLICIT_RK4)
        .value("CRANK_NICOLSON",            Old::Steppers::StepperScheme::CRANK_NICOLSON)
        .value("GENERALIZED_ALPHA",         Old::Steppers::StepperScheme::GENERALIZED_ALPHA)
        .value("BULIRSCH_STOER",            Old::Steppers::StepperScheme::BULIRSCH_STOER)
    ;

    py::class_<Old::ODEIntegrationParameters<double, double> >(m, "ODEIntegrationParameters")
        .def(py::init<Old::Steppers::StepperScheme>(),
             R"rst(
             Initialisation of the integration parameters given a stepper
             scheme.
             By default the integration uses:

            - an adaptive step
            - an absolute tolerance of 1.e-6
            - a relative tolerance of 1.e-6
            - a scale y error of 1.
            - a scale dy error of 0.
            - a maximum step size of 200.

             :param stepper: a :py:class:`~_Math.ODEIntegration.StepperScheme`
                 value.
             )rst")
        .def(py::init<Old::Steppers::StepperScheme, bool>(),
             R"rst(
             Initialisation of the integration parameters given a stepper
             scheme and whether to use an adaptive step.

             :param stepper: a :py:class:`~_Math.ODEIntegration.StepperScheme`
                 value.
             :param bool useAdaptive: if True, the integration will use an
                 adaptive step, or a fixed step otherwise.
             )rst")
        .def(py::init<Old::Steppers::StepperScheme, bool, double, double>(),
             R"rst(
             Initialisation of the integration parameters given a stepper
             scheme, whether to use an adaptive step and the tolerances.

             :param stepper: a :py:class:`~_Math.ODEIntegration.StepperScheme`
                 value.
             :param bool useAdaptive: if True, the integration will use an
                 adaptive step, or a fixed step otherwise.
             :param float aTol: the absolute error tolerance when computing
                 the error for step adaptation.
             :param float rTol: the relative error tolerance when computing
                 the error for step adaptation.
             )rst")
        .def(py::init<Old::Steppers::StepperScheme, bool, double, double, double, double, double>(),
             R"rst(
             Initialisation of the integration parameters given a stepper
             scheme, whether to use an adaptive step and the tolerances.


             :param stepper: a :py:class:`~_Math.ODEIntegration.StepperScheme`
                 value.
             :param bool useAdaptive: if True, the integration will use an
                 adaptive step, or a fixed step otherwise.
             :param float aTol: the absolute error tolerance when computing
                 the error for step adaptation.
             :param float rTol: the relative error tolerance when computing
                 the error for step adaptation.
             :param float scaleYError: the scale on the state vector error
             :param float scaleDYError: the scale on the derivative vector error
             :param float maxStepSize: the maximum allowed step size when
                 adaptive stepping is used.
             )rst")
         .def("getStepperScheme", &Old::ODEIntegrationParameters<double, double>::getStepperScheme)
         .def("getUseAdaptive", &Old::ODEIntegrationParameters<double, double>::getUseAdaptive)
         .def("getATol", &Old::ODEIntegrationParameters<double, double>::getATol)
         .def("getRTol", &Old::ODEIntegrationParameters<double, double>::getRTol)
         .def("getScaleYError", &Old::ODEIntegrationParameters<double, double>::getScaleYError)
         .def("getScaleDYError", &Old::ODEIntegrationParameters<double, double>::getScaleDYError)
         .def("getMaxStepSize", &Old::ODEIntegrationParameters<double, double>::getMaxStepSize)
    ;

}

} // End of namespace ODE
} // End of namespace Integration
} // End of namespace Math
} // End of namespace PythonInterface
} // End of namespace BV
