#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "Math/Solvers/SolverParameters.hpp"
#include "Math/Solvers/Types.hpp"

namespace BV {
namespace PythonInterface {
namespace Math {
namespace Solvers {

using namespace BV::Math::Solvers ;
namespace py = pybind11 ;

void exportModule(pybind11::module & m)
{
    py::enum_<SolverType>(m, "SolverType")
        .value("NEWTON", SolverType::NEWTON)
    ;

    py::class_<SolverParameters>(m, "SolverParameters")
        .def(py::init<>(),
             R"rst(
             Initialisation of the solver parameters with default values.

             The default values can be modified using the setters.

             Below is a list of the parameters name, setter method,
             default value and explanation:

             - maxIterations, setUnsigned, :math:`20`, the maximum number of iterations
                  allowed before aborting.
             - absoluteError, setDouble, :math:`1.e-4`, the absolute error between
                  the objective and the solver result.
             - verbose, setBool, False, whether to print information on the
                  intermediate iterations.
             - jacobianDelta, setDouble, :math:`1.e-4`, if a jacobian is calculated
                  numerically, what is the delta used in the finite difference.
             - computeJOnce, setBool, False, should the solver compute the
                  jacobian only at the first iteration and use it for the others
                  or not.

             )rst")
        .def("setFloat", py::overload_cast<const std::string &, const double &>(&SolverParameters::set),
             R"rst(
             Set a float parameter.

             :param name: the name of the parameter to set.
             :param float value: the value of the parameter.
             )rst")
        .def("setUnsigned", py::overload_cast<const std::string &, const unsigned &>(&SolverParameters::set),
             R"rst(
             Set an unsigned parameter. This type does not exist in python
             but there is a distinction in c++.

             :param name: the name of the parameter to set.
             :param int value: the value of the parameter.
             )rst")
        .def("setInt", py::overload_cast<const std::string &, const int &>(&SolverParameters::set),
             R"rst(
             Set an int parameter.

             :param name: the name of the parameter to set.
             :param int value: the value of the parameter.
             )rst")
        .def("setBool", py::overload_cast<const std::string &, const bool &>(&SolverParameters::set),
             R"rst(
             Set a bool parameter.

             :param name: the name of the parameter to set.
             :param bool value: the value of the parameter.
             )rst")
        .def("setString", py::overload_cast<const std::string &, const std::string &>(&SolverParameters::set),
             R"rst(
             Set a string parameter.

             :param name: the name of the parameter to set.
             :param string value: the value of the parameter.
             )rst")
    ;
}

} // End of namespace Solvers
} // End of namespace Math
} // End of namespace PythonInterface
} // End of namespace BV
