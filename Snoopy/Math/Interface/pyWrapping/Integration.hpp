#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include <Eigen/Dense>

#include "Math/Integration/simpson.hpp"

using namespace BV::Math::Integration;

namespace BV {
	namespace PythonInterface {
		namespace Math {
			namespace Integration {

				namespace py = pybind11;
				void exportModule(pybind11::module & m)
				{

                    m.def("simps",&simps,
						R"rst(Integrate with Simpson rules
    		            :param y: Evalution of f(x)
                        :param x: Point at which the function is evaluated
                        :return: the integral $\int_x f(x) dx$
                        )rst");


                    m.def("trapz", py::overload_cast<const Eigen::ArrayXd&, const Eigen::ArrayXd&>(&trapz),
						R"rst(Integrate with trapezoidal rule
    		            :param y: Evalution of f(x)
                        :param x: Point at which the function is evaluated
                        :return: the integral $\int_x f(x) dx$
                        )rst");

                    m.def("trapz", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXd>&, const double&>(&trapz),
                        R"rst(Integrate with trapezoidal rule a one variable function with equally distributed points
                        :param f: 1D array of $f(x_i)$
                        :param dx: $\Delta x$
                        :return: $\int f(x) dx$
                        )rst)");

                    m.def("trapz", py::overload_cast<const Eigen::Ref<const Eigen::ArrayXXd>&, const double&, const double&>(&trapz),
                        R"rst(Integrate with trapezoidal rule a two variable function with equally distributed points
                        :param f_xy: 2D array of $f(x_i,y_j)$
                        :param dx: $\Delta x$
                        :param dy: $\Delta y$
                        :return: $\int\int f(x,y) dx dy$
                        )rst");

                }
			} // End of namespace DspFilters
		} // End of namespace Math
	} // End of namespace PythonInterface
} // End of namespace BV
