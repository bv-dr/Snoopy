#pragma once

#include <memory>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include <Eigen/Dense>

#include "Math/DspFilters/Butterworth.h"
#include "Math/DspFilters/ChebyshevI.h"
#include "Math/DspFilters/Filter.h"
#include "Math/DspFilters/Params.h"
#include "Math/DspFilters/Utilities.h"

namespace BV {
namespace PythonInterface {
namespace Math {
namespace DspFilters {

namespace py = pybind11 ;

void exportModule(pybind11::module & m)
{

    py::enum_<Dsp::ParamID>(m, "ParamId")
            .value("idSampleRate", Dsp::ParamID::idSampleRate)
            .value("idFrequency", Dsp::ParamID::idFrequency)
            .value("idQ", Dsp::ParamID::idQ)
            .value("idBandwidth", Dsp::ParamID::idBandwidth)
            .value("idBandwidthHz", Dsp::ParamID::idBandwidthHz)
            .value("idGain", Dsp::ParamID::idGain)
            .value("idSlope", Dsp::ParamID::idSlope)
            .value("idOrder", Dsp::ParamID::idOrder)
            .value("idRippleDb", Dsp::ParamID::idRippleDb)
            .value("idStopDb", Dsp::ParamID::idStopDb)
            .value("idRolloff", Dsp::ParamID::idRolloff)
            .value("idPoleRho", Dsp::ParamID::idPoleRho)
            .value("idPoleTheta", Dsp::ParamID::idPoleTheta)
            .value("idZeroRho", Dsp::ParamID::idZeroRho)
            .value("idZeroTheta", Dsp::ParamID::idZeroTheta)
            .value("idPoleReal", Dsp::ParamID::idPoleReal)
            .value("idZeroReal", Dsp::ParamID::idZeroReal)
    ;

    py::class_<Dsp::ParamInfo>(m, "ParamInfo")
        .def(py::init<>())
        .def("getLabel", &Dsp::ParamInfo::getLabel)
        .def("getName", &Dsp::ParamInfo::getName)
    ;

    py::class_<Dsp::Params>(m, "Params")
        .def(py::init<>())
        .def("clear", &Dsp::Params::clear)
    ;

    py::class_<Dsp::Filter,
               std::shared_ptr<Dsp::Filter> >(m, "Filter")
        .def("setParams", &Dsp::Filter::setParams)
        .def("setParamById", &Dsp::Filter::setParam)
        .def("getParams", &Dsp::Filter::getParams)
        .def("getDefaultParams", &Dsp::Filter::getDefaultParams)
        .def("getParamById", &Dsp::Filter::getParam)
        .def("getParamInfo", &Dsp::Filter::getParamInfo)
        .def("getNumParams", &Dsp::Filter::getNumParams)
        .def("getNumChannels", &Dsp::Filter::getNumChannels)
        .def("reset", &Dsp::Filter::reset)
        .def("process",
             [](Dsp::Filter & f, int numSamples, Eigen::MatrixXd & vectorOfChannels)
             {
                 double * doubleOfChannels(vectorOfChannels.data()) ;
                 double * const * arrayOfChannels(& doubleOfChannels) ;
                  f.process(numSamples, arrayOfChannels) ;
             })
    ;

    py::class_<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <1>, 12>,
               std::shared_ptr<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <1>, 12> >,
               Dsp::Filter>(m, "ButterworthLowPassOrder1",
                           R"rst(
                           Butterworth Low Pass Order 1
                           )rst")
        .def(py::init<>())
    ;
    py::class_<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <2>, 12>,
               std::shared_ptr<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <2>, 12> >,
               Dsp::Filter>(m, "ButterworthLowPassOrder2",
                           R"rst(
                           Butterworth Low Pass Order 2
                           )rst")
        .def(py::init<>())
    ;
    py::class_<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <3>, 12>,
               std::shared_ptr<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <3>, 12> >,
               Dsp::Filter>(m, "ButterworthLowPassOrder3",
                           R"rst(
                           Butterworth Low Pass Order 3
                           )rst")
        .def(py::init<>())
    ;
    py::class_<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <4>, 12>,
               std::shared_ptr<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <4>, 12> >,
               Dsp::Filter>(m, "ButterworthLowPassOrder4",
                           R"rst(
                           Butterworth Low Pass Order 4
                           )rst")
        .def(py::init<>())
    ;
    py::class_<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <5>, 12>,
               std::shared_ptr<Dsp::FilterDesign<Dsp::Butterworth::Design::LowPass <5>, 12> >,
               Dsp::Filter>(m, "ButterworthLowPassOrder5",
                           R"rst(
                           Butterworth Low Pass Order 5
                           )rst")
        .def(py::init<>())
    ;

}

} // End of namespace DspFilters
} // End of namespace Math
} // End of namespace PythonInterface
} // End of namespace BV
