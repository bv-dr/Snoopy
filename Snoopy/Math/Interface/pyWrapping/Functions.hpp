#pragma once

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>

#include "Math/Functions/ABC.hpp"
#include "Math/Functions/Analytical.hpp"
#include "Math/Functions/Composite.hpp"
#include "Math/Functions/Discrete.hpp"
#include "Math/Functions/Polynomial.hpp"
#include "Math/Functions/Uniform.hpp"
#include "Math/Functions/UserDefined.hpp"
#include "Math/Functions/Zero.hpp"
#include "Math/Functions/Step.hpp"
#include "Math/Functions/Current.hpp"
#include "Math/Functions/ScaledAxis.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Tools/TensorPythonCaster.hpp"

namespace BV {
namespace PythonInterface {
namespace Math {
namespace Functions {

using namespace BV::Math::Functions ;
namespace py = pybind11 ;

template <int N, int M, typename DataType>
struct DefineAdditionalEval_ ;

template <typename DataType>
struct DefineAdditionalEval_<2, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 2 ;
        scope.def("__call__", (const DataType & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Evaluation of the function with 2 scalars.

                  :param scalar1: the first scalar needed for the function evaluation
                  :param scalar2: the second scalar needed for the function evaluation
                  :return: a scalar, the result of the function evaluation
                  )rst")
            .def("dEval", (const Eigen::Matrix<DataType, N, 1> & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 2 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of 2 values, the result
                     of the function first derivative evaluation according to
                     the 2 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, N, N> & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 2 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :return: a 2x2 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 2 function variables.
                 )rst")
        ;
    }
} ;

template <typename DataType>
struct DefineAdditionalEval_<3, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 3 ;
        scope.def("__call__", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Evaluation of the function with 3 scalars.

                  :param scalar1: the first scalar needed for the function evaluation
                  :param scalar2: the second scalar needed for the function evaluation
                  :param scalar3: the third scalar needed for the function evaluation
                  :return: a scalar, the result of the function evaluation
                  )rst")
            .def("dEval", (const Eigen::Matrix<DataType, N, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 3 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of 3 values, the result
                     of the function first derivative evaluation according to
                     the 3 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, N, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 3 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :return: a 3x3 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 3 function variables.
                 )rst")
        ;
    }
} ;

template <typename DataType>
struct DefineAdditionalEval_<4, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 4 ;
        scope.def("__call__", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 4 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :param scalar4: the fourth scalar needed for the function evaluation
                 :return: a scalar, the result of the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, N, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 4 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of 4 values, the result
                     of the function first derivative evaluation according to
                     the 4 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, N, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 4 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function second
                     derivative evaluation
                 :return: a 4x4 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 4 function variables.
                 )rst")
        ;
    }
} ;

template <typename DataType>
struct DefineAdditionalEval_<5, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 5 ;
        scope.def("__call__", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 5 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :param scalar4: the fourth scalar needed for the function evaluation
                 :param scalar5: the fifth scalar needed for the function evaluation
                 :return: a scalar, the result of the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, N, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 5 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function first
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of 5 values, the result
                     of the function first derivative evaluation according to
                     the 5 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, N, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 5 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function second
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function second
                     derivative evaluation
                 :return: a 5x5 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 5 function variables.
                 )rst")
        ;
    }
} ;


template <typename DataType>
struct DefineAdditionalEval_<6, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 6 ;
        scope.def("__call__", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 6 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :param scalar4: the fourth scalar needed for the function evaluation
                 :param scalar5: the fifth scalar needed for the function evaluation
                 :param scalar6: the sixth scalar needed for the function evaluation
                 :return: a scalar, the result of the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, N, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 6 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function first
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function first
                     derivative evaluation
                 :param scalar6: the sixth scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of 6 values, the result
                     of the function first derivative evaluation according to
                     the 6 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, N, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 6 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function second
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function second
                     derivative evaluation
                 :param scalar6: the sixth scalar needed for the function second
                     derivative evaluation
                 :return: a 6x6 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 6 function variables.
                 )rst")
        ;
    }
} ;

template <int M, typename DataType>
struct DefineAdditionalEval_<2, M, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 2 ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 2 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :return: a :py:class:`numpy.ndarray` of M values, the result of the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, N> & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 2 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :return: a Mx2 :py:class:`numpy.ndarray`, the result
                     of the function first derivatives evaluation according to
                     the 2 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Tensor<DataType, 3> & (ClassType::*)(const DataType &, const DataType &) const) &ClassType::dEval2 , py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 2 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :return: a Mx2x2 :py:class:`numpy.ndarray`, the result
                     of the function second derivatives evaluation according to
                     the 2 function variables.
                 )rst")
        ;
    }
} ;

template <int M, typename DataType>
struct DefineAdditionalEval_<3, M, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 3 ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 3 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :return: a :py:class:`numpy.ndarray` of M values, the result of the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 3 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :return: a Mx3 :py:class:`numpy.ndarray`, the result
                     of the function first derivative evaluation according to
                     the 3 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Tensor<DataType, 3> & (ClassType::*)(const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2 , py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 3 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :return: a Mx3x3 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 3 function variables.
                 )rst")
        ;
    }
} ;

template <int M, typename DataType>
struct DefineAdditionalEval_<4, M, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 4 ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 4 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :param scalar4: the fourth scalar needed for the function evaluation
                 :return: a :py:class:`numpy.ndarray` of M values, the result of
                     the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 4 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function first
                     derivative evaluation
                 :return: a Mx4 :py:class:`numpy.ndarray`, the result
                     of the function first derivative evaluation according to
                     the 4 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Tensor<DataType, 3> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2 , py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 4 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function second
                     derivative evaluation
                 :return: a Mx4x4 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 4 function variables.
                 )rst")
        ;
    }
} ;

template <int M, typename DataType>
struct DefineAdditionalEval_<5, M, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 5 ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 5 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :param scalar4: the fourth scalar needed for the function evaluation
                 :param scalar5: the fifth scalar needed for the function evaluation
                 :return: a :py:class:`numpy.ndarray` of M values, the result of
                     the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 5 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function first
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of Mx5 values, the result
                     of the function first derivative evaluation according to
                     the 5 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Tensor<DataType, 3> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2 , py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 5 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function second
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function second
                     derivative evaluation
                 :return: a Mx5x5 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 5 function variables.
                 )rst")
        ;
    }
} ;


template <int M, typename DataType>
struct DefineAdditionalEval_<6, M, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        constexpr int N = 6 ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with 6 scalars.

                 :param scalar1: the first scalar needed for the function evaluation
                 :param scalar2: the second scalar needed for the function evaluation
                 :param scalar3: the third scalar needed for the function evaluation
                 :param scalar4: the fourth scalar needed for the function evaluation
                 :param scalar5: the fifth scalar needed for the function evaluation
                 :param scalar6: the sixth scalar needed for the function evaluation
                 :return: a :py:class:`numpy.ndarray` of M values, the result of
                     the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, N> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 6 scalars.

                 :param scalar1: the first scalar needed for the function first
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function first
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function first
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function first
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function first
                     derivative evaluation
                 :param scalar6: the sixth scalar needed for the function first
                     derivative evaluation
                 :return: a :py:class:`numpy.ndarray` of Mx6 values, the result
                     of the function first derivative evaluation according to
                     the 6 function variables.
                 )rst")
            .def("dEval2", (const Eigen::Tensor<DataType, 3> & (ClassType::*)(const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &) const) &ClassType::dEval2 , py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 6 scalars.

                 :param scalar1: the first scalar needed for the function second
                     derivative evaluation
                 :param scalar2: the second scalar needed for the function second
                     derivative evaluation
                 :param scalar3: the third scalar needed for the function second
                     derivative evaluation
                 :param scalar4: the fourth scalar needed for the function second
                     derivative evaluation
                 :param scalar5: the fifth scalar needed for the function second
                     derivative evaluation
                 :param scalar6: the sixth scalar needed for the function second
                     derivative evaluation
                 :return: a Mx6x6 :py:class:`numpy.ndarray`, the result
                     of the function second derivative evaluation according to
                     the 6 function variables.
                 )rst")
        ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionEval_
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function providing a vector of N values.

                 :param values: a :py:class:`numpy.ndarray` of N values, the
                     input values for the function evaluation.
                 :return: a :py:class:`numpy.ndarray` of M values, the result
                     of the function evaluation for the N input values provided.
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, N> & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 a vector of N values.

                 :param values: a :py:class:`numpy.ndarray` of N values, the
                      input values for the function first derivative evaluation
                 :return: a MxN :py:class:`numpy.ndarray`, the result
                      of the function first derivatives evaluation according to
                      the N function variables.
                 )rst")
            .def("dEval2", (const Eigen::Tensor<DataType, 3> & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 a vector of N values.

                 :param values: a :py:class:`numpy.ndarray` of N values, the
                      input values for the function second derivative evaluation
                 :return: a MxNxN :py:class:`numpy.ndarray`, the result
                     of the function second derivatives evaluation according to
                     the N function variables.
                 )rst")
        ;
        DefineAdditionalEval_<N, M, DataType>::define(scope) ;
    }
} ;

template <typename DataType>
struct DefineFunctionEval_<1, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        scope.def("__call__", (const DataType & (ClassType::*)(const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with a scalar.

                 :param scalar: the scalar needed for the function evaluation
                 :return: a scalar, the result of the function evaluation
                 )rst")
            .def("dEval", (const DataType & (ClassType::*)(const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 a scalar.
                 
                 :param scalar: the scalar needed for the function first
                     derivative evaluation
                 :return: a scalar, the result of the function first derivative
                     evaluation according to the function variable.
                 )rst")
            .def("dEval2", (const DataType & (ClassType::*)(const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 a scalar.

                 :param scalar: the scalar needed for the function second
                     derivative evaluation
                 :return: a scalar, the result of the function second derivative
                     evaluation according to the function variable.
                 )rst")
        ;
    }
} ;

template <int N, typename DataType>
struct DefineFunctionEval_<N, 1, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        scope.def("__call__", (const DataType & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const DataType & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function with N scalars.

                 :param values: a :py:class:`numpy.ndarray` of N values needed
                     for the function evaluation.
                 :return: a scalar, the result of the function evaluation
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, N, 1> & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the first derivative of the function providing
                 a vector of N values.

                 :param values: a :py:class:`numpy.ndarray` of N values needed
                     for the function first derivative evaluation.
                 :return: a :py:class:`numpy.ndarray` of N values, the result of
                     the function first derivative evaluation according to the
                     N variables.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, N, N> & (ClassType::*)(const Eigen::Matrix<DataType, N, 1> &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the second derivative of the function providing
                 a vector of N values.

                 :param values: a :py:class:`numpy.ndarray` of N values needed
                     for the function second derivative evaluation.
                 :return: a NxN :py:class:`numpy.ndarray`, the result of
                     the function second derivative evaluation according to the
                     N variables.
                 )rst")
        ;
        DefineAdditionalEval_<N, 1, DataType>::define(scope) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionEval_<1, M, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        using ClassType = typename Scope::type ;
        scope.def("__call__", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                  R"rst(
                  Same as :py:meth:`eval`.
                  )rst")
            .def("eval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &) const) &ClassType::eval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function providing a scalar.

                 :param scalar: the scalar needed for the function evaluation.
                 :return: a :py:class:`numpy.ndarray` of M values, the result
                     of the function evaluation with the scalar input value.
                 )rst")
            .def("dEval", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &) const) &ClassType::dEval, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function first derivative providing a scalar.

                 :param scalar: the input value for which the function first
                     derivative should be evaluated.
                 :return: a :py:class:`numpy.ndarray` of M values, the result
                     of the function first derivative evaluation according
                     to providing scalar.
                 )rst")
            .def("dEval2", (const Eigen::Matrix<DataType, M, 1> & (ClassType::*)(const DataType &) const) &ClassType::dEval2, py::return_value_policy::copy,
                 R"rst(
                 Evaluation of the function second derivative providing a scalar.

                 :param scalar: the input value for which the function second
                     derivative should be evaluated.
                 :return: a :py:class:`numpy.ndarray` of M values, the result
                     of the function second derivative evaluation according
                     to providing scalar.
                 )rst")
        ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineAdditionalInit_ ;

template <int N, typename DataType>
struct DefineAdditionalInit_<N, 2, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        scope.def(py::init<const DataType &, const DataType &>(),
                  R"rst(
                  Initialisation of the uniform function from 2 scalars.
                  These scalars correspond to the end dimension M=2 uniform
                  values.

                  :param scalar1: the first uniform value of the function.
                  :param scalar2: the second uniform value of the function.
                  )rst") ;
    }
} ;

template <int N, typename DataType>
struct DefineAdditionalInit_<N, 3, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        scope.def(py::init<const DataType &, const DataType &, const DataType &>(),
                  R"rst(
                  Initialisation of the uniform function from 3 scalars.
                  These scalars correspond to the end dimension M=3 uniform
                  values.

                  :param scalar1: the first uniform value of the function.
                  :param scalar2: the second uniform value of the function.
                  :param scalar3: the third uniform value of the function.
                  )rst") ;
    }
} ;

template <int N, typename DataType>
struct DefineAdditionalInit_<N, 4, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        scope.def(py::init<const DataType &, const DataType &, const DataType &, const DataType &>(),
                  R"rst(
                  Initialisation of the uniform function from 4 scalars.
                  These scalars correspond to the end dimension M=4 uniform
                  values.

                  :param scalar1: the first uniform value of the function.
                  :param scalar2: the second uniform value of the function.
                  :param scalar3: the third uniform value of the function.
                  :param scalar4: the fourth uniform value of the function.
                  )rst") ;
    }
} ;

template <int N, typename DataType>
struct DefineAdditionalInit_<N, 5, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        scope.def(py::init<const DataType &, const DataType &, const DataType &, const DataType &, const DataType &>(),
                  R"rst(
                  Initialisation of the uniform function from 5 scalars.
                  These scalars correspond to the end dimension M=5 uniform
                  values.

                  :param scalar1: the first uniform value of the function.
                  :param scalar2: the second uniform value of the function.
                  :param scalar3: the third uniform value of the function.
                  :param scalar4: the fourth uniform value of the function.
                  :param scalar5: the fifth uniform value of the function.
                  )rst") ;
    }
} ;


template <int N, typename DataType>
struct DefineAdditionalInit_<N, 6, DataType>
{
    template <typename Scope>
    static void define(Scope & scope)
    {
        scope.def(py::init<const DataType &, const DataType &, const DataType &, const DataType &, const DataType &, const DataType &>(),
                  R"rst(
                  Initialisation of the uniform function from 6 scalars.
                  These scalars correspond to the end dimension M=6 uniform
                  values.

                  :param scalar1: the first uniform value of the function.
                  :param scalar2: the second uniform value of the function.
                  :param scalar3: the third uniform value of the function.
                  :param scalar4: the fourth uniform value of the function.
                  :param scalar5: the fifth uniform value of the function.
                  :param scalar6: the sixth uniform value of the function.
                  )rst") ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionUserDefined_
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<N, M, DataType>,
                   std::shared_ptr<UserDefined<N, M, DataType> >,
                   ABC<N, M, DataType> > userDef(m, pythonName,
                                                 R"rst(
                                                 User defined function mapping:

                                                 .. math::

                                                     f: \mathbb{R}^N \rightarrow \mathbb{R}^M

                                                 This means that the function has :math:`M` uniform values,
                                                 whatever the values of the :math:`N` input values.

                                                 )rst") ;
        userDef.def(py::init<std::function<
                       Eigen::Matrix<DataType, M, 1>(const Eigen::Matrix<DataType, N, 1> &)
                                          > >(),
                    R"rst(
                    Initialisation of the :math:`M` uniform values with a vector of
                    :math:`M` components.

                    The :math:`M` uniform values correspond to the result of the
                    function, whatever the :math:`N` input values.

                    :param values: a :py:class:`numpy.ndarray` of M components,
                        defining the uniform values of the function.
                    )rst") ;
        DefineFunctionEval_<N, M, DataType>::define(userDef) ;
    }
} ;

template <typename DataType>
struct DefineFunctionUserDefined_<1, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<1, 1, DataType>,
                   std::shared_ptr<UserDefined<1, 1, DataType> >,
                   ABC<1, 1, DataType> > userDef(m, pythonName,
                                                 R"rst(
                                                 User defined function mapping:

                                                 .. math::

                                                     f: \mathbb{R} \rightarrow \mathbb{R}

                                                 )rst") ;
        userDef.def(py::init<std::function<DataType(const DataType &)> >(),
                    R"rst(
                    Initialisation of the function uniform value with a scalar.

                    The scalar provided defines the uniform value of the function.

                    :param scalar: the uniform value of the function.
                    )rst") ;
        DefineFunctionEval_<1, 1, DataType>::define(userDef) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUserDefined_<1, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<1, M, DataType>,
                   std::shared_ptr<UserDefined<1, M, DataType> >,
                   ABC<1, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<Eigen::Matrix<DataType, M, 1>(const DataType &)> >()) ;
        DefineFunctionEval_<1, M, DataType>::define(userDef) ;
    }
} ;

template <int N, typename DataType>
struct DefineFunctionUserDefined_<N, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<N, 1, DataType>,
                   std::shared_ptr<UserDefined<N, 1, DataType> >,
                   ABC<N, 1, DataType> > userDef(m, pythonName,
                                                 R"rst(
                                                 Uniform function mapping:

                                                 .. math::

                                                     f: \mathbb{R}^N \rightarrow \mathbb{R}

                                                 )rst") ;
        userDef.def(py::init<std::function<DataType(const Eigen::Matrix<DataType, N, 1> &)> >(),
                    R"rst(
                    Initialisation of the function uniform value with a scalar.

                    The scalar provided defines the uniform value of the function.

                    :param scalar: the uniform value of the function.
                    )rst") ;
        DefineFunctionEval_<N, 1, DataType>::define(userDef) ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_ ;

template <int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_<1, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<1, M, DataType>,
                   std::shared_ptr<UserDefined<1, M, DataType> >,
                   ABC<1, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<
               Eigen::Matrix<DataType, M, 1>(const DataType &)
                                          > >()) ;
        DefineFunctionEval_<1, M, DataType>::define(userDef) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_<2, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<2, M, DataType, false>,
                   std::shared_ptr<UserDefined<2, M, DataType, false> >,
                   ABC<2, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<
               Eigen::Matrix<DataType, M, 1>(const DataType &, const DataType &)
                                          > >()) ;
        DefineFunctionEval_<2, M, DataType>::define(userDef) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_<3, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<3, M, DataType, false>,
                   std::shared_ptr<UserDefined<3, M, DataType, false> >,
                   ABC<3, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<
               Eigen::Matrix<DataType, M, 1>(const DataType &, const DataType &,
                                             const DataType &)
                                          > >()) ;
        DefineFunctionEval_<3, M, DataType>::define(userDef) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_<4, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<4, M, DataType, false>,
                   std::shared_ptr<UserDefined<4, M, DataType, false> >,
                   ABC<4, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<
               Eigen::Matrix<DataType, M, 1>(const DataType &, const DataType &,
                                             const DataType &, const DataType &)
                                          > >()) ;
        DefineFunctionEval_<4, M, DataType>::define(userDef) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_<5, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<5, M, DataType, false>,
                   std::shared_ptr<UserDefined<5, M, DataType, false> >,
                   ABC<5, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<
               Eigen::Matrix<DataType, M, 1>(const DataType &, const DataType &,
                                             const DataType &, const DataType &,
                                             const DataType &)
                                          > >()) ;
        DefineFunctionEval_<5, M, DataType>::define(userDef) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUserDefinedNArgs_<6, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<5, M, DataType, false>,
                   std::shared_ptr<UserDefined<6, M, DataType, false> >,
                   ABC<6, M, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<
               Eigen::Matrix<DataType, M, 1>(const DataType &, const DataType &,
                                             const DataType &, const DataType &,
                                             const DataType &, const DataType &)
                                          > >()) ;
        DefineFunctionEval_<6, M, DataType>::define(userDef) ;
    }
} ;


template <typename DataType>
struct DefineFunctionUserDefinedNArgs_<2, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<2, 1, DataType, false>,
                   std::shared_ptr<UserDefined<2, 1, DataType, false> >,
                   ABC<2, 1, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<DataType(const DataType &, const DataType &)> >()) ;
        DefineFunctionEval_<2, 1, DataType>::define(userDef) ;
    }
} ;

template <typename DataType>
struct DefineFunctionUserDefinedNArgs_<3, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<3, 1, DataType, false>,
                   std::shared_ptr<UserDefined<3, 1, DataType, false> >,
                   ABC<3, 1, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<DataType(const DataType &,
                                                    const DataType &,
                                                    const DataType &)> >()) ;
        DefineFunctionEval_<3, 1, DataType>::define(userDef) ;
    }
} ;

template <typename DataType>
struct DefineFunctionUserDefinedNArgs_<4, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<4, 1, DataType, false>,
                   std::shared_ptr<UserDefined<4, 1, DataType, false> >,
                   ABC<4, 1, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<DataType(const DataType &,
                                                    const DataType &,
                                                    const DataType &,
                                                    const DataType &)> >()) ;
        DefineFunctionEval_<4, 1, DataType>::define(userDef) ;
    }
} ;

template <typename DataType>
struct DefineFunctionUserDefinedNArgs_<5, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<5, 1, DataType, false>,
                   std::shared_ptr<UserDefined<5, 1, DataType, false> >,
                   ABC<5, 1, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<DataType(const DataType &,
                                                    const DataType &,
                                                    const DataType &,
                                                    const DataType &,
                                                    const DataType &)> >()) ;
        DefineFunctionEval_<5, 1, DataType>::define(userDef) ;
    }
} ;

template <typename DataType>
struct DefineFunctionUserDefinedNArgs_<6, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<UserDefined<6, 1, DataType, false>,
                   std::shared_ptr<UserDefined<6, 1, DataType, false> >,
                   ABC<6, 1, DataType> > userDef(m, pythonName) ;
        userDef.def(py::init<std::function<DataType(const DataType &,
                                                    const DataType &,
                                                    const DataType &,
                                                    const DataType &,
                                                    const DataType &,
                                                    const DataType &)> >()) ;
        DefineFunctionEval_<6, 1, DataType>::define(userDef) ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionUniform_
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Uniform<N, M, DataType>,
                   std::shared_ptr<Uniform<N, M, DataType> >,
                   ABC<N, M, DataType> > unif(m, pythonName,
                                              R"rst(
                                              Uniform function mapping:

                                              .. math::

                                                  f: \mathbb{R}^N \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` uniform values,
                                              whatever the values of the :math:`N` input values.

                                              )rst") ;
        unif.def(py::init<Eigen::Matrix<DataType, M, 1> >(),
                 R"rst(
                 Initialisation of the :math:`M` uniform values with a vector of
                 :math:`M` components.

                 The :math:`M` uniform values correspond to the result of the
                 function, whatever the :math:`N` input values.

                 :param values: a :py:class:`numpy.ndarray` of M components,
                     defining the uniform values of the function.
                 )rst") ;
        DefineAdditionalInit_<N, M, DataType>::define(unif) ;
        DefineFunctionEval_<N, M, DataType>::define(unif) ;
    }
} ;

template <std::size_t N, std::size_t M, typename DataType, std::size_t IAxis>
struct DefineScaledAxisFunction_
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<ScaledAxis<N, M, DataType, IAxis>,
                   std::shared_ptr<ScaledAxis<N, M, DataType, IAxis> >,
                   ABC<N, M, DataType> > scaled(m, pythonName) ;
        scaled.def(py::init<std::shared_ptr<ABC<N, M, DataType> > >()) ;
        scaled.def("setScaleFactor", &ScaledAxis<N, M, DataType, IAxis>::setScaleFactor) ;
        scaled.def("getScaleFactor", &ScaledAxis<N, M, DataType, IAxis>::getScaleFactor) ;
        DefineFunctionEval_<N, M, DataType>::define(scaled) ;
    }
} ;

template <typename DataType>
struct DefineFunctionUniform_<1, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Uniform<1, 1, DataType>,
                   std::shared_ptr<Uniform<1, 1, DataType> >,
                   ABC<1, 1, DataType> > unif(m, pythonName,
                                              R"rst(
                                              Uniform function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}

                                              This means that the function has 1 uniform value,
                                              whatever the input value.
                                              )rst") ;
        unif.def(py::init<DataType>(),
                 R"rst(
                 Initialisation of the function uniform value with a scalar.

                 The scalar provided defines the uniform value of the function.

                 :param scalar: the uniform value of the function.
                 )rst") ;
        DefineFunctionEval_<1, 1, DataType>::define(unif) ;
    }
} ;

template <int N, typename DataType>
struct DefineFunctionUniform_<N, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Uniform<N, 1, DataType>,
                   std::shared_ptr<Uniform<N, 1, DataType> >,
                   ABC<N, 1, DataType> > unif(m, pythonName,
                                              R"rst(
                                              Uniform function mapping:

                                              .. math::

                                                  f: \mathbb{R}^N \rightarrow \mathbb{R}

                                              This means that the function has 1 uniform value,
                                              whatever the :math:`N` input values.
                                              )rst") ;
        unif.def(py::init<DataType>(),
                 R"rst(
                 Initialisation of the function uniform value with a scalar.

                 The scalar provided defines the uniform value of the function.

                 :param scalar: the uniform value of the function.
                 )rst") ;
        DefineFunctionEval_<N, 1, DataType>::define(unif) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionUniform_<1, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Uniform<1, M, DataType>,
                   std::shared_ptr<Uniform<1, M, DataType> >,
                   ABC<1, M, DataType> > unif(m, pythonName,
                                              R"rst(
                                              Uniform function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` uniform values,
                                              whatever the input value.
                                              )rst") ;
        unif.def(py::init<Eigen::Matrix<DataType, M, 1> >(),
                 R"rst(
                 Initialisation of the :math:`M` uniform values with a vector of
                 :math:`M` components.

                 The :math:`M` uniform values correspond to the result of the
                 function, whatever the input value.

                 :param values: a :py:class:`numpy.ndarray` of M components,
                     defining the uniform values of the function.
                 )rst") ;
        DefineAdditionalInit_<1, M, DataType>::define(unif) ;
        DefineFunctionEval_<1, M, DataType>::define(unif) ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionZero_
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Zero<N, M, DataType>,
                   std::shared_ptr<Zero<N, M, DataType> >,
                   ABC<N, M, DataType> > zero(m, pythonName,
                                              R"rst(
                                              Zero function mapping:

                                              .. math::

                                                  f: \mathbb{R}^N \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` 0. values,
                                              whatever the :math:`N` input values.
                                              )rst") ;
        zero.def(py::init<>(),
                 R"rst(
                 Initialisation of the :math:`M` zeros that will be the result
                 of the function whatever the values of the :math:`N` input.
                 )rst") ;
        DefineFunctionEval_<N, M, DataType>::define(zero) ;
    }

} ;

template <int NF, typename DataType>
using F = std::shared_ptr<ABC<NF, 1, DataType> > ;

template <int N, int M, typename DataType, typename Scope>
void DefineAnalyticalFunctionAddFunctor(Scope & scope)
{
    using ClassType = typename Scope::type ;
    scope.def("addFunctor", (void (ClassType::*)(const std::string &, const F<1, DataType> &)) &ClassType::addFunctor,
              R"rst(
              Provide the possibility to refer to another function
              that is used by one or more of the M expressions.

              Also note that the same variable names should be used.

              Additional note, a functor can itself include functors.

              :param name: the name of the functor used in the expressions.
              :param func: the functor itself. Note that this functor should
                  generally be of type RXToR1Analytical with X <= N.
             )rst")
        .def("addFunctor", (void (ClassType::*)(const std::string &, const F<2, DataType> &)) &ClassType::addFunctor,
             R"rst(Same as :py:meth:`addFunctor`)rst")
        .def("addFunctor", (void (ClassType::*)(const std::string &, const F<3, DataType> &)) &ClassType::addFunctor,
             R"rst(Same as :py:meth:`addFunctor`)rst")
        .def("addFunctor", (void (ClassType::*)(const std::string &, const F<4, DataType> &)) &ClassType::addFunctor,
             R"rst(Same as :py:meth:`addFunctor`)rst")
        .def("addFunctor", (void (ClassType::*)(const std::string &, const F<5, DataType> &)) &ClassType::addFunctor,
             R"rst(Same as :py:meth:`addFunctor`)rst")
    ;
}

template <int N, int M, typename DataType>
struct DefineFunctionAnalytical_
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Analytical<N, M, DataType>,
                   std::shared_ptr<Analytical<N, M, DataType> >,
                   ABC<N, M, DataType> > anal(m, pythonName,
                                              R"rst(
                                              Analytical function mapping:

                                              .. math::

                                                  f: \mathbb{R}^N \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` user defined formulas,
                                              depending on the :math:`N` input values.)rst") ;
        anal.def(py::init<const std::array<std::string, M> &,
                          const std::array<std::string, N> &>(),
                 R"rst(
                 Initialisation of the function given the list of
                 formulas and a list of variables.

                 :param formulas: a list of :math:`M` strings
                     providing formulas using a set of :math:`N`
                     variables
                 :param variables: a list of :math:`N` strings
                     providing the variables names used in the
                     formulas.
                 )rst") ;
        DefineFunctionEval_<N, M, DataType>::define(anal) ;
        DefineAnalyticalFunctionAddFunctor<N, M, DataType>(anal) ;
        // FIXME for below to compile, I had to add a default dummy constructor
        anal.def(py::init<const std::array<std::string, M> &,
                          const std::array<std::string, N> &,
                          const std::array<Analytical<N, N, DataType>, M>&>(),
                 R"rst(
                 Initialisation providing the list of formulas, variables and
                 analytical first derivatives function objects.

                 This allows the first derivatives to be calculated *exactly*
                 instead of using the default finite difference scheme.

                 :param formulas: a list of :math:`M` strings
                     providing formulas using a set of :math:`N`
                     variables
                 :param variables: a list of :math:`N` strings
                     providing the variables names used in the
                     formulas.
                 :param firstDer: a list of :math:`M` RNToRNAnalytical functions
                     providing the analytical formulas for the function first
                     derivative.

                     Note that if these analytical derivative functions also
                     contain their analytical derivatives this provides the
                     second derivatives to the main function object.
                 )rst") ;

    }
} ;

template <typename DataType>
struct DefineFunctionAnalytical_<1, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Analytical<1, 1, DataType>,
                   std::shared_ptr<Analytical<1, 1, DataType> >,
                   ABC<1, 1, DataType> > anal(m, pythonName,
                                              R"rst(
                                              Analytical function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}

                                              This means that the function has 1 user defined formulas,
                                              depending on 1 input value.)rst") ;
        anal.def(py::init<const std::string &, const std::string &>(),
                 R"rst(
                 Initialisation of the function with a formula and a variable.

                 :param str formula: the string user defined formula using
                     provided variable name.
                 :param str variable: the string variable name used in provided
                     formula. 
                 )rst")
            .def(py::init<const std::string &,
                          const std::string &,
                          Analytical<1, 1, DataType> >(),
                 R"rst(
                 Initialisation providing the formula, variable and
                 analytical first derivative function object.
                 This allows the first derivative to be calculated *exactly*
                 instead of using the default finite difference scheme.

                 :param str formula: the string user defined formula using
                     provided variable name.
                 :param str variable: the string variable name used in provided
                     formula. 
                 :param firstDer: a R1ToR1Analytical function
                     providing the analytical formula for the function first
                     derivative.

                     Note that if this analytical derivative function also
                     contain its analytical derivative this provides the
                     second derivative to the main function object.
                 )rst") ;
        DefineFunctionEval_<1, 1, DataType>::define(anal) ;
        DefineAnalyticalFunctionAddFunctor<1, 1, DataType>(anal) ;
    }
} ;

template <int N, typename DataType>
struct DefineFunctionAnalytical_<N, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Analytical<N, 1, DataType>,
                   std::shared_ptr<Analytical<N, 1, DataType> >,
                   ABC<N, 1, DataType> > anal(m, pythonName,
                                              R"rst(
                                              Analytical function mapping:

                                              .. math::

                                                  f: \mathbb{R}^N \rightarrow \mathbb{R}

                                              This means that the function has 1 user defined formula,
                                              depending on the :math:`N` input values.)rst") ;
        anal.def(py::init<const std::string &, const std::array<std::string, N> &>(),
                 R"rst(
                 Initialisation of the function given the formula and a list
                 of variables.

                 :param formula: a string providing the user defined formula
                     using a set of :math:`N` variables
                 :param variables: a list of :math:`N` strings
                     providing the variables names used in the
                     formulas.
                 )rst") ;
        anal.def(py::init<const std::string &,
                      const std::array<std::string, N> &,
                      Analytical<N, N, DataType> >(),
                 R"rst(
                 Initialisation providing the formula, variables and
                 analytical first derivatives function objects.

                 This allows the first derivatives to be calculated *exactly*
                 instead of using the default finite difference scheme.

                 :param formula: a string providing the user defined formula
                     using a set of :math:`N` variables
                 :param variables: a list of :math:`N` strings
                     providing the variables names used in the
                     formulas.
                 :param firstDer: a RNToRNAnalytical function
                     providing the analytical formulas for the function first
                     derivative.

                     Note that if these analytical derivative functions also
                     contain their analytical derivatives this provides the
                     second derivatives to the main function object.
                 )rst") ;
        DefineFunctionEval_<N, 1, DataType>::define(anal) ;
        DefineAnalyticalFunctionAddFunctor<N, 1, DataType>(anal) ;

    }
} ;

template <int M, typename DataType>
struct DefineFunctionAnalytical_<1, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Analytical<1, M, DataType>,
                   std::shared_ptr<Analytical<1, M, DataType> >,
                   ABC<1, M, DataType> > anal(m, pythonName,
                                              R"rst(
                                              Analytical function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` user defined formulas,
                                              depending on 1 input value.)rst") ;
        anal.def(py::init<const std::array<std::string, M> &, const std::string &>(),
                 R"rst(
                 Initialisation of the function given the list of
                 formulas and a variable.

                 :param formulas: a list of :math:`M` strings
                     providing formulas using a set of :math:`N`
                     variables
                 :param variable: a string
                     providing the variable name used in the
                     formulas.
                 )rst") ;
        DefineFunctionEval_<1, M, DataType>::define(anal) ;
        DefineAnalyticalFunctionAddFunctor<1, M, DataType>(anal) ;
        // FIXME for below to compile, I had to add a default dummy constructor
        anal.def(py::init<const std::array<std::string, M> &,
                          const std::string &,
                          std::array<Analytical<1, 1, DataType>, M> >(),
                 R"rst(
                 Initialisation providing the list of formulas, variable and
                 analytical first derivatives function objects.

                 This allows the first derivatives to be calculated *exactly*
                 instead of using the default finite difference scheme.

                 :param formulas: a list of :math:`M` strings
                     providing formulas using a set of :math:`N`
                     variables
                 :param variable: a string
                     providing the variable name used in the
                     formulas.
                 :param firstDers: a list of :math:`M` R1ToR1Analytical functions
                     providing the analytical formulas for the function first
                     derivative.

                     Note that if these analytical derivative functions also
                     contain their analytical derivatives this provides the
                     second derivatives to the main function object.
                 )rst") ;

    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionPolynomial_ ;

template <typename DataType>
struct DefineFunctionPolynomial_<1, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Polynomial<1, DataType>,
                   std::shared_ptr<Polynomial<1, DataType> >,
                   ABC<1, 1, DataType> > poly(m, pythonName,
                                              R"rst(
                                              Polynomial function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}

                                              This means that the function has 1 polynom of X coefficients
                                              depending on 1 input value, :math:`X \geq 1`)rst") ;
        poly.def(py::init<const Eigen::Matrix<DataType, Eigen::Dynamic, 1> &>(),
                 R"rst(
                 Initialisation providing the list of polynom coefficients.

                 :param coefs: a :py:class:`numpy.ndarray` of arbitrary size
                     :math:`X \geq 1` containing the polynom coefficients.
                 )rst") ;
        DefineFunctionEval_<1, 1, DataType>::define(poly) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionPolynomial_<1, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Polynomial<M, DataType>,
                   std::shared_ptr<Polynomial<M, DataType> >,
                   ABC<1, M, DataType> > poly(m, pythonName,
                                              R"rst(
                                              Polynomial function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` polynoms of variable coefficients
                                              depending on 1 input value.)rst") ;
        poly.def(py::init<std::array<Eigen::Matrix<DataType, Eigen::Dynamic, 1>, M> >(),
                 R"rst(
                 Initialisation providing a list of arrays containing polynoms
                 coefficients.

                 :param coefs: a list of :math:`M` :py:class:`numpy.ndarray`.
                     Each of the :py:class:`numpy.ndarray` has an arbitrary
                     size, depending on each of the polynoms order.
                 )rst") ;
        DefineFunctionEval_<1, M, DataType>::define(poly) ;
    }
} ;

// FIXME N should be 2 according to the signatures...
template <int N, int M, typename DataType>
struct DefineFunctionDiscrete_
{
    static void define(py::module & m, const char * pythonName)
    {
        using T = Discrete<N, M, BV::Math::Interpolators::InterpScheme::LINEAR, DataType,
                           Eigen::Matrix<DataType, Eigen::Dynamic, 1>,
                           Eigen::Matrix<DataType, Eigen::Dynamic, Eigen::Dynamic> > ;
        py::class_<T, std::shared_ptr<T>,
                   ABC<N, M, DataType> > disc(m, pythonName,
                                              R"rst(
                                              Discrete function mapping:

                                              .. math::

                                                  f: \mathbb{R}^2 \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` 2-D grids,
                                              into which the 2 input values are interpolated.)rst") ;
        disc.def(py::init<std::array<Eigen::Matrix<DataType, Eigen::Dynamic, 1>, M>,
                          std::array<Eigen::Matrix<DataType, Eigen::Dynamic, 1>, M>,
                          std::array<Eigen::Matrix<DataType, Eigen::Dynamic, Eigen::Dynamic>, M> >(),
                 R"rst(
                 Initialisation providing the list of :math:`M` axes and data.
                 
                 :param axis1: the list of :math:`M` 1D :py:class:`numpy.ndarray`
                     representing the first dimension axis.
                 :param axis2: the list of :math:`M` 1D :py:class:`numpy.ndarray`
                     representing the second dimension axis.
                 :param data: the list of :math:`M` 2D :py:class:`numpy.ndarray`
                     containing the data into which to interpolate.
                 )rst") ;
        DefineFunctionEval_<N, M, DataType>::define(disc) ;
    }
} ;

template <typename DataType>
struct DefineFunctionDiscrete_<1, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        using T = Discrete<1, 1, BV::Math::Interpolators::InterpScheme::LINEAR, DataType> ;
        py::class_<T, std::shared_ptr<T>,
                   ABC<1, 1, DataType> > disc(m, pythonName,
                                              R"rst(
                                              Discrete function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}

                                              This means that the function has 1 1-D grid,
                                              into which the input value is interpolated.)rst") ;
        disc.def(py::init<const Eigen::Matrix<DataType, Eigen::Dynamic, 1> &,
                          const Eigen::Matrix<DataType, Eigen::Dynamic, 1> &>(),
                 R"rst(
                 Initialisation providing the interpolation axis and data.

                 :param axis: a 1D :py:class:`numpy.ndarray` containing the axis
                     corresponding to the interpolation data.
                 :param data: a 1D :py:class:`numpy.ndarray` containing the data
                     into which to interpolate.
                 )rst") ;
        DefineFunctionEval_<1, 1, DataType>::define(disc) ;
    }
} ;

template <int M, typename DataType>
struct DefineFunctionDiscrete_<1, M, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        using T = Discrete<1, M, BV::Math::Interpolators::InterpScheme::LINEAR, DataType> ;
        py::class_<T, std::shared_ptr<T>,
                   ABC<1, M, DataType> > disc(m, pythonName,
                                              R"rst(
                                              Discrete function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}^M

                                              This means that the function has :math:`M` 1-D grids,
                                              into which the input value is interpolated.)rst") ;
        disc.def(py::init<std::array<Eigen::Matrix<DataType, Eigen::Dynamic, 1>, M>,
                          std::array<Eigen::Matrix<DataType, Eigen::Dynamic, 1>, M> >(),
                 R"rst(
                 Initialisation providing the list of :math:`M` axes and data.
                 
                 :param axis: the list of :math:`M` 1D :py:class:`numpy.ndarray`
                     representing the interpolation axis.
                 :param data: the list of :math:`M` 1D :py:class:`numpy.ndarray`
                     containing the data into which to interpolate.
                 )rst") ;
        DefineFunctionEval_<1, M, DataType>::define(disc) ;
    }
} ;

// FIXME N should be 2
template <int N, typename DataType>
struct DefineFunctionDiscrete_<N, 1, DataType>
{
    static void define(py::module & m, const char * pythonName)
    {
        using T = Discrete<N, 1, BV::Math::Interpolators::InterpScheme::LINEAR, DataType,
                           Eigen::Matrix<DataType, Eigen::Dynamic, 1>,
                           Eigen::Matrix<DataType, Eigen::Dynamic, Eigen::Dynamic> > ;
        py::class_<T, std::shared_ptr<T>,
                   ABC<N, 1, DataType> > disc(m, pythonName,
                                              R"rst(
                                              Discrete function mapping:

                                              .. math::

                                                  f: \mathbb{R}^2 \rightarrow \mathbb{R}

                                              This means that the function has 1 2-D grid,
                                              into which the 2 input values are interpolated.)rst") ;
        disc.def(py::init<const Eigen::Matrix<DataType, Eigen::Dynamic, 1> &,
                          const Eigen::Matrix<DataType, Eigen::Dynamic, 1> &,
                          const Eigen::Matrix<DataType, Eigen::Dynamic, Eigen::Dynamic> &>(),
                 R"rst(
                 Initialisation providing the interpolation axes and data.
                 
                 :param axis1: a 1D :py:class:`numpy.ndarray` representing the
                     first dimension axis.
                 :param axis2: a 1D :py:class:`numpy.ndarray` representing the
                     second dimension axis.
                 :param data: a 2D :py:class:`numpy.ndarray`
                     containing the data into which to interpolate.
                 )rst") ;
        DefineFunctionEval_<N, 1, DataType>::define(disc) ;
    }
} ;

template <int N, int M, typename DataType>
struct DefineFunctionStep_
{
    static void define(py::module & m, const char * pythonName)
    {
        py::class_<Step<1, 1, DataType>,
                   std::shared_ptr<Step<1, 1, DataType> >,
                   ABC<1, 1, DataType> > step(m, pythonName,
                                              R"rst(
                                              Step function mapping:

                                              .. math::

                                                  f: \mathbb{R} \rightarrow \mathbb{R}

                                              )rst") ;
        step.def(py::init<DataType, DataType>(),
                 R"rst(
                 Initialisation of the function step with two scalars.

                 The first one is the step value and the second defines the value of the function.

                 :param scalar: the step value
                 :param scalar: the value of the function.
                 )rst") ;
        DefineFunctionEval_<1, 1, DataType>::define(step) ;
    }
} ;


template <typename DataType>
struct DefineFunctionCurrent_
{
    static void define(py::module & m, const char * pythonName)
    {
        using T = BV::Math::Functions::Current<DataType> ;
        py::class_<T, std::shared_ptr<T>,
                   ABC<4, 3, DataType> > curr(m, pythonName,
                                              R"rst(
                                              Current function mapping:

                                              .. math::


                                              )rst") ;
        curr.def(py::init<double, double, double, double, double, double, double, double>(),
                 R"rst(
                 Initialisation providing the list of :math:`M` axes and data.
                 
                 :param double : surfaceTideCurrent
                 :param double : seabedTideCurrent
                 :param double : tideCurrentHeading
                 :param double : waterdepth
                 :param double : powerLawExponent
                 :param double : surfaceWindInducedCurrent
                 :param double : windInducedCurrentHeading
                 :param double : windInfluenceWaterdepthLimit
                )rst") ;
        DefineFunctionEval_<4, 3, DataType>::define(curr) ;
    }
} ;

void exportModule(pybind11::module & m)
{
    // WARNING: compiling all possible types is impossible (infinity).
    // We will have to add a wrapper whenever we need it !
    // Note that for the analytical derivatives Analytical<N, N, DataType>
    // should be available.

    py::class_<ABC<1, 1, double>, std::shared_ptr<ABC<1, 1, double> > >(m, "_R1ToR1ABC") ;
    py::class_<ABC<1, 2, double>, std::shared_ptr<ABC<1, 2, double> > >(m, "_R1ToR2ABC") ;
    py::class_<ABC<1, 3, double>, std::shared_ptr<ABC<1, 3, double> > >(m, "_R1ToR3ABC") ;
    py::class_<ABC<1, 6, double>, std::shared_ptr<ABC<1, 6, double> > >(m, "_R1ToR6ABC") ;
    py::class_<ABC<2, 1, double>, std::shared_ptr<ABC<2, 1, double> > >(m, "_R2ToR1ABC") ;
    py::class_<ABC<2, 2, double>, std::shared_ptr<ABC<2, 2, double> > >(m, "_R2ToR2ABC") ;
    py::class_<ABC<2, 3, double>, std::shared_ptr<ABC<2, 3, double> > >(m, "_R2ToR3ABC") ;
    py::class_<ABC<2, 6, double>, std::shared_ptr<ABC<2, 6, double> > >(m, "_R2ToR6ABC") ;
    py::class_<ABC<3, 1, double>, std::shared_ptr<ABC<3, 1, double> > >(m, "_R3ToR1ABC") ;
    py::class_<ABC<3, 2, double>, std::shared_ptr<ABC<3, 2, double> > >(m, "_R3ToR2ABC") ;
    py::class_<ABC<3, 3, double>, std::shared_ptr<ABC<3, 3, double> > >(m, "_R3ToR3ABC") ;
    py::class_<ABC<4, 1, double>, std::shared_ptr<ABC<4, 1, double> > >(m, "_R4ToR1ABC") ;
    py::class_<ABC<4, 2, double>, std::shared_ptr<ABC<4, 2, double> > >(m, "_R4ToR2ABC") ;
    py::class_<ABC<4, 3, double>, std::shared_ptr<ABC<4, 3, double> > >(m, "_R4ToR3ABC") ;
    py::class_<ABC<4, 4, double>, std::shared_ptr<ABC<4, 4, double> > >(m, "_R4ToR4ABC") ;
    py::class_<ABC<5, 1, double>, std::shared_ptr<ABC<5, 1, double> > >(m, "_R5ToR1ABC") ;
    py::class_<ABC<5, 5, double>, std::shared_ptr<ABC<5, 5, double> > >(m, "_R5ToR5ABC") ;

    DefineFunctionUniform_<1, 1, double>::define(m, "R1ToR1Uniform") ;
    DefineFunctionUniform_<1, 2, double>::define(m, "R1ToR2Uniform") ;
    DefineFunctionUniform_<1, 3, double>::define(m, "R1ToR3Uniform") ;
    DefineFunctionUniform_<1, 6, double>::define(m, "R1ToR6Uniform") ;
    DefineFunctionUniform_<2, 1, double>::define(m, "R2ToR1Uniform") ;
    DefineFunctionUniform_<2, 2, double>::define(m, "R2ToR2Uniform") ;
    DefineFunctionUniform_<2, 6, double>::define(m, "R2ToR6Uniform") ;
    DefineFunctionUniform_<3, 1, double>::define(m, "R3ToR1Uniform") ;
    DefineFunctionUniform_<3, 2, double>::define(m, "R3ToR2Uniform") ;
    DefineFunctionUniform_<3, 3, double>::define(m, "R3ToR3Uniform") ;
    DefineFunctionUniform_<4, 1, double>::define(m, "R4ToR1Uniform") ;
    DefineFunctionUniform_<4, 2, double>::define(m, "R4ToR2Uniform") ;
    DefineFunctionUniform_<4, 3, double>::define(m, "R4ToR3Uniform") ;
    DefineFunctionUniform_<4, 4, double>::define(m, "R4ToR4Uniform") ;
    DefineFunctionUniform_<5, 1, double>::define(m, "R5ToR1Uniform") ;
    DefineFunctionUniform_<5, 5, double>::define(m, "R5ToR5Uniform") ;

    DefineFunctionZero_<1, 1, double>::define(m, "R1ToR1Zero") ;
    DefineFunctionZero_<1, 2, double>::define(m, "R1ToR2Zero") ;
    DefineFunctionZero_<1, 3, double>::define(m, "R1ToR3Zero") ;
    DefineFunctionZero_<1, 6, double>::define(m, "R1ToR6Zero") ;
    DefineFunctionZero_<2, 1, double>::define(m, "R2ToR1Zero") ;
    DefineFunctionZero_<2, 2, double>::define(m, "R2ToR2Zero") ;
    DefineFunctionZero_<2, 6, double>::define(m, "R2ToR6Zero") ;
    DefineFunctionZero_<3, 1, double>::define(m, "R3ToR1Zero") ;
    DefineFunctionZero_<3, 2, double>::define(m, "R3ToR2Zero") ;
    DefineFunctionZero_<3, 3, double>::define(m, "R3ToR3Zero") ;
    DefineFunctionZero_<4, 1, double>::define(m, "R4ToR1Zero") ;
    DefineFunctionZero_<4, 2, double>::define(m, "R4ToR2Zero") ;
    DefineFunctionZero_<4, 3, double>::define(m, "R4ToR3Zero") ;
    DefineFunctionZero_<4, 4, double>::define(m, "R4ToR4Zero") ;
    DefineFunctionZero_<5, 1, double>::define(m, "R5ToR1Zero") ;
    DefineFunctionZero_<5, 5, double>::define(m, "R5ToR5Zero") ;

    DefineFunctionAnalytical_<1, 1, double>::define(m, "R1ToR1Analytical") ;
    DefineFunctionAnalytical_<1, 2, double>::define(m, "R1ToR2Analytical") ;
    DefineFunctionAnalytical_<1, 3, double>::define(m, "R1ToR3Analytical") ;
    DefineFunctionAnalytical_<1, 6, double>::define(m, "R1ToR6Analytical") ;
    DefineFunctionAnalytical_<2, 1, double>::define(m, "R2ToR1Analytical") ;
    DefineFunctionAnalytical_<2, 2, double>::define(m, "R2ToR2Analytical") ;
    DefineFunctionAnalytical_<2, 3, double>::define(m, "R2ToR3Analytical") ;
    DefineFunctionAnalytical_<2, 6, double>::define(m, "R2ToR6Analytical") ;
    DefineFunctionAnalytical_<3, 1, double>::define(m, "R3ToR1Analytical") ;
    DefineFunctionAnalytical_<3, 2, double>::define(m, "R3ToR2Analytical") ;
    DefineFunctionAnalytical_<3, 3, double>::define(m, "R3ToR3Analytical") ;
    DefineFunctionAnalytical_<4, 1, double>::define(m, "R4ToR1Analytical") ;
    DefineFunctionAnalytical_<4, 2, double>::define(m, "R4ToR2Analytical") ;
    DefineFunctionAnalytical_<4, 3, double>::define(m, "R4ToR3Analytical") ;
    DefineFunctionAnalytical_<4, 4, double>::define(m, "R4ToR4Analytical") ;
    DefineFunctionAnalytical_<5, 1, double>::define(m, "R5ToR1Analytical") ;
    DefineFunctionAnalytical_<5, 5, double>::define(m, "R5ToR5Analytical") ;

    DefineFunctionPolynomial_<1, 1, double>::define(m, "R1ToR1Polynomial") ;
    DefineFunctionPolynomial_<1, 2, double>::define(m, "R1ToR2Polynomial") ;
    DefineFunctionPolynomial_<1, 3, double>::define(m, "R1ToR3Polynomial") ;
    DefineFunctionPolynomial_<1, 6, double>::define(m, "R1ToR6Polynomial") ;

    DefineFunctionDiscrete_<1, 1, double>::define(m, "R1ToR1Discrete") ;
    DefineFunctionDiscrete_<1, 2, double>::define(m, "R1ToR2Discrete") ;
    DefineFunctionDiscrete_<1, 3, double>::define(m, "R1ToR3Discrete") ;
    DefineFunctionDiscrete_<1, 6, double>::define(m, "R1ToR6Discrete") ;
    DefineFunctionDiscrete_<2, 1, double>::define(m, "R2ToR1Discrete") ;
    DefineFunctionDiscrete_<2, 2, double>::define(m, "R2ToR2Discrete") ;

    DefineFunctionUserDefined_<1, 1, double>::define(m, "R1ToR1UserDefined") ;
    DefineFunctionUserDefined_<1, 2, double>::define(m, "R1ToR2UserDefined") ;
    DefineFunctionUserDefined_<1, 3, double>::define(m, "R1ToR3UserDefined") ;
    DefineFunctionUserDefined_<1, 6, double>::define(m, "R1ToR6UserDefined") ;
    DefineFunctionUserDefined_<2, 1, double>::define(m, "R2ToR1UserDefined") ;
    DefineFunctionUserDefined_<2, 2, double>::define(m, "R2ToR2UserDefined") ;
    DefineFunctionUserDefined_<2, 6, double>::define(m, "R2ToR6UserDefined") ;
    DefineFunctionUserDefined_<3, 1, double>::define(m, "R3ToR1UserDefined") ;
    DefineFunctionUserDefined_<3, 2, double>::define(m, "R3ToR2UserDefined") ;
    DefineFunctionUserDefined_<3, 3, double>::define(m, "R3ToR3UserDefined") ;
    DefineFunctionUserDefined_<4, 1, double>::define(m, "R4ToR1UserDefined") ;
    DefineFunctionUserDefined_<4, 2, double>::define(m, "R4ToR2UserDefined") ;
    DefineFunctionUserDefined_<4, 3, double>::define(m, "R4ToR3UserDefined") ;
    DefineFunctionUserDefined_<4, 4, double>::define(m, "R4ToR4UserDefined") ;
    DefineFunctionUserDefined_<5, 1, double>::define(m, "R5ToR1UserDefined") ;
    DefineFunctionUserDefined_<5, 5, double>::define(m, "R5ToR5UserDefined") ;

    DefineFunctionUserDefinedNArgs_<2, 1, double>::define(m, "R2ToR1UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<2, 2, double>::define(m, "R2ToR2UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<2, 6, double>::define(m, "R2ToR6UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<3, 1, double>::define(m, "R3ToR1UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<3, 2, double>::define(m, "R3ToR2UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<3, 3, double>::define(m, "R3ToR3UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<4, 1, double>::define(m, "R4ToR1UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<4, 2, double>::define(m, "R4ToR2UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<4, 3, double>::define(m, "R4ToR3UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<4, 4, double>::define(m, "R4ToR4UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<5, 1, double>::define(m, "R5ToR1UserDefinedNArgs") ;
    DefineFunctionUserDefinedNArgs_<5, 5, double>::define(m, "R5ToR5UserDefinedNArgs") ;

    DefineFunctionStep_<1, 1, double>::define(m, "R1ToR1Step") ;

    DefineScaledAxisFunction_<1, 1, double, 0>::define(m, "R1ToR1ScaledAxis") ;
    DefineScaledAxisFunction_<1, 2, double, 0>::define(m, "R1ToR2ScaledAxis") ;
    DefineScaledAxisFunction_<1, 3, double, 0>::define(m, "R1ToR3ScaledAxis") ;
    DefineScaledAxisFunction_<2, 1, double, 0>::define(m, "R2ToR1ScaledAxis") ;
    DefineScaledAxisFunction_<3, 1, double, 0>::define(m, "R3ToR1ScaledAxis") ;

    DefineFunctionCurrent_<double>::define(m, "R4ToR3Current") ;

}

} // End of namespace Functions
} // End of namespace Math
} // End of namespace PythonInterface
} // End of namespace BV
