#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>

#include "Interface/pyWrapping/Functions.hpp"
#include "Interface/pyWrapping/ODEIntegration.hpp"
#include "Interface/pyWrapping/Solvers.hpp"
#include "Interface/pyWrapping/DspFilters.hpp"
#include "Interface/pyWrapping/Integration.hpp"
#include "Math/Interpolators/Interpolators.hpp"
#include "Math/movmax.hpp"

using namespace py::literals;

PYBIND11_MODULE(_Math, m)
{
	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    m.def("movmax", &BV::Math::movmax, "data"_a, "k"_a, "location"_a = 0);

    pybind11::module mFunctions = m.def_submodule("_Functions", "Math Functions module") ;
    BV::PythonInterface::Math::Functions::exportModule(mFunctions) ;

	pybind11::module mIntegration = m.def_submodule("Integration", "Math Integration module");
	BV::PythonInterface::Math::Integration::exportModule(mIntegration);

    pybind11::module mODE = m.def_submodule("ODEIntegration", "Math ODE integration module") ;
    BV::PythonInterface::Math::Integration::ODE::exportModule(mODE) ;

    pybind11::module mSolvers = m.def_submodule("Solvers", "Math Solvers module") ;
    BV::PythonInterface::Math::Solvers::exportModule(mSolvers) ;

    pybind11::module mFilters = m.def_submodule("DspFilters", "Math DspFilters module") ;
    BV::PythonInterface::Math::DspFilters::exportModule(mFilters) ;

    pybind11::module mInterp = m.def_submodule("Interpolators", "Math Interpolators module") ;
    pybind11::enum_<BV::Math::Interpolators::InterpScheme>(mInterp, "InterpolationScheme")
        .value("LINEAR", BV::Math::Interpolators::InterpScheme::LINEAR)
    ;
    pybind11::enum_<BV::Math::Interpolators::ExtrapolationType>(mInterp, "ExtrapolationType")
        .value("EXCEPTION", BV::Math::Interpolators::ExtrapolationType::EXCEPTION)
        .value("BOUNDARY", BV::Math::Interpolators::ExtrapolationType::BOUNDARY)
        .value("ZERO", BV::Math::Interpolators::ExtrapolationType::ZERO)
        .value("EXTRAPOLATE", BV::Math::Interpolators::ExtrapolationType::EXTRAPOLATE)
    ;
}
