Polynomial
==========

Polynomial functions allow to evaluate :math:`M` polynomial expressions, along
with their derivatives.

Here is an example for a function containing two polynoms:

.. math::

    p1 &= 2 + 3 x + 4 x^2 \\
    p2 &= 1 + 6 x

::

    >>> import numpy
    >>> from _Math.Functions import R1ToR2Polynomial

    >>> coefs1 = numpy.array((2., 3., 4.), dtype=float)
    >>> coefs2 = numpy.array((1., 6.), dtype=float)
    >>> func = R1ToR2Polynomial([coefs1, coefs2])
    >>> res = func(2.)
    >>> print(res)
    [ 24.  13.]
    >>> #res[0] = 2. + 3.*2. + 4*2**2
    ... #res[1] = 1. + 6.*2.
    ...

In addition note that the derivatives are calculated in an *exact* way.

The name pattern of the functions is the following::

    R1ToRMPolynomial

With :code:`M` the end (output) dimension.

Here is a list of the available polynomial functions:

- :py:class:`~_Math.Functions.R1ToR1Polynomial`
- :py:class:`~_Math.Functions.R1ToR2Polynomial`
- :py:class:`~_Math.Functions.R1ToR3Polynomial`


.. autoclass:: _Math.Functions.R1ToR1Polynomial
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR1Polynomial
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR1Polynomial
        :attributes:

    .. rubric:: Details

.. autoclass:: _Math.Functions.R1ToR2Polynomial
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR2Polynomial
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR2Polynomial
        :attributes:

    .. rubric:: Details

.. autoclass:: _Math.Functions.R1ToR3Polynomial
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR3Polynomial
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR3Polynomial
        :attributes:

    .. rubric:: Details

