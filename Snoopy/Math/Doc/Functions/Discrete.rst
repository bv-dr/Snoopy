Discrete
========

Discrete functions allow to perform :math:`M` :math:`N`-D interpolations.

Here is an example of 2D grid interpolating function::

    >>> import numpy
    >>> from _Math.Functions import R2ToR1Discrete

    >>> nx = 10
    >>> ny = 20
    >>> axis1 = numpy.arange(nx, dtype=float)
    >>> axis2 = numpy.arange(ny, dtype=float)
    >>> data = numpy.arange(nx*ny, dtype=float)
    >>> data.shape = nx, ny
    >>> func = R2ToR1Discrete(axis1, axis2, data)
    >>> res = func.eval(5., 6.)
    >>> print(res)
    106.0

The name pattern of the functions is the following::

    RNToRMDiscrete

With :code:`N` the start (input) dimension and :code:`M` the end (output) dimension.

Here is a list of the available discrete functions:
R1ToR1Discrete") ;

- :py:class:`~_Math.Functions.R1ToR1Discrete`
- :py:class:`~_Math.Functions.R1ToR2Discrete`
- :py:class:`~_Math.Functions.R1ToR3Discrete`
- :py:class:`~_Math.Functions.R2ToR1Discrete`
- :py:class:`~_Math.Functions.R2ToR2Discrete`

..
    python script to generate the doc for each of the functions:

    fNames = ("R1ToR1Discrete", "R1ToR2Discrete", "R1ToR3Discrete",
              "R2ToR1Discrete", "R2ToR2Discrete")
    
    for name in fNames:
        print("""
    .. autoclass:: _Math.Functions.{0}
        :members:
        :inherited-members:
        :special-members: __init__, __call__
    
        .. rubric:: Methods
    
        .. autoautosummary:: _Math.Functions.{0}
            :methods:
        
        .. rubric:: Attributes
    
        .. autoautosummary:: _Math.Functions.{0}
            :attributes:
    
        .. rubric:: Details
    """.format(name))

.. module:: Math

.. result of the python script

.. autoclass:: _Math.Functions.R1ToR1Discrete
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR1Discrete
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR1Discrete
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR2Discrete
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR2Discrete
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR2Discrete
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR3Discrete
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR3Discrete
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR3Discrete
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR1Discrete
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR1Discrete
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR1Discrete
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR2Discrete
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR2Discrete
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR2Discrete
        :attributes:

    .. rubric:: Details

