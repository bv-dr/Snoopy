Zero
====

Zero functions provide the possibility to write M zero valued functions that will
be evaluated at runtime for the N input values.::

    >>> import numpy
    >>> from _Math.Functions import R4ToR2Zero

    >>> func = R4ToR2Zero()
    >>> res = func(4., 5., 6., 7.)
    >>> print(res)
    [ 0.  0.]

    >>> inputValues = numpy.array((4., 5., 6., 7.), dtype=float)
    >>> res2 = func.eval(inputValues)
    >>> print(res2)
    [ 0.  0.]

    >>> firstDeriv = func.dEval(inputValues)
    >>> print(firstDeriv)
    array([[ 0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.]])


The name pattern of the functions is the following::

    RNToRMZero

With :code:`N` the start (input) dimension and :code:`M` the end (output) dimension.

Here is a list of the available zero functions:

- :py:class:`~_Math.Functions.R1ToR1Zero`
- :py:class:`~_Math.Functions.R1ToR2Zero`
- :py:class:`~_Math.Functions.R1ToR3Zero`
- :py:class:`~_Math.Functions.R2ToR1Zero`
- :py:class:`~_Math.Functions.R2ToR2Zero`
- :py:class:`~_Math.Functions.R3ToR1Zero`
- :py:class:`~_Math.Functions.R3ToR2Zero`
- :py:class:`~_Math.Functions.R3ToR3Zero`
- :py:class:`~_Math.Functions.R4ToR1Zero`
- :py:class:`~_Math.Functions.R4ToR2Zero`
- :py:class:`~_Math.Functions.R4ToR3Zero`
- :py:class:`~_Math.Functions.R4ToR4Zero`
- :py:class:`~_Math.Functions.R5ToR1Zero`
- :py:class:`~_Math.Functions.R5ToR5Zero`

..
    python script to generate the doc for each of the functions:

    fNames = ("R1ToR1Zero", "R1ToR2Zero", "R1ToR3Zero", "R2ToR1Zero",
                "R2ToR2Zero", "R3ToR1Zero", "R3ToR2Zero", "R3ToR3Zero",
                "R4ToR1Zero", "R4ToR2Zero", "R4ToR3Zero", "R4ToR4Zero",
                "R5ToR1Zero", "R5ToR5Zero")

    for name in fNames:
        print("""
    .. autoclass:: _Math.Functions.{0}
        :members:
        :inherited-members:
        :special-members: __init__, __call__

        .. rubric:: Methods

        .. autoautosummary:: _Math.Functions.{0}
            :methods:
        
        .. rubric:: Attributes

        .. autoautosummary:: _Math.Functions.{0}
            :attributes:

        .. rubric:: Details
    """.format(name))

.. module:: Math

.. result of the python script

.. autoclass:: _Math.Functions.R1ToR1Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR1Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR1Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR2Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR2Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR2Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR3Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR3Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR3Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR1Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR1Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR1Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR2Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR2Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR2Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR1Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR1Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR1Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR2Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR2Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR2Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR3Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR3Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR3Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR1Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR1Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR1Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR2Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR2Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR2Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR3Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR3Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR3Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR4Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR4Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR4Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R5ToR1Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R5ToR1Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R5ToR1Zero
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R5ToR5Zero
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R5ToR5Zero
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R5ToR5Zero
        :attributes:

    .. rubric:: Details

