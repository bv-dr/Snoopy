Analytical
==========

Analytical functions provide the possibility to write M user defined analytical
formulas that will be evaluated at runtime for the N input values.
Many features are available as:

- use standard functions (trigonometry, pow, sqrt, ...)
- use of user defined variables names
- embed other analytical functions.

Here is an example of the use of analytical functions::

    >>> import numpy
    >>> import math
    >>> from _Math.Functions import R2ToR3Analytical, R1ToR1Analytical
    >>> from _Math.Functions import R2ToR1Analytical, R2ToR2Analytical

    >>> variables = ["x", "y"]
    >>> func = R2ToR3Analytical(["2*x+f(y)", "4.*y+g(x, y)", "2.*sin(pi*y)"], variables)
    >>> functor1 = R1ToR1Analytical("4.*y+10.", "y")
    >>> functor2 = R2ToR1Analytical("y+sin(pi*x)", variables)
    >>> func.addFunctor("f", functor1)
    >>> func.addFunctor("g", functor2)

    >>> values = numpy.array((2., 3.), dtype=float)
    >>> res = func.eval(values) # We could also do: func.eval(2., 3.) or even func(2., 3.)
    >>> print(res)
    array([  2.60000000e+01,   1.50000000e+01,   7.34788079e-16])
    >>> # res[0] = 2.*2.+4.*3.+10.
    ... # res[1] = 4.*3.+3.+sin(pi*2.)
    ... # res[2] = 2.*sin(pi*3.)
    ...

In above example, the derivatives will be evaluated using a finite difference
scheme. However it is also possible to provide the analytical derivatives too::

    >>> import numpy
    >>> import math
    >>> from _Math.Functions import R2ToR3Analytical, R1ToR1Analytical
    >>> from _Math.Functions import R2ToR1Analytical, R2ToR2Analytical

    >>> variables = ["x", "y"]
    >>> formulae = ["2*x+f(y)", "4.*y+3.*x", "2.*sin(pi*y)"]
    >>> df1 = R2ToR2Analytical(["2.", "6."], variables)
    >>> df2 = R2ToR2Analytical(["3.", "4."], variables)
    >>> df3 = R2ToR2Analytical(["0.", "2.*pi*cos(pi*y)"], variables)
    >>> func = R2ToR3Analytical(formulae, variables, [df1, df2, df3])

    >>> functor1 = R1ToR1Analytical("6.*y+10.", "y")
    >>> func.addFunctor("f", functor1)

    >>> der = func.dEval(1., 2.)
    >>> print(der)
    [[ 2.          6.        ]
     [ 3.          4.        ]
     [ 0.          6.28318531]]

Note that if analytical derivatives are provided to above first derivatives,
this provides the analytical second derivatives of the function object.
Note that if no analytical first derivatives were provided, an exception
is thrown if asking for the second derivative as the numeric calculation of
partial second derivatives is not implemented yet.

The name pattern of the functions is the following::

    RNToRMAnalytical

With :code:`N` the start (input) dimension and :code:`M` the end (output) dimension.

Here is a list of the available analytical functions:

- :py:class:`~_Math.Functions.R1ToR1Analytical`
- :py:class:`~_Math.Functions.R1ToR2Analytical`
- :py:class:`~_Math.Functions.R1ToR3Analytical`
- :py:class:`~_Math.Functions.R2ToR1Analytical`
- :py:class:`~_Math.Functions.R2ToR2Analytical`
- :py:class:`~_Math.Functions.R2ToR3Analytical`
- :py:class:`~_Math.Functions.R3ToR1Analytical`
- :py:class:`~_Math.Functions.R3ToR2Analytical`
- :py:class:`~_Math.Functions.R3ToR3Analytical`
- :py:class:`~_Math.Functions.R4ToR1Analytical`
- :py:class:`~_Math.Functions.R4ToR2Analytical`
- :py:class:`~_Math.Functions.R4ToR3Analytical`
- :py:class:`~_Math.Functions.R4ToR4Analytical`
- :py:class:`~_Math.Functions.R5ToR1Analytical`
- :py:class:`~_Math.Functions.R5ToR5Analytical`

..
    python script to generate the doc for each of the functions:

    fNames = ("R1ToR1Analytical", "R1ToR2Analytical", "R1ToR3Analytical", "R2ToR1Analytical",
                "R2ToR2Analytical", "R2ToR3Analytical", "R3ToR1Analytical", "R3ToR2Analytical",
                "R3ToR3Analytical", "R4ToR1Analytical", "R4ToR2Analytical", "R4ToR3Analytical",
                "R4ToR4Analytical", "R5ToR1Analytical", "R5ToR5Analytical")

    for name in fNames:
        print("""
    .. autoclass:: _Math.Functions.{0}
        :members:
        :inherited-members:
        :special-members: __init__, __call__

        .. rubric:: Methods

        .. autoautosummary:: _Math.Functions.{0}
            :methods:
        
        .. rubric:: Attributes

        .. autoautosummary:: _Math.Functions.{0}
            :attributes:

        .. rubric:: Details
    """.format(name))

.. module:: Math

.. result of the python script

.. autoclass:: _Math.Functions.R1ToR1Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR1Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR1Analytical
        :attributes:

    .. rubric:: Details

.. autoclass:: _Math.Functions.R1ToR2Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR2Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR2Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR3Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR3Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR3Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR1Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR1Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR1Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR2Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR2Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR2Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR3Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR3Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR3Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR1Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR1Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR1Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR2Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR2Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR2Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR3Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR3Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR3Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR1Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR1Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR1Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR2Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR2Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR2Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR3Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR3Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR3Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR4Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR4Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR4Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R5ToR1Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R5ToR1Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R5ToR1Analytical
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R5ToR5Analytical
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R5ToR5Analytical
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R5ToR5Analytical
        :attributes:

    .. rubric:: Details

