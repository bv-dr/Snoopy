Uniform
=======

Uniform functions provide the possibility to write M uniform functions that will
be evaluated at runtime for the N input values.::

    >>> import numpy
    >>> from _Math.Functions import R4ToR2Uniform

    >>> vals = numpy.array((1., 2.), dtype=float)
    >>> func = R4ToR2Uniform(vals)
    >>> res = func(4., 5., 6., 7.)
    >>> print(res)
    [ 1.  2.]

    >>> inputValues = numpy.array((4., 5., 6., 7.), dtype=float)
    >>> res2 = func(inputValues)
    >>> print(res2)
    [ 1.  2.]

    >>> firstDeriv = func.dEval(inputValues)
    >>> print(firstDeriv)
    array([[ 0.,  0.,  0.,  0.],
           [ 0.,  0.,  0.,  0.]])

The name pattern of the functions is the following::

    RNToRMUniform

With :code:`N` the start (input) dimension and :code:`M` the end (output) dimension.

Here is a list of the available uniform functions:

- :py:class:`~_Math.Functions.R1ToR1Uniform`
- :py:class:`~_Math.Functions.R1ToR2Uniform`
- :py:class:`~_Math.Functions.R1ToR3Uniform`
- :py:class:`~_Math.Functions.R2ToR1Uniform`
- :py:class:`~_Math.Functions.R2ToR2Uniform`
- :py:class:`~_Math.Functions.R3ToR1Uniform`
- :py:class:`~_Math.Functions.R3ToR2Uniform`
- :py:class:`~_Math.Functions.R3ToR3Uniform`
- :py:class:`~_Math.Functions.R4ToR1Uniform`
- :py:class:`~_Math.Functions.R4ToR2Uniform`
- :py:class:`~_Math.Functions.R4ToR3Uniform`
- :py:class:`~_Math.Functions.R4ToR4Uniform`
- :py:class:`~_Math.Functions.R5ToR1Uniform`
- :py:class:`~_Math.Functions.R5ToR5Uniform`

..
    python script to generate the doc for each of the functions:

    fNames = ("R1ToR1Uniform", "R1ToR2Uniform", "R1ToR3Uniform", "R2ToR1Uniform",
              "R2ToR2Uniform", "R3ToR1Uniform", "R3ToR2Uniform", "R3ToR3Uniform",
              "R4ToR1Uniform", "R4ToR2Uniform", "R4ToR3Uniform", "R4ToR4Uniform",
              "R5ToR1Uniform", "R5ToR5Uniform")
    
    for name in fNames:
        print("""
    .. autoclass:: _Math.Functions.{0}
        :members:
        :inherited-members:
        :special-members: __init__, __call__
    
        .. rubric:: Methods
    
        .. autoautosummary:: _Math.Functions.{0}
            :methods:
        
        .. rubric:: Attributes
    
        .. autoautosummary:: _Math.Functions.{0}
            :attributes:
    
        .. rubric:: Details
    """.format(name))

.. module:: Math

.. result of the python script

.. autoclass:: _Math.Functions.R1ToR1Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR1Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR1Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR2Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR2Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR2Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R1ToR3Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R1ToR3Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R1ToR3Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR1Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR1Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR1Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R2ToR2Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R2ToR2Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R2ToR2Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR1Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR1Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR1Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR2Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR2Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR2Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R3ToR3Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R3ToR3Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R3ToR3Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR1Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR1Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR1Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR2Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR2Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR2Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR3Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR3Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR3Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R4ToR4Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R4ToR4Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R4ToR4Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R5ToR1Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R5ToR1Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R5ToR1Uniform
        :attributes:

    .. rubric:: Details


.. autoclass:: _Math.Functions.R5ToR5Uniform
    :members:
    :inherited-members:
    :special-members: __init__, __call__

    .. rubric:: Methods

    .. autoautosummary:: _Math.Functions.R5ToR5Uniform
        :methods:
    
    .. rubric:: Attributes

    .. autoautosummary:: _Math.Functions.R5ToR5Uniform
        :attributes:

    .. rubric:: Details

