.. _FunctionsModule:

Functions
=========

A function is a user defined relation between a set of inputs of size :math:`N`
and a set of permissible outputs of size :math:`M`.

.. math::

   f: \mathbb{R}^N \rightarrow \mathbb{R}^M

In addition function objects are able to return their first and second
derivatives according to their :math:`N` input values.

.. math::

   f': \mathbb{R}^N &\rightarrow \mathbb{R}^M \times \mathbb{R}^N \\
   f": \mathbb{R}^N &\rightarrow \mathbb{R}^M \times \mathbb{R}^N \times \mathbb{R}^N

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Zero
   Uniform
   Analytical
   Polynomial
   Discrete
