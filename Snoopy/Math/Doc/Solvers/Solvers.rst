Solvers
=======

Solvers are not directly wrapped in python. Their initialisation in Opera
analyses is done via their enum key :py:class:`~_Math.Solvers.SolverType` and
some calculation parameters :py:class:`~_Math.Solvers.SolverParameters`.

.. autoclass:: _Math.Solvers.SolverType
    :members:

.. autoclass:: _Math.Solvers.SolverParameters
    :members:
    :inherited-members:
    :special-members: __init__

