.. Math documentation master file, created by
   sphinx-quickstart on Wed Feb 28 09:13:51 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _MathDocumentation:

Welcome to Math's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Functions/Functions
   ODEIntegration/ODEIntegration
   Solvers/Solvers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
