ODE integration
===============

Many Ordinary Differential Equations (ODE) integration schemes are implemented
in Opera.

Generally  Opera solvers will initialise the calculations directly according to
the integration calculation parameters.

For more information on the parameters and on error calculation for step adaptation,
please refer to this `boost odeint documentation <http://www.boost.org/doc/libs/1_61_0/libs/numeric/odeint/doc/html/boost_numeric_odeint/odeint_in_detail/steppers.html#boost_numeric_odeint.odeint_in_detail.steppers.controlled_steppers>`_

.. module:: Math

.. autoclass:: _Math.ODEIntegration.ODEIntegrationParameters
    :members:
    :inherited-members:
    :special-members: __init__

.. autoclass:: _Math.ODEIntegration.StepperScheme
    :members:

