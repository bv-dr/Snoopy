#include "Meshing/Mesh.hpp"

#include "Tools/BVException.hpp"

#include <igl/readMESH.h>
#include <igl/readOBJ.h>
#include <igl/readOFF.h>
#include <igl/readSTL.h>
#include <igl/readPLY.h>
#include <igl/readWRL.h>
#include <igl/remove_unreferenced.h>
#include <igl/write_triangle_mesh.h>
#include <igl/pathinfo.h>
#include <igl/quad_planarity.h>
#include <igl/boundary_facets.h>
#include <igl/barycenter.h>
#include <igl/doublearea.h>
#include <igl/per_face_normals.h>
#include <igl/polygon_mesh_to_triangle_mesh.h>
#include <igl/dot_row.h>
#include <igl/cross.h>
#include <igl/planarize_quad_mesh.h>
#include <igl/bounding_box.h>
#include <igl/remove_duplicate_vertices.h>


using namespace BV::Meshing ;
using namespace BV::Tools ;

namespace BV {
namespace Meshing {
namespace Details {

template <class T, class T2>
void AddElement(std::vector<std::vector<T> > & container, const T2 & element)
{
    std::vector<T> t ;
    t.reserve(element.cols()) ;
    for (int i = 0; i < element.cols(); ++i)
    {
        t.push_back(element(0, i)) ;
    }
    container.push_back(t) ;
}

BV::Geometry::Point getSegmentPlaneIntersection(
    const BV::Geometry::Point & ptA, const BV::Geometry::Point & ptB,
    const BV::Geometry::Point & ptO, const BV::Geometry::Vector & direction)
{
    BV::Geometry::Vector AO(ptO - ptA) ;
    BV::Geometry::Vector AB(ptB - ptA) ;
    const double k((AO * direction) / (AB * direction)) ;
    return ptA + (AB * k) ;
}

template <typename IndexType>
void AddFace(const IndexType & ind0, const IndexType & ind1, const IndexType & ind2,
             std::vector<std::vector<unsigned int> > & container)
{
    std::vector<unsigned int> f ;
    f.reserve(3) ;
    f.push_back(static_cast<unsigned int>(ind0)) ;
    f.push_back(static_cast<unsigned int>(ind1)) ;
    f.push_back(static_cast<unsigned int>(ind2)) ;
    container.push_back(f) ;
}

template <typename IndexType>
void AddFace(const IndexType & ind0, const IndexType & ind1, const IndexType & ind2,
             const IndexType & ind3, std::vector<std::vector<unsigned int> > & container)
{
    std::vector<unsigned int> f ;
    f.reserve(4) ;
    f.push_back(static_cast<unsigned int>(ind0)) ;
    f.push_back(static_cast<unsigned int>(ind1)) ;
    f.push_back(static_cast<unsigned int>(ind2)) ;
    f.push_back(static_cast<unsigned int>(ind3)) ;
    container.push_back(f) ;
}

/*
 * ind0, ind1, ind2, ind3 defines a quadrilateral (or triangle if some indices equal to each other)
 * A = ind0, B = ind1, C = ind2, D = ind3
 * function find interaction between the plane and the sides AD and BC
 */
template <typename IndexType>
void Cut(const IndexType & ind0, const IndexType & ind1, const IndexType & ind2, const IndexType & ind3,
         const BV::Geometry::Point & pt, const BV::Geometry::Vector & direction,
         std::vector<std::vector<double> > & vertices,
         std::vector<std::vector<unsigned int> > & container)
{
    const BV::Geometry::Point ptA(vertices[ind0][0], vertices[ind0][1],
                                  vertices[ind0][2]) ;
    const BV::Geometry::Point ptB(vertices[ind1][0], vertices[ind1][1],
                                  vertices[ind1][2]) ;
    const BV::Geometry::Point ptC(vertices[ind2][0], vertices[ind2][1],
                                  vertices[ind2][2]) ;
    const BV::Geometry::Point ptD(vertices[ind3][0], vertices[ind3][1],
                                  vertices[ind3][2]) ;
    const BV::Geometry::Point pt1 = getSegmentPlaneIntersection(ptB, ptC, pt,
                                                                direction) ;
    const BV::Geometry::Point pt2 = getSegmentPlaneIntersection(ptA, ptD, pt,
                                                                direction) ;
    AddElement<double, Eigen::MatrixXd>(vertices, pt1.toArray().transpose()) ;
    AddElement<double, Eigen::MatrixXd>(vertices, pt2.toArray().transpose()) ;
    const IndexType nV(static_cast<IndexType>(vertices.size())) ; // after the 2 vertices are added
    if (ind0 == ind1) AddFace(ind0, nV - 2, nV - 1, container) ;        // A == B (triangle A,pt1, pt2)
    else AddFace(ind0, ind1, nV - 2, nV - 1, container) ;
}

template <typename IndexType>
void SetIndices(const std::vector<bool> & sideVector, const MatrixXu & F,
                const IndexType & i, const IndexType & nCols, IndexType & ind0,
				IndexType & ind1, IndexType & ind2, IndexType & ind3)
{
// Get previous and next point indices
    ind0 = F(i, nCols - 3) ;
    ind1 = F(i, nCols - 2) ;
    ind2 = F(i, nCols - 1) ;
    ind3 = F(i, 0) ;            // if nCols == 3 => ind3 == ind0
    bool firstFound(false) ;
    // modIndex is a lambda function, which capture nCols and has 1 argument index
    //                   index +nCols   if index < 0
    // modIndex(index) = index          if 0 <= index < nCols
    //                   index -nCols   if nCols <= index
    auto modIndex =
        [nCols] (const IndexType & index)->IndexType
        {
            return index < 0 ? index + nCols : index >= nCols ? index - nCols : index ;
        } ;
    // Next loop set inidces in the following order: (|-> means plane with normals)
    // Quads:
    // ind0, ind1, ind2, |-> ind3 (1 point ind3 in front of the plane)
    // |-> ind0, ind1, ind2, |-> ind3 (2 points ind0 and ind3 in front of the plane)
    // |-> ind0, |-> ind1, ind2, |-> ind3 (3 points ind0, ind1 and ind3 in front of the plane)
    // Triangles:
    // |-> ind0, ind1, ind2 (|-> ind3 == ind0) (1 point ind0 in front of the plane)
    // |-> ind0, |->ind1, ind2 (|-> ind3 == ind0) (2 points ind0 and ind1 in front of the plane)
    for (IndexType j = 0; j < nCols; ++j)
    {
        if (sideVector[F(i, j)])    // if j-th vertex of the i-th panel is behind the cut plane
        {
            if (!firstFound) firstFound = true ;    // mark, if it was not done, that found at least 1 vertex behind the plane
        }
        else
        {   // j-th vertex of the i-th panel is in front of the cut plane
            if (firstFound) // if earlier we found a vertex behind the cut plane
            {               // permutate indices
                            // actually, the previous vertex was already behind otherwise the loop was exited
                ind3 = F(i, j) ;
                ind2 = F(i, modIndex(j - 1)) ;
                ind1 = F(i, modIndex(j - 2)) ;
                ind0 = F(i, modIndex(j - 3)) ;
                break ;     // and exit the loop
            }
        }
        // continue to search a vertex behind the cut plane
    }
}

void AddPanels(std::vector<std::vector<double> > & vertices,
               std::vector<std::vector<unsigned int> > & triangles,
               std::vector<std::vector<unsigned int> > & quads,
               const std::vector<bool> & sideVector, const MatrixXu & F,
               const BV::Geometry::Point & pt,
               const BV::Geometry::Vector & direction,
               std::vector<size_t> &new2OldT,
               std::vector<size_t> &new2OldQ,
               const size_t &offset)
{
	using IndexType = typename MatrixXu::Index ;
    // F is either T_ or Q_, which has: T_.cols() == 3 and Q_.cols() == 4
    const IndexType Fsize(F.rows()) ;
    const IndexType nCols(F.cols()) ;
    // depending on F (triangles or quads) pContainer points to triangles or quads (unifies the name)
    std::vector<std::vector<unsigned int> > * pContainer = 0 ;
    std::vector<size_t> *pNew2Old = 0 ;
    if (nCols == 3)
    {
        pContainer = &triangles ;
        pNew2Old = &new2OldT ;
    }
    else if (nCols == 4)
    {
        pContainer = &quads ;
        pNew2Old = &new2OldQ ;
    }
    else
    {
        throw BV::Tools::Exceptions::BVException(
            "Can only add 3 or 4 vertices panels") ;
    }
    // lambda function which catches: sideVector, F and nCols == F.cols()
    // and has 1 argument index. The function returns a result of type IndexType
    // For a given index, getSum calculates the sum of sideVector values for a vertices of a panel F(index, i) for each column i
    // sideVector is a boolean vector which is true if a vertex behind the cut plane (distance to plane is negative)
    auto getSum = [sideVector, F, nCols](const IndexType & index)->IndexType
    {
        IndexType res(0) ;
        for (IndexType i=0 ; i< nCols ; ++i)
        {
            if (sideVector[F(index, i)]) ++res;
        }
        return res ;
    } ;
    IndexType ind0, ind1, ind2, ind3 ;
    for (IndexType i = 0; i < Fsize; ++i)
    {
        const IndexType n(getSum(i)) ;
        // test if all vertices of the plane F[i, :] are behind the cut plane
        if (n == nCols)
        {
            // yes => add panel and continue
            AddElement<unsigned int, MatrixXu>(*pContainer, F.row(i)) ;
            pNew2Old->push_back(i +offset);
            continue ;
        }
        else if (n == 0) continue ; // all vertices are in front of the plane => ignore and continue
        // Some vertices are behind and some of them are in front of the cut plane
        // SetIndices: set ind0, ind1, ind2, ind3 based on sideVector, F and panel i
        SetIndices(sideVector, F, i, nCols, ind0, ind1, ind2, ind3) ;
        if (n == 1) // cut a triangle: 1 vertex is behind and 2T(3Q) are in front of the plane
        {
            // Quads: |-> ind0, |-> ind1, ind2, |-> ind3
            // Tris : |-> ind0, |-> ind1, ind2, |-> ind3 == ind0
            // A = ind2, B = ind2, C = ind3(ind0T), D = ind1
            Cut(ind2, ind2, ind3, ind1, pt, direction, vertices, triangles) ;
            new2OldT.push_back(i +offset);
        }
        else if (n == 2) // Cut a quad: 2 vertices are behind and 1(2) are in front of the plane
        {
            // Quads: |-> ind0, ind1, ind2, |-> ind3
            // Tris : |-> ind0, |-> ind1, ind2, |-> ind3 == ind0
            // A = ind1, B = ind2, C = ind3(ind0T), D = ind0
            Cut(ind1, ind2, ind3, ind0, pt, direction, vertices, quads) ;
            new2OldQ.push_back(i +offset);
        }
        else if (n == 3) // Cut a triangle and a quad: 3 vertices are behind and 1 vertex is in front of the plane
        {
            // Quads: ind0, ind1, ind2, |-> ind3
            // ind0, ind1, ind2 -> triangle,
            // A = ind0, B = ind2, C = ind3, D = ind3
            Cut(ind0, ind2, ind3, ind3, pt, direction, vertices, quads) ;
            new2OldQ.push_back(i +offset);
            AddFace(ind0, ind1, ind2, triangles) ;
            new2OldT.push_back(i +offset);
        }
    }
}
}
}
}

PanelMetaData::PanelMetaData(const std::string& dataName,
                             const size_t& dataType,
                             const double& dataFreq,
                             const double& dataHead) :
    dataName_(dataName),
    dataType_(dataType),
    dataFreq_(dataFreq),
    dataHead_(dataHead)
{
}

unsigned int Mesh::getUnplanarQuadsNumber_(const double & tolerance) const
{
    unsigned int n(0) ;
    if (hasQuads_)
    {
        Eigen::VectorXd P ;
        igl::quad_planarity(V_, Q_, P) ;
        for (int i = 0; i < P.rows(); ++i)
        {
            if (std::fabs(double(P(i))) > tolerance) n++ ;
        }
    }
    return n ;
}

std::vector<std::vector<unsigned int> > Mesh::getDuplicatedVerticePanels_(
    const MatrixXu & F, const bool & checkAligned,
    const double & tolerance) const
{
    const double threshold(-1. + tolerance) ;
    std::vector<std::vector<unsigned int> > indicesToRemove ;
    const unsigned int nCols(static_cast<unsigned int>(F.cols())) ;
    for (unsigned int iRow = 0; iRow < F.rows(); ++iRow)
    {
        bool nextRow(false) ;
        for (unsigned int i = 0; i < nCols - 1; ++i)
        {
            if (nextRow) break ;
            const unsigned int value(F(iRow, i)) ;
            for (unsigned int j = i + 1; j < nCols; ++j)
            {
                if (F(iRow, j) == value)
                {
                    std::vector<unsigned int> temp ;
                    temp.reserve(2) ;
                    temp.push_back(iRow) ;
                    temp.push_back(j) ;
                    indicesToRemove.push_back(temp) ;
                    nextRow = true ;
                    break ;
                }
            }
        }
        if (nextRow) continue ;
        if (!checkAligned) continue ;
        for (unsigned int i = 0; i < nCols; ++i)
        {
            BV::Geometry::Vector AB(
                V_.row(F(iRow, i + 1 == nCols ? 0 : i + 1))
                    - V_.row(F(iRow, i))) ;
            BV::Geometry::Vector AC(
                V_.row(F(iRow, int(i - 1) == -1 ? nCols - 1 : i - 1))
                    - V_.row(F(iRow, i))) ;
            if (AB * AC < threshold * AB.norm() * AC.norm())
            {
                std::vector<unsigned int> temp ;
                temp.reserve(2) ;
                temp.push_back(iRow) ;
                temp.push_back(i) ;
                indicesToRemove.push_back(temp) ;
                break ;
            }
        }
    }
    return indicesToRemove ;
}

void Mesh::clearQuads_(double tolerance)
{
    //FIXME handle panelsData
    if (!hasQuads_) return ;
    std::vector<std::vector<unsigned int> > indicesToRemove =
        getDuplicatedVerticePanels_(Q_, true, tolerance) ; //FIXME set a parameter
    using IndexType = typename MatrixX3u::Index ;
    const IndexType nToRemove(static_cast<IndexType>(indicesToRemove.size())) ;

    if (nToRemove > 0)
    {
        
        IndexType iRow(0);
        if (hasTriangles_) iRow = T_.rows() ;
        MatrixX3u TT(iRow + nToRemove, 3) ;
        Eigen::ArrayXXd panelsData(panelsData_.rows(), panelsData_.cols());

        //Triangles are not modified
        if (hasTriangles_)
        {
            TT.block(0, 0, T_.rows(), 3) = T_;
            if (hasPanelsData())
            {
                panelsData.block(0, 0, T_.rows(), panelsData_.cols()) = panelsData_.block(0, 0, T_.rows(), panelsData_.cols());
            }
        }
        IndexType iQ(0) ;
        const IndexType n(Q_.rows() - nToRemove) ;
        if (n == 0) hasQuads_ = false ;
        MatrixX4u Q(n, 4) ;

        auto it = indicesToRemove.begin() ;
        for (IndexType i = 0; i < Q_.rows(); ++i)
        {
            if (it != indicesToRemove.end() && (*it)[0] == i)
            {
                const IndexType iCol(static_cast<IndexType>((*it)[1])) ;
                IndexType ind(0) ;
                for (IndexType j = 0; j < 4; ++j)
                {
                    if (j != iCol) TT(iRow, ind++) = Q_(i, j) ;
                }
                if (hasPanelsData())
                {
                    panelsData.row( iRow ) = panelsData_.row(T_.rows() + i);
                }
                ++iRow ;
                ++it ;
                continue ;
            }
            if (hasPanelsData())
            {
                panelsData.row(T_.rows() + nToRemove+ iQ) = panelsData_.row(T_.rows() + i);
            }
            Q.row(iQ++) = Q_.row(i) ;
        }
        Q_ = Q ;
        T_ = TT ;
        panelsData_ = panelsData;
        hasTriangles_ = true ;
    }
}

void Mesh::clearTriangles_(double tolerance)
{
    if (!hasTriangles_) return ;
    auto indicesToRemove = getDuplicatedVerticePanels_(T_, false, tolerance) ;
    using IndexType = typename MatrixX3u::Index ;
    const IndexType nToRemove(static_cast<IndexType>(indicesToRemove.size())) ;
    if (nToRemove > 0)
    {
        const IndexType nRows(T_.rows() - nToRemove) ;

        if (nRows == 0)
        {
            T_.setZero(0, 3) ;
            hasTriangles_ = false ;
            if (hasPanelsData())
            {
                panelsData_ = panelsData_.block(nToRemove, 0, Q_.rows(), panelsData_.cols());
            }
            return ;
        }

        Eigen::ArrayXXd panelsData ;
        if (hasPanelsData())
        {
            panelsData = Eigen::ArrayXXd(panelsData_.rows()-nToRemove, panelsData_.cols());
            //Quads data unchanged
            panelsData.block(nRows, 0, Q_.rows(), panelsData_.cols()) = panelsData_.block(T_.rows(), 0, Q_.rows(), panelsData_.cols());
        }

        MatrixX3u T(nRows, 3) ;
        IndexType iT(0) ;
        IndexType indToRemove(0) ;
        IndexType nextIndToRemove(static_cast<IndexType>(indicesToRemove[indToRemove][0])) ;
        for (IndexType i = 0; i < T_.rows(); ++i)
        {
            if (i == nextIndToRemove)
            {
                ++indToRemove ;
                if (indToRemove < nToRemove)
                    nextIndToRemove = static_cast<IndexType>(indicesToRemove[indToRemove][0]) ;
                continue ;
            }
            if (hasPanelsData())
            {
                panelsData.row(iT) = panelsData_.row(i);
            }
            T.row(iT++) = T_.row(i) ;
        }
        T_ = T ;
        panelsData_ = panelsData;
    }
}

void Mesh::offset(Eigen::Array3d coords)
{
    // V_.array() += coords.transpose();   // Should work ?
    for (auto i = 0; i < V_.rows(); ++i)
    {
        V_.array().row(i) += coords.transpose();
    }
}

void Mesh::scale(Eigen::Array3d scale)
{
    V_.array() *= scale.transpose();
}

void Mesh::clean(double tolerance)
{
    removeDuplicatedVertices_(tolerance) ;
    clearQuads_(tolerance);
    clearTriangles_(tolerance); // Must be called after clearQuads_
	removeUnreferencedNodes();
    refreshAll_();  // Necessary, as clearQuads_ and clearTriangles_ might have changed panels.
}

void Mesh::refreshNormals_(void)
{
    Eigen::MatrixXd normals ;
    Eigen::MatrixXd::Index offset(0) ;
    const unsigned int n(getTotalPanelsNumber()) ;
    normals_.setZero(n, 3) ;
    if (hasTriangles_)
    {
        offset = T_.rows() ;
        igl::per_face_normals(V_, T_, normals) ;
        normals_.block(0, 0, offset, 3) = normals ;
    }
    if (hasQuads_)
    {
        igl::per_face_normals(V_, Q_, normals) ;
        normals_.block(offset, 0, n - offset, 3) = normals ;
    }
}

void Mesh::refreshAreas_(void)
{
    Eigen::VectorXd areas ;
    Eigen::VectorXd::Index offset(0) ;
    const unsigned int n(getTotalPanelsNumber()) ;
    areas_.setZero(n) ;
    if (hasTriangles_)
    {
        offset = T_.rows() ;
        igl::doublearea(V_, T_, areas) ;
        areas_.block(0, 0, offset, 1) = areas ;
    }
    if (hasQuads_)
    {
        igl::doublearea(V_, Q_, areas) ;
        areas_.block(offset, 0, n - offset, 1) = areas ;
    }
    areas_ *= 0.5 ;
}

void Mesh::refreshBarycenters_(void)
{
    Eigen::MatrixXd barycenters ;
    Eigen::MatrixXd::Index offset(0) ;
    const unsigned int n(getTotalPanelsNumber()) ;
    barycenters_.setZero(n, 3) ;
    if (hasTriangles_)
    {
        offset = T_.rows() ;
        igl::barycenter(V_, T_, barycenters) ;
        barycenters_.block(0, 0, offset, 3) = barycenters ;
    }
    if (hasQuads_)
    {
        igl::barycenter(V_, Q_, barycenters) ;
        barycenters_.block(offset, 0, n - offset, 3) = barycenters ;
    }
}

void Mesh::removeDuplicatedVertices_(const double & tolerance)
{
    Eigen::MatrixX3d SV ;
    Eigen::Matrix<unsigned int, Eigen::Dynamic, 1> SVI, SVJ ;
    using IndexType = Eigen::VectorXd::Index ;
    igl::remove_duplicate_vertices(V_, tolerance, SV, SVI, SVJ) ;
    const IndexType n(V_.rows() - SV.rows()) ;
    if (n == 0) return ;
    if (hasTriangles_)
    {
        for (IndexType i = 0; i < T_.rows(); ++i)
        {
            for (IndexType j = 0; j < 3; ++j)
            {
                T_(i, j) = SVJ(T_(i, j)) ;
            }
        }
    }
    if (hasQuads_)
    {
        for (IndexType i = 0; i < Q_.rows(); ++i)
        {
            for (IndexType j = 0; j < 4; ++j)
            {
                Q_(i, j) = SVJ(Q_(i, j)) ;
            }
        }
    }
    V_ = SV ;
}

void Mesh::refreshAll_(void)
{
    // refreshBarycenters_() ; Not requested for the moment
    refreshAreas_() ;
    refreshNormals_() ;
}

void Mesh::read_(const std::string & filename)
{
    std::string d, b, ext, f ;
    igl::pathinfo(filename, d, b, ext, f) ;
// Convert extension to lower case
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower) ;
    std::vector<std::vector<double> > vV, vN, vTC, vC ;
    std::vector<std::vector<int> > vF, vFTC, vFN ;
    if (ext == "mesh")
    {
        MatrixX3u T ;
        if (!igl::readMESH(filename, V_, T, T_))
        {
            throw BV::Tools::Exceptions::BVException("Wrong MESH file") ;
        }
        //if(F.size() > T.size() || F.size() == 0)
        {
            igl::boundary_facets(T, T_) ;
        }
        hasTriangles_ = true ;
        hasQuads_ = false ;
        return ;
    }
    else if (ext == "stl")
    {
        Eigen::MatrixXd temp ;
        if (!igl::readSTL(filename, V_, T_, temp))
        {
            throw BV::Tools::Exceptions::BVException("Wrong STL file") ;
        }
        hasTriangles_ = true ;
        hasQuads_ = false ;
        return ;
    }
    else if (ext == "obj")
    {
        if (!igl::readOBJ(filename, vV, vTC, vN, vF, vFTC, vFN))
        {
            throw BV::Tools::Exceptions::BVException("Wrong OBJ file") ;
        }
        // There can be a 4th value for vertex weight
        for (auto & v : vV)
        {
            v.resize(std::min(v.size(), (size_t) 3)) ;
        }
    }
    else if (ext == "off")
    {
        if (!igl::readOFF(filename, vV, vF, vN, vC))
        {
            throw BV::Tools::Exceptions::BVException("Wrong OFF file") ;
        }
    }
    else if (ext == "ply")
    {
        if (!igl::readPLY(filename, vV, vF, vN, vTC))
        {
            throw BV::Tools::Exceptions::BVException("Wrong PLY file") ;
        }
    }
    else if (ext == "wrl")
    {
        if (!igl::readWRL(filename, vV, vF))
        {
            throw BV::Tools::Exceptions::BVException("Wrong WRL file") ;
        }
    }
    else
    {
        throw BV::Tools::Exceptions::BVException("Unknown extension file") ;
    }
    if (vV.size() > 0)
    {
        if (!igl::list_to_matrix(vV, V_))
        {
            throw BV::Tools::Exceptions::BVException(
                "Wrong definition of vertices") ;
        }
        // Sort elements in T_ and Q_
        std::vector<std::vector<int> > triangles ;
        std::vector<std::vector<int> > quads ;
        unsigned int errors(0) ;
        for (auto & v : vF)
        {
            const std::size_t n(v.size()) ;
            if (n == 3) triangles.push_back(v) ;
            else if (n == 4) quads.push_back(v) ;
            else errors++ ;
        }
        if (errors)
        {
            std::cout
                << "Warning: some panel are not defined with 3 nor 4 vertices"
                << std::endl ;
        }
        hasTriangles_ = false ;
        hasQuads_ = false ;
        if (triangles.size() > 0)
        {
            if (!igl::list_to_matrix(triangles, T_))
            {
                throw BV::Tools::Exceptions::BVException(
                    "Wrong definition of triangles") ;
            }
            hasTriangles_ = true ;
        }
        if (quads.size() > 0)
        {
            if (!igl::list_to_matrix(quads, Q_))
            {
                throw BV::Tools::Exceptions::BVException(
                    "Wrong definition of quads") ;
            }
            hasQuads_ = true ;
        }
    }
    else
    {
        throw BV::Tools::Exceptions::BVException("No vertex defined in file") ;
    }
}



MatrixX3u Mesh::getAllPanelsInTriangles_(void) const
{
    if (hasQuads_)
    {
        MatrixX3u T ;
        if (hasTriangles_)
        {
            MatrixX3u T2 ;
            igl::polygon_mesh_to_triangle_mesh(Q_, T2) ;
            typename MatrixX3u::Index nT(T_.rows()) ;
            const typename MatrixX3u::Index nT2(T2.rows()) ;
            T.setZero(nT + nT2, 3) ;
            T.block(0, 0, nT, 3) = T_ ;
            T.block(nT, 0, nT2, 3) = T2 ;
        }
        else
        {
            igl::polygon_mesh_to_triangle_mesh(Q_, T) ;
        }
        return T ;
    }
    if (hasTriangles_) return T_ ;
    throw BV::Tools::Exceptions::BVException(
        "Cannot get triangles panels from empty mesh") ;
}

Eigen::VectorXd Mesh::getOrthoDistanceToPlane_(
    const Eigen::MatrixX3d & pts, const BV::Geometry::Point & pt,
    const BV::Geometry::Vector & normal) const
{
    const Eigen::MatrixXd::Index n(pts.rows()) ;
    Eigen::MatrixXd D(n, 3) ;
    D.col(0).setConstant(normal.x()) ;
    D.col(1).setConstant(normal.y()) ;
    D.col(2).setConstant(normal.z()) ;
    Eigen::MatrixXd AB(n, 3) ;
    AB.col(0).setConstant(-pt.x()) ;
    AB.col(1).setConstant(-pt.y()) ;
    AB.col(2).setConstant(-pt.z()) ;
    AB += pts ;
    return igl::dot_row(AB, D) ;
}

unsigned int Mesh::refreshTrianglesGaussPoints_(
    const unsigned int & nPts, const unsigned int & ruleNumber) const
{
    const double x1(0.) ;
    const double y1(0.) ;
    const double y2(0.) ;
    unsigned int index(0) ;
    for (unsigned int iT = 0; iT < T_.rows(); ++iT)
    {
        const Eigen::VectorXd vAB = V_.row(T_(iT, 1)) - V_.row(T_(iT, 0)) ;
        const BV::Geometry::Vector AB(vAB) ;
        const BV::Geometry::Vector AC(V_.row(T_(iT, 2)) - V_.row(T_(iT, 0))) ;
        const double nAB(AB.norm()) ;
        const BV::Geometry::Vector x = AB / nAB ;
        const BV::Geometry::Vector y = ((x ^ AC) ^ x).normalised() ;
        const Eigen::Vector3d vX = x.toArray() ;
        const Eigen::Vector3d vY = y.toArray() ;
        const double x2(nAB) ;
        const double x3(AC * x) ;
        const double y3(AC * y) ;
        for (unsigned int iPt = 0; iPt < nPts; ++iPt)
        {
            const std::vector<double> & gaussPt =
                BV::Meshing::Tools::GaussGenerator::getTriPoint(iPt,
                                                                ruleNumber) ;
            const double ksi(gaussPt[0]) ;
            const double eta(gaussPt[1]) ;
            const double wi(gaussPt[2]) ;
            const double N1(1. - ksi - eta) ;
            const double N2(ksi) ;
            const double N3(eta) ;
            gaussPointsInPanel_(index, 0) = x1 * N1 + x2 * N2 + x3 * N3 ;
            gaussPointsInPanel_(index, 1) = y1 * N1 + y2 * N2 + y3 * N3 ;
            gaussWiWjDetJ_(index) = wi * areas_(iT) ;
            normalsAtGaussPoints_.row(index).head(3) = normals_.row(iT) ;
            const Eigen::Vector3d vInPanel = vX * gaussPointsInPanel_(index, 0)
                + vY * gaussPointsInPanel_(index, 1) ;
            gaussPoints_.row(index) = V_.row(T_(iT, 0)) + vInPanel.transpose() ;
            ++index ;
        }
    }
    return index ;
}

void Mesh::refreshQuadsGaussPoints_(unsigned int index) const
{
    if (!hasQuads_) return ;
    const Eigen::VectorXd::Index offset = hasTriangles_ ? T_.rows() : 0 ;
    const double x1(0.) ;
    const double y1(0.) ;
    const double y2(0.) ;
    for (unsigned int iQ = 0; iQ < Q_.rows(); ++iQ)
    {
        const Eigen::VectorXd vAB = V_.row(Q_(iQ, 1)) - V_.row(Q_(iQ, 0)) ;
        const Eigen::VectorXd vAD = V_.row(Q_(iQ, 3)) - V_.row(Q_(iQ, 0)) ;
        const BV::Geometry::Vector AB(vAB) ;
        const BV::Geometry::Vector AC(V_.row(Q_(iQ, 2)) - V_.row(Q_(iQ, 0))) ;
        const BV::Geometry::Vector AD(vAD) ;
        const double nAB(AB.norm()) ;
        const BV::Geometry::Vector x = AB / nAB ;
        const BV::Geometry::Vector y = ((x ^ AD) ^ x).normalised() ;
        const Eigen::Vector3d vX = x.toArray() ;
        const Eigen::Vector3d vY = y.toArray() ;
        const double x2(nAB) ;
        const double x3(AC * x) ;
        const double y3(AC * y) ;
        const double x4(AD * x) ;
        const double y4(AD * y) ;
        for (unsigned int i = 0; i < nPtsPerDirection_; ++i)
        {
            const double ksi(
                BV::Meshing::Tools::GaussGenerator::getPoint(
                    i, nPtsPerDirection_)) ;
            const double wi(
                Meshing::Tools::GaussGenerator::getWeight(i,
                                                          nPtsPerDirection_)) ;
            for (unsigned int j = 0; j < nPtsPerDirection_; ++j)
            {
                const double eta(
                    BV::Meshing::Tools::GaussGenerator::getPoint(
                        j, nPtsPerDirection_)) ;
                const double N1(0.25 * (1. - ksi) * (1. - eta)) ;
                const double N2(0.25 * (1. + ksi) * (1. - eta)) ;
                const double N3(0.25 * (1. + ksi) * (1. + eta)) ;
                const double N4(0.25 * (1. - ksi) * (1. + eta)) ;
                gaussPointsInPanel_(index, 0) = x1 * N1 + x2 * N2 + x3 * N3
                    + x4 * N4 ;
                gaussPointsInPanel_(index, 1) = y1 * N1 + y2 * N2 + y3 * N3
                    + y4 * N4 ;
                double ad((1. - eta) * (x2 - x1) + (1. + eta) * (x3 - x4)) ;
                ad *= (1. - ksi) * (y4 - y1) + (1. + ksi) * (y3 - y2) ;
                double bc((1. - ksi) * (x4 - x1) + (1. + ksi) * (x3 - x2)) ;
                bc *= (1. - eta) * (y2 - y1) + (1. + eta) * (y3 - y4) ;
                gaussWiWjDetJ_(index) = 0.25 * 0.25 * (ad - bc) * wi
                    * BV::Meshing::Tools::GaussGenerator::getWeight(
                        j, nPtsPerDirection_) ;
                normalsAtGaussPoints_.row(index).head(3) = normals_.row(
                    offset + iQ) ;
                const Eigen::Vector3d vInPanel = vX
                    * gaussPointsInPanel_(index, 0)
                    + vY * gaussPointsInPanel_(index, 1) ;
                gaussPoints_.row(index) = V_.row(Q_(iQ, 0))
                    + vInPanel.transpose() ;
                ++index ;
            }
        }
    }
}

Mesh::Mesh() : gaussMethod_(GaussPointsMethods::HYDROSTAR), hasTriangles_(false), hasQuads_(false)
{
    resetPanelsData();
    refreshAll_();
}

Mesh::Mesh(const Eigen::MatrixX3d & V,
           const MatrixXu & F, const SymmetryTypes & symType,
           const unsigned int & nPtsPerDirection, bool keepSym,
           const Eigen::ArrayXXd & panelsData,
           const std::vector<PanelMetaData> & panelsMeta) :
    V_(V), T_(), Q_(), normals_(), areas_(), barycenters_(),
    gaussPointsInPanel_(), gaussPoints_(), gaussWiWjDetJ_(),
    normalsAtGaussPoints_(), nPtsPerDirection_(nPtsPerDirection),
    gaussMethod_(GaussPointsMethods::HYDROSTAR),
    hasTriangles_(), hasQuads_(), sym_(symType)
{
    const typename MatrixXu::Index nCols(F.cols()) ;
    if (nCols == 3)
    {
        T_ = F ;
        hasTriangles_ = true ;
        hasQuads_ = false ;
    }
    else if (nCols == 4)
    {
        Q_ = F ;
        hasTriangles_ = false ;
        hasQuads_ = true ;
    }
    else
    {
        throw BV::Tools::Exceptions::BVException(
            "Only triangles and quads are allowed") ;
    }
    if (panelsData.cols() > 0)
    {
        setPanelsData(panelsData);
    }
    else
    {
        resetPanelsData();
    }
    if (panelsMeta.size() > 0)
        setPanelsMeta(panelsMeta);

    if (keepSym == false) toSymmetry(SymmetryTypes::NONE);

    clean() ;

}

Mesh::Mesh(const Eigen::MatrixX3d & V,
           const MatrixX3u & T, const MatrixX4u & Q,
           const SymmetryTypes & symType, const unsigned int & nPtsPerDirection,
           bool keepSym,
           const Eigen::ArrayXXd & panelsData,
           const std::vector<PanelMetaData> & panelsMeta) :
    V_(V), T_(T), Q_(Q), normals_(), areas_(), barycenters_(),
    gaussPointsInPanel_(), gaussPoints_(), gaussWiWjDetJ_(),
    normalsAtGaussPoints_(), nPtsPerDirection_(nPtsPerDirection),
    gaussMethod_(GaussPointsMethods::HYDROSTAR),
    hasTriangles_(T.rows() > 0), hasQuads_(Q.rows() > 0), sym_(symType)
{
    if (panelsData.cols() > 0)
    {
        setPanelsData(panelsData);
    }
    else
    {
        resetPanelsData();
    }
    if (panelsMeta.size() > 0)
        setPanelsMeta(panelsMeta);

    if (keepSym == false) toSymmetry(SymmetryTypes::NONE);
    clean() ;

}

Mesh::Mesh(const std::string & filename,
           const SymmetryTypes & symType, const unsigned int & nPtsPerDirection,
           bool keepSym) :
    V_(), T_(), Q_(), normals_(), areas_(), barycenters_(),
    gaussPointsInPanel_(), gaussPoints_(), gaussWiWjDetJ_(),
    normalsAtGaussPoints_(), nPtsPerDirection_(nPtsPerDirection),
    gaussMethod_(GaussPointsMethods::HYDROSTAR),
    hasTriangles_(), hasQuads_(), sym_(symType)
{
    read_(filename) ;
    if (keepSym == false) toSymmetry(SymmetryTypes::NONE);
    clean() ;
    resetPanelsData(); // FIXME read panels data
    refreshAll_() ;
}

void BV::Meshing::Mesh::toSymmetry(SymmetryTypes sym)
{
    if (sym == sym_) { return; }
    if (sym == SymmetryTypes::NONE)  symmetrize(sym_);
    sym_ = sym;
    return;
}

void BV::Meshing::Mesh::removeUnreferencedNodes()
{
    Eigen::MatrixX3d new_V;
    MatrixX4u new_P;
    Eigen::VectorXi IM;
    MatrixX4u allP(getAllPanels());
    igl::remove_unreferenced( V_, allP, new_V, new_P, IM);
    V_ = new_V;
    Q_ = new_P.block(0, 0, Q_.rows(), 4);
    T_ = new_P.block(Q_.rows(), 0, T_.rows(), 3);
}

unsigned int Mesh::getTotalPanelsNumber(void) const
{
    return static_cast<unsigned int>((hasTriangles_ ? T_.rows() : 0) + (hasQuads_ ? Q_.rows() : 0)) ;
}

void Mesh::convertPanelsInTriangles(void)
{
    if (hasQuads_)
    {
        T_ = getAllPanelsInTriangles_() ;
        Q_.setZero(0, 4) ;
        hasTriangles_ = true ;
        hasQuads_ = false ;
        refreshAll_() ;
    }
}

void Mesh::removeUnplanarQuadsToTriangles(const double & tolerance)
{
    if (!hasQuads_) return ;
    Eigen::VectorXd P ;
    igl::quad_planarity(V_, Q_, P) ;
    unsigned n(0) ;
    for (unsigned int i = 0; i < P.rows(); ++i)
    {
        P(i) = std::fabs(double(P(i))) ;
        if (P(i) > tolerance) n++ ;
    }
    if (n == 0) return ;
    MatrixX4u Q(Q_.rows() - n, 4) ;
    MatrixX4u QToRemove(n, 4) ;
    unsigned int iQ(0), iQToRemove(0) ;
    for (unsigned int i = 0; i < Q_.rows(); ++i)
    {
        if (P(i) > tolerance)
        {
            QToRemove.row(iQToRemove++) = Q_.row(i) ;
        }
        else
        {
            Q.row(iQ++) = Q_.row(i) ;
        }
    }
    Q_ = Q ;
    if (hasTriangles_)
    {
        MatrixX3u T2 ;
        igl::polygon_mesh_to_triangle_mesh(QToRemove, T2) ;
        typename MatrixX3u::Index nT(T_.rows()) ;
        const typename MatrixX3u::Index nT2(T2.rows()) ;
        MatrixX3u T(nT + nT2, 3) ;
        T.block(0, 0, nT, 3) = T_ ;
        T.block(nT, 0, nT2, 3) = T2 ;
        T_ = T ;
    }
    else
    {
        igl::polygon_mesh_to_triangle_mesh(QToRemove, T_) ;
        hasTriangles_ = true ;
    }
    refreshAll_() ;
}


void Mesh::orientPanels(const int & direction )
{
    // Triangles
    for (Eigen::Index i = 0; i < T_.rows(); ++i)
    {
        if (direction * normals_(i, 2) < 0.) 
        {
            T_.row(i) = T_.row(i).reverse().eval();
        }
    }

    // Quadrangles
    for (Eigen::Index i = T_.rows(); i < normals_.rows(); ++i)
    {
        if (direction * normals_(i, 2) < 0.)
        {
            Q_.row( i - T_.rows() ) = Q_.row(i - T_.rows()).reverse().eval();
        }
    }
    refreshGaussPoints();
}


void Mesh::removeUpwardNormals(const double & tolerance)
{
    // would be 2 simple lines in python, there is probably a much clever way than below in cpp as well

    // Triangles
    std::vector<Eigen::Index> toKeep;
    for (Eigen::Index i = 0; i < T_.rows(); ++i)
    {
        if (normals_(i, 2) < tolerance)  { toKeep.push_back(i); }
    }
    BV::Tools::MatrixX3u T_new(toKeep.size(),3);
    for (size_t i = 0; i < toKeep.size(); ++i)
    {
        T_new.row(i) = T_.row( toKeep[i]  );
    }
    
    // Quadrangles
    toKeep.clear();
    for (Eigen::Index i = T_.rows(); i < normals_.rows(); ++i)
    {
        if (normals_(i, 2) < tolerance) { toKeep.push_back(i-T_.rows()); }
    }
    BV::Tools::MatrixX4u Q_new(toKeep.size(), 4); // Quads
    for (unsigned int i = 0; i < toKeep.size(); ++i)
    {
            Q_new.row(i) = Q_.row(toKeep[i]);
    }

    Q_ = Q_new;
    T_ = T_new;
    clean();
}

void Mesh::write(const std::string & filename) const
{
    igl::write_triangle_mesh(filename, V_, getAllPanelsInTriangles_()) ;
}

void Mesh::printDetails(const bool & minimal) const
{
    if (isEmpty())
    {
        std::cout << "Mesh is empty" << std::endl ;
        return ;
    }
    std::cout << "Number of vertices: " << V_.rows() << std::endl ;
    if (!minimal)
    {
        for (int i = 0; i < V_.rows(); ++i)
        {
            std::cout << "\t" << V_.row(i) << std::endl ;
        }
    }
    std::cout << "Panels:" << std::endl ;
    std::cout << "  - Number of triangles: " << T_.rows() << std::endl ;
    std::cout << "  - Number of quads: " << Q_.rows() << " ("
        << getUnplanarQuadsNumber_() << " unplanar)" << std::endl ;
    if (!minimal)
    {
        for (int i = 0; i < Q_.rows(); ++i)
        {
            std::cout << "\t" << Q_.row(i) << std::endl ;
        }
    }
    std::cout << "Booleans for T and Q: " << hasTriangles_ << ", " << hasQuads_
        << std::endl ;
    Eigen::MatrixXd pts ;
    MatrixXu faces ;
    igl::bounding_box(V_, pts, faces) ;
    std::cout << "Bounding box coordinates" << std::endl ;
    std::cout << pts.colwise().minCoeff() << std::endl ;
    std::cout << pts.colwise().maxCoeff() << std::endl ;
}

void Mesh::symmetrize(const SymmetryTypes & symType)
{
    if (symType == NONE) return ;
    if (symType == XZ_YZ_PLANES)
    {
        symmetrize(XZ_PLANE) ;
        symmetrize(YZ_PLANE) ;
        return ;
    }
    if (symType == XY_XZ_PLANES)
    {
        symmetrize(XY_PLANE) ;
        symmetrize(XZ_PLANE) ;
        return ;
    }
    if (symType == XY_YZ_PLANES)
    {
        symmetrize(XY_PLANE) ;
        symmetrize(YZ_PLANE) ;
        return ;
    }
    if (symType == XY_XZ_YZ_PLANES)
    {
        symmetrize(XY_PLANE) ;
        symmetrize(XZ_PLANE) ;
        symmetrize(YZ_PLANE) ;
        return ;
    }
    //Vertices
    const Eigen::MatrixX3d::Index nRows(V_.rows()) ;
    Eigen::MatrixX3d V(nRows * 2, 3) ;
    V.block(0, 0, nRows, 3) = V_ ;
    V.block(nRows, 0, nRows, 3) = V_ ;
    if (symType == XZ_PLANE) V.block(nRows, 1, nRows, 1) *= -1. ;
    else if (symType == YZ_PLANE) V.block(nRows, 0, nRows, 1) *= -1. ;
    else if (symType == XY_PLANE) V.block(nRows, 2, nRows, 1) *= -1. ;
    V_ = V ;

    //Triangles
    if (hasTriangles_)
    {
        const Eigen::MatrixX3d::Index n(T_.rows()) ;
        MatrixX3u T(n * 2, 3) ;
        T.block(0, 0, n, 3) = T_ ;
        T.block(n, 0, n, 1) = T_.col(0) ;
        T.block(n, 1, n, 1) = T_.col(2) ;
        T.block(n, 2, n, 1) = T_.col(1) ;
        T.block(n, 0, n, 3) += MatrixX3u::Constant(n, 3, static_cast<unsigned int>(nRows)) ;
        T_ = T ;
    }

    //Quads
    if (hasQuads_)
    {
        const Eigen::MatrixX3d::Index n(Q_.rows()) ;
        MatrixX4u Q(n * 2, 4) ;
        Q.block(0, 0, n, 4) = Q_ ;
        Q.block(n, 0, n, 1) = Q_.col(0) ;
        Q.block(n, 1, n, 1) = Q_.col(3) ;
        Q.block(n, 2, n, 1) = Q_.col(2) ;
        Q.block(n, 3, n, 1) = Q_.col(1) ;
        Q.block(n, 0, n, 4) += MatrixX4u::Constant(n, 4, static_cast<unsigned int>(nRows)) ;
        Q_ = Q ;
    }

    //Data
    auto n = panelsData_.rows();
    auto c = panelsData_.cols();
    panelsData_.conservativeResize(n*2,c);
    panelsData_.block(n, 0, n, c) = panelsData_.block(0, 0, n, c);

    refreshAll_();
}


void Mesh::append(const Mesh & mesh)
{
    if (mesh.isEmpty()) return ;

    //Vertices
    Eigen::MatrixX3d V0 = V_ ;
    Eigen::MatrixX3d V1 = mesh.getVertices() ;
    using IndexType = Eigen::MatrixX3d::Index ;
    const IndexType nV0(V0.rows()) ;
    const IndexType nV1(V1.rows()) ;
    V_ = Eigen::MatrixX3d(nV0 + nV1, 3) ;
    V_.block(0, 0, nV0, 3) = V0 ;
    V_.block(nV0, 0, nV1, 3) = V1 ;

    //Panels data
    auto ncol = panelsData_.cols();
    auto n_old = panelsData_.rows();
    auto n_added = mesh.getPanelsData().rows();
    auto n_tri_old = T_.rows();
    auto n_tri_added = mesh.T_.rows();
    auto n_quad_old = Q_.rows();
    auto n_quad_added = mesh.Q_.rows();
    Eigen::ArrayXXd panelsData(n_old + n_added, ncol);

    panelsData.block(0, 0, n_tri_old, ncol) = panelsData_.block(0, 0, n_tri_old, ncol);
    panelsData.block(n_tri_old, 0, n_tri_added, ncol) = mesh.getPanelsData().block(0, 0, n_tri_added, ncol);
    panelsData.block(n_tri_old + n_tri_added, 0, n_quad_old, ncol) = panelsData_.block(n_tri_old, 0, n_quad_old, ncol);
    panelsData.block(n_tri_old + n_tri_added + n_quad_old, 0, n_quad_added, ncol) = mesh.getPanelsData().block(n_tri_added, 0, n_quad_added, ncol);

    panelsData_ = panelsData;

    //Tris
    BV::Tools::MatrixX3u T0 = T_ ;
    BV::Tools::MatrixX3u T1 = mesh.getTriangles() ;
    const IndexType nT0 = hasTriangles_ ? T_.rows() : 0 ;
    const IndexType nT1 = T1.rows() ;
    if (nT0 + nT1 == 0)
    {
        hasTriangles_ = false ;
    }
    else
    {
        T_ = BV::Tools::MatrixX3u(nT0 + nT1, 3) ;
        if (hasTriangles_) T_.block(0, 0, nT0, 3) = T0;
        if (nT1 != 0) T_.block(nT0, 0, nT1, 3).array() = T1.array() + nV0;
        hasTriangles_ = true ;
    }

    //Quads
    BV::Tools::MatrixX4u Q0 = Q_ ;
    BV::Tools::MatrixX4u Q1 = mesh.getQuads() ;
    const IndexType nQ0 = hasQuads_ ? Q_.rows() : 0 ;
    const IndexType nQ1 = Q1.rows() ;
    if (nQ0 + nQ1 == 0)
    {
        hasQuads_ = false ;
    }
    else
    {
        Q_ = BV::Tools::MatrixX4u(nQ0 + nQ1, 4) ;
        if (hasQuads_) Q_.block(0, 0, nQ0, 4) = Q0;
        if (nQ1 != 0) Q_.block(nQ0, 0, nQ1, 4).array() = Q1.array() + nV0;
        hasQuads_ = true ;
    }

    clean();
}

void Mesh::planarizeQuads(const int & maxIter, const double & threshold)
{
    Eigen::MatrixX3d V ;
    igl::planarize_quad_mesh(V_, Q_, maxIter, threshold, V) ;
    V_ = V ;
    refreshAll_() ;
}

void Mesh::setVertices( const Eigen::MatrixX3d & vertices  )
{
    if ( V_.size() != vertices.size() )
        {
        throw BV::Tools::Exceptions::BVException("Number of vertices should not be modified") ;
        }
    V_ = vertices;
}

Mesh Mesh::getCuttedMesh(const BV::Geometry::Point & pt,
                         const BV::Geometry::Vector & direction) const
{
    // build relative position vector
    const Eigen::VectorXd::Index n(V_.rows()) ;
    Eigen::VectorXd dotVector = getOrthoDistanceToPlane_(V_, pt, direction) ;
    std::vector<bool> sideVector ;
    std::vector<std::vector<double> > vertices ;
    sideVector.reserve(n) ;
    vertices.reserve(n) ;
    for (Eigen::VectorXd::Index i = 0; i < n; ++i)
    {
        sideVector.push_back(bool(dotVector(i) < 0)) ;
        Details::AddElement<double, Eigen::MatrixX3d>(vertices, V_.row(i)) ;
    }
    std::vector<std::vector<unsigned int> > triangles ;
    std::vector<std::vector<unsigned int> > quads ;
    // new2Old[new_panel] = old_panel
    std::vector<size_t> new2OldT ;    // for triangles
    std::vector<size_t> new2OldQ ;    // for quads
    if (hasTriangles_)
    {
        Details::AddPanels(vertices, triangles, quads, sideVector, T_, pt,
                           direction, new2OldT, new2OldQ, 0) ;
    }
    if (hasQuads_)
    {
        Details::AddPanels(vertices, triangles, quads, sideVector, Q_, pt,
                           direction, new2OldT, new2OldQ, T_.rows()) ;
    }
    Eigen::MatrixX3d V ;
    MatrixX3u T ;
    MatrixX4u Q ;
    igl::list_to_matrix(vertices, V) ;
    igl::list_to_matrix(triangles, T) ;
    igl::list_to_matrix(quads, Q) ;

    Mesh newMesh(V, T, Q, sym_, nPtsPerDirection_, true) ;

    // merging new2OldT and new2OldQ
    Eigen::Array<size_t, Eigen::Dynamic, 1> new2Old(new2OldT.size() +new2OldQ.size()) ;
    const size_t offset(new2OldT.size()) ;
    for(size_t i = 0; i < new2OldT.size(); i ++)
        new2Old[i] = new2OldT[i] ;
    for(size_t i = 0; i < new2OldQ.size(); i ++)
        new2Old[i+offset] = new2OldQ[i] ;
    if (hasPanelsData())
    {
        Eigen::ArrayXXd panelsData(new2Old.size(), panelsData_.cols());
        for(size_t i = 0; i < (size_t)new2Old.size(); i++)
            panelsData.row(i) = panelsData_.row(new2Old[i]);
        newMesh.setPanelsData(panelsData);
        newMesh.setPanelsMeta(getPanelsMeta());
    }

    return newMesh;
}

void Mesh::refreshGaussPoints(const unsigned int & nPerDirection) const
{
    if (nPerDirection > 0) nPtsPerDirection_ = nPerDirection;
    if (gaussMethod_ == GaussPointsMethods::HYDROSTAR && nPtsPerDirection_ < 2) // nPtsPerDirection_ = 1 or 0 (use previous)
    {
        spdlog::debug("Refreshing Gauss Points. GaussMethod : HydroStar, nb_gp = {}", nPtsPerDirection_);
        /*
         * HydroStar style based on hslec/fillProphull.f90: calcProp0
         * The following code is simplified and uses 'C' style
         * x13 = x3 -x1
         * x24 = x4 -x2
         * xn = x13 ^ x24 (xn.x = x13.y x24.z -x13.z x24.y,
         *                 xn.y = x13.z x24.x -x13.x x24.z,
         *                 xn.z = x13.x x24.y -x13.y x24.x)
         * n /= xn.norm     normal vector to panel
         *
         * qn = x13.norm
         * xn = x13/qn == x13/x13.norm
         * yn = n ^ xn  (yn.x = n.y xn.z -n.z xn.y,
         *               yn.y = n.z xn.x -n.x xn.z,
         *               yn.z = n.x xn.y -n.y xn.x) unit vector perpendicular to x13 and n
         * since n is perpendicular to x13 and x24, it should be perpendicular to
         * x12 and x14 (this is not necessary the case if the panel is not flat)
         * Thus, yn lies in the panel's plane and perpendicular to x13
         *
         * xo1 = (x1 +x2 +x3) /3.0  middle point of a triangule x1,x2,x3
         * xo2 = (x1 +x3 +x4) /3.0  middle point of a triangule x1,x3,x4
         * aire1 = |yn * (x2-x1)| *x13.norm *1/2    area of triangule with sides x12 and x13
         * aire2 = |yn * (x4-x1)| *x13.norm *1/2    area of triangule with sides x14 and x13
         * Note:
         *  yn *(x2-x1) is the projection of the vector x12 to yn, which is the height of the triangule x1,x2,x3
         *  yn *(x4-x1) is the projection of the vector x14 to yn, which is the height of the triangule x1,x3,x4
         * airet = aire1 +aire2
         * x_COG_p = (aire1 *xo1 +aire2 *xo2)/airet
         */
        Eigen::Index index(0);
        Eigen::ArrayXXd res(getTotalPanelsNumber(), 3);
        gaussWiWjDetJ_.resize(getTotalPanelsNumber());
        if (hasTriangles_)
        {
            for(Eigen::Index iT = 0; iT < T_.rows(); iT ++)
            {
                Eigen::Array3d x1(V_.row(T_(iT,0)));
                Eigen::Array3d x2(V_.row(T_(iT,1)));
                Eigen::Array3d x3(V_.row(T_(iT,2)));

                Eigen::Vector3d x13(x3-x1);
                Eigen::Vector3d x12(x2 - x1);
                Eigen::Vector3d x23(x3-x2);
                Eigen::Vector3d n((x13.cross(x23)).normalized());
                //normals_.row(index) = n;  // does not work, why?
                static_cast<Eigen::RowVector3d>(normals_.row(index)) = n.transpose();
                res.row(index) = (x1+x2+x3)/3.;
                Eigen::Vector3d yn(n.cross(x13));
                double aire1(abs(yn.dot(x12) * 0.5)); // no need to multiply by ||x13||, because ||yn|| = ||x13||
                gaussWiWjDetJ_(index) = aire1;
                //gaussWiWjDetJ_(index) = areas_(index);
                index ++;
            }
        }
        if (hasQuads_)
        {
            for(Eigen::Index iQ = 0; iQ < Q_.rows(); iQ ++)
            {
                Eigen::Array3d x1(V_.row(Q_(iQ,0)));
                Eigen::Array3d x2(V_.row(Q_(iQ,1)));
                Eigen::Array3d x3(V_.row(Q_(iQ,2)));
                Eigen::Array3d x4(V_.row(Q_(iQ,3)));
                Eigen::Vector3d x13(x3-x1);
                Eigen::Vector3d x24(x4-x2);
                Eigen::Vector3d x12(x2-x1);
                Eigen::Vector3d x14(x4-x1);
                Eigen::Vector3d n((x13.cross(x24)).normalized());
                //normals_.row(index) = n;  // does not work, why?
                static_cast<Eigen::RowVector3d>(normals_.row(index)) = n.transpose();
                Eigen::Vector3d yn(n.cross(x13));
                Eigen::Vector3d xo1( (x1+x2+x3)/3.0 );
                Eigen::Vector3d xo2( (x1+x3+x4)/3.0 );
                double aire1( abs(yn.dot(x12)*0.5)); // no need to multiply by ||x13||, because ||yn|| = ||x13||
                double aire2( abs(yn.dot(x14)*0.5));
                double airet( aire1 +aire2 );
                res.row(index) = (aire1 *xo1 +aire2 *xo2)/airet;
                gaussWiWjDetJ_(index) = airet;
                //gaussWiWjDetJ_(index) = areas_(index);        It seems that areas_(index) is very different from airet (same thing)
                //                                              Also, I (iten) do not understand, why the wrong gaussWiWjDetJ_(index) affects
                //                                              generalized normals: n4, n5 and n6.
                index ++;
            }
        }
        gaussPoints_ = res;
        normalsAtGaussPoints_.resize(getTotalPanelsNumber(), 6);
        normalsAtGaussPoints_.block(0,0,gaussPoints_.rows(), 3) = normals_;
        for (Eigen::Index index = 0; index < gaussPoints_.rows(); index++)
        {
            Eigen::Vector3d ref(static_cast<Eigen::Vector3d>(gaussPoints_.row(index)) -refPoint_);
            normalsAtGaussPoints_.row(index).rightCols(3) = ref.cross(normals_.row(index));
        }
    }
    else
    {
        spdlog::debug("Refreshing Gauss Points. GaussMethod : Standard, nb_gp = {}", nPtsPerDirection_);
        if (nPtsPerDirection_ < 1)
            throw BV::Tools::Exceptions::BVException(
                "Wrong number of Gauss points per direction (must be >= 1)") ;
        unsigned int nPtsTri(0) ;
        unsigned int nTable(0) ;
        unsigned int ruleNumber(0) ;
        if (hasTriangles_)
        {
            ruleNumber = nPtsPerDirection_ ;
            nPtsTri = BV::Meshing::Tools::GaussGenerator::getTriNumberOfPoints(
                ruleNumber) ;
            nTable = static_cast<unsigned int>(T_.rows()) * nPtsTri ;
        }
        if (hasQuads_)
        {
            nTable += static_cast<unsigned int>(Q_.rows()) * nPtsPerDirection_ * nPtsPerDirection_ ;
        }
        if (nTable < 1)
            throw BV::Tools::Exceptions::BVException(
                "Cannot update gauss points on an empty mesh") ;
        gaussPointsInPanel_.setZero(nTable, 2) ;
        gaussWiWjDetJ_.setZero(nTable) ;
        normalsAtGaussPoints_.setZero(nTable, 6) ;
        gaussPoints_.setZero(nTable, 3) ;
        unsigned int index(0) ;
        if (hasTriangles_)
            index = refreshTrianglesGaussPoints_(nPtsTri, ruleNumber) ;
        if (hasQuads_) refreshQuadsGaussPoints_(index) ;

        // setup generalized normals
        Eigen::MatrixXd normalGen ;
        Eigen::MatrixXd normal3 = normalsAtGaussPoints_.block(0, 0, nTable, 3) ;
        Eigen::MatrixX3d gpoffset = Eigen::MatrixX3d::Zero(gaussPoints_.rows(), 3);
        for (unsigned int iPt = 0; iPt < gaussPoints_.rows(); ++iPt)  // Maybe there is a Eigen operation to do that directly (as in numpy).
        {
            gpoffset.row(iPt) = gaussPoints_.row(iPt) - refPoint_.transpose();
        }
        igl::cross(gpoffset, normal3, normalGen) ;
        normalsAtGaussPoints_.block(0, 3, nTable, 3) = normalGen;
    }
}

const Eigen::MatrixX2d & Mesh::getGaussPointsInPanels(void) const
{
    return gaussPointsInPanel_ ;
}

Eigen::VectorXd Mesh::getDistanceToPlaneAtGaussPoints(
    const BV::Geometry::Point & pt,
    const BV::Geometry::Vector & direction) const
{
    return getOrthoDistanceToPlane_(gaussPoints_, pt, direction) ;
}

Eigen::MatrixX3d Mesh::getGaussPointsProjectionsInPlane(
    const BV::Geometry::Point & pt,
    const BV::Geometry::Vector & direction) const
{
    const Eigen::MatrixXd::Index n(gaussPoints_.rows()) ;
    Eigen::MatrixXd D(n, 3) ;
    D.col(0).setConstant(direction.x()) ;
    D.col(1).setConstant(direction.y()) ;
    D.col(2).setConstant(direction.z()) ;
    Eigen::MatrixXd AB(n, 3) ;
    AB.col(0).setConstant(-pt.x()) ;
    AB.col(1).setConstant(-pt.y()) ;
    AB.col(2).setConstant(-pt.z()) ;
    AB += gaussPoints_ ;
    Eigen::VectorXd dot = igl::dot_row(AB, D) ;
    for (unsigned int i = 0; i < n; ++i)
    {
        D.row(i) *= dot(i) ;
    }
    D *= -1. ;
    D += gaussPoints_ ;
    return D ;
}


MatrixX4u BV::Meshing::Mesh::getAllPanels(void) const
{
    auto n = this->getNPanels();
    MatrixX4u allPanels( n,4 );
    allPanels.block(0, 0, Q_.rows(), 4) = Q_ ;
    allPanels.block(Q_.rows(), 0, T_.rows(), 3) = T_;
    allPanels.block(Q_.rows(), 3, T_.rows(), 1) = T_.block(0,2,T_.rows(),1);
    return allPanels;
}




const Eigen::VectorXd & Mesh::getGaussWiWjdetJ(void) const
{
    return gaussWiWjDetJ_ ;
}

void Mesh::setRefPoint(Eigen::Vector3d refPoint)
{
    //throw std::runtime_error( "Not implemented yet");  // Modification to do in refreshGaussPoints
    refPoint_ = refPoint;
    refreshGaussPoints(nPtsPerDirection_);
}

double BV::Meshing::Mesh::integrate_volume(int direction, int nbGP) const
{
    refreshGaussPoints(nbGP);
    auto gp = getGaussPoints().array();
    auto normals = getNormalsAtGaussPoints().array();
    auto weight = getGaussWiWjdetJ().array();
    double res = (gp.col(direction) * normals.col(direction) * weight).sum();
    if (sym_ == SymmetryTypes::NONE)
    {
        return res;
    }
    else if (sym_ == SymmetryTypes::XZ_PLANE)
    {
        return res * 2.;
    }
    else if (sym_ == SymmetryTypes::XZ_YZ_PLANES)
    {
        return res * 4.;
    }
    else
    {
        throw std::runtime_error("Integrate volume, problem with symmetry");
    }
}

Eigen::Matrix3d BV::Meshing::Mesh::integrate_vol_inertia(int nbGP) const
{
    if (sym_ != SymmetryTypes::NONE) 
    {
        BV::Meshing::Mesh tmp = Mesh(*this);  // Shallow copy because no reference/pointer in Mesh
        tmp.toSymmetry(SymmetryTypes::NONE);
        return tmp.integrate_vol_inertia(nbGP);
    }
    auto gp = getGaussPoints().array();
    auto normals = getNormalsAtGaussPoints().array();
    auto weight = getGaussWiWjdetJ().array();
    /*
     * Gausse theorem:
     * int_V div vec{a}  dv = closed_int_dV vec{a} vec{n} ds
     * int_V xy dv : vec{a} = (0, 0, xyz) => xyz n_z
     * int_V yz dv : vec{a} = (xyz, 0, 0) => xyz n_x
     * int_V zx dv : vec{a} = (0, xyz, 0) => xyz n_y
     * int_V xx dv : vec{a} = (0, 0, xxz) => xxz n_z
     * int_V yy dv : vec{a} = (0, 0, yyz) => yyz n_z
     * int_V zz dv : vec{a} = (xzz/2, yzz/2, 0) => (x n_x +y n_y) zz/2  ((xzz,0,0), (0,yzz,0), (0,0,zzz/3) are also posible)
     */
    auto vol = integrate_volume(2, nbGP);
    auto cob = integrate_cob(nbGP);
    auto xyz = gp.col(0) *gp.col(1) *gp.col(2);
    double xy = (xyz * normals.col(2) * weight).sum() -cob[0]*cob[1]*vol;
    double yz = (xyz * normals.col(0) * weight).sum() -cob[1]*cob[2]*vol;
    double xz = (xyz * normals.col(1) * weight).sum() -cob[0]*cob[2]*vol;
    double xx = (gp.col(0) * gp.col(0) *gp.col(2) * normals.col(2) * weight).sum() -cob[0]*cob[0]*vol;
    double yy = (gp.col(1) * gp.col(1) *gp.col(2) * normals.col(2) * weight).sum() -cob[1]*cob[1]*vol;
    double zz = 0.5*((gp.col(0) *normals.col(0) +gp.col(1) *normals.col(1)) * gp.col(2) *gp.col(2) *weight).sum() -cob[2]*cob[2]*vol;
    return (Eigen::Matrix3d() << yy+zz, -xy, -xz, -xy, xx+zz, -yz, -xz, -yz, xx+yy).finished();
}

Eigen::Vector3d BV::Meshing::Mesh::integrate_cob(int nbGP) const
{
    if (sym_ != SymmetryTypes::NONE) 
    {
        //throw std::runtime_error("Integrate CoB only on full mesh for now");
        BV::Meshing::Mesh tmp = Mesh(*this);  // Shallow copy because no reference/pointer in Mesh
        tmp.toSymmetry(SymmetryTypes::NONE);
        return tmp.integrate_cob(nbGP);
    }

    auto volume = integrate_volume(2, nbGP);
    auto gp = getGaussPoints().array();
    auto normals = getNormalsAtGaussPoints().array();
    auto weight = getGaussWiWjdetJ().array();
    double x = (gp.col(2) * normals.col(2) * gp.col(0) * weight).sum();
    double y = (gp.col(2) * normals.col(2) * gp.col(1) * weight).sum();
    double z = (0.5*(gp.col(0) * normals.col(0) + gp.col(1) * normals.col(1)) * gp.col(2) * weight).sum();
    return Eigen::Vector3d({x,y,z}).array() / volume;
}

Eigen::ArrayXXd BV::Meshing::Mesh::integrate_fields(const Eigen::ArrayXi & cols)
{
    if (cols.size() > panelsData_.cols())
        throw BV::Tools::Exceptions::BVException("Number of columns is greater than number of panels data");
    if (sym_ != SymmetryTypes::NONE) 
    {
        BV::Meshing::Mesh tmp = Mesh(*this);  // Shallow copy because no reference/pointer in Mesh
        tmp.toSymmetry(SymmetryTypes::NONE);
        return tmp.integrate_fields(cols);
    }
    // Only 1 Gauss point per panel because I do not know how to determine a panel by Gauss Point number
    refreshGaussPoints(1);
    //auto gp = getGaussPoints().array();
    auto normals = getNormalsAtGaussPoints().array();
    auto weight = getGaussWiWjdetJ().array();
    Eigen::ArrayXi cols_tmp(cols);
    size_t nCols(cols.size() > 0 ? cols.size() : panelsData_.cols());
    Eigen::ArrayXXd res(Eigen::ArrayXXd::Zero(6, nCols));
    if (cols.size() == 0)
    {
        cols_tmp.setLinSpaced(nCols, 0, nCols-1);
    }
    for (size_t i = 0; i < nCols; i++)
    {
        if (cols_tmp[i] < panelsData_.cols())
        {
            for (int k = 0; k < 6; k++)
                res(k, i) = (panelsData_.col(cols_tmp[i]) *normals.col(k) *weight).sum();
        }
    }
    return res;
}

Eigen::ArrayX2i BV::Meshing::Mesh::getSegments()
{
    Eigen::ArrayX2i segments( Q_.rows()*4 + T_.rows()*3, 2 );

    unsigned int iSeg = 0;
    for (unsigned int iPanel = 0; iPanel < Q_.rows(); ++iPanel)
    {
        segments(iSeg, 0) = Q_(iPanel, 0);
        segments(iSeg,1) = Q_(iPanel, 1);
        iSeg += 1;
        segments(iSeg, 0) = Q_(iPanel, 1);
        segments(iSeg, 1) = Q_(iPanel, 2);
        iSeg += 1;
        segments(iSeg, 0) = Q_(iPanel, 2);
        segments(iSeg, 1) = Q_(iPanel, 3);
        iSeg += 1;
        segments(iSeg, 0) = Q_(iPanel, 3);
        segments(iSeg, 1) = Q_(iPanel, 0);
        iSeg += 1;
    }

    for (unsigned int iPanel = 0; iPanel < T_.rows(); ++iPanel)
    {
        segments(iSeg, 0) = T_(iPanel, 0);
        segments(iSeg, 1) = T_(iPanel, 1);
        iSeg += 1;
        segments(iSeg, 0) = T_(iPanel, 1);
        segments(iSeg, 1) = T_(iPanel, 2);
        iSeg += 1;
        segments(iSeg, 0) = T_(iPanel, 2);
        segments(iSeg, 1) = T_(iPanel, 0);
        iSeg += 1;
    }

    return segments;
}

