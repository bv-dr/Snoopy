#pragma once

#include "MeshingExport.hpp"

#include "Meshing/Mesh.hpp"

#include "spdlog/spdlog.h"

namespace BV {
namespace Meshing {

class MESHING_API HydroStarMesh
{
    unsigned int nPtsPerDirection_ ;
    std::vector<Mesh> underWaterHullMeshes_ ;
    std::vector<Mesh> aboveWaterHullMeshes_ ;
    std::vector<Mesh> plateMeshes_ = {}; // thin plates
    std::vector<Mesh> swatMeshes_ = {};
    std::vector<Mesh> fsMeshes_ = {};
    std::vector<Mesh> csfMeshes_ = {};
    std::vector<Mesh> tankMeshes_ = {};
    const Mesh & get_(const std::vector<Mesh> & meshes,
                      const unsigned int & index) const ;

public:
    HydroStarMesh(const std::string & filename,
                  const unsigned int & nPtsPerDirection = 1, bool keepSym = false) ;

    HydroStarMesh(std::vector<Mesh> underWaterHullMeshes,
                  std::vector<Mesh> aboveWaterHullMeshes = {},
                  std::vector<Mesh> plateMeshes = {},
                  std::vector<Mesh> fsMeshes = {},
                  std::vector<Mesh> tankMeshes = {}
                 );

    /*!
     * \brief Appends meshes
     */
    void append(const std::string & filename, bool keepSym = false) ;

    void write(const std::string & filename) const ;

    void openDeck(int iBody, double normalThreshold = 0.0);

    void offset(Eigen::Array3d coords);

    void removeUnplanarQuadsToTriangles(const double & tolerance) ;

    int getNbBody();

    int getNbFs();

    int getNbTank();

    void addTank( const Mesh & mesh );

    void addFreeSurface(const Mesh& mesh);
    
    const Mesh & getUnderWaterHullMesh(const unsigned & iBody) const ;
    const Mesh & getAboveWaterHullMesh(const unsigned & iBody) const ;
    Mesh getHullMesh(const unsigned & iBody) const ;
    const Mesh & getSwatMesh(const unsigned & iBody) const ;
    const Mesh & getFsMesh(const unsigned & iFs) const;
    const Mesh & getCsfMesh(const unsigned & iBody) const ;
    const Mesh & getTankMesh(const unsigned & iTank) const ;
    const Mesh & getPlateMesh(const unsigned & iBody) const;

    Mesh getMesh() const;

} ;
}
}
