#pragma once

#include "MeshingExport.hpp"

#include <Eigen/Dense>

#include "Tools/EigenTypedefs.hpp"
#include "Geometry/Point.hpp"
#include "Geometry/Vector.hpp"
#include "Meshing/GaussGenerator.hpp"

#include "spdlog/spdlog.h"

namespace BV {
namespace Meshing {

enum MESHING_API SymmetryTypes
{
    NONE, XY_PLANE, XZ_PLANE, YZ_PLANE,
    XY_XZ_PLANES, XY_YZ_PLANES, XZ_YZ_PLANES,
    XY_XZ_YZ_PLANES
} ;

enum MESHING_API GaussPointsMethods
{
    STANDARD,
    HYDROSTAR
};

/*
 * PanelMetaData structure
 *
 * I do not know yet the best place to put it, so it is here for a while
 */

struct MESHING_API PanelMetaData
{
    /*
     * C-tor
     */
    PanelMetaData(const std::string& dataName = "Undefined",
                  const size_t& dataType = 0,
                  const double& dataFreq = NAN,
                  const double& dataHead = NAN);
    /*
     * D-tor
     */
    ~PanelMetaData() {}

    const std::string& getName() const {return dataName_;}
    const size_t& getType() const {return dataType_;}
    double getFreq() const {return dataFreq_;}
    double getHead() const {return dataHead_;}

    void setName(std::string value) {dataName_ = value;}
    void setType(size_t value) {dataType_ = value;}
    void setFreq(double value) {dataFreq_ = value;}
    void setHead(double value) {dataHead_ = value;}
    private:
        std::string dataName_;
        size_t dataType_;
        double dataFreq_;
        double dataHead_;
};

class MESHING_API Mesh
{

protected:
    Eigen::MatrixX3d V_ ; // vertices
    BV::Tools::MatrixX3u T_ ; // triangles
    BV::Tools::MatrixX4u Q_ ; // quadrangles
    Eigen::MatrixX3d normals_ ; // triangles then quadrangles
    Eigen::VectorXd areas_ ; // triangles then quadrangles
    Eigen::MatrixX3d barycenters_ ; // triangles then quadrangles
    mutable Eigen::MatrixX2d gaussPointsInPanel_ ; // in panel coordinates of gauss points (for quads and tris)
    mutable Eigen::MatrixX3d gaussPoints_ ; // Local coordinates of gauss points (for quads and tris)
    mutable Eigen::VectorXd gaussWiWjDetJ_ ; // Gauss jacobian
    mutable BV::Tools::MatrixX6d normalsAtGaussPoints_ ; // generalized normals
    mutable unsigned int nPtsPerDirection_ ; // number of Gauss points per direction
    Eigen::Vector3d refPoint_ = Eigen::Vector3d::Zero();
    mutable GaussPointsMethods gaussMethod_ = GaussPointsMethods::HYDROSTAR; // method to calculate the Gauss Points

    bool hasTriangles_ ;
    bool hasQuads_ ;

    SymmetryTypes sym_ = SymmetryTypes::NONE;

    // if the hst file is read with HydorStarReader, panelsData will be always presented, so hasPanelsData() == true with panelsData_ filled in with zeros (if there were no any panelsData in the file)
    Eigen::ArrayXXd panelsData_;  // Panels data (Triangles then Quads)
    std::vector<PanelMetaData> panelsMeta_;

    /*!
     * \brief Returns the number of unplanar quads
     * \param[in] tolerance Tolerance for planarity test
     * \return Number of unplanar quads. 0 is no quads.
     */
    unsigned int getUnplanarQuadsNumber_(
        const double & tolerance = 1.e-4) const ;
    /*!
     * \brief Assign indices to remove due to duplicated points
     * \param[in] F T_ or Q_
     * \return indicesToRemove, Vector indices to remove (iRow, jCol)
     */
    std::vector<std::vector<unsigned int> > getDuplicatedVerticePanels_(
        const BV::Tools::MatrixXu & F, const bool & checkAligned,
        const double & tolerance = 1.e-4) const ;
    /*!
     * \brief Clears quads removing false quads ! (aligned points or twice the same index)
     */
    void clearQuads_(double tolerance = 1e-4) ;
    /*!
     * \brief Clears triangles removing flat ones
     */
    void clearTriangles_(double tolerance = 1e-4) ;
    /*!
     * \brief Refreshes normals
     */
    void refreshNormals_(void) ;
    /*!
     * \Refreshes panels areas
     */
    void refreshAreas_(void) ;
    /*!
     * Refreshes barycenters
     */
    void refreshBarycenters_(void) ;
    /*!
     * \brief Removes duplicated vertices
     * \param[in] tolerance Tolerance for vertices comparisons
     */
    void removeDuplicatedVertices_(const double & tolerance = 1.e-6) ;
    /*!
     * \brief Refreshes panels areas and normals
     */
    void refreshAll_(void) ;
    /*!
     * \brief Reads a mesh file based on the name extension.
     * Fills vertices, quads and triangles containers.
     * \param[in] filename Filename with extension (requested for type analysis)
     */
    void read_(const std::string & filename) ;
    /*!
     * \brief Returns a merged container of triangles and quads converted to triangles
     */
    BV::Tools::MatrixX3u getAllPanelsInTriangles_(void) const ;
    /*!
     * \brief Returns the orthogonal (signed) distance between points and a plane defined by one pt and a normal
     *  \param[in] pts Set of input points
     *  \param[in] pt A plane pt
     *  \param[in] normal The plane normal
     */
    Eigen::VectorXd getOrthoDistanceToPlane_(
        const Eigen::MatrixX3d & pts, const BV::Geometry::Point & pt,
        const BV::Geometry::Vector & normal) const ;
    /*!
     * \brief Refreshes Triangles Gauss points
     * \param[in] nPts Number of Gauss points for triangles
     * \param[in] ruleNumber Rule number according to Gauss triangle points generator
     * \return Last traingle index in the global Gauss point table (usefull for quads)
     */
    unsigned int refreshTrianglesGaussPoints_(
        const unsigned int & nPts, const unsigned int & ruleNumber) const ;
    /*!
     * \brief Refreshes Quads Gauss points
     * \param[in] index Starting row index for quads in the global Gauss points matrix
     */
    void refreshQuadsGaussPoints_(unsigned int index) const ;

public:

    /*!
     * \brief Mesh constructor
     * Create an empty mesh
     */
    Mesh();

    /*!
     * \brief Mesh constructor
     * \param[in] V Vertices matrix (nVx3)
     * \param[in] F Panels vertices indices. Can have 3 or 4 columns (triangles or quads)
     * \param[in] symType Symmetry type (see corresponding enum)
     * \param[in] nPtsPerDirection Number of Gauss points per direction for quads. Defines the rule number for triangles
     * If 2, 2x2=4 Gauss points for quads.
     * If 2, Dunavant rule -> 3 for triangles
     */
    Mesh(const Eigen::MatrixX3d & V,
         const BV::Tools::MatrixXu & F, const SymmetryTypes & symType = NONE,
         const unsigned int & nPtsPerDirection = 1,
         bool keepSym = false,
         const Eigen::ArrayXXd & panelsData = Eigen::ArrayXXd(0, 0),
         const std::vector<PanelMetaData> & panelsMeta = {}) ;
    /*!
     * \brief Mesh constructor
     * \param[in] V Vertices matrix (nVx3)
     * \param[in] T Triangles panels vertices indices.
     * \param[in] Q Quads panels vertices indices.
     * \param[in] symType Symmetry type (see corresponding enum)
     * \param[in] nPtsPerDirection Number of Gauss points per direction for quads. Defines the rule number for triangles
     * If 2, 2x2=4 Gauss points for quads.
     * If 2, Dunavant rule -> 3 for triangles
     */
    Mesh(const Eigen::MatrixX3d & V,
         const BV::Tools::MatrixX3u & T, const BV::Tools::MatrixX4u & Q,
         const SymmetryTypes & symType = NONE,
         const unsigned int & nPtsPerDirection = 1,
         bool keepSym = false,
         const Eigen::ArrayXXd & panelsData = Eigen::ArrayXXd(0,0),
         const std::vector<PanelMetaData>& panelsMeta = {}) ;
    /*!
     * \brief Mesh constructor
     * \param[in] filename File name with requested extension in (mesh, stl, obj, off, ply, wrl)
     * \param[in] symType Symmetry type (see corresponding enum)
     * \param[in] nPtsPerDirection Number of Gauss points per direction for quads. Defines the rule number for triangles
     * If 2, 2x2=4 Gauss points for quads.
     * If 2, Dunavant rule -> 3 for triangles
     */
    Mesh(const std::string & filename,
         const SymmetryTypes & symType = NONE,
         const unsigned int & nPtsPerDirection = 1,
         bool keepSym = false) ;


    SymmetryTypes getSym() const
    {
        return sym_;
    }

    void setSym(SymmetryTypes sym)
    {
        sym_ = sym;
    }


    /*!
     * \brief Clean the mesh removing unused nodes
     */
    void removeUnreferencedNodes();

    /*!
     * \brief offset the mesh
     */
    void offset( Eigen::Array3d coord);

    /*!
     * \brief offset the mesh
     */
    void scale(Eigen::Array3d scale);

	/*!
	 * \brief Clean the mesh removing duplicated vertices and cleaning quads and triangles
	 */
	void clean(double tolerance = 1e-10);
    /*!
     * \brief Returns the number of Gauss points per direction for quads and the rule number for triangles
     * \return Number of Gauss points per direction for quads and the rule number for triangles
     */
    unsigned int getNPtsPerDirection(void) const
    {
        return nPtsPerDirection_ ;
    }

    /*!
     *  \brief Returns the total number of panels (triangles and quads)
     *  \return Total number of panels (triangles and quads)
     */
    unsigned int getTotalPanelsNumber(void) const ;

    /*!
     * \brief Check if the mesh is empty
     * \Return Mesh is empty: true, Mesh has at least one panel: false
     */
    bool isEmpty(void) const
    {
        return getTotalPanelsNumber() < 1 ;
    }

    /*!
     * \brief Converts all quads into triangles
     */
    void convertPanelsInTriangles(void) ;

    /*!
     * \brief Remove unplanar quads and replaces them by 2 triangles
     * \param[in] tolerance Tolerance for planar check
     */
    void removeUnplanarQuadsToTriangles(const double & tolerance) ;


    /*!
     * \brief Remove panel oriented upwards (used to remove deck panels)
     * \param[in] tolerance Tolerance for upward direction
     */
    void removeUpwardNormals(const double & tolerance);

    void orientPanels(const int & direction);  

    /*!
     * \brief Writes the mesh into a file. All panels are converted to triangles for compatibility
     * \param[in] filename File name with a requested extension in mesh, stl, obj, off, ply, wrl
     */
    void write(const std::string & filename) const ;

    /*!
     * \brief Prints all details
     * \minimal If false, print all points and panels coordinates
     */
    void printDetails(const bool & minimal = true) const ;

    /*!
     * \brief Symmetries the mesh (apply symmetry given in argument.)
     * \param[in] symTyper Symmetry type (see corresponding enum)
     */
    void symmetrize(const SymmetryTypes & symType) ;

    /*!
     * \brief Convert symmetry type
     */
    void toSymmetry(SymmetryTypes sym);

    /*!
     * \brief Appends a mesh
     * \param[in] mesh Mesh to merge
     */
    void append(const Mesh & mesh) ;

    /*!
     * \brief Panarizes quads moving vertices positions (based of IGL)
     * \param[in] maxIter Maximum numbers of iterations
     * \param[in] threshold Minimum allowed threshold for non-planarity
     */
    void planarizeQuads(const int & maxIter, const double & threshold) ;

    /*!
     * \brief Returns a cutted mesh by a plane.
     * \param[in] pt Plane point
     * \param[in] direction Normal of the plane
     * \Return A mesh
     */
    Mesh getCuttedMesh(const Eigen::Array3d& pt,
                       const Eigen::Array3d& direction) const
    {
        return getCuttedMesh(BV::Geometry::Point(pt[0], pt[1], pt[2]),
                             BV::Geometry::Vector(direction[0], direction[1], direction[2]));
    }

    /*!
     * \brief Returns a cutted mesh by a plane
     * \param[in] pt Plane point
     * \param[in] direction Normal of the plane
     * \Return A mesh
     */
    Mesh getCuttedMesh(const BV::Geometry::Point & pt,
                       const BV::Geometry::Vector & direction) const ;

    /*!
     * \brief Refresh Gauss Points. This method is not in refreshAll to be used only when requested
     * \param[in] n If 0, uses the mesh attribute. Number of Gauss points per direction for quads. Defines the rule number for triangles
     * If 2, 2x2=4 Gauss points for quads.
     * If 2, Dunavant rule -> 3 for triangles
     */
    void refreshGaussPoints(const unsigned int & n = 0) const ;

    /*!
     * \brief Set the method to calculate the Gauss Points.
     * \param[in] method If 0 (default), classical Gauss point definitions are used,
     *                   If 1, HydroStar algorithm
     * NOTE that this function does not calculate the Gauss points
     */
    void setGaussPointsMethod(const GaussPointsMethods & method = GaussPointsMethods::HYDROSTAR) const
    {
        gaussMethod_ = method;
    }

    /*!
     * \brief Get the method to calculate the Gauss Points
     */
    GaussPointsMethods getGaussPointsMethod() const {return gaussMethod_;}

    /*!
     * \brief Returns the Gauss points local in panel coordinates
     * \return Gauss points local in panel coordinates
     */
    const Eigen::MatrixX2d & getGaussPointsInPanels(void) const ;

    /*!
     * \brief Returns Gauss points coordinates in the mesh axis system
     * \return Gauss points coordinates in the mesh axis system
     */
    const Eigen::MatrixX3d & getGaussPoints(void) const
    {
        return gaussPoints_ ;
    }

    /*!
     * \brief Returns generalized normals at Gauss Points
     * \return Generalized normals at Gauss Points
     */
    const BV::Tools::MatrixX6d & getNormalsAtGaussPoints(void) const
    {
        return normalsAtGaussPoints_ ;
    }

    /*!
     * \brief Returns the orthogonal distances to a plane for all Gauss points
     * \param[in] pt Plane point
     * \param[in] direction Normal of the plane
     * \Return A vector of orthogonal distances to a plane for all Gauss points
     */
    Eigen::VectorXd getDistanceToPlaneAtGaussPoints(
        const BV::Geometry::Point & pt,
        const BV::Geometry::Vector & direction) const ;

    /*!
     * \brief Return Gauss point projected on a plane
     * \param[in] pt Plane point
     * \param[in] direction Normal of the plane
     * \Return A Matrix3d of projected points in plane
     */
    Eigen::MatrixX3d getGaussPointsProjectionsInPlane(
        const BV::Geometry::Point & pt,
        const BV::Geometry::Vector & direction) const ;


    /*!
     * \brief Returns all panels as QUADS (TRIs considered as degenerate QUADS)
     */
    BV::Tools::MatrixX4u getAllPanels(void) const;

    /*!
     * \brief Returns a vector with Gauss points weights by jacobian determinant (calculation intermediaite)
     */
    const Eigen::VectorXd & getGaussWiWjdetJ(void) const ;

    /*!
     * \brief Returns vertices
     * \return Vertices
     */
    const Eigen::MatrixX3d & getVertices(void) const
    {
        return V_ ;
    }

    /*!
     * \brief Set vertices (length should be unmodified)
     */
    void setVertices( const Eigen::MatrixX3d & vertices) ;


    /*!
     * \brief Returns triangles
     * \return Triangles
     */
    const BV::Tools::MatrixX3u & getTriangles(void) const
    {
        return T_ ;
    }

    /*!
     * \brief Returns quads
     * \return Quads
     */
    const BV::Tools::MatrixX4u & getQuads(void) const
    {
        return Q_ ;
    }

    /*!
     * \brief Returns total number of panels (triangle + quads)
     * \return Quads
     */
    Eigen::Index getNPanels(void) const
    {
        return Q_.rows() + T_.rows();
    }

    /*!
     * \brief Returns normals for all panels (triangles then quads)
     * \return Normals for all panels (triangles then quads)
     */
    const Eigen::MatrixX3d & getNormals(void) const
    {
        return normals_ ;
    }

    const Eigen::ArrayXXd& getPanelsData() const
    {
        return panelsData_;
    };

    const std::vector<PanelMetaData>& getPanelsMeta() const
    {
        return panelsMeta_;
    };


    bool hasPanelsData() const
    {
        return (panelsData_.cols() > 0);
    };

    /*
     * delete panelsData and panelsMeta.
     */
    void resetPanelsData()
    {
        //panelsData_ = Eigen::ArrayXXd(getTotalPanelsNumber(), 0);
        panelsData_ = Eigen::ArrayXXd(0, 0);
        panelsMeta_.clear();
    }
    void resetPanelsMeta()
    {
        panelsMeta_.clear();
        panelsMeta_.reserve(panelsData_.cols());
        for(size_t i = 0; i < static_cast<size_t>(panelsData_.cols()); i++)
            panelsMeta_.push_back(PanelMetaData("Field_"+std::to_string(i+1))); // 1-based index
    }

    /**
     * /brief set the panelsData_.
     *
     *  NOTE: MetaData by default is erased, use keepMeta = true otherwise
     */
    void setPanelsData(const Eigen::ArrayXXd& panelsData, bool keepMeta = false) 
    {
        if (panelsData.rows() != getTotalPanelsNumber())
            throw BV::Tools::Exceptions::BVException("Number of panel data " +std::to_string(getTotalPanelsNumber()) +" is different from the number of rows in panelsData " +std::to_string(panelsData.rows()) +"!");
        // Force to erase metadata if new and old panelsData have different number of columns
        if (keepMeta && (panelsData_.cols() != panelsData.cols()))
            throw BV::Tools::Exceptions::BVException("keepMeta was set to True, but the old and new panelsData have different number of columns("
                    +std::to_string(panelsData_.cols()) +" vs. " +std::to_string(panelsData.cols()) +")\n");
        panelsData_ = panelsData;
        if (!keepMeta)     // erase meta
            resetPanelsMeta();
    };
    /**
     * /brief set the panelsData_ and panelsMeta_ at the same time.
     *
     * In addition, dataNames, dataTypes, dataFreqs and dataHeads should have as same size as number of columns in the panelsData
     *
     * In order to erase the old panelsMeta use another version: setPanelsData(const Eigen::ArrayXXd&, bool)
     */
    void setPanelsData(const Eigen::ArrayXXd& panelsData,
                       const std::vector<std::string>& dataNames,
                       const Eigen::ArrayXi& dataTypes,
                       const Eigen::ArrayXd& dataFreqs,
                       const Eigen::ArrayXd& dataHeads)
    {
        if (panelsData.rows() != getTotalPanelsNumber())
            throw BV::Tools::Exceptions::BVException("Number of panel data " +std::to_string(getTotalPanelsNumber()) +" is different from the number of rows in panelsData " +std::to_string(panelsData.rows()) +"!");
        panelsData_ = panelsData;
        setPanelsMeta(dataNames, dataTypes, dataFreqs, dataHeads);
    }

    /**
     * /brief set the metadata. This should be used, when the metadata was generated automatically.
     * Note, that the size of the panelsMeta_ must be as same as number of columns in the panelsData_
     */
    void setPanelsMeta(const std::vector<PanelMetaData>& panelsMeta)
    {
        if (static_cast<size_t>(panelsData_.cols()) != panelsMeta.size())
            throw BV::Tools::Exceptions::BVException("setPanelsMeta: panelsMeta size must be the same as number of columns in panelsData.\n  panelsData has " +std::to_string(panelsData_.cols())
                                                   + " columns\n  The metadata has the size " +std::to_string(panelsMeta.size()) +"\n");
        panelsMeta_.clear();
        panelsMeta_.reserve(panelsMeta.size());
        // fill in the panelsMeta_
        for(size_t i = 0; i < panelsMeta.size(); i++)
            panelsMeta_.push_back(panelsMeta[i]);
    }

    /**
     * /brief set the panels metadata by using dataNames, dataTypes, dataFreqs, dataHeads
     *
     * The sizes of all parameters must be equal to the number of columns in panelsData
     */
    void setPanelsMeta(const std::vector<std::string>& dataNames,
                       const Eigen::ArrayXi& dataTypes,
                       const Eigen::ArrayXd& dataFreqs,
                       const Eigen::ArrayXd& dataHeads)
    {
        resetPanelsMeta();
        if ((dataNames.size() != static_cast<size_t>(dataTypes.size())) ||
            (dataNames.size() != static_cast<size_t>(dataFreqs.size())) ||
            (dataNames.size() != static_cast<size_t>(dataHeads.size())))
            throw BV::Tools::Exceptions::BVException("setPanelsMeta must have equivalent sizes of : dataNames (" +std::to_string(dataNames.size())
                    + "), dataTypes (" +std::to_string(dataTypes.size()) +"), dataFreqs(" +std::to_string(dataFreqs.size())
                    + "), dataHeads (" +std::to_string(dataHeads.size()) +"), which is not the case\n");
        if (static_cast<size_t>(panelsData_.cols()) != dataNames.size())
            throw BV::Tools::Exceptions::BVException("setPanelsMeta: the sizes of the dataNames, dataTypes, dataFreqs and dataHeads (" +std::to_string(dataNames.size())
                    + ") is different from the number of columns in panelsData(" +std::to_string(panelsData_.cols()) +")\n");
        setDataNames(dataNames);
        setDataTypes(dataTypes);
        setDataFreqs(dataFreqs);
        setDataHeads(dataHeads);
    }
    /**
     * set panels metadata for a given index (0-based)
     */
    void setPanelsMeta(size_t index, const std::string& name, int type, double freq, double head)
    {
        if (index >= panelsMeta_.size())
            throw BV::Tools::Exceptions::BVException("setPanelsMeta: index(" +std::to_string(index) +" is out of range (0 : " +std::to_string(panelsMeta_.size()-1) +")\n");
        panelsMeta_[index].setName(name);
        panelsMeta_[index].setType(type);
        panelsMeta_[index].setFreq(freq);
        panelsMeta_[index].setHead(head);
    }
    /*
     * PanelsMeta helper functions for python API
     */
    std::vector<std::string> getDataNames() const
    {
        if (panelsMeta_.size() == 0) return std::vector<std::string>();
        //size_t cols(panelsMeat_.size());
        size_t cols(static_cast<size_t>(panelsData_.cols()) < panelsMeta_.size() ? panelsData_.cols() : panelsMeta_.size());
        std::vector<std::string> dataNames(cols);
        for (size_t i = 0; i < cols; i++)
            dataNames[i] = panelsMeta_[i].getName();
        return dataNames;
    }
    Eigen::ArrayXi getDataTypes() const
    {
        if (panelsMeta_.size() == 0) return Eigen::ArrayXi(0);
        //size_t cols(panelsMeat_.size());
        size_t cols(static_cast<size_t>(panelsData_.cols()) < panelsMeta_.size() ? panelsData_.cols() : panelsMeta_.size());
        Eigen::ArrayXi dataTypes(cols);
        for (size_t i = 0; i < cols; i++)
            dataTypes[i] = panelsMeta_[i].getType();
        return dataTypes;
    }
    Eigen::ArrayXd getDataFreqs() const
    {
        if (panelsMeta_.size() == 0) return Eigen::ArrayXd(0);
        //size_t cols(panelsMeat_.size());
        size_t cols(static_cast<size_t>(panelsData_.cols()) < panelsMeta_.size() ? panelsData_.cols() : panelsMeta_.size());
        Eigen::ArrayXd dataFreqs(cols);
        for (size_t i = 0; i < cols; i++)
            dataFreqs[i] = panelsMeta_[i].getFreq();
        return dataFreqs;
    }
    Eigen::ArrayXd getDataHeads() const
    {
        if (panelsMeta_.size() == 0) return Eigen::ArrayXd(0);
        //size_t cols(panelsMeat_.size());
        size_t cols(static_cast<size_t>(panelsData_.cols()) < panelsMeta_.size() ? panelsData_.cols() : panelsMeta_.size());
        Eigen::ArrayXd dataHeads(cols);
        for (size_t i = 0; i < cols; i++)
            dataHeads[i] = panelsMeta_[i].getHead();
        return dataHeads;
    }
    /*
     * setDataNames is the helper function, thus, there is no size check. It is assumed that the sizes are equivalent
     *
     * The sizes are checked only in DEBUG version
     */
    void setDataNames(const std::vector<std::string> values)
    {
        assert(panelsMeta_.size() == values.size());

        // panelsMeta_.size() == values.size()
        // we may set directly in panelsMeta_
        for (size_t i = 0; i < static_cast<size_t>(values.size()); i ++)
            panelsMeta_[i].setName(values[i]);
    }
    /*
     * setDataTypes is the helper function, thus, there is no size check. It is assumed that the sizes are equivalent
     *
     * The size are checked only in DEBUG version
     */
    void setDataTypes(const Eigen::ArrayXi values)
    {
        assert(panelsMeta_.size() == static_cast<size_t>(values.size()));

        // panelsMeta_.size() == values.size()
        // we may set directly in panelsMeta_
        for (size_t i = 0; i < static_cast<size_t>(values.size()); i ++)
            panelsMeta_[i].setType(values[i]);
    }
    /*
     * setDataFreqs is the helper function, thus, there is no size check. It is assumed that the sizes are equivalent
     *
     * The size are checked only in DEBUG version
     */
    void setDataFreqs(const Eigen::ArrayXd values)
    {
        assert(panelsMeta_.size() == static_cast<size_t>(values.size()));

        // panelsMeta_.size() == values.size()
        // we may set directly in panelsMeta_
        for (size_t i = 0; i < static_cast<size_t>(values.size()); i ++)
            panelsMeta_[i].setFreq(values[i]);
    }
    /*
     * setDataHeads is the helper function, thus, there is no size check. It is assumed that the sizes are equivalent
     *
     * The size are checked only in DEBUG version
     */
    void setDataHeads(const Eigen::ArrayXd values)
    {
        assert(panelsMeta_.size() == static_cast<size_t>(values.size()));

        // panelsMeta_.size() == values.size()
        // we may set directly in panelsMeta_
        for (size_t i = 0; i < static_cast<size_t>(values.size()); i ++)
            panelsMeta_[i].setHead(values[i]);
    }
    /*
     * End of PanelsMeta helper functions
     */

    /*!
     * \brief Returns the reference point (used for generalized normals)
     * \return Returns the reference point (used for generalized normals)
     */
    const Eigen::Vector3d & getRefPoint() const
    {
        return refPoint_;
    }

    /*!
     *\brief Set refPoint (=> update generalized normals)
     */
    void setRefPoint( Eigen::Vector3d refPoint );

    /*!
     *\brief Compute and return volume
     */
    double integrate_volume(int direction, int nbGP) const;

    /*!
     *\brief Compute and return the Moments of inertia I_{ij}/rho for a body of constant density rho
     */
    Eigen::Matrix3d integrate_vol_inertia(int nbGP) const;

    /*!
     *\brief Compute and return center of buoyuancy
     */
    Eigen::Vector3d integrate_cob(int nbGP) const;

    /*!
     *\brief Compute the integrals of the panelData
     */
    Eigen::ArrayXXd integrate_fields(const Eigen::ArrayXi & cols);

    /*!
    *\brief Return segments
    */
    Eigen::ArrayX2i getSegments();

};
}
}
