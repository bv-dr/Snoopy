/*
 * GaussConstants.hpp
 *
 *  Created on: 24 nov. 2016
 *      Author: cbrun
 */

#ifndef MESHING_MESHING_GAUSSGENERATOR_HPP_
#define MESHING_MESHING_GAUSSGENERATOR_HPP_

#include "MeshingExport.hpp"

#include <cmath>
#include <vector>

#include "Tools/BVException.hpp"
#include "Math/TriangleDunavantRule.hpp"

namespace BV {
namespace Meshing {
namespace Tools {

class MESHING_API GaussGenerator
{
    // Gauss points for Quads
    double p_[5][5] ;
    double w_[5][5] ;

    // Gauss points for triangles
    std::vector<std::vector<std::vector<double> > > triangles_ ;

    static GaussGenerator gaussGenerator_ ;

    GaussGenerator() ;

public:
    static double getPoint(const int & index, const int & order)
    {
        if (index >= order)
            throw BV::Tools::Exceptions::BVException(
                "Gauss index out of range") ;
        return gaussGenerator_.p_[order - 1][index] ;
    }

    static double getWeight(const int & index, const int & order)
    {
        if (index >= order)
            throw BV::Tools::Exceptions::BVException(
                "Gauss index out of range") ;
        return gaussGenerator_.w_[order - 1][index] ;
    }

    static unsigned int getTriNumberOfPoints(const unsigned int & ruleNumber)
    {
        return BV::Math::dunavant_order_num(ruleNumber) ;
    }

    static const std::vector<double> & getTriPoint(
        const unsigned int & index, const unsigned int & ruleNumber)
    {
        if (ruleNumber > 20)
            throw BV::Tools::Exceptions::BVException(
                "Too high rule for triangle Gauss points") ;
        if (index >= gaussGenerator_.triangles_[ruleNumber - 1].size())
            throw BV::Tools::Exceptions::BVException(
                "Gauss index out of range") ;
        return gaussGenerator_.triangles_[ruleNumber - 1][index] ;
    }
} ;

}
}
}

#endif /* MESHING_MESHING_GAUSSGENERATOR_HPP_ */
