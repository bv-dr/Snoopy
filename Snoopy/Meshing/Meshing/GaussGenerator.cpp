/*
 * GaussGenerator.cpp
 *
 *  Created on: 25 nov. 2016
 *      Author: cbrun
 */

#include "Meshing/GaussGenerator.hpp"
using namespace BV::Meshing::Tools ;
using namespace BV::Math ;

GaussGenerator GaussGenerator::gaussGenerator_ = GaussGenerator() ;

GaussGenerator::GaussGenerator(void)
{
    // For uni-direction
    p_[0][0] = 0. ;
    //
    p_[1][0] = -1. / std::sqrt(3.) ;
    p_[1][1] = 1. / std::sqrt(3.) ;
    //
    p_[2][0] = -std::sqrt(3. / 5.) ;
    p_[2][1] = 0. ;
    p_[2][2] = std::sqrt(3. / 5.) ;
    //
    p_[3][0] = -std::sqrt(3. + 2. * std::sqrt(6. / 7.)) / 7. ;
    p_[3][1] = -std::sqrt(3. - 2. * std::sqrt(6. / 7.)) / 7. ;
    p_[3][2] = std::sqrt(3. - 2. * std::sqrt(6. / 7.)) / 7. ;
    p_[3][3] = std::sqrt(3. + 2. * std::sqrt(6. / 7.)) / 7. ;
    //
    p_[4][0] = -std::sqrt(5. + 2. * std::sqrt(10. / 7.)) / 3. ;
    p_[4][1] = -std::sqrt(5. - 2. * std::sqrt(10. / 7.)) / 3. ;
    p_[4][2] = 0. ;
    p_[4][3] = std::sqrt(5. - 2. * std::sqrt(10. / 7.)) / 3. ;
    p_[4][4] = std::sqrt(5. + 2. * std::sqrt(10. / 7.)) / 3. ;
    // Gauss weights for uni-direction
    w_[0][0] = 2. ;
    //
    w_[1][0] = 1. ;
    w_[1][1] = w_[1][0] ;
    //
    w_[2][0] = 5. / 9. ;
    w_[2][1] = 8. / 9. ;
    w_[2][2] = w_[2][0] ;
    //
    w_[3][0] = (18. - std::sqrt(30.)) / 36. ;
    w_[3][1] = (18. + std::sqrt(30.)) / 36. ;
    w_[3][2] = w_[3][1] ;
    w_[3][3] = w_[3][0] ;
    //
    w_[4][0] = (322. - 13. * std::sqrt(70.)) / 900. ;
    w_[4][1] = (322. + 13. * std::sqrt(70.)) / 900. ;
    w_[4][2] = 128. / 225. ;
    w_[4][3] = w_[4][1] ;
    w_[4][4] = w_[4][0] ;
    // Fill triangles points

    const int rule_num = dunavant_rule_num() ;
    for (int rule = 1; rule <= rule_num; rule++)
    {
        int order_num = dunavant_order_num(rule) ;

        double * xytab = new double[2 * order_num] ;
        double * wtab = new double[order_num] ;

        dunavant_rule(rule, order_num, xytab, wtab) ;
        std::vector<std::vector<double> > temp ;
        for (int order = 0; order < order_num; order++)
        {
            std::vector<double> temp2 ;
            temp2.push_back(xytab[2 * order]) ;
            temp2.push_back(xytab[2 * order + 1]) ;
            temp2.push_back(wtab[order]) ;
            temp.push_back(temp2) ;
        }
        triangles_.push_back(temp) ;
        delete[] wtab ;
        delete[] xytab ;
    }
}
