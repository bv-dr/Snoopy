#include "Meshing/HydroStarMeshReader.hpp"
#include "Tools/BVException.hpp"
#include <sstream>
#include <cstring>
#include <cctype>
#include <memory>
#include <algorithm> 

#ifdef _MSC_VER
# define strtok_r strtok_s
#endif

using namespace BV::Meshing;

namespace
{
    /**
     * HydroStar mesh file keywords
     */
    const std::string kSymmetryBodyKw("SYMMETRY");
    const std::string kSymmetryTankKw("SYMMTANK");
    const std::string kNumPanelKw("NUMPANEL");
    const std::string kNumFpontKw("NUMFPONT");
    const std::string kNumFplateKw("NUMFPLATE");
    const std::string kNumFswatKw("NUMFSWAT");
    const std::string kNumFsKw("NFREESURFACE"); //propzero in HydroStar
    const std::string kNumFcsfKw("NUMFCSF");
    const std::string kNumTankKw("NUMTANK");
    const std::string kNumFtankKw("NUMFTANK");
    const std::string kCoordinatesKw("COORDINATES");
    const std::string kEndCoordinatesKw("ENDCOORDINATES");
    const std::string kPanelKw("PANEL");
    const std::string kPanelsKw("PANELS");
    const std::string kEndPanelKw("ENDPANEL");
    const std::string kEndPanelsKw("ENDPANELS");
    const std::string kEndFileKw("ENDFILE");
    const std::string kNbTank("NBTANK");
    const std::string kRefPtank ("REFPTANK");
    const std::string kZfs("ZFSURFACE");

    /**
     * Friendly definitions to wrap a FILE pointer in a unique_ptr.
     */
    struct FileDeleter
    {
        void operator()(FILE* f) const
        {
            fclose(f);
        }
    };

    using file_ptr = std::unique_ptr<FILE, FileDeleter>;

    file_ptr make_file(const char* filePath, const char* flags)
    {
        return file_ptr(std::fopen(filePath, flags));
    }
}

HydroStarMeshReader::HydroStarMeshReader(const std::string& filePath)
{
    read(filePath);
}

void HydroStarMeshReader::read(const std::string& filePath)
{
    // Open file and check for error
    file_ptr file = make_file(filePath.c_str(), "r");
    if (file == nullptr)
    {
        throw BV::Tools::Exceptions::BVException("File cannot be opened :" + filePath);
    }

#ifndef HSTAR_LINE_MAX
#  define HSTAR_LINE_MAX 2048
#endif

    char line[HSTAR_LINE_MAX];
    unsigned int lineNum(0);
    bool readingVertices(false);
    bool readingFaces(false);
    unsigned int nCoords(4);
    unsigned int iV(1); // first index is 1
    unsigned int node0(0);
    double trans[2]{ 0.0, 0.0 };
    offset_ = 0;
    const char* delims = " \t\r\n";
    double zfs_ = 0.;

    while (fgets(line, HSTAR_LINE_MAX, file.get()) != nullptr)
    {
        ++lineNum;
        // Read first word containing type
        if (line[0] == '\n')
        {
            // Empty line
            continue;
        }

        char type[HSTAR_LINE_MAX];
        sscanf(line, "%s", type);

        if (type == kCoordinatesKw)
        {
            spdlog::debug("HydroStarMeshReader::read. Start reading coordinates\n");
            readingVertices = true;
            // Read parameters
            char* l = &line[strlen(type)] + 1;
            char* next = nullptr;
            char* param = strtok_r(l, delims, &next);
            while (param != nullptr)
            {
                if (strcmp(param, "TYPE") == 0)
                {
                    char* token = strtok_r(nullptr, delims, &next);
                    int val = std::stoi(token);
                    nCoords = val != 0 ? 3 : 4;
                }
                else if (strcmp(param, "TRANS") == 0)
                {
                    char* token = strtok_r(nullptr, delims, &next);
                    trans[0] = std::stod(token);
                    token = strtok_r(nullptr, delims, &next);
                    trans[1] = std::stod(token);
                }
                else if (strcmp(param, "NODE0") == 0)
                {
                    char* token = strtok_r(nullptr, delims, &next);
                    node0 = std::stoul(token);
                }
                else
                {
                    throw BV::Tools::Exceptions::BVException("Unknown parameter " + std::string(param) + " at line " + std::to_string(lineNum));
                }

                param = strtok_r(nullptr, delims, &next);
            }
        }

        else if (type == kZfs)
        {
            char* l = &line[strlen(type)] + 1;
            std::istringstream iss(l);
            iss >> zfs_;
        }
        else if (type == kEndCoordinatesKw)
        {
            readingVertices = false;
        }
        else if (type == kPanelKw || type == kPanelsKw)
        {
            spdlog::debug("HydroStarMeshReader::read. Start reading panels\n");
            readingFaces = true;
            // Read parameters
            char* l = &line[strlen(type)] + 1;
            char* next = nullptr;
            char* param = strtok_r(l, delims, &next);
            while (param != nullptr)
            {
                if (strcmp(param, "TYPE") == 0)
                {
                    char* token = strtok_r(nullptr, delims, &next);
                    int val = std::stoi(token);
                    offset_ = val != 0 ? 1 : 0;
                }
                else if (strcmp(param, "NODE0") == 0)
                {
                    char* token = strtok_r(nullptr, delims, &next);
                    node0 = std::stoul(token);
                }
                else
                {
                   throw BV::Tools::Exceptions::BVException("Unknown parameter " + std::string(param) + " at line " + std::to_string(lineNum));
                }

                param = strtok_r(nullptr, delims, &next);
            }
        }
        else if (type == kEndPanelKw || type == kEndPanelsKw)
        {
            readingFaces = false;
        }
        else if (type == kEndFileKw)
        {
            spdlog::debug("HydroStarMeshReader::read. Found end of the file");
            break;
        }
        else if (std::string(type).find(kSymmetryBodyKw) == 0)  // if type starts from SYMMETRY
        {
            spdlog::debug("HydroStarMeshReader::read. Found body symmetry keyword: {}", std::string(type));
            char* l = &line[strlen(type)] + 1;
            unsigned int x[2];
            int count = sscanf(l, "%u %u\n", &x[0], &x[1]);
            if (count != 2)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kSymmetryBodyKw);
            }
            getBodyHeader(x[0] - 1).symmetry = x[1];
            spdlog::debug("Body {:d} symmetry = {:d}", x[0], getBodyHeader(x[0] -1).symmetry);
        }
        else if (type == kSymmetryTankKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[2];
            int count = sscanf(l, "%u %u\n", &x[0], &x[1]);
            if (count != 2)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kSymmetryTankKw);
            }
            getTankHeader(x[0] - 1).symmetry = x[1];
        }
        else if (type == kNumPanelKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[3];
            int count = sscanf(l, "%u %u %u\n", &x[0], &x[1], &x[2]);
            if (count != 3)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kNumPanelKw);
            }
            getBodyHeader(x[0] - 1).numPanels = { x[1], x[2] };
        }
        else if (type == kNumFpontKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[3];
            int count = sscanf(l, "%u %u %u\n", &x[0], &x[1], &x[2]);
            if (count != 3)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kNumFpontKw);
            }
            getBodyHeader(x[0] - 1).numPont = { x[1], x[2] };
        }
        else if (type == kNumFplateKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[3];
            int count = sscanf(l, "%u %u %u\n", &x[0], &x[1], &x[2]);
            if (count != 3)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kNumFplateKw);
            }
            getBodyHeader(x[0] - 1).numPlate = { x[1], x[2] };
        }
        else if (type == kNumFswatKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[3];
            int count = sscanf(l, "%u %u %u\n", &x[0], &x[1], &x[2]);
            if (count != 3)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kNumFswatKw);
            }
            getBodyHeader(x[0] - 1).numSwat = { x[1], x[2] };
        }
        else if (type == kNumFsKw)
        {
        char* l = &line[strlen(type)] + 1;
        unsigned int x[3];
        int count = sscanf(l, "%u %u %u\n", &x[0], &x[1], &x[2]);
        if (count != 3)
        {
            throw BV::Tools::Exceptions::BVException("Error reading " + kNumFsKw);
        }
        getBodyHeader(0).numFs = { x[1], x[2] };
        }
        else if (type == kNumFcsfKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[3];
            int count = sscanf(l, "%u %u %u\n", &x[0], &x[1], &x[2]);
            if (count != 3)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kNumFcsfKw);
            }
            getBodyHeader(x[0] - 1).numCsf = { x[1], x[2] };
        }
        else if (type == kNumFtankKw || type == kNumTankKw)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int x[3];
            double zfstank = 0.;
            int count = sscanf(l, "%u %u %u %lf\n", &x[0], &x[1], &x[2], &zfstank);
            if (count < 3)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kNumTankKw);
            }
            getTankHeader(x[0] - 1).zfs = zfstank;
            getTankHeader(x[0] - 1).numPanels = { x[1], x[2] };
        }
        else if (type == kRefPtank)
        {
            char* l = &line[strlen(type)] + 1;
            unsigned int tankID;
            double x[3];
            double rho= -1.;
            int count = sscanf(l, "%u %lf %lf %lf %lf\n", &tankID, &x[0], &x[1], &x[2], &rho);
            if (count < 4)
            {
                throw BV::Tools::Exceptions::BVException("Error reading " + kRefPtank);
            }
            getTankHeader(tankID - 1).rho = rho;
            getTankHeader(tankID - 1).refptank = { x[0], x[1], x[2] };
        }
        else if (readingVertices)
        {
            unsigned int index;
            std::vector<double> coords(3);
            if (nCoords == 3)
            {
                int count = sscanf(line, "%lf %lf %lf\n", &coords[0],
                    &coords[1], &coords[2]);
                if (count != 3) continue;
                index = iV++;
            }
            if (nCoords == 4)
            {
                int count = sscanf(line, "%u %lf %lf %lf\n", &index,
                    &coords[0], &coords[1],
                    &coords[2]);
                if (count != 4) continue;
            }
            else
            {
                continue;
            }

            coords = { coords[0] + trans[0], coords[1] + trans[1], coords[2] };
            meshData_.indices[index + node0] = static_cast<unsigned int>(meshData_.vertices.size());
            meshData_.vertices.push_back(std::move(coords));
        }
        else if (readingFaces)
        {
            unsigned int x[5];
            double scalar;

            unsigned int count = 0;
            if (offset_ == 1) 
            {
                count = sscanf(line, "%u %u %u %u %u %lf", &x[0], &x[1], &x[2], &x[3], &x[4], &scalar);
            }
            else
            {
                count = sscanf(line, "%u %u %u %u %lf", &x[0], &x[1], &x[2], &x[3], &scalar);
            }
           
            if ( count < (unsigned int)offset_ + 3 )
            {
                std::cout << line << std::endl;
                throw std::logic_error("Cannot read panel");
            };

            auto faceSize = std::min(count - offset_, static_cast<unsigned int>(4));
            std::vector<unsigned int> face(faceSize);
            for (unsigned int i = 0; i < faceSize; ++i)
            {
                face[i] = x[i + offset_] + node0;
            }
            meshData_.faces.push_back(std::move(face));
            if (count == (unsigned int)offset_ + 5)
            {
                meshData_.facesData.push_back( scalar );
            }
            else
            {
                meshData_.facesData.push_back(0.);
            }
        }
    }


    for (unsigned int i = 0; i < meshData_.vertices.size()  ; ++i)
    {
        meshData_.vertices[i][2] -= zfs_;
    }

    // There is always at least one body
    if (bodiesHeaders_.size() == 0)
    {
        bodiesHeaders_.resize(1);
    }

    // If a header is empty, consider everything as wetted panels
    for (auto& body : bodiesHeaders_)
    {
        if (body.numPanels[0] == 0 && body.numPanels[1] == 0 &&
            body.numPont[0] == 0 && body.numPont[1] == 0 &&
            body.numPlate[0] == 0 && body.numPlate[1] == 0 &&
            body.numSwat[0] == 0 && body.numSwat[1] == 0 &&
            body.numFs[0] == 0 && body.numFs[1] == 0 &&
            body.numCsf[0] == 0 && body.numCsf[1] == 0)
        {
            body.numPanels[0] = 1;
            body.numPanels[1] = static_cast<unsigned int>(meshData_.faces.size());
        }
    }

}

HydroStarMeshReader::BodyHeader& HydroStarMeshReader::getBodyHeader(size_t index)
{
    if (index >= bodiesHeaders_.size())
    {
        bodiesHeaders_.resize(index + 1);
    }
    return bodiesHeaders_[index];
}

HydroStarMeshReader::TankHeader& HydroStarMeshReader::getTankHeader(size_t index)
{
    if (index >= tanksHeaders_.size())
    {
        tanksHeaders_.resize(index + 1);
    }
    return tanksHeaders_[index];
}
