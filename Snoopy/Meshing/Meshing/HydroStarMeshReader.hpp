#pragma once

#include "MeshingExport.hpp"

#include <array>
#include <map>
#include <vector>
#include <iostream>

#include <spdlog/spdlog.h>

namespace BV {
namespace Meshing {

/**
 * @class HydroStarMeshReader
 *
 * @brief Class used to read a HydroStar mesh file.
 */
class MESHING_API HydroStarMeshReader
{
public:
    struct MESHING_API BodyHeader
    {
        unsigned int symmetry{ 0 };
        std::array<unsigned int, 2> numPanels{{ 0, 0 }};
        std::array<unsigned int, 2> numPont{{ 0, 0 }};
        std::array<unsigned int, 2> numPlate{{ 0, 0 }};
        std::array<unsigned int, 2> numSwat{{ 0, 0 }};
        std::array<unsigned int, 2> numFs{ { 0, 0 } };
        std::array<unsigned int, 2> numCsf{{ 0, 0 }};
    };

    struct MESHING_API TankHeader
    {
        unsigned int symmetry{ 0 };
        std::array<unsigned int, 2> numPanels{{ 0, 0 }};
        double zfs;
        std::array<double, 3> refptank;
        double rho;
    };

    struct MESHING_API MeshData
    {
      std::vector<std::vector<double>> vertices;
      std::map<unsigned int, unsigned int> indices;
      std::vector<std::vector<unsigned int>> faces;
      std::vector<double> facesData;
    };

    HydroStarMeshReader(const std::string& filePath);
    HydroStarMeshReader(const HydroStarMeshReader&) = delete;
    HydroStarMeshReader& operator=(const HydroStarMeshReader&) = delete;

    inline const MeshData& getMeshData() const;

    inline const std::vector<BodyHeader>& getBodyHeaders() const;

    inline const std::vector<TankHeader>& getTankHeaders() const;

    inline unsigned int getOffset() const;

    inline double getCoefZ0() const;

private:

    /**
     * Read the mesh
     */
    void read(const std::string& filePath);

    /**
     * Get a body or tank given its index. If not found, update the list of
     * bodies or tanks accordingly.
     */
    BodyHeader& getBodyHeader(size_t index);
    TankHeader& getTankHeader(size_t index);

    MeshData meshData_;

    std::vector<BodyHeader> bodiesHeaders_;
    std::vector<TankHeader> tanksHeaders_;
    int offset_;
    double coefz0_;
};

const HydroStarMeshReader::MeshData& HydroStarMeshReader::getMeshData() const
{
    return meshData_;
}

const std::vector<HydroStarMeshReader::BodyHeader>& HydroStarMeshReader::getBodyHeaders() const
{
    return bodiesHeaders_;
}

const std::vector<HydroStarMeshReader::TankHeader>& HydroStarMeshReader::getTankHeaders() const
{
    return tanksHeaders_;
}

unsigned int HydroStarMeshReader::getOffset() const
{
    return offset_;
}

double HydroStarMeshReader::getCoefZ0() const
{
    return coefz0_;
}

}
}
