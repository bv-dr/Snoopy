#include "Meshing/HydroStarMesh.hpp"
#include "Meshing/HydroStarMeshReader.hpp"

#include "Tools/BVException.hpp"

#include <algorithm>

#include <igl/list_to_matrix.h>

using namespace BV::Meshing ;

const Mesh & HydroStarMesh::get_(const std::vector<Mesh> & meshes,
                                 const unsigned int & index) const
{
    if (index >= static_cast<unsigned int>(meshes.size()))
    {
        std::cout << "Error: " << index << " " << meshes.size() << std::endl ;
        throw BV::Tools::Exceptions::BVException("Index out of range") ;
    }
    return meshes[index] ;
}

HydroStarMesh::HydroStarMesh(const std::string & filename,
                             const unsigned int & nPtsPerDirection, bool keepSym) :
    nPtsPerDirection_(nPtsPerDirection)
{
    append(filename, keepSym) ;
}


HydroStarMesh::HydroStarMesh(std::vector<Mesh> underWaterHullMeshes,
                             std::vector<Mesh> aboveWaterHullMeshes,
                             std::vector<Mesh> plateMeshes,
                             std::vector<Mesh> fsMeshes,
                             std::vector<Mesh> tankMeshes
                            ):
underWaterHullMeshes_(underWaterHullMeshes),
fsMeshes_(fsMeshes),
tankMeshes_(tankMeshes)
{
    if (underWaterHullMeshes_.size() == 1)
    {
        // if empty list, create empty mesh, only feasible if nbbody == 1
        if (aboveWaterHullMeshes.size() == 1 )
        {
            aboveWaterHullMeshes_ = aboveWaterHullMeshes;
        }
        else
        {
            aboveWaterHullMeshes_ = { Mesh(), };
        }

        if (plateMeshes.size() == 1)
        {
            plateMeshes_ = plateMeshes;
        }
        else
        {
            plateMeshes_ = { Mesh(), };
        }
    }
    else
    {
        if (underWaterHullMeshes.size() != aboveWaterHullMeshes.size()) throw std::logic_error("Wrong number of NUMFPONT meshes");
        if (underWaterHullMeshes.size() != plateMeshes.size()) throw std::logic_error("Wrong number of NUMFPLATE meshes");
        plateMeshes_ = plateMeshes;
        aboveWaterHullMeshes_ = aboveWaterHullMeshes;
    }
}

void HydroStarMesh::append(const std::string & filename, bool keepSym)
{
    spdlog::debug("HydroStarMesh::append. keepSym = {}", (keepSym ? "True" : "False"));
    HydroStarMeshReader meshReader(filename);

    auto& meshData = meshReader.getMeshData();
    auto& vertices = meshData.vertices;
    auto& faces = meshData.faces;
    auto& facesData = meshData.facesData;
    auto& indices = meshData.indices;

    // Fill different meshes
    Eigen::MatrixX3d V ;
    igl::list_to_matrix(vertices, V) ;

    

    auto getMesh =
        [&V, &faces, &facesData, &indices, &keepSym](const std::array<unsigned int, 2>& meshIndices,
            const SymmetryTypes & sym, const unsigned int & nPtsPerDirection)->Mesh
        {
            if (meshIndices[0] == 0 )
            {
                return Mesh(Eigen::MatrixX3d(0, 3), BV::Tools::MatrixX3u(0, 3)) ;
            }


            Eigen::ArrayXXd panelsData(meshIndices[1] - meshIndices[0] + 1, 1);

            std::vector<std::vector<unsigned int> > triangles ;
            std::vector<std::vector<unsigned int> > quads ;
            std::vector<std::vector<unsigned int> > * pF ;
            for (unsigned int iHstar=meshIndices[0] ; iHstar<meshIndices[1]+1 ; iHstar++)
            {
                const std::vector<unsigned int> & face = faces[iHstar-1] ; // Hstar starts at 1
                const size_t n = face.size();
                if (n==3) pF = &triangles ;
                else if (n==4) pF = &quads ;
                else throw BV::Tools::Exceptions::BVException("Error reading panels") ;
                std::vector<unsigned int> temp(n) ;
                for (size_t i=0 ; i<n ; ++i)
                {
                    temp[i] = indices.at(face[i]);
                }
                panelsData(iHstar- meshIndices[0], 0) = facesData[iHstar - 1];
                pF->push_back(temp);
            }
            BV::Tools::MatrixX3u T ;
            BV::Tools::MatrixX4u Q ;
            
            igl::list_to_matrix(triangles, T) ;
            igl::list_to_matrix(quads, Q) ;
            return Mesh(V, T, Q, sym, nPtsPerDirection, keepSym, panelsData) ;
        } ;

    auto bodiesHeaders = meshReader.getBodyHeaders();
    size_t tmp_body_index(0);
    for (auto& body : bodiesHeaders)
    {
        spdlog::debug("HydroStarMesh::append. Body {:d} has symmetry: {:d}", (++tmp_body_index), body.symmetry);
        const SymmetryTypes sym(
            body.symmetry == 1 ? SymmetryTypes::XZ_PLANE :
            body.symmetry == 2 ? SymmetryTypes::XZ_YZ_PLANES : 
            SymmetryTypes::NONE) ;
        underWaterHullMeshes_.push_back( getMesh(body.numPanels, sym, nPtsPerDirection_)) ;
        aboveWaterHullMeshes_.push_back( getMesh(body.numPont, sym, nPtsPerDirection_)) ;
        plateMeshes_.push_back(getMesh(body.numPlate, sym, nPtsPerDirection_));
        swatMeshes_.push_back(getMesh(body.numSwat, sym, nPtsPerDirection_)) ;
        fsMeshes_.push_back(getMesh(body.numFs, sym, nPtsPerDirection_));
        csfMeshes_.push_back(getMesh(body.numCsf, sym, nPtsPerDirection_)) ;
    }

    auto tanksHeaders = meshReader.getTankHeaders();
    for (auto& tank : tanksHeaders)
    {
        const SymmetryTypes sym(
            tank.symmetry == 1 ? SymmetryTypes::XZ_PLANE :
            tank.symmetry == 2 ? SymmetryTypes::XZ_YZ_PLANES : 
            SymmetryTypes::NONE);

        tankMeshes_.push_back(getMesh(tank.numPanels, sym, nPtsPerDirection_));
    }
}

void HydroStarMesh::write(const std::string & filename) const
{
    throw "write option not implemented yet" ; // FIXME
}

void HydroStarMesh::openDeck(int iBody, double normalThreshold)
{
    aboveWaterHullMeshes_[iBody].removeUpwardNormals(normalThreshold);
    return;
}

void HydroStarMesh::offset(Eigen::Array3d coords)
{
    for (Mesh & mesh : underWaterHullMeshes_)
    {
        mesh.offset(coords);
    }
    for (Mesh & mesh : aboveWaterHullMeshes_)
    {
        mesh.offset(coords);
    }
    for (Mesh & mesh : plateMeshes_)
    {
        mesh.offset(coords);
    }

    for (Mesh & mesh : swatMeshes_)
    {
        mesh.offset(coords);
    }
    for (Mesh & mesh : csfMeshes_)
    {
        mesh.offset(coords);
    }
    for (Mesh & mesh : tankMeshes_)
    {
        mesh.offset(coords);
    }
}

void HydroStarMesh::removeUnplanarQuadsToTriangles(const double & tolerance)
{
    for (Mesh & mesh : underWaterHullMeshes_)
    {
        mesh.removeUnplanarQuadsToTriangles(tolerance) ;
    }
    for (Mesh & mesh : aboveWaterHullMeshes_)
    {
        mesh.removeUnplanarQuadsToTriangles(tolerance) ;
    }
    for (Mesh & mesh : plateMeshes_)
    {
        mesh.removeUnplanarQuadsToTriangles(tolerance);
    }

    for (Mesh & mesh : swatMeshes_)
    {
        mesh.removeUnplanarQuadsToTriangles(tolerance) ;
    }
    for (Mesh & mesh : csfMeshes_)
    {
        mesh.removeUnplanarQuadsToTriangles(tolerance) ;
    }
    for (Mesh & mesh : tankMeshes_)
    {
        mesh.removeUnplanarQuadsToTriangles(tolerance) ;
    }
}

int BV::Meshing::HydroStarMesh::getNbBody()
{
    return static_cast<int>(underWaterHullMeshes_.size());
}

int BV::Meshing::HydroStarMesh::getNbFs()
{
    return static_cast<int>(fsMeshes_.size());
}

int BV::Meshing::HydroStarMesh::getNbTank()
{
    return static_cast<int>(tankMeshes_.size());
}

void BV::Meshing::HydroStarMesh::addTank(const Mesh & mesh)
{
    tankMeshes_.push_back(mesh);
}

void BV::Meshing::HydroStarMesh::addFreeSurface(const Mesh& mesh)
{
    // Add free-surface mesh. Not clear if HydroStar Mesh without FS has an empty fs-mesh, or no fs-mesh at all (depends on way to build). 
    // This is until proper fix.
    if (fsMeshes_.size() > 0)
    {
        fsMeshes_[0] = mesh;
    }
    else
    {
        fsMeshes_.push_back(mesh);
    }
    
}

const Mesh & HydroStarMesh::getUnderWaterHullMesh(const unsigned & iBody) const
{
    return get_(underWaterHullMeshes_, iBody) ;
}

const Mesh & HydroStarMesh::getAboveWaterHullMesh(const unsigned & iBody) const
{
    return get_(aboveWaterHullMeshes_, iBody) ;
}

const Mesh & HydroStarMesh::getPlateMesh(const unsigned & iBody) const
{
    return get_(plateMeshes_, iBody);
}

Mesh HydroStarMesh::getHullMesh(const unsigned & iBody) const
{
    Mesh mesh = getUnderWaterHullMesh(iBody);
    mesh.append(getAboveWaterHullMesh(iBody));
    return mesh;
}

const Mesh & HydroStarMesh::getSwatMesh(const unsigned & iBody) const
{
    return get_(swatMeshes_, iBody) ;
}

const Mesh & HydroStarMesh::getFsMesh(const unsigned & iFs) const
{
    return get_(fsMeshes_, iFs);
}

const Mesh & HydroStarMesh::getCsfMesh(const unsigned & iBody) const
{
    return get_(csfMeshes_, iBody) ;
}

const Mesh & HydroStarMesh::getTankMesh(const unsigned & iTank) const
{
    return get_(tankMeshes_, iTank) ;
}

Mesh HydroStarMesh::getMesh() const
{
    Mesh mesh{};

    // TODO: ensure symmetry consistency(when tank are there)
    mesh.setSym( underWaterHullMeshes_[0].getSym() );

    auto appendMeshes = [&mesh](const std::vector<Mesh>& meshes)
    {
        for (auto& m : meshes)
        {
            mesh.append(m);
        }
    };

    appendMeshes(underWaterHullMeshes_);
    appendMeshes(aboveWaterHullMeshes_);
    appendMeshes(plateMeshes_);
    appendMeshes(swatMeshes_);
    appendMeshes(fsMeshes_);
    appendMeshes(csfMeshes_);
    appendMeshes(tankMeshes_);

    return mesh;
}
