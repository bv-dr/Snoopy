#include <pybind11/pybind11.h>

#include "Interface/pyWrapping/Meshing.hpp"

PYBIND11_MODULE(_Meshing, m)
{
	BV::PythonInterface::Meshing::exportModule(m) ;
}
