#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/iostream.h>
#include <pybind11/functional.h>
#include "Interface/pyWrapping/Meshing.hpp"
#include "Meshing/HydroStarMesh.hpp"
#include "Meshing/HydroStarMeshReader.hpp"
#include "Meshing/Mesh.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Meshing;
using namespace BV::Tools;

namespace BV {
namespace PythonInterface {
namespace Meshing {

// Intermediate class to be able to expose protected attribute to Python
class PyMesh : public Mesh
{
public:
    using Mesh::V_;
    using Mesh::T_;
    using Mesh::Q_;
    using Mesh::sym_;
};

void exportModule(py::module & m)
{

    py::add_ostream_redirect(m, "ostream_redirect");

    py::enum_<SymmetryTypes>(m, "SymmetryTypes")
        .value("NONE", SymmetryTypes::NONE)
        .value("XY_PLANE", SymmetryTypes::XY_PLANE)
        .value("XZ_PLANE", SymmetryTypes::XZ_PLANE)
        .value("YZ_PLANE", SymmetryTypes::YZ_PLANE)
        .value("XY_XZ_PLANES", SymmetryTypes::XY_XZ_PLANES)
        .value("XY_YZ_PLANES", SymmetryTypes::XY_YZ_PLANES)
        .value("XZ_YZ_PLANES", SymmetryTypes::XZ_YZ_PLANES)
        .value("XY_XZ_YZ_PLANES", SymmetryTypes::XY_XZ_YZ_PLANES)
    ;

    py::enum_<GaussPointsMethods>(m, "GaussPointsMethods")
        .value("STANDARD", GaussPointsMethods::STANDARD)
        .value("HYDROSTAR", GaussPointsMethods::HYDROSTAR)
    ;

	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    py::class_<Mesh>(m, "Mesh")

        .def(py::init<>(),
            R"rst(
             Empty mesh constructor (shallow)
             )rst")

        .def(py::init<const Mesh&>(),
            R"rst(
             Copy constructor (shallow)
             :param Mesh
             )rst")

        .def(py::init<const Eigen::MatrixXd &,
                      const MatrixXu &,
                      const SymmetryTypes &,
                      const unsigned int &,
                      bool,
                      const Eigen::ArrayXXd &,
                      const std::vector<PanelMetaData> &>(),
             R"rst(
             Initialisation of the mesh using triangles *or* quadrangles.

             :param Vertices: a nV x 3 :py:class:`numpy.ndarray` containing the vertices
             :param Panels: a nP x 3(or 4) :py:class:`numpy.ndarray` of int
                 containing the panels indices. Panels can be triangles
                 (3 coordinates) or quadrangles (4 coordinates)
             :param symType: a :py:class:`~_Meshing.SymmetryTypes` value
                 defining the symmetry planes of the mesh.
             :param int nbGp: number of Gauss points per direction
                 for quadrangles. Define the rule number for triangles.
                 If 2, 2x2=4 Gauss points for quadrangles.
                 If 2, Dunavant rule -> 3 for triangles.
             :param boolean keepSymmetry: keep the mesh symmetry (True) or convert to
                 a mesh without symmetry (False). Default is False
             :param panelsData: 2D numpy.ndarray panels' data. The description of the
                 panelsData columns must be set by using setPanelsMetadata(names, types, freqs, heads).
             )rst", "Vertices"_a,
                    "Panels"_a,
                    "symType"_a,
                    "nbGp"_a = 1,
                    "keepSymmetry"_a = false,
                    "panelsData"_a = Eigen::ArrayXXd(0, 0), 
                     // panelsMeta should not be used directly in Python
                    "panelsMeta"_a = std::vector<PanelMetaData>())

         .def(py::init<const Eigen::MatrixXd &,
                       const MatrixX3u &,
                       const MatrixX4u &,
                       const SymmetryTypes &,
                       const unsigned int &,
                       bool, 
                       const Eigen::ArrayXXd &,
                       const std::vector<PanelMetaData> &>(),
             R"rst(
             Initialisation of the mesh using triangles *and* quadrangles.

             :param Vertices: a nV x 3 :py:class:`numpy.ndarray` containing the vertices
             :param Tris: a nT x 3 :py:class:`numpy.ndarray` of int
                 containing the triangle panels indices.
             :param Quads: a nQ x 4 :py:class:`numpy.ndarray` of int
                 containing the quadrangle panels indices.
             :param symType: a :py:class:`~_Meshing.SymmetryTypes` value
                 defining the symmetry planes of the mesh.
             :param int nbGp: number of Gauss points per direction
                 for quadrangles. Define the rule number for triangles.
                 If 2, 2x2=4 Gauss points for quadrangles.
                 If 2, Dunavant rule -> 3 for triangles.
             :param boolean keepSymmetry: keep the mesh symmetry (True) or convert to
                 a mesh without symmetry (False). Default is False
             :param panelsData: 2D numpy.ndarray panels' data. The description of the
                 panelsData columns must be set by using setPanelsMetadata(names, types, freqs, heads).
             )rst",  "Vertices"_a,
                     "Tris"_a,
                     "Quads"_a,
                     "symType"_a = SymmetryTypes::NONE,
                     "nbGp"_a = 1,
                     "keepSymmetry"_a = false,
                     "panelsData"_a = Eigen::ArrayXXd(0,0),
                     // panelsMeta should not be used directly in Python
                     "panelsMeta"_a = std::vector<PanelMetaData>())

         .def(py::init<const std::string &,
                       const SymmetryTypes &,
                       const unsigned int &,
                       bool>(),
             R"rst(Initialisation of the mesh using a file.

             Parameters
             ----------
             filename : str
                The file name in which to read the mesh.
                Required extension among: mesh, stl, obj, off, ply, wrl.
             symType : _Meshing.SymmetryTypes
                A value defining the symmetry planes of the mesh.
             nbGp : int
                Number of Gauss points per direction for quadrangles.
                Define the rule number for triangles.
                If 2, 2x2=4 Gauss points for quadrangles.
                If 2, Dunavant rule -> 3 for triangles.
             keepSymmetry : bool, optional
                Keep the mesh symmetry (True) or convert to a mesh without symmetry (False).
                The default is False.
             )rst",  "filename"_a, "symType"_a = SymmetryTypes::NONE, "nbGp"_a = 1, "keepSymmetry"_a = false)

         .def("append", &Mesh::append, "Append mesh to the current one", "mesh"_a)

         .def("removeUnreferencedNodes", &Mesh::removeUnreferencedNodes, "Remove unused nodes")

         .def("orientPanels", &Mesh::orientPanels, "direction"_a = 1, 
               R"rst("Orient panels upwards, In-place.

               Parameters
               ----------
               orientPanels : int
                   If -1 orient downwards, if +1 orient upwards
                )rst")

         .def("getVertices", &Mesh::getVertices,
              R"rst(
              Return the vertices of the mesh.

              :return: a nV x 3 :py:class:`numpy.ndarray` containing the
                  vertices.
              )rst")

        .def("getNPanels", &Mesh::getNPanels,
                     R"rst(
              Return total number of panels (TRIS + QUADS)
              )rst")

        .def("getPanelsData", &Mesh::getPanelsData,
            R"rst(
              Return panels data
              )rst")

        .def("setPanelsData", py::overload_cast<const Eigen::ArrayXXd&, bool>(&Mesh::setPanelsData),
            "panelsData"_a,
            "keepMeta"_a = false,
            R"rst(
              Replace the panels data by new values.

              NOTES: The metadata is erased by default (keepMeta == False)

              :param panelsData : 2D numpy.ndarray, which contains the panels data to be set
              :param keepMeta : boolean, by default is False.
                if keepMeta is True, the method allows only to replace the data, otherwise
                the metadata are erased and the default Field_N (1-based index) are created
              )rst")
        .def("setPanelsData", py::overload_cast<const Eigen::ArrayXXd&,
                const std::vector<std::string>&,
                const Eigen::ArrayXi&,
                const Eigen::ArrayXd&,
                const Eigen::ArrayXd&>(&Mesh::setPanelsData),
            "panelsData"_a,
            "dataNames"_a,
            "dataTypes"_a,
            "dataFreqs"_a,
            "dataHeads"_a,
            R"rst(
              Replace the panels data and metadata

              NOTES: The number of columns of panelsData and sizes of dataNames, dataTypes,
                     dataFreqs and dataHeads MUST be the same

              :param panelsData : 2D numpy.ndarray, which contains the panels data to be set
              :param dataNames  : list of names for each panel's data column
              :param dataTypes  : 1D numpy.ndarray of types for each panel's data column
              :param dataFreqs  : 1D numpy.ndarray of frequencies for each panel's data column
              :param dataHeads  : 1D numpy.ndarray of headings for each panel's data column
              )rst")

        .def("getDataNames", &Mesh::getDataNames,
            R"rst(
              Return panels data column names
              )rst")

        .def("getDataTypes", &Mesh::getDataTypes,
            R"rst(
              Return panels data column types
              )rst")

        .def("getDataFreqs", &Mesh::getDataFreqs,
            R"rst(
              Return panels data column frequencies
              )rst")

        .def("getDataHeads", &Mesh::getDataHeads,
            R"rst(
              Return panels data column headings
              )rst")

        .def("setDataNames", &Mesh::setDataNames,
            R"rst(
              Set panels data column names
              )rst")

        .def("setDataTypes", &Mesh::setDataTypes,
            R"rst(
              Set panels data column types
              )rst")

        .def("setDataFreqs", &Mesh::setDataFreqs,
            R"rst(
              Set panels data column frequencies
              )rst")

        .def("setDataHeads", &Mesh::setDataHeads,
            R"rst(
              Set panels data column headings
              )rst")

        .def("setPanelsMetadata", py::overload_cast<const std::vector<std::string>&,
                                                    const Eigen::ArrayXi&,
                                                    const Eigen::ArrayXd&,
                                                    const Eigen::ArrayXd&>(&Mesh::setPanelsMeta),
            "dataNames"_a = std::vector<std::string>(),
            "dataTypes"_a = Eigen::ArrayXi(0),
            "dataFreqs"_a = Eigen::ArrayXd(0),
            "dataHeads"_a = Eigen::ArrayXd(0),
            R"rst(
              Set panels data metadata

             :param dataNames: list of names for data columns
             :param dataTypes: numpy.ndarray of int containing the data types
             :param dataFreqs: numpy.ndarray of float containing the frequencies
             :param dataHeads: numpy.ndarray of float containing the headings
              )rst")

        .def("setPanelsMetadata", py::overload_cast<size_t,
                                                    const std::string&,
                                                    int,
                                                    double,
                                                    double>(&Mesh::setPanelsMeta),
            "index"_a, "dataName"_a, "dataType"_a, "dataFreq"_a, "dataHead"_a,
            R"rst(
              Set index's panel data metadata

             :param index: int, index of the column (0-based) the metadata to be changed
             :param dataName: list of names for data columns
             :param dataType: int, type
             :param dataFreq: double, frequency
             :param dataHead: double, heading
              )rst")

        .def("hasPanelsData", &Mesh::hasPanelsData,
            R"rst(
              Return True if panels data are present
              )rst")

        .def("resetPanelsData", &Mesh::resetPanelsData,
            R"rst(
              reset panels data
              )rst")
        .def("resetPanelsMetaData", &Mesh::resetPanelsMeta,
            R"rst(
              reset panels metadata by setting name = Field_n, for n = 1,2,3,...,N
              )rst")

         .def("clean", &Mesh::clean,
              R"rst(
              Clean the mesh (remove duplicated nodes and fake quads)
              )rst")


         .def("setVertices", &Mesh::setVertices,
              R"rst(
              Set the vertices of the mesh.

              Number of vertices should remain the same.
              )rst")


         .def("getTriangles", &Mesh::getTriangles,
              R"rst(
              Return the triangles of the mesh.

              :return: a nT x 3 :py:class:`numpy.ndarray` containing the
                  triangle panels.
              )rst")

         .def("getAllPanels", &Mesh::getAllPanels,
              R"rst(
              Return the all panels, quads, and tris as denegerated quads.

              :return: a nT x 4 :py:class:`numpy.ndarray` containing the
                  triangle panels.
              )rst")


         .def("getQuads", &Mesh::getQuads,
              R"rst(
              Return the quadrangles of the mesh.

              :return: a nT x 4 :py:class:`numpy.ndarray` containing the
                  quadrangle panels.
              )rst")
         .def("getNormals", &Mesh::getNormals,
              R"rst(
              Return the normals for all panels (first triangles then quads)

              :return: a nP x 3 :py:class:`numpy.ndarray` containing all the
                  normals of the mesh (first triangles then quadrangles).
              )rst")
         .def("getNormalsAtGaussPoints", &Mesh::getNormalsAtGaussPoints,
              R"rst(
              Return the generalised normals at Gauss points.

              :return: a n x 6 :py:class:`numpy.ndarray` containing
                  the generalised normals at Gauss points.
              )rst")
         .def("getGaussPoints", &Mesh::getGaussPoints,
              R"rst(
              Return the Gauss points coordinates in the mesh axis system.

              :return: a n x 3 :py:class:`numpy.ndarray` containing the
                  Gauss points coordinates in the mesh axis system.
              )rst")

         .def("getGaussWiWjdetJ" , &Mesh::getGaussWiWjdetJ,
              R"rst(
              Return the Gauss points weight.
              :return: a :py:class:`numpy.ndarray` containing the Gauss weight
              )rst")

         .def("getGaussPointsProjectionsInPlane", &Mesh::getGaussPointsProjectionsInPlane,
              R"rst(
              Return the Gauss points coordinates projected on a plane.

              :param pt: a :py:class:`~Geometry.Point` lying in the plane.
              :param direction: a :py:class:`~Geometry.Vector` the normal to
                  the plane.
              :return: a n x 3 :py:class:`numpy.ndarray` containing the
                  Gauss points coordinates projected on the provided plane.
              )rst")
         .def("getCutMesh", py::overload_cast<const Eigen::Array3d&, const Eigen::Array3d&>(&Mesh::getCuttedMesh, py::const_),
              "pt"_a,
              "direction"_a,
              R"rst(
              Return a new mesh which is a cut portion of current one.
              The cut is performed by a plane.

              :param pt: a numpy.ndarray of size 3 point lying in the plane
              :param direction: a numpy.ndarray of size 3 normal to the
                  plane, pointing to the portion of the mesh that is **not**
                  returned.
              :return: a :py:class:`~_Meshing.Mesh` the cut portion of current
                  mesh which is not pointed by the normal: the normal points
                  to the portion that is *not* returned.
              )rst")
         .def("getCutMesh", py::overload_cast<const BV::Geometry::Point&, const BV::Geometry::Vector&>(&Mesh::getCuttedMesh, py::const_),
              "pt"_a,
              "direction"_a,
              R"rst(
              Return a new mesh which is a cut portion of current one.
              The cut is performed by a plane.

              :param pt: a :py:class:`~Geometry.Point` lying in the plane.
              :param direction: a :py:class:`~Geometry.Vector` normal to the
                  plane, pointing to the portion of the mesh that is **not**
                  returned.
              :return: a :py:class:`~_Meshing.Mesh` the cut portion of current
                  mesh which is not pointed by the normal: the normal points
                  to the portion that is *not* returned.
              )rst")
         .def("write", &Mesh::write,
              R"rst(
              Write the mesh into a file.

              Note that all panels are converted to triangles for compatibility
              with `IGL <http://libigl.github.io/libigl/>`_ library.

              :param string fileName: the name of the output file with a required
                  extension among: mesh, stl, obj, off, ply, wrl
              )rst")
         .def("printDetails", &Mesh::printDetails,
              R"rst(
              Print useful details on the mesh: number of vertices, triangles,
              quadrangles, panels, bounding box coordinates...

              :param bool minimal: if False, all points and panels coordinates is
                  printed. Otherwise, only a summary is printed.
              )rst")
         .def("planarizeQuads", &Mesh::planarizeQuads,
              R"rst(
              Make all quadrangles planar moving vertices positions (based on
              `IGL <http://libigl.github.io/libigl/>`_ library)

              :param int maxIter: maximum number of iterations in the process.
              :param float threshold: minimum allowed threshold for non-planarity.
              )rst")
                  

         .def("toSymmetry", &Mesh::toSymmetry,
                 R"rst(
              Convert symmetry ()
              )rst")

         .def("removeUnplanarQuadsToTriangles", &Mesh::removeUnplanarQuadsToTriangles,
              R"rst(
              Remove unplanar quadrangles and replace them by 2 triangles.

              :param float tolerance: tolerance for planarity check.
              )rst")
         .def("refreshGaussPoints", &Mesh::refreshGaussPoints, "nPerDirection"_a = 1,
              R"rst(
              Refresh Gauss points.

              :param int nPerDirection: If 0, uses the mesh initialisation parameter
                  nPtsPerDirection.

                  Number of Gauss points per direction for quads.

                  Defines the rule number for triangles.

                  If 2, 2x2=4 Gauss points for quads.

                  If 2, Dunavant rule -> 3 for triangles.

                  if gaussMethod is set to 1 (HydroStar), nPerDirection is ignored
              )rst")

           .def("getRefPoint", &Mesh::getRefPoint,
              R"rst(
              Get reference point used for the generalized normal
              )rst")

           .def("setRefPoint", &Mesh::setRefPoint,
              R"rst(
              Set reference point used for the generalized normal
              :param refPoint [x,y,z] array
              )rst")

           .def("integrate_volume", &Mesh::integrate_volume,  "Return volume", "direction"_a = 2 , "nbGP"_a = 1)

           .def("integrate_vol_inertia", &Mesh::integrate_vol_inertia,
              R"rst(
              Returns Moments of body Inertia maxtrix 3x3 divided by rho. This is valid only for a body with mass density rho = const.
              Note, that this is 3D integral int_V (d_ij (x_1^2 +x_2^2 +x_3^2) -x_i x_j)dv and not the 2nd moments of area used for the Waterplane
              )rst", "nbGP"_a = 1)

           .def("integrate_cob", &Mesh::integrate_cob, "nbGP"_a = 1)

           .def("setGaussPointsMethod", &Mesh::setGaussPointsMethod, "method"_a,
              R"rst(
              Set the method how to calculate the Gauss Points

              :param method : GaussPointsMethods. By default is GaussPointsMethods.STANDARD.
              If method is GaussPointsMethods.STANDARD then the classical method is used to define the gauss points,
              if it is GaussPointsMethods.HYDROSTAR, then HydroStar algorithm is used.
              )rst")

           .def("getGaussPointsMethod", &Mesh::getGaussPointsMethod,
              R"rst(
              Return the current method used to determine the Gauss points.
              )rst")

           .def("integrate_fields", &Mesh::integrate_fields, "columns"_a = Eigen::ArrayXd(0),
              R"rst(
              Integrate the panel data fields multiplied by the panel normals (n_x, n_y, n_z, n_4, n_5 and n_6),
              so the result is 6x size(columns)

              :param columns : numpy.darray of type int, list of the columns in the panels data to be integrated. If
                  the list is empty, all columns are used

              :return numpy.ndarray of size 6 x size(columns), where each row 'i' presents the result for n_i-th normal result.
              )rst")

           .def("offset", &Mesh::offset, "Offset mesh by vector", "coords"_a )

           .def("scale", &Mesh::scale, "Scale mesh", "coords"_a)


         .def_readonly("nodes", &PyMesh::V_)
         .def_readonly("quads", &PyMesh::Q_)
         .def_readonly("tris", &PyMesh::T_)
         .def_readonly("sym", &PyMesh::sym_)

         .def("getSegments", &Mesh::getSegments,
                      R"rst(
             Get all mesh segments
             )rst")
    ;

    py::class_<HydroStarMeshReader>(m, "HydroStarMeshReader")
        .def(py::init<const std::string &>(),
            R"rst(
              Read a HydroStar mesh file.

             :param string filepath: path to the file containing the
                 mesh in HydroStar format (usually with a '.hst' extension)rst", "filepath"_a)
        .def("getMeshData", &HydroStarMeshReader::getMeshData, py::return_value_policy::reference_internal,
            R"rst(
             Return the list of vertices, indices and faces
             )rst")
        .def("getBodyHeaders", &HydroStarMeshReader::getBodyHeaders, py::return_value_policy::reference_internal,
            R"rst(
             Return the list of body headers
             )rst")
        .def("getTankHeaders", &HydroStarMeshReader::getTankHeaders, py::return_value_policy::reference_internal,
            R"rst(
             Return the list of tank headers
             )rst")
        .def("getOffset", &HydroStarMeshReader::getOffset,
            R"rst(
             Return the mesh type offset
             )rst")
        .def("getCoefZ0", &HydroStarMeshReader::getCoefZ0,
            R"rst(
             Return the Z0 coefficient
             )rst")
    ;

    py::class_<HydroStarMeshReader::BodyHeader>(m, "HydroStarMeshBodyHeader")
        .def(py::init<>())
        .def_readwrite("symmetry", &HydroStarMeshReader::BodyHeader::symmetry)
        .def_readwrite("numPanels", &HydroStarMeshReader::BodyHeader::numPanels)
        .def_readwrite("numPont", &HydroStarMeshReader::BodyHeader::numPont)
        .def_readwrite("numPlate", &HydroStarMeshReader::BodyHeader::numPlate)
        .def_readwrite("numSwat", &HydroStarMeshReader::BodyHeader::numSwat)
        .def_readwrite("numCsf", &HydroStarMeshReader::BodyHeader::numCsf)
        .def_readwrite("numFs", &HydroStarMeshReader::BodyHeader::numFs)
    ;

    py::class_<HydroStarMeshReader::TankHeader>(m, "HydroStarMeshTankHeader")
        .def(py::init<>())
        .def_readwrite("symmetry", &HydroStarMeshReader::TankHeader::symmetry)
        .def_readwrite("numPanels", &HydroStarMeshReader::TankHeader::numPanels)
        .def_readwrite("zfs", &HydroStarMeshReader::TankHeader::zfs)
        .def_readwrite("refptank", &HydroStarMeshReader::TankHeader::refptank)
        .def_readwrite("rho", &HydroStarMeshReader::TankHeader::rho)
    ;

    py::class_<HydroStarMeshReader::MeshData>(m, "HydroStarMeshData")
        .def(py::init<>())
        .def_readwrite("vertices", &HydroStarMeshReader::MeshData::vertices)
        .def_readwrite("indices", &HydroStarMeshReader::MeshData::indices)
        .def_readwrite("faces", &HydroStarMeshReader::MeshData::faces)
    ;

    py::class_<HydroStarMesh>(m, "HydroStarMesh")

        .def(py::init<const HydroStarMesh&>(),
            R"rst(
             Copy constructor (shallow)
             :param HydroStarMesh
             )rst")


        .def(py::init<const std::string &, const unsigned int &, bool>(),
             R"rst(
             Initialisation of a Snoopy Mesh from a HydroStar mesh file.

             :param string filepath: path to the file containing the
                 mesh in HydroStar format (usually with a '.hst' extension)
             :param int nPtsPerDirection: number of Gauss points per direction
                 for quadrangles. Define the rule number for triangles.
                 If 2, 2x2=4 Gauss points for quadrangles.
                 If 2, Dunavant rule -> 3 for triangles.
             )rst", "filepath"_a , "nbGP"_a = 1 , "keepSym"_a = false)


        .def(py::init< std::vector<Mesh> ,
                       std::vector<Mesh> ,
                       std::vector<Mesh> ,
                       std::vector<Mesh> ,
                       std::vector<Mesh> >(),
            R"rst()rst", "underWaterHullMeshes"_a, "aboveWaterHullMeshes"_a , "plateMeshes"_a, "fsMeshes"_a, "tankMeshes"_a)


        .def("append", &HydroStarMesh::append,
             R"rst(
             Add a new mesh object.

             :param fileName: the name of the input file containing the
                 Hydrostar mesh.
             )rst")
        .def("write", &HydroStarMesh::write,
             R"rst(
             Write the mesh into a file.

             :param string fileName: the name of the file into which to write
                 the mesh objects.
             )rst")
        .def("removeUnplanarQuadsToTriangles", &HydroStarMesh::removeUnplanarQuadsToTriangles,
             R"rst(
             Same as :py:meth:`~Mesh.removeUnplanarQuadsToTriangles`.

             :param int iBody: index of the body to process.
             )rst")
        .def("getUnderWaterHullMesh", &HydroStarMesh::getUnderWaterHullMesh, py::return_value_policy::reference_internal,
             R"rst(
             Return the portion of the :code:`iBody` th mesh that is under
             water.

             :param int iBody: index of the body to process.
             )rst")
        .def("getAboveWaterHullMesh", &HydroStarMesh::getAboveWaterHullMesh, py::return_value_policy::reference_internal,
             R"rst(
             Return the portion of the :code:`iBody` th mesh that is above
             water.

             :param int iBody: index of the body to process.
             )rst")

        .def("getPlateMesh", &HydroStarMesh::getPlateMesh, py::return_value_policy::reference_internal,
            R"rst(
             Return the portion of the :code:`iBody` th mesh that are thin plates

             :param int iBody: index of the body to process.
             )rst")
        .def("getFsMesh", &HydroStarMesh::getFsMesh, py::return_value_policy::reference_internal,
            R"rst(
             Return the portion of the :code:`iBody` th mesh that are free-surface mesh

             :param int iBody: index of the body to process.
             )rst")

        .def("getHullMesh", &HydroStarMesh::getHullMesh,
             R"rst(
             Return hull mesh (under + above).
             :param int iBody: index of the body to process.
             )rst")

        .def("getSwatMesh", &HydroStarMesh::getSwatMesh, py::return_value_policy::reference_internal,
             R"rst(
             Return the :code:`iBody` th swat mesh.

             :param int iBody: index of the body to process.
             )rst")
        .def("getCsfMesh", &HydroStarMesh::getCsfMesh, py::return_value_policy::reference_internal,
             R"rst(
             Return the :code:`iBody` th csf mesh.

             :param int iBody: index of the body to process.
             )rst")
        .def("getTankMesh", &HydroStarMesh::getTankMesh, py::return_value_policy::reference_internal,
             R"rst(
             Return the :code:`iTank` th tank mesh.

             :param int iTank: index of the tank to process.
             )rst")

        .def("getNbTank", &HydroStarMesh::getNbTank,
            R"rst(
             Return number of defined tanks
             )rst")

        .def("getNbBody", &HydroStarMesh::getNbBody,
            R"rst(
             Return number of defined bodies
             )rst")

        .def("getNbFs", &HydroStarMesh::getNbFs,
             R"rst(
             Return number of defined free-surface mesh.
             )rst")

        .def("openDeck", &HydroStarMesh::openDeck,
            R"rst(
             )rst")

        .def("addTank", &HydroStarMesh::addTank, "mesh"_a,
            R"rst(
             )rst")

        .def("addFreeSurface", &HydroStarMesh::addFreeSurface, "mesh"_a,
                R"rst(Add a free-surface mesh.

                More precisely, the free-surface mesh of the first body (which is empty by default) is replaced by 'mesh'.

                Parameters
                ----------
                mesh : Mesh
                    The free-surface mesh
             )rst")

        .def("offset", &HydroStarMesh::offset, "coords"_a,
            R"rst(
             )rst")

        .def("getMesh", &HydroStarMesh::getMesh,
            R"rst(
             Return a merged mesh of all bodies parts.
             )rst")
    ;

}

} // End of namespace Meshing
} // End of namespace PythonInterface
} // End of namespace BV
