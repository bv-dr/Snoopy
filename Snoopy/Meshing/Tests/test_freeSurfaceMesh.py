from Snoopy import Meshing as msh
from Snoopy.Meshing.vtk_fs_tools import create_fs_mesh
from Snoopy import logger
from Snoopy.Meshing.vtkView import viewPolyData


def test_vtk_fs_mesh(display = False, meshFile = f"{msh.TEST_DATA:}/B31.hst"):
    """Test the creation of free-surface polydata from a waterline object
    """
    
    logger.setLevel(10)
    
    x0, y0, radius, dx , dy  = 150. , 0.0 , 600., 10. , 10.

    # With symmetry
    mesh = msh.Mesh(msh.HydroStarMesh(meshFile, keepSym = True).getUnderWaterHullMesh(0))
    waterline = mesh.extractWaterlineObj()
    fs = create_fs_mesh( waterline, dx=dx, dy= dy, x0 = x0 , y0 = y0 , radius = radius  )    
   
    # Without symmetry
    mesh_s0 = msh.Mesh(msh.HydroStarMesh(meshFile, keepSym = False).getUnderWaterHullMesh(0))
    waterline_s0 = mesh_s0.extractWaterlineObj()
    fs_0 = create_fs_mesh( waterline_s0, dx=dx, dy= dy, x0 = x0 , y0 = y0 , radius = radius  )    
    
    sMesh = msh.Mesh.FromPolydata( fs_0 )
    
    assert( sMesh.nbpanels > 100 )
    
    if display : 
        viewPolyData(fs, display_props={"edges" : 1})
        viewPolyData(fs_0, display_props={"edges" : 1})
    


def test_vtk_fs(display = False  ):
    """Test fs mesh without waterline
    """
    x0, y0, radius, dx , dy  = 150. , 0.0 , 600., 10. , 10.
    fs1 = create_fs_mesh( waterline=None, dx=dx, dy= dy, x0 = x0 , y0 = y0 , radius = radius  )    
    fs2 = create_fs_mesh( waterline=None, dx=dx, dy= dy, x_min = -100, x_max = +100, y_min = -100, y_max = +100 )
    if display : 
        viewPolyData(fs1, display_props={"edges" : 1})
        viewPolyData(fs2, display_props={"edges" : 1})
    

def test_hstar_add_fs(display = False, meshFile =  rf"{msh.TEST_DATA:}/B31.hst"):
    """Test the addition of an automatic FS mesh to an HydroStar mesh
    """
    # Symmetric case
    hmesh = msh.HydroStarMesh(meshFile, keepSym = True)
    hmesh.clean(1e-5)
    hmesh.addAutoFreeSurface(600, dx=10., dy=10.)
    
    if display : 
        hmesh.vtkView(display_props={"edges" : 1})
        
    # Whole mesh
    hmesh = msh.HydroStarMesh(meshFile, keepSym = False)
    hmesh.clean(1e-5)
    hmesh.addAutoFreeSurface(600, dx=10., dy=10.)
    
    if display : 
        hmesh.vtkView(display_props={"edges" : 1})
        
    # More complicated case : 
    m1 = msh.HydroStarMesh(meshFile,0)
    m2 = msh.HydroStarMesh(meshFile,0)
    mergedMesh = msh.hydroStarMesh.mergeMeshes( m1,m2 , [0,0,0], [100,100,0] )
    mergedMesh.clean(1e-5)
    
    
    mergedMesh.addAutoFreeSurface(600, dx=10., dy=10.)
    
    if display : 
        mergedMesh.vtkView(display_props={"edges" : 1})
        

if __name__ == "__main__" :
    display = False
    meshFile =  rf"{msh.TEST_DATA:}/B31.hst"
    # meshFile =  rf"{msh.TEST_DATA:}/hawai_eq_ref.hst"
    
    test_vtk_fs_mesh( display, meshFile )
    test_hstar_add_fs(display, meshFile)
    test_vtk_fs(display)
