import os
from Snoopy import Meshing as msh


# Not named "test", as it will provoke a crash if run by pytest on runners not supporting open-gl
def demo_pics(clean = True):
    
    fname = "test_mesh_pics.png"
    if os.path.exists(fname) : 
        os.remove(fname)
    mesh = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst" ,   1 )
    m = msh.Mesh( mesh.getUnderWaterHullMesh(0) )
    msh.vtkView.VtkLite.FromMesh(m, display_props={"edges":1, "color" : [1,0,0]},
                                 camera_preset = "left", camera_kwds = { "fitView" : 1.0}
                                 ).to_picture(fname)
    
    assert( os.path.exists(fname) )
    
    if clean : 
        os.remove(fname)



if __name__ == "__main__" :
    demo_pics(False)
    
