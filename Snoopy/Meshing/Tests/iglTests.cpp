/*
 * iglTests.cpp
 *
 *  Created on: 21 nov. 2016
 *      Author: cbrun
 */

#include "Tests/iglTests.hpp"

Eigen::MatrixXd V, VC ;
Eigen::MatrixXi F, FC ;
Eigen::MatrixXd U ;
Eigen::MatrixXi G ;
Eigen::VectorXi I ;
Eigen::VectorXi J ;

int main(int argc, char *argv[])
{
    // Load a mesh in OFF format
    igl::readOBJ("C://Users/cbrun/DVT/libIGL/libigl/tutorial/shared/lilium.obj",
                 V, F) ;
    igl::readOBJ("C://Users/cbrun/DVT/libIGL/libigl/tutorial/shared/cube.obj",
                 VC, FC) ;

    // Print the vertices and faces matrices
//    std::cout << "Vertices: " << std::endl << V << std::endl ;
//    std::cout << "Faces:    " << std::endl << F << std::endl ;
    Eigen::MatrixXd barycenters ;
    Eigen::VectorXd areas ;
    Eigen::MatrixXd normals ;
    igl::barycenter(V, F, barycenters) ;
    igl::doublearea(V, F, areas) ;
    areas *= 0.5 ;
    igl::per_face_normals(V, F, normals) ;
//    std::cout << "Barycenters:    " << std::endl << barycenters << std::endl ;
//    std::cout << "Areas:    " << std::endl << areas << std::endl ;
//    std::cout << "Normals:    " << std::endl << normals << std::endl ;
    igl::writePLY("meshTests/testIni.ply", V, F) ;
    igl::writePLY("meshTests/cubeIni.ply", VC, FC) ;
//    // Intersection
//    igl::copyleft::cgal::CSGTree M ;
//    M =
//    {
//        {   V, F},
//        {   VC, FC}, "i"} ;
//    igl::writePLY("meshTests/testIntersection.ply", M.cast_V<Eigen::MatrixXd>(),
//                  M.F()) ;
//
    igl::decimate(V, F, 1000, U, G, J) ;
    igl::writePLY("meshTests/testIni1000.ply", U, G) ;
    igl::decimate(V, F, 100, U, G, I, J) ;
    std::cout << "decimate done" << std::endl ;
    igl::writePLY("meshTests/testIni100.ply", U, G) ;
    igl::decimate(V, F, 10, U, G, J) ;
    igl::writePLY("meshTests/testIni10.ply", U, G) ;
    return 0 ;
}
