import os
import numpy as np
from Snoopy import Meshing as msh

def test_mergedMesh() :

    m1 = msh.HydroStarMesh(f"{msh.TEST_DATA:}/B31.hst",0)
    m2 = msh.HydroStarMesh(f"{msh.TEST_DATA:}/B31.hst",0)

    mergedMesh = msh.hydroStarMesh.mergeMeshes( m1,m2 , [0,0,0], [100,100,0] )

    if not os.path.exists("mesh_write"):
       os.mkdir("mesh_write")

    mergedMesh.write("mesh_write/merged.hst")
    assert(mergedMesh.getNbBody() == 2)
    reread = msh.HydroStarMesh(filepath = r"mesh_write/merged.hst")
    os.remove("mesh_write/merged.hst")
    assert(True)

if __name__ == "__main__" :


    print ("Test merge mesh")
    test_mergedMesh()








