from Snoopy import Meshing as msh
from Snoopy import Mechanics as mcn
import numpy as np
from Snoopy import logger, logging

from Snoopy.Tools.print_format import write_matrix
from Snoopy.Mechanics.mechanicalsolver import get_gravity_stiffness

logger.setLevel(logging.DEBUG)
#msh.set_logger_level(logging.DEBUG, "")


def test_integration_ship():

    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst", 1 )

    hstar_cob = np.array([148.2669839, 0.000,  -8.499106328])
    
    hstar_volume = 0.18946E+06

    nbGP=1

    hull = msh.Mesh(a.getUnderWaterHullMesh(0))
    hull.clean(1e-4)  # Not sure why, but stricter tolerance lead to more difference with HydroStar
    hull.refreshGaussPoints(nbGP)

    volumeX = hull.integrate_volume(direction = 0, nbGP = nbGP)
    volumeY = hull.integrate_volume(direction = 1, nbGP = nbGP)
    volumeZ = hull.integrate_volume(direction = 2, nbGP = nbGP)

    cob = hull.integrate_cob(nbGP=nbGP)

    print ( f"{volumeX / hstar_volume-1:.1%}")
    print ( f"{volumeY / hstar_volume-1:.1%}")
    print ( f"{volumeZ / hstar_volume-1:.1%}")

    assert( np.isclose( volumeX,  hstar_volume, rtol = 0.03 ) )
    assert( np.isclose( volumeY,  hstar_volume, rtol = 0.02 ) )
    assert( np.isclose( volumeZ,  hstar_volume, rtol = 0.02 ) )

    assert( np.isclose( cob,  hstar_cob, rtol = 0.005 ).all())
    print ("Volume and COB integration ok with {} gauss points".format(nbGP))


def test_integration_box():
    
    vol_ref = 4.0
    surface = 12.0
    cob_ref = np.array([0.0 , 0.0 , -0.5] , dtype = float)
    
    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/box.hst", 1 )
    hull = a.getUnderWaterHullMesh(0)
    hull.setGaussPointsMethod( msh.GaussPointsMethods.STANDARD )
    volume = hull.integrate_volume()
    cob = hull.integrate_cob()
    
    assert( np.isclose( volume , vol_ref ) )
    assert( np.isclose( cob , cob_ref ).all() )
    

    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/boxTrap.hst", 1 )
    hull = a.getUnderWaterHullMesh(0)
    hull.setGaussPointsMethod( msh.GaussPointsMethods.HYDROSTAR )
    hull.refreshGaussPoints()
    assert np.sum(hull.getGaussWiWjdetJ()) == 12.0
    
    volume = hull.integrate_volume()
    cob = hull.integrate_cob()
    assert( np.isclose( volume , vol_ref ) )
    assert( np.isclose( cob , cob_ref ).all() )
    
    #Note the integration would have been wrong with "standard" gauss point : 
    hull.setGaussPointsMethod( msh.GaussPointsMethods.STANDARD )
    hull.refreshGaussPoints()
    cob_wrong = hull.integrate_cob()
    assert np.sum(hull.getGaussWiWjdetJ()) == 12.0
    print ( "Reference   cob : ", cob_ref )
    print ( "HydroStarGP cob : ", cob )
    print ( "StandardGP cob : ", cob_wrong )
        

def test_normals():
    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst", 1 )
    hull = a.getUnderWaterHullMesh(0)
    hull.integrate_volume()
    hull.integrate_cob()

    gn_snoop = hull.getNormalsAtGaussPoints()
    refPoint = hull.getRefPoint()

    for i in range(hull.getNPanels()) :
        gn = np.cross( hull.getGaussPoints()[i] - refPoint , hull.getNormalsAtGaussPoints()[i][0:3])
        if not ((abs(gn_snoop[i][3:] - gn) < 0.05).all()) :
            assert(False)
    print ("Generalized normals ok")


def test_hydrostatic_ref_point():
    """Compare hydrostatic matrix to the one calculated from HydroStar."""
    
    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst", 1 )
    mesh = msh.Mesh(a.getUnderWaterHullMesh(0))
    
    cob = mesh.integrate_cob() 
    np.array([ 148.267, 0.000, -4.343])
    
    p1 = np.array([10.0 , 20., 30.])
    p2 = np.array([5.0 , 10., -30.])
    p3 = np.array([5.0 , 0.,  20.])
    
    rho , g = 1025, 9.81
    
    mesh.setRefPoint( p1 )
    hyds_bf_p2 = mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "body-fixed" ) * rho * g
    hyds_hydro_p2 = mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "hydro", buoyancy_stiffness=True )  * rho * g
    hyds_hydroh_p2 = mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "hydro", buoyancy_stiffness=False )  * rho * g
        
    mesh.setRefPoint( p2 )
    hyds_bf_p2_check = mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "body-fixed" ) * rho * g
    hyds_hydro_p2_check = mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "hydro", buoyancy_stiffness=True ) * rho * g
    hyds_hydroh_p2_check = mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "hydrostatic_hull", buoyancy_stiffness=False  ) * rho * g

    hyds_hydro_p3 = mesh.integrate_stiffness_matrix( output_ref_point = p3, ref_frame = "hydro", buoyancy_stiffness=True ) * rho * g
    hyds_hydroh_p3 = mesh.integrate_stiffness_matrix( output_ref_point = p3, ref_frame = "hydro", buoyancy_stiffness=False ) * rho * g
    hyds_hydro_p2_check2 = mcn.matrans3( hyds_hydro_p3, p3, p2 )
    hyds_hydroh_p2_check2 = mcn.matrans3( hyds_hydroh_p3, p3, p2 )
    
    assert( np.isclose( hyds_bf_p2, hyds_bf_p2_check , atol = 1e-02, rtol = 0.001).all() )
    assert( np.isclose( hyds_hydro_p2, hyds_hydro_p2_check ).all() )
    
    assert( np.isclose( hyds_hydroh_p2, hyds_hydroh_p2_check ).all() )
    
    assert( np.isclose( hyds_hydroh_p2, hyds_hydroh_p2_check2 ).all() )
    

    # Pressure part of hydrostatic stiffness can not be moved with matrans :
    assert( not np.isclose( hyds_hydro_p2, hyds_hydro_p2_check2 ).all() )
    
    # print( write_matrix(hyds_hydroh_p2) )
    # print( write_matrix(hyds_hydroh_p2_check2) )
    # assert( np.isclose( hyds_hydro_p2, hyds_hydro_p2_check2, atol = 1e-02, rtol = 0.001 ).all() )
    

def test_gravity_stiffness_ref_point():

    mass = 1
    
    cog = np.array([5., 8., 10.])
    p1 = np.array([6., 4., 0.])
    p2 = np.array([5., 6., -10.])
    
    m_bf_p1 = get_gravity_stiffness( mass, cog, p1 , ref_frame = "body-fixed" )    
    m_bf_p2 = get_gravity_stiffness( mass, cog, p2 , ref_frame = "body-fixed" )
    m_bf_p1_check = mcn.matrans3( m_bf_p2, p2 , p1 )
    assert(  np.isclose( m_bf_p1 , m_bf_p1_check ).all()   )



def test_hydrostatic_total():
    """Compares total hydrostatic stiffness (hydro / body-fixed).
    """
    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst", 1 )
    mesh = msh.Mesh(a.getUnderWaterHullMesh(0))

    rho, g = 1025, 9.81
    
    cob = mesh.integrate_cob()
    volume = mesh.integrate_volume()
    mass = rho*volume
    cog = np.array([cob[0], cob[1], -4])
    
    p1 = np.array([6., 4., 0.])
    p2 = np.array([5., 6., -10.])
    p2 = cog
    
    hyds_bf_p1 = get_gravity_stiffness( mass, cog, p1 , ref_frame = "body-fixed" ) + mesh.integrate_stiffness_matrix( output_ref_point = p1, ref_frame = "body-fixed")  * rho * g
    hyds_bf_p2 = get_gravity_stiffness( mass, cog, p2 , ref_frame = "body-fixed" ) + mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "body-fixed")  * rho * g
    
    hyds_hydro_p1 = get_gravity_stiffness( mass, cog, p1 , ref_frame = "hydro" ) + mesh.integrate_stiffness_matrix( output_ref_point = p1, ref_frame = "hydro" )  * rho * g
    hyds_hydro_p2 = get_gravity_stiffness( mass, cog, p2 , ref_frame = "hydro" ) + mesh.integrate_stiffness_matrix( output_ref_point = p2, ref_frame = "hydro" )  * rho * g
    
    hyds_bf_p2_check = mcn.matrans3( hyds_bf_p1, p1 , p2 )
    hyds_hydro_p2_check = mcn.matrans3( hyds_hydro_p1, p1 , p2 )
    
    assert(  np.isclose( hyds_bf_p2 , hyds_bf_p2_check, atol = 1e-4 ).all()  )
    assert(  np.isclose( hyds_hydro_p2_check , hyds_hydro_p2, atol = 1e-4 ).all() )
    
    
    assert(  np.isclose( hyds_hydro_p2[0:3,0:3] , hyds_bf_p2[0:3,0:3], atol = np.abs(hyds_hydro_p2[0:3,0:3]).max() / 1000.  ).all()   )
    assert(  np.isclose( hyds_hydro_p2[3:6,3:6] , hyds_bf_p2[3:6,3:6], atol = np.abs(hyds_hydro_p2[3:6,3:6]).max() / 1000.  ).all()   )
    
    assert(  np.isclose( hyds_hydro_p2[0:3,3:6] , hyds_bf_p2[0:3,3:6], atol = np.abs(hyds_hydro_p2[0:3,3:6]).max() / 1000. , rtol = 0.001 ).all()   )
    assert(  np.isclose( hyds_hydro_p2[3:6,0:3] , hyds_bf_p2[3:6,0:3], atol = np.abs(hyds_hydro_p2[3:6,0:3]).max() / 1000.  ).all()   )


def test_hydrostatic_hstar():
    """Compare hydrostatic matrix to the one calculated from HydroStar."""

    a = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst", 1 )

    hstar_hstat = np.array([[0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,   0.00000000e+00, 0.00000000e+00],
                            [0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,   0.00000000e+00, 0.00000000e+00],
                            [0.00000000e+00, 0.00000000e+00, 1.16620665e+04, 0.00000000e+00,   1.10612563e+05, 0.00000000e+00],
                            [0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.75431207e+06,   0.00000000e+00, 0.00000000e+00],
                            [0.00000000e+00, 0.00000000e+00, 1.10622901e+05, 0.00000000e+00,   6.83054147e+07, 0.00000000e+00],
                            [0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 0.00000000e+00,   0.00000000e+00, 0.00000000e+00]])

    # hstar_volume = 0.18946E+06
    hstar_cob = np.array([148.2669839, 0.000,  -8.499106328])

    mesh = msh.Mesh(a.getUnderWaterHullMesh(0))
    
    res_cob = mesh.integrate_cob()
    
    assert(np.isclose( res_cob , hstar_cob , atol = 1e-6 , rtol = 0.01).all())
    
    mesh.setRefPoint([0.2, 0.5, 10])

    res_hstat = mesh.integrate_stiffness_matrix(ref_frame = "hydro", output_ref_point = hstar_cob, buoyancy_stiffness=False)
            
    assert( np.isclose( res_hstat ,  hstar_hstat , rtol = 0.02 , atol = 1e-4  ).all()  )

def MxRollRef(cubeDim, alpha):
    tanAlpha = np.tan(alpha)
    dim_2 = cubeDim / 2.
    rectangleLowerV = 4. * dim_2**3 * (1. - tanAlpha)
    rectangleLowerLocalPtz = - dim_2 / 2. * (1. + tanAlpha)
    triangleUpperV = 4. * dim_2**3 * tanAlpha
    triangleUpperLocalPtz = -1. / 3. * dim_2 * tanAlpha
    triangleUpperLocalPty = 1. / 3. * dim_2
    volume = rectangleLowerV + triangleUpperV

    volumePtz = (rectangleLowerV * rectangleLowerLocalPtz +
                 triangleUpperV * triangleUpperLocalPtz) / volume

    volumePty = (rectangleLowerV * 0. +
                 triangleUpperV * triangleUpperLocalPty) / volume

    F = 1. * cubeDim**2
    FyLocal = -F * np.sin(alpha)
    FzLocal = F * np.cos(alpha)

    return -(volumePty * FzLocal - volumePtz * FyLocal)


def test_moment_cube():

    from Snoopy import Geometry as geo
    angle = np.deg2rad(2.5)
    moment_ref = MxRollRef(cubeDim = 2.0 , alpha = angle)
    
    for mesh_file, method, nb_gp in [  (f"{msh.TEST_DATA:}/bigCube.obj" , msh.GaussPointsMethods.STANDARD , 2), 
                                       (f"{msh.TEST_DATA:}/bigCubeDiscretized.obj" , msh.GaussPointsMethods.STANDARD , 1), 
                                       (f"{msh.TEST_DATA:}/bigCube.obj" , msh.GaussPointsMethods.HYDROSTAR , 2), 
                                       (f"{msh.TEST_DATA:}/bigCubeDiscretized.obj" , msh.GaussPointsMethods.HYDROSTAR , 1), 
                                ] :
    
        if "bigCubeDiscretized" in mesh_file: 
            symType =  msh.SymmetryTypes.XY_XZ_PLANES
        else : 
            symType =  msh.SymmetryTypes.NONE
            
        mesh = msh.Mesh( mesh_file, symType = symType , nbGp=2)
        mesh.rotateAxis( [1.0 , 0.0 , 0.0] , angle )
    
        cuttedMesh = msh.Mesh(mesh.getCutMesh( geo.Point( [0.,0.,0.] ) , geo.Vector([0.,0.,1.0])))
        cuttedMesh.setRefPoint([0., 0., 0.])
        
        cuttedMesh.setGaussPointsMethod(method)
        cuttedMesh.refreshGaussPoints(nb_gp)
        moment = cuttedMesh.integrate_hydrostatic()[3]
        print ( f"Error = {moment / moment_ref-1:.2%}")

        assert( np.isclose( moment, moment_ref, rtol = 1e-3))
    

if __name__ == "__main__" :
    test_normals()
    test_integration_ship()
    test_integration_box()
    test_hydrostatic_ref_point()
    test_hydrostatic_total()
    print("test_moment_cube")
    test_moment_cube()
    

    
   
