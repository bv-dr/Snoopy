/*
 * meshTest.cpp
 *
 *  Created on: 22 nov. 2016
 *      Author: cbrun
 */

#include <iostream>

#include "Meshing/Mesh.hpp"

int main(void)
{
    BV::Meshing::Mesh mesh(
        "C://Users/cbrun/DVT/libIGL/libigl/tutorial/shared/lilium.obj") ;
    mesh.write("meshTests/testIni.ply") ;
    mesh.getDecimated(100).write("meshTests/test100.ply") ;
    mesh.getDecimated(1000).write("meshTests/test1000.ply") ;
    mesh.getDecimated(10000).write("meshTests/test10000.ply") ;
    BV::Meshing::Mesh mesh2(
        "C://Users/cbrun/DVT/libIGL/libigl/tutorial/shared/meshfpso.obj") ;
    std::cout << "write fpso mesh" << std::endl ;
    mesh2.write("meshTests/testfpso.ply") ;
    mesh2.write("meshTests/testfpso.obj") ;
    mesh2.convertPanelsInTriangles() ;
    std::cout << "fpso mesh (triangles)" << std::endl ;
    mesh2.write("meshTests/testfpsoTriangles.ply") ;
    mesh2.write("meshTests/testfpsoTriangles.obj") ;
    BV::Meshing::Mesh mesh3(
        "C://Users/cbrun/DVT/libIGL/libigl/tutorial/shared/horse_quad.obj") ;
    mesh3.write("meshTests/horse_triangles.obj") ;
    mesh3.getDecimated(100).write("meshTests/horse100.obj") ;
    mesh3.getDecimated(1000).write("meshTests/horse1000.obj") ;
    // Cut a mesh
    BV::Geometry::Point pt(0., 0., 0.) ;
    BV::Geometry::Vector direction(0., 0., 1.) ;
    std::string fname = std::string("meshTests/horseCut0.obj") ;
    mesh3.getCuttedMesh(pt, direction).write(fname) ;
//    double z(-0.08) ;
//    int i(0) ;
//    while (z < 0.08)
//    {
//        BV::Geometry::Point pt(0., 0., z) ;
//        BV::Geometry::Vector direction(0., 0., 1.) ;
//        try
//        {
//            std::string fname = std::string("meshTests/horseCut")
//                + std::to_string(i) + std::string(".obj") ;
//            std::cout << fname << std::endl ;
//            mesh3.getCuttedMesh(pt, direction).write(fname) ;
//        }
//        catch (...)
//        {
//
//        }
//        z += 0.01 ;
//        ++i ;
//    }
    std::cout << "End of tests" << std::endl ;
}
