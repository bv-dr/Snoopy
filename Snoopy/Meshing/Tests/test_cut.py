from scipy.optimize import fsolve
from Snoopy import Meshing as msh
from Snoopy import Geometry as geo
import numpy as np

from Snoopy import logger

logger.setLevel(10)


def test_mesh_cut():
    # For now, just check that the cut runs
    hs_mesh = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst" , keepSym = True )

    mesh = hs_mesh.getUnderWaterHullMesh(0)
    mesh.append( hs_mesh.getAboveWaterHullMesh(0))
    mesh = msh.Mesh(mesh)
    wetted = mesh.getCutMesh( geo.Point( 0,0,0), geo.Vector(0,0,1 ))
    dry = mesh.getCutMesh( geo.Point( 0,0,0), geo.Vector(0,0,-1 ))

    assert(True)

def test_mesh_cut2(printPanelsData = False):
    hs_mesh = msh.HydroStarMesh( f"{msh.TEST_DATA:}/cube.hst", keepSym = True )
    mesh = hs_mesh.getUnderWaterHullMesh(0)
    if printPanelsData:
        if mesh.hasPanelsData():
            print(mesh.getPanelsData())
        else:
            print(f"mesh does not have PanelsData")
    #msh.Mesh(mesh).write(f"{msh.TEST_DATA:}/cube_copy.hst")

    mesh = msh.Mesh(mesh.getCutMesh( geo.Point(-2.5, 0, 0), geo.Vector(-np.sqrt(2), -np.sqrt(2), 0)))
    if printPanelsData:
        if mesh.hasPanelsData():
            print(mesh.getPanelsData())
        else:
            print(f"mesh does not have PanelsData")
    #mesh.write(f"{msh.TEST_DATA:}/cube_cutted.hst")
    mesh.write_vtk_hdf(f"{msh.TEST_DATA:}/cube_cutted.hdf")

    assert(True)

if __name__ == "__main__" :

    #test_mesh_cut()
    test_mesh_cut2()
