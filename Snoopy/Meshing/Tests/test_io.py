import numpy as np
from Snoopy import Meshing as msh

f = f"{msh.TEST_DATA:}/B31_NOHEADER.hst"

def test_write_vtk_hdf():
    # Test writing vtkhdf with h5 and reading it back with vtk.
    hmesh = msh.HydroStarMesh(  f , 0 )
    mesh = msh.Mesh(hmesh.getUnderWaterHullMesh(0))

    mesh.setPanelsData( np.arange( mesh.getNPanels()*4 ) .reshape( (mesh.getNPanels(), 4)  ) )
       
    mesh.setDataNames( ["TEST", "TEST", "TEST2" , "TEST3"] )
    mesh.setDataTypes( [ 1 , 2, 2 , 1] ) 
    mesh.setDataFreqs( [ 0.5, 0.5, 0.6, 0.6] )
    mesh.setDataHeads( [ 180. , 180, 150., 150.])
        
    mesh.write_vtk_hdf("test.hdf")

    mesh2  = msh.Mesh.ReadHdf( "test.hdf" )
    
    assert( (mesh.getMetaDataDF().values ==  mesh2.getMetaDataDF().values).all() )

    assert( (mesh.getPanelsData() ==  mesh2.getPanelsData()).all() )
    
    
    import vtk
    r = vtk.vtkHDFReader()
    r.SetFileName( "test.hdf" )
    r.Update()
    ug = r.GetOutput()
    data = vtk.util.numpy_support.vtk_to_numpy( ug.GetCellData().GetArray(0) )
    assert( np.isclose( data , mesh.getPanelsData()[:,0] ).all())


def test_convert_vtk():
    # Test writing vtkhdf with h5 and reading it back with vtk.
    hmesh = msh.HydroStarMesh(  f , 0 )
    mesh = msh.Mesh(hmesh.getUnderWaterHullMesh(0))
    polydata = mesh.convertToVtk()
    mesh2 = msh.Mesh.FromPolydata(polydata)
    
    assert( mesh2.nbpanels == mesh.nbpanels)
    
    

if __name__ == "__main__":

    test_write_vtk_hdf()    
    test_convert_vtk()
