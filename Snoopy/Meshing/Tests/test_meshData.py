import os
import sys
from Snoopy import Meshing as msh
import numpy as np

#msh.set_logger_level(10, "")

f = f"{msh.TEST_DATA:}/B31_NOHEADER.hst"


def test_hstarReader() :

    hmesh = msh.HydroStarMesh(  f , 0 )
    hmesh.getUnderWaterHullMesh(0).setPanelsData( hmesh.getUnderWaterHullMesh(0).getPanelsData() * np.pi )
    hmesh.write("mesh_write/mesh_data.hst")
    print (hmesh)
    #hmesh = hmesh.getUnderWaterHullMesh(0)
    #print(hmesh.getPanelsData())
    #print(hmesh.getDataNames())
    #print(hmesh.getDataTypes())
    #print(hmesh.getDataFreqs())
    #print(hmesh.getDataHeads())


def test_clean():

    vertices = [ [0,0,0],
                 [0,2,0],
                 [2,2,0],
                 [2,0,0],

                 [0,0,2],
                 [0,2,2],
                 [2,2,2],
                 [2,0,2],
                ]
    tris = [
             [4,5,6 ],         #15  25
             [4,5,5 ],         #16  26
           ]
    quads = [
              [ 0,1,5,4 ],     #17  27
              [ 0,1,2,2 ],     #18  28
              [ 7,6,2,3 ],     #19  29
             ]
    panelsData = [[15.,25.],
                  [16, 26],
                  [17, 27],
                  [18, 28],
                  [19, 29],
                  ]

    mesh = msh.Mesh( Vertices = np.array(vertices, dtype = float),
                     Quads = np.array(quads),
                     Tris = np.array(tris).reshape(-1,3),
                     panelsData = np.array(panelsData, dtype = float))
    #mesh.setDataNames(["Data1", "Data2"])
    #mesh.setDataTypes([ 0, 1 ])
    #mesh.setDataFreqs([ 0.1, 0.2])
    #mesh.setDataHeads([180., 270.])
    mesh.setPanelsMetadata(["Data1", "Data2"], [0, 1], [0.1, 0.2], [180., 270.])
    mesh.setPanelsMetadata(0, "NoData", 5, 0.5, 150)
    print(f"panelsData : {mesh.getPanelsData():}")
    print(f"      names: {mesh.getDataNames():}")
    print(f"      types: {mesh.getDataTypes():}")
    print(f"      freqs: {mesh.getDataFreqs():}")
    print(f"      heads: {mesh.getDataHeads():}")
    mesh.setGaussPointsMethod(msh.GaussPointsMethods.HYDROSTAR)
    print(f"      field integrals as HydroStar:\n{mesh.integrate_fields([0, 1]):}")
    print(mesh.getNormalsAtGaussPoints())
    mesh.setGaussPointsMethod(msh.GaussPointsMethods.STANDARD)
    print(f"      field integrals by Gauss points:\n{mesh.integrate_fields([0, 1]):}")
    print(mesh.getNormalsAtGaussPoints())
    #mesh.write_vtk_hdf("test.hdf")

    assert( np.isclose( [[15., 25], [18., 28], [17, 27], [19, 29] ], mesh.getPanelsData()).all() )

if __name__ == "__main__" :
    test_clean()

    #test_hstarReader()
