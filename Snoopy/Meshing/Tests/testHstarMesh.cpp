/*
 * testHstarMesh.cpp
 *
 *  Created on: 12 d�c. 2016
 *      Author: cbrun
 */

#include "Meshing/HydroStarMesh.hpp"

using namespace BV::Meshing ;
int main(void)
{
    std::cout << "Start" << std::endl ;
    HydroStarMesh mesh(
        "C:/Users/cbrun/My Documents/SametimeFileTransfers/11400.hst") ;
    std::cout << "get mesh &" << std::endl ;
    mesh.getUnderWaterHullMesh(0).write("Meshing/meshUnder.obj") ;
    mesh.getAboveWaterHullMesh(0).write("Meshing/meshAbove.obj") ;
    mesh.getHullMesh(0).write("Meshing/meshHull.obj") ;
    std::cout << "End" << std::endl ;
    return 0 ;
}

