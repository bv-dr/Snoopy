/*
 * iglTests.hpp
 *
 *  Created on: 21 nov. 2016
 *      Author: cbrun
 */

#ifndef MESHING_TESTS_IGLTESTS_HPP_
#define MESHING_TESTS_IGLTESTS_HPP_

#include <igl/barycenter.h>
#include <igl/decimate.h>
#include <igl/doublearea.h>
#include <igl/per_face_normals.h>
#include <igl/readOBJ.h>
#include <igl/writePLY.h>




#endif /* MESHING_TESTS_IGLTESTS_HPP_ */
