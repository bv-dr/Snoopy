import numpy as np
from scipy.optimize import fsolve
from Snoopy import Meshing as msh
from Snoopy import Geometry as geo
from Snoopy import logger

logger.setLevel(10)


def test_balancing():
    # For now, just check that equilibrium calculation runs

    hs_mesh = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst" , keepSym = True )

    mesh = hs_mesh.getUnderWaterHullMesh(0)
    mesh.append( hs_mesh.getAboveWaterHullMesh(0))
    mesh = msh.Mesh(mesh)

    CoG = [  148.267  , 0.0 , -8]
    mass = 1.92394E+08
    bal = msh.balance.MeshBalance(  mesh , mass , CoG , rho = 1025 )
    bal.get_balanced_HydroStarMesh().write("mesh_write/b31_balanced.hst")
    print (bal.balancedPosition)

    assert(True)

    bal = msh.balance.MeshBalance(  mesh , mass , CoG , rho = 1025, forceSymmetry = True )
    bal.get_balanced_HydroStarMesh().write("mesh_write/b31_balanced.hst")
    print (bal.balancedPosition)

    assert(True)


def test_cube():
    L = 10
    mesh = msh.Mesh( Vertices = 0.5*L*np.array([ [-1., +1. ,  +1. ],
                                           [-1., -1. ,  +1. ],
                                           [+1., -1. ,   +1. ],
                                           [+1., +1. ,   +1. ],
                                           [+1., +1 ,  -1 ],
                                           [+1., -1 ,  -1 ],
                                           [-1., -1. ,  -1 ],
                                           [-1., +1. ,  -1 ],
                                           ], dtype = float),
                     Quads = np.array( [[0,1,2,3],
                                        [0,3,4,7],
                                        [1,2,5,6][::-1],
                                        [4,5,6,7],
                                        [0,1,6,7][::-1],
                                        [2,3,4,5][::-1]
                                        ], dtype = int),
                     Tris = np.empty( (0,3), dtype = int),

                            )
    mesh.write("mesh_write/cube.hst")

    rho = 1000
    CoG = [  0.0 , 0.0 , 10.0 ]
    T = 1
    mass = L**2  * T * rho

    #---  For now balance, find the closest equilibrium position, even if unstable
    bal = msh.balance.MeshBalance(  mesh , mass , CoG , rho = rho , x0 = [0.0 , np.pi, 0.0] )
    bal.get_balanced_HydroStarMesh().write("mesh_write/balance_cube_1.hst")

    assert( np.isclose(  bal.balancedPosition, [ 4 , np.pi , 0.0] ).all())
    p = [0 , 0 , 10.]
    new_p = bal.get_point_in_new_system( p, bal.balancedPosition )
    assert(  np.isclose( new_p , [0 , 0 , -6.0] ).all() )


    #---  For now balance, find the closest equilibrium position, even if unstable
    bal_2 = msh.balance.MeshBalance(  mesh , mass , CoG , rho = rho , x0 = [0.0 , np.pi*0.9, 0.0] )
    bal_2.get_balanced_HydroStarMesh().write("mesh_write/balance_cube_2.hst")



if __name__ == "__main__":

    test_balancing()
    test_cube()
