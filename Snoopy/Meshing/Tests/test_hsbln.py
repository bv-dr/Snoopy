import os
import Snoopy.Meshing as msh
import pytest

cases = [[
          f"{msh.TEST_DATA:}/hawai.bln" ,
          "hawai_eq.hst" ,
          f"{msh.TEST_DATA:}/hawai_eq_ref.hst"
         ],
         [
          f"{msh.TEST_DATA:}/hawai_dism.bln" ,
          "hawai_eq_dism.hst" ,
          f"{msh.TEST_DATA:}/hawai_eq_dism_ref.hst"
         ],
        ]

@pytest.mark.parametrize("case", cases)
def test_hsbln( case ) :
    # Changing the working path to the 'test_HSbln.py' location
    filepath = os.path.dirname( os.path.abspath(__file__))
    here = os.getcwd()  # reference to the current working directory to be restored
    os.chdir(filepath)

    inputFile = case[0]
    fileNew   = case[1]
    fileRef   = case[2]

    print("Testing HSbln: {:s}".format(inputFile))

    print("Balancing the mesh")
    msh.balance.hsbln(inputFile)

    # Compare new mesh to reference mesh. For now, chech that they are identical, test to refine.
    with open(fileRef , "r") as fil:
        data_ref = [line.strip().split() for line in fil.readlines() if line.strip() and not line.strip().startswith('#')]

    with open(fileNew , "r") as fil:
        data_new = [line.strip().split() for line in fil.readlines() if line.strip() and not line.strip().startswith('#')]

    ok = True

    for ielem , elem in enumerate(data_ref) :
        for isubelem, subelem in enumerate(elem) :
            if len( data_ref[ielem]) == len( data_new[ielem]) :
                if data_ref[ielem][isubelem] != data_new[ielem][isubelem] :
                    try :  # Si flottant, on laisse un marge d'erreur
                        diff = float( data_ref[ielem][isubelem] ) - float( data_new[ielem][isubelem] )
                        if diff > 1e-4 :
                            ok = False
                            print("Difference found, line=" , ielem , "word=", isubelem , data_ref[ielem][isubelem] , data_new[ielem][isubelem])
                    except :
                        ok = False
                        print("Difference found, line=" , ielem , "word=", isubelem , data_ref[ielem][isubelem] , data_new[ielem][isubelem])
            else :
                ok = False
                print("Difference found:" ,data_ref[ielem] , data_new[ielem])

    if ok :
        print("Test OK : " ,fileNew,  " = " , fileRef , " !")
        os.remove(fileNew)
        if os.path.exists(fileNew + ".pts"):
            os.remove(fileNew + ".pts")

    os.chdir(here)  # restore the working directory before assert
    assert(ok)


if __name__ == "__main__" :

    print ("Running HSbln tests")
    for case in cases:
        test_hsbln(case)
        print (case , "ok")
