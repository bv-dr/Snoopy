from Snoopy import Meshing as msh


def test_clean():
    m = msh.HydroStarMesh( f"{msh.TEST_DATA:}/B31.hst", 1 )
    m.clean(1.e-4)
    m = m.getUnderWaterHullMesh(0)
    #m.removeUnreferencedNodes()
    assert( len(m.nodes) == 804 )


if __name__ == "__main__" :

    test_clean()

