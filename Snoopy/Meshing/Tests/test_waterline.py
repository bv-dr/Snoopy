from Snoopy import Meshing as msh
from matplotlib import pyplot as plt
from Snoopy.Meshing.waterline import inside_wl


def test_waterline(  display = False, meshFile = f"{msh.TEST_DATA:}/B31.hst", ref = 110) :
    """Test waterline extraction
    """
    for keepSym, d in ([True , 2] , [False,1] ):
        mesh = msh.Mesh( msh.HydroStarMesh( meshFile, keepSym = keepSym ).getUnderWaterHullMesh(0) )
        mesh.clean(1e-5) # Merge close that are very close together
        waterline = mesh.extractWaterlineObj()
        if ref is not None :
            assert( waterline.n_segments*d == ref)
            
        if display :
            waterline.plot()
            
        mesh_all = msh.HydroStarMesh( meshFile, keepSym = keepSym ).getMesh()
        mesh_all.clean(1e-5)
        waterline_all = mesh_all.extractWaterlineObj()
        
    
        if ref is not None :
            assert( waterline_all.n_segments*d == ref)

        if display :
            waterline_all.plot()


def test_vtkWaterline( meshFile = f"{msh.TEST_DATA:}/B31.hst", display = False) :
    from Snoopy.Meshing.vtk_fs_tools import extractWaterLine
    mesh = msh.Mesh( msh.HydroStarMesh( meshFile ).getUnderWaterHullMesh(0) )
    orderWaterline = extractWaterLine( mesh.toVtkPolyData() )
    if display :
        fig, ax = plt.subplots()
        ax.plot( orderWaterline[:,0] , orderWaterline[:,1])




def test_isInWaterPlane():
    from Snoopy.Meshing.mesh_io import read_hslec_waterline_h5
    x1, y1, x2, y2 = read_hslec_waterline_h5(f"{msh.TEST_DATA:}/hslec_b31.h5", engine = "h5py")
    assert( inside_wl( 0.0, 0.0, x1, y1, x2, y2 ) is True )
    assert( inside_wl( 0.0, 200.0, x1, y1, x2, y2 ) is False )
    assert( inside_wl( 500.0, 0.0, x1, y1, x2, y2 ) is False )




if __name__ == "__main__" :
    display = True
    meshFile, ref =  rf"{msh.TEST_DATA:}/b31.hst" , 110
    meshFile, ref=  rf"{msh.TEST_DATA:}/hawai_eq_ref.hst", 88
    test_isInWaterPlane()
    test_waterline(meshFile=meshFile, display = display, ref = ref)
    
    

    


