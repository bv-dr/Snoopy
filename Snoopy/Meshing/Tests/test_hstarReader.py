import os
from Snoopy import Meshing as msh

testMeshes = [
                (f"{msh.TEST_DATA:}/TwoPanels_Type1.hst", [ 1,1,0,0 ] ) ,
                (f"{msh.TEST_DATA:}/TwoPanels_Type0.hst", [ 1,1,0,0 ] ) ,
                (f"{msh.TEST_DATA:}/B31.hst", [ 376*2, 392*2,0,0 ] ) ,
                (f"{msh.TEST_DATA:}/TwoPanels_Type0_NOHEADER.hst", [ 2, 0,0,0 ] ) ,
                (f"{msh.TEST_DATA:}/B31_NOHEADER.hst", [ 376*2, 0,0,0 ] ) ,
                (f"{msh.TEST_DATA:}/tank.hst", [ 526*2, 0,364*2,364*2 ] ) ,
              ]

import pytest
@pytest.mark.parametrize( "meshFile, panelPerPart",  testMeshes )
def test_reader( meshFile , panelPerPart ) :
    """ Read mesh and check number of panels per part (wetteed and above water only for now)
    """
    print (meshFile)
    absPathMesh =  (os.path.abspath(os.path.join(os.path.dirname(__file__), meshFile)))
    mesh = msh.HydroStarMesh(  absPathMesh , 0 )
    print (mesh)
    assert(  mesh.getUnderWaterHullMesh(0).getNPanels() == panelPerPart[0] )
    assert(  mesh.getAboveWaterHullMesh(0).getNPanels() == panelPerPart[1] )

    for i in range(mesh.getNbTank()):
        assert(  mesh.getTankMesh(i).getNPanels() == panelPerPart[ 2 + i ] )



def test_readh5( engine = "h5py" ):

    from Snoopy.Meshing.mesh_io import read_hslec_h5
    filename = rf"{msh.TEST_DATA:}/hslec_b31.h5"
    mesh = read_hslec_h5(filename , engine = engine, format ="mesh" )
    mesh.getBounds()
    print (mesh)
    assert(True)



if __name__ == "__main__" :

    import numpy as np
    for test in testMeshes :
        test_reader( test[0] , test[1]  )
    print ("test_read ok")

    test_readh5()
    print ("test_readh5 ok")

    

