#include <iostream>

#include "Meshing/Mesh.hpp"

using namespace BV::Meshing;

Mesh CreateTestMesh(int metasize, bool tris_only = true)
{
    Eigen::MatrixXd V(9,3);             // Vertices
    V(0, 0) = 0.;   V(0, 1) = 0.;   V(0, 2) = 0.;
    V(1, 0) = 1.;   V(1, 1) = 0.;   V(1, 2) = 0.;
    V(2, 0) = 2.;   V(2, 1) = 0.;   V(2, 2) = 0.;
    V(3, 0) = 0.;   V(3, 1) = 1.;   V(3, 2) = 1.;
    V(4, 0) = 1.;   V(4, 1) = 1.;   V(4, 2) = 1.;
    V(5, 0) = 2.;   V(5, 1) = 1.;   V(5, 2) = 1.;
    V(6, 0) = 0.;   V(6, 1) = 3.;   V(6, 2) = 2.;
    V(7, 0) = 1.;   V(7, 1) = 2.5;  V(7, 2) = 2.;
    V(8, 0) = 2.;   V(8, 1) = 3.;   V(8, 2) = 2.;
    BV::Tools::MatrixX3u F(8,3);        // Triangular panels
    F(0, 0) = 0;    F(0, 1) = 1;    F(0, 2) = 3;
    F(1, 0) = 1;    F(1, 1) = 2;    F(1, 2) = 5;
    F(2, 0) = 1;    F(2, 1) = 4;    F(2, 2) = 3;
    F(3, 0) = 1;    F(3, 1) = 5;    F(3, 2) = 4;
    F(4, 0) = 3;    F(4, 1) = 7;    F(4, 2) = 6;
    F(5, 0) = 3;    F(5, 1) = 4;    F(5, 2) = 7;
    F(6, 0) = 4;    F(6, 1) = 5;    F(6, 2) = 7;
    F(7, 0) = 5;    F(7, 1) = 8;    F(7, 2) = 7;
    int ms(metasize < 0 ? 4 : metasize);
    std::vector<PanelMetaData> panelsMeta(ms);
    Eigen::ArrayXXd panelsData(tris_only ? 8 : 10, 4);
    panelsMeta[0] = PanelMetaData("pD1", 0, 1., 0.1);
    panelsMeta[1] = PanelMetaData("pD2", 1, 2., 0.2);
    panelsMeta[2] = PanelMetaData("pD3");
    if (tris_only)
    {
       /* 
        * C-tor used:
        * Mesh(const Eigen::MatrixX3d & V,
        *      const BV::Tools::MatrixXu & F,
        *      const SymmetryTypes & symType = NONE,
        *      const unsigned int & nPtsPerDirection = 1,
        *      bool keepSym = false,
        *      const Eigen::ArrayXXd & panelsData = Eigen::ArrayXXd(0, 0),
        *      const std::vector<PanelMetaData> & panelsMeta = {}) ;
        */
        return Mesh(V, F, NONE, 1, false, panelsData, panelsMeta);
    }
   /* 
    * C-tor used:
    * Mesh(const Eigen::MatrixX3d & V,
    *      const BV::Tools::MatrixX3u & T,
    *      const BV::Tools::MatrixX4u & Q,
    *      const SymmetryTypes & symType = NONE,
    *      const unsigned int & nPtsPerDirection = 1,
    *      bool keepSym = false,
    *      const Eigen::ArrayXXd & panelsData = Eigen::ArrayXXd(0,0),
    *      const std::vector<PanelMetaData>& panelsMeta = {}) ;
    */
    BV::Tools::MatrixX4u Q(2,4);        // Quadrangular panels
    Q(0, 0) = 0;    Q(0, 1) = 3;    Q(0, 2) = 5;    Q(0, 3) = 2;
    Q(1, 0) = 3;    Q(1, 1) = 6;    Q(1, 2) = 8;    Q(1, 3) = 5;

    return Mesh(V, F, Q, NONE, 1, false, panelsData, panelsMeta);
}

void test01(int metasize, bool tris_only)
{
    if (tris_only)
        std::cout << "Test 01: Generating an object Mesh using triangle panels\n";
    else
        std::cout << "Test 01: Generating an object Mesh using triangle and quad panels\n";

    Mesh mesh = CreateTestMesh(metasize, tris_only);

    auto pD = mesh.getPanelsData();
    auto pM = mesh.getPanelsMeta();
    std::cout << "pD has dimensions: " << pD.rows() << " x " << pD.cols() << "\n";
    std::cout << "pM has size:       " << pM.size() << "\n";
    for (size_t i = 0; i < pM.size(); i++)
    {
        std::cout << "\t[" << i << "]:\n"
            << "\t\tname = " << pM[i].getName() << "\n"
            << "\t\ttype = " << pM[i].getType() << "\n"
            << "\t\tfreq = " << pM[i].getFreq() << "\n"
            << "\t\thead = " << pM[i].getHead() << "\n"
            ;
    }

    std::vector<std::string> data_names = mesh.getDataNames();
    Eigen::ArrayXi data_types = mesh.getDataTypes();
    Eigen::ArrayXd data_freqs = mesh.getDataFreqs();
    Eigen::ArrayXd data_heads = mesh.getDataHeads();
    for (size_t i = 0; i < data_names.size(); i++)
    {
        std::cout << "---------------\n";
        std::cout << "[" << i << "].name = " << data_names[i] << "\n";
        std::cout << "[" << i << "].type = " << data_types[i] << "\n";
        std::cout << "[" << i << "].freq = " << data_freqs[i] << "\n";
        std::cout << "[" << i << "].head = " << data_heads[i] << "\n";
    }
    std::cout << "Modification metadata names only\n";
    std::vector<std::string> new_names(metasize +1);
    Eigen::ArrayXi new_types(metasize +2);
    Eigen::ArrayXd new_freqs(metasize +2);
    Eigen::ArrayXd new_heads(metasize +2);
    for (size_t i = 0; i < static_cast<size_t>(metasize) +1; i ++)
    {
        new_names[i] = "New_name_" +std::to_string(i+11);
    }
    for (size_t i = 0; i < static_cast<size_t>(metasize) +2; i ++)
    {
        new_types[i] = i +11;
        new_freqs[i] = i*0.01 +1;
        new_heads[i] = i*20;
    }
    mesh.setDataNames(new_names);
    data_names = mesh.getDataNames();
    data_types = mesh.getDataTypes();
    data_freqs = mesh.getDataFreqs();
    data_heads = mesh.getDataHeads();
    for (size_t i = 0; i < data_names.size(); i++)
    {
        std::cout << "---------------\n";
        std::cout << "[" << i << "].name = " << data_names[i] << "\n";
        std::cout << "[" << i << "].type = " << data_types[i] << "\n";
        std::cout << "[" << i << "].freq = " << data_freqs[i] << "\n";
        std::cout << "[" << i << "].head = " << data_heads[i] << "\n";
    }
    std::cout << "Modification metadata types, freqs and heads. Size of each field is 1 bigger than for names\n";
    mesh.setDataTypes(new_types);
    mesh.setDataFreqs(new_freqs);
    mesh.setDataHeads(new_heads);
    data_names = mesh.getDataNames();
    data_types = mesh.getDataTypes();
    data_freqs = mesh.getDataFreqs();
    data_heads = mesh.getDataHeads();
    for (size_t i = 0; i < data_names.size(); i++)
    {
        std::cout << "---------------\n";
        std::cout << "[" << i << "].name = " << data_names[i] << "\n";
        std::cout << "[" << i << "].type = " << data_types[i] << "\n";
        std::cout << "[" << i << "].freq = " << data_freqs[i] << "\n";
        std::cout << "[" << i << "].head = " << data_heads[i] << "\n";
    }
    std::cout << "*****************************************************\n";
}

int main(int argv, char** argc)
{
    std::cout << "Snoopy.Meshing tests\n";

    bool tris_only(false);
    std::cout << "panelsMeta size > panelsData cols\n";
    test01(6, tris_only);
    std::cout << "panelsMeta size < panelsData cols\n";
    test01(3, tris_only);

    return 0;
}
