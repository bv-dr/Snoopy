# based on boostOneAPI.py
import sys
import os

def patch_msvc(fname_in, fname_out):

    line_tmp = "            if [ MATCH \"(14.4)\" : $(version) ]\n" \
             + "            {\n" \
             + "                if $(.debug-configuration)\n"   \
             + "                {\n"    \
             + "                    ECHO \"notice: [generate-setup-cmd] $(version) is 14.4\" ;\n"   \
             + "                }\n"    \
             + "                parent = [ path.native [ path.join  $(parent) \""+r"..\\..\\..\\..\\..\\Auxiliary\\Build" +"\" ] ] ;\n"    \
             + "            }\n"    \
             + "            else "
    search_line = "if [ MATCH \"(14.3)\" : $(version) ]"
    with open(fname_in, "r") as fIn:
        lines = fIn.readlines()
    with open(fname_out, "w") as fOut:
        for iline, line in enumerate(lines):
            if search_line in line and "else" not in line:
                line = line_tmp +search_line +"\n"

            fOut.write(line)

    print ("msvc.jam file patched by Snoopy cmake" )


if __name__ == "__main__":

    boost_path = sys.argv[1]
    fname = os.path.join(boost_path, "tools", "build", "src", "tools", "msvc.jam")
    patch_msvc(fname, fname)
