import sys
import os

def patch_win_jam(fname_in, fname_out):

    with open(fname_in, "r") as fIn:
        lines = fIn.readlines()
    with open(fname_out, "w") as fOut:
        for iline, line in enumerate(lines):
            if line.strip().startswith('setup_bat = "iclvars.bat" ;'):
                line = line.replace('setup_bat = "iclvars.bat" ;',
                                    'setup_bat = "../../env/vars.bat" ;')

            # For oneAPI 2022.
            if line.strip().startswith('setup_bat = "setvars.bat" ;'):
                line = line.replace('setup_bat = "setvars.bat" ;',
                                    'setup_bat = "../../../../setvars.bat" ;')


            # For VS2022, not sure whether it this is necessary
            if "supported-vcs" in line and "14.3" not in line :
                line = line.replace( "14.2 14.1" , "14.3 14.2 14.1")

            if ".iclvars-version-alias-vc14.2 = vs2019" in line :
                if "vs2022" not in lines[ iline-1 ]:
                    line = ".iclvars-version-alias-vc14.3 = vs2022 ;\n" + line

            fOut.write(line)

    print ("Intel-win.jam file patched by Snoopy cmake" )


if __name__ == "__main__":

    boost_path = sys.argv[1]
    fname = os.path.join(boost_path, "tools", "build", "src", "tools", "intel-win.jam")
    patch_win_jam(fname, fname)
