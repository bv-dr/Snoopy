import pandas as pd
import numpy as np
from Snoopy import Statistics as st

def test_get_compacted(  ):
    
    df = pd.DataFrame(index = [1,2,3,4,5] , data = {"val" : [1,1,1,2,2],"val2" : [3.,1.,1.,2.,2.1], "PROB" : [1/5,1/5,1/5,1/5,1/5] }  )
    probabilty_column = "PROB"
    df_compact = st.compact_probability_df(df, probabilty_column, rounding_dict = {"val2":0})

    assert( not df_compact.duplicated().any() )
    assert( np.sum(df.PROB) == np.sum(df_compact.PROB) )



if __name__ == "__main__" : 
     
    test_get_compacted()
