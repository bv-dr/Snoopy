import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from Snoopy.TimeDomain import TEST_DIR
from Snoopy import Statistics as st

def test_pot( display = False ):
    
    data = pd.read_csv( f"{TEST_DIR}/hs.csv", index_col = 0 , parse_dates = True ).hs
    duration = len(data) / 2922

    pot = st.POT_GPD.FromTimeSeries( se = data, duration=duration, threshold_q = 0.95, window = pd.offsets.Day(2), solver = "minimize_mle" )

    x5 = pot.rp_to_x( 5.0 )
    
    pot.x_to_rp( 11. )
    
    x5_emp = pot.rp_to_x_empirical( 5. )

    # Roughly check that the fitted results are not completely out of the interpolated value.
    assert(np.isclose(x5, x5_emp, rtol = 0.05))

    # For now just check that code runs...    
    decluster_data = pot._sample
    mr = st.MeanResidualLife( decluster_data )
    t = st.ThresholdSensitivity( sample = decluster_data, duration=duration, threshold_range = np.arange(6.0 , 14. , 0.1) )

    if display:
        mr.plot()
        fig, ax = plt.subplots()
        pot.plot_rp_data(ax=ax, label = "data")
        pot.plot_rp_data_bulk(ax=ax, label = "data bulk")
        pot.plot_rp_fit(ax=ax, label = "GPD")
        pot.plot_rp_ci(ax=ax, ci_level=0.95, ci_type = "delta",  color = "blue")
        pot.plot_rp_ci(ax=ax, ci_level=0.95, ci_type = "bootstrap",  color = "red")
        t.plot()
    


def perf_pot():
    """Test POT performance"""
    from timeit import timeit
    data = pd.read_csv( f"{TEST_DIR}/hs.csv", index_col = 0 , parse_dates = True ).hs
    duration = len(data) / 2922
    data_dec = st.rolling_declustering( data, window_int = 48 ).values
    
    
    n = 1000    
    t = timeit( "st.rolling_declustering( data, window_int = 48 )", globals=locals()|globals() , number = n) / n
    print(t * 1000, "ms for declustering" )
    
    threshold = np.quantile( data_dec , 0.95 )

    pot = st.POT_GPD( data_dec, duration=duration, threshold = threshold, solver = 'minimize_mle' )
    pot._fit()
    
    t = timeit( "threshold = np.quantile( data_dec , 0.95 );pot = st.POT_GPD( data_dec, duration=duration, threshold = threshold, solver = 'minimize_mle' );pot._fit()", globals=locals()|globals()  , number = n) / n
    print(t * 1000, "ms for mle fit with minimize_mle")
    
    t = timeit( "threshold = np.quantile( data_dec , 0.95 );pot = st.POT_GPD( data_dec, duration=duration, threshold = threshold, solver = 'genpareto.fit' );pot._fit()", globals=locals()|globals()  , number = n) / n
    print(t * 1000, "ms for mle fit with scipy.genpareto.fit")
    
    t = timeit( "threshold = np.quantile( data_dec , 0.95 );pot = st.POT_GPD( data_dec, duration=duration, threshold = threshold, solver = 'mom' );pot._fit()", globals=locals()|globals()  , number = n) / n
    print(t * 1000, "ms for mle fit with mom")
    

if __name__ == "__main__" :

    test_pot( display = True )
    perf_pot()
