import numpy as np
from Snoopy.Statistics import LongTermSpectral, LongTermGen, Powern
from Snoopy import Spectral as sp
from Snoopy import logger
from Snoopy.Statistics._impact_velocity import LongTermImpactSL, LongTermImpact
from Snoopy.Statistics import LongTermRao
from Snoopy.Dataset import rec34_SD
from matplotlib import pyplot as plt

logger.setLevel(20)



def test_long_term_impact():
    
    # RWE Rao, from HydroStar
    rwe_rao = sp.Rao( f"{sp.TEST_DATA:}/rao/RWE_175m.rao" ).getRaoForSpectral( wmin=0.2, wmax = 1.8, dw = 0.005 )
    
    ssList = rec34_SD.to_seastate_list( headingList = 12, gamma  = 1.5 , spreadingType=sp.SpreadingType.Cosn, spreadingValue=3.0)
    
    vrwe_rao = rwe_rao.getDerivate()

    lt_v = LongTermRao( rao = vrwe_rao,ssList = ssList, dss = 10800 )
    lt_vslam0 = LongTermImpact( rwe_rao = rwe_rao, ssList = ssList, z = 0.0 , dss = 10800 )
    lt_vslam2 = LongTermImpact( rwe_rao = rwe_rao, ssList = ssList, z = 10.0 , dss = 10800 )
    
    
    fig, ax = plt.subplots()
    lt_v.longTermSingle().plot_rp(rp_range = np.linspace(0.1,100,100), ax=ax, label = "Relative velocity")
    lt_vslam0.longTermSingle().plot_rp(rp_range =  np.linspace(0.1,100,100) , ax=ax, label = "Relative velocity, conditioned to point emergence 0")
    lt_vslam2.longTermSingle().plot_rp(rp_range =  np.linspace(0.1,100,100) , ax=ax, label = "Relative velocity, conditioned to point emergence 10")
    ax.set(ylabel="Long-term velocity (m/s)")
    ax.legend(loc = "upper left");
    
    
    

def test_impact_sl():


    ssList = rec34_SD.to_seastate_list( headingList = 12, gamma  = 1.5 , spreadingType=sp.SpreadingType.No, spreadingValue=3.0)

    roll_rao = sp.Rao( f"{sp.TEST_DATA:}/rao/Impact_vel_Roll.rao" ) * np.pi / 180.
    rwe_rao = sp.Rao( f"{sp.TEST_DATA:}/rao/Impact_vel_RWE.rao" )



    logger.info( "TEST START" )
    lt = LongTermImpactSL( ssList, rao_sl = roll_rao , rwe_rao = rwe_rao , z = 1.088 , bLin = 15548000.0*10, bQuad = 194570000.0  )

    a = lt.rp_to_x( 25.0 )
    b = lt.longTermSingle().nExceed( x = 17.65, duration = 25. ).sum()


    logger.info( "TEST STOP" )


    print (a , b)

    from matplotlib import pyplot as plt
    fig, ax  = plt.subplots()
    roll_rao.plot(imode = 1, headingsDeg=[90], ax=ax)
    roll_rao.plot(imode = 3, headingsDeg=[90], ax=ax)



if __name__ == "__main__" :
    
    test_long_term_impact()
    # test_impact_sl()
    
    
