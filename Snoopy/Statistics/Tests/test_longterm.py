import numpy as np
from Snoopy.Statistics import LongTermSpectral, LongTermGen, Powern
from Snoopy import Spectral as sp
from Snoopy import logger
from Snoopy.Statistics._longTerm import squashSpectralResponseList, LongTermGen

logger.setLevel(20)


def test_longterm():
    """ Basic test for long term calculation
    1000 years on the same sea-state should provide same results by short-term or long-term analysis
    """
    rs = 1.0
    tz = 10.0
    n = 1000000

    spStats = sp.SpectralStats.FromRsRtz(rs, tz)

    rsList = np.ones( (n) ) * rs
    rtzList = np.ones( (n) ) * tz

    spStats_v = sp.SpectralStats.FromRsRtz(rsList, rtzList)

    x_sht = spStats.risk_to_x(0.5, 25*365.24*24*3600)

    for dss in ["INDEP" , "LARGE_T" , 10800.] :
        dryrun = LongTermSpectral( rsList , rtzList , probabilityList="iso" , dss = dss, engine="numba" )
        dryrun.p_to_x(0.5, 25)


    # TODO : does not pass with engine = "cpp_p" and rp = 5 (while 25 is ok...)
    for dss in ["INDEP" , "LARGE_T" , 10800.] :
        for engine in ["python" , "cpp" , "numba", "cpp_p"] :
            logger.info( f"{engine:}_{dss:} START")
            lt = LongTermSpectral( rsList , rtzList , probabilityList="iso" , dss = dss, engine=engine, numThreads=20 )
            x_lt = lt.p_to_x(0.5, 25)
            logger.info( f"{engine:}_{dss:} STOP")
            print (x_lt / x_sht)
            assert(np.isclose( x_lt , x_sht , rtol = 1e-4))

            # Back on forth x <-> rp
            assert(   np.isclose(lt.x_to_rp(  lt.rp_to_x( 25 ) ) , 25, rtol = 1e-5)  )


    # Test with squashed list
    rsList[2] = 1.0001
    rsList_opt , rtzList_opt , probabilityList_opt = squashSpectralResponseList( rsList , rtzList , np.ones( (len(rtzList)), dtype = float ) / len(rtzList) )
    dss  = 10800.
    engine = "cpp"
    logger.info( f"{engine:}_{dss:} START")
    lt = LongTermSpectral( rsList_opt , rtzList_opt , probabilityList=probabilityList_opt , dss = dss, engine=engine, numThreads=20 )
    x_lt = lt.p_to_x(0.5, 25)
    logger.info( f"{engine:}_{dss:} STOP")
    print (x_lt / x_sht)
    assert(np.isclose( x_lt , x_sht , rtol = 1e-4))

    # Test with LongTermGen (must output exactly sampe result using p_sht = p_cycle**(dss/rtz)
    lt_gen = LongTermGen(  spStats_v.getShtMaxDistribution(dss) , probabilityList = np.ones( (n) )/n , dss = dss)
    x_lt_gen = lt_gen.p_to_x(0.5, 25)
    assert(np.isclose( x_lt_gen , x_sht , rtol = 1e-4))



def test_longterm_inv_cpp():

    rs = 1.0
    tz = 10.0
    n = 10000
    nrao = 100

    rsList = np.ones( (n,nrao) ) * rs
    rtzList = np.ones( (n,nrao) ) * tz

    x = 0.5
    sp.SpectralStats.FromRsRtz(rs, tz).rayleigh.cdf(x)
    1-np.exp( -8*x**2 / (rs/1.001)**2 )

    x_sht = sp.SpectralStats.FromRsRtz(rs, tz).risk_to_x(0.5, 25*365.24*24*3600)

    lt_vect = LongTermSpectral( rsList , rtzList , probabilityList="iso" , dss = 10800., engine="cpp" )

    logger.info("lt sequential START")
    for i in np.arange( nrao ):
        lt = LongTermSpectral( rsList[:,i] , rtzList[:,i] , probabilityList="iso" , dss = 10800., engine="cpp_p", numThreads=10 )
        lt.p_to_x(0.5, 25)
    logger.info("lt sequential STOP")

    logger.info("lt parallel 1 START")
    x_lt_vect = lt_vect.p_to_x_parallel(0.5, 25, numThreads = 1)
    logger.info("lt parallel 1 STOP")
    assert(np.isclose( x_lt_vect , x_sht , rtol = 1e-3).all())

    logger.info("lt parallel 10 START")
    x_lt_vect = lt_vect.p_to_x_parallel(0.5, 25, numThreads = 10)
    logger.info("lt parallel 10 STOP")

    assert(np.isclose( x_lt_vect , x_sht , rtol = 1e-3).all())

    print ( "test_longterm_inv_cpp ok" )


def test_longTerm_prob():

    rs = 1.0
    tz = 10.0
    n = 1000

    spStats = sp.SpectralStats.FromRsRtz(rs, tz)

    rsList = np.ones( (n) ) * rs
    rtzList = np.ones( (n) ) * tz

    lt = LongTermSpectral( rsList , rtzList , probabilityList="iso" , dss = "LARGE_T")

    x = 0.5
    sht_ = spStats.rayleigh.sf( x )
    lt_ = lt.x_to_pcycle( x )
    print (sht_ , lt_)

    assert( np.isclose( lt_ , sht_ , rtol = 1e-5))

    assert( np.isclose( lt.pcycle_to_x( lt_ ) , x , rtol = 1e-5))


    # Should be closed, although sctrictly speeking, using BV definition of RP, it is not exactly the same
    assert( np.isclose( lt.nExceed( lt.rp_to_x(25) , 25  ).sum()  , 1.0, rtol = 0.001 ))


def test_LongTermGen():

    rsList = np.linspace(5, 20 , 50)
    rtzList = np.linspace(1, 12 , 50)

    spStats_v = sp.SpectralStats.FromRsRtz(rsList, rtzList)

    spStats_l = [ sp.SpectralStats.FromRsRtz(rs, rtz) for rs, rtz in zip(rsList, rtzList) ]

    lt = LongTermSpectral( rsList , rtzList , probabilityList="iso" , dss = 10800., engine="cpp")

    dist_3h_v = spStats_v.getShtMaxDistribution(duration = 10800)
    ltGen_v = LongTermGen( dist_3h_v, probabilityList=lt.probabilityList , dss = 10800 )

    dist_3h_l = [ Powern( s.rayleigh, 10800. / s.Rtz) for s in spStats_l  ]
    ltGen_l = LongTermGen( dist_3h_l, probabilityList=lt.probabilityList , dss = 10800 )

    x_lt = lt.rp_to_x(25)
    x_lt_gen_v = ltGen_v.rp_to_x(25)
    x_lt_gen_l = ltGen_l.rp_to_x(25)

    assert( np.isclose( x_lt , x_lt_gen_v ) )
    assert( np.isclose( x_lt , x_lt_gen_l ) )




if __name__ == "__main__" :
    test_longterm()
    test_longterm_inv_cpp()
    test_longTerm_prob()
    test_LongTermGen()

