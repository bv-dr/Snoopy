import numpy as np
from Snoopy.Statistics import DiscreteSD
from Snoopy.Dataset import rec34_SD

def test_aggregate():

    sd = DiscreteSD(rec34_SD)

    sd_count = (10000000*sd).getCountScatterDiagram( )

    sd_l = sd_count.getAggregated( np.arange(0,18,1), axis = 0 ).getAggregated( sd.get_v2_edges() , axis = 1 )
    assert((sd_l == sd_count).all().all())

    # Case where new edge coincide with original center
    sd_l2 = sd_count.getAggregated( np.arange(-0.5, 18.5,1), axis = 0, eps = 0.001 )
    assert(True)



def test_iso_probability():
    from Snoopy.Math import df_interpolate
    sd = rec34_SD
    iso_level = 4.0
    hs, tz = sd.iso_value(iso_level)

    for h, t in zip(hs[:-1] , tz[:-1]) : 
        interp_val = df_interpolate( df_interpolate( sd, newIndex=[ h ] , k = 1 )  , newColumns= [t ], axis = 1, k = 1 ).iloc[0,0]
        assert(  np.isclose( interp_val , iso_level)  )
    


if __name__ == "__main__" :

    test_aggregate()
    test_iso_probability()