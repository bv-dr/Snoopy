import numpy as np
import pytest
from Snoopy import Statistics as st


test_n3 = ( [ 1.00000000e+00, 1.58148123e-02, 4.14988027e-04] ,
            [ -0.6,  30, 0.  ]  ,
            "hybr")


#Test case for which the solver does not find solution
test_n5 = ( [-1.1822804, 33.42455056,   0.   , 0.   ,   0.   ],
            [1.0e+00, 1.49590643e-02,   5.26368044e-04, 2.59096230e-05,   1.54787176e-06],
            "hybr")

test_n4 = ( [ 1-1.47609036e+00, -7.57059444e+01, +2.04035948e+03, +1.91729074e+01 ],
            [ 1.00000000e+00, 1.58148123e-02, 4.14988027e-04, 1.66478346e-05 ] ,
            "hybr")


@pytest.mark.parametrize("moment, startingValue, method", [ test_n3 ])
def test_maxEntropySolver( moment, startingValue, method  ):

    l0 = np.array(startingValue)
    mu_ = np.array(moment)

    solver = st.MaxEntropySolver( mu = mu_ , x = np.linspace(-0.7 , +0.7, 5000) )

    l = solver.solve( l0 , method = method )
    # print (l)
    # print (solver.gn(l0) / solver.mu)
    relErr = solver.gn(l) / solver.mu
    print (relErr)

    assert( np.isclose(relErr , 1).all())
    print (len(moment) , "Ok")

    # test = solver.getDistribution()
    # a = np.linspace(-0.3, 0.3, 100)
    # plt.plot( a,   test.pdf(  a ) )


if __name__ == "__main__" :

    from matplotlib import pyplot as plt
    test_maxEntropySolver( *test_n3 )
