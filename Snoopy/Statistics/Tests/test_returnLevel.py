import numpy as np
from Snoopy.Statistics import ReturnLevel, Powern
from Snoopy.Statistics.returnLevel import xrp_pdf
from scipy import stats
from matplotlib import pyplot as plt


def check_beta( dist, T_block, rp, alpha) :
    """Compare numerical and analytical empirical return value distribution.
    """

    import seaborn as sns

    n = int(rp*alpha/T_block)

    res_num = []
    for i in range(5000) :
        data = dist.rvs(n)
        rl = ReturnLevel(data, duration = rp*alpha)
        res_num.append( rl.rp_to_x( rp ) )

    res_num = np.array(res_num)

    xrange = np.linspace( np.min(res_num) , np.max(res_num) , 200)
    x_ci_beta = xrp_pdf( xrange , rp = rp , alpha = alpha , T_block=T_block , dist = dist )

    fig, ax = plt.subplots()
    sns.histplot( res_num, ax = ax, stat ="density", label = "Numerical re-sample")
    ax.plot(xrange, x_ci_beta, linewidth = 4, label = "Beta distribution")
    ax.legend()


def test_return_level(display = False):

    rtz = 10.0
    r = stats.rayleigh(1.0)
    rp = 500

    rl_v = ReturnLevel.rp_to_x_distribution( distribution = r, blockSize=rtz, rp = rp)
    pn_v = Powern(r, rp / 10.0).ppf(1/np.exp(1))
    assert( np.isclose( rl_v , pn_v ) )
    assert(  np.isclose( ReturnLevel.x_to_rp_distribution(r , rtz, rl_v) , rp))

    n = 10000
    data = r.rvs( n, random_state=15 )

    rl = ReturnLevel(data, duration = rtz*n)

    assert( np.isclose( rl.rp_to_x( rp ) , rl_v, rtol = 0.05)) # Tolerance to account for sampling error
    assert( np.isclose( rl.x_to_rp( rl_v ) , rp, rtol = 0.1)) # Tolerance to account for sampling error

    x_range = np.linspace( min(data) , max(data) , 1000 )

    # Check interpolation (spurious results with spline interpolation k=3 would appear here )
    assert( (np.diff(rl.x_to_rp(x_range)) > 0.0).all() )

    if display :
        rp_range = np.logspace( 1.01*np.log10(rl.empirical_rp.min()) , 0.99*np.log10( rl.empirical_rp.max() ) , 1000)
        fig, ax = plt.subplots()
        rl.plot(ax=ax, label = "data")
        rl.plot_ci(ax=ax, alpha_ci = 0.05, label = "CI")
        rl.plot_distribution( distribution=r, blockSize=rtz, rp_range = rp_range , ax=ax,  label = "underlying distribution")
        ax.plot( rp_range, rl.rp_to_x( rp_range ), label = "Interpolated empirical 1"  )
        ax.plot( rl.x_to_rp(x_range), x_range, label = "Interpolated empirical 2"  )
        ax.legend()





def check_error_estimate():
    dist = stats.genextreme(c = 0.0)
    rp = 3600.
    alpha = 50.
    T_block = 10

    N = 1000
    x_rp_estimate = []
    error_estimate = []
    for n in range(N) :
        data = dist.rvs(size = int(rp*alpha / T_block))
        rl = ReturnLevel( data , duration = rp*alpha )
        err_rel = rl.rp_to_xerror( rp = rp,  ci_type = "beta" )
        x_rp = rl.rp_to_x(rp)
    
        x_rp_estimate.append(x_rp)
        error_estimate.append(err_rel)
        
    x_rp_estimate = np.array(x_rp_estimate)
    error_estimate = np.array(error_estimate)
    
    x_rp = ReturnLevel.rp_to_x_distribution( dist , blockSize=T_block , rp = rp )
    
    true_error = (np.mean( (x_rp_estimate / x_rp - 1)**2))**0.5
    
    np.mean(error_estimate)
        
    error_of_error = np.mean((error_estimate / true_error -1)**2)**0.5
    
    import seaborn as sns
    fig, ax = plt.subplots()
    sns.histplot( error_estimate , ax=ax)
    ax.axvline( x = true_error )
    


if __name__ == "__main__":
    

    dist = stats.genextreme(c = 0.0)
    rp = 3600.
    alpha = 50.
    T_block = 10

    check_beta(dist, T_block, rp, alpha)
    test_return_level(display = True)

    
    
    
