import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from datetime import timedelta
from Snoopy import Statistics as st


def test_bm(display = False, time_type = "float"):
    from Snoopy.TimeDomain import TEST_DIR
    
    data = pd.read_csv( f"{TEST_DIR:}/hs.csv", index_col = 0 , parse_dates = True ).hs
       
    if time_type == "timedelta" : 
        #Example 1 : bm_gev + TS with time index by 3h
        bm = st.BM_GEV.FromTimeSeries( se = data, block_size = timedelta(days=365.24), time_convert = timedelta(days=365.24) )
        #block_size --> en timedelta
        bm._time_label = "years"
        
    elif time_type == "float" : 
        #Example 2 : bm_gev + TS with float index (here 1 float = 3 hours)
        data_ = data.copy(deep = True)
        data_.index = np.linspace(0, (data_.index[-1] - data_.index[0]).total_seconds()/3600/3, data_.index.size )
        
        bm = st.BM_GEV.FromTimeSeries( se = data_, block_size = 365.24*24/3, time_convert = 365*24/3,
                                       fit_kwargs = {"bounds" : [ (0.1, 0.5), (None, None), (None, None) ] })
        bm._time_label = "years"
        
        b_sens = st._blockMaxima.BlockSizeSensitivity( se = data_, block_size_range = np.arange( 50*24/3 , 300*24/3  , 50) , time_convert = 365*24/3 )

    # Both RP definition should provide close results at large RP / block_size
    rp = 50
    assert( np.isclose( bm.rp_to_x( rp, formulation = 1) , bm.rp_to_x( rp, formulation = 2) , rtol = 0.001 ) )
    
    # ... but not when RP / block_size is close to 1.
    rp = 1.05
    assert( not np.isclose( bm.rp_to_x( rp, formulation = 1) , bm.rp_to_x( rp, formulation = 2) , rtol = 0.001 ) )

    # Roughly check that the fitted results are not completely out of the interpolated value.
    x5 = bm.rp_to_x(5)
    x5_emp = bm.rp_to_x_empirical(5)
       
    assert( np.isclose( x5, x5_emp , rtol = 0.04) )
    
    print(bm)
    
    if display : 
        fig, ax = plt.subplots()
        bm.plot_rp_data(ax=ax)
        bm.plot_rp_fit(ax=ax)
        bm.plot_rp_ci(ax=ax, ci_level=0.9, ci_type = "bootstrap", color = "blue", alpha = 0.5)
        bm.plot_rp_ci(ax=ax, ci_level=0.9, ci_type = "delta", color = "orange", alpha = 0.5)
        ax.set_ylim([5, 22])
        ax.set_xlim([None, 100])

        if time_type == "float":
            b_sens.plot(ci_level=0.95, rps = [25.])
    

if __name__ == "__main__" : 

    test_bm(display = True, time_type="float")
    # test_bm(display = True, time_type="timedelta")

