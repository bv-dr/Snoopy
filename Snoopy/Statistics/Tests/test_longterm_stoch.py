import numpy as np
import pandas as pd
from Snoopy import Spectral as sp
from Snoopy import Statistics as st



def test_stochlin_long_term_starspec():

    raos = sp.Rao( f"{sp.TEST_DATA:}/rao/roll_multi.rao" ).getRaoForSpectral(wmin = 0.05, wmax = 3.5 , dw = 0.005, db_deg = 5)

    #In Snoopy, roll RAO in radians by default !
    raos /= (180 / np.pi)

    #Read starspec res
    ref = pd.read_csv( f"{sp.TEST_DATA:}/hspec_reference/stochLin_spec/report_stochLin.csv" , delimiter = ";" )

    hsList = np.arange( 2 , 10 , 2 )

    ssList = [sp.SeaState.Jonswap( hs , 10.0 , 1.0  , np.pi / 2 ) for hs in hsList]

    lt_stoch = st._longTermSD.LongTermQuadDamping(ssList, raos, [raos], bLin = 2e+08 , bQuad = 2.0e10 , dss = "INDEP")

    snoopy_res = lt_stoch.rp_to_x(25)[0] *  180. / np.pi

    starspec_res = ref.iloc[0,:].loc[ "Long term value (double amplitude) at  25.00 years" ] / 2

    assert( np.isclose( snoopy_res, starspec_res , rtol = 1e-3 ) )

    print ( f"{snoopy_res:} {starspec_res:}  {snoopy_res / starspec_res -1 :.2%}")



if __name__ == "__main__" :

    test_stochlin_long_term_starspec()