import numpy as np
from scipy.stats import genextreme, weibull_min , rayleigh, pearson3, gengamma, genpareto
from Snoopy import Statistics as st
from Snoopy import logger
from Snoopy.Statistics.distribution_jitted import weibull_min_p
import pytest


"""
Compare C++ to scipy implementation
"""

logger.setLevel(10)

test_data = [  # Dist ,  ref, coef , coef_ref
           ( st.geneextreme_c, genextreme ,  [-0.01 , 2.0 , 2.0],  [-0.01 , 2.0 , 2.0]),
           ( st.weibull_min_c, weibull_min,  [2. , 1 , 2.], [2. , 1 , 2.]  ),
           ( weibull_min_p, weibull_min,  [2. , 1 , 2.], [2. , 1 , 2.]  ),
           ( st.rayleigh_c, rayleigh,  [2.] ,  [0, 2] ),
           ( st.Rayleigh_n(10), None , [10.0], None  ),
           ( st.pearson3_patched, pearson3 , [1.0 , 1.0 , 1.0], [1.0 , 1.0 , 1.0]  ),
           ( st.gengamma_patched, gengamma , [1.0 , 1.0 , 1.0, 1.0], [1.0 , 1.0 , 1.0,1.0]  )
            ]


test_data_scipy = [t for t in test_data if t[1] is not None]

def test_powern():

    sigma = 5
    n = 100
    d1 = st.Rayleigh_n(n)(sigma)
    d2 = st.Powern( st.rayleigh_c(sigma), n )

    x = d1.isf(0.6)

    assert( np.isclose( d1.pdf(x), d2.pdf(x) ) )
    assert( np.isclose( d1.cdf(x), d2.cdf(x) ) )
    assert( np.isclose( d1.isf(0.2), d2.isf(0.2) ) )


def test_MoM_GEV():
    from Snoopy.Statistics import _blockMaxima
    c = 2.51
    loc = 1.99
    scale = 0.8
    
    params = np.array( [c, loc, scale])
    
    E = genextreme.mean(c = c, scale = scale, loc = loc)
    var = genextreme.var(c = c, scale = scale)
    skew = genextreme.stats(c = c, moments = "s")
    
    c_mom = _blockMaxima.skew_to_shape(skew)
    scale_mom = _blockMaxima.var_to_scale(var, c_mom)
    loc_mom = _blockMaxima.E_to_loc(E, c_mom, scale_mom)
    
    params_mom = np.array( [c_mom, loc_mom, scale_mom])
    
    assert( np.isclose(params, params_mom, atol = 1e-3, rtol = 1e-3 ).all())


@pytest.mark.parametrize("dist, coefs",  [  [t[0] , t[2] ] for t in test_data ] )
def test_consistency(  dist, coefs ):
    """
    Quick consistency check of distribution
    """
    logger.debug("Testing " + dist.__str__())
    p = 0.2
    dist_f = dist(*coefs)

    #Back and forth
    x = dist_f.isf(p)
    assert ( np.isclose( dist_f.sf(x) ,  p ) )
    assert ( np.isclose( dist_f.cdf(dist_f.ppf(p)) ,  p ) )

    # cdf' = pdf
    from scipy.misc import derivative
    pdf_num = derivative( dist_f.cdf, x, dx = 0.0001 )
    pdf_ = dist_f.pdf(x)
    assert ( np.isclose( pdf_ ,  pdf_num ) )



@pytest.mark.parametrize("distToTest, ref, coefs, coefs_ref", test_data_scipy)
def test_scipy_compare( distToTest, ref, coefs, coefs_ref  ):

    if coefs_ref is None :
        coefs_ref = coefs
    x = 1.5

    snoopyD = distToTest(  *coefs )
    scipyD = ref( *coefs_ref)
    data = scipyD.rvs(1000)

    print ("PDF")
    t_pdf = snoopyD.pdf(x)
    r_pdf = scipyD.pdf(x)
    print (t_pdf)
    print (r_pdf)
    assert( np.isclose(t_pdf, r_pdf))


    print ("CDF")
    t_pdf = snoopyD.cdf(x)
    r_pdf = scipyD.cdf(x)
    print (t_pdf)
    print (r_pdf)
    assert( np.isclose(t_pdf, r_pdf))

    print ("PPF")
    t_pdf = snoopyD.ppf(0.1)
    r_pdf = scipyD.ppf(0.1)
    print (t_pdf)
    print (r_pdf)
    assert( np.isclose(t_pdf, r_pdf))

    print ("MEAN")
    t = distToTest.mean( *coefs)
    r = ref.mean( *coefs_ref)
    print (t)
    print (r)
    assert( np.isclose(t, r))

    print ("VAR")
    t = distToTest.var( *coefs)
    r = ref.var( *coefs_ref)
    print (t)
    print (r)
    assert( np.isclose(t, r))

    print ("MOMEMT1")
    t = snoopyD.moment(1)
    r = scipyD.moment(1)
    print (t)
    print (r)
    assert( np.isclose(t, r))

    print ("MOMEMT2")
    t = snoopyD.moment(2)
    r = scipyD.moment(2)
    print (t)
    print (r)
    assert( np.isclose(t, r))

    print ("NNLF")
    t = distToTest.nnlf( coefs, data )
    r = ref.nnlf( coefs_ref, data )
    print (t)
    print (r)
    assert( np.isclose(t, r))

    print ("Penalized NNLF")
    t = distToTest._penalized_nnlf( coefs, data )
    r = ref._penalized_nnlf( coefs_ref, data )
    print (t)
    print (r)
    assert( np.isclose(t, r))



#@pytest.mark.parametrize("distToTest, ref, coefs, coefs_ref", test_data_scipy)
def eval_perf( distToTest, ref, coefs, coefs_ref, n = 10000000 ) :

    from Pluto.System import Chrono

    if coefs_ref is None :
        coefs_ref = coefs

    print ("------------ TIMER for " + distToTest.name )

    c = Chrono(start = True)

    snoopyD = distToTest(  *coefs )

    scipyD = ref( *coefs_ref )

    c = Chrono(start = True)

    scipyD.rvs(n)
    c.printLap("SCIPY RVS")
    data = snoopyD.rvs(n)
    c.printLap("SNOOPY RVS")

    scipyD.pdf(data)
    c.printLap("SCIPY pdf")
    snoopyD.pdf(data)
    c.printLap("SNOOPY pdf")

    ref.nnlf(coefs_ref, data)
    c.printLap("SCIPY nnlf")
    distToTest.nnlf(coefs, data)
    c.printLap("SNOOPY nnlf")


    ref._penalized_nnlf(coefs_ref, data)
    c.printLap("SCIPY penalized nnlf")
    distToTest._penalized_nnlf(coefs, data)
    c.printLap("SNOOPY penalized nnlf")

    # data = snoopyD.rvs(10000)
    # rf = ref.fit(data)
    # c.printLap("SCIPY Fit")
    # tf = distToTest.fit(data, coefs[0], *coefs[1:]  )
    # c.printLap("SNOOPY Fit")
    # print (rf)
    # print (tf)


def test_genpareto():

    import timeit
    
    shape, loc , scale = -0.05 , 0.0 , 2.0
    d = genpareto( shape, loc, scale )
    
    # Penalty can be different than in scipy, but this hardly matters
    genpareto._penalized_nnlf( [shape , loc , scale] , np.array([41]) )
    st.genpareto_2p.nnlf( [shape ,  scale] , np.array([41]) )
    
    for n in [100 , 1000, 10000]:
        data = d.rvs(n)
        nnlf_scipy = genpareto.nnlf( [shape, loc , scale] , data )
        nnlf_snoopy = st.genpareto_2p.nnlf( [shape , scale] , data )
        assert(np.isclose( nnlf_scipy, nnlf_snoopy ))
        t_scipy = timeit.timeit( lambda : genpareto.nnlf( [shape, loc , scale] , data ), number = int( 100000 / n ) )
        t_snoopy = timeit.timeit( lambda : st.genpareto_2p.nnlf( [shape, scale] , data ), number = int( 100000 / n ) )
        print (f"Snoopy is {t_scipy / t_snoopy:} time faster for genpareto.nnlf, with n = {n:}")




def test_genextreme():
    import timeit
    shape, loc , scale = -0.05 , 1.0 , 2.0
    d = genextreme( shape, loc, scale )
    
    # Penalty is much larger then the scipy one
    print(genextreme._penalized_nnlf( [shape , loc , scale] , np.array([ -38.9999999999999 ]) ))
    print(st.geneextreme_c.nnlf( [shape , loc,  scale] , np.array([ -38.9999999999999]) ))

    print(genextreme._penalized_nnlf( [shape , loc , scale] , np.array([ -40.1 ]) ))
    print(st.geneextreme_c.nnlf( [shape , loc,  scale] , np.array([ -40.1]) ))
    
    for n in [100 , 1000, 10000]:
        data = d.rvs(n)
        nnlf_scipy = genextreme.nnlf( [shape, loc , scale] , data )
        nnlf_snoopy = st.geneextreme_c.nnlf( [shape ,loc, scale] , data )
        assert(np.isclose( nnlf_scipy, nnlf_snoopy ))
        t_scipy = timeit.timeit( lambda : genextreme.nnlf( [shape, loc , scale] , data ), number = int( 100000 / n ) )
        t_snoopy = timeit.timeit( lambda : st.geneextreme_c.nnlf( [shape, loc, scale] , data ), number = int( 100000 / n ) )
        
        print (f"Snoopy is {t_scipy / t_snoopy:} time faster for genpareto.nnlf, with n = {n:}")


if __name__ == "__main__" :

    for test_p in test_data :
        distToTest , ref, coefs , coefs_ref = test_p
        print (test_p[0])
        test_consistency(  dist = test_p[0], coefs = test_p[2] )

        if test_p[1] is not None :
            test_scipy_compare( distToTest , ref, coefs , coefs_ref )
            eval_perf( distToTest , ref, coefs , coefs_ref, n = 1000000 )

    test_powern()
    
    
    test_genpareto()
    
    st.omp_set_num_threads(1)
    test_genpareto()
    
    
    test_genextreme()

    test_MoM_GEV()

