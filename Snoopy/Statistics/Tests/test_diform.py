import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from Snoopy.Statistics import DirectIform, DirectIformBM
from Snoopy.Statistics import TEST_DATA
from Snoopy.Statistics._diform import direction_vector
from Snoopy import logger
import pytest


@pytest.mark.parametrize( "univariate",  ["POT", "BM"]  )
@pytest.mark.xfail(reason="DFORM minimisation a bit fragile depending on environment (probably scipy version)")
def test_diform_2d(univariate, display = False):
    
    from scipy.io import loadmat
    a = loadmat( f"{TEST_DATA:}/Data_Celtic_Sea.mat", struct_as_record = False )
    names = ["Hs" , "Tm" , "U10" , "dir_rel"]
    df = pd.DataFrame( index = a["time"][:,0] , data = { k:a[k][:,0] for k in names  } )
    
    df.loc[: , "Hs**0.5"] = df.loc[: , "Hs"]**0.5
    df["HL"] = df.Hs**0.5 * np.cos( df.dir_rel*np.pi/180 )
    df["HT"] = df.Hs**0.5 * np.sin( df.dir_rel*np.pi/180 )

    # Variable change dictionnary, this will be used to plot results in any desired space
    transform_dict =  {  "Hs" : lambda df:df["Hs**0.5"]**2,
                         "Hs " : lambda df : (df["HL"]**2 +  df["HT"] **2), 
                         "Hs**0.5" : lambda df : (df["HL"]**2 +  df["HT"] **2)**0.5 ,
                         "THETA_REL" : lambda df : np.rad2deg(np.arctan2(  df["HT"] ,  df["HL"]  ))
                      }

    #---------- 2D
    if univariate == "POT" : 
        pot_kwargs = { "fit_kwargs" : {"method" : "powell"} }
        diform_2d = DirectIform( df.loc[: , ["Hs**0.5" , "Tm" ] ], npoints = 30 , window_int= 48 , duration = len(df) / (24*365),
                                 threshold_q = 0.9 , pot_kwargs = pot_kwargs )
    else: 
        bm_kwargs = { "solver" : "genextreme.fit", "time_convert" : 365.24,
                      #"fit_kwargs" : {"method" : "powell"} 
                     }
        diform_2d = DirectIformBM( df.loc[: , ["Hs**0.5" , "Tm" ] ], npoints = 30 , block_size=365.25,
                                   bm_kwargs = bm_kwargs )

    diform_2d.fit_projections( )

    contour_25 = diform_2d.extract_contour(rp = 25.)

    # Check that calculated back the RP of the contour point yield the starting RP.
    rp_back = diform_2d.point_to_rp( contour_25 )
    assert( np.isclose( rp_back.RP.values , 25.0).all() )

    # Re-calculate RP using optimization, if original discretisation is not suffisiant, the will provide higher RP
    rp_back_constraint = diform_2d.point_to_rp( contour_25.loc[[1,15,],:], method = "optimize_constraint", 
                                                opt_kwargs={"tol" : 1e-8} )
    rp_back_spherical = diform_2d.point_to_rp( contour_25.loc[[1,15,],:], method = "optimize_spherical", 
                                               opt_kwargs={"method" : "nelder-mead", "tol" : 1e-8} )


    # Note point 31 would fail in "BM" + "contraint" configuration...
    for rp_back in [rp_back_constraint, rp_back_spherical]:
        assert( (rp_back.RP >= 24.).all() )
        #Should theoretically be >= 25....
        if not (rp_back.RP >= 24.).all() :
            logger.warning("Small issue on DFORM test...")

        assert( (rp_back.RP < 30.).all() )  # Should not be  to far from 25 years...


    if display :
        fig, ax = plt.subplots()
        diform_2d.plot_tangent( variables = ["Tm" , "Hs**0.5"], rp = 25.0 , ax=ax)

        fig, ax = plt.subplots()
        diform_2d.plot_projection_2d( variables = ["Tm", "Hs**0.5"], final_variables = ["Tm", "Hs"], rp = 25.0 , ax=ax, transform_dict = transform_dict, color = "blue")
        diform_2d.plot_projection_2d( variables = ["Tm", "Hs**0.5"], final_variables = ["Tm", "Hs"], rp = 25.0 , ax=ax, transform_dict = transform_dict, ci_level=0.95, low_high = "low", linestyle = "--", color ="blue")
        diform_2d.plot_projection_2d( variables = ["Tm", "Hs**0.5"], final_variables = ["Tm", "Hs"], rp = 25.0 , ax=ax, transform_dict = transform_dict, ci_level=0.95, low_high = "high", linestyle = "--", color ="blue")
        diform_2d.plot_projection_2d( variables = ["Tm", "Hs**0.5"], final_variables = ["Tm", "Hs"], rp = 25.0 , ax=ax, transform_dict = transform_dict)
        diform_2d.plot_data_2d( variables = ["Tm", "Hs"],  transform_dict = transform_dict, ax=ax)
        diform_2d.plot_angle_parameters( plane = ["Tm_a", "Hs**0.5_a"], values = ["SHAPE", "SCALE", "NNLF"] )


def test_direction_vector(display = False):
    dv2 = direction_vector(2,11)
    dv3 = direction_vector(3,11)
    assert( np.isclose((dv2**2).sum(axis = 1) , 1.).all() )
    assert( np.isclose((dv3**2).sum(axis = 1) , 1.).all() )
    if display : 
        dv2.plot(x = 0 , y  = 1, kind = "scatter")
        fig, ax = plt.subplots( subplot_kw={"projection": "3d"} )
        ax.plot( dv3.iloc[:,0], dv3.iloc[:,1] , dv3.iloc[:,2] , marker = "o", linestyle = "")

if __name__ == "__main__"    :
    
    display = True
    
    test_diform_2d(display = True, univariate = "BM")
    test_diform_2d(display = True, univariate = "POT")
