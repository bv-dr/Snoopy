#include <math.h>
#include "Weibull.hpp"

using namespace BV::Statistics ;



int Weibull::get_nDof() const
{
    return 3;
}

double Weibull::_pdf(double x, double shape) const
{
    if ( x >=0 && shape >=0 )
    {
        return shape * std::pow( x , shape - 1) * std::exp(-std::pow( x, shape));
    }
    else
    {
        return 0.0;
    }
}

double Weibull::pdf(double x, double shape, double loc, double scale) const
{
    if (check( x, shape, loc, scale ) )
    { 
        return (shape / scale) * std::pow(  (x-loc)/scale , shape-1  ) * std::exp(-std::pow((x-loc)/scale, shape)) ;
    }
    else 
    {
        return 0.0;
    }
}

double Weibull::cdf(double x, double shape, double loc, double scale) const
{
    return 1 - std::exp( -std::pow( (x-loc) / scale, shape ));
}

double Weibull::ppf(double p, double shape, double loc, double scale) const
{
    return scale * std::pow( -std::log(  1 - p )  , 1./shape ) + loc ;
}

double Weibull::nnlf( const Eigen::ArrayXd& coefs , const Eigen::ArrayXd& data) const
{
    Eigen::ArrayXd  logpdf_v = logpdf( data  , coefs[0], coefs[1], coefs[2] );
    return -logpdf_v.sum();
}

double Weibull::moment_central( int n , double shape , double loc , double scale) const
{
    return std::pow(scale,n) * tgamma( 1. + n / shape );
}


double Weibull::mean( double shape , double loc , double scale) const
{
    return scale * tgamma( 1 +  (1. / shape )) + loc;
}

double Weibull::var( double shape , double loc , double scale) const
{
    return pow(scale,2) * (   tgamma( 1. + (2./shape) ) - std::pow( tgamma( 1. + (1. / shape) ), 2 )  );
}

bool Weibull::check(double x, double shape, double loc, double scale) const
{
    if (x - loc < 0) { return false; }
    if (shape < 0) { return false; }
    if (scale < 0) { return false; }
    return true;
}
