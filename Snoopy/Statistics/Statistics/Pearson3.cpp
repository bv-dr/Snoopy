#include <math.h>
#include "Pearson3.hpp"

using namespace BV::Statistics ;



int Pearson3::get_nDof() const
{
    return 3;
}


double Pearson3::_pdf(double x, double shape) const
{
    double b = 2 / shape;
    double a = b * b;
    double ksi = -b;
    return ( abs(b) / tgamma(a)) * pow(b*(x - ksi), a - 1) * exp(-b * (x - ksi)); ;
}

double Pearson3::_logpdf(double x, double shape) const
{
    double b = 2 / shape;
    double a = b * b;
    double ksi = -b;
    return log(abs(b)) - lgamma(a) + (a - 1) * log(b*(x - ksi)) - b * (x - ksi);
}




double Pearson3::pdf(double x, double shape, double loc , double scale) const
{
    return _pdf(x, (x - loc) / scale) / scale;
};






