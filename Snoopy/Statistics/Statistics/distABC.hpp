#pragma once
#include <stdexcept>
#include <algorithm>
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>

#include "StatisticsExport.hpp"

namespace BV {
namespace Statistics {


int nCr(int n, int r);



    
    


class STATISTICS_API DistributionModelABC
{
public:

    virtual  ~DistributionModelABC()
    {
    }

    virtual int get_nDof() const = 0 ;

    virtual double nnlf(const Eigen::ArrayXd& coefs, const Eigen::ArrayXd& data) const
    {
        throw std::invalid_argument("nnlf not implemented for this method");
        return 0.0;
    } ;


	// Normalized pdf (loc = 0, scale = 0)
    inline virtual double _pdf(double x) const;
    inline virtual double _pdf(double x, double) const;
    inline virtual double _pdf(double x, double, double) const;

    inline virtual double _logpdf(double x, double) const;
    inline virtual double _logpdf(double x, double, double) const;

	// Function to be sub-classed
	inline virtual bool check(double x, double) const;
	inline virtual double pdf(double x, double) const;
    virtual Eigen::ArrayXd pdf_vect(const Eigen::ArrayXd&  x, double) const;
    inline virtual double cdf(double x, double) const;
	inline virtual double ppf(double p, double) const;
	inline virtual double moment_central(int n, double) const;


	//Complements
	double sf(double x, double) const;
	double isf(double p, double) const;
	double logpdf(double x, double) const;
	virtual double moment(int n, double) const;  // Compute raw_moment from central moments (Default implementation assumes that the two last parameter are loc and scale!)
	virtual double mean(double) const;
	virtual double var(double) const;

	virtual Eigen::ArrayXd logpdf(const Eigen::ArrayXd& x, double) const;
    // 3 parameters

    // Function to be sub-classed
    inline virtual bool check( double x, double, double, double ) const ;
    inline virtual double pdf( double x, double, double, double ) const ;
    inline virtual double cdf( double x, double, double, double ) const ;
    inline virtual double ppf( double p, double, double, double ) const ;
    inline virtual double moment_central( int n , double, double, double ) const ;


    //Complements
    double sf( double x, double, double, double ) const;
    double isf( double p, double, double, double ) const;
    double logpdf( double x, double, double, double ) const;
    virtual double moment( int n , double, double, double ) const ;  // Compute raw_moment from central moments (Default implementation assumes that the two last parameter are loc and scale!)
    virtual double mean( double, double, double ) const ;
    virtual double var( double, double, double ) const ;

    virtual Eigen::ArrayXd logpdf( const Eigen::ArrayXd& x, double, double, double ) const;

    // 4 parameters
    // Function to be sub-classed
    inline virtual bool check( double x, double, double, double, double ) const ;
    inline virtual double pdf( double x, double, double, double, double ) const ;
    inline virtual double cdf( double x, double, double, double, double ) const ;
    inline virtual double ppf( double p, double, double, double, double ) const ;
    inline virtual double moment_central( int n , double, double, double, double ) const ;


    //Complements
    double sf( double x, double, double, double, double ) const;
    double isf( double p, double, double, double, double ) const;
    double logpdf( double x, double, double, double, double ) const;
    virtual double moment( int n , double, double, double, double ) const ;  // Compute raw moment from central moments
    virtual double mean( double, double, double, double ) const ;
    virtual double var( double, double, double, double ) const ;

    Eigen::ArrayXd logpdf( const Eigen::ArrayXd& x, double, double, double, double ) const;
private:

protected:
} ;


double DistributionModelABC::_pdf(double x, double) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}

double DistributionModelABC::_pdf(double x, double, double) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}

double DistributionModelABC::_pdf(double x) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}



// 1 parameters
bool DistributionModelABC::check(double x, double) const
{
	throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}

double DistributionModelABC::pdf(double x, double coef) const
{
	throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
    return 0.0;
}


double DistributionModelABC::cdf(double x, double) const
{
	throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}

double DistributionModelABC::ppf(double p, double) const
{
	throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}


double DistributionModelABC::moment_central(int n, double) const
{
	throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}



// 3 parameters
bool DistributionModelABC::check( double x, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}


double DistributionModelABC::pdf( double x, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}

double DistributionModelABC::_logpdf(double x, double) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}

double DistributionModelABC::_logpdf(double x, double, double) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution");
}

double DistributionModelABC::cdf( double x, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}

double DistributionModelABC::ppf( double p, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}


double DistributionModelABC::moment_central( int n, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}


// 4 parameters
bool DistributionModelABC::check( double x, double, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}


double DistributionModelABC::pdf( double x, double, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}

double DistributionModelABC::cdf( double x, double, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}

double DistributionModelABC::ppf( double p, double, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}

double DistributionModelABC::moment_central( int n, double, double, double, double ) const
{
    throw std::invalid_argument("Invalid arguments / number of parameter for distribution") ;
}




}
}
