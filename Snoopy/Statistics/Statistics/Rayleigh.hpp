#pragma once
#include <Eigen/Dense>
#include "StatisticsExport.hpp"
#include "distABC.hpp"

namespace BV
{
namespace Statistics
{

namespace rayleigh
{
    STATISTICS_API double cdf(double x, double sigma);
}

class STATISTICS_API Rayleigh : public DistributionModelABC
{

using DistributionModelABC::logpdf ;
using DistributionModelABC::DistributionModelABC ;

public:

    int get_nDof() const override;

    double pdf(double x, double sigma) const override;
    double cdf(double x, double sigma) const override;
    double ppf(double p, double sigma) const override;

    bool check( double x, double sigma ) const override;

    //TODO : Move in DistributionModelABC
    double nnlf( const Eigen::ArrayXd& coefs , const Eigen::ArrayXd& data) const override;

    double moment_central( int n , double sigma) const override;
    double mean( double sigma) const override;
    double var( double sigma) const override;

};



class STATISTICS_API Rayleigh_n : public DistributionModelABC
{

    using DistributionModelABC::logpdf;
    using DistributionModelABC::DistributionModelABC;

protected :
    int n_;


public:

    Rayleigh_n(int n)
        : n_(n)
    {
    }

    int get_nDof() const override;

    double nnlf(const Eigen::ArrayXd& coefs, const Eigen::ArrayXd& data) const override { throw std::logic_error("Not implemented"); };

    double pdf(double x, double sigma) const override;
    double cdf(double x, double sigma) const override;
    double ppf(double p, double sigma) const override;

};

}
}
