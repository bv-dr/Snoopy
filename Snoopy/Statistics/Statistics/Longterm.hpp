#pragma once
#include <math.h>
#include "StatisticsExport.hpp"
#include <Eigen/Dense>

namespace BV
{
    namespace Statistics
    {

        STATISTICS_API double longTermBase(const Eigen::ArrayXd& shtProb,
                                               const Eigen::ArrayXd& rTzList,
                                               const Eigen::ArrayXd& probabilityList,
                                               double x,
                                               double duration_s,
                                               double dss
                                               );

        STATISTICS_API double longTermBase_p(const Eigen::ArrayXd& shtProb,
            const Eigen::ArrayXd& rTzList,
            const Eigen::ArrayXd& probabilityList,
            double x,
            double duration_s,
            double dss,
            int numThreads = 10
        );

        STATISTICS_API double longTermSpectral_inv( const Eigen::ArrayXd & shtProb,
                                                const Eigen::ArrayXd & rTzList,
                                                const Eigen::ArrayXd & probabilityList,
                                                double p,
                                                double duration_s,
                                                double dss,
                                                double lower,
                                                double upper, 
                                                double tol );

        STATISTICS_API Eigen::ArrayXd longTermSpectral_inv_p(const Eigen::ArrayXXd& rsList,
                                                             const Eigen::ArrayXXd & rTzList,
                                                             const Eigen::ArrayXd & probabilityList,
                                                             double p,
                                                             double duration_s,
                                                             double dss,
                                                             //double lower,
                                                             //double upper,
                                                             double tol, 
                                                             int numThreads = 1);

    }
}