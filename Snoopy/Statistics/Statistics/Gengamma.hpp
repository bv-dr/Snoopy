#pragma once
#include <Eigen/Dense>
#include "StatisticsExport.hpp"
#include "distABC.hpp"

namespace BV
{
namespace Statistics
{

class STATISTICS_API Gengamma : public DistributionModelABC
{

using DistributionModelABC::logpdf ;
using DistributionModelABC::DistributionModelABC ;

public:

    int get_nDof() const override;

    double _pdf(double x, double a, double c) const;
    double _logpdf(double x, double, double) const;
    double pdf(double x, double a, double c, double loc, double scale) const override;
    double cdf(double x, double a, double c, double loc, double scale) const override;
    double ppf(double p, double a, double c, double loc, double scale) const override;

    bool check( double x, double a, double c, double loc, double scale) const override;

    //TODO : Move in DistributionModelABC
    double nnlf( const Eigen::ArrayXd& coefs , const Eigen::ArrayXd& data) const override;

    double moment_central( int n , double a, double c, double loc, double scale) const override;

};
}
}
