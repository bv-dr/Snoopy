
#include "distABC.hpp"

using namespace BV::Statistics ;


int BV::Statistics::nCr(int n, int r) {
	if (r > n / 2) r = n - r; // because C(n, r) == C(n, n - r)
	int ans = 1;
	int i;

	for (i = 1; i <= r; i++) {
		ans *= n - r + i;
		ans /= i;
	}
	return ans;
}

// -------------- 1 parameters

double DistributionModelABC::sf(double x, double a) const
{
	return 1. - cdf(x, a);
}

double DistributionModelABC::isf(double p, double a) const
{
	return ppf(1 - p, a);
}

double DistributionModelABC::logpdf(double x, double a) const
{
	return std::log(pdf(x, a));
}
/*
 *  Compute raw moment from central moments  (1 parameters)
 */
double DistributionModelABC::moment(int n, double a) const
{
	return moment_central(n, a);
}

double DistributionModelABC::mean(double a) const
{
	return moment(1, a);
}

double DistributionModelABC::var(double a) const
{
	return pow(moment_central(2, a), 0.5);
}

Eigen::ArrayXd DistributionModelABC::logpdf(const Eigen::ArrayXd& x, double a) const
{
    return x.unaryExpr([this, a](double tmp) { return logpdf(tmp, a);  });
}

Eigen::ArrayXd DistributionModelABC::pdf_vect(const Eigen::ArrayXd& x, double a) const
{
    return x.unaryExpr([this, a](double tmp) { return pdf(tmp, a);  });
}

//-------------  3 parameters

double DistributionModelABC::sf( double x, double a , double b, double c ) const
{
    return 1. - cdf(x, a, b, c);
}

double DistributionModelABC::isf( double p, double a , double b, double c ) const
{
    return ppf(1 - p,  a, b, c);
}

double DistributionModelABC::logpdf( double x, double a , double b, double c ) const
{
    return std::log( pdf(x,  a, b, c));
}



/*
 *  Compute raw moment from central moments (loc=0) (3 parameters)
 */
double DistributionModelABC::moment( int n, double shape, double loc, double scale) const
{

    double result = 0;
    double val = moment_central( n , shape, loc , scale );
    if (loc == 0.)
    {
        return val;
    }

    double fac = 1. / float(loc);
    for (int k = 0; k < n; ++k)
    {
        double valk =  moment_central( k , shape, loc , scale );
        result += nCr(n, k) * pow(fac, k) * valk;
    }

    result += pow(fac, n ) * val;
    return result * pow(loc,n);
}




double DistributionModelABC::var( double shape, double loc, double scale) const
{
    return pow( moment_central( 2, shape, loc , scale ) , 0.5);
}


Eigen::ArrayXd DistributionModelABC::logpdf( const Eigen::ArrayXd& x, double a , double b, double c ) const
{
   
   return x.unaryExpr([this, a, b, c](double tmp) { return logpdf(tmp, a, b, c);  });
}
//-------------  4 parameters


double DistributionModelABC::sf( double x, double a , double b, double c, double d ) const
{
    return 1. - cdf(x, a, b, c,d);
}

double DistributionModelABC::isf( double p, double a , double b, double c, double d ) const
{
    return ppf(1 - p,  a, b, c,d);
}

double DistributionModelABC::logpdf( double x, double a , double b, double c, double d ) const
{
    return std::log( pdf(x,  a, b, c,d));
}

/*
 *  Compute raw moment from central moments  (4 parameters)
 */
double DistributionModelABC::moment(int n, double a, double c, double loc, double scale) const
{

	double result = 0;
	double val = moment_central(n, a, c, loc, scale);
	if (loc == 0.)
	{
		return val;
	}

	double fac = 1. / float(loc);
	for (int k = 0; k < n; ++k)
	{
		double valk = moment_central(k, a, c, loc, scale);
		result += nCr(n, k) * pow(fac, k) * valk;
	}

	result += pow(fac, n) * val;
	return result * pow(loc, n);
}

double DistributionModelABC::mean(double a, double c, double loc, double scale) const
{
	return moment(1, a, c, loc, scale);
}

double DistributionModelABC::mean(double shape, double loc, double scale) const
{
	return moment(1, shape, loc, scale);
}

double DistributionModelABC::var(double a, double c, double loc, double scale) const
{
	return pow(moment_central(2, a, c, loc, scale), 0.5);
}


Eigen::ArrayXd DistributionModelABC::logpdf( const Eigen::ArrayXd& x, double a , double b, double c, double d ) const
{
   return x.unaryExpr([this, a, b, c, d](double tmp) { return logpdf(tmp, a, b, c, d);  });
}
