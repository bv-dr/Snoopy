#include <math.h>
#include "genpareto.hpp"
#include <omp.h>

using namespace BV::Statistics;

double genpareto_2p::nnlf(const Eigen::Array2d& shape_scale, const Eigen::ArrayXd& data, double penalty)
{
	auto shape = shape_scale[0];
	auto scale = shape_scale[1];
	double _c = (1. + 1. / shape);
	double nnlf = 0.;
	#ifdef _MSC_VER
	#pragma omp parallel for reduction(+:nnlf)
	#else
	#pragma omp parallel for simd reduction(+:nnlf)
	#endif
	for (Eigen::Index i = 0; i < data.size(); i++) 
	{
		if (data[i] * shape / scale > -1 + 1e-12)
		{
		    nnlf +=  std::log1p(shape * data[i] / scale);
		}
		else
		{
			nnlf += penalty / _c;
		}
	}
	return nnlf * _c + std::log(scale) * data.size();
};


Eigen::Array2d genpareto_2p::nnlf_grad(const Eigen::Array2d& shape_scale, const Eigen::ArrayXd& data)
{
	auto shape = shape_scale[0];
	auto scale = shape_scale[1];
	Eigen::Array2d res(0., 0.);

	for (Eigen::Index i = 0; i < data.size(); i++)
	{
		res[0] -= (-shape * data[i] * (shape + 1) + (scale + shape * data[i]) * std::log((scale + shape * data[i]) / scale)) / (shape * shape * (scale + shape * data[i]));
		res[1] -= (-scale + data[i]) / (scale * (scale + shape * data[i]));
	}
	return res;
};
