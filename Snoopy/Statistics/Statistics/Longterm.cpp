#include "Longterm.hpp"
#include <boost/math/tools/roots.hpp>


double BV::Statistics::longTermBase(const Eigen::ArrayXd & shtProb, const Eigen::ArrayXd & rTzList, const Eigen::ArrayXd & probabilityList, double x, double duration_s, double dss)
{
    if (shtProb.size() != probabilityList.size())
    {
        throw std::invalid_argument("Error, Longterm base : dimensions does not match");
    };
    if (dss > 0.)
        {
        double pe_ss = (probabilityList * (1 - Eigen::pow( shtProb, dss / rTzList)  )).sum();
        return pow(1 - pe_ss, duration_s / dss);
        }
    else if (dss < 10.)
        {
        double v = ((probabilityList / rTzList) * (1 - shtProb)).sum();
        double tz_mean = 1. / ((probabilityList / rTzList)).sum();
        return pow(1 - tz_mean * v, duration_s / tz_mean);
        }
    else
        {
        return (Eigen::pow(shtProb, probabilityList * duration_s / rTzList)).prod();
        }
     return 0.0;
}


// Same as above, using openmp as an attempt to fasten the calculation
double BV::Statistics::longTermBase_p(const Eigen::ArrayXd & shtProb, const Eigen::ArrayXd & rTzList, const Eigen::ArrayXd & probabilityList, double x, double duration_s, double dss, int numThreads)
{
    if (shtProb.size() != probabilityList.size())
    {
        throw std::invalid_argument("Error, Longterm base : dimensions does not match");
    };
    if (dss > 0.)
    {
        double pe_ss = 0.0;
        #pragma omp parallel for reduction(+ : pe_ss) num_threads(numThreads)
        for (Eigen::ArrayXXd::Index i = 0; i < shtProb.size(); ++i)
        {
            pe_ss += (probabilityList(i) * (1 - std::pow(shtProb(i), dss / rTzList(i))));
        }
        return pow(1 - pe_ss, duration_s / dss);
    }
    else if (dss < 10.)
    {
        double v = ((probabilityList / rTzList) * (1 - shtProb)).sum();
        double tz_mean = 1. / ((probabilityList / rTzList)).sum();
        return pow(1 - tz_mean * v, duration_s / tz_mean);
    }
    else
    {
        return (Eigen::pow(shtProb, probabilityList * duration_s / rTzList)).prod();
    }
    return 0.0;
}

double BV::Statistics::longTermSpectral_inv(const Eigen::ArrayXd & rsList, const Eigen::ArrayXd & rTzList, const Eigen::ArrayXd & probabilityList, double p, double duration_s, double dss, double lower, double upper, double tol)
{
    boost::uintmax_t maxit = 50;

    // Code that could throw an exception
    std::pair<double, double> res = boost::math::tools::toms748_solve([rsList, rTzList, probabilityList, p, duration_s, dss](double x)
    { 
        Eigen::ArrayXd shtProb(  1 - Eigen::exp( - 8. * x * x / Eigen::pow(rsList , 2 ) ));
        return longTermBase(shtProb, rTzList, probabilityList, x, duration_s, dss) - p; }, lower, upper, [tol](double a, double b)->bool {return std::abs(a / b - 1.) < tol; 
    }, maxit);
    return res.first;
}


// Parallel tentative, actually very slow.
Eigen::ArrayXd BV::Statistics::longTermSpectral_inv_p(const Eigen::ArrayXXd & rsList, const Eigen::ArrayXXd & rTzList, const Eigen::ArrayXd & probabilityList, double p, double duration_s, double dss, double tol, int numThreads)
{

    Eigen::ArrayXd res(rsList.cols());
    #pragma omp parallel for num_threads(numThreads)
    for (Eigen::ArrayXXd::Index i = 0; i < rsList.cols(); ++i)
        {
        double lower = 1e-8;
        double upper = rsList.col(i).maxCoeff()*3.;
        res(i) = BV::Statistics::longTermSpectral_inv(rsList.col(i), rTzList.col(i), probabilityList, p, duration_s, dss, lower, upper, tol);
        }

    return res;
}
