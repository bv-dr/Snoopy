#pragma once
#include <Eigen/Dense>
#include "StatisticsExport.hpp"

namespace BV
{
namespace Statistics
{
namespace genpareto_2p
{
 STATISTICS_API double nnlf(const Eigen::Array2d& shape_scale , const Eigen::ArrayXd& data, double penalty = 1e6); 
 STATISTICS_API Eigen::Array2d nnlf_grad(const Eigen::Array2d& shape_scale, const Eigen::ArrayXd& data);
};
}
}
