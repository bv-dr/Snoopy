#include <math.h>
#include "Gengamma.hpp"
#include <boost/math/special_functions/gamma.hpp>
using namespace BV::Statistics ;
using namespace boost::math ;


int Gengamma::get_nDof() const
{
    return 4;
}

double Gengamma::_pdf(double x, double a, double c) const
{
    return  (abs(c) * pow(x, c*a - 1) / std::tgamma(a) ) * std::exp(-std::pow( x, c));
}

double Gengamma::_logpdf(double x, double a, double c) const
{
    return  log(c) + (c*a - 1) * log(x) - pow(x, c) - lgamma(a);
}


double Gengamma::pdf(double x, double a, double c, double loc, double scale) const
{
    return  (c * pow( x-loc, c*a-1 ) / (pow( scale, c*a ) * std::tgamma(a)) ) * std::exp( - std::pow( (x-loc) / scale , c ) );
}

double Gengamma::cdf(double x, double a, double c, double loc, double scale) const
{
    return gamma_p( a , pow( (x-loc)/scale, c ) ) ;
}

double Gengamma::ppf(double p, double a, double c, double loc, double scale) const
{
    return  scale * pow( gamma_p_inv( a , p ), 1./c ) + loc ;
}

double Gengamma::nnlf( const Eigen::ArrayXd& coefs , const Eigen::ArrayXd& data) const
{
    Eigen::ArrayXd  logpdf_v = logpdf( data  , coefs[0], coefs[1], coefs[2], coefs[3] );
    return -logpdf_v.sum();
}

double Gengamma::moment_central( int n , double a, double c , double loc , double scale) const
{
     return pow( scale , n  ) * tgamma( a + n / c ) / tgamma( a );
}

bool Gengamma::check(double x, double a, double c, double loc, double scale) const
{
    return true ;
}
