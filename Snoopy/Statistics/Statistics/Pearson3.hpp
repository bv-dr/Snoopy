#pragma once
#include <Eigen/Dense>
#include "StatisticsExport.hpp"
#include "distABC.hpp"

namespace BV
{
namespace Statistics
{

class STATISTICS_API Pearson3 : public DistributionModelABC
{

using DistributionModelABC::logpdf ;
using DistributionModelABC::DistributionModelABC ;

public:

    int get_nDof() const override;

    double _pdf(double x, double shape) const override;

    double _logpdf(double x, double shape) const override;

    //Eigen::ArrayXd logpdf(const Eigen::ArrayXd& x , double shape, double loc, double scale) const override;

    //double ppf(double p, double shape, double loc, double scale) const override;

    double pdf(double x, double shape, double loc , double scale) const override;

    //bool check( double x, double shape , double loc, double scale ) const override;

    //TODO : Move in DistributionModelABC
    

    //double moment_central( int n , double shape , double loc , double scale) const override;
    //double mean( double shape , double loc , double scale) const override;
    //double var( double shape , double loc , double scale) const override;

};


}
}
