#include <math.h>
#include "Genextreme.hpp"

using namespace BV::Statistics ;



int Genextreme::get_nDof() const
{
    return 3;
}


double Genextreme::pdf(double x, double shape, double loc, double scale) const
{
    double t = std::pow( 1 - shape*( (x-loc)/scale  ) , 1/shape );
    return ( 1. / scale) * std::pow( t , -shape + 1) * std::exp( -t );
}


//Eigen::ArrayXd Genextreme::pdf(const Eigen::ArrayXd& x , double shape, double loc, double scale) const
//{
//    Eigen::ArrayXd t = Eigen::pow( 1 - shape*( (x-loc)/scale  ) , 1/shape );
//    return ( 1. / scale) * Eigen::pow( t , -shape + 1) * Eigen::exp( -t );
//}

Eigen::ArrayXd Genextreme::logpdf(const Eigen::ArrayXd& x , double shape, double loc, double scale) const
{
    Eigen::ArrayXd t = Eigen::pow( 1 - shape*( (x-loc)/scale  ) , 1/shape );
    return std::log(1/scale) + (1-shape) * Eigen::log(t) - t;
}

double Genextreme::cdf(double x, double shape, double loc, double scale) const
{
    double t = std::pow( 1 - shape*( (x-loc)/scale  ) , 1/shape );
    return  std::exp( -t ) ;
}

double Genextreme::ppf(double p, double shape, double loc, double scale) const
{
    return  (-scale / shape) * ( std::pow( -std::log(p), shape ) - 1 ) + loc ;
}

double Genextreme::nnlf( const Eigen::ArrayXd& coefs , const Eigen::ArrayXd& data) const
{
    // Eigen::ArrayXd  logpdf_v = logpdf(data, coefs[0], coefs[1], coefs[2]);
    // return -logpdf_v.sum();

    /* Helps with optimizer(better than + Inf). 
    Scipy penalty is 709.782712893 * 100.
    We here put much larger penalty: as logpdf is calculated directly and "legit" data could otherwise have much higher nnlf than the penalty.
    */
    double penalty = 1e300 / data.size();

    auto shape = coefs[0];
    auto loc = coefs[1];
    auto scale = coefs[2];

    double nnlf = 0.;
    #ifdef _MSC_VER
    #pragma omp parallel for reduction(+:nnlf)
    #else
    #pragma omp parallel for simd reduction(+:nnlf)
    #endif
    for (Eigen::Index i = 0; i < data.size(); i++)
    {
        double tt = 1 - shape * ((data[i] - loc) / scale);
        if (tt > 0)
        {
            double t = std::pow(tt, 1 / shape);
            nnlf -= (1 - shape) * std::log(t) - t;
        }
        else
        { 
            nnlf += penalty;
        }
    }
    return nnlf + std::log(scale) * data.size();
}

double Genextreme::moment_central( int n , double shape , double loc , double scale) const
{
    double result = 0;
    for (int k = 0; k < n+1; ++k)
    {
        result += nCr(n, k) * pow(-1, k) * tgamma( 1 + shape*k );
    }
    result *= pow( 1. / shape, n );
    result *= pow(scale, n );
    return result ;
}


double Genextreme::mean( double shape , double loc , double scale) const
{
    return loc + (scale / shape) * ( 1- tgamma( 1+shape ) );
}

double Genextreme::var( double shape , double loc , double scale) const
{
    double g1 = tgamma( 1.+shape );
    double g2 = tgamma( 1.+shape*2 );
    return std::pow( scale / shape, 2 ) * ( g2 - pow(g1,2)  );
}

bool Genextreme::check(double x, double shape, double loc, double scale) const
{
    return true ;
}


double Genextreme::_pdf(double x, double shape) const
{
    double t = std::pow(1 - shape *  x , 1 / shape);
    return std::pow(t, -shape + 1) * std::exp(-t);
}
