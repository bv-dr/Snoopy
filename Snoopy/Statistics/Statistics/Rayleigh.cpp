#include <math.h>
#include "Rayleigh.hpp"

using namespace BV::Statistics ;



// Function not in class for easier access from python (pickable -> easier usage in parallelized script)
double BV::Statistics::rayleigh::cdf(double x, double sigma)
{
    return 1 - std::exp(-x * x / (2 * sigma * sigma));
}

int Rayleigh::get_nDof() const
{
    return 1;
}


double Rayleigh::pdf(double x, double sigma) const
{
    return (x / (sigma*sigma))  * std::exp(-x*x / (2*sigma*sigma)) ;
}

double Rayleigh::cdf(double x, double sigma) const
{
    return rayleigh::cdf(x, sigma);
}

double Rayleigh::ppf(double p, double sigma) const
{
    return sigma*sqrt(2) * sqrt( -log( 1-p ) ) ;
}

double Rayleigh::nnlf( const Eigen::ArrayXd& coefs , const Eigen::ArrayXd& data) const
{
    Eigen::ArrayXd  logpdf_v = logpdf( data  , coefs[0] );
    return -logpdf_v.sum();
}

double Rayleigh::moment_central( int n , double sigma) const
{
    return std::pow(sigma*sqrt(2),n) * tgamma( 1. + n / 2. );
}


double Rayleigh::mean( double sigma) const
{
    return sigma * sqrt( 0.5 * M_PI) ;
}

double Rayleigh::var( double sigma) const
{
    return 0.5*(4-M_PI)*sigma*sigma;
}

bool Rayleigh::check(double x, double sigma) const
{
    return true ;
}




int Rayleigh_n::get_nDof() const
{
    return 1;
}

double Rayleigh_n::cdf(double x, double sigma) const
{
    return std::pow( 1 - std::exp(-x * x / (2. * sigma*sigma)) , n_) ;
}

double Rayleigh_n::ppf(double p, double sigma) const
{
    return sigma * std::sqrt(-2. * std::log(1. - std::pow(p, 1. / n_)));
}

double Rayleigh_n::pdf(double x, double sigma) const
{
    return n_ * std::pow(1. - std::exp(-x * x / (2. * sigma*sigma)), n_ - 1. ) *  (x / (sigma*sigma))  * std::exp(-x * x / (2. * sigma*sigma));
}