#include "Tools/SpdLogger.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <pybind11/functional.h>
#include <omp.h>
#include "Statistics/distABC.hpp"
#include "Statistics/Weibull.hpp"
#include "Statistics/Gengamma.hpp"
#include "Statistics/Genextreme.hpp"
#include "Statistics/Pearson3.hpp"
#include "Statistics/Rayleigh.hpp"
#include "Statistics/Longterm.hpp"
#include "Statistics/genpareto.hpp"

namespace py = pybind11;
using namespace py::literals;
using namespace BV::Statistics;

// Intermediate class to be able to expose protected attribute to Python
// TODO : Move up to DistributionModelABC
class PyDistributionModelABC : public DistributionModelABC
{
public:
    using DistributionModelABC::DistributionModelABC;


    int get_nDof() const override {
        PYBIND11_OVERLOAD_PURE( int, DistributionModelABC, get_nDof, );
    }

    double nnlf( const Eigen::ArrayXd& data , const Eigen::ArrayXd& coefs ) const override {
        PYBIND11_OVERLOAD_PURE(
            double, /* Return type */
            DistributionModelABC,      /* Parent class */
            nnlf,          /* Name of function in C++ (must match Python name) */
            data, coefs      /* Argument(s) */
        );
    }


    /* Trampoline (need one for each virtual function) */
    double _pdf(double x) const override {
        PYBIND11_OVERLOAD(double, DistributionModelABC, _pdf, x);
    }

    /* Trampoline (need one for each virtual function) */
    double _pdf(double x, double a) const override {
        PYBIND11_OVERLOAD(double, DistributionModelABC, _pdf, x, a);
    }

    /* Trampoline (need one for each virtual function) */
    double _pdf(double x, double a, double b) const override {
        PYBIND11_OVERLOAD(double, DistributionModelABC, _pdf, x, a, b);
    }

    /* Trampoline (need one for each virtual function) */
    double _logpdf(double x, double a) const override {
        PYBIND11_OVERLOAD(double, DistributionModelABC, _logpdf, x, a);
    }

    double _logpdf(double x, double a, double b) const override {
        PYBIND11_OVERLOAD(double, DistributionModelABC, _logpdf, x, a, b);
    }


	// 1 parameters
	/* Trampoline (need one for each virtual function) */
	double pdf(double x, double a) const override {
		PYBIND11_OVERLOAD(double, DistributionModelABC, pdf, x, a);
	}

    bool check(double x, double a) const override {
		PYBIND11_OVERLOAD(bool, DistributionModelABC, check, x, a);
	}

	double cdf(double x, double a) const override {
		PYBIND11_OVERLOAD(double, DistributionModelABC, cdf, x, a);
	}

	double ppf(double p, double a) const override {
		PYBIND11_OVERLOAD(double, DistributionModelABC, ppf, p, a);
	}

	double moment_central(int n, double a) const override {
		PYBIND11_OVERLOAD(double, DistributionModelABC, moment_central, n, a);
	}

	double mean(double a) const override {
		PYBIND11_OVERLOAD(double, DistributionModelABC, mean, a);
	}

	double var(double a) const override {
		PYBIND11_OVERLOAD(double, DistributionModelABC, var, a);
	}


    // 3 parameters

    /* Trampoline (need one for each virtual function) */
    double pdf(double x, double a, double b, double c ) const override {
        PYBIND11_OVERLOAD(  double,  DistributionModelABC,  pdf, x, a, b, c );
    }

    bool check( double x, double a, double b, double c ) const override {
        PYBIND11_OVERLOAD( bool, DistributionModelABC, check, x, a, b, c  );
    }

    double cdf(double x, double a, double b, double c ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, cdf, x, a, b, c  );
    }

    double ppf(double p, double a, double b, double c ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, ppf, p, a, b, c  );
    }

    double moment_central(int n, double a, double b, double c ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, moment_central, n, a, b, c  );
    }

    double mean( double a, double b, double c ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, mean, a, b, c  );
    }

    double var( double a, double b, double c ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, var, a, b, c  );
    }


    // 4 parameters
    double pdf(double x, double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, pdf, x, a, c, loc, scale  );
    }

    bool check( double x, double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( bool, DistributionModelABC, check, x, a, c, loc, scale  );
    }

    double cdf(double x, double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, cdf, x, a, c, loc, scale  );
    }

    double ppf(double p, double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, ppf, p, a, c, loc, scale  );
    }

    double moment_central(int n, double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, moment_central, n, a, c, loc, scale  );
    }

    double mean( double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, mean, a, c, loc, scale  );
    }

    double var( double a, double c, double loc, double scale ) const override {
        PYBIND11_OVERLOAD( double, DistributionModelABC, mean, a, c, loc, scale  );
    }

};



PYBIND11_MODULE(_Statistics, m)
{
    m.doc() = "Statistic module";

    m.def("omp_set_num_threads", omp_set_num_threads);
    m.def("omp_get_max_threads", omp_get_max_threads);

	m.def("set_logger_level", &BV::Tools::set_logger_level);
	m.def("add_logger_callback", &BV::Tools::add_logger_callback);
    // FIXME this is necessary to release python callbacks in spdlog
    // see https://github.com/pybind/pybind11/issues/749
    m.add_object("__cleanup_logger", pybind11::capsule(spdlog::drop_all)) ;

    // Dispersion relation ship
    m.def("longTermBase", &longTermBase, "shtProb"_a, "rTzList"_a, "probabilityList"_a, "x"_a, "duration_s"_a, "dss"_a);
    m.def("longTermBase_p", &longTermBase_p, "shtProb"_a, "rTzList"_a, "probabilityList"_a, "x"_a, "duration_s"_a, "dss"_a, "numThreads"_a = 1);
    m.def("longTermSpectral_inv", &longTermSpectral_inv, "rsList"_a, "rTzList"_a, "probabilityList"_a, "p"_a, "duration_s"_a, "dss"_a , "lower"_a , "upper"_a , "tol"_a);
    m.def("longTermSpectral_inv_p", &longTermSpectral_inv_p, "rsList"_a, "rTzList"_a, "probabilityList"_a, "p"_a, "duration_s"_a, "dss"_a, "tol"_a = 1e-5 , "numThreads"_a = 1);


    // Dispersion relation ship
    m.def("rayleigh_cdf", py::vectorize( &BV::Statistics::rayleigh::cdf), "x"_a, "sigma"_a);
    
    pybind11::module mgenpareto = m.def_submodule("genpareto_2p", "Function related to Generalized Pareto Distribution");
    mgenpareto.def("nnlf", &genpareto_2p::nnlf, "shape_scape"_a, "data"_a, "penalty"_a = 1.e6);
    mgenpareto.def("nnlf_grad", &genpareto_2p::nnlf_grad, "shape_scape"_a, "data"_a);


    py::class_<DistributionModelABC, PyDistributionModelABC> distributionModelABC(m, "DistributionModelABC" );
    distributionModelABC

        .def(py::init<>())

        .def("get_nDof", &DistributionModelABC::get_nDof, "Return number of parameter")

        .def("nnlf", &DistributionModelABC::nnlf, "Return negative log-likelyhood")


        // One parameter distribution
        .def("_pdf", py::vectorize(py::overload_cast<double>(&DistributionModelABC::_pdf, py::const_)), "x"_a)

        .def("_pdf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::_pdf, py::const_)), "x"_a, "coef"_a)

        .def("_pdf", py::vectorize(py::overload_cast<double, double, double>(&DistributionModelABC::_pdf, py::const_)), "x"_a, "a"_a , "b"_a)

        .def("_logpdf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::_logpdf, py::const_)), "x"_a, "coef"_a)

        .def("_logpdf", py::vectorize(py::overload_cast<double, double, double>(&DistributionModelABC::_logpdf, py::const_)), "x"_a, "a"_a , "b"_a)

		// One parameter distribution
        .def("pdf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::pdf, py::const_)), "x"_a, "coef"_a)

        .def("pdf_vect", py::overload_cast<const Eigen::ArrayXd& , double>(&DistributionModelABC::pdf_vect, py::const_), "x"_a, "coef"_a)

        .def("logpdf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::logpdf, py::const_)), "x"_a, "coef"_a)
		.def("check", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::check, py::const_)), "x"_a, "coef"_a)
		.def("cdf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::cdf, py::const_)), "x"_a, "coef"_a)
		.def("sf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::sf, py::const_)), "x"_a, "coef"_a)
		.def("ppf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::ppf, py::const_)), "x"_a, "coef"_a)
		.def("isf", py::vectorize(py::overload_cast<double, double>(&DistributionModelABC::isf, py::const_)), "x"_a, "coef"_a)

		.def("moment", py::overload_cast<int, double>(&DistributionModelABC::moment, py::const_), "n"_a, "coef"_a, "Return moment")
		.def("moment_central", py::overload_cast<int, double>(&DistributionModelABC::moment_central, py::const_), "n"_a, "coef"_a, "Return moment (central)")
		.def("mean", py::overload_cast<double>(&DistributionModelABC::mean, py::const_), "coef"_a, "Return mean")
		.def("var", py::overload_cast<double>(&DistributionModelABC::var, py::const_), "coef"_a, "Return variance")


        // Three parameter distribution
        // Notes : default argument names are (shape, loc, scale), this can be changed in the sub-class binding

        .def("pdf", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::pdf, py::const_)) , "x"_a , "shape"_a , "loc"_a, "scale"_a )
        .def("logpdf", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::logpdf, py::const_)), "x"_a, "shape"_a, "loc"_a, "scale"_a)
        .def("check", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::check, py::const_)) , "x"_a , "shape"_a , "loc"_a, "scale"_a )
        .def("cdf", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::cdf, py::const_)), "x"_a , "shape"_a , "loc"_a, "scale"_a )
        .def("sf", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::sf, py::const_)) , "x"_a , "shape"_a , "loc"_a, "scale"_a )
        .def("ppf", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::ppf, py::const_)) , "x"_a , "shape"_a , "loc"_a, "scale"_a )
        .def("isf", py::vectorize(py::overload_cast<double, double, double, double>(&DistributionModelABC::isf, py::const_)) , "x"_a , "shape"_a , "loc"_a, "scale"_a )

        .def("moment", py::overload_cast<int, double, double, double>(&DistributionModelABC::moment, py::const_) , "n"_a , "shape"_a , "loc"_a, "scale"_a, "Return moment" )
        .def("moment_central", py::overload_cast<int, double, double, double>(&DistributionModelABC::moment_central, py::const_) , "n"_a , "shape"_a , "loc"_a, "scale"_a, "Return moment (central)" )
        .def("mean", py::overload_cast< double, double, double>(&DistributionModelABC::mean, py::const_) , "shape"_a , "loc"_a, "scale"_a, "Return mean" )
        .def("var", py::overload_cast< double, double, double>(&DistributionModelABC::var, py::const_) , "shape"_a , "loc"_a, "scale"_a, "Return variance" )


        // 4 parameters distributions
        .def("pdf", py::overload_cast<double, double, double, double, double>(&DistributionModelABC::pdf, py::const_) , "x"_a , "a"_a , "c"_a ,"loc"_a, "scale"_a )

        .def("logpdf", py::vectorize(py::overload_cast<double, double, double, double, double>(&DistributionModelABC::logpdf, py::const_)), "x"_a, "a"_a, "c"_a, "loc"_a, "scale"_a)
        .def("check", py::vectorize(py::overload_cast<double, double, double, double, double>(&DistributionModelABC::check, py::const_)) , "x"_a , "a"_a , "c"_a ,"loc"_a, "scale"_a )
        .def("cdf", py::vectorize(py::overload_cast<double, double, double, double, double>(&DistributionModelABC::cdf, py::const_)) , "x"_a , "a"_a , "c"_a, "loc"_a, "scale"_a )
        .def("sf", py::vectorize(py::overload_cast<double, double, double, double, double>(&DistributionModelABC::sf, py::const_)) , "x"_a , "a"_a , "c"_a, "loc"_a, "scale"_a )
        .def("ppf", py::vectorize(py::overload_cast<double, double, double, double, double>(&DistributionModelABC::ppf, py::const_)) , "x"_a , "a"_a , "c"_a, "loc"_a, "scale"_a )
        .def("isf", py::vectorize(py::overload_cast<double, double, double, double, double>(&DistributionModelABC::isf, py::const_)) , "x"_a , "a"_a , "c"_a, "loc"_a, "scale"_a )

        .def("moment", py::overload_cast<int, double, double, double, double>(&DistributionModelABC::moment, py::const_) , "n"_a ,  "a"_a , "c"_a , "loc"_a, "scale"_a, "Return moment (raw)" )
        .def("moment_central", py::overload_cast<int, double, double, double, double>(&DistributionModelABC::moment_central, py::const_) , "n"_a ,  "a"_a , "c"_a , "loc"_a, "scale"_a, "Return moment (central)" )

        .def("mean", py::overload_cast< double, double, double, double>(&DistributionModelABC::mean, py::const_) ,  "a"_a , "c"_a, "loc"_a, "scale"_a, "Return mean" )
        .def("var", py::overload_cast< double, double, double, double>(&DistributionModelABC::var, py::const_) ,  "a"_a , "c"_a, "loc"_a, "scale"_a, "Return variance" )

        ;

    py::class_<Weibull>(m, "Weibull", distributionModelABC)
        .def(py::init<>())
          ;

    py::class_<Gengamma>(m, "Gengamma", distributionModelABC)
        .def(py::init<>())
          ;

    py::class_<Genextreme>(m, "Genextreme", distributionModelABC)
        .def(py::init<>())
          ;

	py::class_<Rayleigh>(m, "Rayleigh", distributionModelABC)
		.def(py::init<>())
		;

    py::class_<Rayleigh_n>(m, "Rayleigh_n", distributionModelABC)
        .def(py::init<int>(), "n"_a)
        ;

    py::class_<Pearson3>(m, "Pearson3", distributionModelABC)
        .def(py::init<>())
        ;
}

