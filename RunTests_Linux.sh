pytest --rootdir=Spectral/Tests Spectral/Tests
pytest --rootdir=Meshing/Tests Meshing/Tests
pytest --rootdir=TimeDomain/Tests TimeDomain/Tests
pytest --rootdir=Statistics/Tests Statistics/Tests
pytest --rootdir=Fatigue/Tests Fatigue/Tests
pytest --rootdir=WaveKinematic/Tests WaveKinematic/Tests

