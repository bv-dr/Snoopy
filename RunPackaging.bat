rem @echo off
rem arguments:
rem %1: build directory, relative to current working directory (%env.binDir%)
rem %2: Snoopy Number : Python version 38 = 3.8, 39 = 3.9 310 = 3.10 (SnoopyXX)
rem %3: Shared dir (%env.SHARED_DIR%)
rem %4: pypi user (%env.pypi_user%)
rem %5: pypi user's password (%env.pypi_pass%)
call C:\ProgramData\miniforge3\Scripts\activate Snoopy%2
set SNOOPY_PYD=%~dp0%1
echo "Building wheel"
rem rm /y/q dist
rem Do not remove dist, because it may delete *37*.whl Snoopy version if MSVC Static Py37 is run before MSVC Shared
rem if exist dist\ (rmdir /S /Q dist)
call python setup.py bdist_wheel
echo "Installing twine (if it is not installed yet)"
python -m pip install twine
echo "Uploading the wheel file to pypi"
twine upload --verbose --skip-existing -u %~4 -p %~5 --repository-url https://upload.pypi.org/legacy/ dist/*%2*.whl
echo "Copy wheel to shared location" %3
copy /y dist\*cp%2*.whl "%3"\
echo "Copy dlls to shared location" %3\DLLs

if "%2" == "38" (
    if not exist "%3\DLLs_py38" mkdir "%3\DLLs_py38"
    copy /y %1\*.dll "%3\DLLs_py38\"
    copy /y %1\*.pyd "%3\DLLs_py38\"
) else (
    if not exist "%3\DLLs" mkdir "%3\DLLs"
    copy /y %1\*.pyd "%3\DLLs\"
)
