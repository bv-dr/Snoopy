.. Snoopy documentation master file, created by
   sphinx-quickstart on Fri Sep 11 14:26:42 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. include:: ../readme.rst

.. toctree::
   :maxdepth: 2
   :hidden:

   Introduction/Introduction.rst

   Tutorials/Tutorials.rst
   
   Tutorials/AdvancedExample.rst

   Theory/Hydro_cheat_sheet.rst

   modules/Modules.rst
