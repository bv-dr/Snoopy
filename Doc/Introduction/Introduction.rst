Getting started
---------------
.. toctree::
   :maxdepth: 2

Documentation build on : |today|

Installing from pre-build
~~~~~~~~~~~~~~~~~~~~~~~~~

> pip install Snoopy.whl


Select package corresponding to your python version on :

[Update when available]

Dependencies :

- Numpy, scipy, matplotlib, pandas (standard python Scientific libs, packaged with anaconda by default)
- xarray



Convention
~~~~~~~~~~

Unless specified otherwise :

- Units are in S.I.
- Angles in radians (phase, heading...)


Local ship coordinate system is as follow :

.. image:: convention.png
  :width: 400
  :alt: ConventionPicture


Global coordinates system is as follow :

.. toctree::
   :maxdepth: 2
   
   ./../Tutorials/headingConvention.ipynb


