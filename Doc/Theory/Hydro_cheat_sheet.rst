Useful formulas
===============
Spectral analysis
-----------------

Dispersion relationship :

.. math:: \omega = g k \tanh(kh)

Encounter frequency

.. math:: \omega_e=\omega-k V cos\beta

Response spectrum

.. math:: S_R(\omega)=RAO^2(\omega)*S_w(\omega)

Spectral moments

.. math:: m_n=\int^\infty_0 \omega_e^n S_R(\omega) d\omega

or more generally, for short-crested sea-states :

.. math:: m_n=\int_{\theta} \int^\infty_{\omega=0} \omega_e^n S_w(\omega,\theta)* RAO^2(\omega, \theta)d\omega d\theta

Significant response (double amplitude) :

.. math:: R_s = 4 * \sqrt{m_0}

Standard deviation of the signal :

.. math:: \sigma = \sqrt{m_0}

The mean period :math:`T_m`, also referred as :math:`T_1` or
:math:`T_{01}` :

.. math:: T_m=2\pi \frac{m_0}{m_1}

Mean zero up-crossing period (noted :math:`T_Z`, :math:`T_2` or
:math:`T_{02}`):

.. math:: T_Z=2\pi\sqrt{\frac{m_0}{m_2}}

Mean period :math:`T_{0m1}`

.. math:: T_{0m1}=2\pi \frac{m_{-1}}{m_0}

Jonswap spectrum :

.. math::

   S_w(\omega)=A * \frac{5}{16} \ H_s^2 \ \omega_p^{4} \ \omega^{-5} exp\left[ -\frac{5}{4}\left(\frac{\omega}{\omega_P}\right) ^{-4}\right]
   \gamma^{\left[exp\left(\frac{-(\omega-\omega_P)^2}{2\sigma^2\omega^2_P}\right)\right]}

Assuming narrow banded process, maxima are Rayleigh distributed :

.. math::

   p(X)=\frac{X}{m_0}exp\left(\frac{-X^2}{2m_0}\right)
   \label{Rayleigh}

The distribution function (probability that :math:`x < X`) being :

.. math::

   P(X)=1-exp\left(\frac{-X^2}{2m_0}\right)
   \label{Rayleighdis}

Time domain reconstruction
--------------------------

.. math:: \eta(t, X, Y) = \sum_i A_i \cos( \omega_i t + \phi_i - k (X\cos{\beta} + Y\sin{\beta} ))

.. math:: A_i^2 = 2 * S_w(\omega_i) * d\omega

For linear response :math:`R`, with transfer function
:math:`RAO(\omega)` :

.. math:: R(t) = \sum_i |RAO(\omega_i)| A_i \cos( \omega_{e_i} t + \phi_i + RAO_{\phi}(\omega_i)  ))
