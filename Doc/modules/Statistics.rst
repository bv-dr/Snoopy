.. _Statistics:


.. currentmodule:: Snoopy


Statistics
~~~~~~~~~~


Long term statistics
____________________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Statistics.LongTermGen
    Snoopy.Statistics.LongTerm
    Snoopy.Statistics.LongTermSpectral
    Snoopy.Statistics.LongTermSD



Empirical return period
_______________________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Statistics.ReturnLevel
    
    
Discrete scatter-diagram
________________________

.. autosummary::
   :toctree: _autosummary
   :template: no_inh_class.rst

    Snoopy.Statistics.DiscreteSD

Extreme value analysis
______________________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

   Snoopy.Statistics.POT_GPD
   Snoopy.Statistics.DirectIform