.. _PyplotTools:

PyplotTools
~~~~~~~~~~~

.. toctree::
   :maxdepth: 2

Map plot
;;;;;;;;

.. autosummary::
   :toctree: _autosummary
   
   Snoopy.PyplotTools.mapPlot
   Snoopy.PyplotTools.drawRoute
   Snoopy.PyplotTools.animRoute
   Snoopy.PyplotTools.drawMap
   Snoopy.PyplotTools.drawGws


Stat plots
;;;;;;;;;;
   
.. autosummary::
   :toctree: _autosummary   
   
   Snoopy.PyplotTools.distPlot
   Snoopy.PyplotTools.rpPlot
   
   
  
Scatter plots
;;;;;;;;;;;;;
.. autosummary::
   :toctree: _autosummary
   
   Snoopy.PyplotTools.kde_scatter
   Snoopy.PyplotTools.density_scatter
   Snoopy.PyplotTools.scatterPlot
   Snoopy.PyplotTools.add_linregress

   
   






   
