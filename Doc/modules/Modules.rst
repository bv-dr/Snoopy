API Reference
-------------
.. toctree::
   :maxdepth: 3

   ./Spectral.rst
   ./TimeDomain.rst
   ./Meshing.rst
   ./Fatigue.rst
   ./Statistics.rst
   ./WaveKinematic.rst
   ./Reader.rst
   ./PyplotTools.rst
   ./Math.rst
