.. _Spectral:


.. currentmodule:: Snoopy


Spectral
~~~~~~~~


WaveSpectrum
____________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Spectral.WaveSpectrum
    Spectral.Jonswap
    Snoopy.Spectral.Wallop
    Snoopy.Spectral.Gamma
    Snoopy.Spectral.LogNormal
    Snoopy.Spectral.WaveTabulatedSpectrum


SeaState
________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.SeaState


Wif
___

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.Wif


RAO
___

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.Rao


QTF
___

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.Qtf


ResponseSpectrum
________________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.ResponseSpectrum
    Snoopy.Spectral.ResponseSpectrum2nd


SpectralMoments
_______________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.SpectralMoments


SpectralStats
_____________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

    Snoopy.Spectral.SpectralStats



Wind spectrum
_____________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

   Snoopy.Spectral.API
   Snoopy.Spectral.Davenport
   Snoopy.Spectral.Harris
   Snoopy.Spectral.Hino
   Snoopy.Spectral.Kaimal
   Snoopy.Spectral.Kareem
   Snoopy.Spectral.ModifiedHarris
   Snoopy.Spectral.NPD
   Snoopy.Spectral.OchiShin
   Snoopy.Spectral.Queffeulou

   
Dispersion relationship
_______________________

.. autosummary::
   :toctree: _autosummary
   
    Snoopy.Spectral.w2k
    Snoopy.Spectral.k2w
    Snoopy.Spectral.w2we
    Snoopy.Spectral.k2Cp
    Snoopy.Spectral.w2l
    Snoopy.Spectral.w2Cg
    Snoopy.Spectral.l2t
    Snoopy.Spectral.T2Te
    Snoopy.Spectral.t2l
    Snoopy.Spectral.we2w
    Snoopy.Spectral.cp2t
    Snoopy.Spectral.cp2k
    Snoopy.Spectral.t2k
