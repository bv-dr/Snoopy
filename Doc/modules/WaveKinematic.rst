.. _WaveKinematic:


.. currentmodule:: Snoopy

WaveKinematic
-------------


First order kinematic
_____________________

   
.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst
   
   WaveKinematic.FirstOrderKinematic
   WaveKinematic.Wheeler1st
 

Second order kinematic
______________________

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst
   
   WaveKinematic.SecondOrderKinematic
   WaveKinematic.SecondOrderKinematic21
   WaveKinematic.Wheeler2nd

