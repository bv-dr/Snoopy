.. _Math:


.. currentmodule:: Snoopy

Math
----


Form
____

.. autosummary::
   :toctree: _autosummary

    Snoopy.Math.MHLGA


Interpolation
_____________

.. autosummary::
   :toctree: _autosummary
   
    Snoopy.Math.InterpolatedComplexSpline
    Snoopy.Math.df_interpolate


Miscellaneous
_____________

.. autosummary::
   :toctree: _autosummary
   
    Snoopy.Math.approx_jacobian_n
    Snoopy.Math.df_interpolate
    Snoopy.Math.round_sum

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst

   Snoopy.Math.FunContourGenerator