.. _TimeDomain:


.. currentmodule:: Snoopy

TimeDomain
----------


Reconstruction to time-domain
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.. autosummary::
   :toctree: _autosummary
   :template: cust_class.rst


   TimeDomain.ReconstructionWifLocal
   TimeDomain.ReconstructionRaoLocal
   TimeDomain.ReconstructionQtfLocal


   
Up-crossing and declustering
;;;;;;;;;;;;;;;;;;;;;;;;;;;;


.. autosummary::
   :toctree: _autosummary
   :template: no_inh_class.rst
   
   Snoopy.TimeDomain.Decluster   
   Snoopy.TimeDomain.UpCrossAnalysis



FFT's and co
;;;;;;;;;;;;

.. autosummary::
   :toctree: _autosummary
   
    Snoopy.TimeDomain.getPSD
    Snoopy.TimeDomain.slidingFFT
    Snoopy.TimeDomain.fftDf
    Snoopy.TimeDomain.bandPass
    Snoopy.TimeDomain.reSample
