Advanced examples
-----------------
.. toctree::
   :maxdepth: 2

   ./retardationFunction
   ./FitSeaState
   ./LogNormal
   ./Spectrum_discretization
   ./LinearizeAndMatch
   # ./SlammingVelocity  # To check
   ./Tutorial_Variable_Bathymetry
   ./Crest_2ndOrder_EDW

