Tutorials
---------



.. toctree::
   :maxdepth: 2

Waves and ship response - short-term
____________________________________
.. toctree::
   :includehidden:
   :maxdepth: 1

   ./Seastate
   ./RAO
   ./Spectral
   ./spectrum2nd
   ./TimeReconstruction
   ./edw
   ./msi
   ./StochasticLin
   ./fatigue_basics
   ./enc_freq
   ./decayAnalysis
   ./Upcrossing
   ./WaveKinematic

Plots
_____
.. toctree::
   :includehidden:
   :maxdepth: 1

   ./geoMap
   ./pyplotTools

Statistics
__________
.. toctree::
   :includehidden:
   :maxdepth: 1

   ./LongTerm_SD
   ./LongTerm
   ./Extreme_Value_Analysis
   ./DirectIFORM_OE_Paper